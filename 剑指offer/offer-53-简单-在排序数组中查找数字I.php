<?php
/*
剑指 Offer 53 - I. 在排序数组中查找数字 I
统计一个数字在排序数组中出现的次数。

示例 1:
输入: nums = [5,7,7,8,8,10], target = 8
输出: 2

示例 2:
输入: nums = [5,7,7,8,8,10], target = 6
输出: 0

提示：
0 <= nums.length <= 105
-109 <= nums[i] <= 109
nums 是一个非递减数组
-109 <= target <= 109

难度：简单

https://leetcode.cn/problems/zai-pai-xu-shu-zu-zhong-cha-zhao-shu-zi-lcof/

*/

$nums = [5,7,7,8,8,10];
$target = 8;
$obj = new Code_Offer53_1();
var_dump($obj->main($nums, $target));

class Code_Offer53_1
{
    public function main($nums, $target)
    {
        $res = 0;
        foreach ($nums as $num) {
            if ($num == $target) {
                $res++;
            } else if ($num > $target) {
                return $res;
            }
        }
        return $res;
    }

}