<?php
/*
剑指 Offer 32 - II. 从上到下打印二叉树 II
从上到下按层打印二叉树，同一层的节点按从左到右的顺序打印，每一层打印到一行。



例如:
给定二叉树: [3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7
返回其层次遍历结果：

[
  [3],
  [9,20],
  [15,7]
]


提示：

节点总数 <= 1000



难度：简单

https://leetcode.cn/problems/cong-shang-dao-xia-da-yin-er-cha-shu-ii-lcof/


*/

require_once '../class/TreeNode.class.php';
$arr1 = [3,9,20,null,null,15,7];
$head1 = generateTreeByArray($arr1);
$obj = new Code_Offer32_2();
$res = $obj->main($head1);
var_dump($res);

class Code_Offer32_2
{
    public function main($root)
    {
        if (!$root) {
            return [];
        }
        $res = [];
        $queue = new SplQueue();
        $queue->enqueue($root);
        while (!$queue->isEmpty()) {
            $size = $queue->count();
            $tmp = [];
            for ($i = 0; $i < $size; $i++) {
                $cur = $queue->dequeue();
                $tmp[] = $cur->val;
                $cur->left != null && $queue->enqueue($cur->left);
                $cur->right != null && $queue->enqueue($cur->right);
            }
            $res[] = $tmp;
        }
        return $res;
    }
}