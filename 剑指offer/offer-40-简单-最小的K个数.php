<?php
/*
剑指 Offer 40. 最小的k个数
输入整数数组 arr ，找出其中最小的 k 个数。例如，输入4、5、1、6、2、7、3、8这8个数字，则最小的4个数字是1、2、3、4。

示例 1：
输入：arr = [3,2,1], k = 2
输出：[1,2] 或者 [2,1]

示例 2：
输入：arr = [0,1,2,1], k = 1
输出：[0]

限制：
0 <= k <= arr.length <= 10000
0 <= arr[i] <= 10000



难度：简单

https://leetcode.cn/problems/zui-xiao-de-kge-shu-lcof/


*/

$arr = [4,5,1,6,2,7,3,8];$k=4;
$obj = new Code_Offer40();
$res = $obj->main($arr, $k);
var_dump($res);

class Code_Offer40
{
    public function main($arr, $k)
    {
        if ($k == 0) {
            return [];
        }
        $heap = new SplMaxHeap();
        foreach ($arr as $v) {
            if ($heap->count() < $k) {
                $heap->insert($v);
            } else if ($v < $heap->top()) {
                $heap->extract();
                $heap->insert($v);
            }
        }
        $res = [];
        while (!$heap->isEmpty()) {
            $res[] = $heap->extract();
        }
        return $res;

    }
}