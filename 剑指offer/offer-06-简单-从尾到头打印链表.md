# 剑指 Offer 06. 从尾到头打印链表

# 题目
输入一个链表的头节点，从尾到头反过来打印出每个节点的值。


# 解析

### 辅助栈
我们想到解决这个问题肯定是要遍历链表。遍历的顺序是从头到尾，而打印的顺序是从尾到头。也就是说第一个遍历到的节点最后一格输出，
最后一个遍历到的节点第一个输出。这就是典形的“后进先出”，可以用栈实现这个顺序。

每经过一格节点的时候，把节点放到一个栈中。当遍历完后，再从栈顶开始逐个输出节点的值，此时输出的顺序就反过来了。


### 递归
既然想到了用栈来实现，而递归在本质上就是一个栈结构，于是又可以想到用递归来实现。要实现反向输出链表，每访问到一个节点的时候，
先递归输出它后面的节点，再输出节点自身，这样打印结果就反过来了。


### 链表转数组
使用数据接收反转后的链表，只需计算出链表的长度，创建和链表等长度的数组，链表从前向后遍历，同时数组从后向前填充，即可达到反转链表的目的。


# 代码

### php
```php
class Code_Offer06 {

    /**
     * @param ListNode $head
     * @return Integer[]
     */

    // 栈来实现逆序打印
    public function reverseList1($head) {
        $stack = new SplStack();
        $node = $head;
        while ($node != null) {
            $stack->push($node);
            $node = $node->next;
        }
        $res = [];
        while ($stack->isEmpty() == false) {
            $cur = $stack->pop();
            $res[] = $cur->val;
        }
        return $res;
    }

    // 递归实现逆序打印
    private $res2 = [];

    function reverseList2($head) {
        $this->_recur($head);
        return $this->res2;
    }
    
    protected function _recur($head) {
        if ($head == null) {
            return;
        }
        if ($head->next != null) {
            $this->_recur($head->next);
        }
        $this->res2[] = $head->val;
    }
    
    // 链表转数组
    function reverseList3($head) {
        $len = 0;
        $node = $head;
        while ($node != null) {
            $len++;
            $node = $node->next;
        }
        $res = array_fill(0, $len, 0);
        // 链表从前向后遍历  数组从后向前填充
        while ($head != null) {
            $len--;
            $res[$len] = $head->val;
            $head = $head->next;
        }
        return $res;
    }
}
```

### java
```java
// 辅助栈
public int[] reverseList1(ListNode head) {
    Stack<ListNode> st = new Stack();
    while (head != null) {
        st.push(head);
        head = head.next;
    }
    int[] res = new int[st.size()];
    for (int i = 0; i < res.length; i++) {
        ListNode cur = st.pop();
        res[i] = cur.val;
    }
    return res;
}

// 递归
ArrayList<Integer> arr = new ArrayList<>();

public int[] reverseList2(ListNode head) {
    _recur(head);
    int[] res = new int[arr.size()];
    for (int i = 0; i < res.length; i++) {
        res[i]= arr.get(i);
    }
    return res;
}

protected void _recur(ListNode node) {
    if (node == null) {
        return;
    }
    if (node.next != null) {
        _recur(node.next);
    }
    arr.add(node.val);
}

// 链表转数组
public int[] reverseBookList(ListNode head) {
    int len = 0;
    ListNode node = head;
    while (node != null) {
        len++;
        node = node.next;
    }
    int[] res = new int[len];
    while (head != null) {
        len--;
        res[len] = head.val;
        head = head.next;
    }
    return res;
}
```
