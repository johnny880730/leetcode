<?php
/*
剑指 Offer 32 - I. 从上到下打印二叉树
从上到下打印出二叉树的每个节点，同一层的节点按照从左到右的顺序打印。



例如:
给定二叉树: [3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7
返回：

[3,9,20,15,7]


提示：

节点总数 <= 1000



难度：中等

https://leetcode.cn/problems/cong-shang-dao-xia-da-yin-er-cha-shu-lcof/


*/

require_once '../class/TreeNode.class.php';
$arr1 = [3,9,20,null,null,15,7];
$head1 = generateTreeByArray($arr1);
$obj = new Code_Offer32_1();
$res = $obj->main($head1);
var_dump($res);

class Code_Offer32_1
{
    public function main($root)
    {
        $res = [];
        $queue = new SplQueue();
        $queue->enqueue($root);
        while (!$queue->isEmpty()) {
            $cur = $queue->dequeue();
            $res[] = $cur->val;
            $cur->left != null && $queue->enqueue($cur->left);
            $cur->right != null && $queue->enqueue($cur->right);
        }
        return $res;
    }
}