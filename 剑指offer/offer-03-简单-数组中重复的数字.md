# 剑指 Offer 03. 找出数组中重复的数字

# 题目

在一个长度为 n 的数组 nums 里的所有数字都在 0\~n-1 的范围内。
数组中某些数字是重复的，但不知道有几个数字重复了，也不知道每个数字重复了几次。
请找出数组中任意一个重复的数字。

https://leetcode.cn/problems/find-all-duplicates-in-an-array/

# 示例

```
输入：[2, 3, 1, 0, 2, 5, 3]
输出：2 或 3。
```

限制：2 ≤ n ≤ 100000

【进阶】  
在一个长度为 n+1 的数组里的数字都在 1~n 的范围内。，所以数组中至少有一个数字是重复的。请找出任意一个重复的数字，
但不能修改输入的数组。例如，长度为 8 的数组 [2, 3, 5, 4, 3, 2, 6, 7]，输出是重复的数字 2 或者 3。

# 解析

### 原题

可以给数组排序：从排序的数组中找出重复的数字是一件容易的事情。排序一个数组需要 O(nlogn) 的时间。

还利用哈希表来解决这个问题。从头到尾按顺序扫描数组的每个数字，每扫描到一个数字的时候，都可以用 O(1) 的时间
来判断哈希表里是否已经包含了这个数字。如果哈希表里没有，就把它加入哈希表。这个方法的时间复杂度为 O(n)，空间复杂度为 O(n)。

那么有没有空间复杂度为 O(1)的方法呢？

注意到数组中的数字都在 0 ~ n - 1 的范围内 。如果这个数组中没有重复的数字，那么当数组排序之后数字 i 将出现在下标为 i 的位置。
由于数组中有重复的数字，有些位置可能存在多个数字，同时有些位置可能没有数字。

重排这个数组。从头到尾扫描这个数组中的每个数字。当扫描到下标 i 的数字时，首先比较这个数字 m 是不是等于 i。如果是，则扫描下一个数字；
如果不是，则拿它和第 m 个数字进行比较。如果它和第 m 个数字相等，就找到了一个重复的数字（该数字在下标为 i 和 m 的位置出现了）；
如果它和第 m 个数字不相等，就把 nums[i] 和 nums[m] 交换，把 m 放到属于它的位置。接下来重复这个比较，直到发现一个重复的数字。

以 [2, 3, 1, 0, 2, 5, 3] 为例：

- nums[0] = 2，与下标不相等，于是和 nums[2] 交换。此时数组为 [1, 3, 2, 0, 2, 5, 3]；
- nums[0] = 1，与下标不相等，于是和 nums[1] 交换。此时数组为 [3, 1, 2, 0, 2, 5, 3]；
- nums[0] = 3，与下标不相等，于是和 nums[3] 交换。此时数组为 [0, 1, 2, 3, 2, 5, 3]；
- nums[0] = 0，与下标相等，扫描下一个数字。
- nums[1]、nums[2]、nums[3] 的数字与下标都是相等的，不需要执行任何操作。
- nums[4] = 2，与下标不等，又发现 nums[4] 与 nums[2] 相等，因此找到了一个重复的数字，返回即可。
- 代码参考 findDuplication1 方法

### 进阶问题

题目要求不能修改输入数组，我们可以创建一个长度为 n + 1 的辅助数组 help，然后逐一把原数组的每个数字复制到辅助数组。
如果 nums[i] = m，则把它赋值到 help[m] = m。这样就很容易发现哪个是重复的。

接下来尝试避免使用这个辅助数组。如果数组中没有重复的数字，那么在 1 ~ n 范围里只有 n 个数字。由于数组里包含超过 n 个
数字，所以一定包含了重复的数字。

我们把从 1 ~ n 的数字从中间的数字 m 分为两个部分，前一半为 1 ~ m，后一半为 m + 1 ~ n。如果 1 ~ m 的数字数目超过了 m，
那么这一版的区间里一定包含了重复的数字；否则就是另一半一定包含了重复的数字。我们可以继续把包含重复数字的区间一分为二，直到找到一个重复的
数字。这个过程和二分查找很相似，只是多了一步统计区间里数字的数目。

还是以 [2, 3, 5, 4, 3, 2, 6, 7] 为例。这个长度为 8 的数组里的数字都在 1 ~ 7 的范围。中间数字 4 把 1 ~ 7 分为两端，
一段 1 ~ 4，另一段 5 ~ 7。接下来统计 1 ~ 4 这四个数字出现了 5 次，因此这一段一定有重复的数字。再把 1 ~ 4 一分为二，一段是 1、2
两个数字，
另一端是 3、4 两个数字。数字 1 或者 2 移动出现了两次。数字 3 或者 4 出现了三次，意味着 3、4 中肯定有重复的数字。再分别统计这两个数字的次数，
就发现数字 3 出现了两次，返回即可。

代码参考 findDuplication2 方法

按照二分的思路，长度为 n 的数组，函数 countRange 调用 O(logn) 次，每次需要 O(n) 的时间，所以总的时间复杂度为 O(nlogn)，
空间复杂度为 O(1)。和前面那个需要 O(n) 的辅助数组相比，相当于以时间换空间。

另外需要指出的是，这个算法不能保证找出所有重复的数字。比如就没法找出例子里的数字 2。

# 代码

### php
```php
class Offer03 {

    public function findDuplication1($nums) {
        $len = count($nums);
        for ($i = 0; $i < $len; $i++) {
            while ($nums[$i] != $i) {
                if ($nums[$i] == $nums[$nums[$i]]) {
                    return $nums[$i];
                }
                $tmp = $nums[$nums[$i]];
                $nums[$nums[$i]] = $nums[$i];
                $nums[$i] = $tmp;
            }
        }
    }


    public function findDuplication2($nums) {
        $start = 1;
        $len = count($nums);
        $end = $len - 1;
        while ($end >= $start) {
            $mid = (($end - $start) >> 1) + $start;
            $count = $this->countRange($nums, $len, $start, $mid);
            if ($end == $start) {
                if ($count > 1) {
                    return $start;
                } else {
                    break;
                }
            }

            if ($count > ($mid - $start + 1)) {
                $end = $mid;
            } else {
                 $start = $mid + 1;
            }
        }
        return -1;
    }

    protected function countRange($nums, $len, $start, $end) {
        $count = 0;
        for ($i = 0; $i < $len; $i++) {
            if ($nums[$i] >= $start && $nums[$i] <= $end) {
                ++$count;
            }
        }
        return $count;
    }
}
```

### java
```java
class Offer03 {

    public int findRepeatNumber(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            while (nums[i] != i) {
                if (nums[i] == nums[nums[i]]) {
                    return nums[i];
                }
                int tmp = nums[i];
                nums[i] = nums[tmp];
                nums[tmp] = tmp;
            }
        }
        return -1;
    }
}
```

### go
```go
func findRepeatNumber(nums []int) int {
    for i := 0; i < len(nums); i++ {
        for nums[i] != i {
            if nums[i] == nums[nums[i]] {
                return nums[i]
            }
            nums[i], nums[nums[i]] = nums[nums[i]], nums[i]
        }
    }
    return -1
} 
```
