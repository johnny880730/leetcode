<?php
/*
剑指 Offer 64. 求1+2+…+n
求 1+2+...+n ，要求不能使用乘除法、for、while、if、else、switch、case等关键字及条件判断语句（A?B:C）。


示例 1：
输入: n = 3
输出: 6

示例 2：
输入: n = 9
输出: 45


限制：

1 <= n <= 10000


难度：中等

https://leetcode.cn/problems/qiu-12n-lcof/


*/

$n = 9;
$obj = new Code_Offer64();
$res = $obj->main($n);
var_dump($res);

class Code_Offer64
{
    // 位运算符的“短路“效应
    public function main($n)
    {
        $n > 1 && $n += $this->main($n - 1);
        return $n;
    }
}