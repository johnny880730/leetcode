<?php
/*
剑指 Offer 28. 对称的二叉树
请实现一个函数，用来判断一棵二叉树是不是对称的。如果一棵二叉树和它的镜像一样，那么它是对称的。

例如，二叉树 [1,2,2,3,4,4,3] 是对称的。

    1
   / \
  2   2
 / \ / \
3  4 4  3
但是下面这个 [1,2,2,null,3,null,3] 则不是镜像对称的:

    1
   / \
  2   2
   \   \
   3    3


示例 1：
输入：root = [1,2,2,3,4,4,3]
输出：true

示例 2：
输入：root = [1,2,2,null,3,null,3]
输出：false


限制：
0 <= 节点个数 <= 1000

难度：简单

https://leetcode.cn/problems/er-cha-shu-de-jing-xiang-lcof/


*/

require_once '../class/TreeNode.class.php';
$arr1 = [1,2,2,3,4,4,3];
$arr1 = [1,2,2,null,3,null,3];
$head1 = generateTreeByArray($arr1);
$obj = new Code_Offer28();
$res = $obj->main($head1);
var_dump($res);

class Code_Offer28
{
    public function main($root)
    {
        if ($root == null) {
            return true;
        }
        return $this->_check($root, $root);
    }

    protected function _check($left, $right)
    {
        if ($left == null && $right == null) {
            return true;
        }
        if ($left == null || $right == null) {
            return false;
        }
        if ($left->val != $right->val) {
            return false;
        }
        return $this->_check($left->left, $right->right)
            && $this->_check($left->right, $right->left);
    }
}