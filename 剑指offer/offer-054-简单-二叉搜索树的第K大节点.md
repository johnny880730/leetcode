# 剑指 Offer 54. 二叉搜索树的第k大节点

# 题目
给定一棵二叉搜索树，请找出其中第 k 大的节点。

提示：
- 1 <= head.length <= 100
- 0 <= head[i] <= 100
- 1 <= cnt <= head.length

https://leetcode.cn/problems/er-cha-sou-suo-shu-de-di-kda-jie-dian-lcof/description/

# 示例
```
示例 1:
输入: root = [3,1,4,null,2], k = 1
   3
  / \
 1   4
  \
   2
输出: 4
```
```
示例 2:
输入: root = [5,3,6,2,4,null,null,1], k = 3
       5
      / \
     3   6
    / \
   2   4
  /
 1
输出: 4
```

# 解析

二叉搜索树的中序遍历为递增序列。根据此性质，易得二叉搜索树的 **中序遍历倒序** 为 **递减序列** 。

因此，可将求 “二叉搜索树第 cnt 大的节点” 可转化为求 “此树的中序遍历倒序的第 cnt 个节点”

求第 cnt 个节点，需要实现以下三项工作：
- 递归遍历时计数，统计当前节点的序号；
- 递归到第 cnt 个节点时，应记录结果 res ；
- 记录结果后，后续的遍历即失去意义，应提前终止（即返回）

递归解析：
1. 终止条件： 当节点 root 为空（越过叶节点），则直接返回；
2. 递归右子树： 即 dfs(root.right) ；
3. 递推工作：
    - 提前返回： 若 cnt = 0，代表已找到目标节点，无需继续遍历，因此直接返回；
    - 统计序号： 执行 cnt = cnt − 1 （即从 cnt 减至 00 ）；
    - 记录结果： 若 cnt = 0 ，代表当前节点为第 cnt 大的节点，因此记录 res = root.val；
4. 递归左子树： 即 dfs(root.left)；


# 代码

### php
```php
class Offer054 {

    public $res;
    public $cnt;

    function findTargetNode($root, $cnt) {
        $this->cnt = $cnt;
        $this->_dfs($root);
        return $this->res;
    }

    private function _dfs($node) {
        if (!$node || $this->cnt == 0) {
            return;
        }
        $this->_dfs($node->right);
        $this->cnt -= 1;
        if ($this->cnt == 0) {
            $this->res = $node->val;
            return;
        }
        $this->_dfs($node->left);
    }

}
```

### java
```java
class Offer054 {

    private int res;
    private int cnt;

    public int findTargetNode(TreeNode root, int cnt) {
        this.cnt = cnt;
        _dfs(root);
        return this.res;
    }

    private void _dfs(TreeNode node) {
        if (node == null || this.cnt == 0) {
            return;
        }
        _dfs(node.right);
        this.cnt -= 1;
        if (this.cnt == 0) {
            this.res = node.val;
            return;
        }
        _dfs(node.left);
    }
}
```

### go
```go
var k int
var res int

func findTargetNode(root *TreeNode, cnt int) int {
    k = cnt
    _dfs(root)
    return res
}

func _dfs(node *TreeNode) {
    if node == nil || k == 0 {
        return
    }
    _dfs(node.Right)
    k -= 1
    if k == 0 {
        res = node.Val
        return
    }
    _dfs(node.Left)
}
```