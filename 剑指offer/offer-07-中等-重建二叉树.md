## 剑指 Offer 07. 重建二叉树

# 题目
输入某二叉树的前序遍历和中序遍历的结果，请构建该二叉树并返回其根节点。

假设输入的前序遍历和中序遍历的结果中都不含重复的数字。



【示例】
```
        3
       / \
      9   20
         /  \
        15   7
```  
输入: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]  
输出: [3,9,20,null,null,15,7]

限制：0 ≤ 节点个数 ≤ 5000

# 解析
&emsp;&emsp;前序遍历序列中，第一个数字总是树的根节点的值。但在中序里，根节点的值在序列的中间，左子树的节点在根节点值的左边，
右子树的节点在根节点值的右边。

&emsp;&emsp;既然已经找到了左、右子树的前序遍历序列和中序遍历序列，那么可以用同样的方法分别构建左、右子树。也就是说，
接下来的事情可以用递归的方法去完成。

# 代码
```php
require_once '../class/TreeNode.class.php';
$preorder = [3, 9, 20, 15, 7];
$inorder  = [9, 3, 15, 20, 7];
$obj      = new Code_Offer07();
var_dump($obj->main($preorder, $inorder));

class Code_Offer07
{
    /*
     * 前序遍历：[根, [左子树], [右子树]]
     * 中序遍历：[[左子树], 根, [右子树]]
     */
    function main($preorder, $inorder)
    {
        $tree = new TreeNode(null);
        $this->_build($tree, $preorder, $inorder);
        return $tree;
    }

    protected function _build(&$node, $preorder, $inorder)
    {
        if (!$preorder) {
            return ;
        }
        $cur = $preorder[0];
        $node = new TreeNode($cur);
        $idx = array_search($cur, $inorder);

        $preLeft = array_slice($preorder, 1, $idx);     //前序里的左子树
        $inLeft = array_slice($inorder, 0, $idx);       //中序里的左子树
        $this->_build($node->left, $preLeft, $inLeft);   //递归创建左子树

        $preRight = array_slice($preorder, $idx+1);     //前序里的右子树
        $inRight = array_slice($inorder, $idx+1);       //中序里的右子树
        $this->_build($node->right, $preRight, $inRight);//递归创建右子树

    }
}
```