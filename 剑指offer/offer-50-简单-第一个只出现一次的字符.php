<?php
/*
剑指 Offer 50. 第一个只出现一次的字符
在字符串 s 中找出第一个只出现一次的字符。如果没有，返回一个单空格。 s 只包含小写字母。

示例 1:
输入：s = "abaccdeff"
输出：'b'

示例 2:
输入：s = ""
输出：' '


限制：

0 <= s 的长度 <= 50000

难度：简单

https://leetcode.cn/problems/di-yi-ge-zhi-chu-xian-yi-ci-de-zi-fu-lcof/

*/

$s = 'abaccdeff';
$s = '';
$obj = new Code_Offer50();
var_dump($obj->main($s));

class Code_Offer50
{
    public function main($s)
    {
        $hash = [];
        for($i = 0, $len = strlen($s); $i < $len; $i++) {
            if (array_key_exists($s[$i], $hash)) {
                $hash[$s[$i]] = -1;
            } else {
                $hash[$s[$i]] = $i;
            }
        }
        $res = PHP_INT_MAX;
        foreach ($hash as $c => $n) {
            if ($n != -1) {
                $res = min($n, $res);
            }
        }
        return $res == PHP_INT_MAX ? ' ' : $s[$res];
    }

}