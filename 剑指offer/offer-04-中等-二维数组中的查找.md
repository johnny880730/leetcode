# 剑指 Offer 04. 二维数组中的查找

# 题目
在一个 n × m 的二维数组中，每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。
请完成一个高效的函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。

# 示例  
现有矩阵 matrix 如下：  
```
[  
 [1, 4, 7, 11, 15],  
 [2, 5, 8, 12, 19],  
 [3, 6, 9, 16, 22],  
 [10, 13, 14, 17, 24],  
 [18, 21, 23, 26, 30]  
]  

给定 target = 5，返回 true。  
给定 target = 20，返回 false。
```

限制：  
0 <= n <= 1000  
0 <= m <= 1000

# 解析
首先选组数组中右上角的数组。如果该数字等于要查找的数字，则查找过程结束；如果该数字大于要查找的数字，则剔除这一列；

如果该数字小于要查找的数字，则提出这一行。也就是说，如果要查找的数字不在数组的右上角，则每一次都在数组的查找范围中剔除一行或一列，
这样每一步都可以缩小查找的范围，直到找到数字，或者查找范围为空。

我们也可以选组左下角的数字，但不能选组左上角的数字或者右下角的数字。

# 代码

### php
```php
class Offer04 {

    function findInMatrix($m, $target) {
        // 从右上角开始
        $i = 0;
        $j = count($m) - 1;
        while ($j >= 0 && $i < count($m)) {
            if ($m[$i][$j] > $target) {
                $j--;
            } else if ($m[$i][$j] < $target) {
                $i++;
            } else {
                return true;
            }
        }
        return false;
    }
}
```

### java
```java
class Offer04 {

    public boolean findNumberIn2DArray(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return false;
        }
        int i = 0, j = matrix[0].length - 1;
        while (j >= 0 && i < matrix.length) {
            if (matrix[i][j] == target) {
                return true;
            } else if (matrix[i][j] > target) {
                j--;
            } else {
                i++;
            }
        }
        return false;
    }
    
}
```
