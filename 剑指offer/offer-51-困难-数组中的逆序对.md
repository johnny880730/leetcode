### 剑指 Offer 51. 数组中的逆序对

# 题目
在数组中的两个数字，如果前面一个数字大于后面的数字，则这两个数字组成一个逆序对。输入一个数组，求出这个数组中的逆序对的总数。

提示：
- 0 <= 数组长度 <= 50000

# 示例
```
输入: [7,5,6,4]
输出: 5
```

# 解析

#### 归并排序的思想
&emsp;&emsp;「归并排序」是分治思想的典型应用，它包含这样三个步骤：
- 分解：待排序的区间为 [l, r]，令 mid = floor((l + r) / 2)，即分成了 [l, mid] 和 [mid, r]
- 解决：使用归并排序递归地排序两个子序列   
- 合并：把两个已经排好序的子序列

&emsp;&emsp;那么求逆序对和归并排序又有什么关系呢？关键就在于「归并」当中「并」的过程。通过一个实例来看看。
假设有两个已排序的序列等待合并，分别是 L = [8, 12, 16, 22, 100] 和 R = [9, 26, 55, 64, 91]。
一开始用指针 lPtr = 0 指向 L 的首部，rPtr = 0 指向 R 的头部。记已经合并好的部分为 M。
```
L = [8, 12, 16, 22, 100]   R = [9, 26, 55, 64, 91]  M = []
     |                          |
    lPtr                       rPtr
```

&emsp;&emsp;发现 lPtr 指向的元素小于 rPtr 指向的元素，于是把 lPtr 指向的元素放入答案，并把 lPtr 后移一位。
``` 
L = [8, 12, 16, 22, 100]   R = [9, 26, 55, 64, 91]  M = [8]
        |                       |
      lPtr                     rPtr
```

&emsp;&emsp;这个时候把左边的 8 加入了答案，发现右边没有数比 8 小，所以 8 对逆序对总数的「贡献」为 0。
接着继续合并，把 9 加入了答案，此时 lPtr 指向 12，rPtr 指向 26。
```
L = [8, 12, 16, 22, 100]   R = [9, 26, 55, 64, 91]  M = [8, 9]
        |                          |
       lPtr                       rPtr
```

&emsp;&emsp;此时 lPtr 比 rPtr 小，把 lPtr 对应的数加入答案，并考虑它对逆序对总数的贡献为 rPtr 相对 R 首
位置的偏移 1（即右边只有一个数比 12 小，所以只有它和 12 构成逆序对），以此类推。

&emsp;&emsp;可以发现用这种「算贡献」的思想在合并的过程中计算逆序对的数量的时候，只在 lPtr 右移的时候计算，是基于这样的事实：
当前 lPtr 指向的数字比 rPtr 小，但是比 R 中 [0 ... rPtr - 1] 的其他数字大，[0 ... rPtr - 1] 的其他数字本应当排在 
lPtr 对应数字的左边，但是它排在了右边，所以这里就贡献了 rPtr 个逆序对。

# 代码

```php
$arg = [7, 5, 6, 4];
(new Offer51)->main($arg);

class Offer51
{
    private $res = 0;

    function main($arg)
    {
        echo $this->reversePairs($arg);
    }

    function reversePairs($nums)
    {
        if (count($nums) < 2) {
            return 0;
        }
        $this->sort($nums, 0, count($nums) - 1);
        return $this->res;
    }

    private function sort(&$arr, $left, $right) {
        if ($left >= $right) {
            return;
        }
        $mid = $left + (($right - $left) >> 1);
        $this->sort($arr, $left, $mid);
        $this->sort($arr, $mid + 1, $right);
        $this->merge($arr, $left, $mid, $right);
    }

    private function merge(&$nums, $left, $mid, $right) {
        $i = 0;
        $p1 = $left;
        $p2 = $mid + 1;
        $help = [];
        while ($p1 <= $mid && $p2 <= $right) {
            $this->res += $nums[$p1] > $nums[$p2] ? $mid - $p1 + 1 : 0;         // 计算逆序对
            $help[$i++] = $nums[$p1] <= $nums[$p2] ? $nums[$p1++] : $nums[$p2++];
        }

        while ($p1 <= $mid) {
            $help[$i++] = $nums[$p1++];
        }

        while ($p2 <= $right) {
            $help[$i++] = $nums[$p2++];
        }

        // 将合并结果拷贝回原数组
        for ($k = 0; $k < count($help); $k++) {
            $nums[$left + $k] = $help[$k];
        }
    }
}
```