## 剑指 Offer 10- II. 青蛙跳台阶问题

# 题目
一只青蛙一次可以跳上1级台阶，也可以跳上2级台阶。求该青蛙跳上一个 n 级的台阶总共有多少种跳法。

答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。

【示例】  
输入：n = 7  
输出：21

提示：0 ≤ n ≤ 100

# 代码
```php
$n = 7;
$obj = new Code_Offer10_2();
var_dump($obj->main($n));

class Code_Offer10_2
{
    public function main($n)
    {
        if ($n == 0) {
            return 1;
        }
        if ($n <= 2) {
            return $n;
        }
        $dp[1] = 1;
        $dp[2] = 2;
        for ($i = 3; $i <= $n ;$i++) {
            $dp[$i] = $dp[$i - 1] + $dp[$i - 2];
            $dp[$i] %= 1000000007;
        }
        return $dp[$n];
    }
}
```