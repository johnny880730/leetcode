<?php
/*
剑指 Offer 27. 二叉树的镜像
请完成一个函数，输入一个二叉树，该函数输出它的镜像。

例如输入：
     4
   /   \
  2     7
 / \   / \
1   3 6   9

镜像输出：
     4
   /   \
  7     2
 / \   / \
9   6 3   1

示例 1：
输入：root = [4,2,7,1,3,6,9]
输出：[4,7,2,9,6,3,1]

限制：
0 <= 节点个数 <= 1000

难度：简单

https://leetcode.cn/problems/er-cha-shu-de-jing-xiang-lcof/


*/

require_once '../class/TreeNode.class.php';
$arr1 = [4,2,7,1,3,6,9];
$head1 = generateTreeByArray($arr1);
$obj = new Code_Offer27();
$res = $obj->main($head1);
var_dump($res);

class Code_Offer27
{
    public function main($root)
    {
        if ($root == null) {
            return null;
        }
        $tmp = $root->left;
        $root->left = $root->right;
        $root->right = $tmp;
        $this->main($root->left);
        $this->main($root->right);
        return $root;
    }
}