<?php
/*
剑指 Offer 45. 把数组排成最小的数
输入一个非负整数数组，把数组里所有数字拼接起来排成一个数，打印能拼接出的所有数字中最小的一个。

示例 1:
输入: [10,2]
输出: "102"

示例 2:
输入: [3,30,34,5,9]
输出: "3033459"


提示:
0 < nums.length <= 100

说明:
输出结果可能非常大，所以你需要返回一个字符串而不是整数
拼接起来的数字可能会有前导 0，最后结果不需要去掉前导 0

难度：中等

https://leetcode.cn/problems/ba-shu-zu-pai-cheng-zui-xiao-de-shu-lcof/


*/

$arr1 = [3,30,34,5,9];
$obj = new Code_Offer54();
$res = $obj->main($arr1);
var_dump($res);

class Test
{
    public $val;

    public function __construct($val)
    {
        $this->val = $val;
    }
}

class Code_Offer54
{
    // 自带的usort函数，实现匿名函数的自定义排序
    public function main($nums)
    {
//        var_dump('303' > '330');exit;
        usort($nums, function($a, $b) {
            $aa = strval($a);
            $bb = strval($b);
            return ($aa . $bb) > ($bb . $aa);
        });
        return join('', $nums);
    }
}


