<?php
/*
剑指 Offer 61. 扑克牌中的顺子
从若干副扑克牌中随机抽 5 张牌，判断是不是一个顺子，即这5张牌是不是连续的。2～10为数字本身，A为1，J为11，Q为12，K为13，而大、小王为 0 ，可以看成任意数字。A 不能视为 14。



示例 1:

输入: [1,2,3,4,5]
输出: True


示例 2:

输入: [0,0,1,2,5]
输出: True


限制：
数组长度为 5
数组的数取值为 [0, 13] .

https://leetcode.cn/problems/bu-ke-pai-zhong-de-shun-zi-lcof/

*/

$nums =  [0,0,1,2,5];
$nums =  [1,2,3,4,5];
$obj = new Code_Offer61();
var_dump($obj->main($nums));

class Code_Offer61
{
    public function main($nums)
    {
        $hash = [];
        $max = -1;
        $min = 20;
        foreach ($nums as $num) {
            if ($num === 0) {
                continue;
            } else if (array_key_exists($num, $hash)) {
                return false;
            } else {
                $hash[$num] = true;
                $max = max($max, $num);
                $min = min($min, $num);
            }
        }
        // 不能max-min+1 == 5 比如[11,10,0,0,0]
        return $max - $min < 5;

    }

}