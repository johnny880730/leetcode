<?php
/*
剑指 Offer 58 - II. 左旋转字符串

难度：简单

字符串的左旋转操作是把字符串前面的若干个字符转移到字符串的尾部。请定义一个函数实现字符串左旋转操作的功能。比如，输入字符串"abcdefg"和数字2，该函数将返回左旋转两位得到的结果"cdefgab"。

 

示例 1：

输入: s = "abcdefg", k = 2
输出:"cdefgab"
示例 2：

输入: s = "lrloseumgh", k = 6
输出:"umghlrlose"


限制：

1 <= k < s.length <= 10000

来源：力扣（LeetCode）
链接：https://leetcode.cn/problems/zuo-xuan-zhuan-zi-fu-chuan-lcof
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。

*/

$s = "abcdefg";
$k = 2;
$obj = new Code_Offer58_2();
var_dump($obj->main($s, $k));

class Code_Offer58_2
{
    public function main($s, $k)
    {
        // s = abcdefg, k = 2
        $len = strlen($s);

        // 反转k之前的部分  abcdefg => bacdefg
        $s = $this->reverse($s, 0, $k - 1);

        // 反转k之后的部分 bacdefg => bagfedc
        $s = $this->reverse($s, $k, $len - 1);

        // 反转完整字符串  bagfedc => cdefgab
        $s = $this->reverse($s, 0, $len - 1);

        return $s;
    }

    protected function reverse($str, $start, $end)
    {
        while ($start < $end) {
            $tmp = $str[$start];
            $str[$start] = $str[$end];
            $str[$end] = $tmp;
            $start++;
            $end--;
        }
        return $str;
    }
}