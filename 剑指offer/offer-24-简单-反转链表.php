<?php
/*
剑指 Offer 24. 反转链表
定义一个函数，输入一个链表的头节点，反转该链表并输出反转后链表的头节点。

示例:

输入: 1->2->3->4->5->NULL
输出: 5->4->3->2->1->NULL

限制：
0 <= 节点个数 <= 5000



难度：简单

https://leetcode.cn/problems/fan-zhuan-lian-biao-lcof/


*/

require_once '../class/ListNode.class.php';
$arr = [1,2,3,4,5,];
$head = array2LinkList($arr);
$obj = new Code_Offer24();
$res = $obj->main($head);
fetchNode($res);

class Code_Offer24
{
    public function main($head)
    {
        $prev = new ListNode(null);
        $cur = $head;
        while ($cur) {
            $next = $cur->next;
            $cur->next = $prev;
            $prev = $cur;
            $cur = $next;
        }
        $head->next = null;
        return $prev;
    }
}