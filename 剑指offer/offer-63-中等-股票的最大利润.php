<?php
/*
剑指 Offer 63. 股票的最大利润
假设把某股票的价格按照时间先后顺序存储在数组中，请问买卖该股票一次可能获得的最大利润是多少？


示例 1:

输入: [7,1,5,3,6,4]
输出: 5
解释: 在第 2 天（股票价格 = 1）的时候买入，在第 5 天（股票价格 = 6）的时候卖出，最大利润 = 6-1 = 5 。
     注意利润不能是 7-1 = 6, 因为卖出价格需要大于买入价格。
示例 2:

输入: [7,6,4,3,1]
输出: 0
解释: 在这种情况下, 没有交易完成, 所以最大利润为 0。


限制：

0 <= 数组长度 <= 10^5


难度：中等

https://leetcode.cn/problems/gu-piao-de-zui-da-li-run-lcof/


*/

$arr1 = [7,1,5,3,6,4];
$obj = new Code_Offer63();
$res = $obj->main($arr1);
var_dump($res);

class Code_Offer63
{
    /*
        两个变量
        1，保存最小值
        2，保存当前的最大利润
     */
    public function main($arr)
    {
        $min = $arr[0];
        $max = 0;
        for ($i = 1, $len = count($arr); $i < $len; $i++) {
            if ($arr[$i] < $min) {
                $min = $arr[$i];
            } else {
                $max = max($max, $arr[$i] - $min);
            }
        }
        return $max;

    }
}