<?php
/*
剑指 Offer 42. 连续子数组的最大和
输入一个整型数组，数组中的一个或连续多个整数组成一个子数组。求所有子数组的和的最大值。

要求时间复杂度为O(n)。

示例1:
输入: nums = [-2,1,-3,4,-1,2,1,-5,4]
输出: 6
解释: 连续子数组 [4,-1,2,1] 的和最大，为 6。


提示：
1 <= arr.length <= 10^5
-100 <= arr[i] <= 100



难度：简单

https://leetcode.cn/problems/lian-xu-zi-shu-zu-de-zui-da-he-lcof/


*/

$arr = [-2, 1, -3, 4, -1, 2, 1, -5, 4];
$obj = new Code_Offer42();
$res = $obj->main($arr);
var_dump($res);

class Code_Offer42
{
    // dp[i]表示以第i个元素结尾的最大子数组的和
    public function main($arr)
    {
        if (empty($arr)) {
            return 0;
        }
        $len   = count($arr);
        $dp    = [];
        $dp[0] = $arr[0];
        for ($i = 1; $i < $len; $i++) {
            if ($dp[$i - 1] > 0) {
                // 如果以arr[i-1]结尾的最大子数组和为正数，那么第i个元素的最大子数组和就是自己本身+dp[i-1]
                $dp[$i] = $arr[$i] + $dp[$i - 1];
            } else {
                // 如果以arr[i-1]结尾的最大子数组和为负数，直接就是arr[i]，因为负数+arr[i]<arr[i]
                $dp[$i] = $arr[$i];
            }
        }
        return max($dp);
    }
}