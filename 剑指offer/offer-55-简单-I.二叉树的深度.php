<?php
/*
剑指 Offer 55 - I. 二叉树的深度
输入一棵二叉树的根节点，求该树的深度。从根节点到叶节点依次经过的节点（含根、叶节点）形成树的一条路径，最长路径的长度为树的深度。

例如：
给定二叉树 [3,9,20,null,null,15,7]，

    3
   / \
  9  20
    /  \
   15   7
返回它的最大深度 3 。


提示：
节点总数 <= 10000

难度：简单

https://leetcode.cn/problems/er-cha-shu-de-shen-du-lcof/

*/

require_once '../class/TreeNode.class.php';
$nums = [3,9,20,null,null,15,7];
$root = generateTreeByArray($nums);
$obj = new Code_Offer55_1();
var_dump($obj->main($root));

class Code_Offer55_1
{
    public function main($root)
    {
        if ($root == null) {
            return 0;
        }
        $leftDepth = $this->main($root->left);
        $rightDepth = $this->main($root->right);
        return max($leftDepth, $rightDepth) + 1;
    }

}