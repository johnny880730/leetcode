# 剑指 Offer 05. 替换空格

# 题目
请实现一个函数，把字符串 s 中的每个空格替换成 "%20"。

https://leetcode.cn/problems/ti-huan-kong-ge-lcof

# 示例
```
输入：s = "We are happy."  
输出："We%20are%20happy."
```

限制：0 ≤ s 的长度 ≤ 10000

# 解析
先遍历一次字符串，统计出空格的总数，并可以计算出替换之后的字符串的总长度。每替换一个空格，长度增加 2，因此替换之后的字符串的
长度等于原来的长度加上 2 * 空格数。

然后从字符串的后面开始复制和替换。首先准备两个指针 p1 和 p2。p1 指向原始字符串的末尾，p2 指向替换之后的字符串的末尾。

接下来向前移动 p1，逐个把它指向的字符复制到 p2 位置，直到碰到第一个空格。

碰到第一个空格之后，p1 也要向前移动一格，在 p2 之前插入 "%20"。由于 "%20" 长度为3，所以 p2 也要向前移动三格。接着继续向前复制。

可以看出，所有的字符都只复制（移动）了一次，因此时间复杂度为 O(n)。

# 代码 

### php
```php
class Offer05 {

    public function replaceBlank($s) {
        $len = strlen($s);
        $num = 0;
        for ($i = 0; $i < $len; $i++) {
            if ($s[$i] == ' ') {
                $num++;
            }
        }
        $newLen = $len + $num * 2;
        $p1 = $len - 1;
        $p2 = $newLen - 1;
        while ($p1 >= 0 && $p2 >= $p1) {
            if ($s[$p1] == ' ') {
                $s[$p2--] = '0';
                $s[$p2--] = '2';
                $s[$p2--] = '%';
            } else {
                $s[$p2--] = $s[$p1];
            }
            --$p1;
        }
        return $s;
    }
}
```

### java
```java
class Offer05 {

    public String replaceSpace(String s) {
        int num = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                num++;
            }
        }
        int newLen = s.length() + 2 * num;
        char[] str = new char[newLen];
        int p1 = s.length() - 1, p2 = newLen - 1;
        while (p1 >= 0 && p2 >= p1) {
            if (s.charAt(p1) == ' ') {
                str[p2--] = '0';
                str[p2--] = '2';
                str[p2--] = '%';
            } else {
                str[p2--] = s.charAt(p1);
            }
            p1--;
        }
        String newStr = new String(str, 0, newLen);
        return newStr;
    }
    
}
```
