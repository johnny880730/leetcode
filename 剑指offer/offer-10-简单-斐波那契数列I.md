## 剑指 Offer 10- I. 斐波那契数列

# 题目
写一个函数，输入 n ，求斐波那契数列的第 n 项。

答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。

提示：0 ≤ n ≤ 100

# 代码
```php
$n = 5;
$obj = new Code_Offer10_1();
var_dump($obj->main($n));

class Code_Offer10_1
{
    public function main($n)
    {
        if ($n == 0) return 0;

        $dp[0] = 0;
        $dp[1] = 1;
        for ($i = 2; $i <= $n; $i++) {
            $dp[$i] = $dp[$i - 1] + $dp[$i - 2];
            $dp[$i] %= 1000000007;
        }
        return $dp[$n];
    }
}
```