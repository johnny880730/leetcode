## 剑指 Offer 09. 用两个栈实现队列

# 题目
用两个栈实现一个队列。队列的声明如下，请实现它的两个函数 appendTail 和 deleteHead，分别完成在队列尾部插入整数和在队列头部删除整数的功能。

若队列中没有元素，deleteHead 操作返回 -1。 

【示例 1】    
输入：  
["CQueue","appendTail","deleteHead","deleteHead"]  
[[],[3],[],[]]  
输出：[null,null,3,-1]

【示例 2】  
输入：  
["CQueue","deleteHead","appendTail","appendTail","deleteHead","deleteHead"]  
[[],[],[5],[2],[],[]]  
输出：[null,-1,null,null,5,2]  

提示：  
1 ≤ values ≤ 10000  
最多会对appendTail、deleteHead 进行10000次调用

# 代码
```php
$obj = new Code_Offer09();
$obj->main();

class Code_Offer09
{
    function main()
    {
        $obj = new CQueue();
        $obj->deleteHead();
        $obj->appendTail(5);
        $obj->appendTail(2);
        $obj->deleteHead();
        $obj->deleteHead();
    }
}

class CQueue
{
    public $stack1;     //主栈
    public $stack2;     //辅助栈

    /**
     */
    function __construct()
    {
        $this->stack1 = new SplStack();
        $this->stack2 = new SplStack();
    }

    /**
     * @param Integer $value
     * @return NULL
     */
    function appendTail($value)
    {
        $this->stack1->push($value);
    }

    /**
     * @return Integer
     */
    function deleteHead()
    {
        if (!$this->stack2->isEmpty()) {
            return $this->stack2->pop();
        }
        if ($this->stack1->isEmpty()) {
            return -1;
        }
        while (!$this->stack1->isEmpty()) {
            $this->stack2->push($this->stack1->pop());
        }
        return $this->stack2->pop();
    }
}

/**
 * Your CQueue object will be instantiated and called as such:
 * $obj = CQueue();
 * $obj->appendTail($value);
 * $ret_2 = $obj->deleteHead();
 */
```