<?php
/*
输入一棵二叉树的根节点，判断该树是不是平衡二叉树。
如果某二叉树中任意节点的左右子树的深度相差不超过1，那么它就是一棵平衡二叉树。

示例 1:

给定二叉树 [3,9,20,null,null,15,7]

    3
   / \
  9  20
    /  \
   15   7
返回 true 。

示例 2:

给定二叉树 [1,2,2,3,3,null,null,4,4]

       1
      / \
     2   2
    / \
   3   3
  / \
 4   4
返回false 。

限制：

0 <= 树的结点个数 <= 10000

来源：力扣（LeetCode）
链接：https://leetcode.cn/problems/ping-heng-er-cha-shu-lcof
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。

*/

require_once '../class/TreeNode.class.php';
$nums = [1,2,2,3,3,null,null,4,4];
$nums = [3,9,20,null,null,15,7];
$root = generateTreeByArray($nums);
$obj = new Code_Offer55_2();
var_dump($obj->main($root));

class Code_Offer55_2
{
    public function main($head)
    {
        if ($head == null) {
            return true;
        }
        $leftDepth  = $this->getTreeDepth($head->left);
        $rightDepth = $this->getTreeDepth($head->right);
        return abs($leftDepth - $rightDepth) <= 1
            && $this->main($head->left)
            && $this->main($head->right);
    }

    protected function getTreeDepth($node)
    {
        if ($node == null) {
            return 0;
        }
        $leftDepth  = $this->getTreeDepth($node->left);
        $rightDepth = $this->getTreeDepth($node->right);
        return max($leftDepth, $rightDepth) + 1;
    }

}