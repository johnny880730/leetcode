<?php
/*
剑指 Offer 46. 把数字翻译成字符串
给定一个数字，我们按照如下规则把它翻译为字符串：
0 翻译成 “a” ，1 翻译成 “b”，……，11 翻译成 “l”，……，25 翻译成 “z”。
一个数字可能有多个翻译。
请编程实现一个函数，用来计算一个数字有多少种不同的翻译方法。



示例 1:
输入: 12258
输出: 5
解释: 12258有5种不同的翻译，分别是"bccfi", "bwfi", "bczi", "mcfi"和"mzi"


提示：
0 <= num < 231

难度：中等

https://leetcode.cn/problems/ba-shu-zi-fan-yi-cheng-zi-fu-chuan-lcof/


*/

$arr1 = '12258';
$arr1 = '25';
$obj = new Code_Offer46();
$res = $obj->main($arr1);
var_dump($res);

class Code_Offer46
{
    public function main($num)
    {
        // 如果最后两位数字在0~25之间，那么f(i) = f(i-2)+f(i-1)
        // 如果最后两位数字不在0~25之间，那么f(i) = f(i-1)
        // dp[i]表示前i位可以解码的总数
        $num = strval($num);
        $dp = [];
        $dp[0] = 1;
        $len = strlen($num);
        for ($i = 1; $i < $len; $i++) {
            // 这个if判断倒数两位在10~25之间
            if ($num[$i-1] == 1 || ($num[$i-1] == 2 && $num[$i] <= 5)) {
                if ($i == 1) {
                    // i==1的时候要特别判断下
                    $dp[$i] = 2;
                } else {
                    $dp[$i] = $dp[$i-1] + $dp[$i-2];
                }
            } else {
                $dp[$i] = $dp[$i-1];
            }
        }
        return $dp[$len - 1];
    }
}