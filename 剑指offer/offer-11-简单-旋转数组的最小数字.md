## 剑指 Offer 11. 旋转数组的最小数字

# 题目
把一个数组最开始的若干个元素搬到数组的末尾，我们称之为数组的旋转。输入一个递增排序的数组的一个旋转，输出旋转数组的最小元素。

【示例】  
数组 [3,4,5,1,2] 为 [1,2,3,4,5] 的一个旋转，该数组的最小值为1。

# 解析
&emsp;&emsp;可以注意到旋转之后的数组实际上可以划分为两个排序的子数组，还逐一到最小的元素刚好是这两个子数组的分界线。
排序的数组中我们可以用二分来实现查找。因此可以试着用二分法的思路来寻找这个问题的答案

&emsp;&emsp;和二分法一样，用两个指针 p1、p2 分别指向第一个元素和最后一个元素。接着找数组中间的元素 mid，如果 mid 指向的数字位于前面的递增子数组中，
那么它应该大于或等于 p1 指向的的元素，此时最小元素应该在 mid 的后面。我们可以把 p1 指向 mid，这样可以缩小寻找的范围。

&emsp;&emsp;同样，如果 mid 元素位于后面的递增子数组，那么它影噶i小于或者等于 p2 指向的元素。此时该数组最小的元素应该位于 mid 的前面。
我们可以把 p2 指向 mid，这样也可以缩小寻找范围。

&emsp;&emsp;不管是移动 p1 还是 p2，查找范围都缩小了一半，接下来再用更新之后的两个指针重复做新一轮的查找。



# 代码
```php
$arr = [3,4,5,1,2];
$obj = new Code_Offer11();
var_dump($obj->main($arr));

class Code_Offer11
{
    public function main($arr)
    {
        $left = 0;
        $right = count($arr) - 1;
        while ($left < $right) {
            $mid = intval(($left + $right) / 2);
            if ($arr[$mid] > $arr[$right]) {
                // 中点大于arr[right]，旋转点在mid后方
                $left = $mid + 1;
            } else if ($arr[$mid] < $arr[$right]) {
                // 中点小于arr[right]，也由于原数组是递增的，旋转点在[left, mid]区间
                $right = $mid;
            } else {
                return $this->findMin($arr, $left, $right);
            }
        }

        return $arr[$left];
    }

    protected function findMin($arr, $left, $right)
    {
        $res = $arr[$left];
        for ($i = $left; $i <= $right; $i++) {
            $res = min($res, $arr[$i]);
        }
        return $res;
    }
}
```