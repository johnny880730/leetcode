# KMP 算法

# 题目
str = abc1234def，match = 1234。如果 str 的某个字串为 match，返回字串的开始位置，没有则返回 -1。

# 解析

## next 数组的定义
首先理解一个概念：一个字符串中，每个位置有个指标：它之前字符串的最长前缀和最长后缀的最大匹配长度。

如 abcabck，来看最后位置的 k，它之前的字符串是 abcabc。
- 前缀长度为 1 时字符串为 a，后缀长度为 1 时字符串为 c，他们不相等
- 前缀长度为 2 时字符串为 ab，后缀长度为 2 时字符串为 bc，他们不相等
- 前缀长度为 3 时字符串为 abc，后缀长度为 3 时字符串为 abc，他们相等，记下这个 3。
- 前缀长度为 4 时字符串为 abca，后缀长度为 4 时字符串为 cabc，他们不相等
- 前缀长度为 5 时字符串为 abcab，后缀长度为 5 时字符串为 bcabc，他们不相等

因此可以认为最后位置 k 的指标就是 3。

注意：
- 不要让前缀和后缀取到整体
- 0 位置，因为前面没字符串，所以人为规定 0 位置的指标为 -1。
- 1 位置之前的字符串只有 1 个字符，根据不要让前缀和后缀取到整体的原则，1 位置的指标人为规定为 0

根据上述内容对 match 进行指标计算，计算 match 每个位置的指标，结果应该是一个数组，命名为 next。

## kmp 算法过程
如 match = "aabaabcaabaabcs"，计算指标的结果数组 next = [-1, 0, 1, 0, 1, 2, 3, ……]。
接下来利用这个 next 数组来加速 KMP 算法的匹配速度。

假设当前情况为 str 从 i 位置开始与 match 的 0 位置开始比对，比对进行到 str 的 x 位置和 match 的 y 位置不相等，即 str[x] != match[y]。
如果是暴力解法的话，str 要回退到 i + 1 位置而 match 回退到 0 位置重新开始比对。

到 KMP 算法里，如果 str[x] != match[y]，在 y 位置有个事先计算好的最长前缀后缀匹配长度 next[y]，这里假设 next[y] = 5，
根据 next 定义可以知道，这个 5 就是 match[y] 前面的字符串的前缀和后缀匹配的最长长度为 5，
此时 y 回退到 前缀长度 5 的下一个位置，x 位置不动，继续查看此时的 match[y] 是否等于 str[x]。可见下图：

![](./images/kmp-img1.png)

另外会有个问题：为何要从 i 位置跳到 j 位置重新开始比对呢？中间是否有个 k 位置能配出整个 match 呢？

假设从 k 出发能配出整个 match ，那么可以得到从 k 到 x - 1 位置这一段为 len，和 match 从 0 位置出发长度为 len 的一段是相等的。这里就有个矛盾：
str 里从 k 出发到 x - 1 （长度为 len）这段对应的 match 部分就是同样的 k 位置到 y - 1 的这一段，暂称这一段为 子串 A。而根据假设内容：子串 A 与 
match 从 0 位置出发长度为 len 的一段也是相等的，那么实际就是 match 里的 y 位置的前缀和后缀匹配长度，也就是 next[y]。
只要事先计算 next 数组没有出错，这个 len 和 next[y] 是矛盾的，所以假设内容错误，所以 i 和 j 中间没有位置能配出 match。

![](./images/kmp-img2.png)

## 如何生成 next 数组

根据 next 数组的定义部分可知，next[0] = -1，next[1] = 0，而 next[2] = match[0] == match[1] ? 1 : 0。由此 0、1、2 位置已经得到。
接下来看 3 位置怎么得到，3 位置得到就看 4 位置，以此类推。由此可得，推算 i 位置时可知 0~ i - 1 位置已经推算完毕了。
现在就是如何根据 0 ~ i - 1 的信息来快速推出 next[i] 位置。

假设 match[i - 1] = "a"，next[i - 1] = 7，根据 next 定义可以知道，match[0..6] 的字符和 match[i - 8...i - 2] 的字符是相等的。
而如果 match[0..6] 的下一个位置 match[7] 的字符等于 match[i - 1] 的字符，那么 next[i] 的值起码是 7 + 1 = 8。那么 next[i] 可能比 8 大吗？

假设 next[i] = 9，可知 match[0..8] == match[i - 9...i - 1]，而 match[0..8] 取前 8 个也就是 match[0..7]，
match[i - 9..i - 1] 也取前 8 个也就是 match[i - 9...i - 2] 也是相等的，即 match[0..8] == match[i - 9..i - 2]，
这个等式说明 next[i - 1] = 8，与给出的 next[i - 1] = 7 是矛盾的，所以不可能比 8 大。

如果 match[0..6] 的下一个位置 match[7] 的字符不等于 match[i - 1] 的字符，根据 next[7] 再将 match[0..6] 分成前缀和后缀两段。
如 match = "abasaba t abasaba kz"（方便展示用空格分隔相同的子串）:
- 可知倒数第二位（也就是字符 k 的位置）的 next[i - 1] = 7，前后缀也就是 "abasaba"。而 match[7] != match[i - 1]，从 match[7] 开始再往前分两段
- 肉眼可知 next[7] = 3，前后缀也就是 "aba"，看 match[3] 是否等于 match[i - 1]，相等的话 next[i - 1] = next[7] + 1 = 4，
  不等的话再往前跳到 "aba" 的 b 的位置
- 当前的 b 是第二个字符，因此 next[1] = 0，而 match[0] != match[i - 1]，所以 next[i] = 0

总结的话就是不断往前跳分两段，如果中间某一步跳出来的，next[i] 就是跳出来的地方的信息再加一，没有就是 0

# 代码

### php
```php
class KMP {

    function getIndexOf($str, $match) {
        $len1 = strlen($str);
        $len2 = strlen($match);
        $x = 0;     // str 中当前比对的位置
        $y = 0;     // match 中当前比对的位置
        $next = $this->getNextArray($match);
        // O(N)
        while ($x < $len1 && $y < $len2) {
            if ($str[$x] == $match[$y]) {
                $x++;
                $y++;
            } else if ($next[$y] == -1) {   // y == 0
                // y已经指向 next开头了，str只能指向下一个位置了
                $x++;
            } else {
                // y 还能往前跳
                $y = $next[$y];
            }
        }
        // 1、x越界但y没越界，说明遍历所有x都没匹配到，返回 -1
        // 2、x没越界但y越界，说明匹配到了。x位置减去长度就是开始的位置
        // 3、x y 都越界了，说明 y 和 x 等量长的后缀配出来了，也是 x 位置减去长度就是开始的位置
        return $y == $len2 ? $x - $y : -1;
    }

    // 生成 next 数组， O(M)
    protected function getNextArray ($match) {
        if (strlen($match) == 1) {
            return [-1];
        }
        $len = strlen($match);
        $next = array_fill(0, $len, 0);
        $next[0] = -1;
        $next[1] = 0;
        // cn代表 next[i - 1]=cn，也可以根据cn获取 match[cn] 的字符去和 match[i - 1] 的字符比较，相等的话 next[i]=cn+1
        $cn = 0;
        $i = 2;
        while ($i < $len) {
            if ($match[$i - 1] == $match[$cn]) {
                /*
                 * 跳出来的时候的逻辑
                 * next[i]=cn+1; i++; cn++;
                 * 三句合一
                 */
                $next[$i++] = ++$cn;
            } else if ($cn > 0) {
                // cn 还能往前跳
                $cn = $next[$cn];
            } else {
                $next[$i++] = 0;
            }
        }
        return $next;
    }
}
```