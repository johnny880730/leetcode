# bfprt 算法

# 题目
在 O(N) 中求一个无序数组 nums 中第 k （k 从 1 开始算）小（或者大）的数字。

# 示例
```
[0,3,1,8,5,2]，k = 2
这个数组，第 2 大的数是 5
```

# 解析

## 改进的快排
快速排序中，有一个 partition 的过程, 这个过程随机选择数组中的一个数，假设叫 pivot，这个过程主要作用是将数组的 l...r 区间内的数：
- 小于 pivot 的数放左边（求第 k 大的话就是小于 Pivot 的放右边，这里按第 k 小来处理，下同）
- 大于 pivot 的数放右边（求第 k 大的话就是小于 Pivot 的放左边）
- 等于 pivot 的数放中间

返回两个值，一个是左边界和一个右边界，位于左边界和右边界的值均等于 pivot，小于左边界的位置的值都小于 pivot，大于右边界的位置的值均大于 pivot。
简言之：如果要排序，pivot 这个值在一次 partition 以后，所在的位置就是最终排序后 pivot 应该在的位置。

所以，如果数组中某个数在经历上述 partition 之后正好位于 K - 1 位置，那么这个数就是整个数组第 K 小的数。

## bfprt
bfprt 算法和改进快排相比较的话，就是取 pivot 的时候有所不同。数组 nums 长度为 N。方法定义为：在 nums 数组中找到第 k 小的数字，
也就是 int bfprt(nums, k)

第一步：逻辑上把 0 ~ 4 这五个数划为一组、5 ~ 9 这五个数划为一组、10 ~ 14 这五个数划为一组 …… 这样五个五个一组，最后不够五个的话就单独一组。
这样一共有 N / 5 个组。

第二步：0 ~ 4 这五个数的内部排个序、5 ~ 9 这五个数的内部排个序、10 ~ 14 这五个数的内部排个序 …… 但组与组之间是无序的。

第三步：每一组拿出中间位置的数字，分别记为 m0、m1、m2 …… 最后一组不是五个的话，如果奇数个数字，就拿中位数；若是偶数个数字，
就拿上中位数（不要拿这两数相加除二的结果）。将这些中位数组成一个数组，叫做数组 m

第四步：通过递归调用 bfprt 方法，找到这个中位数组成的数组 m 的中位数。也就是 bfprt(m, N / 10)，这个函数的结果就是选择的 pivot。

举例说明，nums = [3, 6, 2, 1, 4, 5, 0, 2, 1, 9, 3, 4]。

开始分组，分为 [3, 6, 2, 1, 4]、[5, 0, 2, 1, 9]、[3, 4]。每个小组排序的结果是 [1, 2, 3, 4, 6]、[0, 1, 2, 5, 9]、[3, 4]。
取每个组的中位数组成新数组 m = [3, 2, 3]。m 数组的中位数是 3，因此 pivot = 3。




# 代码
```php
class bfprt {

    // 改进的快排
    function findKthSmallest1($nums, $k) {
        return $this->_process1($nums, 0, count($nums) - 1, $k - 1);
    }

    // 求 nums 第 k 小的数
    // nums[L..R] 范围上，如果排序的话（不是真的去排序），找位于 index 的数，L <= index <= R
    protected function _process1($nums, $L, $R, $index){
        if ($L == $R) {
            return $nums[$L];
        }
        $pivot = $nums[rand($L, $R)];  // 从 nums[L..R] 随机选一个值作为pivot
        $range = $this->_partition($nums, $L, $R, $pivot);
        if ($index >= $range[0] && $index <= $range[1]) {
            return $nums[$index];
        } else if ($index < $range[0]) {
            return $this->_process1($nums, $L, $range[0] - 1, $index);
        } else {
            return $this->_process1($nums, $range[1] + 1, $R, $index);
        }
    }

    protected function _partition(&$nums, $L, $R, $pivot){
        $less = $L - 1;
        $more = $R + 1;
        $cur = $L;
        while ($cur < $more) {
            if ($nums[$cur] < $pivot) {
                $this->_swap($nums, ++$less, $cur++);
            } else if ($nums[$cur] > $pivot) {
                $this->_swap($nums, $cur, --$more);
            } else {
                $cur++;
            }
        }
        return array($less + 1, $more - 1);
    }

    public function _swap(&$nums, $i1, $i2) {
        $tmp = $nums[$i1];
        $nums[$i1] = $nums[$i2];
        $nums[$i2] = $tmp;
    }
	
	// bfprt
    function findKthSmallest2($nums, $k) {
        return $this->_bfprt($nums, 0, count($nums) - 1, $k - 1);
    }

    // $nums[L..R]  如果排序的话，位于index位置的数，是什么，返回
    protected function _bfprt($nums, $L, $R, $index){
        if ($L == $R) {
            return $nums[$L];
        }
        $pivot = $this->_medianOfMedians($nums, $L, $R);
        $range = $this->_partition($nums, $L, $R, $pivot);
        if ($index >= $range[0] && $index <= $range[1]) {
            return $nums[$index];
        } else if ($index < $range[0]) {
            return $this->_bfprt($nums, $L, $range[0] - 1, $index);
        } else {
            return $this->_bfprt($nums, $range[1] + 1, $R, $index);
        }
    }

    // arr[L...R]  五个数一组
    // 每个小组内部排序
    // 每个小组中位数领出来，组成marr
    // marr中的中位数，返回
    protected function _medianOfMedians ($nums, $L, $R) {
        $size = $R - $L + 1;
        $mArr = array_fill(0, ceil($size / 5), 0);
        $len = count($mArr);
        for ($team = 0; $team < $len; $team++) {
            $teamFirst = $L + $team * 5;
            // L ... L + 4
            // L +5 ... L +9
            // L +10....L+14
            $mArr[$team] = $this->_getMedian($nums, $teamFirst, min($R, $teamFirst + 4));
        }
        // marr中，找到中位数
        // marr(0, marr.len - 1,  mArr.length / 2 )
        return $this->_bfprt($nums, 0, $len - 1, $len >> 1);
    }

    protected function _getMedian (&$nums, $L, $R) {
        $this->_insertionSort($nums, $L, $R);
        return $nums[floor(($L + $R) / 2)];
    }

    protected function _insertionSort (&$nums, $L, $R) {
        for ($i = $L + 1; $i <= $R; $i++) {
            for ($j = $i - 1; $j >= $L && $nums[$j] > $nums[$j + 1]; $j--) {
                $this->_swap($nums, $j, $j + 1);
            }
        }
    }

}

```
