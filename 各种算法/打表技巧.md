# 打表技巧

## 打表法：
1. 问题如果返回值不太多，可以用 hardcode 的方式列出，作为程序的一部分
2. 一个大问题解决时底层频繁使用规模不大的小问题的解，如果小问题的返回值满足条件 1，可以把小问题的解列成一张表，作为程序的一部分
3. 打表找规律

## 打表找规律
1. 某个题目，输入参数类型简单，并且只有一个实际参数
2. 要求的返回值类型也简单，并且只有一个
3. 用暴力方法，把输入参数对应的返回值，打印出来看看，进而优化 code

和【预处理结构】的区别：
- 大的问题分解成小的问题，而小的问题如果数据规模比较小，可以在事先去生成一个表，要用的时候直接去这个表里去取，而不用实时调函数去获取结果。

### 题目一
一个人去买苹果，商家只提供两种类型的塑料袋，每种可以认为有任意数量：能装下 6 个苹果的袋子 和 能装下 8 个苹果的袋子。
要求买苹果使用的袋子尽量少，且每个袋子都必须装满。给定一个正整数 N，返回至少使用的袋子。如果无法让使用的每个袋子都装满，返回 -1。

**解析**

如果 N 是奇数的话，明显无法满足，返回 -1。先用暴力方法看 1 < N <= 100 且 N 为偶数的输出结果：

```php
// 暴力解法
function minBags($n) {
    if ($n < 0) {
        return -1;
    }
    $bag6 = -1;
    $bag8 = floor($n / 8);
    $rest = $n - 8 * $bag8;
    while ($bag8 > 0 && $rest < 24) {
        $restUse6 = minBagBase6($rest);
        if ($restUse6 != -1) {
            $bag6 = $restUse6;
            break;
        }
        $rest = $n - 8 * (--$bag8);
    }
    return $bag6 == -1 ? -1 : $bag6 + $bag8;
}
// 剩余如果都能被装进6个苹果的袋子，返回袋子数量；不能就返回 -1
function minBagBase6($rest) {
    return $rest % 6 == 0 ? ($rest / 6) : -1;
}
```

| 苹果 | 袋子 |
| ---- | --- |
|  2   |  -1 |
|  4  |  -1  |
| 6   | 1  |
| 8   | 1  |
| 10 | -1 |
| 12 | 2 |
| 14 | 2 |
| 16 | 2 |
| 18 | 3 |
| 20 | 3 |
| 22 | 3 |
| 24 | 3 |
| 26 | 4 |
| 28 | 4 |
| 30 | 4 |
| 32 | 4 |
| 34 | 5 |
| 36 | 5 |
| 38 | 5 |
| 40 | 5 |
| 42 | 6 |
| 44 | 6 |
| 46 | 6 |
| 48 | 6 |
| 50 | 7 |
| 52 | 7 |
| 54 | 7 |
| 56 | 7 |
| 58 | 8 |
| 60 | 8 |
| 62 | 8 |
| 64 | 8 |
| 66 | 9 |
| 68 | 9 |
| 70 | 9 |
| 72 | 9 |
| 74 | 10 |
| 76 | 10 |
| 78 | 10 |
| 80 | 10 |
| 82 | 11 |
| 84 | 11 |
| 86 | 11 |
| 88 | 11 |
| 90 | 12 |
| 92 | 12 |
| 94 | 12 |
| 96 | 12 |
| 98 | 13 |
| 100 | 13 |

发现当 N < 18 时候没什么规律。但是当 N >= 18 时，每四个偶数作为一组的返回值是相同的，之后的四个偶数的返回值是前四个偶数的返回值 +1。
那么可以总结通项公式直接用这个公式去计算即可。

```php
function getMinBagsAwesome($n) {
    if (($n & 1) != 0) {
        return -1;
    }
    if ($n < 18) {
        return $n == 0 ? 0 : in_array($n, [6, 8]) ? 1 
                : in_array($n, [12, 14, 16]) ? 2 : -1;
    }
    return intval(($n - 18) / 3) + 3;
}
```

### 题目二
给定一个正整数 N，表示有 N 份青草统一对方在仓库里。有一只牛和一只羊，牛先吃，羊后吃，轮流吃草。不管是牛还是羊，每一轮能吃的草量必须是：
1、4、16、64……（4 的某次方），谁先把草吃完谁获胜（也就是谁先面对无草可吃的情况谁就输了）。

假设牛和羊都想赢，都能作出理性的决定。根据参数 N，返回谁会赢。

**解析**

- 如果 N = 0，是后手赢。因为先手先面对的无草可吃的情况。
- 如果 N = 1，是先手赢。先手吃 1 份即可。
- 如果 N = 2，是后手赢。先手只能吃 1 份，后手再吃 1 份，先手没的吃了
- 如果 N = 3，是先手赢。先手只能吃 1 份，后手再吃 1 份，先手再吃一份，没了
- 如果 N = 4，是先手赢。先手直接吃 4 份，没了
- 如果 N = 5，是后手赢。先手可以吃 1 份或者 4 份。无论先手选择哪种，后手都能吃完剩下的。

就这么来看暴力解。

```php
// 暴力解
function winner1($n) {
    // 0   1   2   3   4
    // 后  先  后   先  先
    if ($n < 5) {
        return ($n == 0 || $n == 2) ? '后手' : '先手';
    } 
    $base = 1;      //当前先手决定吃的数量
    // 当前是先手在选
    while ($base <= $n) {
        // 当前一共 n 份草，先手吃掉的是 base 份，n - base 是留给后手
        if (winner1($n - $base) == '后手') {
            // 母过程的先手是子过程的后手
            return '先手';
        }
        if ($base > intval($n / 4)) {      // 防止下面的 base * 4 溢出
            break;
        }
        // 根据题意，先手尝试吃的草增加到 base * 4 
        $base *= 4;
    }
    return '后手';
}
```

暴力解的结果如下表：

|  N  |  res |
| --- |  --- |
|  0  |  后手 |
|  1  |  先手 |
|  2  |  后手 |
|  3  |  先手 |
|  4  |  先手 |
|  5  |  后手 |
|  6  |  先手 |
|  7  |  后手 |
|  8  |  先手 |
|  9  |  先手 |
|  10  |  后手 |
|  11  |  先手 |
|  12  |  后手 |
|  13  |  先手 |
|  14  |  先手 |
|  15  |  后手 |
|  16  |  先手 |
|  17  |  后手 |
|  18  |  先手 |
|  19  |  先手 |
|  20  |  后手 |
|  21  |  先手 |
|  22  |  后手 |
|  23  |  先手 |
|  24  |  先手 |
|  25  |  后手 |
|  26  |  先手 |
|  27  |  后手 |
|  28  |  先手 |
|  29  |  先手 |
|  30  |  后手 |
|  31  |  先手 |
|  32  |  后手 |
|  33  |  先手 |
|  34  |  先手 |
|  35  |  后手 |
|  36  |  先手 |
|  37  |  后手 |
|  38  |  先手 |
|  39  |  先手 |
|  40  |  后手 |
|  41  |  先手 |
|  42  |  后手 |
|  43  |  先手 |
|  44  |  先手 |
|  45  |  后手 |
|  46  |  先手 |
|  47  |  后手 |
|  48  |  先手 |
|  49  |  先手 |
|  50  |  后手 |

从结果可以看到，0 ~ 4 的五个数字的结果是“后先后先先”，5 ~ 9 的五个结果也是“后先后先先”，之后都是每五个数字（10 ~ 14、15 ~ 19 等等）
的结果是“后先后先先”，因此修改后的代码就很容易得到：

```php
// 优化版
function winner2($n) {
    return ($n % 5 == 0 || $n % 5 == 2) ? '后手' : '先手';
}
```

### 题目三
定义一种数：可以表示成若干（数量 > 1）连续正数和的数字。比如：
- 5 = 2 + 3
- 12 = 3 + 4 + 5

1 不是这样的树，因为要求数量必须大于一个；2 = 1 + 1，但也不是，因为等号右边不是连续正数。

给定一个参数 N，返回是不是可以表示成若干连续正数和的数

**解析**

开始尝试暴力解。比如 N = 100，从 1 开始加，1 + 2 + 3 + …… + ?，如果加到某个数正好等于 100，那就是 true。
如果加后的结果超过 100，那就从 2 开始重新开始加，然后就从 3 开头往下尝试、从 4 开头往下尝试。如果什么都试不出来，那就是 false。

```php
// 暴力解法
function isMSum1($num) {
    for ($i = 1; $i <= $num; $i++) {    // i：开头的数字
        $sum = $i;
        for ($j = $i + 1; $j <= $num; $j++) {
            if ($sum + $j > $num) {
                // 加的超过了 num，就换个开头的数字 i
                break;
            }
            if ($sum + $j == $num) {
                return true;
            }
            $sum += $j;
        }
    }
    return false;
}
```

暴力解法的结果如下：

|  num  |  res |
| --- |  --- |
|  1  |  false |
|  2  |  false |
|  3  |  true |
|  4  |  false |
|  5  |  true |
|  6  |  true |
|  7  |  true |
|  8  |  false |
|  9  |  true |
|  10  |  true |
|  11  |  true |
|  12  |  true |
|  13  |  true |
|  14  |  true |
|  15  |  true |
|  16  |  false |
|  17  |  true |
|  18  |  true |
|  19  |  true |
|  20  |  true |
|  21  |  true |
|  22  |  true |
|  23  |  true |
|  24  |  true |
|  25  |  true |
|  26  |  true |
|  27  |  true |
|  28  |  true |
|  29  |  true |
|  30  |  true |
|  31  |  true |
|  32  |  false |
|  33  |  true |
|  34  |  true |
|  35  |  true |
|  36  |  true |
|  37  |  true |
|  38  |  true |
|  39  |  true |
|  40  |  true |
|  41  |  true |
|  42  |  true |
|  43  |  true |
|  44  |  true |
|  45  |  true |
|  46  |  true |
|  47  |  true |
|  48  |  true |
|  49  |  true |
|  50  |  true |
|  51  |  true |
|  52  |  true |
|  53  |  true |
|  54  |  true |
|  55  |  true |
|  56  |  true |
|  57  |  true |
|  58  |  true |
|  59  |  true |
|  60  |  true |
|  61  |  true |
|  62  |  true |
|  63  |  true |
|  64  |  false |
|  65  |  true |
|  66  |  true |
|  67  |  true |
|  68  |  true |
|  69  |  true |
|  70  |  true |
|  71  |  true |
|  72  |  true |
|  73  |  true |
|  74  |  true |
|  75  |  true |
|  76  |  true |
|  77  |  true |
|  78  |  true |
|  79  |  true |
|  80  |  true |
|  81  |  true |
|  82  |  true |
|  83  |  true |
|  84  |  true |
|  85  |  true |
|  86  |  true |
|  87  |  true |
|  88  |  true |
|  89  |  true |
|  90  |  true |
|  91  |  true |
|  92  |  true |
|  93  |  true |
|  94  |  true |
|  95  |  true |
|  96  |  true |
|  97  |  true |
|  98  |  true |
|  99  |  true |
|  100  |  true |
|  101  |  true |
|  102  |  true |
|  103  |  true |
|  104  |  true |
|  105  |  true |
|  106  |  true |
|  107  |  true |
|  108  |  true |
|  109  |  true |
|  110  |  true |
|  111  |  true |
|  112  |  true |
|  113  |  true |
|  114  |  true |
|  115  |  true |
|  116  |  true |
|  117  |  true |
|  118  |  true |
|  119  |  true |
|  120  |  true |
|  121  |  true |
|  122  |  true |
|  123  |  true |
|  124  |  true |
|  125  |  true |
|  126  |  true |
|  127  |  true |
|  128  |  false |
|  129  |  true |
|  130  |  true |
|  131  |  true |
|  132  |  true |
|  133  |  true |
|  134  |  true |
|  135  |  true |
|  136  |  true |
|  137  |  true |
|  138  |  true |
|  139  |  true |
|  140  |  true |
|  141  |  true |
|  142  |  true |
|  143  |  true |
|  144  |  true |
|  145  |  true |
|  146  |  true |
|  147  |  true |
|  148  |  true |
|  149  |  true |
|  150  |  true |
|  151  |  true |
|  152  |  true |
|  153  |  true |
|  154  |  true |
|  155  |  true |
|  156  |  true |
|  157  |  true |
|  158  |  true |
|  159  |  true |
|  160  |  true |
|  161  |  true |
|  162  |  true |
|  163  |  true |
|  164  |  true |
|  165  |  true |
|  166  |  true |
|  167  |  true |
|  168  |  true |
|  169  |  true |
|  170  |  true |
|  171  |  true |
|  172  |  true |
|  173  |  true |
|  174  |  true |
|  175  |  true |
|  176  |  true |
|  177  |  true |
|  178  |  true |
|  179  |  true |
|  180  |  true |
|  181  |  true |
|  182  |  true |
|  183  |  true |
|  184  |  true |
|  185  |  true |
|  186  |  true |
|  187  |  true |
|  188  |  true |
|  189  |  true |
|  190  |  true |
|  191  |  true |
|  192  |  true |
|  193  |  true |
|  194  |  true |
|  195  |  true |
|  196  |  true |
|  197  |  true |
|  198  |  true |
|  199  |  true |
|  200  |  true |

通过观察可以发现，当 num == 1 或者 2 的时候是 false。此外，num == 4、8、16、32、64、128 的时候是 false。
这样就找到了规律：num == 1 或者 num == 2 或者 num 是 2 的某次幂的时候是 false，其他都是 true。

```php
function isMSum2($num) {
    if ($num < 3) {
        return false;
    }
    // 判断 num 是不是 2 的某次幂
    // 如果 (num & (num - 1)) == 0， 那么 num 就是 2 的某次方
    return ($num & ($num - 1)) != 0;
}
```







