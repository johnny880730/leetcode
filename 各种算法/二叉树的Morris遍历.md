# 题目
二叉树的 Morris 遍历

实现遍历二叉树的时间复杂度 O(n)，空间复杂度 O(1)

# 解析

morris 遍历的过程：
1. 当前节点 cur，一开始 cur 指向整棵树的头：
2. 若 cur 无左树，直接向右移动：cur = cur.right
3. 若 cur 有左树，找到左树最右的节点，记位 mostRight
   1. 若 mostRight 的右指针指向 null，让 mostRight 的右指针指向 cur，然后 cur 向左移动，即 cur = cur.left
   2. 若 mostRight 的右指针指向 cur，让 mostRight 的右指针指向 null，然后 cur 向右移动，即 cur = cur.right
4. cur 指向 null 的时候遍历停止

例如以下的二叉树，其 morris 遍历序列为：1、2、4、2、5、1、3、6、3、7
```
              1
           /     \
          2       3
         / \     / \    
       4    5   6   7 

```

通过例子可知一个规律：如果某节点的左子树不为空，它肯定会遍历两次。

前序遍历：第一次来到这个节点就打印。

中序遍历：第二次来到这个节点才打印（只会遍历一次的节点就第一次遇到就打印）。

后序遍历：针对第二次来到的节点，逆序打印它的左子树的右边界。所有节点遍历完后，再逆序打印整棵树的有边界。结果就是后序



# 代码
```php
class MorrisTraversal {

     // morris 序列
     public function morris($head) {
        if ($head == null) {
            return;
        }
        $cur = $head;
        while ($cur != null) {
            // 判断有没有左树
            $mostRight = $cur->left;
            if ($mostRight != null) {
                // 有左树，找左树上真实的最右的节点（指向null或者指向cur停止）
                while ($mostRight->right != null && $mostRight->right != $cur) {
                    $mostRight = $mostRight->right;
                }
                
                if ($mostRight->right == null) {
                    // mostRight的右指针指向 null 的情况
                    $mostRight->right = $cur;
                    $cur = $cur->left;
                    continue;
                } else {
                    // mostRight的右指针指向 cur
                    $mostRight->right = null;
                }
            }
            $cur = $cur->right;
        }
     }
     
     // morris 前序遍历
     public function morrisPre($head) {
        if ($head == null) {
            return;
        }
        $cur = $head;
        while ($cur != null) {
            $mostRight = $cur->left;
            if ($mostRight != null) {
                while ($mostRight->right != null && $mostRight->right != $cur) {
                    $mostRight = $mostRight->right;
                }
                if ($mostRight->right == null) {
                    $mostRight->right = $cur;
                    // 有左树的话 第一次到达的时候打印
                    echo $cur->val . PHP_EOL;
                    $cur = $cur->left;
                    continue;
                } else {
                    $mostRight->right = null;
                }
            } else {
                // 没有左树 打印
                echo $cur->val . PHP_EOL;
            }
            $cur = $cur->right;
        }
     }
     
     // morris 中序遍历
     public function morrisIn($head) {
        if ($head == null) {
            return;
        }
        $cur = $head;
        while ($cur != null) {
            $mostRight = $cur->left;
            if ($mostRight != null) {
                while ($mostRight->right != null && $mostRight->right != $cur) {
                    $mostRight = $mostRight->right;
                }
                if ($mostRight->right == null) {
                    $mostRight->right = $cur;
                    $cur = $cur->left;
                    continue;
                } else {
                    $mostRight->right = null;
                }
            }
            // 打印timing
            echo $cur->val . PHP_EOL;
            $cur = $cur->right;
        }
     }
     
     // morris 后序遍历
     public function morrisPost($head) {
        if ($head == null) {
            return;
        }
        $cur = $head;
        while ($cur != null) {
            $mostRight = $cur->left;
            if ($mostRight != null) {
                while ($mostRight->right != null && $mostRight->right != $cur) {
                    $mostRight = $mostRight->right;
                }
                if ($mostRight->right == null) {
                    $mostRight->right = $cur;
                    $cur = $cur->left;
                    continue;
                } else {
                    $mostRight->right = null;
                    // 逆序打印左子树的右边界
                    $this->printEdge($cur->left);
                }
            }
            $cur = $cur->right;
        }
        // 逆序打印整棵树的右边界
        $this->printEdge($head);
     }
     
     // 逆序打印
     protected function printEdge($head){
         $tail = $this->reverseEdge($head);
         $cur = $tail;
         while ($cur) {
            echo $cur->val . PHP_EOL;
            $cur = $cur->right;
         }
         $this->reverseEdge($tail);
     }
     
     // 翻转
     protected function reverseEdge($from){
         $pre = null;
         while ($from) {
            $next = $from->right;
            $from->right = $pre;
            $pre = $from;
            $from = $next;
         }
         return $pre;
     }
}

```
