# LRU 算法

## LRU 算法描述
力扣第 146 题 “LRU 缓存”就是设计这种数据结构。

首先要接收一个 capacity 参数作为缓存的最大容量，然后实现两个 API ，一个是 put(key , val) 存入键值对，另一个是 get(key) 获取 key 
对应的 val，如果 key 不存在返回 -1。

注意：get 和 put 方法必须都是 O(1) 的时间复杂度，下面举个例子来看看 LRU 算法是如何工作的
```
/* 缓存容量为 2 */
LRUCache cache = new LRUCache(2);
// 可以把 cache 理解成一个队列
// 假设左边是队头，右边是队尾
// 最近使用的排在队头，久未使用的排在队尾
// 圆括号表示键值对 (key, val)

cache.put(1, 1);
// cache = [(1, 1)]

cache.put(2, 2);
// cache = [(2, 2), (1, 1)]

cache.get(1);   // 返回 1
// cache = [(1, 1), (2, 2)]
// 因为最近访问了 1，所以提前到队头
// 返回键 1 对应的值 1

cache.put(3, 3);
// cache = [(3, 3), (1, 1)]
// 缓存容量已满，需要删除内容空出位置
// 优先删除久未使用的数据，也就是队尾的数据
// 然后把新的数据插入队头

cache.get(2);       // 返回 -1，未找到

cache.put(1, 4);
// cache = [(1, 4), (3, 3)]
// 键 1 已存在，用 4 覆盖掉原始值 1
// 不要忘了把键值对提前到队头
```

## LRU 算法设计
根据上面的操作过程，要让 put 和 get 的时间复杂度为 O(1)，可以总结出 cache 这个结构的必要条件：
1. cache 中的元素要有时序，以区分最近使用的和久未使用的数据。容量满了后要删除久未使用的数据腾位置
2. 要在 cache 中快速找到某个 key 是否已存在并返回对应的 val
3. 每次访问 cache 中的某个 key，需要将这个元素变为最近使用的，也就是说要支持在任意位置快速插入和删除元素

那么，什么数据结构符合上述条件呢？哈希表查找快，但是无固定顺序；链表有顺序之分，插入、删除快，但是查找慢。所以结合一下，
形成一种新的树结构：哈希链表 LinkedHashMap

LRU 缓存算法的核心数据结构就是哈希链表，它是双向链表 + 哈希表的结合体

![](./images/LRU算法-哈希链表.png)

借助这个结构，来逐一分析上面的三个条件：
1. 如果每次默认从链表尾部添加元素，那么显然越靠尾部的元素越是最近使用的，越靠头部就是越久未使用的。
2. 对于某个 key，可以通过哈希表快速定位到链表中的节点，从而取得对应的 val
3. 链表显然是支持在任意位置快速插入和删除的，改下指针就行。只不过传统链表无法按照索引快速访问某个位置的元素，
   这里借助哈希表，可以通过 key 快速映射到任意一个链表节点，进行插入和删除

为什么要是双向链表？单链表行不行？另外，既然哈希表中存了 key，为何链表中还要存 key 和 val 呢？先往下看。

## 代码实现
很多编程语言都有内置的哈希链表或者类似 LRU 功能的库函数，这里先自己实现一遍 LRU 算法，再用 java 内置的 LinkedHashMap实现一遍。

首先，把双链表的节点类写出来。为了简化，key 和 val 都为 int 类型:
```java
class Node {
    public int key, val;
    public Node next, prev;
    public Node(int k, int v) {
        this.key = k;
        this.val = v;
    }
}
```

然后用 Node 类型构建一个双链表，实现 LRU 算法必需的 API：

```java
class DoubleList {
    // 头尾虚节点
    private Node head, tail;
    
    // 链表元素数量
    private int size;
    
    public DoubleList() {
        // 初始化双向链表的数据
        head = new Node(0, 0);
        tail = new Node(0, 0);
        head.next = tail
        tail.prev = head;
        size = 0;
    }
    
    // 在链表的尾部添加节点 x，时间复杂度为 O(1)
    public void addLast(Node x) {
        x.prev = tail.prev;
        x.next = tail;
        tail.prev.next = x;
        tail.prev = x;
        size++;
    }
    
    // 删除链表中的节点 x（x 一定存在）
    // 由于是双向链表且给的是目标 Node 节点，时间复杂度为 O(1)
    public void remove(Node x) {
        x.prev.next = x.next;
        x.next.prev = x.prev;
        size--;
    }
    
    // 删除链表中的第一个节点，并返回该节点。时间复杂度 O(1)
    public Node removeFirst() {
        if (head.next == tail) {
            return null;
        }
        Node first = head.next;
        remove(first);
        return first;
    }
    
    // 返回链表长度，时间复杂度 O(1)
    public int size() {
        return size;
    }
}
```

这里就能回答“为何必须是双向链表”的问题了：因为在删除操作中，删除一个节点不仅要得到该节点本身的指针，也需要操作其前驱节点的指针，
而双向链表才能支持直接查找前驱节点，保证操作的时间复杂度为 O(1)。

注意：这里实现的双向链表 API 只能从尾部插入，也就是越靠近尾部的数据是最近使用的，靠头部的是最久未使用的。

有了双向链表的实现，只需在 LRU 算法中把它和哈希表结合起来，先搭出代码框架

```java
class LRUCache {
    // key -> Node(key, val)
    private HashMap<Integer, Node> map;
    
    // Node(k1, v1) <--> Node(k2, v2)
    private DoubleList cache;
    
    // 最大容量
    private int cap;
    
    public LRUCache(int capacity) {
      this.cap = capacity;
      map = new HashMap<>();
      cache = new DoubleList();
    }
}
```

先不着急去实现 LRU 算法的 get 和 put 方法。由于要同时维护一个双链表 cache 和哈希表 map，很容易漏掉一些操作，比如删除某个 key 
时候，在 cache 中删除了对用的 Node，但是却忘记了在 map 中删除 key。

解决这种问题的有效方法是：在这两种数据结构之上提供一层抽象 API。

实际上很简单，就是尽量在 LRU 的主方法 get 和 put 避免直接操作 map 和 cache 的细节。可以先实现下面几个函数：

```java
// 将某个 key 提升为最近使用
private void makeRecently(int key) {
   Node x = map.get(key);
   cache.remove(x);
   cache.addLast(x);
}

// 添加最近使用的元素
private void addRecently(int key, int val) {
   Node x = new Node(key, val);
   cache.addLast(x);
   map.put(key, x);
}

// 删除某个 key
private void deleteKey(int key) {
   Node x = map.get(key);
   cache.remove(x);
   map.remove(key);
}

// 删除最久未使用的元素
private void removeLeastRecently() {
   // 链表头部的第一个就是最久未使用的
   Node deletedNode = cache.removeFirst();
   // 别忘了从 map 中删除它的 key
   int deletedKey = deletedNode.key;
   map.remove(deletedKey);
}
```

这里就能回答“为何要在链表中同时存储 key 和 val” 的问题。注意，在 removeLeastRecently() 中，需要用 deletedNode 得到 deletedKey。

也就是说，当缓存容量已满，不仅要删除最后一个 Node 节点，还要把 map 中映射到该节点的 key 同时删除，而这个 key 只能 Node 得到。
如果 Node 结构中只存储 val，那么就无法得知 key，也就无法删除 map 中的键，造成错误。

上述方法就是简单的操作封装，调用这些函数可以避免直接操作 cache 链表和 map 哈希表。下面先来实现 LRU 算法的 get 方法：

```java
public int get(int key) {
   if (!map.containsKey(key)) {
      return -1;
   }
   // 将该数据提升为最近使用的
   makeRecently(key);
   return map.get(key).val;
}
```

put 方法稍微复杂一些，先用个图搞清楚逻辑：

![](./images/LRU算法-put逻辑图.png)

这样就可以写出 put 方法的代码：

```java
public void put(int key, int val) {
   if (map.containsKey(key)) {
      // 删除旧的
      deleteKey(key);
      // 新插入的数据为最近使用
      addRecently(key, val);
      return;
   }
   
   if (cap == cache.size()) {
      // 删除最久未使用的数据
      removeLeastRecently();
   }
   // 添加为最近使用的数据
   addRecently(key, val);
}
```

至此，LRU 的原理和实现已经结束，最后用 java 内置类型的 LinkedHashMap 来实现，逻辑和之前完全一致。

```java
class LRUCache {
   int cap;
   LinkedHashMap<Integer, Integer> cache = new LinkedHashMap<>();
   
   public LRUCache(int capacity) {
      this.cap = capacity;
   }
   
   public int get(int key) {
      if (!cache.containsKey(key)) {
         return -1;
      }
      // 将 key 变为最近使用
      makeRecently(key);
      return cache.get(key);
   }
   
   public void put(int key, int val) {
      if (cache.containsKey(key)) {
         // 修改 key 的值
         cache.put(key, val);
         // 将 key 变为最近使用
         makeRecently(key);
         return;
      }
      
      if (cache.size() == this.cap) {
         // 链表头部就是最久未使用的 key
         int oldestKey = cache.keySet().iterator().next();
         cache.remove(oldestKey);
      }
      // 将新的 key 添加到尾部
      cache.put(key, val);
   }
   
   private void makeRecently(int key) {
      int val = cache.get(key);
      cache.remove(key);
      cache.put(key, val);
   }
}
```




