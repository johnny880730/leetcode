# Nim 博弈

# 题目
给定一个非负数组 nums，每一个值代表该位置上有几个铜板。甲和乙玩游戏，甲先手，乙后手。轮到某个人的时候，只能在一个位置上拿任意数量的铜板，
但不能不拿。谁最先把铜板拿完就算胜利。

假设两人都极度聪明，返回获胜者是谁。

# 解析

**结论：把数组中所有的数字异或起来，如果异或和是 0 的话先手输，不是 0 则先手赢。**

所谓输方，就是指最先面对数组元素全是 0 的人。如果数组元素全部是 0 的话，它的异或和 eor 也是 0。

如果先手能够做到以下的严格目标，先手必胜：
1. 面对数组的时候，数组的异或和都不是 0
2. 拿去铜板后，让后手面对的数组的异或和永远都是 0

举例，nums = [7, 4, 2]，7 的二进制是 111，4 是 100，2 是 010，这三个数的异或和是 001，是不等于 0 的。
先手如果从 7 这堆拿的话怎么拿？4 和 2 的异或和是 110，所以如果从 7 拿走之后也要变成 110（也就是从 111 变成 110），
那么整个的异或和就是 0。所以先手可以从 7 这里拿走 1 个。数组变成 [6, 4, 2]。先满足了先手的目标 2。

[6, 4, 2] 的异或和是 0，这时候后手无论怎么拿，变动后的数组的异或和一定不是 0。这样就满足了先手的目标 1。

但实际如果先手第一步的时候面对的是异或和为 0 的话，上文的情况就反过来了，先手就输了。