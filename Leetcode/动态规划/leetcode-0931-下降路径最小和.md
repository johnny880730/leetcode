# 931. 下降路径最小和

# 题目
给你一个 n x n 的 方形 整数数组 matrix ，请你找出并返回通过 matrix 的下降路径 的 最小和 。

下降路径 可以从第一行中的任何元素开始，并从每一行中选择一个元素。
在下一行选择的元素和当前行所选元素最多相隔一列（即位于正下方或者沿对角线向左或者向右的第一个元素）。
具体来说，位置 (row, col) 的下一个元素应当是 (row + 1, col - 1)、(row + 1, col) 或者 (row + 1, col + 1) 。

https://leetcode.cn/problems/minimum-falling-path-sum/

提示：
- n == matrix.length
- n == matrix[i].length
- 1 <= n <= 100
- -100 <= matrix[i][j] <= 100


# 示例
```
输入：matrix = [[2,1,3],[6,5,4],[7,8,9]]
输出：13
解释：下面是两条和最小的下降路径，
1、5、7 或者 1、4、8
```
```
输入：matrix = [[-19,57],[-40,-5]]
输出：-59
解释：下面是一条和最小的下降路径：-19、-40
```
```
输入：matrix = [[-48]]
输出：-48
```
   

# 解析

## 动态规划

先定义 dp 函数

```java
int dp(int[][] matrix, int i, int j)
```

含义：从第一行 matrix[0][..] 向下落，落到位置 matrix[i][j] 的最小路径和为 dp(matrix, i, j)

接下来看看 dp 函数如何实现。对于 matrix[i][j]，只有可能从 matrix[i - 1][j]、matrix[i - 1][j - 1] 和 matrix[i - 1][j + 1] 
这三个位置转移过来。那么只要知道 (i - 1, j)、(i - 1, j - 1) 和 (i - 1、j + 1) 这三个位置的最小路径和，再加上 matrixt[i][j] 
的值，就能够计算当前位置 (i, j) 的最小路径和。

当然上面的是暴力穷举法，可以用备忘录的方法来消除重叠子问题。



# 代码

### php
```php
class LeetCode0931 {

    public $memo;

    // dp
    function minFallingPathSum($matrix) {
        $n = count($matrix);
        $res = PHP_INT_MAX;
        // 备忘录里的值初始化为 66666
        $this->memo = array_fill(0, $n, array_fill(0, $n, 66666));
        // 终点可能在 matrix[n-1] 行里的任何一列
        for ($j = 0; $j < $n; $j++) {
            $res = min($res, $this->_dp($matrix, $n - 1, $j));
        }
        return $res;
    }

    function _dp($matrix, $i, $j) {
        // 索引检查
        if ($i < 0 || $j < 0 || $i >= count($matrix) || $j >= count($matrix[0])) {
            return 99999;
        }
        // base case
        if ($i == 0) {
            return $matrix[$i][$j];
        }
        // 查找备忘录
        if ($this->memo[$i][$j] != 66666) {
            return $this->memo[$i][$j];
        }
        // 状态转移
        $this->memo[$i][$j] = $matrix[$i][$j] + min(
            $this->_dp($matrix, $i - 1, $j),
            $this->_dp($matrix, $i - 1, $j - 1),
            $this->_dp($matrix, $i - 1, $j + 1)
        );
        return $this->memo[$i][$j];
    }

}
```

# 探讨
代码里对于 dp 函数可以探讨三个问题：
1. 对于索引的检测，返回值为何是 99999？其他的行不行？
2. base case 为何是 i == 0 ？
3. 备忘录 memo 的初始值为何是 66666？其他的行不行？

先说第 2 条：这是根据 dp 函数的定义决定的。根据定义，就是从 matrix[0][j] 开始下落，如果落到的目的地就是 i == 0，
那么所需的路径和就是 matrix[0][j]。

再说第 3 条：这是由题目给出的数据范围决定的。memo 的作用就是防止重复计算，如果遇到重复计算可以直接返回。那么就必须知道 memo[i][j] 
有没有存储结果。如果存了就直接返回；如果没存就去递归计算。所以 memo 的初始值一定是个特殊值，和合法的答案要有所区分。回过头看题目给出的数据范围：
matrix 是 n x n 的二维数组，其中 1 <= n <= 100。其中的元素有 -100 <= matrix[i][j] <= 100。显而易见，
合法结果的区间在 [-10000, 10000] 中。所以 memo 的初始值要避开区间 [-10000, 10000] 就行。

最后说第 1 条：对于索引越界的情况，应该返回一个不可能取到的值。因为调用的是 min() 函数，最终返回的值是最小值，所以对于不合格的索引，
只要 dp 函数返回一个永远不会被取到的最大值即可。上面说了合法答案的区间在 [-10000, 10000]，所以返回值只要大于 10000 都可以。
