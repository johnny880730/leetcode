# 91. 解码方法

# 题目
一条包含字母 A-Z 的消息通过以下映射进行了 编码 ：
```
'A' -> "1"
'B' -> "2"
...
'Z' -> "26"
```

要 解码 已编码的消息，所有数字必须基于上述映射的方法，反向映射回字母（可能有多种方法）。例如，"11106" 可以映射为：
- "AAJF" ，将消息分组为 (1 1 10 6)
- "KJF" ，将消息分组为 (11 10 6)

注意，消息不能分组为  (1 11 06) ，因为 "06" 不能映射为 "F" ，这是由于 "6" 和 "06" 在映射中并不等价。

给你一个只含数字的 非空 字符串 s ，请计算并返回 解码 方法的 总数 。

题目数据保证答案肯定是一个 32 位 的整数。

https://leetcode.cn/problems/decode-ways

提示：
- 1 <= s.length <= 100
- s 只包含数字，并且可能包含前导零。

# 示例
```
输入：s = "12"
输出：2
解释：它可以解码为 "AB"（1 2）或者 "L"（12）。
```

```
输入：s = "226"
输出：3
解释：它可以解码为 "BZ" (2 26), "VF" (22 6), 或者 "BBF" (2 2 6) 。
```

```
输入：s = "0"
输出：0
解释：没有字符映射到以 0 开头的数字。
含有 0 的有效映射是 'J' -> "10" 和 'T'-> "20" 。
由于没有字符，因此没有有效的方法对此进行解码，因为所有数字都需要映射。
```

# 解析

## 递归
定义函数 process(str, i)，返回 int，表示 str 从 i 位置开始到最后有多少种表达方式。会报超时

## 递归转DP 
dp[i] 的含义就是 process(str, i) 的含义。

# 代码

### php
```php

class LeetCode0091 {
    
    // 递归，报超时
    function numDecodings($s) {
        if (!$s) {
            return 0;
        }
        return $this->_process($s, 0);
    }
    
    // str[0...index-1] 已经转化完了
    // str[index...]能转出多少有效的，返回方法数
    protected function _process($str, $index){
        if ($index == strlen($str)) {
            return 1;
        }
        // 0 出发的任何字符串都不是有效的
        if ($str[$index] == '0') {
            return 0;
        }
        // (index) => 1~9
        $ways = $this->_process($str, $index + 1);
        // (index, index+1) 可以解码处数字的情况 -> index + 2
        if ($index + 1 == strlen($str)) {
            return $ways;
        }
        // (index index+1) "23"->23  "17"->17
        $num = intval($str[$index]) * 10 + intval($str[$index + 1]);
        // num > 26
        if ($num <= 26) {
            $ways += $this->_process($str, $index + 2);
        }
        return $ways;
    }
    
    // 递归转DP
    function numDecodings2($s) {
        $len = strlen($s);
        // dp[i] => process(str, index) 的返回值
        
        $dp = array_fill(0, $len + 1, 0);
        // index == len
        $dp[$len] = 1;
        
        // 由递归可知：index 依赖 index+1 和 index+2，所以 dp[i] 依赖 dp[i+1] 和 dp[i+2]
        // dp 从右往左填，根据递归内容略加修改即可
        for ($i = $len - 1; $i >= 0; $i--) {
            if ($s[$i] != '0') {
                $ways = $dp[$i + 1];
                if ($i + 1 == $len) {
                    $dp[$i] = $ways;
                    continue;
                }
                $num = intval($s[$i]) * 10 + intval($s[$i + 1]);
                if ($num <= 26) {
                    $ways += $dp[$i + 2];
                }
                $dp[$i] = $ways;
            }
        }
        return $dp[0];
    }
}
``` 

### go
```go
func numDecodings(s string) int {
    length := len(s)
    dp := make([]int, length + 1)
    dp[length] = 1
    for i := length - 1; i >= 0; i-- {
        if string(s[i]) != "0" {
            ways := dp[i + 1]
            if i + 1 == length {
                dp[i] = ways
                continue
            }
            s1, _ := strconv.ParseInt(string(s[i]), 10 ,32)
            s2, _ := strconv.ParseInt(string(s[i + 1]), 10 ,32)
            num := s1 * 10 + s2
            if num <= 26 {
                ways += dp[i + 2]
            }
            dp[i] = ways
        }
    }
    return dp[0]
}
```