# 583. 两个字符串的删除操作

# 题目
给定两个单词 word1 和 word2，找到使得 word1 和 word2 相同所需的最小步数，每步可以删除任意一个字符串中的一个字符。

# 示例：
```
输入: "sea", "eat"
输出: 2
解释: 第一步将"sea"变为"ea"，第二步将"eat"变为"ea"
```

https://leetcode.cn/problems/delete-operation-for-two-strings/

提示：
- 给定单词的长度不超过500。
- 给定单词中的字符只含有小写字母。


# 解析
本题和 《115. 不同的子序列》 相比，其实就是两个字符串都可以删除了，情况虽说复杂一些，但整体思路是不变的。

这次是两个字符串可以相互删了，这种题目也知道用动态规划的思路来解

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
dp[i][j]：以 word1[i - 1] 为结尾的字符串，和以 word2[j - 1] 为结尾的字符串，想要达到相等，所需要删除元素的最少次数。

### 确定递推公式
- 当word1[i - 1] 与 word2[j - 1]相同的时候
  - dp[i][j] = dp[i - 1][j - 1];
- 当 word1[i - 1] 与 word2[j - 1] 不相同的时候
  - 情况一：删 word1[i - 1]，最少操作次数为 dp[i - 1][j] + 1
  - 情况二：删 word2[j - 1]，最少操作次数为 dp[i][j - 1] + 1
  - 情况三：同时删 word1[i - 1] 和 word2[j - 1]，操作的最少次数为 dp[i - 1][j - 1] + 2
  - 三种情况取最小值

因为 dp[i][j - 1] + 1 = dp[i - 1][j - 1] + 2，所以递推公式可简化为：dp[i][j] = min(dp[i - 1][j] + 1, dp[i][j - 1] + 1)。
代码里不简化了。

从字面上理解就是，当同时删 word1[i - 1] 和 word2[j - 1]，dp[i][j - 1] 本来就不考虑 word2[j - 1]了，那么在删 word1[i - 1]，
是不是就达到两个元素都删除的效果，即 dp[i][j - 1] + 1。

### dp 数组如何初始化
从递推公式中，可以看出来，dp[i][0] 和 dp[0][j] 是一定要初始化的。

dp[i][0]：word2 为空字符串，以 word1[i - 1] 为结尾的字符串要删除多少个元素，才能和 word2 相同呢，很明显 dp[i][0] = i。

同理：dp[0][j] = j

### 确定遍历顺序
从递推公式 dp[i][j] = min(dp[i - 1][j - 1] + 2, min(dp[i - 1][j], dp[i][j - 1]) + 1); 和 dp[i][j] = dp[i - 1][j - 1] 
可以看出 dp[i][j] 都是根据左上方、正上方、正左方推出来的。

所以遍历的时候一定是从上到下，从左到右，这样保证 dp[i][j] 可以根据之前计算出来的数值进行计算。

### 举例推导 dp 数组
以 word1 = "sea"，word2 = "eat" 为例，推导 dp 数组状态图如下：

![](./images/leetcode-0583-img1.png)

时间复杂度: O(n * m)

空间复杂度: O(n * m)


# 代码

### php
```php
class LeetCode0583 {

    public function minDistance($w1, $w2) {
        $len1 = strlen($w1);
        $len2 = strlen($w2);
        $dp = array_fill(0, $len1 + 1, array_fill(0, $len2 + 1, 0));
        for ($i = 0; $i <= $len1; $i++) {
            $dp[$i][0] = $i;
        }
        for ($j = 0; $j <= $len2; $j++) {
            $dp[0][$j] = $j;
        }
        for ($i = 1; $i <= $len1; $i++) {
            for ($j = 1; $j <= $len2; $j++) {
                if ($w1[$i - 1] == $w2[$j - 1]) {
                    $dp[$i][$j] = $dp[$i - 1][$j - 1];
                } else {
                    $dp[$i][$j] = min($dp[$i - 1][$j - 1] + 2, $dp[$i - 1][$j] + 1, $dp[$i][$j - 1] + 1);
                }
            }
        }
        return $dp[$len1][$len2];
    }

}

```

### go
```go
func minDistance(w1 string, w2 string) int {
    len1, len2 := len(w1), len(w2)
    dp := make([][]int, len1 + 1)
    for k, _ := range dp {
        dp[k] = make([]int, len2 + 1)
    }
    for i := 0; i <= len1; i++ {
        dp[i][0] = i
    }
    for j := 0; j <= len2; j++ {
        dp[0][j] = j
    }
    for i := 1; i <= len1; i++ {
        for j := 1; j <= len2; j++ {
            if w1[i - 1] == w2[j - 1] {
                dp[i][j] = dp[i - 1][j - 1]
            } else {
                dp[i][j] = min(dp[i - 1][j - 1] + 2, min(dp[i - 1][j], dp[i][j - 1]) + 1)
            }
        }
    }
    return dp[len1][len2]
}

func min(a, b int) int {
    if a < b {
        return a
    }
    return b
}
```

### java
```java
public int minDistance(String word1, String word2) {
    int len1 = word1.length(), len2 = word2.length();
    int[][] dp = new int[len1 + 1][len2 + 1];
    for (int i = 0; i <= len1; i++) {
        dp[i][0] = i;
    }
    for (int j = 0; j <= len2; j++) {
        dp[0][j] =  j;
    }
    for (int i = 1; i <= len1; i++) {
        for (int j = 1; j <= len2; j++) {
            if (word1.charAt(i - 1) == word2.charAt(j - 1)) {
                dp[i][j] = dp[i - 1][j - 1];
            } else {
                dp[i][j] = Math.min(dp[i - 1][j - 1] + 2, Math.min(dp[i - 1][j], dp[i][j - 1]) + 1);
            }
        }
    }
    return dp[len1][len2];
}
```

