# 0120. 三角形最小路径和

# 题目
给定一个三角形 triangle ，找出自顶向下的最小路径和。

每一步只能移动到下一行中相邻的结点上。相邻的结点 在这里指的是 下标 与 上一层结点下标 相同或者等于 上一层结点下标 + 1 的两个结点。也就是说，如果正位于当前行的下标 i ，那么下一步可以移动到下一行的下标 i 或 i + 1 。

https://leetcode.cn/problems/triangle/description/


提示：
- 1 <= triangle.length <= 200
- triangle[0].length == 1
- triangle[i].length == triangle[i - 1].length + 1
- -10000 <= triangle[i][j] <= 10000

# 示例
```
示例 1：

输入：triangle = [[2],[3,4],[6,5,7],[4,1,8,3]]
输出：11
解释：如下面简图所示：
   2
  3 4
 6 5 7
4 1 8 3
自顶向下的最小路径和为 11（即，2 + 3 + 5 + 1 = 11）。
```
```
示例 2：

输入：triangle = [[-10]]
输出：-10
```

# 解析

## 动态规划

### DP 的定义
用 dp[i][j] 表示从三角形顶部走到位置 (i, j) 的最小路径和。这里的位置 (i, j) 指的是三角形中第 i 行第 j 列（均从 0 开始编号）的位置。

### 状态转移方程
由于每一步只能移动到下一行「相邻的节点」上，因此要想走到位置 (i, j)，上一步就只能在位置 (i − 1, j − 1) 或者位置 (i − 1, j)。在这两个位置中选择一个路径和较小的来进行转移，状态转移方程为：
```
dp[i][j] = min(dp[i − 1][j − 1], dp[i − 1][j]) + T[i][j]
```
其中 T[i][j] 表示位置 (i,j) 对应的元素值。

注意第 i 行有 i + 1 个元素，它们对应的 j 的范围为 [0, i]。当 j = 0 或 j = i 时，上述状态转移方程中有一些项是没有意义的。

例如当 j = 0 时，dp[i − 1][j − 1] 没有意义，因此状态转移方程为：
```
dp[i][0] = dp[i − 1][0] + T[i][0]
```
即当我们在第 i 行的最左侧时，我们只能从第 i−1 行的最左侧移动过来。

当 j = i 时，dp[i − 1][j] 没有意义，因此状态转移方程为：
```
dp[i][i] = dp[i − 1][i − 1] + T[i][i]
```
即当我们在第 i 行的最右侧时，我们只能从第 i−1 行的最右侧移动过来。

最终的答案即为 dp[n − 1][0] 到 dp[n − 1][n − 1] 中的最小值，其中 n 是三角形的行数。

### 初始化 DP

状态转移方程的边界条件是什么？由于已经去除了所有「没有意义」的状态，因此边界条件可以定为：
```
dp[0][0] = T[0][0]
```
即在三角形的顶部时，最小路径和就等于对应位置的元素值。这样一来，从 1 开始递增地枚举 i，并在 [0, i] 的范围内递增地枚举 j，就可以完成所有状态的计算。


# 代码

### php
```php
class LeetCode0120 {

    function minimumTotal($triangle) {
        $n = count($triangle);
        $dp = array_fill(0, $n, array_fill(0, $n, 0));
        $dp[0][0] = $triangle[0][0];
        for ($i = 1; $i < $n; $i++) {
            $dp[$i][0] = $dp[$i - 1][0] + $triangle[$i][0];
            for ($j = 1; $j < $i; $j++) {
                $dp[$i][$j] = min($dp[$i - 1][$j - 1], $dp[$i - 1][$j]) + $triangle[$i][$j];
            }
            $dp[$i][$i] = $dp[$i - 1][$i - 1] + $triangle[$i][$i];
        }
        $res = $dp[$n - 1][0];
        for ($i = 1; $i < $n; $i++) {
            $res = min($res, $dp[$n - 1][$i]);
        }

        return $res;
    }
}
```

### java
```java
class LeetCode0120 {

    public int minimumTotal(List<List<Integer>> triangle) {
        int n = triangle.size();
        int[][] dp = new int[n][n];
        dp[0][0] = triangle.get(0).get(0);
        for (int i = 1; i < n; i++) {
            dp[i][0] = dp[i - 1][0] + triangle.get(i).get(0);
            for (int j = 1; j < i; j++) {
                dp[i][j] = Math.min(dp[i - 1][j - 1], dp[i - 1][j]) + triangle.get(i).get(j);
            }
            dp[i][i] = dp[i - 1][i - 1] + triangle.get(i).get(i);
        }
        int res = dp[n - 1][0];
        for (int i = 1; i < n; i++) {
            res = Math.min(res, dp[n - 1][i]);
        }

        return res;
    }
}
```

### go
```go
func minimumTotal(triangle [][]int) int {
    n := len(triangle)
    dp := make([][]int, n)
    for i := 0; i < n; i++ {
        dp[i] = make([]int, n)
    }
    dp[0][0] = triangle[0][0]
    for i := 1; i < n; i++ {
        dp[i][0] = dp[i - 1][0] + triangle[i][0]
        for j := 1; j < i; j++ {
            dp[i][j] = min(dp[i - 1][j - 1], dp[i - 1][j]) + triangle[i][j]
        }
        dp[i][i] = dp[i - 1][i - 1] + triangle[i][i]
    }
    res := dp[n - 1][0]
    for i := 1; i < n; i++ {
        res = min(res, dp[n - 1][i])
    }
    return res
}

func min(x, y int) int {
    if x < y {
        return x
    }
    return y
}
```
