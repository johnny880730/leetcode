# 0300. 最长递增子序列

# 题目
给你一个整数数组 nums ，找到其中最长严格递增子序列的长度。

子序列是由数组派生而来的序列，删除（或不删除）数组中的元素而不改变其余元素的顺序。例如，[3, 6, 2, 7] 是数组 [0, 3, 1, 6, 2, 2, 7] 的子序列。

https://leetcode.cn/problems/longest-increasing-subsequence/description/

提示：
- 1 <= nums.length <= 2500
- -10^4 <= nums[i] <= 10^4

# 示例：
```
输入：nums = [10,9,2,5,3,7,101,18]
输出：4
解释：最长递增子序列是 [2,3,7,101]，因此长度为 4 。
```

```
输入：nums = [0,1,0,3,2,3]
输出：4
```

```
输入：nums = [7,7,7,7,7,7,7]
输出：1
```

# 解析
子序列问题是动态规划解决的经典问题，当前下标 i 的递增子序列长度，其实和 i 之前的下标 j 的子序列长度有关系，那又是什么样的关系呢。

## O(n²)的动态规划

### 定义 dp 数组
dp[i]：表示 i 之前（包括 i）的且以 nums[i] 结尾的最长递增子序列的长度

为什么一定表示 “以 nums[i] 结尾的最长递增子序” ，因为在做递增比较的时候，如果比较 nums[j] 和 nums[i] 的大小，
那么两个递增子序列一定分别以 nums[j] 为结尾 和 nums[i] 为结尾， 要不然这个比较就没有意义了，不是尾部元素的比较那么如何算递增呢。

### 确定递推公式
位置 i 的最长升序子序列等于 j 从 0 到 i - 1 各个位置的最长升序子序列 +1 的最大值。

所以：if (nums[i] > nums[j]) dp[i] = max(dp[i], dp[j] + 1);

注意这里不是要 dp[i] 与 dp[j] + 1 进行比较，而是要取 dp[j] + 1 的最大值。

### dp 数组初始化
每一个 i 对应的 dp[i] 的起始大小都是 1，因为递增子序列总归会包括自己。

### 遍历顺序
dp[i] 是由 0 到 i - 1 各个位置的最长递增子序列推导而来，那么遍历 i 一定是从前向后遍历。

j 其实就是遍历 0 到 i - 1，那么是从前到后，还是从后到前遍历都无所谓，只要把 0 到 i-1 的元素都遍历了就行了。 所以默认习惯从前向后遍历。

### 举例推导 dp 数组
输入：[0, 1, 0, 3, 2]，dp 数组的变化如下：

![](./images/leetcode-0300-img1.png)

---

## O(n) 的动态规划

增加辅助数组 ends[b] 和 right 变量。ends[b] 表示长度为 b + 1 的递增子序列的最小结尾数是 ends[b]。

ends[0 ... right] 一定为一个递增数组，看作为有效区；ends[right + 1 ... N - 1] 看作无效区。

遍历（ends 为递增，可以使用二分查找）：
- 在遍历 num[i] 的过程中，查找 ends[0 ... right - 1], 存在比 num[i] 大的则可以用 num[i] 替换掉。
- 如果 ends 中都比 num[i] 小，则有效区间右移


# 代码

### php
```php
class LeetCode0300 {

    // O(n²)
    public function lengthOfLIS($nums) {
        $len = count($nums);
        $dp = array_fill(0, $len, 1);
        for ($i = 0; $i < $len; $i++) {
            for ($j = 0; $j < $i; $j++) {
                if ($nums[$i] > $nums[$j]) {
                    $dp[$i] = max($dp[$i], $dp[$j] + 1);
                }
            }
        }
        return max($dp);
    }
    
    // O(n)
    public function lengthOfLIS2($nums) {
        if (!$nums) {
            return 0;
        }
        $len = count($nums);
        $ends = array_fill(0, $len, 0);
        $ends[0] = $nums[0];
        $right = 0;
        $max = 1;
        for ($i = 1; $i < $len; $i++) {
            $l = 0;
            $r = $right;
            while ($l <= $r) {
                $m = $l + (($r - $l) >> 1);
                if ($nums[$i] > $ends[$m]) {
                    $l = $m + 1;
                } else {
                    $r = $m - 1;
                }
            }
            $right = max($right, $l);
            $ends[$l] = $nums[$i];
            $max = max($max, $l + 1);
        }
        return $max;
    }

}
```

### go
```go
// O(n²)
func lengthOfLIS(nums []int) int {
    size := len(nums)
    dp := make([]int, size)
    for k, _ := range dp {
        dp[k] = 1
    }
    for i := 0; i < size; i++ {
        for j := 0; j < i; j++ {
            if nums[i] > nums[j] {
                dp[i] = max(dp[i],  dp[j] + 1)
            }
        }
    }
    res := -1
    for i := 0; i < size ; i++ {
        if dp[i] > res {
            res = dp[i]
        }
    }
    return res
}

func max (a, b int) int {
    if a > b {
        return a
    }
    return b
}
```

### java
```java
class Solution {
    public int lengthOfLIS(int[] nums) {
        int[] dp = new int[nums.length];
        for (int i = 0; i < dp.length; i++) {
            dp[i] = 1;
        }
        for (int i = 0; i < dp.length; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
        }
        int res = -1;
        for (int i = 0; i < dp.length; i++) {
            res = Math.max(res, dp[i]);
        }
        return res;

    }
}
```