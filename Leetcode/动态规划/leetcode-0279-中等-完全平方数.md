# 0279. 完全平方数

# 题目
给定正整数 n，找到若干个完全平方数（比如 1, 4, 9, 16, ...）使得它们的和等于 n。你需要让组成和的完全平方数的个数最少。
给你一个整数 n ，返回和为 n 的完全平方数的 最少数量 。
完全平方数 是一个整数，其值等于另一个整数的平方；换句话说，其值等于一个整数自乘的积。例如，1、4、9 和 16 都是完全平方数，而 3 和 11 不是。

https://leetcode.cn/problems/perfect-squares/

提示：
- 1 <= n <= 10^4

# 示例：
```
输入：n = 12
输出：3
解释：12 = 4 + 4 + 4
```

```
输入：n = 13
输出：2
解释：13 = 4 + 9
```


# 解析
可能刚看这种题感觉没啥思路，又平方和的，又最小数的。

题目翻译一下：完全平方数就是物品（可以无限件使用），凑个正整数 n 就是背包，问凑满这个背包最少有多少物品？

感受出来了没，这么浓厚的完全背包氛围！

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
dp[j]：和为 j 的完全平方数的最少数量为 dp[j]

### 确定递推公式
dp[j] 可以由 dp[j - i * i] 推出， dp[j - i * i] + 1 便可以凑成 dp[j]。

此时要选择最小的 dp[j]，所以递推公式：dp[j] = min(dp[j - i * i] + 1, dp[j]);

### dp 数组初始化
dp[0] 表示和为 0 的完全平方数的最小数量，那么 dp[0] 一定是 0。

那 0 * 0 也算是一种啊，为啥 dp[0] 就是 0 呢？看题目描述，找到若干个完全平方数（比如 1, 4, 9, 16, ...），
题目描述中可没说要从 0 开始，dp[0] = 0 完全是为了递推公式。

非 0 下标的 dp[j] 应该是多少呢？

从递归公式 dp[j] = min(dp[j - i * i] + 1, dp[j]) 中可以看出每次 dp[j] 都要选最小的，所以非 0 下标的 dp[j] 一定要初始为最大值，
这样 dp[j] 在递推的时候才不会被初始值覆盖。

### 确定遍历顺序
现在知道这是完全背包
- **如果求组合数，就是外层 for 遍历物品，内层 for 遍历背包。**
- **如果求排列数，就是外层 for 遍历背包，内层 for 遍历物品。**

本题和 《322. 零钱兑换》也是一样的，是求最小数。所以本题两种遍历都是可以的。

### 举例推导dp数组
已输入 n = 5 例，dp 状态图如下：

![](./images/leetcode-0279-img1.png)

# 代码

### php
```php
class LeetCode0279 {

    public function numSquares($n) {
        $dp = array_fill(0, $n + 1, PHP_INT_MAX);
        $dp[0] = 0;
        for ($i = 1; $i * $i <= $n; $j++) {
            for ($j = $i * $i; $j <= $n; $j++) {
                $dp[$j] = min($dp[$j - $i * $i] + 1, $dp[$j]);
            }
        }
        return $dp[$n];
    }

}
```

### java
```java
class Leetcode0279 {
    
    public int numSquares(int n) {
        int[] dp = new int[n + 1];
        for (int i = 0; i < dp.length; i++) {
            dp[i] = Integer.MAX_VALUE;
        }
        dp[0] = 0;
        for (int i = 1; i * i <= n; i++) {
            for (int j = i * i; j <= n; j++) {
                dp[j] = Math.min(dp[j], dp[j - i * i] + 1);
            }
        }
        return dp[n];
    }
}
```

### go
```go
package main

import "math"

func numSquares(n int) int {
    dp := make([]int, n + 1)
    for i := 0; i <= n; i++ {
        dp[i] = math.MaxInt32
    }
    dp[0] = 0
	for i := 1; i * i <= n; i++ {
		for j := i * i; j <= n; j++ {
	        dp[j] = Min(dp[j], dp[j - i * i] + 1)		
        }
    }
    return dp[n]
}

func Min(a, b int) int {
	if a < b {
	    return a	
    }
	return b
}
```
