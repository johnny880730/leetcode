# 121. 买卖股票的最佳时机

# 题目
给定一个数组 prices ，它的第 i 个元素 prices[i] 表示一支给定股票第 i 天的价格。

你只能选择 某一天 买入这只股票，并选择在 未来的某一个不同的日子 卖出该股票。设计一个算法来计算你所能获取的最大利润。

返回你可以从这笔交易中获取的最大利润。如果你不能获取任何利润，返回 0 。

https://leetcode.cn/problems/best-time-to-buy-and-sell-stock/


提示：
- 1 <= prices.length <= 10^5
- 0 <= prices[i] <= 10^4

# 示例：
```
输入：[7,1,5,3,6,4]
输出：5
解释：在第 2 天（股票价格 = 1）的时候买入，在第 5 天（股票价格 = 6）的时候卖出，最大利润 = 6-1 = 5 。
注意利润不能是 7-1 = 6, 因为卖出价格需要大于买入价格；同时，你不能在买入前卖出股票。
```

```
输入：prices = [7,6,4,3,1]
输出：0
解释：在这种情况下, 没有交易完成, 所以最大利润为 0。
```


# 解析

# 普通思路

算出每一天都卖出股票的收益，其中最大的收益就是答案。

第 i 天的股票价格是 prices[i]，这天的收益就是 0 到 i - 1 这些天的最小值作为买入点，在 i 这天卖出，如果 0 到 i - 1 的股价都大于 prices[i]，
只能选择在第 i 天买入又卖出，也就是没收入（总比买入之前高价这天又卖出实现负收益的好）。这么循环出每一天的收益，求得最大值


## 动态规划五部曲

### 定义 dp 数组
dp[i][j]：
- i 表示第几天；
- j = 0 表示持有股票的状态，j = 1 表示不持有股票的状态

### 确定递推公式
第 i 天持有的股票即 dp[i][0] 可以从两个状态推导得来：
- 第 i - 1 天就持有股票，那么就保持现状，即 dp[i - 1][0]
- 第 i 天买入股票，所得现金就是买入今天的股票后所得的现金，即 -prices[i]

dp[i][0] 应该取最大值，所以 dp[i][0] = max( dp[i - 1][0], -prices[i] )

如果第 i 天不持有股票即 dp[i][1]，也可以从两个状态推导： 
- 第 i - 1 天不持有，保持现状，即 dp[i - 1][1]
- 第 i 天卖出股票获得收益，即 prices[i] + dp[i - 1][0]

同样 dp[i][1] 取最大值，所以 dp[i][1] = max(dp[i - 1][1]，dp[i - 1][0] + prices[i])
     
### dp 数组初始化
由递推公式可知，dp[i][j] 都是从 dp[0][0] 和 dp[0][1] 推导来的：
- dp[0][0] 表示第 0 天持有股票，此时持有的一定是买入的股票了，所以 dp[0][0] = -prices[0]
- dp[0][1] 表示第 0 天不持有股票，就是 0，所以 dp[0][1] = 0
     
### 遍历顺序
dp[i] 都是从 dp[i - 1] 推导出来的，所以遍历顺序一定是从前往后

### 举例推导 dp 数组
以示例1，输入：[7, 1, 5, 3, 6, 4] 为例，dp 数组状态如下：

![](./images/leetcode-0121-img1.png)
     

# 代码

### php
```php

class LeetCode0121 {

    public function maxProfit($prices) {
        $len = count($prices);
        $dp = array_fill(0, $len, array_fill(0, 2, 0));
        $dp[0][0] = -$prices[0];
        $dp[0][1] = 0;
        for ($i = 1; $i < $len; $i++) {
            $dp[$i][0] = max($dp[$i - 1][0], -$prices[$i]);
            $dp[$i][1] = max($dp[$i - 1][1], $dp[$i - 1][0] + $prices[$i]);
        }
        return $dp[$len - 1][1];    // 返回最后一天没有持有股票的状态
    }

}
```

### go
```go
func maxProfit(prices []int) int {
    length := len(prices)
    dp := make([][]int, length)
    for i := 0; i < length; i++ {
        dp[i] = make([]int, 2)
    }
    dp[0][0] = -prices[0]
    dp[0][1] = 0
    for i := 1; i < length; i++ {
        dp[i][0] = max(dp[i - 1][0], -prices[i])
        dp[i][1] = max(dp[i - 1][1], dp[i - 0][0] + prices[i])
    }
    return dp[length - 1][1]
}

func max(a, b int) int {
	if a > b {
	    return a	
    }
	return b
}
```

### java
```java
class LeetCode0121 {

    public int maxProfit(int[] prices) {
        int[][] dp = new int[prices.length][2];
        dp[0][0] = -prices[0];
        dp[0][1] = 0;
        for (int i = 1; i < prices.length; i++) {
            dp[i][0] = Math.max(dp[i - 1][0], -prices[i]);
            dp[i][1] = Math.max(dp[i - 1][1], dp[i - 1][0] + prices[i]);
        }
        return dp[prices.length - 1][1];
    }
}
```
