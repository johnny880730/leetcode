# 0005. 最长回文子串

# 题目
给你一个字符串 s，找到 s 中最长的回文子串。

https://leetcode.cn/problems/longest-palindromic-substring/description/

提示：
- 1 <= s.length <= 1000
- s 仅由数字和英文字母（大写和/或小写）组成


# 示例
```
输入：s = "babad"
输出："bab"
解释："aba" 同样是符合题意的答案。
```
```
输入：s = "cbbd"
输出："bb"
```
```
输入：s = "a"
输出："a"
```
```
输入：s = "ac"
输出："a"
```

# 解析
本题和 [647. 回文子串](../动态规划/leetcode-0647-回文子串.md) 差不多是一样的，但 《647.回文子串》更基本一点，建议可以先做《647.回文子串》

## 双指针
回文串的难度就是：回文串的长度可能是偶数也可能是奇数。

解决该问题的核心是：从中心向两端发散的双指针技巧。
- 如果长度是奇数，只有一个中心
- 如果长度是偶数，就有两个中心

可以用一个辅助方法处理这两种情况

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
布尔类型的 dp[i][j]：表示区间 [i, j] （注意是左闭右闭）的子串是否是回文子串，如果是则 dp[i][j] 为 true，否则为 false。

### 确定递推公式
在确定递推公式时，就要分析如下几种情况。

整体上是两种，就是 s[i] 与 s[j] 相等，s[i] 与 s[j] 不相等这两种
- 当 s[i] 与 s[j] 不相等，那没啥好说的了，dp[i][j] 一定是 false。
- 当 s[i] 与 s[j] 相等时，这就复杂一些了，有如下三种情况
  - 下标 i 与 j 相同，表示 i、j 指向同一个字符。例如 a，当然是回文子串
  - 下标 i 与 j 相差为 1，例如 aa，也是文子串
  - 下标 i 与 j 相差大于 1 的时候，例如 cabac，此时 s[i] 与 s[j] 已经相同了，我们看 [i, j] 区间是不是回文子串，就看 aba 是不是回文就可以了。那么 aba 的区间就是 [i + 1, j - 1] 区间，这个区间是不是回文就看 dp[i + 1][j - 1] 是否为 true。

那么递推公式如下：
```
if (s[i] == s[j]) {
    if (j - i <= 1) { // 情况一 和 情况二
        dp[i][j] = true;
    } else if (dp[i + 1][j - 1]) { // 情况三
        dp[i][j] = true;
    }
}
```

注意这里没有列出当 s[i] != s[j] 的时候，因为在 dp[i][j] 初始化的时候，就初始为 false。

在得到 [i, j] 区间是否是回文子串的时候，直接保存最长回文子串的左边界和右边界

```
if (dp[i][j] && j - i + 1 > maxLen) {
    maxLen = j - i + 1;
    left = i;
    right = j;
}
```

### dp 数组初始化
dp[i][j] 可以初始化为 true 么？ 当然不行，怎能刚开始就全都匹配上了。所以 dp[i][j] 初始化为false。

### 确定遍历顺序
遍历顺序可有点讲究了。

首先从递推公式中可以看出，情况三是根据 dp[i + 1][j - 1] 是否为true，在对 dp[i][j] 进行赋值 true 的。

dp[i + 1][j - 1] 在 dp[i][j] 的左下角，如图：

![](./images/leetcode-0005-img1.png)

如果这矩阵是从上到下，从左到右遍历，那么会用到没有计算过的 dp[i + 1][j - 1]，也就是根据不确定是不是回文的区间 [i + 1, j - 1]，
来判断了 [i, j] 是不是回文，那结果一定是不对的。

所以一定要从下到上，从左到右遍历，这样保证 dp[i + 1][j - 1] 都是经过计算的。

### 举例推导 dp 数组
举例，输入："aaa"，dp[i][j] 状态如下：

![](./images/leetcode-0005-img2.png)




# 代码

### php
```php
class LeetCode0005 {

    // 双指针
    function longestPalindrome($s) {
        $res = '';
        $len = strlen($s);
        for ($i = 0; $i < $len; $i++) {
            $s1 = $this->_check($s, $i, $i);
            $s2 = $this->_check($s, $i, $i + 1);
            $len1 = strlen($s1);
            $len2 = strlen($s2);
            $res = $len1 > strlen($res) ? $s1 : $res;
            $res = $len2 > strlen($res) ? $s2 : $res;
        }
        return $res;
    }
    
    function _check($s, $l, $r) {
        $len = strlen($s);
        while ($l >= 0 && $r < $len && $s[$l] == $s[$r]) {
            $l--;
            $r++;
        }
        return substr($s, $l + 1, $r - $l - 1);
    }

    // dp
    function longestPalindrome2($s) {
        $len = strlen($s);
        $dp = array_fill(0, $len, array_fill(0, $len, false));
        $maxLength = $left = $right = 0;
        for ($i = $len - 1; $i >= 0; $i--) {
            for ($j = $i; $j < $len; $j++) {
                if ($s[$i] == $s[$j]) {
                    if ($j - $i <= 1) {     // 情况一、情况二
                        $dp[$i][$j] = true;
                    } else if ($dp[$i + 1][$j - 1]) {   // 情况三
                        $dp[$i][$j] = true;
                    }
                }
                if ($dp[$i][$j] && $j - $i + 1 > $maxLength) {
                    $maxLength = $j - $i + 1;
                    $left = $i;
                    $right = $j;
                }
            }
        }
        return substr($s, $left, $right - $left + 1);
    }


}
```

### java
```java
class LeetCode0005 {

    public String longestPalindrome(String s) {
        int len = s.length();
        if (len == 0) {
            return "";
        }
        boolean[][] dp = new boolean[len][len];
        int maxLen = 0, left = 0, right = 0;
        char[] charArr = s.toCharArray();
        for (int i = len - 1; i >= 0; i--) {
            for (int j = i; j < len; j++) {
                if (charArr[i] == charArr[j]) {
                    if (j - i <= 1) {
                        dp[i][j] = true;
                    } else if (dp[i + 1][j - 1]) {
                        dp[i][j] = true;
                    }
                }
                if (dp[i][j] && j - i + 1 > maxLen) {
                    maxLen = j - i + 1;
                    left = i;
                    right = j;
                }
            }
        }
        return s.substring(left, right + 1);
    }
}
```


### go
```go
func longestPalindrome(s string) string {
    size := len(s)
    dp := make([][]bool, size)
    for k := range dp {
        dp[k] = make([]bool, size)
    }
    maxLength, left, right := 0, 0, 0
    for i := size - 1; i >= 0; i-- {
        for j := i; j < size; j++ {
            if s[i] == s[j] {
                if j - i <= 1 {
                    dp[i][j] = true
                } else if dp[i + 1][j - 1] == true {
                    dp[i][j] = true
                }
            }
            if dp[i][j] == true && j - i + 1 > maxLength {
                maxLength = j - i + 1
                left = i
                right = j
            }
        }
    }
    
    return s[left : right + 1]
}
```


