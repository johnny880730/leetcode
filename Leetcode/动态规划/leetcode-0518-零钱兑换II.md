# 518. 零钱兑换 II

# 题目
给你一个整数数组 coins 表示不同面额的硬币，另给一个整数 amount 表示总金额。

请你计算并返回可以凑成总金额的硬币组合数。如果任何硬币组合都无法凑出总金额，返回 0 。

假设每一种面额的硬币有无限个。

题目数据保证结果符合 32 位带符号整数。

https://leetcode.cn/problems/coin-change-ii/


提示：
- 1 <= coins.length <= 300
- 1 <= coins[i] <= 5000
- coins 中的所有值 互不相同
- 0 <= amount <= 5000

# 示例：
```
输入：amount = 5, coins = [1, 2, 5]
输出：4
解释：有四种方式可以凑成总金额：
5 = 5
5 = 2 + 2 + 1
5 = 2 + 1 + 1 + 1
5 = 1 + 1 + 1 + 1 + 1
```

```
输入：amount = 3, coins = [2]
输出：0
解释：只用面额 2 的硬币不能凑成总金额 3 。
```

```
输入：amount = 10, coins = [10]
输出：1
```


# 解析

## 动态规划
把题目的问题转化为背包问题的描述形式：有一个背包，最大容量为 amount，有一系列物品 coins，每个物品的重量为 coins[i]，每个物品的数量无限。
问有多少种方法，能够恰好把背包装满？

这里的每个物品的数量是无限的，也就是“完全背包”问题

### “状态” 和 “选择”
状态有两个：就是“背包的容量”和“可选择的物品”，选择则就是“装进背包”和“不装进背包”了

### dp 数组及含义
状态有两个，所以需要二维 dp 数组。dp[i][j] 定义如下：

若只使用前 i 个物品（可以重复使用，i 从 1 开始计数），当背包容量为 j 时，有 dp[i][j] 种方法可以装满背包。

经过上面的定义可以得到 base case：dp[0][..] = 0，dp[..][0] = 1。 
- i = 0 代表不使用任何硬币，这种情况下无法凑出任何金额；
- j = 0 表示需要凑出的目标金额为 0，那么什么都不做就是唯一的一种方法。

### 状态转移方程
如果不把第 i 个物品放入背包，也就是不使用 coins[i - 1] 这个面值的硬币，那么凑出面额 j 的方法数 dp[i][j] 等于 dp[i - 1][j]，继承之前的结果。

如果把第 i 个物品放入背包，也就是使用 coins[i - 1] 这个面值的硬币，那么凑出面额 j 的方法数 dp[i][j] 等于 dp[i][j - coins[i - 1]]

注意：定义中的 i 从 1 开始计数，所以 coins 的索引 i - 1 表示第 i 个硬币的面值。

以上就是两种选择，而想求的 dp[i][j] 是“共有多少种凑法”，所以 dp[i][j] 的值是上面两种选择的结果之和。

# 代码

### php
```php
class LeetCode0518 {

    public function change($amount, $coins) {
        $len = count($coins);
        $dp = array_fill(0, $len + 1, array_fill(0, $amount + 1, 0));
        // base case
        for ($i = 0; $i <= $len; $i++) {
            $dp[$i][0] = 1;
        }

        for ($i = 1; $i <= $len; $i++) {
            for ($j = 1; $j <= $amount; $j++) {
                if ($coins[$i - 1] > $j) {
                    $dp[$i][$j] = $dp[$i - 1][$j];
                } else {
                    $dp[$i][$j] = $dp[$i - 1][$j] + $dp[$i][$j - $coins[$i - 1]];
                }
            }
        }

        return $dp[$len][$amount];
    }

}
```

### java
```java
public int change(int amount, int[] coins) {
    int len = coins.length;
    int[][] dp = new int[len + 1][amount + 1];
    for (int i = 0; i <= len; i++) {
        dp[i][0] = 1;
    }
    for (int i = 1; i <= len; i++) {
        for (int j = 1; j <= amount; j++) {
            if (coins[i - 1] > j) {
                dp[i][j] = dp[i - 1][j];
            } else {
                dp[i][j] = dp[i - 1][j] + dp[i][j - coins[i - 1]];
            }
        }
    }

    return dp[len][amount];
}
```

### go
```go
func change(amount int, coins []int) int {
    len := len(coins)
    dp := make([][]int, len + 1);
    for k := range dp {
        dp[k] = make([]int, amount + 1)
    }
    for i := 0; i <= len; i++ {
        dp[i][0] = 1
    }
    for i := 1; i <= len; i++ {
        for j := 1; j <= amount; j++ {
            if coins[i - 1] > j {
                dp[i][j] = dp[i - 1][j]
            } else {
                dp[i][j] = dp[i - 1][j] + dp[i][j - coins[i - 1]]
            }
        }
    }
    return dp[len][amount]
}
```
