# 1155. 掷骰子等于目标和的方法数


# 题目
这里有 n 个一样的骰子，每个骰子上都有 k 个面，分别标号为 1 到 k 。

给定三个整数 n, k 和 target ，返回可能的方式（从总共 k^n 种方式中）滚动骰子的数量，使正面朝上的数字之和等于 target 。

答案可能很大，你需要对 10^9 + 7 取模 。

提示：
- 1 <= n, k <= 30
- 1 <= target <= 1000

https://leetcode.cn/problems/number-of-dice-rolls-with-target-sum/

# 示例
```
输入：n = 1, k = 6, target = 3
输出：1
解释：你扔一个有 6 个面的骰子。
得到 3 的和只有一种方法。
```
```
输入：n = 2, k = 6, target = 7
输出：6
解释：你扔两个骰子，每个骰子有 6 个面。
得到 7 的和有 6 种方法：1+6 2+5 3+4 4+3 5+2 6+1。
```
```
输入：n = 30, k = 30, target = 500
输出：222616187
解释：返回的结果必须是对 10^9 + 7 取模。
```

# 解析

## 动态规划
假设每个骰子都有 6 个面，现在要计算用 3 个骰子掷出数字之和恰好等于 13 的方案数。

掷一个骰子，枚举掷出的数字（正面朝上的数字）：
- 掷出了 1：问题变成「用 2 个骰子掷出数字之和恰好等于 12 的方案数」。
- 掷出了 2：问题变成「用 2 个骰子掷出数字之和恰好等于 11 的方案数」。
- ...
- 掷出了 6：问题变成「用 2 个骰子掷出数字之和恰好等于 7 的方案数」。

这 6 种情况，都会把原问题变成一个和原问题相似的、规模更小的子问题，所以可以用递归解决。

不同的子问题，需要处理的「骰子个数」和「数字之和」是不同的，所以用两个参数就可以表示一个子问题。

具体地，定义 dp(i, j) 表示用 i 个骰子掷出数字之和恰好等于 j 的方案数。

掷一个骰子，枚举掷出的数字（正面朝上的数字）：
- 掷出了 1：问题变成「用 i − 1 个骰子掷出数字之和恰好等于 j − 1 的方案数」。
- 掷出了 2：问题变成「用 i − 1 个骰子掷出数字之和恰好等于 j − 2 的方案数」。
- ...
- 掷出了 k：问题变成「用 i − 1 个骰子掷出数字之和恰好等于 j − k 的方案数」。


根据加法原理，累加这 k 种情况的方案数之和，就得到了 dp(i, j)。写成式子就是

```
dp(i, j) = dp(i - 1, j - 1) + dp(i - 1, j - 2) + ... + dp(i - 1, j - k)
```

递归边界：dp(0, 0) = 1，表示没有骰子，数字之和恰好等于 0 有 1 种方案。对于 j < 0 和 i = 0、j > 0 的情况，都无法做到，返回 0。

递归入口：dp(n, target)，也就是答案。

此外，递归之前可以判断下：如果 target < n 或者 target > nk，那么无法得到 target，返回 0。

为了避免讨论 j < 0 的情况，可以把问题改成：每个骰子上的数字是 0 到 k − 1，数字之和是 target − n。相当于把每个骰子掷出的数字都提一个 1 出来，
那么这些骰子的数字之和就等于 target − n。

如此一来，如果 x > j，那么 j − x < 0，方案数必然为 0，所以 x 至多枚举到 j。

修改后的式子为
```
dp(i, j) = dp(i - 1, j) + dp(i - 1, j - 1) + ... + dp(i - 1, j - min(k - 1, j))
```

作者：灵茶山艾府
链接：https://leetcode.cn/problems/number-of-dice-rolls-with-target-sum/solutions/2495836/ji-bai-100cong-ji-yi-hua-sou-suo-dao-di-421ab/
来源：力扣（LeetCode）
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。


注意到，「先掷出 1 再掷出 2」和「先掷出 2 再掷出 1」，都相当于用 2 个骰子掷出 3，都会从 dp(i, j) 递归到 dp(i − 2, j − 3)。

一叶知秋，整个递归中有大量重复递归调用（递归入参相同）。由于递归函数没有副作用，同样的入参无论计算多少次，算出来的结果都是一样的，因此可以用记忆化搜索来优化：
- 如果一个状态（递归入参）是第一次遇到，那么可以在返回前，把状态及其结果记到一个 memo 数组中。
- 如果一个状态不是第一次遇到（memo 中保存的结果不等于 memo 的初始值），那么可以直接返回 memo 中保存的结果。

注意：memo 数组的初始值一定不能等于要记忆化的值！例如初始值设置为 0，并且要记忆化的 dp(i, j) 也等于 0，那就没法判断 0 到底表示第一次遇到这个状态，
还是表示之前遇到过了，从而导致记忆化失效。对于包含取模的题目，是有可能会算出 0 的。一般初始值设置为 −1。


# 代码

### php
```php
class Leetcode1155 {
    
    function numRollsToTarget($n, $k, $target) {
        // 无法组成 target
        if ($target < $n || $target > $n * $k) {
            return 0;
        }
        $memo = array_fill(0, $n + 1, array_fill(0, $target - $n + 1, -1));
        return $this->_dp($n, $target - $n, $k, $memo);
    }

    function  _dp($i, $j, $k, &$memo) {
        if ($i == 0) {
            return $j == 0 ? 1 : 0;
        }
        if ($memo[$i][$j] != -1) {
            return $memo[$i][$j];
        }
        $res = 0;
        for ($x = 0; $x < $k && $x <= $j; $x++) {
            // 掷出了 x
            $res = ($res + $this->_dp($i - 1, $j - $x, $k, $memo)) % 1000000007;
        }
        $memo[$i][$j] = $res;
        
        return $res;
    }
}
```

### java
```java
class LeetCode1155 {

    private static final int MOD = 1_000_000_007;

    public int numRollsToTarget(int n, int k, int target) {
        if (target < n || target > n * k) {
            return 0; // 无法组成 target
        }
        int[][] memo = new int[n + 1][target - n + 1];
        for (int[] m : memo) {
            Arrays.fill(m, -1); // -1 表示没有计算过
        }
        return dfs(n, target - n, memo, k);
    }

    private int dfs(int i, int j, int[][] memo, int k) {
        if (i == 0) {
            return j == 0 ? 1 : 0;
        }
        if (memo[i][j] != -1) { // 之前计算过
            return memo[i][j];
        }
        int res = 0;
        for (int x = 0; x < k && x <= j; x++) { // 掷出了 x
            res = (res + dfs(i - 1, j - x, memo, k)) % MOD;
        }
        return memo[i][j] = res; // 记忆化
    }
}

```
