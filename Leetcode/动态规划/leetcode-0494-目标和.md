# 494. 目标和

# 题目
给你一个整数数组 nums 和一个整数 target 。

向数组中的每个整数前添加 '+' 或 '-' ，然后串联起所有整数，可以构造一个 表达式 ：

例如，nums = [2, 1] ，可以在 2 之前添加 '+' ，在 1 之前添加 '-' ，然后串联起来得到表达式 "+2-1" 。
返回可以通过上述方法构造的、运算结果等于 target 的不同 表达式 的数目。

https://leetcode.cn/problems/target-sum

提示：
- 1 <= nums.length <= 20
- 0 <= nums[i] <= 1000
- 0 <= sum(nums[i]) <= 1000
- -1000 <= target <= 1000


# 示例
```
输入：nums = [1,1,1,1,1], target = 3
输出：5
解释：一共有 5 种方法让最终目标和为 3 。
-1 + 1 + 1 + 1 + 1 = 3
+1 - 1 + 1 + 1 + 1 = 3
+1 + 1 - 1 + 1 + 1 = 3
+1 + 1 + 1 - 1 + 1 = 3
+1 + 1 + 1 + 1 - 1 = 3
```
```
输入：nums = [1], target = 1
输出：1
```

# 解析
加法的总和为 x，那么减法对应的总和就是 sum - x。所以要求的是 x - (sum - x) = target，得到 x = (target + sum) / 2，
此时问题就转化为，装满容量为 x 的背包，有几种方法。

这里的 x，就是 bagSize，也就是背包容量。

看到 (target + sum) / 2 应该担心计算的过程中向下取整有没有影响。当 target + sum 不能给 2 整除的话说明此题没有方案可解。

再回归到 0-1 背包问题，为什么是 0-1 背包呢？

因为每个物品（题目中的 1 ）只用一次！这次和之前遇到的背包问题不一样了，之前都是求容量为 j 的背包，最多能装多少。
本题则是装满有几种方法。其实这就是一个组合问题了。

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
dp[j] 表示：填满 j（包括 j ）这么大容积的包，有 dp[j] 种方法

### 确定递推公式
有哪些来源可以推出 dp[j] 呢？

期是只要搞到 nums[i]，凑成 dp[j] 就有 dp[j - nums[i]] 种方法。例如：dp[j]，j 为5，
- 已经有一个 1 （nums[i]） 的话，有 dp[4] 种方法 凑成 容量为5的背包。
- 已经有一个 2 （nums[i]） 的话，有 dp[3] 种方法 凑成 容量为5的背包。
- 已经有一个 3 （nums[i]） 的话，有 dp[2] 中方法 凑成 容量为5的背包
- 已经有一个 4 （nums[i]） 的话，有 dp[1] 中方法 凑成 容量为5的背包
- 已经有一个 5 （nums[i]） 的话，有 dp[0] 中方法 凑成 容量为5的背包

那么凑整 dp[5] 有多少方法呢，也就是把 所有的 dp[j - nums[i]] 累加起来。

所以求组合类问题的公式，都是类似这种：
```
dp[j] += dp[j - nums[i]]
```

**这个公式在背包解决排列组合问题的时候还会用到！**

### dp 数组如何初始化
从递推公式可以看出，在初始化的时候 dp[0] 一定要初始化为1，因为 dp[0] 是在公式中一切递推结果的起源，如果 dp[0] 是 0的话，递推结果将都是 0。

这里有人可能认为从 dp 数组定义来说 dp[0] 应该是 0，也有人认为 dp[0] 应该是 1。其实不要硬去解释它的含义，就把 dp[0] 的情况带入本题看看应该等于多少。

如果数组为 [0] ，target = 0，那么 bagSize = (target + sum) / 2 = 0。 dp[0] 也应该是 1，也就是说给数组里的元素 0 前面无论放加法还是减法，都是 1 种方法。

所以本题应该初始化 dp[0] 为 1。

可能有人想了，那如果是 数组 [0,0,0,0,0] target = 0 呢。

其实此时最终的 dp[0] = 32，也就是这五个零子集的所有组合情况，但此 dp[0] 非彼 dp[0]，dp[0] 能算出32，其基础是因为 dp[0] = 1 累加起来的。

dp[j] 其他下标对应的数值也应该初始化为0，从递推公式也可以看出，dp[j] 要保证是 0 的初始值，才能正确的由 dp[j - nums[i]] 推导出来。

### 确定遍历顺序
在动态规划：关于 0-1 背包问题，对于 0-1 背包问题一维 dp 的遍历，nums 放在外循环，target 在内循环，且内循环倒序。

### 举例推导dp数组
输入：nums: [1, 1, 1, 1, 1], target: 3。

bagSize = (S + sum) / 2 = (3 + 5) / 2 = 4

dp 数组状态变化如下：

![](./images/leetcode-0494-img1.png)


# 代码

### php
```php
class LeetCode0494 {

    // DP
    public function findTargetSumWays($nums, $target) {
        $sum = array_sum($nums);
        if (abs($target) > $sum || ($sum + $target) % 2 != 0) {
            return 0;
        }
        $bagSize = ($sum + $target) >> 1;
        $dp = array_fill(0, $bagSize + 1, 0);
        $dp[0] = 1;
        for ($i = 0; $i < count($nums); $i++) {
            for ($j = $bagSize; $j >= $nums[$i]; $j--) {
                $dp[$j] += $dp[$j - $nums[$i]];
            }
        }
        return $dp[$bagSize];
    }

}
```

### go
```go
func findTargetSumWays(nums []int, target int) int {
	sum := 0
	for _, v := range nums {
		sum += v
	}
	if math.Abs(float64(target)) > float64(sum) || (sum + target) % 2 != 0 {
		return 0
	}
	bagSize := (sum + target) >> 1
	dp := make([]int, bagSize + 1)
	dp[0] = 1
	for i := 0; i < len(nums); i++ {
		for j := bagSize; j >= nums[i]; j-- {
			dp[j] += dp[j - nums[i]]
		}
	}
	return dp[bagSize]
}
```