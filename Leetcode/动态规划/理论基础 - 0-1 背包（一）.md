# 理论基础 - 0-1 背包（一）

背包问题有不同的种类，区分的规则如下图：

![](./images/bagsProblem-img1.png)

leetcode 上连多重背包的题目都没有，所以题库也告诉我们，0-1 背包和完全背包就够用了。而完全背包又是也是 0-1 背包稍作变化而来，
即：完全背包的物品数量是无限的。

所以背包问题的理论基础重中之重是 0-1 背包，一定要理解透。

leetcode 上没有纯 0-1 背包的问题，都是 0-1 背包应用方面的题目，也就是需要转化为 0-1 背包问题。

# 0-1 背包
有 n 件物品和一个最多能背重量为w 的背包。第 i 件物品的重量是 weight[i]，得到的价值是 value[i] 。
每件物品只能用一次，求解将哪些物品装入背包里物品价值总和最大。

以上就是 0-1 背包问题的问题描述。暴力的解法应该是怎么样的呢？

每一件物品其实只有两个状态：取或者不取，所以可以使用回溯法搜索出所有的情况，那么时间复杂度就是 o(2 ^ n)，这里的 n 表示物品数量。
所以暴力的解法是指数级别的时间复杂度。进而才需要动态规划的解法来进行优化。

举一个例子：背包最大重量为 4，weight = [1, 3, 4]，value = [15, 20, 30]，问背包能背的最大价值是多少？

## 二维 dp 数组解决 0-1 背包
动态规划五部曲

### 确定 dp 数组及下标的含义
对于背包问题，有一种写法， 是使用二维数组，即 dp[i][j] 表示从下标为 [0 - i] 的物品里任意取，放进容量为 j 的背包，价值总和最大是多少。
可以看下图：

![](./images/bagsProblem-img2.png)

### 确定递推公式
再回顾一下 dp[i][j] 的含义：从下标为 [0 - i] 的物品里任意取，放进容量为 j 的背包，价值总和最大是多少。

那么可以有两个方向推出来 dp[i][j]：
- **不放物品 i**：由 dp[i - 1][j] 推出，即背包容量为 j，里面不放物品 i 的最大价值，此时 dp[i][j] 就是 dp[i - 1][j]。
  （其实就是当物品 i 的重量大于背包 j 的重量时，物品 i 无法放进背包中，所以背包内的价值依然和前面相同。)
- **放物品 i**：由 dp[i - 1][j - weight[i]] 推出，dp[i - 1][j - weight[i]] 为背包容量为 j - weight[i] 的时候不放物品 i 的最大价值，
  那么 dp[i - 1][j - weight[i]] + value[i] （物品 i 的价值），就是背包放物品 i 得到的最大价值

所以递归公式： dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - weight[i]] + value[i])

### dp 数组初始化
首先从 dp[i][j] 的定义出发，如果背包容量 j = 0 的话，即 dp[i][0]，无论是选取哪些物品，背包价值总和一定为 0。

再看其他情况。

状态转移方程 dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - weight[i]] + value[i])  可以看出 i 是由 i - 1 推导出来，
那么 i 为 0 的时候就一定要初始化。dp[0][j]，即：i = 0，只存放编号 0 的物品的时候，各个容量的背包所能存放的最大价值。
- 当 j < weight[0] 时，dp[0][j] = 0，因为背包容量比编号 0 的物品重量还小
- 当j  >= weight[0] 时，dp[0][j] = value[0]，因为背包容量放足够放编号 0 物品

初始化代码如下
```
// 当然这一步，如果把dp数组预先初始化为0了，这一步就可以省略
for (int j = 0 ; j < weight[0]; j++) {
    dp[0][j] = 0;
}
// 正序遍历
for (int j = weight[0]; j <= bagweight; j++) {
    dp[0][j] = value[0];
}
```

dp[0][j] 和 dp[i][0] 都已经初始化了，那么其他下标应该初始化多少呢？

其实从递归公式： dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - weight[i]] + value[i]); 可以看出dp[i][j] 是由左上方数值推导出来了，
那么其他下标初始为什么数值都可以，因为都会被覆盖。 初始 -1，初始 -2，初始 100，都可以。但只不过一开始就统一把 dp 数组统一初始为 0，
更方便一些。

此时 dp 数组情况如下图：

![](./images/bagsProblem-img3.png)

### 确定遍历顺序
在图中，可以看出，有两个遍历的维度：物品与背包重量。那么问题来了，先遍历 物品还是先遍历背包重量呢？

其实都可以！！ 但是先遍历物品更好理解。以下是先遍历物品、再遍历背包中两的代码：
```
// weight数组的大小 就是物品个数
for (int i = 1; i < weight.size(); i++) { // 遍历物品
    for (int j = 0; j <= bagweight; j++) { // 遍历背包容量
        if (j < weight[i]) {
            dp[i][j] = dp[i - 1][j];
        } else {
            dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - weight[i]] + value[i]);
        }
    }
}
```

先遍历背包，再遍历物品，也是可以的，代码如下：
```
// weight数组的大小 就是物品个数
for (int j = 0; j <= bagweight; j++) { // 遍历背包容量
    for (int i = 1; i < weight.size(); i++) { // 遍历物品
        if (j < weight[i]) {
            dp[i][j] = dp[i - 1][j];
        } else {
            dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - weight[i]] + value[i]);
        }
    }
}
```

为什么也是可以的呢？要理解递归的本质和递推的方向。

dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - weight[i]] + value[i])，递归公式中可以看出 dp[i][j] 是靠 dp[i-1][j] 和 dp[i - 1][j - weight[i]] 推导出来的。
dp[i - 1][j] 和 dp[i - 1][j - weight[i]] 都在 dp[i][j] 的左上角方向（包括正上方向），那么先遍历物品，再遍历背包的过程如图所示：

![](./images/bagsProblem-img4.png)

可以看出，虽然两个 for 循环遍历的次序不同，但是 dp[i][j] 所需要的数据就是左上角，根本不影响 dp[i][j] 公式的推导。

但先遍历物品再遍历背包这个顺序更好理解。

其实背包问题里，两个 for 循环的先后循序是非常有讲究的，理解遍历顺序其实比理解推导公式难多了。

### 举例推导 dp 数组
来看一下对应的dp数组的数值，如图：

![](./images/bagsProblem-img5.png)

最终结果就是 dp[2][4]。

# 代码

### go
```go
func main() {
    weight := []int{1, 3, 4}
    value := []int{15, 20, 30}
    fmt.Println(bagProblem1(weight, value, 4))
}

func bagProblem1(weight, value []int, bagweight int) int {
    // 定义dp数组
    dp := make([][]int, len(weight))
    for i, _ := range dp {
        dp[i] = make([]int, bagweight+1)
    }
    // 初始化
    for j := weight[0]; j <= bagweight; j++ {
        dp[0][j] = value[0]
    }
    // 递推公式
    for i := 1; i < len(weight); i++ {			// 遍历物品
        for j := 0; j <= bagweight; j++ {		// 遍历背包容量
            if j < weight[i] {
                dp[i][j] = dp[i - 1][j]
            } else {
                dp[i][j] = max(dp[i - 1][j], dp[i - 1][j - weight[i]] + value[i])
            }
        }
    }
    return dp[len(weight) - 1][bagweight]
}

func max(a, b int) int {
    if a > b {
        return a
    }
    return b
}
```