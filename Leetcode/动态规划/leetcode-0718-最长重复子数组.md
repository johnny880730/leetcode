# 718. 最长重复子数组

# 题目
给两个整数数组 A 和 B ，返回两个数组中公共的、长度最长的子数组的长度。

https://leetcode.cn/problems/maximum-length-of-repeated-subarray/

# 示例
```
输入：
A: [1,2,3,2,1]
B: [3,2,1,4,7]
输出：3
解释：
长度最长的公共子数组是 [3, 2, 1] 。
```

提示：
- 1 <= len(A), len(B) <= 1000
- 0 <= A[i], B[i] < 100

# 解析
注意题目中说的子数组，其实就是连续子序列。

要求两个数组中最长重复子数组，如果是暴力的解法，需要先两层 for 循环确定两个数组起始位置，然后再来一个循环，来从两个起始位置开始比较，
取得重复子数组的长度。

本题其实是动规解决的经典题目，只要想到用二维数组可以记录两个字符串的所有比较情况，这样就比较好推递推公式了。

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
dp[i][j] ：以 A[i - 1] 为结尾，和 B[j - 1]为结尾，最长重复子数组长度为 dp[i][j]。

那么 dp[0][0] 是什么含义呢？总不能是以下 A[-1] 为结尾吧。其实 dp[i][j] 的定义也就决定着，在遍历 dp[i][j] 的时候，i 和 j 都要从 1 开始。

那如果就定义 dp[i][j] 为 A[i] 结尾，和 B[j] 为结尾的最长重复子数组长度。不行么？行倒是行！但实现起来就麻烦一点，需要单独处理初始化部分。

### 确定递推公式
根据 dp[i][j] 的定义，dp[i][j] 的状态只能由 dp[i - 1][j - 1] 推导出来。

即当 A[i - 1] == B[j - 1] 时，dp[i][j] = dp[i - 1][j - 1] + 1;

根据递推公式可以看出，遍历 i 和 j 要从 1 开始

### dp 数组初始化
根据 dp[i][j] 的定义，dp[i][0] 和 dp[0][j] 其实都是没有意义的！

但 dp[i][0] 和 dp[0][j] 要初始值，因为为了方便递归公式 dp[i][j] = dp[i - 1][j - 1] + 1，所以 dp[i][0] = dp[0][j] = 0。

举个例子 A[0] 如果和 B[0] 相同的话，dp[1][1] = dp[0][0] + 1，只有 dp[0][0] 初始为0，正好符合递推公式逐步累加起来。

### 确定遍历顺序
外层循环遍历 A，内层循环遍历B，或者先遍历 B 再遍历 A 都可以。

同时题目要求长度最长的子数组的长度。所以在遍历的时候顺便把  dp[i][j] 的最大值记录下来。

### 举例推导 dp 数组
拿示例 1 中，A = [1, 2, 3, 2, 1]，B = [3, 2, 1, 4, 7] 为例，画一个 dp 数组的状态变化，如下：

![](./images/leetcode-0718-img1.png)


时间复杂度：O(n × m)，n 为 A 长度，m 为 B 长度

空间复杂度：O(n × m)


# 代码

### php
```php
class Leetcode0718 {
    
    public function findLength($nums1, $nums2) {
        $len1 = count($nums1);
        $len2 = count($nums2);
        $res = 0;
        $dp = array_fill(0, $len1 + 1, array_fill(0, $len2 + 1, 0));
        for ($i = 1; $i <= $len1; $i++) {
            for ($j = 1; $j <= $len2; $j++) {
                if ($nums1[$i - 1] == $nums2[$j - 1]) {
                    $dp[$i][$j] = $dp[$i - 1][$j - 1] + 1;
                }
                if ($dp[$i][$j] > $res) {
                    $res = $dp[$i][$j];
                }
            }
        }
        return $res;
    }

}
```

### go
```go
func findLength(nums1 []int, nums2 []int) int {
    size1, size2 := len(nums1), len(nums2)
    dp := make([][]int, size1 + 1)
    for k, _ := range dp {
        dp[k] = make([]int, size2 + 1)
    }
    res := 0
    for i := 1 ; i <= size1; i++ {
        for j := 1; j <= size2; j++ {
            if nums1[i - 1] == nums2[j - 1] {
                dp[i][j] = dp[i - 1][j - 1] + 1
            }
            if dp[i][j] > res {
                res = dp[i][j]
            }
        }
    }
    return res
}
```

### 拓展
前面讲了 dp 数组为什么定义：以 A[i - 1]为结尾，和以 B[j - 1]为结尾的最长重复子数组长度为 dp[i][j]。

那我直接定义dp[i][j] 为 A[i]为结尾的和以 B[j] 为结尾的最长重复子数组长度。不行么？

当然可以，就是实现起来麻烦一些。

如果这么定义的话，那么第一行和第一列毕竟要进行初始化，如果 nums1[i] 与 nums2[0] 相同的话，对应的 dp[i][0] 就要初始为1，
因为此时最长重复子数组为1。 nums2[j] 与 nums1[0]相同的话，同理。

代码如下：

```go
// go
func findLength(nums1 []int, nums2 []int) int {
    size1, size2 := len(nums1), len(nums2)
    dp := make([][]int, size1 + 1)
    for k, _ := range dp {
        dp[k] = make([]int, size2 + 1)
    }
    // 要对第一行，第一列经行初始化
    for i := 0; i < size1; i++ {
        if nums1[i] == nums2[0] {
            dp[i][0] = 1
        }
    }
    for j := 0; j < size2; j++ {
        if nums2[j] == nums1[0] {
            dp[0][j] = 1
        }
    }
    res := 0
    for i := 0 ; i < size1; i++ {
        for j := 0; j < size2; j++ {
            if nums1[i] == nums2[j] && i > 0 && j > 0 {
                dp[i][j] = dp[i - 1][j - 1] + 1  // 防止 i-1 出现负数
            }
            if dp[i][j] > res {
                res = dp[i][j]
            }
        }
    }
    return res
}
```