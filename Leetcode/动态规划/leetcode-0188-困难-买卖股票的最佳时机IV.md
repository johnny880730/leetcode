# 0188. 买卖股票的最佳时机 IV

# 题目
给定一个整数数组 prices ，它的第 i 个元素 prices[i] 是一支给定的股票在第 i 天的价格。

设计一个算法来计算你所能获取的最大利润。你最多可以完成 k 笔交易。

注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。

https://leetcode.cn/problems/best-time-to-buy-and-sell-stock-iv/description/

提示：
- 0 <= k <= 100
- 0 <= prices.length <= 1000
- 0 <= prices[i] <= 1000


# 示例：
```
输入：k = 2, prices = [2,4,1]
输出：2
解释：在第 1 天 (股票价格 = 2) 的时候买入，在第 2 天 (股票价格 = 4) 的时候卖出，这笔交易所能获得利润 = 4-2 = 2 。
```

```
输入：k = 2, prices = [3,2,6,5,0,3]
输出：7
解释：在第 2 天 (股票价格 = 2) 的时候买入，在第 3 天 (股票价格 = 6) 的时候卖出, 这笔交易所能获得利润 = 6-2 = 4 。
     随后，在第 5 天 (股票价格 = 0) 的时候买入，在第 6 天 (股票价格 = 3) 的时候卖出, 这笔交易所能获得利润 = 3-0 = 3 。
```


# 解析

## 动态规划
《123. 买卖股票的最佳时机 III》 的进阶版

### 定义 dp 数组
dp[i][j]：i 表示第 i 天，j 表示状态，dp[i][j] 表示第 i 天的最大现金

j 的状态如下：
- 0 = 不操作
- 1 = 第一次持有
- 2 = 第一次不持有
- 3 = 第二次持有
- 4 = 第二次不持有

以此类推。**除了 0，偶数表示卖出，奇数表示买入。**

题目要求至多 k 笔交易，所以 j 的范围就是 [0, 2k]

### 确定递推公式
dp[i][1] 状态，有两个操作，取最大：
- 第 i 天没有操作，沿用前一天持有的状态，dp[i][1] = dp[i - 1][1]
- 第 i 天买入股票，dp[i][1] = dp[i - 1][0] - prices[i]
   
dp[i][2] 状态，也有两个操作，取最大：
- 第 i 天没有操作，沿用前一天不持有的状态，dp[i][2] = dp[i - 1][2]
- 第 i 天卖出股票，dp[i][2] = dp[i - 1][1] + prices[i]

同理可以类比剩下的状态

本题和 《123.买卖股票的最佳时机III》 最大的区别就是这里要类比 j 为奇数是买，偶数是卖的状态。

### 初始化
第 0 天没有操作 dp[0][0] = 0

第 0 天第一次买入的操作，dp[0][1] = -prices[0]

第 0 天第一次卖出的操作，dp[0][2] = 0

《123. 买卖股票的最佳时机 III》里已经提过，第 0 天第二次买入的操作，dp[0][3] = -prices[0]

同理可以推导当 j 为奇数的时候，dp[0][j] = -prices[0]

在初始化的地方同样要类比 j 为偶数是卖、奇数是买的状态。

### 遍历顺序
从递推公式可以看出，一定是从前向后遍历


# 代码

### php
```php

class LeetCode0188 {

    public function maxProfit($k, $prices) {
        $len = count($prices);
        if ($len == 0) {
            return 0;
        }
        $dp = array_fill(0, $len, array_fill(0, 2 * $k + 1, 0));
        for ($j = 0; $j < 2 * $k; $j += 2) {
            $dp[0][$j + 1] = -$prices[0];
        }
        for ($i = 1; $i < $len; $i++) {
            for ($j = 0; $j < 2 * $k; $j += 2) {
                $dp[$i][$j + 1] = max($dp[$i - 1][$j + 1], $dp[$i - 1][$j] - $prices[$i]);
                $dp[$i][$j + 2] = max($dp[$i - 1][$j + 2], $dp[$i - 1][$j + 1] + $prices[$i]);
            }
        }
        return $dp[$len - 1][2 * $k];
    }

}
```

### go
```go
func maxProfit(k int, prices []int) int {
    size := len(prices)
    dp := make([][]int, size)
    for kk, _ := range dp {
        dp[kk] = make([]int, 2 * k + 1)
    }
    for j := 0; j < 2 * k; j += 2 {
        dp[0][j + 1] = -prices[0]
    }
    for i := 1; i < size; i++ {
        for j := 0; j < 2 * k; j += 2 {
            dp[i][j + 1] = max(dp[i - 1][j + 1], dp[i - 1][j] - prices[i])
            dp[i][j + 2] = max(dp[i - 1][j + 2], dp[i - 1][j + 1] + prices[i])
        }
    }
    return dp[size - 1][2 * k]

}

func max(a, b int) int {
    if a > b {
        return a
    }
    return b
}
```

### java
```java
class LeetCode0188 {

    public int maxProfit(int k, int[] prices) {
        int len = prices.length;
        if (len == 0) {
            return 0;
        }
        int[][] dp = new int[len][2 * k + 1];
        for (int j = 0; j < 2 * k; j += 2) {
            dp[0][j + 1] = -prices[0];
        }
        for (int i = 1; i < len; i++) {
            for (int j = 0; j < 2 * k; j += 2) {
                dp[i][j + 1] = Math.max(dp[i - 1][j + 1], dp[i - 1][j] - prices[i]);
                dp[i][j + 2] = Math.max(dp[i - 1][j + 2], dp[i - 1][j + 1] + prices[i]);
            }
        }
        return dp[len - 1][2 * k];
    }
}
```