# 0139. 单词拆分

# 题目
给你一个字符串 s 和一个字符串列表 wordDict 作为字典。请你判断是否可以利用字典中出现的单词拼接出 s 。
注意：不要求字典中出现的单词全部都使用，并且字典中的单词可以重复使用。

https://leetcode.cn/problems/word-break/description/

提示：
- 1 <= s.length <= 300
- 1 <= wordDict.length <= 1000
- 1 <= wordDict[i].length <= 20
- s 和 wordDict[i] 仅有小写英文字母组成
- wordDict 中的所有字符串 互不相同

# 示例：
```
输入: s = "leetcode", wordDict = ["leet", "code"]
输出: true
解释: 返回 true 因为 "leetcode" 可以由 "leet" 和 "code" 拼接成。
```

```
输入: s = "applepenapple", wordDict = ["apple", "pen"]
输出: true
解释: 返回 true 因为 "applepenapple" 可以由 "apple" "pen" "apple" 拼接成。
     注意，你可以重复使用字典中的单词。
```

```
输入: s = "catsandog", wordDict = ["cats", "dog", "sand", "and", "cat"]
输出: false
```



# 解析
单词就是物品，字符串 s 就是背包，单词能否组成字符串s，就是问物品能不能把背包装满。拆分时可以重复使用字典中的单词，说明就是一个完全背包！

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
dp[i] : 假设字符串长度为 i ，如果 dp[i] == true，表示可以拆分为一个或多个在字典中出现的单词。

### 确定递推公式
如果确定 dp[j] == true，且 区间 [j, i] 的子串出现在字典里，那么 dp[i] = true。（前提 j < i ）。

所以递推公式是 if ( 区间 [j, i] 的子串出现在字典里 && dp[j] == true) 那么 dp[i] = true。

### dp 数组初始化
从递推公式中可以看出，dp[i] 的状态依靠 dp[j] 是否为 true，那么 dp[0] 就是递推的根基，dp[0] 一定要为 true，否则递推下去后面都都是 false 了。

那么 dp[0] 有没有意义呢？dp[0] 表示如果字符串为空的话，说明出现在字典里。但题目中说了“给定一个非空字符串 s”，
所以测试数据中不会出现 i = 0 的情况，那么 dp[0] 初始为 true 完全就是为了推导公式。

下标非 0 的 dp[i] 初始化为 false，只要没有被覆盖说明都是不可拆分为一个或多个在字典中出现的单词。

### 确定遍历顺序
题目中说是拆分为一个或多个在字典中出现的单词，所以这是完全背包。还要讨论两层for循环的前后顺序。
- **如果求组合数，就是外层遍历物品，内层遍历背包。**
- **如果求排列数，就是外层遍历背包，内层遍历物品。**

本题其实求的是排列数，为什么呢。 拿 s = "applepenapple", wordDict = ["apple", "pen"] 举例。

"apple", "pen" 是物品，那么要求物品的组合一定是 "apple" + "pen" + "apple" 才能组成 "applepenapple"。

"apple" + "apple" + "pen" 或者 "pen" + "apple" + "apple" 是不可以的，那么就是强调物品之间顺序。

所以说，本题一定是先遍历背包，再遍历物品。

### 举例推导 dp
以输入: s = "leetcode", wordDict = ["leet", "code"]为例，dp 状态如图：

![](./images/leetcode-0139-img1.png)


# 代码

### php
```php
class Leetcode0139 {

    function wordBreak($s, $wordDict) {
        $hashMap = [];
        foreach ($wordDict as $item) {
            $hashMap[$item] = 1;
        }
        $size = strlen($s);
        $dp = array_fill(0, $size + 1, false);
        $dp[0] = true;
        for ($i = 1; $i <= $size; $i++) {
            for ($j = 0; $j < $i; $j++) {
                $word = substr($s, $j, $i - $j);
                if (isset($hashMap[$word]) && $dp[$j] == true) {
                    $dp[$i] = true;
                }
            }
        }
        return $dp[$size];
    }

}
```

### go
```go
func wordBreak(s string, wordDict []string) bool {
    hashMap := make(map[string]int)
    for _, word := range wordDict {
        hashMap[word] = 1
    }
    length := len(s)
    dp := make([]bool, length + 1)
    dp[0] = true
    for i := 1; i <= length; i++ {
        for j := 0; j < i; j++ {
            word := Substr(s, j, i - j)
            _, ok := hashMap[word]
            if ok && dp[j] == true{
                dp[i] = true
            }
        }
    }
    return dp[length]
}

// Substr ： 截取字符串
func Substr(str string, start int, length int) string {
    rs := []rune(str)
    rl := len(rs)
    end := 0

    if start < 0 {
        start = rl - 1 + start
    }
    end = start + length

    if start > end {
        start, end = end, start
    }

    if start < 0 {
        start = 0
    }
    if start > rl {
        start = rl
    }
    if end < 0 {
        end = 0
    }
    if end > rl {
        end = rl
    }
    return string(rs[start:end])
}
```

### java
```java
class LeetCode0139 {

    public boolean wordBreak(String s, List<String> wordDict) {
        HashMap<String, Boolean> map = new HashMap<>();
        for (String word : wordDict) {
            map.put(word, true);
        }
        boolean[] dp = new boolean[s.length() + 1];
        dp[0] = true;
        for (int i = 1; i <= s.length(); i++) {
            for (int j = 0; j < i; j++) {
                String w = s.substring(j, i);
                if (map.containsKey(w) && dp[j] == true) {
                    dp[i] = true;
                }
            }
        }
        return dp[s.length()];
    }
}
```
