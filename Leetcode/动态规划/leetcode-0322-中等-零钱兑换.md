# 0322. 零钱兑换

# 题目
给你一个整数数组 coins ，表示不同面额的硬币；以及一个整数 amount ，表示总金额。
计算并返回可以凑成总金额所需的 最少的硬币个数 。如果没有任何一种硬币组合能组成总金额，返回 -1 。
你可以认为每种硬币的数量是无限的。

https://leetcode.cn/problems/coin-change/description/

提示：
- 1 <= coins.length <= 12
- 1 <= coins[i] <= 2^31 - 1
- 0 <= amount <= 10^4

# 示例：
```
输入：coins = [1, 2, 5], amount = 11
输出：3
解释：11 = 5 + 5 + 1
```

```
输入：coins = [2], amount = 3
输出：-1
```

```
输入：coins = [1], amount = 0
输出：0
```

```
输入：coins = [1], amount = 1
输出：1
```

```
输入：coins = [1], amount = 2
输出：2
```



# 解析

题目中说每种硬币的数量是无限的，可以看出是典型的完全背包问题。

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
dp[j]：凑足总额为 j 所需钱币的最少个数为 dp[j]

### 确定递推公式
凑足总额为 j - coins[i] 的最少个数为 dp [j - coins[i]]，那么只需要加上一个钱币 coins[i] 即 dp [j - coins[i]] + 1 = dp [j]

所以 dp [j] 要取所有  dp [j - coins[i]] + 1 中最小的。递推公式： dp [j] = min(dp[j - coins[i]] + 1, dp[j])

### dp 数组如何初始化
首先凑足总金额为 0 所需钱币的个数一定是 0，那么 dp[0] = 0。其他下标对应的数值呢？

考虑到递推公式的特性， dp[j] 必须初始化为一个最大的数，否则就会在 min(dp[j - coins[i]] + 1, dp[j]) 比较的过程中被初始值覆盖。
所以下标非零的元素都是应该是最大值。

### 确定遍历顺序
本题求钱币最小个数，那么钱币有顺序和没有顺序都可以，都不影响钱币的最小个数。所以本题并不强调集合是组合还是排列。但是：
- **如果求组合数，就是外层遍历物品，内层遍历背包。**
- **如果求排列数，就是外层遍历背包，内层遍历物品。**

本题的两个 for 循环的关系是：外层循环遍历物品 + 内层遍历背包 或者 外层遍历背包 + 内层循环遍历物品都是可以的。
这里采用 coins 放在外循环，target 在内循环的方式。

本题钱币数量可以无限使用，那么是完全背包。所以遍历的内循环是正序

综上所述，遍历顺序为：coins（物品）放在外循环，target（背包）在内循环。且内循环正序。

### 举例推导 dp 数组
以输入：coins = [1, 2, 5], amount = 5 为例

![](./images/leetcode-0322-img1.png)


# 代码

### php
```php
class LeetCode0322 {
    
    public function coinChange($coins, $amount) {
        $dp = array_fill(0, $amount + 1, PHP_INT_MAX);
        $dp[0] = 0;
        for ($i = 0; $i < count($coins); $i++) {
            for ($j = $coins[$i]; $j <= $amount; $j++) {
                if ($dp[$j - $coins[$i]] != PHP_INT_MAX) {  // 如果dp[j - coins[i]]是初始值则跳过
                    $dp[$j] = min($dp[$j - $coins[$i]] + 1, $dp[$j]);
                }
            }
        }
        if ($dp[$amount] == PHP_INT_MAX) {
            return -1;
        }
        return $dp[$amount];
    }

}
```

### go
```go
func coinChange(coins []int, amount int) int {
    length := len(coins)
    dp := make([]int, amount + 1)
    for i := 0; i <= amount; i++ {
        dp[i] = math.MaxInt32
    }
    dp[0] = 0
    for i := 0; i < length; i++ {
        for j := coins[i]; j <= amount; j++ {
            if dp[j - coins[i]] != math.MaxInt32 {
				dp[j] = min(dp[j], dp[j - coins[i]] + 1)
            }
        }
    }
    if dp[amount] == math.MaxInt32 {
        return -1
    }
    return dp[amount]
}

func min(a, b int) int {
	if a < b {
		return a
    }
	return b
}
```

### java
```java
class Leetcode0322 {
    
    public int coinChange(int[] coins, int amount) {
        int[] dp = new int[amount + 1];
        for (int i = 1; i < dp.length; i++) {
            dp[i] = Integer.MAX_VALUE;
        }
        for (int i = 0; i < coins.length; i++) {
            for (int j = coins[i]; j <= amount; j++) {
                if (dp[j - coins[i]] != Integer.MAX_VALUE) {
                    dp[j] = Math.min(dp[j - coins[i]] + 1, dp[j]);
                }
            }
        }
        if (dp[amount] == Integer.MAX_VALUE) {
            return -1;
        }
        return dp[amount];

    }
}
```