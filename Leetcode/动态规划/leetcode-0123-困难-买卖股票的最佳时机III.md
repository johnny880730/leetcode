# 0123. 买卖股票的最佳时机 III

# 题目 
给定一个数组，它的第 i 个元素是一支给定的股票在第 i 天的价格。

设计一个算法来计算你所能获取的最大利润。你最多可以完成 两笔 交易。

注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。

https://leetcode.cn/problems/best-time-to-buy-and-sell-stock-iii/description/

提示：
- 1 <= prices.length <= 10<sup>5</sup>
- 0 <= prices[i] <= 10<sup>5</sup>

# 示例:
```
输入：prices = [3,3,5,0,0,3,1,4]
输出：6
解释：在第 4 天（股票价格 = 0）的时候买入，在第 6 天（股票价格 = 3）的时候卖出，这笔交易所能获得利润 = 3-0 = 3 。
     随后，在第 7 天（股票价格 = 1）的时候买入，在第 8 天 （股票价格 = 4）的时候卖出，这笔交易所能获得利润 = 4-1 = 3 。
```

```
输入：prices = [1,2,3,4,5]
输出：4
解释：在第 1 天（股票价格 = 1）的时候买入，在第 5 天 （股票价格 = 5）的时候卖出, 这笔交易所能获得利润 = 5-1 = 4 。
     注意你不能在第 1 天和第 2 天接连购买股票，之后再将它们卖出。
     因为这样属于同时参与了多笔交易，你必须在再次购买前出售掉之前的股票。
```

```
输入：prices = [7,6,4,3,1]
输出：0
解释：在这个情况下, 没有交易完成, 所以最大利润为 0。
```

```
输入：prices = [1]
输出：0
```



# 解析

## 动态规划

### 定义 dp 数组
一天一共有五个状态：
- 状态 0：没有操作
- 状态 1：第一次持有
- 状态 2：第一次不持有
- 状态 3：第二次持有
- 状态 4：第二次不持有

dp[i][j]：i 表示第 i 天，j 为 0 ~ 4 的五个状态，dp[i][j] 表示第 i 天状态 j 所剩的最大现金

需要注意：dp[i][1]，表示的是第 i 天，持有股票的状态，并不是说一定要第 i 天买入股票，这是容易陷入的误区。

### 确定递推公式
dp[i][0]，表示第 i 天不操作的状态，那么就是延续前一天的数据：dp[i][0] = dp[i - 1][0]

dp[i][1]，表示第 i 天持有股票的状态，有两个具体操作，选最大的：
- 第 i 天没有操作，那么 dp[i][1] = dp[i - 1][1]
- 第 i 天买入股票，那么 dp[i][1] = dp[i - 1][0] - prices[i]

dp[i][2]，也有两个操作，选最大的：
- 第 i 天卖出股票，那么 dp[i][2] = dp[i - 1][1] + prices[i]
- 第 i 天没有操作，沿用前一天的卖出股票的状态，那么 dp[i][2] = dp[i - 1][2]

同理可推导出剩下的状态：

dp[i][3] = max(dp[i - 1][3], dp[i - 1][2] - prices[i])

dp[i][4] = max(dp[i - 1][4], dp[i - 1][3] + prices[i])

### dp 数组初始化
第 0 天没有操作，dp[0][0] = 0

第 0 天执行第一次买入，dp[0][1] = -prices[0]

第 0 天做第一次卖出的操作，这个初始值应该是多少呢？此时还没有买入，怎么就卖出呢？ 其实可以理解当天买入，当天卖出，所以 dp[0][2] = 0

第 0 天第二次买入操作，初始值应该是多少呢？第一次还没买入呢，怎么初始化第二次买入呢？第二次买入依赖于第一次卖出的状态，
其实相当于第 0 天第一次买入了，第一次卖出了，然后再买入一次（第二次买入），那么现在手头上没有现金，只要买入，现金就做相应的减少。
所以第二次买入操作，初始化为：dp[0][3] = -prices[0]

同理第二次卖出初始化 dp[0][4] = 0

### 确定遍历顺序
从递推公式可以看出一定是从前向后遍历

### 举例推导 dp 数组
以输入 [1, 2, 3, 4, 5] 为例

![](./images/leetcode-0123-img1.png)
 

# 代码

### php
```php
class LeetCode0123 {
    
    public function maxProfit($prices) {
        $len = count($prices);
        $dp = array_fill(0, $len, array_fill(0, 5, 0));
        $dp[0][1] = -$prices[0];
        $dp[0][3] = -$prices[0];
        for ($i = 1; $i < $len; $i++) {
            $dp[$i][0] = $dp[$i - 1][0];
            $dp[$i][1] = max($dp[$i - 1][1], $dp[$i - 1][0] - $prices[$i]);
            $dp[$i][2] = max($dp[$i - 1][2], $dp[$i - 1][1] + $prices[$i]);
            $dp[$i][3] = max($dp[$i - 1][3], $dp[$i - 1][2] - $prices[$i]);
            $dp[$i][4] = max($dp[$i - 1][4], $dp[$i - 1][3] + $prices[$i]);
        }
        return $dp[$len - 1][4];
    }

}
```

### go
```go
func maxProfit(prices []int) int {
    size := len(prices)
    dp := make([][]int, size)
    for k, _ := range dp {
        dp[k] = make([]int, 5)
    }
    dp[0][1] = -prices[0]
    dp[0][3] = -prices[0]
    for i := 1; i < size; i++ {
        dp[i][0] = dp[i - 1][0]
        dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] - prices[i])
        dp[i][2] = max(dp[i - 1][2], dp[i - 1][1] + prices[i])
        dp[i][3] = max(dp[i - 1][3], dp[i - 1][2] - prices[i])
        dp[i][4] = max(dp[i - 1][4], dp[i - 1][3] + prices[i])
    }
    return dp[size - 1][4]
}

func max(a, b int) int {
    if a > b {
        return a
    }
    return b
}
```

### java
```java
class LeetCode0123 {

    public int function maxProfit(int[] prices) {
        int len = prices.length;
        int[][] dp = new int[len][5];
        dp[0][1] = -prices[0];
        dp[0][3] = -prices[0];
        for (int i = 1; i < len; i++) {
            dp[i][0] = dp[i - 1][0];
            dp[i][1] = Math.max(dp[i - 1][1], dp[i - 1][0] - prices[i]);
            dp[i][2] = Math.max(dp[i - 1][2], dp[i - 1][1] + prices[i]);
            dp[i][3] = Math.max(dp[i - 1][3], dp[i - 1][2] - prices[i]);
            dp[i][4] = Math.max(dp[i - 1][4], dp[i - 1][3] + prices[i]);
        }
        return dp[len - 1][4];
    }
}
```