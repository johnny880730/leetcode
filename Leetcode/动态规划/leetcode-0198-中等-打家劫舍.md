# 0198. 打家劫舍

# 题目
你是一个专业的小偷，计划偷窃沿街的房屋。每间房内都藏有一定的现金，影响你偷窃的唯一制约因素就是相邻的房屋装有相互连通的防盗系统，
如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警。

给定一个代表每个房屋存放金额的非负整数数组，计算你 不触动警报装置的情况下 ，一夜之内能够偷窃到的最高金额。

https://leetcode.cn/problems/house-robber/description/

提示：
- 1 <= nums.length <= 100
- 0 <= nums[i] <= 400

# 示例
```
输入：[1,2,3,1]
输出：4
解释：偷窃 1 号房屋 (金额 = 1) ，然后偷窃 3 号房屋 (金额 = 3)。
    偷窃到的最高金额 = 1 + 3 = 4 。
```
```
输入：[2,7,9,3,1]
输出：12
解释：偷窃 1 号房屋 (金额 = 2), 偷窃 3 号房屋 (金额 = 9)，接着偷窃 5 号房屋 (金额 = 1)。
    偷窃到的最高金额 = 2 + 9 + 1 = 12 。
```

# 解析

当前房屋偷与不偷取决于 前一个房屋和前两个房屋是否被偷了。

所以这里就感觉到，当前状态和前面状态会有一种依赖关系，那么这种依赖关系都是动规的递推公式。

以上是大概思路，打家劫舍是 dp 解决的经典问题。

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
dp[i]：考虑下标 i （包括 i ）以内的房屋，最多可以偷窃的金额为 dp[i]。

### 确定递推公式
决定 dp[i]的 因素就是第 i 房间偷还是不偷：
- 如果偷，那么 dp[i] = dp[i - 2] + nums[i] ，即：第 i - 1 房一定是不考虑的，找出 i - 2（包括 i - 2）以内的房屋， 
  最多可以偷窃的金额为 dp[i - 2] 加上第 i 房间偷到的钱
- 如果不偷，那么dp[i] = dp[i - 1]，即考虑 i - 1 房，（注意这里是考虑，并不是一定要偷 i - 1 房）

然后 dp[i] 取最大值，即 dp[i] = max(dp[i - 2] + nums[i], dp[i - 1]);

### dp 数组初始化
从递推公式 dp[i] = max(dp[i - 2] + nums[i], dp[i - 1]) 可以看出，递推公式的基础就是 dp[0] 和 dp[1]

从 dp[i] 的定义上来讲，dp[0] 一定是 nums[0]，dp[1]就是 nums[0] 和 nums[1] 的最大值即：dp[1] = max(nums[0], nums[1]);

### 确定遍历顺序
dp[i] 是根据 dp[i - 2] 和 dp[i - 1] 推导出来的，那么一定是从前到后遍历

举例推导dp数组
以示例二，输入[2, 7, 9, 3, 1]为例。

![](./images/leetcode-0198-img1.png)

时间复杂度: O(n)

空间复杂度: O(n)


# 代码

### php
```php
class LeetCode0198 {

    function rob($nums) {
        if (!$nums) {
            return 0;
        }
        $n = count($nums);
        if ($n == 1) {
            return $nums[0];
        }
        if ($n == 2) {
            return max($nums);
        }
        $dp = array_fill(0, $n, 0);
        $dp[0] = $nums[0];
        $dp[1] = max($nums[0], $nums[1]);
        for ($i = 2; $i < $n; $i++) {
            $dp[$i] = max($dp[$i - 1], $nums[$i] + $dp[$i - 2]);
        }
        return $dp[$n - 1];
        
    }
}
```

### java
```java
class LeetCode0198 {
    
    public int rob(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        if (nums.length == 1) {
            return nums[0];
        }
        if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        }
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        dp[1] = Math.max(nums[0], nums[1]);
        for (int i = 2; i < nums.length; i++) {
            dp[i] = Math.max(dp[i - 1], dp[i - 2] + nums[i]);
        }
        return dp[nums.length - 1];
    }
}
```

### go
```go
func rob(nums []int) int {
    size := len(nums)
    if size == 0 {
        return 0
    }
    if size == 1 {
        return nums[0]
    }
    if size == 2 {
		return max(nums[0], nums[1])
    }
    dp := make([]int, size)
    dp[0] = nums[0]
	dp[1] = max(nums[0], nums[1])
    for i := 2; i < size ; i++ {
		dp[i] = max(dp[i - 1], dp[i - 2] + nums[i])
    }
    return dp[size - 1]
}

func max(a, b int) int {
	if a > b {
		return a
    }
	return b
}
```

