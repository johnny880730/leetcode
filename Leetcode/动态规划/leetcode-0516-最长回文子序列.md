# 516. 最长回文子序列

# 题目
给你一个字符串 s ，找出其中最长的回文子序列，并返回该序列的长度。

子序列定义为：不改变剩余字符顺序的情况下，删除某些字符或者不删除任何字符形成的一个序列。

https://leetcode.cn/problems/longest-palindromic-subsequence


提示：
- 1 <= s.length <= 1000
- s 由小写英文字母组成

# 示例：
```
输入：s = "bbbab"
输出：4
解释：一个可能的最长回文子序列为 "bbbb" 。
```
```
输入：s = "cbbd"
输出：2
解释：一个可能的最长回文子序列为 "bb" 。
```



# 解析
《647. 回文子串》 求的是回文子串，而本题要求的是回文子序列， 要搞清楚这两者之间的区别。

回文子串是要连续的，回文子序列可不是连续的。 回文子串，回文子序列都是动态规划经典题目。

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
dp[i][j]：字符串 s 在 [i, j] 范围内最长的回文子序列的长度为 dp[i][j]。

### 确定递推公式
在判断回文子串的题目中，关键逻辑就是看 s[i] 与 s[j] 是否相同。

如果 s[i] 与 s[j] 相同，那么dp[i][j] = dp[i + 1][j - 1] + 2;

![](./images/leetcode-0516-img1.png)

如果 s[i] 与 s[j] 不相同，说明 s[i] 和 s[j] 的同时加入 并不能增加 [i, j] 区间回文子序列的长度，那么分别加入s[i]、s[j]
看看哪一个可以组成最长的回文子序列。
- 加入 s[j] 的回文子序列长度为 dp[i + 1][j]。
- 加入 s[i] 的回文子序列长度为 dp[i][j - 1]。

那么 dp[i][j] 一定是取最大的，即：dp[i][j] = max(dp[i + 1][j], dp[i][j - 1]);

![](./images/leetcode-0516-img2.png)

### dp 数组初始化
首先要考虑当 i 和 j 相同的情况，从递推公式：dp[i][j] = dp[i + 1][j - 1] + 2 可以看出递推公式是计算不到 i 和 j 相同时候的情况。

所以需要手动初始化一下，当 i == j 时，dp[i][j] = 1，即一个字符的回文子序列长度就是 1。

其他情况 dp[i][j] 初始为 0 就行，这样递推公式：dp[i][j] = max(dp[i + 1][j], dp[i][j - 1]) 中 dp[i][j] 才不会被初始值覆盖。

### 确定遍历顺序
从递归公式中，可以看出，dp[i][j] 依赖于 dp[i + 1][j - 1] ，dp[i + 1][j] 和 dp[i][j - 1]。

所以遍历 i 的时候一定要从下到上遍历，这样才能保证下一行的数据是经过计算的。j 的话，可以正常从左向右遍历。

### 举例推导 dp 数组
输入 s = "cbbd" 为例，dp 数组状态如图：

![](./images/leetcode-0516-img3.png)

时间复杂度: O(n^2)

空间复杂度: O(n^2)

# 代码

### php
```php
class LeetCode0516 {

    public function longestPalindromeSubseq($s) {
        $len = strlen($s);
        $dp = array_fill(0, $len, array_fill(0, $len, 0));
        for ($i = 0; $i < $len; $i++) {
            $dp[$i][$i] = 1;
        }
        for ($i = $len - 1; $i >= 0; $i--) {
            for ($j = $i + 1; $j < $len; $j++) {
                if ($s[$i] == $s[$j]) {
                    $dp[$i][$j] = $dp[$i + 1][$j - 1] + 2;
                } else {
                    $dp[$i][$j] = max($dp[$i + 1][$j], $dp[$i][$j - 1]);
                }
            }
        }
        return $dp[0][$len - 1];
    }

}

```

### go
```go
func longestPalindromeSubseq(s string) int {
    size := len(s)
    dp := make([][]int, size)
    for k, _ := range dp {
        dp[k] = make([]int, size)
    }
    for i := 0; i < size; i++ {
        dp[i][i] = 1
    }
    for i := size - 1; i >= 0; i-- {
        for j := i + 1; j < size; j++ {
            if s[i] == s[j] {
                dp[i][j] = dp[i + 1][j - 1] + 2
            } else {
                dp[i][j] = max(dp[i + 1][j], dp[i][j - 1])
            }
        }
    }
    return dp[0][size - 1]
}

func max(a, b int) int {
    if a > b {
        return a
    }
    return b
}
```

### java
```java
public int longestPalindromeSubseq(String s) {
    int len = s.length();
    int[][] dp = new int[len][len];
    for (int i = 0; i < len; i++) {
        dp[i][i] = 1;
    }
    for (int i = len - 1; i >= 0; i--) {
        for (int j = i + 1; j < len; j++) {
            if (s.charAt(i) == s.charAt(j)) {
                dp[i][j] = dp[i + 1][j - 1] + 2;
            } else {
                dp[i][j] = Math.max(dp[i + 1][j], dp[i][j - 1]);
            }
        }
    }
    return dp[0][len - 1];
}
```

