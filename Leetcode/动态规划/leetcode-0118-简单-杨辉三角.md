# 118. 杨辉三角

# 题目
给定一个非负整数 numRows，生成「杨辉三角」的前 numRows 行。

在「杨辉三角」中，每个数是它左上方和右上方的数的和。

https://leetcode.cn/problems/pascals-triangle/

# 示例
```
输入: numRows = 5
输出: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
```


# 代码

```php
class LeetCode0118 {

    function generate($numRows) {
        $res = [];
        for ($i = 1; $i <= $numRows; $i++) {
            $cur = [];
            if ($i == 1) {
                $cur = [1];
            } else if ($cur == 2) {
                $cur = [1, 1];
            } else {
                for ($j = 0; $j < $i; $j++) {
                    if ($j == 0 || $j == $i - 1) {
                        $cur[] = 1;
                    } else {
                        $cur[] = $res[$i - 2][$j - 1] + $res[$i - 2][$j];
                    }
                }
            }
            $res[] = $cur;
        }
        
        return $res;
    }
}
```

### go
```go
func generate(numRows int) [][]int {
    var res [][]int

    for i := 1; i <= numRows; i++ {
        var cur []int

        if i == 1 {
            cur = []int{1}
        } else if i == 2 {
            cur = []int{1, 1}
        } else {
            for j := 0; j < i; j++ {
                if j == 0 || j == i - 1 {
                    cur = append(cur, 1)
                } else  {
                    cur = append(cur, res[i - 2][j - 1] + res[i - 2][j])
                }
            }
        }
        res = append(res, cur)
    }

    return res
}
```

### java
```java
class LeetCode0118 {

    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> res = new ArrayList<>();
        for (int i = 1; i <= numRows; i++)  {
            List<Integer> cur = new ArrayList<>();
            if (i == 1) {
                cur.add(1);
            } else if (i == 2) {
                cur.add(1);
                cur.add(1);
            } else {
                for (int j = 0; j < i; j++) {
                    if (j == 0 || j == i - 1) {
                        cur.add(1);
                    } else {
                        cur.add(res.get(i - 2).get(j - 1) + res.get(i - 2).get(j));
                    }
                }
            }
            res.add(cur);
        }
        return res;
    }
}
```
