# 0918. 环形子数组的最大和

# 题目
给定一个长度为 n 的环形整数数组 nums ，返回 nums 的非空 子数组 的最大可能和 。

环形数组 意味着数组的末端将会与开头相连呈环状。形式上， nums[i] 的下一个元素是 nums[(i + 1) % n] ， nums[i] 的前一个元素是 nums[(i - 1 + n) % n] 。

子数组 最多只能包含固定缓冲区 nums 中的每个元素一次。形式上，对于子数组 nums[i], nums[i + 1], ..., nums[j] ，不存在 i <= k1, k2 <= j 其中 k1 % n == k2 % n 。

https://leetcode.cn/problems/maximum-sum-circular-subarray/description/

提示：
- n == nums.length
- 1 <= n <= 3 * 10,000
-3 * 10,000 <= nums[i] <= 3 * 10,000

# 示例
```
示例 1：

输入：nums = [1,-2,3,-2]
输出：3
解释：从子数组 [3] 得到最大和 3
```

```
示例 2：

输入：nums = [5,-3,5]
输出：10
解释：从子数组 [5,5] 得到最大和 5 + 5 = 10
```

```
示例 3：

输入：nums = [3,-2,2,-3]
输出：3
解释：从子数组 [3] 和 [3,-2,2] 都可以得到最大和 3
```

# 解析

## 动态规划
本题为 [「0053. 最大子数组和」](./leetcode-0053-中等-最大子数组和.md) 的进阶版

由于题目给的数组是【环形】的，因此这个最大子数组可能是【跨国边界】的，这里需要分类讨论。
- 最大子数组没有跨过边界，此时算的是最大非空子数组和（此时同 0053 题）。例如 nums = [5, -9, 4, 4, -9, 5]，最大子数组是 [4, 4]（这里前提是不跨边界）
- 最大子数组有跨过边界（在 nums 两端）。由于 子数组和 + 其余元素和 = sum(nums) = 定值，所以其余元素和越小，子数组和越大
  - 算出最小子数组和 minS，从 sum 减去 minS，就得到了跨界的最大子数组和
  - 例如 nums = [5, -9, 4, 4, -9, 5]，最小子数和是 [-9, 4, 4, -9]，最大子数组是首尾两端跨界的 [5, 5]
  
最终答案就是这两种情况取最大值，即 res = max(maxS, sum - minS)

还有一种特殊情况没有考虑：如果最小子数组是整个数组本身，那么跨过边界的最大子数组是【空的】。这是题目不允许的。

如何判断这种情况呢？判断依据就是 minS == sum(nums)，此时只能返回上面第一种情况的 maxS，即最大子数组在 nums 中间


为什么当 minS == sum(nums) 时，最小子数组可以是整个数组？

答：用反证法证明。假设最小子数组一定不是整个数组，这意味着 nums 的某个前缀或者后缀是大于 0 的（因为包含这个前缀/后缀会让 minS 变大），所以 minS < sum(nums)，矛盾。所以当 minS == sum(nums) 时，最小子数组可以是整个数组。

注：对于 nums = [−1, 1, −1]，最小子数组可以取 [−1]，也可以取整个数组 [−1, 1, −1]。对于这样的 nums，最大子数组一定不会跨过边界，只返回 maxS 仍然是正确的。



# 代码

### php
```php
class LeetCode0918 {

    function maxSubarraySumCircular($nums) {
        if (count($nums) == 1) {
            return $nums[0];
        }

        // 第一种情况：最大子数组在 nums 中间
        $res1 = $this->_maxSubArray($nums);

        // 第二种情况：最大子数组跨过边界了，即包含首尾两个，取中间部分最小的，即第一类中子数组最小的。
        $sum = array_sum($nums);
        $newNum = array_slice($nums, 1, count($nums) - 2); // newNum 去掉了原数组的首尾两个元素
        $res2 = $sum - $this->_minSubArray($newNum);

        return max($res1, $res2);
    }

    function _maxSubArray($nums) {
        $dp = array_fill(0, count($nums), 0);
        $dp[0] = $nums[0];
        $res = $dp[0];
        for ($i = 1; $i < count($nums); $i++) {
            $dp[$i] = max($nums[$i], $dp[$i - 1] + $nums[$i]);
            $res = max($res, $dp[$i]);
        }

        return $res;
    }

    function _minSubArray($nums) {
        $dp = array_fill(0, count($nums), 0);
        $dp[0] = $nums[0];
        $res = $dp[0];
        for ($i = 1; $i < count($nums); $i++) {
            $dp[$i] = min($nums[$i], $dp[$i - 1] + $nums[$i]);
            $res = min($res, $dp[$i]);
        }

        return $res;
    }
}
```

### java
```java
public class LeetCode0918 {

    public static int maxSubarraySumCircular(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        }

        int res1 = maxSubArray(nums);

        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        int[] newNum = Arrays.copyOfRange(nums, 1, nums.length - 1);
        int res2 = sum - minSubArray(newNum);

        return Math.max(res1, res2);
    }

    public static int maxSubArray(int[] nums) {
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        int res = dp[0];
        for (int i = 1; i < nums.length; i++) {
            dp[i] = Math.max(nums[i], dp[i - 1] + nums[i]);
            res = Math.max(res, dp[i]);
        }

        return res;
    }

    public static int minSubArray(int[] nums) {
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        int res = dp[0];
        for (int i = 1; i < nums.length; i++) {
            dp[i] = Math.min(nums[i], dp[i - 1] + nums[i]);
            res = Math.min(res, dp[i]);
        }

        return res;
    }
}
```

### go
```go
func maxSubarraySumCircular(nums []int) int {
	if len(nums) == 1 {
		return nums[0]
	}

	res1 := maxSubArray(nums)

	sum := 0
	for _, num := range nums {
		sum += num
	}
	newNum := nums[1 : len(nums)-1]
	res2 := sum - minSubArray(newNum)

	return max(res1, res2)
}

func maxSubArray(nums []int) int {
	dp := make([]int, len(nums))
	dp[0] = nums[0]
	res := dp[0]
	for i := 1; i < len(nums); i++ {
		dp[i] = max(nums[i], dp[i - 1] + nums[i])
		res = max(res, dp[i])
	}

	return res
}

func minSubArray(nums []int) int {
	dp := make([]int, len(nums))
	dp[0] = nums[0]
	res := dp[0]
	for i := 1; i < len(nums); i++ {
		dp[i] = min(nums[i], dp[i - 1] + nums[i])
		res = min(res, dp[i])
	}

	return res
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
```
