# 152. 乘积最大子数组

# 题目

给你一个整数数组 nums ，请你找出数组中乘积最大的非空连续子数组（该子数组中至少包含一个数字），并返回该子数组所对应的乘积。

测试用例的答案是一个 32 位整数。

子数组 是数组的连续子序列。

https://leetcode.cn/problems/maximum-product-subarray

提示：

- 1 <= nums.length <= 2 * 10<sup>4</sup>
- -10 <= nums[i] <= 10
- nums 的任何前缀或后缀的乘积都 保证 是一个 32位 整数

## 示例

```
输入: nums = [2,3,-2,4]
输出: 6
解释: 子数组 [2,3] 有最大乘积 6。
```

```
输入: nums = [-2,0,-1]
输出: 0
解释: 结果不能为 2, 因为 [-2,-1] 不是子数组。
```

# 解析

## 动态规划

如果用 dpMax 来表示以第 i 个元素结尾的乘积最大子数组的乘积，那么容易推出这样的状态转移方程： 
dpMax[i] = max(dpMax[i - 1] * nums[i], nums[i])

它表示以第 i 个元素结尾的乘积最大子数组的乘积可以考虑 nums[i] 加入前面的 dpMax[i - 1] 对应的一段，或者单独成为一段，
这里两种情况下取最大值。求出所有的 dpMax[i] 之后选取最大的一个作为答案。

可是在这里，这样做是错误的

如果 a = { 5, 6, -3, 4, -3 }，那么此时 dpMax[] = {5, 30, −3, 4, −3}，按照前面的算法可以得到答案为 30，即前两个数的乘积，
而实际上答案应该是全体数字的乘积。问题出在哪里呢？问题出在最后一个 −3 所对应的 dpMax[i] 应该为 5 × 6 × (-3) × 4 × (-3)。 
所以得到了一个结论：当前位置的最优解未必是由前一个位置的最优解转移得到的。

**需要根据正负性进行分类讨论**

考虑当前位置如果是一个负数的话，那么希望以它前一个位置结尾的某个段的积也是个负数，这样就可以负负得正， 并且希望这个积尽可能「负得更多」，即尽可能小。如果当前位置是一个正数的话，更希望以它前一个位置结尾的某个段的积也是个正数，
并且希望它尽可能地大。于是这里可以再维护一个 dpMin[i]，它表示以第 i 个元素结尾的乘积最小子数组的乘积，那么可以得到这样的动态规划转移方程

- dpMax[i] = max(nums[i], nums[i] × dpMax[i - 1], nums[i] × dpMin[i - 1])
- dpMin[i] = min(nums[i], nums[i] × dpMax[i - 1], nums[i] × dpMin[i - 1])

它代表第 i 个元素结尾的乘积最大子数组的乘积 dpMax[i]，可以考虑把 nums[i] 加入第 i − 1 个元素结尾的乘积最大或最小的子数组的乘积中，
二者加上 nums[i]，三者取大，就是第 i 个元素结尾的乘积最大子数组的乘积。第 i 个元素结尾的乘积最小子数组的乘积 dpMin[i] 同理

**考虑优化空间**

由于第 i 个状态只和第 i - 1 个状态相关，根据「滚动数组」思想，可以只用两个变量来维护 i - 1 时刻的状态，一个维护 preMax， 一个维护 preMin 即可。

# 代码

### php
```php
class LeetCode0152 {

    function maxProduct($nums) {
        if (!$nums) {
            return 0;
        }
        $len = count($nums);
        $dpMax = $nums;
        $dpMin = $nums;
        for ($i = 1; $i < $len; $i++) {
            $dpMax[$i] = max($nums[$i], $dpMax[$i - 1] * $nums[$i], $dpMin[$i - 1] * $nums[$i]);
            $dpMin[$i] = min($nums[$i], $dpMax[$i - 1] * $nums[$i], $dpMin[$i - 1] * $nums[$i]);
        }
        return max($dpMax);
    }
}
```


### java
```java
class Leetcode0152 {
    
    public int maxProduct(int[] nums) {
        int[] dpMax = new int[nums.length];
        int[] dpMin = new int[nums.length];
        System.arraycopy(nums, 0, dpMax, 0, nums.length);
        System.arraycopy(nums, 0, dpMin, 0, nums.length);
        for (int i = 1; i < nums.length; i++) {
            dpMax[i] = Math.max(nums[i], Math.max(nums[i] * dpMax[i - 1], nums[i] * dpMin[i - 1]));
            dpMin[i] = Math.min(nums[i], Math.min(nums[i] * dpMax[i - 1], nums[i] * dpMin[i - 1]));
        }
        int res = dpMax[0];
        for (int i = 1; i < dpMax.length; i++) {
            res = Math.max(res, dpMax[i]);
        }
        return res;
    }
} 
```

### go
```go
func maxProduct(nums []int) int {
    length := len(nums)
    dpMax := make([]int, length)
    dpMin := make([]int, length)
    copy(dpMax, nums)
    copy(dpMin, nums)
    for i := 1; i < length; i++ {
        dpMax[i] = Max(nums[i], Max(nums[i] * dpMax[i - 1], nums[i] * dpMin[i - 1]))
        dpMin[i] = Min(nums[i], Min(nums[i] * dpMax[i - 1], nums[i] * dpMin[i - 1]))
    }
    res := dpMax[0]
    for i := 1; i < length; i++ {
        res = Max(res, dpMax[i])
    }
    return res
}

func Max(a int, b int) int {
    if a > b {
    r   eturn a
    }
    return b
}

func Min(a int, b int) int {
    if a < b {
        return a
    }
    return b
}
```
