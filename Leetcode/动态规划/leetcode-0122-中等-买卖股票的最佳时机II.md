# 122. 买卖股票的最佳时机 II

# 题目
给定一个数组 prices ，其中 prices[i] 是一支给定股票第 i 天的价格。

设计一个算法来计算你所能获取的最大利润。你可以尽可能地完成更多的交易（多次买卖一支股票）。

注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。

https://leetcode.cn/problems/best-time-to-buy-and-sell-stock-ii/

提示：
- 1 <= prices.length <= 3  10^4
- 0 <= prices[i] <= 10^4

# 示例:
```
输入: prices = [7,1,5,3,6,4]
输出: 7
解释: 在第 2 天（股票价格 = 1）的时候买入，在第 3 天（股票价格 = 5）的时候卖出, 这笔交易所能获得利润 = 5-1 = 4 。
随后，在第 4 天（股票价格 = 3）的时候买入，在第 5 天（股票价格 = 6）的时候卖出, 这笔交易所能获得利润 = 6-3 = 3 。
```

# 示例
```
输入: prices = [1,2,3,4,5]
输出: 4
解释: 在第 1 天（股票价格 = 1）的时候买入，在第 5 天 （股票价格 = 5）的时候卖出, 这笔交易所能获得利润 = 5-1 = 4 。
注意你不能在第 1 天和第 2 天接连购买股票，之后再将它们卖出。因为这样属于同时参与了多笔交易，你必须在再次购买前出售掉之前的股票。
```


```
输入: prices = [7,6,4,3,1]
输出: 0
解释: 在这种情况下, 没有交易完成, 所以最大利润为 0。
```


# 解析

## 动态规划

### 定义 dp 数组
dp[i][j]：
- i 表示第几天；
- j = 0 表示持有股票的状态，j = 1 表示不持有股票的状态

### 确定递推公式
第 i 天持有股票，即 dp[i][0]，可以由两个状态推导：
1. 第 i - 1 持有股票，维持现状，即 dp[i - 1][0]
2. 第 i 天买入股票，即 dp[i - 1][1] - prices[i]

dp[i][0] 应该取最大值，即 dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i])

和《121. 买卖股票的最佳时机》不同的是，那里股票只能买卖一次，那么第 i 天持有的股票即 dp[i][0] 一定是 -prices[i]。
而这里可以买卖多次，所以当第 i 天买入的时候，手上已经有了之前买卖的利润。

第 i 天不持有股票，即 dp[i][1]，也可以由两个状态推导，取最大的：
1. 第 i - 1 天不持有，保持现状，即 dp[i - 1][1]
2. 第 i 天卖出，获得利润，即 dp[i - 1][0] + prices[i]

同样 dp[i][1] 取最大值，所以 dp[i][1] = max(dp[i - 1][1]，dp[i - 1][0] + prices[i])

### 确定遍历顺序
从递推公式可以看出 dp[i] 都是由 dp[i - 1] 推导出来的，那么一定是从前向后遍历。

### dp 数组初始化
dp[0][0]，即第 0 天买入股票，肯定为 -prices[0]

dp[0][1]，即第 0 天不持有，为 0



# 代码

### php

```php

class LeetCode0122 {
    
    public function maxProfit($prices) {
        $len = count($prices);
        $dp  = [];
        $dp[0][0] = -$prices[0];    //第0天持有
        $dp[0][1] = 0;              //第0天不持有
        for ($i = 1; $i < $len; $i++) {
            $dp[$i][0] = max($dp[$i - 1][1] - $prices[$i], $dp[$i - 1][0]);
            $dp[$i][1] = max($dp[$i - 1][0] + $prices[$i], $dp[$i - 1][1]);
        }
        return $dp[$len - 1][1];    // 返回最后一天没有持有股票的状态
    }

}
```

### go
```go
func maxProfit(prices []int) int {
    length := len(prices)
    dp := make([][]int, length)
    for i := 0; i < length; i++ {
        dp[i] = make([]int, 2)
    }
    dp[0][0] = -prices[0]
    dp[0][1] = 0
    for i := 1; i < length; i++ {
        dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i])
        dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] + prices[i])
    }
    return dp[length - 1][1]
}

func max(n1 int, n2 int) int {
    if n1 >= n2 {
        return n1
    }
    return n2
}
```

### java
```java
class LeetCode0122 {

    public int maxProfit(int[] prices) {
        int[][] dp = new int[prices.length][2];
        dp[0][0] = -prices[0];
        dp[0][1] = 0;
        for (int i = 1; i < prices.length; i++) {
            dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1] - prices[i]);
            dp[i][1] = Math.max(dp[i - 1][1], dp[i - 1][0] + prices[i]);
        }
        return dp[prices.length - 1][1];
    }
}
```
