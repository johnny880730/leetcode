# 1143. 最长公共子序列

# 题目
给定两个字符串 text1 和 text2，返回这两个字符串的最长 公共子序列 的长度。如果不存在 公共子序列 ，返回 0 。
一个字符串的 子序列 是指这样一个新的字符串：它是由原字符串在不改变字符的相对顺序的情况下删除某些字符（也可以不删除任何字符）后组成的新字符串。

例如，"ace" 是 "abcde" 的子序列，但 "aec" 不是 "abcde" 的子序列。
两个字符串的 公共子序列 是这两个字符串所共同拥有的子序列。

https://leetcode.cn/problems/longest-common-subsequence/

# 示例：
```
输入：text1 = "abcde", text2 = "ace"
输出：3
解释：最长公共子序列是 "ace" ，它的长度为 3 。
```

```
输入：text1 = "abc", text2 = "abc"
输出：3
解释：最长公共子序列是 "abc" ，它的长度为 3 。
```

```
输入：text1 = "abc", text2 = "def"
输出：0
解释：两个字符串没有公共子序列，返回 0 。
```


提示：
- 1 <= text1.length, text2.length <= 1000
- text1 和 text2 仅由小写英文字符组成。

# 解析
本题和 《718. 最长重复子数组》 区别在于这里不要求是连续的了，但要有相对顺序，即："ace" 是 "abcde" 的子序列，但 "aec" 不是 "abcde" 的子序列。

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
dp[i][j]：区间为 [0, i - 1] 的字符串 text1 与区间为 [0, j - 1] 的字符串 text2 的最长公共子序列为 dp[i][j]

为什么要定义区间为 [0, i - 1]，定义为 [0, i] 不香么？这样定义是为了后面代码实现方便，如果非要定义为区间为 [0, i] 也可以，
在 《718. 最长重复子数组》 中的《拓展》部分讲了区别所在，其实就是简化了 dp 数组第一行和第一列的初始化逻辑。

### 确定递推公式
主要就是两大情况： text1[i - 1] 与 text2[j - 1] 是否相同
- 如果 text1[i - 1] == text2[j - 1]，那么找到了一个公共元素，所以 dp[i][j] = dp[i - 1][j - 1] + 1
- 如果 text1[i - 1] != text2[j - 1]，这就意味着 text1[i - 1] 和 text2[j - 1] 至少有一个不在最长公共子序列里。下面说说这种情况的分析

text1[i - 1] != text2[j - 1] 的分析如下：
1. 情况一：text1[i - 1] 不在最长公共子序列里
2. 情况二：text2[j - 1] 不在最长公共子序列里
3. 情况三：二者都不在最长公共子序列里

这三种情况，取结果最大的那个，因为题目要求的就是最长公共子序列。

其实情况三可以直接忽略，因为情况三得到的长度肯定小于等于情况二和情况一，说白了情况三被情况一和情况二给包含了，所以可以直接忽略情况三。
所以就看看 text1[0, i - 2] 与 text2[0, j - 1] 的最长公共子序列，和 text1[0, i - 1] 与 text2[0, j - 2] 的最长公共子序列，取最大的。
即：dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);


### dp 数组初始化
dp[i][0] 应该是多少呢？

text1[0, i - 1] 和空串的最长公共子序列自然是 0，所以 dp[i][0] = 0。同理 dp[0][j] 也是 0。

其他下标都是随着递推公式逐步覆盖，初始为多少都可以，那么就统一初始为 0。

### 确定遍历顺序
从递推公式，可以看出，有三个方向可以推出 dp[i][j]，如图：

![](./images/leetcode-1143-img1.png)

那么为了在递推的过程中，这三个方向都是经过计算的数值，所以要从前向后，从上到下来遍历这个矩阵。

### 举例推导 dp 数组
以输入：text1 = "abcde", text2 = "ace" 为例，dp 状态如图：

![](./images/leetcode-1143-img2.png)


时间复杂度: O(n * m)，其中 n 和 m 分别为 text1 和 text2 的长度

空间复杂度: O(n * m)

# 代码

### php
```php
class LeetCode1143 {
    
    public function longestCommonSubsequence($t1, $t2) {
        $len1 = strlen($t1);
        $len2 = strlen($t2);
        $dp = array_fill(0, $len1 + 1, array_fill(0, $len2 + 1, 0));
        for ($i = 1; $i <= $len1; $i++) {
            for ($j = 1; $j <= $len2; $j++) {
                if ($t1[$i - 1] == $t2[$j - 1]) {
                    $dp[$i][$j] = $dp[$i - 1][$j - 1] + 1;
                } else {
                    $dp[$i][$j] = max($dp[$i - 1][$j], $dp[$i][$j - 1]);
                }
            }
        }
        return $dp[$len1][$len2];
    }

}
```

### java
```java
class Leetcode1142 {
    
    public int longestCommonSubsequence(String text1, String text2) {
        int len1 = text1.length(), len2 = text2.length();
        int[][] dp = new int[len1 + 1][len2 + 1];
        for (int i = 1; i <= len1; i++) {
            for (int j = 1; j <= len2; j++) {
                if (text1.charAt(i - 1) == text2.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }
        return dp[len1][len2];
    }
}
```

### go
```go
func longestCommonSubsequence(text1 string, text2 string) int {
	m, n := len(text1), len(text2)
	dp := make([][]int, m + 1)
	for i := 0; i <= m; i++ {
		dp[i] = make([]int, n + 1)
	}

	for i := 1; i <= m; i++ {
		for j := 1; j <= n; j++ {
			if text1[i - 1] == text2[j - 1] {
				dp[i][j] = dp[i - 1][j - 1] + 1
			} else {
				dp[i][j] = max(dp[i - 1][j], dp[i][j - 1])
			}
		}
	}

	return dp[m][n]
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
```


