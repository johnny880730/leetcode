# 174. 地下城游戏


# 题目
恶魔们抓住了公主并将她关在了地下城 dungeon 的 右下角 。地下城是由 m x n 个房间组成的二维网格。我们英勇的骑士最初被安置在 左上角 的房间里，
他必须穿过地下城并通过对抗恶魔来拯救公主。

骑士的初始健康点数为一个正整数。如果他的健康点数在某一时刻降至 0 或以下，他会立即死亡。

有些房间由恶魔守卫，因此骑士在进入这些房间时会失去健康点数（若房间里的值为负整数，则表示骑士将损失健康点数）；其他房间要么是空的（房间里的值为 0），
要么包含增加骑士健康点数的魔法球（若房间里的值为正整数，则表示骑士将增加健康点数）。

为了尽快解救公主，骑士决定每次只 向右 或 向下 移动一步。

返回确保骑士能够拯救到公主所需的最低初始健康点数。

注意：任何房间都可能对骑士的健康点数造成威胁，也可能增加骑士的健康点数，包括骑士进入的左上角房间以及公主被监禁的右下角房间。

提示：
- m == dungeon.length
- n == dungeon[i].length
- 1 <= m, n <= 200
- -1000 <= dungeon[i][j] <= 1000

# 示例
```
输入：dungeon = [[-2,-3,3],[-5,-10,1],[10,30,-5]]
输出：7
解释：如果骑士遵循最佳路径：右 -> 右 -> 下 -> 下 ，则骑士的初始健康点数至少为 7 。
```
```
输入：dungeon = [[0]]
输出：1
```

# 解析

同样是从左上角到右下角的问题，能否套用《0064. 最小路径和》的思路呢？

想要最小化骑士的初始生命值，是不是意味着要最大化行进路上的血瓶？是不是相当于求最大路径和？

其实这个推论并不成立，吃到最多的血瓶并不一定能够最小化初始生命值。最简单的例子就是行进路上一个怪物都没有，因此初始生命值只要为 1 就足够了，
血瓶再多也不起作用。

所以关键不在于吃最多的血瓶，而是在于如何损失最少的生命值。

这类求最值的问题，肯定要借助动态规划技巧

## 动态规划

dp 函数定义：
```java
// 从 dp[i][j] 到达终点（右下角）所需的最少生命值是 dp(grid, i, j)
int dp(int[][] grid, int i, int j)
```

根据这个定义，要求的最终结果就是 dp(grid, 0, 0) 的值。

要求 dp(grid, 0, 0) 的值，是由 dp(grid, 0, 1) 的值和 dp(grid, 1, 0) 的值推导出来的，哪个更小哪个就是起始点的结果。
也就是说 dp(grid, i, j) 的值是由 dp(grid, i + 1, j) 和 dp(grid, i, j + 1) 推导出来的。

但是要注意的是，整个过程骑士的血量必须大于等于 1。

比如 dp(0, 1) = 5, dp(1, 0) = 4，那么肯定是从初始位置往下走。假设 grid[0][0] = 1, 那么初始生命值就是 4 - 1 = 3 了。
但如果 grid[0][0] = 10，也就是一落地就有个大血瓶，4 - 10 = -6，不符合题意，所以这种情况下初始血量就是 1 了。

同样的，存在重叠子问题的情况，依然靠备忘录技巧来加速计算。


# 代码

### php
```php
class LeetcodeXXXX {
    
    public $memo;

    function calculateMinimumHP($dungeon) {
        $m = count($dungeon);
        $n = count($dungeon[0]);
        // 备忘录初始化为 -1
        $this->memo = array_fill(0, $m, array_fill(0, $n, -1));

        return $this->_dp($dungeon, 0, 0);
    }

    function _dp($dungeon, $i, $j) {
        $m = count($dungeon);
        $n = count($dungeon[0]);
        // base case
        if ($i == $m - 1 && $j == $n - 1) {
            return $dungeon[$i][$j] > 0 ? 1 : (0 - $dungeon[$i][$j] + 1);
        }
        if ($i >= $m || $j >= $n) {
            return PHP_INT_MAX;
        }
        // 避免重复计算
        if ($this->memo[$i][$j] != -1) {
            return $this->memo[$i][$j];
        }
        // 状态转移
        $cur = min($this->_dp($dungeon, $i, $j + 1), $this->_dp($dungeon, $i + 1, $j)) - $dungeon[$i][$j];

        $this->memo[$i][$j] = $cur <= 0 ? 1 : $cur;
        return $this->memo[$i][$j];
    }
}
```

### java
```java
class Leetcode0174 {

    int[][] memo;

    public int calculateMinimumHP(int[][] dungeon) {
        int m = dungeon.length;
        int n = dungeon[0].length;
        memo = new int[m][n];
        for (int[] row: memo) {
            Arrays.fill(row, -1);
        }
        return _dp(dungeon, 0, 0);
    }

    public int _dp(int[][] grid, int i, int j) {
        int m = grid.length;
        int n = grid[0].length;
        if (i == m - 1 && j == n - 1) {
            return grid[i][j] > 0 ? 1 : -grid[i][j] + 1;
        }
        if (i >= m || j >= n) {
            return Integer.MAX_VALUE;
        }
        if (memo[i][j] != -1) {
            return memo[i][j];
        }
        int cur = Math.min(_dp(grid, i + 1, j), _dp(grid, i, j + 1)) - grid[i][j];
        memo[i][j] = cur <= 0 ? 1 : cur;

        return memo[i][j];
    }
}
```
