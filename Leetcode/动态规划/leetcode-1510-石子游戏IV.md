### 1510. 石子游戏 IV

# 题目
Alice 和 Bob 两个人轮流玩一个游戏，Alice 先手。

一开始，有 n 个石子堆在一起。每个人轮流操作，正在操作的玩家可以从石子堆里拿走 任意非零平方数个石子。

如果石子堆里没有石子了，则无法操作的玩家输掉游戏。

给你正整数 n ，且已知两个人都采取最优策略。如果 Alice 会赢得比赛，那么返回  True  ，否则返回  False  。

https://leetcode.cn/problems/stone-game-iv

提示：
- 1 <= n <= 10<sup>6</sup>

## 示例
```
输入：n = 1
输出：true
解释：Alice 拿走 1 个石子并赢得胜利，因为 Bob 无法进行任何操作。
```
```
输入：n = 2
输出：false
解释：Alice 只能拿走 1 个石子，然后 Bob 拿走最后一个石子并赢得胜利（2 -> 1 -> 0）。
```
```
输入：n = 4
输出：true
解释：n 已经是一个平方数，Alice 可以一次全拿掉 4 个石子并赢得胜利（4 -> 0）。
```
```
输入：n = 7
输出：false
解释：当 Bob 采取最优策略时，Alice 无法赢得比赛。
如果 Alice 一开始拿走 4 个石子， Bob 会拿走 1 个石子，然后 Alice 只能拿走 1 个石子，Bob 拿走最后一个石子并赢得胜利（7 -> 3 -> 2 -> 1 -> 0）。
如果 Alice 一开始拿走 1 个石子， Bob 会拿走 4 个石子，然后 Alice 只能拿走 1 个石子，Bob 拿走最后一个石子并赢得胜利（7 -> 6 -> 2 -> 1 -> 0）。
```
```
输入：n = 17
输出：false
解释：如果 Bob 采取最优策略，Alice 无法赢得胜利。
```

# 解析

&emsp;&emsp;见代码注释


# 代码
```php
class LeetCode1510 {
    
    // 暴力递归，会超时
    // 函数含义：针对当前的、拿分数的人，如果面对 n 这个分数，当前的人会不会赢
    function winnerSquareGame1($n) {
        if ($n == 0) {
            // 没有分数了，说明上个人已经拿走了最后的分数了，输了
            return false;
        }
        // n > 0
        for ($p = 1; $p * $p <= $n; $p++) {
            // 某一个决定
            $pick = $p * $p;
            // 下一轮 对手面对的分值，如果对手输了，就是“我”赢了
            $rest = $n - $pick;
            if (!$this->winnerSquareGame1($rest)) {
                return true;
            }
        }
        return false;
    }
    
    // 改动态规划
    // 假设n=100，f(100)依赖f(100-1=99)、f(100-4=96)、f(100-9=91) 等等的状态
    // 从f(1)开始，推导出f(2)、f(3) 等等的返回值，直到f(n)的返回值
    // 类似斐波那契数列，改写上面的方法
    function winnerSquareGame2($n) {
        // 这里 f[x] 的值相当于上面 winnerSquareGame1(x) 的返回值
        $f = array_fill(0, $n + 1, false);
        for ($i = 1; $i <= $n; $i++) {
            // f[1] 依赖 f[0..0]
            // f[2] 依赖 f[0..1]
            // f[3] 依赖 f[0..2]
            // f[i] 依赖 f[0..i-1]
            for ($p = 1; $p * $p <= $i; $p++) {
                $pick = $p * $p;
                $rest = $i - $pick;
                if (!$f[$rest]) {
                    $f[$i] = true;
                    break;
                }
            }
        }
        return $f[$n];
    }
}
```