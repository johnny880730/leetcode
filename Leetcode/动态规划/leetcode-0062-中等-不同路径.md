# 62. 不同路径

# 题目
一个机器人位于一个 m x n 网格的左上角。

机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角。

问总共有多少条不同的路径？

https://leetcode.cn/problems/unique-paths/

提示：
- 1 <= m, n <= 100
- 题目数据保证答案小于等于 2 * 10^9

# 示例 
```
输入：m = 3, n = 7
输出：28
```

```
输入：m = 3, n = 2
输出：3
解释：
从左上角开始，总共有 3 条路径可以到达右下角。
1. 向右 -> 向下 -> 向下
2. 向下 -> 向下 -> 向右
3. 向下 -> 向右 -> 向下
```

```
输入：m = 7, n = 3
输出：28
```

```
输入：m = 3, n = 3
输出：6
```

# 解析

## 动态规划五部曲
机器人从(0 , 0) 位置出发，到(m - 1, n - 1)终点。

### 确定 dp 数组以及下标的含义
dp[i][j] ：表示从 (0, 0) 出发，到 (i, j) 有 dp[i][j] 条不同的路径。

### 确定递推公式
想要求dp[i][j]，只能有两个方向来推导出来，即 dp[i - 1][j] 和 dp[i][j - 1]。

此时在回顾一下 dp[i - 1][j] 表示啥，是从 (0, 0) 的位置到 (i - 1, j)有几条路径，dp[i][j - 1] 同理。

那么很自然，dp[i][j] = dp[i - 1][j] + dp[i][j - 1]，因为 dp[i][j] 只有这两个方向过来。

### dp数组的初始化
如何初始化呢，首先 dp[i][0] 一定都是1，因为从 (0, 0) 的位置到 (i, 0) 的路径只有一条，那么 dp[0][j] 也同理。

所以初始化代码为：
```
for (int i = 0; i < m; i++) {
    dp[i][0] = 1;
}
for (int j = 0; j < n; j++) {
    dp[0][j] = 1;
}
```

### 确定遍历顺序
这里要看一下递推公式 dp[i][j] = dp[i - 1][j] + dp[i][j - 1]，dp[i][j] 都是从其上方和左方推导而来，那么从左到右一层一层遍历就可以了。

这样就可以保证推导 dp[i][j] 的时候，dp[i - 1][j] 和 dp[i][j - 1] 一定是有数值的。

### 举例推导dp数组
如下图

![](./images/leetcode-0062-img1.png)



# 代码

### php
```php

class LeetCode0062 {

    public function uniquePaths($m, $n) {
        $dp = array_fill(0, $m, array_fill(0, $n, 0));
        for ($i = 0; $i < $m; $i++) {
            $dp[$i][0] = 1;
        }
        for ($i = 0; $i < $n; $i++) {
            $dp[0][$i] = 1;
        }
        for ($i = 1; $i < $m; $i++) {
            for ($j = 1; $j < $n; $j++) {
                $dp[$i][$j] = $dp[$i - 1][$j] + $dp[$i][$j - 1];
            }
        }
        return $dp[$m - 1][$n - 1];
    }
}
```

### go
```go
func uniquePaths(m int, n int) int {
    dp := make([][]int, m)
    for i := 0; i < m; i++ {
        dp[i] = make([]int, n)
    }
    for i := 0; i < m; i++ {
        dp[i][0] = 1
    }
    for j := 0; j < n; j++ {
        dp[0][j] = 1
    }
    for i := 1; i < m; i++ {
        for j := 1; j < n; j++ {
            dp[i][j] = dp[i - 1][j] + dp[i][j - 1]
        }
    }
    return dp[m - 1][n - 1]
}
```

### java
```java
class LeetCode0062 {

    public int uniquePaths(int m, int n) {
        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++) {
            dp[i][0] = 1;
        }
        for (int j = 0; j < n; j++) {
            dp[0][j] = 1;
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }
        return dp[m - 1][n - 1];
    }
}
```
