# 0063. 不同路径 II

# 题目
一个机器人位于一个 m x n 网格的左上角 （起始点在下图中标记为“Start” ）。

机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为“Finish”）。

现在考虑网格中有障碍物。那么从左上角到右下角将会有多少条不同的路径？

网格中的障碍物和空位置分别用 1 和 0 来表示。

https://leetcode.cn/problems/unique-paths-ii/description/

提示：
- m == obstacleGrid.length
- n == obstacleGrid[i].length
- 1 <= m, n <= 100
- obstacleGrid[i][j] 为 0 或 1



# 示例
```
输入：obstacleGrid = [[0,0,0],[0,1,0],[0,0,0]]
输出：2
解释：
3x3 网格的正中间有一个障碍物。
从左上角到右下角一共有 2 条不同的路径：
1. 向右 -> 向右 -> 向下 -> 向下
2. 向下 -> 向下 -> 向右 -> 向右
```
```
输入：obstacleGrid = [[0,1],[0,0]]
输出：1
```

# 解析
对比《62. 不同路径》，区别就是：有障碍的话，其实就是标记对应的 dp 数组保持初始值 0 就可以了。

## 动态规划五部曲

### 确定dp数组（dp table）以及下标的含义
dp[i][j] ：表示从 (0, 0) 出发，到 (i, j) 有 dp[i][j] 条不同的路径。

### 确定递推公式
递推公式和《62.不同路径》一样，dp[i][j] = dp[i - 1][j] + dp[i][j - 1]。

但这里需要注意一点，因为有了障碍，(i, j) 如果就是障碍的话应该就保持初始状态（初始状态为 0 ）。

### dp数组如何初始化
没有障碍的话，因为从 (0, 0) 的位置到 (i, 0) 的路径只有一条，所以 dp[i][0] 一定为 1，dp[0][j] 也同理。

但如果 (i, 0) 这条边有了障碍之后，障碍之后（包括障碍）都是走不到的位置了，所以障碍之后的 dp[i][0] 应该还是初始值 0。

### 确定遍历顺序
从递推公式 dp[i][j] = dp[i - 1][j] + dp[i][j - 1] 中可以看出，一定是从左到右一层一层遍历，这样保证推导 dp[i][j] 的时候，
dp[i - 1][j] 和 dp[i][j - 1] 一定是有数值。

### 举例推导dp数组
拿示例 1 来举例如题：

![](./images/leetcode-0063-img1.png)

# 代码

### php
```php
class LeetCode0063 {

    public function uniquePathsWithObstacles($grid) {
        $m  = count($grid);
        $n  = count($grid[0]);
        $dp = array_fill(0, $m, array_fill(0, $n, 0));
        for ($i = 0; $i < $m; $i++) {
            if ($grid[$i][0] == 1) {
                break;
            }
            $dp[$i][0] = 1;
        }
        for ($i = 0; $i < $n; $i++) {
            if ($grid[0][$i] == 1) {
                break;
            }
            $dp[0][$i] = 1;
        }
        for ($i = 1; $i < $m; $i++) {
            for ($j = 1; $j < $n; $j++) {
                // 坐标(i,j)没有障碍的时候才推导dp[i][j]
                if ($grid[$i][$j] == 0) {
                    $dp[$i][$j] = $dp[$i - 1][$j] + $dp[$i][$j - 1];
                }
            }
        }
        return $dp[$m - 1][$n - 1];
    }

}
```

### java
```java
class LeetCode0063 {
    
    public int uniquePathsWithObstacles(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        int[][] dp = new int[m][n];
        for (int i = 0; i < m; i++) {
            if (grid[i][0] == 1) {
                break;
            }
            dp[i][0] = 1;
        }
        for (int i = 0; i < n; i++) {
            if (grid[0][i] == 1) {
                break;
            }
            dp[0][i] = 1;
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++ ) {
                if (grid[i][j] == 0) {
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
                }
            }
        }

        return dp[m - 1][n - 1];
    }
}
```

### go
```go
func uniquePathsWithObstacles(grid [][]int) int {
    m, n := len(grid), len(grid[0])
    dp := make([][]int, m)
    for i := 0; i < m; i++ {
        dp[i] = make([]int, n)
    }
    for i := 0; i < m; i++ {
		if grid[i][0] == 1 {
			break
        }
        dp[i][0] = 1
    }
    for i := 0; i < n; i++ {
		if  grid[0][i] == 1 {
		    break;	
        }
        dp[0][i] = 1
    }
    for i := 1; i < m; i++ {
        for j := 1; j < n; j++ {
            if grid[i][j] == 0 {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1]
            }
        }
    }
    return dp[m - 1][n - 1]
}
```