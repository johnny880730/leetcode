# 1035. 不相交的线

# 题目
在两条独立的水平线上按给定的顺序写下 nums1 和 nums2 中的整数。  
现在，可以绘制一些连接两个数字 nums1[i] 和 nums2[j] 的直线，这些直线需要同时满足满足：
- nums1[i] == nums2[j]
- 且绘制的直线不与任何其他连线（非水平线）相交。


https://leetcode.cn/problems/uncrossed-lines

提示：
- 1 <= nums1.length, nums2.length <= 500
- 1 <= nums1[i], nums2[j] <= 2000


# 示例：
![](./images/leetcode-1035-示例.png)
```
输入：nums1 = [1,4,2], nums2 = [1,2,4]
输出：2
解释：可以画出两条不交叉的线，如上图所示。
但无法画出第三条不相交的直线，因为从 nums1[1]=4 到 nums2[2]=4 的直线将与从 nums1[2]=2 到 nums2[1]=2 的直线相交。
```
```
输入：nums1 = [2,5,1,2,5], nums2 = [10,5,2,1,5,2]
输出：3
```
```
输入：nums1 = [1,3,7,1,7,5], nums2 = [1,9,2,5,1]
输出：2
```


# 解析
绘制一些连接两个数字 A[i] 和 B[j] 的直线，只要 A[i] == B[j]，且直线不能相交！

直线不能相交，这就是说明在 A 中找到一个与 B 相同的子序列，且这个子序列不能改变相对顺序，只要相对顺序不改变，链接相同数字的直线就不会相交。

拿示例一 A = [1, 4, 2], B = [1, 2, 4] 为例，其实也就是说 A 和 B 的最长公共子序列是 [1, 4]，长度为 2。
这个公共子序列指的是相对顺序不变（即数字 4 在 A 中数字 1 的后面，那么数字 4 也应该在 B 数字 1 的后面）

这么可以发现：本题说是求绘制的最大连线数，其实就是求两个字符串的最长公共子序列的长度！

那么这道题和《1143. 最长公共子序列》 就是一样的了。一样到什么程度呢？ 把字符串名字改一下，其他代码都不用改，直接 copy 过来就行了。
题目解析直接参考 《1143. 最长公共子序列》 即可。


# 代码

### php
```php
class LeetCode1035 {

    function maxUncrossedLines($nums1, $nums2) {
        $len1 = count($nums1);
        $len2 = count($nums2);
        $dp = array_fill(0, $len1 + 1, array_fill(0, $len2 + 1, 0));
        for ($i = 1; $i <= $len1; $i++) {
            for ($j = 1; $j <= $len2; $j++) {
                if ($nums1[$i - 1] == $nums2[$j - 1]) {
                    $dp[$i][$j] = $dp[$i - 1][$j - 1] + 1;
                } else {
                    $dp[$i][$j] = max($dp[$i - 1][$j], $dp[$i][$j - 1]);
                }
            }
        }
        return $dp[$len1][$len2];
    }

}

```

### go
```go
func maxUncrossedLines(nums1 []int, nums2 []int) int {
    len1, len2 := len(nums1), len(nums2)
    dp := make([][]int, len1 + 1)
    for k, _ := range dp {
        dp[k] = make([]int, len2 + 1)
    }
    for i := 1; i <= len1; i++ {
        for j := 1; j <= len2; j++ {
            if nums1[i - 1] == nums2[j - 1] {
                dp[i][j] = dp[i - 1][j - 1] + 1
            } else {
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1])
            }
        }
    }
    return dp[len1][len2]
}

func max(a, b int) int {
    if a > b {
        return a
    }
    return b
}
```

