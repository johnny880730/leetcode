# 714. 买卖股票的最佳时机含手续费

# 题目 
给定一个整数数组 prices，其中第 i 个元素代表了第 i 天的股票价格 ；整数 fee 代表了交易股票的手续费用。

你可以无限次地完成交易，但是你每笔交易都需要付手续费。如果你已经购买了一个股票，在卖出它之前你就不能再继续购买股票了。

返回获得利润的最大值。

注意：这里的一笔交易指买入持有并卖出股票的整个过程，每笔交易你只需要为支付一次手续费。

https://leetcode.cn/problems/best-time-to-buy-and-sell-stock-with-transaction-fee

# 示例:
```
输入：prices = [1, 3, 2, 8, 4, 9], fee = 2
输出：8
解释：能够达到的最大利润:  
在此处买入 prices[0] = 1
在此处卖出 prices[3] = 8
在此处买入 prices[4] = 4
在此处卖出 prices[5] = 9
总利润: ((8 - 1) - 2) + ((9 - 4) - 2) = 8
```

```
输入：prices = [1,3,7,5,10,3], fee = 3
输出：6
```

提示：
- 1 <= prices.length <= 5 * 10^4
- 1 <= prices[i] < 5 * 10^4
- 0 <= fee < 5 * 10^4

# 解析

相对于 《122. 买卖股票的最佳时机II》 本题只需要在计算卖出操作的时候减去手续费就可以了，代码几乎是一样的。所以主要说一下递推公式部分。

重申一下 dp 数组的含义：dp[i][j]
- j = 0 表示第 i 天持有股票所省最多现金 
- j = 1 表示第 i 天不持有股票所得最多现金

如果第 i 天持有股票即 dp[i][0]， 那么可以由两个状态推出来
- 第 i - 1 天就持有股票，那么就保持现状，即：dp[i - 1][0]
- 第 i 天买入股票，所得现金就是昨天不持有股票的所得现金减去今天的股票价格 即：dp[i - 1][1] - prices[i]

所以：dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i])

在来看看如果第 i 天不持有股票即 dp[i][1] 的情况，依然可以由两个状态推出来
- 第 i - 1 天就不持有股票，那么就保持现状，即：dp[i - 1][1]
- 第 i 天卖出股票，所得现金就是按照今天股票价格卖出后所得现金，注意这里需要有手续费了。即：dp[i - 1][0] + prices[i] - fee

所以：dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] + prices[i] - fee);

本题和 《122. 买卖股票的最佳时机II》的区别就是这里需要多一个减去手续费的操作。

# 代码

### php
```php
class LeetCode0714 {
    
    public function maxProfit($prices, $fee) {
        $n = count($prices);
        $dp = array_fill(0, $n, array_fill(0, 2, 0));
        $dp[0][0] = -$prices[0];        // 第 0 天持有股票
        for ($i = 1; $i < $n; $i++) {
            $dp[$i][0] = max($dp[$i - 1][0], $dp[$i - 1][1] - $prices[$i]);
            $dp[$i][1] = max($dp[$i - 1][1], $dp[$i - 1][0] + $prices[$i] - $fee);
        }
        return $dp[$n - 1][1];
    }

}
```

### go
```go
func maxProfit(prices []int, fee int) int {
    size := len(prices)
    dp := make([][]int, size)
    for k, _ := range dp {
        dp[k] = make([]int, 2)
    }
    dp[0][0] = -prices[0]
    for i := 1; i < size; i++ {
        dp[i][0] = max(dp[i - 1][0], dp[i - 1][1] - prices[i])
        dp[i][1] = max(dp[i - 1][1], dp[i - 1][0] + prices[i] - fee)
    }
    return dp[size - 1][1]
}

func max(a, b int) int {
    if a > b {
        return a
    }
    return b
}
```