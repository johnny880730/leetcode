# 392. 判断子序列

# 题目
给定字符串 s 和 t ，判断 s 是否为 t 的子序列。

字符串的一个子序列是原始字符串删除一些（也可以不删除）字符而不改变剩余字符相对位置形成的新字符串。（例如，"ace"是"abcde"的一个子序列，而"aec"不是）。


# 示例：
```
输入：s = "abc", t = "ahbgdc"
输出：true
```

```
输入：s = "axc", t = "ahbgdc"
输出：false
```


提示：
- 0 <= s.length <= 100
- 0 <= t.length <= 10^4
- 两个字符串都只由小写字符组成。

https://leetcode.cn/problems/is-subsequence

# 解析

本题可以使用双指针的思路来解答，在这里重点用动态规划的解题思路。本题也是“编辑距离系列”的入门题目。
因为从题意可以发现，只需要考虑删除字符串的情况，不用考虑增加和替换字符串的情况。

所以掌握本题也可以为“编辑距离系列”打下基础。

## 动态规划五部曲

### 确定 dp 数组及下标含义    
dp[i][j]：表示 s[i - 1] 为结尾和 t[j - 1]为结尾的相同的子序列的长度是 dp[i][j]

### 确定递推公式  
在确定递推公式的时候，首先要考虑两种情况：
- s[i - 1] == t[j - 1]：t 中找到的一个字符在 s 中也出现了
- s[i - 1] != t[j - 1]：t 要删除元素，继续匹配

如果 s[i - 1] 与 t[j - 1] 相同，那么 dp[i][j] = dp[i - 1][j - 1] + 1，因为找到了相同的字符，长度自然要在 dp[i - 1][j - 1] 基础上加1。

如果 s[i - 1] 与 t[j - 1] 不同，此时相当于 t 要删除元素，如果把当前元素 t[j - 1] 删除，那么 dp[i][j] 的数值就是 s[i - 1] 与 t[j - 2] 的比较结果，
即 dp[i][j] = dp[i][j - 1]    

可以发现此题和 《1143. 最长公共子序列》 的递推公式基本那就是一样的，区别就是本题如果删元素一定是字符串 t，
而 《1143. 最长公共子序列》 是两个字符串都可以删元素。

### dp 数组初始化  
从递推公式可以看出，dp[i][j] 依赖于 dp[i - 1][j - 1] 和 dp[i][j - 1] 的状态，所以 dp[0][0] 和 dp[i][0] 是一定要初始化的。

可以发现，dp[i][j] 为什么定义为下标 i - 1 结尾的字符串 s 和下标 j - 1 结尾的字符串 t 相同子序列的长度，因为这样的定义在 dp 二维矩阵中可以流出初始化的空间。

如果以下标 i 结尾的字符串 s 和下标 j 结尾的字符串 t，那么初始化就麻烦一下。dp[i][0] 和 dp[0][j] 是没有含义的，仅仅是为了给推导公式做普掉，所以初始化为 0。

### 确定遍历顺序  
从递推公式可以看出，dp[i][j] 依赖于 dp[i - 1][j - 1] 和 dp[i][j - 1] 的状态，那么遍历顺序应该是从上到下，从左到右。

### 举例推导 dp 数组
以示例一为例，输入：s = "abc", t = "ahbgdc"，dp 状态转移图如下：

![](./images/leetcode-0392-img1.png)

根据 dp[i][j] 定义，如果 dp[len(s)][len(t)] 与 字符串 s 的长度相同，那就说明 s 与 t 的最长相同子序列就是 s，那么 s 就是 t 的子序列。

图中 dp[len(s)][len(t)] = 3， 而 len(s) 也为 3。所以 s 是 t 的子序列，返回 true。

## 双指针
设置双指针 i , j 分别指向字符串 s , t 的首个字符，遍历字符串 t ：
- 当 s[i] == t[j] 时，代表匹配成功，此时同时 i++ , j++
  - 进而，若 i 已走过 s 尾部，代表 s 是 t 的子序列，此时应提前返回 true ；
- 当 s[i] != t[j] 时，代表匹配失败，此时仅 j++ ；
- 若遍历完字符串 t 后，字符串 s 仍未遍历完，代表 s 不是 t 的子序列，此时返回 false 。


# 代码

### php
```php
class LeetCode0392 {

    // DP
    public function isSubsequence($s, $t) {
        $len1 = strlen($s);
        $len2 = strlen($t);
        $dp = array_fill(0, $len1 + 1, array_fill(0, $len2 + 1, 0));
        for ($i = 1; $i <= $len1; $i++) {
            for ($j = 1; $j <= $len2; $j++) {
                if ($s[$i - 1] == $t[$j - 1]) {
                    $dp[$i][$j] = $dp[$i - 1][$j - 1] + 1;
                } else {
                    $dp[$i][$j] = $dp[$i][$j - 1];
                }
            }
        }
        if ($dp[$len1][$len2] == $len1) {
            return true;
        }
        return false;
    }

    // 双指针
    public function isSubsequence2($s, $t) {
        $len1 = strlen($s);
        $len2 = strlen($t);
        $i = $j = 0;
        while ($i < $len1 && $j < $len2) {
            if ($s[$i] == $t[$j]) {
                $i++;
            }
            $j++;
        }
        if ($i == $len1) {
            return true;
        }
        return false;
    }

}
```

### go
```go
// DP
func isSubsequence(s string, t string) bool {
    len1, len2 := len(s), len(t)
    dp := make([][]int, len1 + 1)
    for k, _ := range dp {
        dp[k] = make([]int, len2 + 1)
    }
    for i := 1; i <= len1; i++ {
        for j := 1; j <= len2; j++ {
            if s[i - 1] == t [j - 1] {
                dp[i][j] = dp[i - 1][j - 1] + 1
            } else {
                dp[i][j] = dp[i][j - 1]
            }
        }
    }
    if dp[len1][len2] == len1 {
        return true
    }
    return false
}

// 双指针
func isSubsequence(s string, t string) bool {
    len1, len2 := len(s), len(t)
    i, j := 0, 0
    for i < len1 && j < len2 {
        if s[i] == t[j] {
            i++
        }
        j++
    }
    if i == len1 {
        return true
    }
    return false
}
```

### java
```java
class Solution {

    // DP
    public boolean isSubsequence(String s, String t) {
        int len1 = s.length();
        int len2 = t.length();
        int[][] dp = new int[len1 + 1][len2 + 1];
        for (int i = 1; i <= len1; i++) {
            for (int j = 1; j <= len2; j++) {
                if (s.charAt(i - 1) == t.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = dp[i][j - 1];
                }
            }
        }
        if (dp[len1][len2] == len1) {
            return true;
        }

        return false;
    }

    // 双指针
    public boolean isSubsequence2(String s, String t) {
        int len1 = s.length();
        int len2 = t.length();
        int i = 0, j = 0;
        while (i < len1 && j < len2) {
            if (s.charAt(i) == t.charAt(j)) {
                i++;
            }
            j++;
        }
        if (i == len1) {
            return true;
        }
        
        return false;
    }
}
```