# 474. 一和零

# 题目
给你一个二进制字符串数组 strs 和两个整数 m 和 n 。
请你找出并返回 strs 的最大子集的长度，该子集中 最多 有 m 个 0 和 n 个 1 。
如果 x 的所有元素也是 y 的元素，集合 x 是集合 y 的 子集 。

https://leetcode.cn/problems/ones-and-zeroes/

提示：
- 1 <= strs.length <= 600
- 1 <= strs[i].length <= 100
- strs[i] 仅由 '0' 和 '1' 组成
- 1 <= m, n <= 100

# 示例：
```
输入：strs = ["10", "0001", "111001", "1", "0"], m = 5, n = 3
输出：4
解释：最多有 5 个 0 和 3 个 1 的最大子集是 {"10","0001","1","0"} ，因此答案是 4 。
其他满足题意但较小的子集包括 {"0001","1"} 和 {"10","1","0"} 。{"111001"} 不满足题意，因为它含 4 个 1 ，大于 n 的值 3 。
```

```
输入：strs = ["10", "0", "1"], m = 1, n = 1
输出：2
解释：最大的子集是 {"0", "1"} ，所以答案是 2 。
```



# 解析
本题有可能会被认为是多重背包，一些题解也是这么写的。其实本题并不是多重背包，再来看一下这个图，捋清几种背包的关系

![](./images/bagsProblem-img1.png)

多重背包是每个物品，数量不同的情况。

本题中 strs 数组里的元素就是物品，每个物品都是一个！而 m 和 n 相当于是一个背包，两个维度的背包。

会理解成多重背包主要是把 m 和 n 混淆为物品了，感觉这是不同数量的物品，所以以为是多重背包。但本题其实是 0-1 背包问题。
只不过这个背包有两个维度，一个是 m 一个是 n，而不同长度的字符串就是不同大小的待装物品。

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
dp[i][j]：最多有 i 个 0 和 j 个 1 的 strs 的最大子集的大小为 dp[i][j]

### 确定递推公式
dp[i][j] 可以由前一个 strs 里的字符串推导出来，strs 里的字符串有 zeroNum 个 0，oneNum 个 1。

dp[i][j] 就可以是 dp[i - zeroNum][j - oneNum] + 1。

然后在遍历的过程中，取 dp[i][j] 的最大值。所以递推公式：dp[i][j] = max(dp[i][j], dp[i - zeroNum][j - oneNum] + 1);

此时可以回想一下 0-1 背包的递推公式：dp[j] = max(dp[j], dp[j - weight[i]] + value[i]);

对比一下就会发现，字符串的 zeroNum 和 oneNum 相当于物品的重量（weight[i]），字符串本身的个数相当于物品的价值（value[i]）。
这就是一个典型的 0-1 背包，只不过物品的重量有了两个维度而已。

### dp 数组初始化
在 0-1 背包的 dp 数组初始化为0就可以。因为物品价值不会是负数，初始为 0，保证递推的时候 dp[i][j] 不会被初始值覆盖。

### 确定遍历顺序
在动态规划：关于 0-1 背包问题一定是外层循环遍历物品，内层循环遍历背包容量且从后向前遍历.

那么本题也是，物品就是 strs 里的字符串，背包容量就是题目描述中的 m 和 n。


# 代码

### php
```php
class LeetCode0474 {
    
    public function findMaxForm($strs, $m, $n) {
        $dp = array_fill(0, $m + 1, array_fill(0, $n + 1, 0));
        foreach ($strs as $str) {
            $oneNum = $zeroNum = 0;
            $arr = str_split($str);
            foreach ($arr as $c) {
                if ($c == '0') $zeroNum++;
                else $oneNum++;
            }
            for ($i = $m; $i >= $zeroNum; $i--) {   // 从后向前遍历背包
                for ($j = $n; $j >= $oneNum; $j--) {
                    $dp[$i][$j] = max($dp[$i][$j], $dp[$i - $zeroNum][$j - $oneNum] + 1);
                }
            }
        }
        return $dp[$m][$n];
    }

}
```

### go
```go
func findMaxForm(strs []string, m int, n int) int {
	dp := make([][]int, m + 1)
	for k, _ := range dp {
		dp[k] = make([]int, n + 1)
	}
	for _, str := range strs {
		oneNum, zeroNum := 0, 0
		arr := strings.Split(str, "")
		for _, c := range arr {
			if c == "0" {
				zeroNum++
			} else {
				oneNum++
			}
		}
		for i := m; i >= zeroNum; i-- {
			for j := n; j >= oneNum; j-- {
				dp[i][j] = max(dp[i][j], dp[i - zeroNum][j - oneNum] + 1)
			}
		}
	}
	return dp[m][n]
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
```