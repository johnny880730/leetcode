# 0064. 最小路径和


# 题目
给定一个包含非负整数的 m x n 网格 grid ，请找出一条从左上角到右下角的路径，使得路径上的数字总和为最小。

说明：每次只能向下或者向右移动一步。

https://leetcode.cn/problems/minimum-path-sum/description/

提示：
- m == grid.length
- n == grid[i].length
- 1 <= m, n <= 200
- 0 <= grid[i][j] <= 200

# 示例
```
输入：grid = [[1,3,1],[1,5,1],[4,2,1]]
输出：7
解释：因为路径 1→3→1→1→1 的总和最小。
```
```
输入：grid = [[1,2,3],[4,5,6]]
输出：12
```

# 解析

## 动态规划
从左上角的起点走到右下角的终点，题目说了只能向右或者向下走。那么终点的前一步必定是终点上方的格子，或者是终点左方的格子。
因此问题也就转化成了：从起点出发，走到终点的左方格子的路径和小，还是走到终点上方的路径和小？这个也就是状态转移了。

### 状态定义
设 dp 为大小 m × n  矩阵，其中 dp[i][j] 的值代表直到走到 (i, j) 的最小路径和。

### 转移方程
走到当前单元格 (i, j)的最小路径和 = "从上方单元格 (i - 1, j) 来的 和 从左方单元格(i, j - 1) 来的两个路径和中的较小值"，
加上当前单元格的值。具体分为以下四种情况：
- 当左边和上边都不是边界， `dp[i][j] = min(dp[i - 1][j], dp[i][j - 1]) + grid[i][j]` 
- 当只有左边是边界时，只能从上面来， `dp[i][j] = dp[i - 1][j] + grid[i][j]` 
- 当只有上边时边界时，只能从左边来， `dp[i][j] = dp[i][j - 1] + grid[i][j]` 
- 当上边左边都是边界时，其实就是起点， `dp[i][j] = grid[i][j]` 

### 初始状态
dp 初始化零值即可

### 返回值
返回 dp 矩阵右下角的值，即走到终点的最小路径和



# 代码

### php
```php
class Leetcode0064 {

    function minPathSum($grid) {
        $m = count($grid);
        $n = count($grid[0]);
        $dp = array_fill(0, $m, array_fill(0, $n, -1));
        $dp[0][0] = $grid[0][0];
        for ($i = 1; $i < $m; $i++) {
            $dp[$i][0] = $dp[$i - 1][0] + $grid[$i][0];
        }
        for ($i = 1; $i < $n; $i++) {
            $dp[0][$i] = $dp[0][$i - 1] + $grid[0][$i];
        }
        for ($i = 1; $i < $m; $i++) {
            for ($j = 1; $j < $n; $j++) {
                $dp[$i][$j] = min($dp[$i - 1][$j], $dp[$i][$j - 1]) + $grid[$i][$j];
            }
        }
        return $dp[$m - 1][$n - 1];
    }
}
```

### java
```java
class LeetCode0064 {

    public int minPathSum(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        int[][] dp = new int[m][n];
        dp[0][0] = grid[0][0];
        for (int i = 1; i < m; i++) {
            dp[i][0] = dp[i - 1][0] + grid[i][0];
        }
        for (int i = 1; i < n; i++) {
            dp[0][i] = dp[0][i - 1] + grid[0][i];
        }
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = Math.min(dp[i - 1][j], dp[i][j - 1]) + grid[i][j];
            }
        }
        return dp[m - 1][n - 1];
    }
}
```

### go
```go
func minPathSum(grid [][]int) int {
    m, n := len(grid), len(grid[0])
    dp := make([][]int, m)
    for k := range dp {
        dp[k] = make([]int, n)
    }
    dp[0][0] = grid[0][0]

    for i := 1; i < m; i++ {
        dp[i][0] = dp[i - 1][0] + grid[i][0];
    }
    for i := 1; i < n; i++ {
        dp[0][i] = dp[0][i - 1] + grid[0][i];
    }
    for i := 1; i < m; i++ {
        for j := 1; j < n; j++ {
            dp[i][j] = Min(dp[i - 1][j], dp[i][j - 1]) + grid[i][j];
        }
    }
    return dp[m - 1][n - 1];

}

func Min(a int, b int) int {
    if a < b {
        return a
    }
    return b
}
```
