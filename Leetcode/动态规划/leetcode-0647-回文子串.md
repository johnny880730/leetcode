# 647. 回文子串

# 题目
给你一个字符串 s ，请你统计并返回这个字符串中 回文子串 的数目。

回文字符串 是正着读和倒过来读一样的字符串。

子字符串 是字符串中的由连续字符组成的一个序列。

具有不同开始位置或结束位置的子串，即使是由相同的字符组成，也会被视作不同的子串。

https://leetcode.cn/problems/palindromic-substrings


提示：
- 1 <= s.length <= 1000
- s 由小写英文字母组成


# 示例：
```
输入：s = "abc"
输出：3
解释：三个回文子串: "a", "b", "c"
```
```
输入：s = "aaa"
输出：6
解释：6个回文子串: "a", "a", "a", "aa", "aa", "aaa"
```



# 解析

## 动态规划

### 确定dp数组及下标的含义
如果做了很多这种子序列相关的题目，在定义 dp 数组的时候很自然就会想题目求什么，就如何定义dp数组。

绝大多数题目确实是这样，不过本题如果定义，dp[i] 为下标 i 结尾的字符串有 dp[i] 个回文串的话，会发现很难找到递归关系。

dp[i] 和 dp[i - 1] ，dp[i + 1] 看上去都没啥关系。

在判断字符串 S 是否是回文，那么如果知道 s[1]，s[2]，s[3] 这个子串是回文的，那么只需要比较 s[0] 和 s[4] 这两个元素是否相同，
如果相同的话，这个字符串 s 就是回文串。

那么此时是不是能找到一种递归关系，也就是判断一个子字符串（字符串的下标范围 [i, j]）是否回文，依赖于，子字符串（下标范围[i + 1, j - 1]）
是否是回文。

所以为了明确这种递归关系，dp 数组是要定义成一位二维dp数组。

布尔类型的 dp[i][j]：表示区间范围 [i, j] （左闭右闭）内的子串是不是回文串，是为 true，否则为 false。

### 确定递推公式
当 s[i] != s[j] 时，dp[i][j] 一定是 false。

当 s[i] == s[j] 时，有如下三种情况：
- 情况一：下标 i 与 j 相同，即同一个字符，例如 “a”，当然是回文子串。
- 情况二：下标 i 与 j 相差 1（即相邻），例如 “aa”，也是回文子串
- 情况三：下标 i 与 j 相差大于 1（即不相邻），例如 “cabac”，此时 s[i] 与 s[j] 已经相同了，要判断字符串在 [i, j] 区间是否为回文子串，
就要看 “aba” 是否为回文子串，那么 “aba” 的区间就是 [i + 1, j - 1] ，所以 [i, j] 区间是否回文就看 dp[i + 1][j - 1] 是否为 true。

### 初始化dp数组
dp[i][j] 初始化都为 false，默认都不是回文子串。

### 确定遍历顺序
从递推公式可以看出，情况三是 dp[i + 1][j - 1]，这个值在 dp[i][j] 的左下角，所以要从下到上，从左到右遍历，

这样才能保证 dp[i + 1][j - 1] 是经过计算的。

### 举例推导 dp 数组
输入："aaa"，dp[i][j] 状态如下：

![](./images/leetcode-0647-img1.png)

注意因为 dp[i][j] 的定义，所以 j 一定是大于等于 i 的，那么在填充 dp[i][j] 的时候一定是只填充右上半部分。



# 代码

### php
```php
class LeetCode0647 {

    public function countSubstrings($s) {
        $len = strlen($s);
        $dp = array_fill(0, $len + 1, array_fill(0, $len + 1, false));
        $res = 0;
        for ($i = $len - 1; $i >= 0; $i--) {
            for ($j = $i; $j < $len; $j++) {
                if ($s[$i] != $s[$j]) {
                    continue;
                }
                if (abs($j - $i) <= 1) {
                    $res++;
                    $dp[$i][$j] = true;
                } else if ($dp[$i + 1][$j - 1] == true) {
                    $res++;
                    $dp[$i][$j] = true;
                }
            }
        }
        return $res;
    }

}
```

### java
```java
class LeetCode0647 {
    
    public int countSubstrings(String s) {
        int len = s.length();
        boolean[][] dp = new boolean[len + 1][len + 1];
        int res = 0;
        for (int i = len - 1; i >= 0; i--) {
            for (int j = i ; j < len; j++) {
                if (s.charAt(i) != s.charAt(j)) {
                    continue;
                }
                if (Math.abs(j - i) <= 1) {
                    res++;
                    dp[i][j] = true;
                } else if (dp[i + 1][j - 1] == true) {
                    res++;
                    dp[i][j] = true;
                }
            }
        }
        return res;
    }
}
```

### go
```go
func countSubstrings(s string) int {
    size := len(s)
    dp := make([][]bool, size + 1)
    for k, _ := range dp {
        dp[k] = make([]bool, size + 1)
    }
    res := 0
    for i := size - 1; i >= 0; i-- {
        for j := i ; j < size; j++ {
            if s[i] != s[j] {
                continue
            }
            if math.Abs(float64(j - i)) <= 1.0 {
                res++
                dp[i][j] = true
            } else if (dp[i + 1][j - 1] == true) {
                res++
                dp[i][j] = true
            }
        }
    }
    return res
}
```

