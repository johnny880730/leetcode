# 673. 最长递增子序列的个数

# 题目
给定一个未排序的整数数组 nums ， 返回最长递增子序列的个数 。

注意 这个数列必须是 严格 递增的。

https://leetcode.cn/problems/number-of-longest-increasing-subsequence/

提示：
- 1 <= nums.length <= 2000
- -10^6 <= nums[i] <= 10^6

# 示例
```
输入: [1,3,5,4,7]
输出: 2
解释: 有两个最长递增子序列，分别是 [1, 3, 4, 7] 和[1, 3, 5, 7]。
```
```
输入: [2,2,2,2,2]
输出: 5
解释: 最长递增子序列的长度是1，并且存在5个子序列的长度为1，因此输出5。
```

# 解析
这道题可以说是 [300. 最长上升子序列](../top-interview-questions/leetcode-0300-中等-最长递增子序列.md) 的进阶版本

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
这道题目要一起维护两个数组。
- dp[i]：i 之前（包括 i ）最长递增子序列的长度为 dp[i]
- count[i]：以 nums[i] 为结尾的最长递增子序列的个数为 count[i]

### 确定递推公式
在《300. 最长上升子序列》 中给出的状态转移是： if (nums[i] > nums[j]) dp[i] = max(dp[i], dp[j] + 1);

即：位置 i 的最长递增子序列长度等于 j 从 0到 i - 1 各个位置的最长升序子序列 +1 的最大值。

本题就没那么简单了，要考虑两个维度，一个是 dp[i] 的更新，一个是 count[i] 的更新。

那么如何更新 count[i] 呢？

以 nums[i] 为结尾的最长递增子序列的个数为 count[i]。那么在 nums[i] > nums[j] 前提下，如果在 [0, i - 1] 的范围内，找到了 j，
使得 dp[j] + 1 > dp[i]，说明找到了一个更长的递增子序列。那么以 j 为结尾的最长递增子序列的个数，就是最新的以 i 为结尾的子串的最长递增子序列的个数，
即：count[i] = count[j]。

在 nums[i] > nums[j] 前提下，如果在 [0, i - 1] 的范围内，找到了 j，使得 dp[j] + 1 == dp[i]，说明找到了两个相同长度的递增子序列。
那么以 i 为结尾的最长递增子序列的个数就应该加上以 j 为结尾的最长递增子序列的个数，即：count[i] += count[j];

```
if (nums[i] > nums[j]) {
    if (dp[j] + 1 > dp[i]) {
        count[i] = count[j];
    } else if (dp[j] + 1 == dp[i]) {
        count[i] += count[j];
    }
    dp[i] = max(dp[i], dp[j] + 1);
}
```

这里 count[i] 记录了以 nums[i] 为结尾的最长递增子序列的个数。dp[i] 记录了 i 之前（包括 i ）最长递增序列的长度。

题目要求最长递增序列的长度的个数，应该把最长长度记录下来。

### dp 数组初始化
再回顾一下 dp[i] 和 count[i] 的定义:
- count[i]：记录了以 nums[i] 为结尾的最长递增子序列的个数。那么最少也就是 1 个，所以 count[i] 初始为 1。
- dp[i]：记录了 i 之前（包括 i ）最长递增序列的长度。最小的长度也是 1，所以 dp[i] 初始为 1。

其实动规的题目中，初始化很有讲究，也很考察对 dp 数组定义的理解。

### 确定遍历顺序
dp[i] 是由 0 到 i - 1 各个位置的最长升序子序列推导而来，那么遍历 i 一定是从前向后遍历。

j 其实就是 0 到 i - 1，遍历 i 的循环在外层，遍历 j 则在内层

最后还有再遍历一遍 dp[i]，把最长递增序列长度对应的 count[i] 累计下来就是结果了。

### 举例推导 dp 数组
输入：[1, 3, 5, 4, 7]

![](./images/leetcode-0673-img1.png)




# 代码

### php
```php
class Leetcode0673 {

    function findNumberOfLIS($nums) {
        $len = count($nums);
        if ($len <= 1) {
            return $len;
        }
        $dp = array_fill(0, $len ,1);
        $cnt = array_fill(0, $len, 1);
        $maxCount = 0;
        for ($i = 1; $i < $len; $i++) {
            for ($j = 0; $j < $i; $j++) {
                if ($nums[$i] > $nums[$j]) {
                    if ($dp[$j] + 1 > $dp[$i]) {
                        $cnt[$i] = $cnt[$j];
                    } else if ($dp[$j] + 1 == $dp[$i]) {
                        $cnt[$i] += $cnt[$j];
                    }
                    $dp[$i] = max($dp[$i], $dp[$j] + 1);
                }
                if ($dp[$i] > $maxCount) {
                    $maxCount = $dp[$i];
                }                
            }
        }

        $res = 0;
        for ($i = 0; $i < $len; $i++) {
            if ($maxCount == $dp[$i]) {
                $res += $cnt[$i];
            }
        }
        return $res;
    }

}
```

### go
```go
```