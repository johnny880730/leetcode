# 674. 最长连续递增序列
给定一个未经排序的整数数组，找到最长且连续递增的子序列，并返回该序列的长度。
连续递增的子序列 可以由两个下标 l 和 r（l < r）确定，如果对于每个 l <= i < r，
都有 nums[i] < nums[i + 1] ，那么子序列 [nums[l], nums[l + 1], ..., nums[r - 1], nums[r]] 就是连续递增子序列。

https://leetcode.cn/problems/longest-continuous-increasing-subsequence/

提示：
1 <= nums.length <= 10^4
-10^9 <= nums[i] <= 10^9


# 示例
```
输入：nums = [1,3,5,4,7]
输出：3
解释：最长连续递增序列是 [1,3,5], 长度为3。
尽管 [1,3,5,7] 也是升序的子序列, 但它不是连续的，因为 5 和 7 在原数组里被 4 隔开。
```
```
输入：nums = [2,2,2,2,2]
输出：1
解释：最长连续递增序列是 [2], 长度为1。
```

# 解析
本题相对于 《300. 最长递增子序列》 最大的区别在于“连续”。

本题要求的是最长连续递增序列

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
dp[i]：以 nums[i] 结尾的连续递增的子序列长度为 dp[i]。

注意这里的定义，一定是以 nums[i] 为结尾，并不是说一定以 nums[0] 起始。

### 确定递推公式
如果 nums[i] > nums[i - 1]，那么以 nums[i] 为结尾的连续递增的子序列长度一定等于 以 nums[i - 1] 结尾的连续递增的子序列长度 + 1 。
即：dp[i] = dp[i - 1] + 1;

注意这里和 《300. 最长递增子序列》的区别。

因为本题要求连续递增子序列，所以就只要比较 nums[i] 与 nums[i - 1]，而不用去比较 nums[j] 与 nums[i] （j 是在 0 到 i 之间遍历）。
既然不用 j 了，那么也不用两层 for 循环，本题一层 for 循环就行，比较 nums[i] 和 nums[i - 1]。

### dp 数组初始化
以 nums[i] 结尾的连续递增的子序列长度最少也应该是 1，即就是 nums[i] 这一个元素。

所以 dp[i] 应该初始 1;

### 确定遍历顺序
从递推公式上可以看出， dp[i + 1] 依赖 dp[i]，所以一定是从前向后遍历。

### 举例推导 dp 数组
以输入 nums = [1, 3, 5, 4, 7]为例，dp 数组状态如下：

![](./images/leetcode-0674-img1.png)


# 代码

### php
```php
class Leetcode0674 {

    public function findLengthOfLCIS($nums) {
        $res = 1;
        $len = count($nums);
        $dp = array_fill(0, $len, 1);
        for ($i = 1; $i < $len; $i++) {
            if ($nums[$i] > $nums[$i - 1]) {
                $dp[$i] = $dp[$i - 1] + 1;
            }
            if ($dp[$i] > $res) {
                $res = $dp[$i];
            }
        }
        return $res;
    }

}
```

### go
```go
func findLengthOfLCIS(nums []int) int {
    size := len(nums)
    dp := make([]int, size)
    for k, _ := range dp {
        dp[k] = 1
    }
    res := 1
    for i := 1; i < size; i++ {
        if nums[i] > nums[i - 1] {
            dp[i] = dp[i - 1] + 1
        }
        if dp[i] > res {
            res = dp[i]
        }
    }

    return res
}
```