# 746. 使用最小花费爬楼梯

# 题目
给你一个整数数组 cost ，其中 cost[i] 是从楼梯第 i 个台阶向上爬需要支付的费用。一旦你支付此费用，即可选择向上爬一个或者两个台阶。

你可以选择从下标为 0 或下标为 1 的台阶开始爬楼梯。

请你计算并返回达到楼梯顶部的最低花费。

https://leetcode.cn/problems/min-cost-climbing-stairs/

提示：
- 2 <= cost.length <= 1000
- 0 <= cost[i] <= 999

# 示例
```
输入：cost = [10, 15, 20]
输出：15
解释：最低花费是从 cost[1] 开始，然后走两步即可到阶梯顶，一共花费 15 。
```
```
输入：cost = [1, 100, 1, 1, 1, 100, 1, 1, 100, 1]
输出：6
解释：最低花费方式是从 cost[0] 开始，逐个经过那些 1 ，跳过 cost[3] ，一共花费 6 。
```

# 解析

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
使用动态规划，就要有一个数组来记录状态，本题只需要一个一维数组 dp[i] 就可以了。

dp[i] 的定义：到达第 i 台阶所花费的最少体力为 dp[i]。

### 确定递推公式
可以有两个途径得到 dp[i]，一个是 dp[i - 1] 一个是 dp[i - 2]
- dp[i - 1] 跳到  dp[i] 需要花费  dp[i - 1] + cost[i - 1]。
- dp[i - 2] 跳到  dp[i] 需要花费  dp[i - 2] + cost[i - 2]。

那么究竟是选从 dp[i - 1] 跳还是从 dp[i - 2] 跳呢？

一定是选最小的，所以 dp[i] = min(dp[i - 1] + cost[i - 1],  dp[i - 2] + cost[i - 2])

### dp 数组如何初始化
看一下递归公式， dp[i] 由 dp[i - 1]， dp[i - 2] 推出，既然初始化所有的 dp[i] 是不可能的，那么只初始化 dp[0] 和 dp[1]就够了，
其他的最终都是 dp[0]、dp[1] 推出。

那么  dp[0] 应该是多少呢？ 根据 dp 数组的定义，到达第 0 台阶所花费的最小体力为 dp[0]，那么有同学可能想，那 dp[0] 应该是 cost[0]，
例如 cost = [1, 100, 1, 1, 1, 100, 1, 1, 100, 1] 的话， dp[0] 就是 cost[0] 应该是 1。

但题目描述中明确说了 “你可以选择从下标为 0 或下标为 1 的台阶开始爬楼梯。” 也就是说到达第 0 个台阶是不花费的，但从第 0 个台阶往上跳的话，
需要花费 cost[0]。

所以初始化  dp[0] = 0， dp[1] = 0;

### 确定遍历顺序
最后一步，递归公式有了，初始化有了，如何遍历呢？

本题的遍历顺序其实比较简单，简单到很多同学都忽略了思考这一步直接就把代码写出来了。

因为是模拟台阶，而且 dp[i] 由 dp[i - 1]、dp[i - 2]推出，所以是从前到后遍历 cost 数组就可以了。

### 举例推导 dp 数组
拿示例2：cost = [1, 100, 1, 1, 1, 100, 1, 1, 100, 1] ，来模拟一下dp数组的状态变化，如下：

![](./images/leetcode-0746-img1.png)

# 代码
```php
class LeetCode0746 {

    function minCostClimbingStairs($cost) {
        $dp[0] = $dp[1] = 0;
        $len = count($cost);
        for ($i = 2; $i <= $len; $i++) {
            $dp[$i] = min($dp[$i - 1] + $cost[$i - 1], $dp[$i - 2] + $cost[$i - 2]);
        }
        return $dp[$len];
    }

}
```

### go
```go
func minCostClimbingStairs(cost []int) int {
    size := len(cost)
    dp := make([]int, size + 1)
    for i := 2; i <= size; i++ {
        dp[i] = min(dp[i - 2] + cost[i - 2], dp[i - 1] + cost[i - 1])
    }
    return dp[size]
}

func min(a, b int) int {
    if a < b {
        return a
    }
    return b
}
```