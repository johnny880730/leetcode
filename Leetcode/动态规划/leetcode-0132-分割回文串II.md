# 132. 分割回文串 II

# 题目
给你一个字符串 s，请你将 s 分割成一些子串，使每个子串都是回文。

返回符合要求的 最少分割次数 。

https://leetcode.cn/problems/palindrome-partitioning-ii/

提示：
- 1 <= s.length <= 2000
- s 仅由小写英文字母组成

# 示例
```
输入：s = "aab"
输出：1
解释：只需一次分割就可将 s 分割成 ["aa","b"] 这样两个回文子串。
```
```
输入：s = "a"
输出：0
```
```
输入：s = "ab"
输出：1
```

# 解析
关于回文子串，两道题目题目大家是一定要掌握的。
- [647. 回文子串](./leetcode-0647-回文子串.md)
- [005. 最长回文子串](../top-interview-questions/leetcode-0005-中等-最长回文子串.md) 和 《647. 回文子串基本一样》的

这两道题目是回文子串的基础题目，本题也要用到相关的知识点。

## 动态规划五部曲

### 确定 dp 数组以及下标的含义
dp[i]：范围是 [0, i] 的回文子串，最少分割次数是 dp[i]。

### 确定递推公式
来看一下由什么可以推出 dp[i]。

如果要对长度为 [0, i] 的子串进行分割，分割点为 j。那么如果分割后，区间 [j + 1, i] 是回文子串，那么 dp[i] 就等于 dp[j] + 1。

为什么只看 [j + 1, i] 区间，不看 [0, j] 区间是不是回文子串呢？那么在回顾一下 dp[i] 的定义： 范围是 [0, i] 的回文子串，
最少分割次数是 dp[i]。 [0, j] 区间的最小切割数量，我们已经知道了就是 dp[j]。

此时就找到了递推关系，当切割点 j 在 [0, i] 之间时候，dp[i] = dp[j] + 1;

本题是要找到最少分割次数，所以遍历 j 的时候要取最小的 dp[i]。

所以最后递推公式为：dp[i] = min(dp[i], dp[j] + 1);

注意这里不是要 dp[j] + 1 和 dp[i]去比较，而是要在遍历 j 的过程中取最小的 dp[i]

可以有 dp[j] + 1 推出，当 [j + 1, i] 为回文子串

### dp 数组初始化
首先来看一下 dp[0] 应该是多少。

dp[i]： 范围是 [0, i] 的回文子串，最少分割次数是 dp[i]。那么 dp[0] 一定是 0，长度为 1 的字符串最小分割次数就是 0。这个是比较直观的。

非零下标的 dp[i] 应该初始化为多少？

在递推公式 dp[i] = min(dp[i], dp[j] + 1) 中我们可以看出每次要取最小的 dp[i]。那么非零下标的 dp[i] 就应该初始化为一个最大数，
这样递推公式在计算结果的时候才不会被初始值覆盖。如果非零下标的 dp[i] 初始化为 0，在那么在递推公式中，所有数值将都是零。

其实也可以这样初始化，根据 dp[i] 的定义，dp[i] 的最大值其实就是 i，也就是把每个字符分割出来。

### 确定遍历顺序
根据递推公式：dp[i] = min(dp[i], dp[j] + 1)

j 是在 [0，i] 之间，所以遍历 i 的 for 循环一定在外层，这里遍历 j 的 for 循环在内层才能通过计算过的 dp[j] 数值推导出 dp[i]。

代码如下：
```
for (int i = 1; i < s.size(); i++) {
    if (isPalindromic[0][i]) { // 判断是不是回文子串
        dp[i] = 0;
        continue;
    }
    for (int j = 0; j < i; j++) {
        if (isPalindromic[j + 1][i]) {
            dp[i] = min(dp[i], dp[j] + 1);
        }
    }
}
```

会发现代码里有一个 isPalindromic ，这是一个二维数组 isPalindromic[i][j]，记录 [i, j] 是不是回文子串。

那么这个 isPalindromic[i][j] 是怎么的代码的呢？就是其实这两道题目的代码：
- 《647. 回文子串》
- 《005. 最长回文子串》

所以先用一个二维数组来保存整个字符串的回文情况。

### 举例推导 dp 数组
以输入："aabc" 为例：

![](./images/leetcode-0132-img1.png)


# 代码

### php
```php
class Leetcode0132 {
    
    function minCut($s) {
        $len = strlen($s);
        // 记录子串 [i..j] 是否是回文串
        $isPalindromic = array_fill(0, $len, array_fill(0, $len, false));
        for ($i = $len - 1; $i >= 0; $i--) {
            for ($j = $i; $j < $len; $j++) {
                if ($s[$i] == $s[$j] && ($j - $i <= 1 || $isPalindromic[$i + 1][$j - 1])) {
                    $isPalindromic[$i][$j] = true;
                }
            }
        }
        // dp[i] 表示[0..i]的最小分割次数
        $dp = array_fill(0, $len, 0);
        for ($i = 0; $i < $len; $i++) {
            //初始考虑最坏的情况。 1个字符分割0次， len个字符分割 len - 1次
            $dp[$i] = $i;
        }

        for ($i = 1; $i < $len; $i++) {
            if ($isPalindromic[0][$i]) {
                // s[0..i]是回文了，那 dp[i] = 0, 一次也不用分割
                $dp[$i] = 0;
                continue;
            }
            for ($j = 0; $j < $i; $j++) {
                // 按文中的思路，不清楚就拿 "ababa" 为例，先写出 isPalindromic 数组，再进行求解
                if ($isPalindromic[$j + 1][$i]) {
                    $dp[$i] = min($dp[$i], $dp[$j] + 1);
                }
            }
        }

        return $dp[$len - 1];
    }
}
```

### go
```go
```