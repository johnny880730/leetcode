# 0053. 最大子数组和

# 题目
给定一个整数数组 nums ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。

子数组 是数组中的一个连续部分。

https://leetcode.cn/problems/maximum-subarray/description/

提示：
- 1 <= nums.length <= 100,000
- -10,000 <= nums[i] <= 10,000


# 示例：
```
示例 1：

输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
输出：6
解释：连续子数组 [4,-1,2,1] 的和最大，为 6 。
```

```
示例 2：

输入：nums = [1]
输出：1
```

```
示例 3：

输入：nums = [5,4,-1,7,8]
输出：23
```

# 解析


## 动态规划

### 确定dp数组及下标含义
dp[i]：包括 nums[i] 之前的最大连续子序列和

### 确定递推公式。
dp[i] 可以通过两个方向推导出来：
- dp[i - 1] + nums[i]，即 nums[i] 加入当前连续子序列
- nums[i]，即从当前项开始重新计算当前连续子序列。
  
因此：dp[i] = max( dp[i - 1] + nums[i], nums[i] )

### 初始化dp数组。
从递推公式可以看出，dp[i] 依赖于 dp[i - 1]的状态，dp[0] 就是递推公式的基础。

根据 dp 的定义，dp[0] 就是 nums[0]，即 dp[0] = nums[0]

### 确定遍历顺序。
因为 dp[i] 依赖于 dp[i - 1] 的状态，所以需要从前往后遍历

### 举例推导 dp 数组
以示例一为例，输入：nums = [-2, 1, -3, 4, -1, 2, 1, -5, 4]，对应的 dp 状态如下：

![](./images/leetcode-0053-img1.png)



# 代码

### php
```php
class LeetCode0053 {

    public function maxSubArray2($nums) {
        $len = count($nums);
        $dp = array_fill(0, $len, 0);
        $dp[0] = $nums[0];
        $res = $dp[0];
        for ($i = 1; $i < $len; $i++) {
            $dp[$i] = max($dp[$i - 1] + $nums[$i], $nums[$i]);
            $res = max($res, $dp[$i]);
        }
        return $res;
    }
}
```

### go
```go
func maxSubArray(nums []int) int {
    dp := make([]int, len(nums))
    dp[0] = nums[0]
    res := dp[0]
    for i := 1; i < len(nums); i++ {
        dp[i] = Max(dp[i - 1] + nums[i], nums[i])
        res = Max(res, dp[i])
    }
    return res
}

func Max(a int, b int) int {
    if a > b {
        return a
    }
    return b
}
```

### java
```java
class LeetCode0053 {

    public int maxSubArray(int[] nums) {
        if (nums.length <= 0) {
            return 0;
        }
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        int res = dp[0];
        for (int i = 1; i < nums.length; i++) {
            dp[i] = Math.max(nums[i], nums[i] + dp[i - 1]);
            res = Math.max(res, dp[i]);
        }
        return res;
    }
}
```

