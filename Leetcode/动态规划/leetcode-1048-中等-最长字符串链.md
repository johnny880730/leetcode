# 1048. 最长字符串链

# 题目
给出一个单词数组 words ，其中每个单词都由小写英文字母组成。

如果我们可以 不改变其他字符的顺序 ，在 wordA 的任何地方添加 恰好一个 字母使其变成 wordB ，那么我们认为 wordA 是 wordB 的 前身 。
- 例如，"abc" 是 "abac" 的 前身 ，而 "cba" 不是 "bcad" 的 前身

词链是单词 [word_1, word_2, ..., word_k] 组成的序列，k >= 1，其中 word1 是 word2 的前身，word2 是 word3 的前身，依此类推。
一个单词通常是 k == 1 的 单词链 。

从给定单词列表 words 中选择单词组成词链，返回 词链的 最长可能长度 。

https://leetcode.cn/problems/longest-string-chain/description/


提示：
- 1 <= words.length <= 1000
- 1 <= words[i].length <= 16
- words[i] 仅由小写英文字母组成。

# 示例
```
示例 1：

输入：words = ["a","b","ba","bca","bda","bdca"]
输出：4
解释：最长单词链之一为 ["a","ba","bda","bdca"]
```
```
示例 2:

输入：words = ["xbc","pcxbcf","xb","cxbc","pcxbc"]
输出：5
解释：所有的单词都可以放入单词链 ["xb", "xbc", "cxbc", "pcxbc", "pcxbcf"].
```
```
示例 3:

输入：words = ["abcd","dbqca"]
输出：1
解释：字链["abcd"]是最长的字链之一。
["abcd"，"dbqca"]不是一个有效的单词链，因为字母的顺序被改变了。
```

# 解析

## 记忆化搜索
对于字符串 s 来说，假设它是词链的最后一个单词，那么去掉 s 中的一个字母，设新字符串为 t，问题就变成计算以 t 结尾的词链的最长长度。
由于这是一个和原问题相似的子问题，因此可以用递归解决。

直接把字符串作为递归的参数，定义 dfs(s) 表示以 s 结尾的词链的最长长度。由于字符串的长度不超过 16，暴力枚举去掉的字符。
设新字符串为 t 且在 words 中，则有：dfs(s) = max{dfs(t)} + 1

为了快速判断字符串是否在 words 中，需要将所有 words[i] 存入哈希表中。

由于 "aba" 和 "aca" 去掉中间字母都会变成 "aa"，为避免重复计算，代码实现时可以用记忆化搜索。进一步地，可以直接把计算结果存到哈希表中


# 代码

### php
```php
class LeetCode1048 {

    public $hash = array();

    /**
     * @param String[] $words
     * @return Integer
     */
    function longestStrChain($words) {
        foreach ($words as $s) {
            $this->hash[$s] = 0;
        }
        $res = 0;
        foreach ($this->hash as $s => $n) {
            $res = max($res, $this->_dfs($s));
        }
        return $res;
    }

    private function _dfs($s) {
        $res = $this->hash[$s];
        if ($res > 0) {
            return $res;
        }
        for ($i = 0; $i < strlen($s); $i++) {
            $t = substr($s, 0, $i) . substr($s, $i + 1);
            if (array_key_exists($t, $this->hash)) {
                $res = max($res, $this->_dfs($t));
            }
        }
        $this->hash[$s] = $res + 1;
        return $res + 1;
    }
}
```

### java
```java
class LeetCode1048 {
    
    public Map<String, Integer> hash = new HashMap<>();

    public int longestStrChain(String[] words) {
        for (String s : words) {
            hash.put(s, 0);
        }
        int res = 0;
        for (String s: hash.keySet()) {
            res = Math.max(res, _dfs(s));
        }
        return res;
    }

    private int _dfs(String s) {
        int res = hash.getOrDefault(s, 0);
        if (res > 0) {
            return res;
        }
        for (int i = 0; i < s.length(); i++) {
            String t = s.substring(0, i) + s.substring(i + 1);
            if (hash.containsKey(t)) {
                res = Math.max(res, _dfs(t));
            }
        }
        hash.put(s, res + 1);
        return res + 1;
    }
}
```

### go
```go
var hash map[string]int

func longestStrChain(words []string) int {
    hash = make(map[string]int)
    for _, s := range words {
        hash[s] = 0
    }
    res := 0
    for s, _ := range hash {
        res = Max(res, _dfs(s))
    }
    return res
}

func _dfs(s string) int {
    res := hash[s]
    if res > 0 {
        return res
    }
    for i := 0; i < len(s); i++ {
        t := s[:i] + s[i + 1:]
        if _, ok := hash[t]; ok {
            res = Max(res, _dfs(t))
        }
    }
    hash[s] = res + 1
    return res + 1
}

func Max(a int, b int) int {
    if a > b {
        return a
    }
    return b
}
```
