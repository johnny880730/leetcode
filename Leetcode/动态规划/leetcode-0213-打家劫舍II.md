# 213. 打家劫舍 II

# 题目
你是一个专业的小偷，计划偷窃沿街的房屋，每间房内都藏有一定的现金。这个地方所有的房屋都 围成一圈 ，这意味着第一个房屋和最后一个房屋是紧挨着的。
同时，相邻的房屋装有相互连通的防盗系统，如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警 。

给定一个代表每个房屋存放金额的非负整数数组，计算你 在不触动警报装置的情况下 ，今晚能够偷窃到的最高金额

https://leetcode.cn/problems/house-robber-ii

提示：
- 1 <= nums.length <= 100
- 0 <= nums[i] <= 1000

## 示例
```
输入：nums = [2,3,2]
输出：3
解释：你不能先偷窃 1 号房屋（金额 = 2），然后偷窃 3 号房屋（金额 = 2）, 因为他们是相邻的。
```
```
输入：nums = [1,2,3,1]
输出：4
解释：你可以先偷窃 1 号房屋（金额 = 1），然后偷窃 3 号房屋（金额 = 3）。
     偷窃到的最高金额 = 1 + 3 = 4 。
```
```
输入：nums = [1,2,3]
输出：3
```

# 解析
这道题目和 《198.打家劫舍》 是差不多的，唯一区别就是成环了。

对于一个数组，成环的话主要有如下三种情况：
- 情况一：考虑不包含首尾元素
- 情况二：考虑包含首元素，不包含尾元素
- 情况三：考虑包含尾元素，不包含首元素

注意说的是“考虑”"，例如情况三，虽然是考虑包含尾元素，但不一定要选尾部元素。而情况二和情况三都包含了情况一了，所以只考虑情况二和情况三就可以了。

分析到这里，本题其实比较简单了。剩下的和 《198.打家劫舍》 就是一样的了。


# 代码
```php
class LeetCode0213 {

    function rob($nums) {
        $len = count($nums);
        if (!$len) {
            return 0;
        }
        if ($len == 1) {
            return $nums[0];
        }
        return max($this->_robRange($nums, 0, $len - 2), $this->_robRange($nums, 1, $len - 1));
    }
    
    function _robRange($nums, $start, $end) {
        if ($end == $start) {
            return $nums[$start];
        }
        $dp = array_fill(0, count($nums), 0);
        $dp[$start] = $nums[$start];
        $dp[$start + 1] = max($nums[$start], $nums[$start + 1]);
        for ($i = $start + 2; $i <= $end; $i++) {
            $dp[$i] = max($dp[$i - 1], $dp[$i - 2] + $nums[$i]);
        }
        return $dp[$end];
    }
}
```

### java
```java
class LeetCode0213 {
    
    public int rob(int[] nums) {
        int len = nums.length;
        if (len == 0) {
            return 0;
        }
        if (len == 1) {
            return nums[0];
        }
        return Math.max(_rob(nums, 0, len - 2), _rob(nums, 1, len - 1));
    }

    private int _rob(int[] nums, int start, int end) {
        if (start == end) {
            return nums[start];
        }
        int[] dp = new int[nums.length];
        dp[start] = nums[start];
        dp[start + 1] = Math.max(nums[start], nums[start + 1]);
        for (int i = start + 2; i <= end; i++) {
            dp[i] = Math.max(dp[i - 1], dp[i - 2] + nums[i]);
        }

        return dp[end];
    }
}

```

### go
```go
func rob(nums []int) int {
	size := len(nums)
	if size == 0 {
		return 0
	}
	if size == 1 {
		return nums[0]
	}
	return max(_robRange(nums, 0, size - 2), _robRange(nums, 1, size - 1))
}

func _robRange(nums []int, start, end int) int {
	if end == start {
		return nums[start]
	}
	dp := make([]int, len(nums))
	dp[start] = nums[start]
	dp[start + 1] = max(nums[start], nums[start + 1])
	for i := start + 2; i <= end; i++ {
		dp[i] = max(dp[i - 2] + nums[i], dp[i - 1])
	}
	return dp[end]
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
```