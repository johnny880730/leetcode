# 416. 分割等和子集

# 题目
给你一个 只包含正整数 的 非空 数组 nums 。请你判断是否可以将这个数组分割成两个子集，使得两个子集的元素和相等。

https://leetcode.cn/problems/partition-equal-subset-sum/

# 示例：
```
输入：nums = [1,5,11,5]
输出：true
解释：数组可以分割成 [1, 5, 5] 和 [11] 。
```

```
输入：nums = [1,2,3,5]
输出：false
解释：数组不能分割成两个元素和相等的子集。
```

提示：
- 1 <= nums.length <= 200
- 1 <= nums[i] <= 100

# 解析

## 动态规划 - 0-1背包问题
只有确定了如下四点，才能把 0-1 背包问题套到本题上来。
- 背包的体积为sum / 2
- 背包要放入的商品（集合里的元素）重量为元素的数值，价值也为元素的数值
- 背包如果正好装满，说明找到了总和为 sum / 2 的子集。
- 背包中每一个元素是不可重复放入。

以上分析完，就可以套用 0-1 背包，来解决这个问题了。

### 确定 dp 数组以及下标的含义
dp[i][j] = x：对于前 i 个物品（ i 从 1 开始计数），当前背包的容量为 j 时：
- 若 x 为 true，则说明可以恰好将背包装满；
- 若 x 为 false，则说明不能恰好将背包装满

比如 dp[4][9] = true，含义为：对于容量为 9 的背包，若只对前 4 个物品进行选择，可以有一种办法恰好装满（存在一个子集的和恰好凑出 9）

根据这个定义，想求的最终答案就是 dp[N][sum / 2]

### 确定递推公式
如果不把 nums[i] 装入背包，那么是否能够恰好装满背包，取决于上一个状态 dp[i - 1][j] 的结果。

如果把 nums[i] 装入背包，那么是否能够恰好装满背包，取悦于 dp[i - 1][j - nums[i - 1]]。换句话说，如果 j - nums[i - 1] 的重量可以恰好被装满，
那么只要把 nums[i] 装入背包，也可以恰好装满 j 的重量；否则，重量 j 是装不满的

注意：由于 dp 数组定义的 i 从 1 开始计数，所以第 i 个物品的重量应该是 nums[i - 1]

### dp 数组如何初始化
默认情况下肯定都是 false，但有一条 dp[..][0] = true，意思是背包没有空间的时候，就相当于装满了


# 代码

### php
```php
class LeetCode0416 {
    
    public function canPartition($nums) {
        $sum = array_sum($nums);
        if ($sum % 2 == 1) {
            return false;
        }
        $len = count($nums);
        $sum = $sum >> 1;
        $dp = array_fill(0, $len + 1, array_fill(0, $sum + 1, false));
        // 初始化
        for ($i = 0; $i <= $len; $i++) {
            $dp[$i][0] = true;
        }  
               
        for ($i = 1; $i <= $len; $i++) {
            for ($j = 1; $j <= $sum; $j++) {
                if ($j - $nums[$i - 1] < 0) {
                    // 背包容量不足，看上个物品的状态
                    $dp[$i][$j] = $dp[$i - 1][$j];
                } else {
                    // 装入或者不装入背包
                    $dp[$i][$j] = $dp[$i - 1][$j] || $dp[$i - 1][$j - $nums[$i - 1]];
                }
            }      
        }
        
        return $dp[$len][$sum];
    }

}
```

### java
```java
class Leetcode0416 {

    public boolean canPartition(int[] nums) {
        int sum = 0;
        for (int num: nums) {
            sum += num;
        }
        if (sum % 2 != 0) {
            return false;
        }
        sum = sum / 2;

        boolean[][] dp = new boolean[nums.length + 1][sum + 1];
        for (int i = 0; i <= nums.length; i++) {
            dp[i][0] = true;
        }

        for (int i = 1; i <= nums.length; i++) {
            for (int j = 1; j <= sum; j++) {
                if (j - nums[i - 1] < 0) {
                    dp[i][j] = dp[i - 1][j];
                } else {
                    dp[i][j] = dp[i - 1][j] || dp[i - 1][j - nums[i - 1]];
                }
            }
        }

        return dp[nums.length][sum];
    }
}
```

### go
```go
func canPartition(nums []int) bool {
    sum := 0
    for _, v := range nums {
        sum += v
    }
    if sum % 2 == 1 {
        return false
    }
    sum = sum >> 1

    dp := make([][]bool, len(nums) + 1)
    for i := 0; i < len(dp); i++ {
        dp[i] = make([]bool, sum + 1)
    }
    for i := 0; i <= len(nums); i++ {
        dp[i][0] = true
    }

    for i := 1; i <= len(nums); i++ {
        for j := 1; j <= sum; j++ {
            if j - nums[i - 1] < 0 {
                dp[i][j] = dp[i - 1][j]
            } else {
                dp[i][j] = dp[i - 1][j] || dp[i - 1][j - nums[i - 1]]
            }
        }
    }

    return dp[len(nums)][sum]
}
```
