# 0097. 交错字符串

# 题目
给定三个字符串 s1、s2、s3，请你帮忙验证 s3 是否是由 s1 和 s2 交错 组成的。

两个字符串 s 和 t 交错 的定义与过程如下，其中每个字符串都会被分割成若干 非空 子字符串：
- s = s1 + s2 + ... + sn
- t = t1 + t2 + ... + tm
- |n - m| <= 1
- 交错 是 s1 + t1 + s2 + t2 + s3 + t3 + ... 或者 t1 + s1 + t2 + s2 + t3 + s3 + ... 
  
注意：a + b 意味着字符串 a 和 b 连接。

https://leetcode.cn/problems/interleaving-string/description/

提示：
- 0 <= s1.length, s2.length <= 100
- 0 <= s3.length <= 200
- s1、s2、和 s3 都由小写英文字母组成


## 示例
```
输入：s1 = "aabcc", s2 = "dbbca", s3 = "aadbbcbcac"
输出：true
```
```
输入：s1 = "aabcc", s2 = "dbbca", s3 = "aadbbbaccc"
输出：false
```
```
输入：s1 = "", s2 = "", s3 = ""
输出：true
```

# 解析

## 动态规划

### DP 的定义
令 s1 长度为 n，s2 长度为 m

首先如果 n + m != s3 的长度，那么 s3 必然不可能由 s1 和 s2 交错组成。

### 状态转移方程
定义 dp[i][j] 表示 s1 的前 i 个元素和 s2 的前 j 个元素是否能交错组成 s3 的 前 i + j 个元素。

如果 s1 的第 i 个元素和 s3 的第 i + j 个元素相等，那么 s1 的前 i 个元素和 s2 的前 j 个元素是否能交错组成 s3 的 前 i + j 个元素取决于 s1 的前 i - 1 个元素和 s2 的前 j 个元素是否能交错组成 s3 的前 i +  j - 1 个元素，即 dp[i][j] 取决于 dp[i - 1][j]。

如果 dp[i - 1][j] 为真，dp[i][j] 也为真

同样的，如果 s2 的第 j 个元素和 s3 的第 i + j 个元素相等并且 dp[i][j - 1] 为真，则 dp[i][j] 为真。

### DP 的初始化
边界条件 dp[0][0] 为真，表示两个空字符串可以组成一个空字符串

初始化时，先假定不取 s2 的字符，即 j = 0，观察 s1 的 0 ~ i 个字符能否组成 s3 的 0 ~ i 个字符。然后反过来不取 s1 的字符，即 i = 0，观察 s2 的 0 ~ j 个字符能否组成 s3 的 0 ~ j 个字符。

# 代码

### php
```php
class LeetCode0097
{

    function isInterleave($s1, $s2, $s3) {
        $len1 = strlen($s1);
        $len2 = strlen($s2);
        $len3 = strlen($s3);
        if ($len3 != $len1 + $len2) {
            return false;
        }
        $dp = array_fill(0, $len1 + 1, array_fill(0, $len2 + 1, false));
        $dp[0][0] = true;
        for ($i = 1; $i <= $len1; $i++) {
            if ($s1[$i - 1] != $s3[$i - 1]) {
                break;
            }
            $dp[$i][0] = true;
        }
        for ($j = 1; $j <= $len2; $j++) {
            if ($s2[$j - 1] != $s3[$j - 1]) {
                break;
            }
            $dp[0][$j] = true;
        }
        for ($i = 1; $i <= $len1; $i++) {
            for ($j = 1; $j <= $len2; $j++) {
                if (
                    ($s1[$i - 1] == $s3[$i + $j - 1] && $dp[$i - 1][$j] == true)
                    ||
                    ($s2[$j - 1] == $s3[$i + $j - 1] && $dp[$i][$j - 1] == true)
                ) {
                    $dp[$i][$j] = true;
                }
            }
        }
        return $dp[$len1][$len2];
    }
}
```

### go
```go
func isInterleave(s1 string, s2 string, s3 string) bool {
	len1, len2, len3 := len(s1), len(s2), len(s3)
	if len3 != len1 + len2 {
		return false
	}
	dp := make([][]bool, len1 + 1)
	for i := range dp {
		dp[i] = make([]bool, len2 + 1)
	}
	dp[0][0] = true
	for i := 1; i <= len1; i++ {
		if s1[i - 1] != s3[i - 1] {
			break
		}
		dp[i][0] = true
	}
	for j := 1; j <= len2; j++ {
		if s2[j - 1] != s3[j - 1] {
			break
		}
		dp[0][j] = true
	}
	for i := 1; i <= len1; i++ {
		for j := 1; j <= len2; j++ {
			if (s1[i - 1] == s3[i + j - 1] && dp[i - 1][j]) || (s2[j - 1] == s3[i + j - 1] && dp[i][j - 1]) {
				dp[i][j] = true
			}
		}
	}
	return dp[len1][len2]
}
```

### java
```java
class LeetCode0097
{

    public boolean isInterleave(String s1, String s2, String s3) {
        int len1 = s1.length();
        int len2 = s2.length();
        int len3 = s3.length();
        if (len3 != len1 + len2) {
            return false;
        }
        boolean[][] dp = new boolean[len1 + 1][len2 + 1];
        dp[0][0] = true;
        for (int i = 1; i <= len1; i++) {
            if (s1.charAt(i - 1) != s3.charAt(i - 1)) {
                break;
            }
            dp[i][0] = true;
        }
        for (int j = 1; j <= len2; j++) {
            if (s2.charAt(j - 1) != s3.charAt(j - 1)) {
                break;
            }
            dp[0][j] = true;
        }
        for (int i = 1; i <= len1; i++) {
            for (int j = 1; j <= len2; j++) {
                if (
                    (s1.charAt(i - 1) == s3.charAt(i + j - 1) && dp[i - 1][j])
                    || 
                    (s2.charAt(j - 1) == s3.charAt(i + j - 1) && dp[i][j - 1])
                   ) {
                    dp[i][j] = true;
                }
            }
        }
        return dp[len1][len2];
    }
}
```