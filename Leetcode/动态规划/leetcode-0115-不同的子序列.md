# 115. 不同的子序列

# 题目
给定一个字符串 s 和一个字符串 t ，计算在 s 的子序列中 t 出现的个数。

字符串的一个 子序列 是指，通过删除一些（也可以不删除）字符且不干扰剩余字符相对位置所组成的新字符串。（例如，"ACE"是"ABCDE"的一个子序列，而"AEC"不是）

题目数据保证答案符合 32 位带符号整数范围。



# 示例：
```
输入：s = "rabbbit", t = "rabbit"
输出：3
解释：
如下图所示, 有 3 种可以从 s 中得到 "rabbit" 的方案。
[ra]b[bbit]
[rab]b[bit]
[rabb]b[it]
```
```
输入：s = "babgbag", t = "bag"
输出：5
解释：
如下图所示, 有 5 种可以从 s 中得到 "bag" 的方案。 
[ba]b[g]bag
[ba]bgba[g]
[babgb[ag]
ba[b]gb[ag]
babg[bag]
```


提示：
- 0 <= s.length, t.length <= 1000
- s 和 t 由英文字母组成

https://leetcode.cn/problems/distinct-subsequences

# 解析
这道题目如果不是子序列，而是要求连续序列的，那就可以考虑用 KMP。

这道题目相对于《72. 编辑距离》，简单了不少，因为本题相当于只有删除操作，不用考虑替换增加之类的。

## 动态规划

### 确定dp数组及下标的含义
dp[i][j]：以 s[i - 1] 结尾和以 t[j - 1] 结尾的子序列的个数。

### 确定递推公式
这一类问题，基本是要分析两种情况
- s[i - 1] == t[j - 1] 
- s[i - 1] != t[j - 1] 

当 s[i - 1] == t[j - 1]相等时，dp[i][j] 可以有两部分组成：
- 一部分是用 s[i - 1]来匹配，那么个数为 dp[i - 1][j - 1]。即不需要考虑当前 s 子串和 t 子串的最后一位字母，所以只需要 dp[i - 1][j - 1]
- 一部分是不用 s[i - 1]来匹配，个数为 dp[i - 1][j]

Q：为什么还要考虑不用 s[i - 1]来匹配，都相同了指定要匹配啊。

例如：s = "bagg" 和 t = "bag" ，s[3] 和 t[2] 是相同的，但是字符串 s 也可以不用 s[3] 来匹配，即用s[0]、s[1]、s[2] 组成的 bag。

当然也可以用 s[3] 来匹配，即：s[0]、s[1]、s[3] 组成的bag。

所以当 s[i - 1] 与 t[j - 1] 相等时，dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j];

当 s[i - 1] 与 t[j - 1] 不相等时，dp[i][j] 只有一部分组成，不用 s[i - 1] 来匹配（就是模拟在s中删除这个元素），即：dp[i - 1][j]

所以此时递推公式为：dp[i][j] = dp[i - 1][j];

Q：为什么只考虑 “不用 s[i - 1] 来匹配” 这种情况， 不考虑“不用 t[j - 1] 来匹配” 的情况呢？

这里要明确，求的是 s 中有多少个 t，而不是求 t 中有多少个 s，所以只考虑 s 中删除元素的情况。

### 初始化dp数组
从递推公式看出，dp[i][0] 和 dp[0][j] 是必须要初始化的。

dp[i][0] 表示以 s[i - 1] 结尾随便删除元素后出现空字符串的个数。那么一定都是 1。因为删除所有元素，出现空字符串的个数就是 1。

dp[0][j] 表示在空字符串 s 中随便删除元素后出现以 t[j - 1] 结尾的个数。那么一定都是 0，s 无论如何也变成不了 t。

那么 dp[0][0] 应该是多少？应该是 1，空字符串 s 删除 0 个元素后变成看空字符串 t。

### 确定遍历顺序
从递推公式可以看出，dp[i][j] 都是根据二维数组的左上方和正上方元素推导出来的，所以一定要从上到下、从左到右遍历进行计算。

### 举例推导dp数组
以 s = "baegg"，t = "bag" 为例，推导 dp 数组状态如下：

![](./images/leetcode-0115-img1.png)

时间复杂度: O(n * m)

空间复杂度: O(n * m)


# 代码

### php
```php
class LeetCode0115 {

    public function numDistinct($s, $t) {
        $len1 = strlen($s);
        $len2 = strlen($t);
        $dp = array_fill(0, $len1 + 1, array_fill(0, $len2 + 1, 0));
        for ($i = 0; $i < $len1; $i++) {
            $dp[$i][0] = 1;
        }
        for ($j = 1; $j < $len2; $j++) {
            $dp[0][$j] = 0;
        }
        for ($i = 1; $i <= $len1; $i++) {
            for ($j = 1; $j <= $len2; $j++) {
                if ($s[$i - 1] == $t[$j - 1]) {
                    $dp[$i][$j] = $dp[$i - 1][$j - 1] + $dp[$i - 1][$j];
                } else {
                    $dp[$i][$j] = $dp[$i - 1][$j];
                }
            }
        }
        return $dp[$len1][$len2];
    }

}

```

### go
```go
func numDistinct(s string, t string) int {
    len1, len2 := len(s), len(t)
    dp := make([][]int, len1 + 1)
    for k, _ := range dp {
        dp[k] = make([]int, len2 + 1)
    }
    for i := 0; i < len1; i++ {
        dp[i][0] = 1
    }
    for i := 1; i <= len1; i++ {
        for j := 1; j <= len2; j++ {
            if s[i - 1] == t[j - 1] {
                dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j]
            } else {
                dp[i][j] = dp[i - 1][j]
            }
        }
    }
    return dp[len1][len2]
}
```
