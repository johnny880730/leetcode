# 1049. 最后一块石头的重量 II

# 题目
有一堆石头，用整数数组 stones 表示。其中 stones[i] 表示第 i 块石头的重量。

每一回合，从中选出任意两块石头，然后将它们一起粉碎。假设石头的重量分别为 x 和 y，且 x <= y。那么粉碎的可能结果如下：
- 如果 x == y，那么两块石头都会被完全粉碎；
- 如果 x != y，那么重量为 x 的石头将会完全粉碎，而重量为 y 的石头新重量为 y-x。
最后，最多只会剩下一块石头。返回此石头最小的可能重量 。如果没有石头剩下，就返回 0。

https://leetcode.cn/problems/last-stone-weight-ii


提示：
- 1 <= stones.length <= 30
- 1 <= stones[i] <= 100

# 示例：
```
输入：stones = [2,7,4,1,8,1]
输出：1
解释：
组合 2 和 4，得到 2，所以数组转化为 [2,7,1,8,1]，
组合 7 和 8，得到 1，所以数组转化为 [2,1,1,1]，
组合 2 和 1，得到 1，所以数组转化为 [1,1,1]，
组合 1 和 1，得到 0，所以数组转化为 [1]，这就是最优值。
```

```
输入：stones = [31,26,33,21,40]
输出：5
```


# 解析
本题其实就是尽量让石头分成重量相同的两堆，相撞之后剩下的石头最小，这样就化解成 0-1 背包问题了。

本题其实和《0416. 分割等和子集》几乎是一样的，只是最后对 dp[target] 的处理方式不同。

《0416. 分割等和子集》 相当于是求背包是否正好装满，而本题是求背包最多能装多少。

## 动态规划五部曲
本题物品的重量为 stones[i]，物品的价值也为 stones[i]。对应着 0-1 背包里的物品重量 weight[i] 和物品价值 value[i]。

### 确定 dp 数组以及下标的含义
dp[j] 表示容量（这里说容量更形象，其实就是重量）为 j 的背包，最多可以背最大重量为 dp[j]。

可以回忆一下 0-1 背包中，dp[j] 的含义，容量为 j 的背包，最多可以装的价值为 dp[j]。相对于 0-1 背包，本题中，石头的重量是 stones[i]，
石头的价值也是 stones[i] ，可以 “最多可以装的价值为 dp[j]” == “最多可以背的重量为dp[j]”

### 确定递推公式
0-1 背包的递推公式为：dp[j] = max(dp[j], dp[j - weight[i]] + value[i]);

本题则是：dp[j] = max(dp[j], dp[j - stones[i]] + stones[i]);

### dp 数组初始化
既然 dp[j]中的 j 表示容量，那么最大容量（重量）是多少呢，就是所有石头的重量和。

因为提示中给出 1 <= stones.length <= 30，1 <= stones[i] <= 100，所以最大重量就是 30 * 100。而要求的 target 其实只是最大重量的一半，
所以 dp 数组开到 1500 大小就可以了。当然也可以把石头遍历一遍，计算出石头总重量然后除以 2 ，得到 dp 数组的大小。

接下来就是如何初始化 dp[j] 呢，因为重量都不会是负数，所以 dp[j] 都初始化为0就可以了，这样在递归公式中 dp[j] 才不会初始值所覆盖。

### 确定遍历顺序
在动态规划：关于 0-1 背包问题，如果使用一维 dp 数组，物品遍历的 for 循环放在外层，遍历背包的 for 循环放在内层，且内层 for 循环倒序遍历

### 举例推导dp数组
举例，输入：[2,4,1,1]，此时 target = (2 + 4 + 1 + 1)/2 = 4 ，dp 数组状态图如下：

![](./images/leetcode-1049-img1.png)

# 代码
```php
class LeetCode1049 {

    public function lastStoneWeightII($stones) {
        $sum = array_sum($stones);
        $target = $sum >> 1;
        $dp = array_fill(0, $target + 1, 0);
        for ($i = 0; $i < count($stones); $i++) {
            for ($j = $target; $j >= $stones[$i]; $j--) {
                $dp[$j] = max($dp[$j], $dp[$j - $stones[$i]] + $stones[$i]);
            }
        }
        return $sum - $dp[$target] - $dp[$target];
    }

}
```

### go
```go
func lastStoneWeightII(stones []int) int {
	sum := 0
	for _, v := range stones {
		sum += v
	}
	target := sum >> 1

	dp := make([]int, target + 1)
	for i := 0; i < len(stones); i++ {
		for j := target; j >= stones[i]; j-- {
			dp[j] = max(dp[j], dp[j - stones[i]] + stones[i])
		}
	}
	return sum - dp[target] - dp[target]
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
```