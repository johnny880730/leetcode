# 0067. 二进制求和

# 题目
给你两个二进制字符串 a 和 b ，以二进制字符串的形式返回它们的和。

https://leetcode.cn/problems/add-binary/description/

提示：
- 1 <= a.length, b.length <= 10000
- a 和 b 仅由字符 '0' 或 '1' 组成
- 字符串如果不是 "0" ，就不含前导零


# 示例
```
示例1:

输入: a = "11", b = "1"
输出: "100"
```
```
示例2:

输入: a = "1010", b = "1011"
输出: "10101"
```

# 解析

## 模拟
可以借鉴「列竖式」的方法，末尾对齐，逐位相加。在十进制的计算中「逢十进一」，二进制中就是「逢二进一」。

具体的，可以取 len = max{∣a∣,∣b∣}，循环 len 次，从最低位开始遍历。我们使用一个变量 carry 表示上一个位置的进位，初始值为 0。记当前位置对其的两个位为 a[i] 和 b[i]，则每一位的答案为 (carry + a[i] + b[i]​) % 2，下一位的进位为 (carry + a[i] + b[i]) / 2 取整。
​
重复上述步骤，直到数字 a 和 b 的每一位计算完毕。最后如果 carry 的最高位不为 0，则将最高位添加到计算结果的末尾。

注意，为了让各个位置对齐，你可以先反转这个代表二进制数字的字符串，然后低下标对应低位，高下标对应高位。当然你也可以直接把 a 和 b 中短的那一个补 0 直到和长的那个一样长，然后从高位向低位遍历，对应位置的答案按照顺序存入答案字符串内，最终将答案串反转。

## 位运算
如果不允许使用加减乘除，则可以使用位运算替代上述运算中的一些加减乘除的操作。
- 把 a 和 b 转换成整型数字 x 和 y，在接下来的过程中，x 保存结果，y 保存进位。
- 当进位不为 0 时
  - 计算当前 x 和 y 的无进位相加结果：answer = x ^ y
  - 计算当前 x 和 y 的进位：carry = (x & y) << 1
  - 完成本次循环，更新 x = answer，y = carry
- 返回 x 的二进制形式

为什么这个方法是可行的呢？在第一轮计算中，answer 的最后一位是 x 和 y 相加之后的结果，carry 的倒数第二位是 x 和 y 最后一位相加的进位。接着每一轮中，由于 carry 是由 x 和 y 按位与并且左移得到的，那么最后会补零，所以在下面计算的过程中后面的数位不受影响，而每一轮都可以得到一个低 i 位的答案和它向低 i + 1 位的进位，也就模拟了加法的过程。


# 代码

### php
```php
class LeetCode0067 {

    // 模拟
    function addBinary1($a, $b) {
        $aLen = strlen($a);
        $bLen = strlen($b);
        # 第一步：计算出每个字符串的长度后，进行比较，以0来填充字符串确保两个字符串位数相同
        if ($aLen != $bLen) {
            if ($aLen < $bLen) {
                $a = str_pad($a, $bLen, '0', STR_PAD_LEFT);
            } else if ($aLen > $bLen) {
                $b = str_pad($b, $aLen, '0', STR_PAD_LEFT);
            }
        }
        $aLen = $bLen = strlen($a);
        # 第二步: 经第一步后，两字符串位数已经相同，aLen-1后的下标即为最后一位数的下标，即从末位开始进行相加
        for ($i = $aLen - 1; $i >= 0; $i--) {
            $a[$i] = $a[$i] + $b[$i];
            if ($i != 0) {
                # 当下标没有到第一个字符的时候，判断相加之和是否大于1，大于1则需要进位
                if ($a[$i] > 1) {
                    //把相加之和取模2后的值赋值给当前下标对应值
                    $a[$i] = $a[$i] % 2;
                    //顺便把前一个下标对应值先加一个进位，这样在下一轮循环的时候，该值已为最新值了
                    $a[$i - 1] = $a[$i - 1] + 1;
                }
            } else {
                # 由于下标0比较特殊，可能两数相加后，会产生一个位数大1的数，则需要再连接一个字符1在字符串前，最后才是正确结果。
                if ($a[$i] > 1) {
                    $a[$i] = $a[$i] % 2;
                    $a = '1' . $a;
                }
            }
        }
        return $a;
    }

}
```

### go
```go
func addBinary(a string, b string) string {
    aLen := len(a)
	bLen := len(b)
	
	if aLen != bLen {
		if aLen < bLen {
			a = strings.Repeat("0", bLen-aLen) + a
            aLen = bLen
		} else if aLen > bLen {
			b = strings.Repeat("0", aLen-bLen) + b
            bLen = aLen
		}
	}
	result := []byte(a)
	
	for i := aLen - 1; i >= 0; i-- {
		result[i] = result[i] + b[i] - '0'
		
		if i != 0 {
			if result[i] > '1' {
				result[i] = result[i] - 2
				result[i - 1] = result[i - 1] + 1
			}
		} else {
			if result[i] > '1' {
				result[i] = result[i] - 2
				result = append([]byte{'1'}, result...)
			}
		}
	}
	
	return string(result)
}
```

### java
```java
class LeetCode0067 {

    public String addBinary(String a, String b) {
        int aLen = a.length();
        int bLen = b.length();

        if (aLen != bLen) {
            if (aLen < bLen) {
                a = String.format("%" + bLen + "s", a).replace(' ', '0');
            } else if (aLen > bLen) {
                b = String.format("%" + aLen + "s", b).replace(' ', '0');
            }
        }

        aLen = bLen = a.length();
        char[] result = a.toCharArray();

        for (int i = aLen - 1; i >= 0; i--) {
            result[i] = (char) (result[i] + b.charAt(i) - '0');

            if (i != 0) {
                if (result[i] > '1') {
                    result[i] = (char) (result[i] - 2);
                    result[i - 1] = (char) (result[i - 1] + 1);
                }
            } else {
                if (result[i] > '1') {
                    result[i] = (char) (result[i] - 2);
                    String temp = new String(result);
                    return "1" + temp;
                }
            }
        }

        return new String(result);
    }
}
```