# 2609. 最长平衡子字符串


# 题目
给你一个仅由 0 和 1 组成的二进制字符串 s 。

如果子字符串中 所有的 0 都在 1 之前 且其中 0 的数量等于 1 的数量，则认为 s 的这个子字符串是平衡子字符串。请注意，空子字符串也视作平衡子字符串。

返回  s 中最长的平衡子字符串长度。

子字符串是字符串中的一个连续字符序列。

提示：
- 1 <= s.length <= 50
- '0' <= s[i] <= '1'

# 示例
```
输入：s = "01000111"
输出：6
解释：最长的平衡子字符串是 "000111" ，长度为 6 。
```
```
输入：s = "00111"
输出：4
解释：最长的平衡子字符串是 "0011" ，长度为  4 。
```
```
输入：s = "111"
输出：0
解释：除了空子字符串之外不存在其他平衡子字符串，所以答案为 0 
```

# 解析

## 双指针
使用双指针 pos0 和 pos1 分别标定连续 0 和连续 1 的起点，每一次循环先查找连续 0 再查找连续 1
1. 先统计连续段 0 的长度，记为 cnt0；再统计连续段 1 的长度，记为 cnt1（此操作满足：子串中 0 均在 1 前面） 
2. 在 cnt0 和 cnt1 中取较小值，进行乘 2 操作，作为当前平衡子字符串的长度，用于更新答案（此操作满足：子串中 0 和 1 数量相同）
3. 从当前轮的结束位置 cur，再进行下轮处理（重复步骤 1 和步骤 2），直到 s 处理完成


# 代码

### php
```php
class Leetcode2609 {

    function findTheLongestBalancedSubstring($s) {
        $res = 0;
        // 连续0的起点，初始为0【假设首个字符为0】
        $pos0 = 0; 
        // 连续1的起点，初始为0【假设首个字符为1】
        $pos1 = 0;
        // 当前遍历位置
        $cur = $pos0;
        $len = strlen($s);
        while ($cur < $len) {
            // 找到连续0之后首个1的位置
            while ($cur < $len && $s[$cur] == '0') {
                $cur++;
            }
            // 计算这一段连续0的个数
            $cnt0 = $cur - $pos0;
            // 更新连续1起点
            $pos1 = $cur;

            // 找到连续1之后首个0的位置
            while ($cur < $len && $s[$cur] == '1') {
                $cur++;
            }
            // 计算这一段连续1的个数
            $cnt1 = $cur - $pos1;
            // 更新连续0的起点
            $pos0 = $cur;

            // 平衡子串为连续0和连续1中最小数目的两倍
            $res = max($res, min($cnt0, $cnt1) * 2);
        }
        
        return $res;
    }
}
```

### java
```java
public int findTheLongestBalancedSubstring(String s) {
    int res = 0;
    int pos0 = 0;
    int pos1 = 0;
    int cur = pos0;
    while(cur < s.length()){
        while(cur < s.length() && s.charAt(cur) == '0') {
            cur++;
        }
        int cnt0 = cur - pos0;
        pos1 = cur;

        while(cur < s.length() && s.charAt(cur) == '1') {
            cur++;
        }
        int cnt1 = cur - pos1;
        pos0 = cur;

        res = Math.max(res, Math.min(cnt0, cnt1) * 2);
        
    }
    return res;
}
```
