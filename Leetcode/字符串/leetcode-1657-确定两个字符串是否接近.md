# 1657. 确定两个字符串是否接近


# 题目
如果可以使用以下操作从一个字符串得到另一个字符串，则认为两个字符串 接近 ：
- 操作 1：交换任意两个 现有 字符。
  - 例如，abcde -> aecdb
- 操作 2：将一个 现有 字符的每次出现转换为另一个 现有 字符，并对另一个字符执行相同的操作。 
  - 例如，aacabb -> bbcbaa（所有 a 转化为 b ，而所有的 b 转换为 a ）

你可以根据需要对任意一个字符串多次使用这两种操作。

给你两个字符串，word1 和 word2 。如果 word1 和 word2 接近 ，就返回 true ；否则，返回 false 。

https://leetcode.cn/problems/determine-if-two-strings-are-close/

提示：
- 1 <= word1.length, word2.length <= 10^5
- word1 和 word2 仅包含小写英文字母

# 示例
```
输入：word1 = "abc", word2 = "bca"
输出：true
解释：2 次操作从 word1 获得 word2 。
执行操作 1："abc" -> "acb"
执行操作 1："acb" -> "bca"
```
```
输入：word1 = "a", word2 = "aa"
输出：false
解释：不管执行多少次操作，都无法从 word1 得到 word2 ，反之亦然。
```
```
输入：word1 = "cabbba", word2 = "abbccc"
输出：true
解释：3 次操作从 word1 获得 word2 。
执行操作 1："cabbba" -> "caabbb"
执行操作 2："caabbb" -> "baaccc"
执行操作 2："baaccc" -> "abbccc"
```
```
输入：word1 = "cabbba", word2 = "aabbss"
输出：false
解释：不管执行多少次操作，都无法从 word1 得到 word2 ，反之亦然。
```

# 解析

下文把 word1 记作 s，把 word2 记作 tt。

**操作 1 的本质：字符可以任意排列**

把 s 看成是一叠扑克牌，我们可以随意洗牌。

统计 s 和 t 中字符的出现次数，如果字符及其出现次数都一样，只用操作 1 就可以把 s 变成 t（把一叠扑克洗成另一叠扑克）。

如果字符一样，但对应的出现次数不一样呢？这就需要用到操作 2 了。

**操作 2 的本质：出现次数是可以交换的**

以示例 3 为例。统计 s = cabbba 的字符出现次数：
- a 出现 2 次。
- b 出现 3 次。
- c 出现 1 次。

我们可以把 a 都变成 b，同时把 b 都变成 a。

这相当于交换 a 和 b 的出现次数，得到：
- a 出现 3 次。
- b 出现 2 次。
- c 出现 1 次。

然后交换 a 和 cc 的出现次数，得到：
- a 出现 1 次。
- b 出现 2 次。
- c 出现 3 次。

这便是字符串 t = abbccc 的字符出现次数。

所以「出现次数」也像操作 1 那样，是可以任意排列的。

如果 s 和 t 的字符一样，并且字符出现次数的集合是相同的（比如上面这个例子都是集合 {1, 2, 3}），那么可以结合操作 1 和操作 2，把 s 变成 t。

算法：
- 判断 s 和 t 的长度是否一样，如果不一样直接返回 false。
- 判断 s 和 t 的字符集合是否一样，如果不一样直接返回 false。例如 s 中有字符 abc，t 中有字符 def，我们无论如何都不能把 s 变成 t。
- 判断 s 的字符出现次数的集合，是否等于 t 的字符出现次数的集合，等于返回 true，不等于返回 false。注意集合可以有相同元素，比如 aabbbccc 对应的集合就是 {2,3,3}。

**实现细节**

判断字符集合是否一样，可以用位运算实现，也就是用二进制数（从低到高）第 i 位来存储是否有第 i 个小写英文字母，这样只需要判断两个二进制数是否一样即可。

判断字符出现次数的集合是否一样，可以用两个长为 26 的数组统计 s 和 t 中每个字母的出现次数，分别记作 sCnt 和 tCnt。如果这两个数组排序后是一样的，
就说明 s 的字符出现次数的集合，等于 t 的字符出现次数的集合。


# 代码

### php
```php
class Leetcode1657 {

    function closeStrings($s, $t) {
        $sLen = strlen($s);
        $tLen = strlen($t);
        if ($sLen != $tLen) {
            return false;
        }
        $sMask = $tMask = 0;
        $sCnt = $tCnt = array_fill(0, 26, 0);
        for ($i = 0; $i < $sLen; $i++) {
            $m = ord($s[$i]) - ord('a');
            $sMask |= 1 << $m;
            $sCnt[$m]++;
        }

        for ($i = 0; $i < $tLen; $i++) {
            $m = ord($t[$i]) - ord('a');
            $tMask |= 1 << $m;
            $tCnt[$m]++;
        }

        sort($sCnt);
        sort($tCnt);

        return $sMask == $tMask && $sCnt == $tCnt;
    }
}
```

### java
```java
public boolean closeStrings(String s, String t) {
      if (s.length() != t.length()) {
          return false;
      }
      int sMask = 0, tMask = 0;
      int[] sCnt = new int[26], tCnt = new int[26];
      for (int i = 0; i < s.length(); i++) {
          sMask |= 1 << (s.charAt(i) - 'a');
          sCnt[s.charAt(i) - 'a']++;
      }
      for (int i = 0; i < t.length(); i++) {
          tMask |= 1 << (t.charAt(i) - 'a');
          tCnt[t.charAt(i) - 'a']++;
      }

      Arrays.sort(sCnt);
      Arrays.sort(tCnt);

      return sMask == tMask && Arrays.equals(sCnt, tCnt);
  }
```

### go
```go
func closeStrings(s string, t string) bool {
    if len(s) != len(t) {
        return false
    }
    var sMask, tMask int
    var sCnt, tCnt [26]int
    for _, c := range s {
        sMask |= 1 << (c - 'a')
        sCnt[c - 'a']++
    }
    for _, c := range t {
        tMask |= 1 << (c - 'a')
        tCnt[c - 'a']++
    }

    slices.Sort(sCnt[:])
    slices.Sort(tCnt[:])

    return sMask == tMask && slices.Equal(sCnt[:], tCnt[:])
}
```
