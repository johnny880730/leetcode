# 925. 长按键入

# 题目
你的朋友正在使用键盘输入他的名字 name。偶尔，在键入字符 c 时，按键可能会被长按，而字符可能被输入 1 次或多次。

你将会检查键盘输入的字符 typed。如果它对应的可能是你的朋友的名字（其中一些字符可能被长按），那么就返回 True。

https://leetcode.cn/problems/long-pressed-name

提示：
- 1 <= name.length, typed.length <= 1000
- name 和 typed 的字符都是小写字母

# 示例
```
输入：name = "alex", typed = "aaleex"
输出：true
解释：'alex' 中的 'a' 和 'e' 被长按。
```
```
输入：name = "saeed", typed = "ssaaedd"
输出：false
解释：'e' 一定需要被键入两次，但在 typed 的输出中不是这样。
```

# 解析
这道题目一看以为是哈希，仔细一看不行，要有顺序。

所以模拟同时遍历两个数组，进行对比就可以了。

对比的时候需要一下几点：
- name[i] 和 typed[j] 相同，则i++，j++ （继续向后对比）
- name[i] 和 typed[j]不相同
  - 看是不是第一位就不相同了，也就是 j 如果等于 0，那么直接返回 false
  - 不是第一位不相同，就让 j 跨越重复项，移动到重复项之后的位置，再次比较 name[i] 和 typed[j]
    - 如果 name[i] 和 typed[j] 相同，则i++，j++ （继续向后对比）
    - 不相同，返回false
    - 
对比完之后有两种情况
- name 没有匹配完，例如 name = "pyplrzzzzdsfa"、type = "ppyypllr"
- type 没有匹配完，例如 name = "alex"、type = "alexxrrrrssda"

![](./images/leetcode-0925-img1.gif)


# 代码

### php
```php
class Leetcode0925 {

    function isLongPressedName($name, $typed) {
        $len1 = strlen($name);
        $len2 = strlen($typed);
        $i = $j = 0;
        while ($i < $len1 && $j < $len2) {
            if ($name[$i] == $typed[$j]) {
                // 相同则同时向后匹配
                $i++;
                $j++;
            } else {
                // 如果是第一位就不相同直接返回false
                if ($j == 0) {
                    return false;
                }
                // 判断边界为 len2-1,若为len2会越界,例如name:"kikcxmvzi" typed:"kiikcxxmmvvzzz"
                while ($j < $len2 && $typed[$j] == $typed[$j - 1]) {
                    $j++;
                }
                // j 跨越重复项之后再次和name[i]匹配
                if ($name[$i] == $typed[$j]) {
                    $i++;
                    $j++;
                } else {
                    return false;
                }
            }
        }
        // 说明name没有匹配完
        if ($i < $len1) {
            return false;
        }
        // 说明type没有匹配完
        while ($j < $len2) {
            if ($typed[$j] == $typed[$j - 1]) {
                $j++;
            } else {
                return false;
            }
        }

        return true;
    }
    
}
```

### go
```go
func isLongPressedName(name string, typed string) bool {
    len1, len2 := len(name), len(typed)
    i, j := 0, 0
    for i < len1 && j < len2 {
        if name[i] == typed[j] {
            i++
            j++
        } else {
            if j == 0 {
                return false
            }
            for j < len2 - 1 && typed[j] == typed[j - 1] {
                j++
            }
            if name[i] == typed[j] {
                i++
                j++
            } else {
                return false
            }
        }
    }
    if i < len1 {
        return false
    }
    for j < len2 {
        if typed[j] == typed[j - 1] {
            j++
        } else {
            return false
        }
    }
    return true
}
```