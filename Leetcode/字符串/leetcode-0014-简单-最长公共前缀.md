# 14. 最长公共前缀

# 题目
编写一个函数来查找字符串数组中的最长公共前缀。如果不存在公共前缀，返回空字符串。

https://leetcode.cn/problems/longest-common-prefix

说明:
- 所有输入只包含小写字母 a-z。

# 示例
```
输入: ["flower","flow","flight"]
输出: "fl"
```
```
输入: ["dog","racecar","car"]
输出: ""
解释: 输入不存在公共前缀。
```

# 解析

## 纵向扫描

纵向扫描时，从前往后遍历所有字符串的每一列，比较相同列上的字符是否相同，如果相同则继续对下一列进行比较，如果不相同则当前列不再属于公共前缀，
当前列之前的部分为最长公共前缀。

# 代码

### php
```php
class LeetCode0014 {

    function longestCommonPrefix($strs) {
        $count    = count($strs);        // 数组总数
        $first    = $strs[0];            // 第一个字符串
        $firstLen = strlen($first);      // 第一个字符串长度
        $res      = '';                  // 公共前缀结果
        # 循环第一个字符串
        for ($i = 0; $i < $firstLen; $i++) {
            $tmp = $first[$i];              // 第一个字符串的第i个字符
            # 遍历数组的其他字符串
            for ($j = 1; $j < $count; $j++) {
                # 判断字符串字符是否相等,不相等则直接返回,相等则在循环外拼接公共前缀
                if ($strs[$j][$i] != $tmp) {
                    return $res;
                }
            }
            $res .= $tmp;
        }
        return $res;
    }
}
```

### go
```go
func longestCommonPrefix(strs []string) string {
    var tmpArr [][]string
    arrLen := len(strs)
    for i := 0; i < arrLen; i++ {
        tmpArr = append(tmpArr, strings.Split(strs[i], ""))
    }
    first := strs[0]
    firstLen := len(first)
    res := ""
    for i := 0; i < firstLen; i++ {
        ch := tmpArr[0][i]
        for j := 1; j < arrLen; j++ {
            if  i >= len(tmpArr[j]) || tmpArr[j][i] != ch {
                return res
            }
        }
        res += ch
    }

    return res
}
```

### java
```java
class LeetCode0014 {

    public String longestCommonPrefix(String[] strs) {
        int strLen = strs.length;
        String first = strs[0];

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < first.length(); i++) {
            char c = first.charAt(i);
            for (int j = 1; j < strLen; j++) {
                if (i >= strs[j].length() || strs[j].charAt(i) != c) {
                    return sb.toString();
                }
            }
            sb.append(c);
        }
        return sb.toString();
    }
}
```
