# 1410. HTML 实体解析器


# 题目
「HTML 实体解析器」 是一种特殊的解析器，它将 HTML 代码作为输入，并用字符本身替换掉所有这些特殊的字符实体。

HTML 里这些特殊字符和它们对应的字符实体包括：
- 双引号：字符实体为 `&quot;` ，对应的字符是 " 。
- 单引号：字符实体为 `&apos;` ，对应的字符是 ' 。
- 与符号：字符实体为 `&amp;` ，对应对的字符是 & 。
- 大于号：字符实体为 `&gt;` ，对应的字符是 > 。
- 小于号：字符实体为 `&lt;` ，对应的字符是 < 。
- 斜线号：字符实体为 `&frasl;` ，对应的字符是 / 。

给你输入字符串 text ，请你实现一个 HTML 实体解析器，返回解析器解析后的结果。

提示：
- 

# 示例
```
输入：text = "&amp; is an HTML entity but &ambassador; is not."
输出："& is an HTML entity but &ambassador; is not."
解释：解析器把字符实体 &amp; 用 & 替换
```
```
输入：text = "and I quote: &quot;...&quot;"
输出："and I quote: \"...\""
```
```
输入：text = "Stay home! Practice on Leetcode :)"
输出："Stay home! Practice on Leetcode :)"
```
```
输入：text = "x &gt; y &amp;&amp; x &lt; y is always false"
输出："x > y && x < y is always false"
```
```
输入：text = "leetcode.com&frasl;problemset&frasl;all"
输出："leetcode.com/problemset/all"
```

# 解析
本题要求把字符串中所有的「字符实体」替换成对应的字符。

「字符实体」都是由 & 开头的，所以只需要遍历一遍字符串，用一个变量 pos 表示当前处理的位置，如果 text[pos] = ‘&’，就在这个位置进行探测。

假设一个「字符实体」为 e，对应的字符为 c，那么可以通过判断 pos 位置开始，长度和 e 相同的子串是否和 e 相等，如果相等就可以替换。


# 代码

### php
```php
class LeetcodeXXXX {

    function entityParser($text) {
        $map = [
            "&quot;" => '"',
            "&apos;" => "'",
            "&amp;" => "&",
            "&gt;" => '>',
            "&lt;" => '<',
            '&frasl;' => '/',
        ];
        $len = strlen($text);
        $res = '';
        $pos = 0;
        while ($pos < $len) {
            $isTag = false;
            if ($text[$pos] == '&') {
                foreach ($map as $k => $v) {
                    if ($pos + strlen($k) <= $len && substr($text, $pos, strlen($k)) == $k) {
                        $res .= $v;
                        $isTag = true;
                        $pos += strlen($k);
                        break;
                    }
                }
            }

            if (!$isTag) {
                $res .= $text[$pos];
                $pos++;
            }

        }
        return $res;
    }
}
```

### java
```java
public String entityParser(String text) {
    Map<String, String> map = new HashMap<>();
    map.put("&quot;", "\"");
    map.put("&apos;", "'");
    map.put("&amp;", "&");
    map.put("&gt;", ">");
    map.put("&lt;", "<");
    map.put("&frasl;", "/");
    int len = text.length();
    StringBuilder sb = new StringBuilder();
    int i = 0;
    while (i < len) {
        boolean isTag = false;
        if (text.charAt(i) == '&') {
            for (String key: map.keySet()) {
                if (i + key.length() <= len && text.substring(i, i + key.length()).equals(key)) {
                    sb.append(map.get(key));
                    isTag = true;
                    i += key.length();
                    break;
                }
            }
        }

        if (!isTag) {
            sb.append(text.charAt(i++));
        }
    }
    
    return sb.toString();
}
```

### go
```go
func entityParser(text string) string {
    myMap := map[string]string{
        "&quot;" : "\"",
        "&apos;" : "'",
        "&amp;" : "&",
        "&gt;" : ">",
        "&lt;" : "<",
        "&frasl;" : "/",
    }
    i := 0
    length := len(text)
    res := make([]string, 0)
    for i < length {
        isTag := false
        if text[i] == '&' {
            for k, v := range myMap {
                if i + len(k) <= length && text[i : i + len(k)] == k {
                    res = append(res, v)
                    isTag = true
                    i += len(k)
                    break
                }
            }
        }

        if !isTag {
            res = append(res, text[i : i + 1])
            i++
        }
    }
    return strings.Join(res, "")
}
```
