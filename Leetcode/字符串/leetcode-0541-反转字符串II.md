# 541. 反转字符串 II

# 题目
给定一个字符串 s 和一个整数 k，从字符串开头算起，每 2k 个字符反转前 k 个字符。

如果剩余字符少于 k 个，则将剩余字符全部反转。

如果剩余字符小于 2k 但大于或等于 k 个，则反转前 k 个字符，其余字符保持原样。

https://leetcode.cn/problems/reverse-string-ii/

# 示例：
```
输入：s = "abcdefg", k = 2
输出："bacdfeg"
```

```
输入：s = "abcd", k = 2
输出："bacd"
```

提示：
- 1 <= s.length <= 104
- s 仅由小写英文组成
- 1 <= k <= 104

# 解析
题目中要找的是每个 2k 区间的起点，在遍历字符串的过程中，只要让 i += 2 * k，i 每次移动 2k，然后判断是否有需要反转的区间即可。

所以当按照固定规律一段一段的处理字符串的时候，要在 for 循环的表达式上多做文章

# 代码

### php
```php
class LeetCode0541 {

    function reverseStr($s, $k) {
        $ch = str_split($s);
        // 每隔2k个反转前k个，尾数不够k个时候全部反转
        for ($i = 0, $len = strlen($s); $i < $len; $i += $k * 2) {
            $start = $i;
            // 判断尾数够不够k个来取决end指针的位置
            $end = min($len - 1, $start + $k - 1);
            while ($start < $end) {
                $tmp = $ch[$start];
                $ch[$start] = $ch[$end];
                $ch[$end] = $tmp;
                $start++;
                $end--;
            }
        }
        return join('', $ch);
    }
}
```

### go
```go
func reverseStr(s string, k int) string {
    ch := strings.Split(s, "")
	length := len(s)
	for i := 0; i < length; i += 2 * k {
        start := i
		end := start + k - 1
		if length - 1 < end {
			end = length - 1
        }
		for start < end {
		    ch[start], ch[end] = ch[end], ch[start]
			start++
			end--
        }
    }
	return strings.Join(ch, "")
}
```
