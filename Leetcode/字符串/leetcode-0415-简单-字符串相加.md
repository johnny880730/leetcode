# 0415. 字符串相加

# 题目
给定两个字符串形式的非负整数 num1 和num2 ，计算它们的和并同样以字符串形式返回

你不能使用任何內建 BigInteger 库， 也不能直接将输入的字符串转换为整数形式

提示：
- num1 和num2 的长度都小于 5100
- num1 和num2 都只包含数字 0-9
- num1 和num2 都不包含任何前导零

https://leetcode.cn/problems/add-strings/

# 示例
```
输入：num1 = "11", num2 = "123"
输出："134"
```
```
输入：num1 = "456", num2 = "77"
输出："533"
```
```
输入：num1 = "0", num2 = "0"
输出："0"
```


# 解析

# 模拟
只需要对两个大整数模拟「竖式加法」的过程。

竖式加法就是平常学习生活中常用的对两个整数相加的方法，回想一下在纸上对两个整数相加的操作，将相同数位对齐，从低到高逐位相加，
如果当前位和超过 10，则向高位进一位。因此只要将这个过程用代码写出来即可。

具体实现也不复杂，定义两个指针 i 和 j 分别指向 num1 和 num2 的末尾，即最低位，同时定义一个变量 carry 维护当前是否有进位，
然后从末尾到开头逐位相加即可。

可能会想两个数字位数不同怎么处理，这里统一在指针当前下标处于负数的时候返回 0，等价于对位数较短的数字进行了补零操作，
这样就可以除去两个数字位数不同情况的处理


# 代码

### php
```php
class LeetCode0415 {

    // 模拟数学的竖式计算加法
    function addStrings($num1, $num2) {
        // 长度不一致的补齐前导0
        $len1 = strlen($num1);
        $len2 = strlen($num2);
        $len = $len1;
        if ($len1 > $len2) {
            $num2 = str_pad($num2, $len1, '0', STR_PAD_LEFT);
        } else if ($len1 < $len2) {
            $num1 = str_pad($num1, $len2, '0', STR_PAD_LEFT);
            $len = $len2;
        }
        // 从后往前开始计算
        $i = $j = $len - 1;
        $carry = 0;
        $res = '';
        while ($i >= 0 || $j >= 0) {
            $a = intval($num1[$i]);
            $b = intval($num2[$j]);
            $sum = $a + $b + $carry;
            $carry = intval($sum / 10);
            $res = strval($sum % 10) . $res;
            $i--;
            $j--;
        }
        return $carry > 0 ? '1'.$res : $res;
    }

}
```

### java
```java
class LeetCode0415 {
    
    public String addStrings(String num1, String num2) {
        int i = num1.length() - 1, j = num2.length() - 1;
        int carry = 0;
        StringBuilder sb = new StringBuilder();
        while (i >= 0 || j >= 0) {
            int x = i >= 0 ? num1.charAt(i) - '0' : 0;
            int y = j >= 0 ? num2.charAt(j) - '0' : 0;
            int sum = x + y + carry;
            carry = sum / 10;
            sb.append(sum % 10);
            i--;
            j--;
        }
        if (carry > 0) {
            sb.append(1);
        }
        // 计算完以后的答案需要翻转过来
        sb.reverse();
        
        return sb.toString();
    }
}
```

### go
```go
func addStrings(num1 string, num2 string) string {
    i, j := len(num1) - 1, len(num2) - 1
    carry := 0
    res := ""
    for i >= 0 || j >= 0 {
        x, y := 0, 0
        if i >= 0 {
            x = int(num1[i] - '0')
        }
        if j >= 0 {
            y = int(num2[j] - '0')
        }
        sum := x + y + carry
        carry = sum / 10
        res = strconv.Itoa(sum % 10) + res
        i--
        j--
    }

    return res
}

```
