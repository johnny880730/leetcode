# 412. Fizz Buzz

# 题目
给你一个整数 n ，找出从 1 到 n 各个整数的 Fizz Buzz 表示，并用字符串数组 answer（下标从 1 开始）返回结果，其中：
- answer[i] == "FizzBuzz" 如果 i 同时是 3 和 5 的倍数。
- answer[i] == "Fizz" 如果 i 是 3 的倍数。
- answer[i] == "Buzz" 如果 i 是 5 的倍数。
- answer[i] == i （以字符串形式）如果上述条件全不满足。

https://leetcode.cn/problems/fizz-buzz

提示：
- 

# 示例
```
输入：n = 3
输出：["1","2","Fizz"]
```

```
输入：n = 5
输出：["1","2","Fizz","4","Buzz"]
```

```
输入：n = 15
输出：["1","2","Fizz","4","Buzz","Fizz","7","8","Fizz","Buzz","11","Fizz","13","14","FizzBuzz"]
```

# 代码

### go
```php
class LeetCode0412 {
    
    function fizzBuzz($n) {
        $res = [];
        for ($i = 1; $i <= $n; $i++) {
            if ($i % 15 == 0) {
                $res[] = "FizzBuzz";
            } else if ($i % 3 == 0) {
                $res[] = "Fizz";
            } else if ($i % 5 == 0) {
                $res[] = "Buzz";
            } else {
                $res[] = strval($i);
            }
        }
        return $res;
    }
}
```

### go
```go
func fizzBuzz(n int) []string {
    res := make([]string, 0)
    for i := 1; i <= n; i++ {
        if i % 15 == 0 {
            res = append(res, "FizzBuzz")    
        } else if i % 3 == 0 {
            res = append(res, "Fizz")
        } else if i % 5 == 0 {
            res = append(res, "Buzz")    
        } else {
            res = append(res, strconv.Itoa(i))    
        }
    }
    return res
}
```