# 0076. 最小覆盖子串

# 题目
给你一个字符串 s 、一个字符串 t 。返回 s 中涵盖 t 所有字符的最小子串。如果 s 中不存在涵盖 t 所有字符的子串，则返回空字符串 "" 。

https://leetcode.cn/problems/minimum-window-substring/description/

注意：
- 对于 t 中重复字符，我们寻找的子字符串中该字符数量必须不少于 t 中该字符数量。
- 如果 s 中存在这样的子串，我们保证它是唯一的答案。


提示：
- m == s.length
- n == t.length
- 1 <= m, n <= 105
- s 和 t 由英文字母组成

# 示例
```
输入：s = "ADOBECODEBANC", t = "ABC"
输出："BANC"
解释：最小覆盖子串 "BANC" 包含来自字符串 t 的 'A'、'B' 和 'C'。
```
```
输入：s = "a", t = "a"
输出："a"
解释：整个字符串 s 是最小覆盖子串。
```
```
输入: s = "a", t = "aa"
输出: ""
解释: t 中两个字符 'a' 均应包含在 s 的子串中，
因此没有符合条件的子字符串，返回空字符串。
```


# 解析

如果使用暴力解法，代码大概是这样的：
```
for (int i = 0; i < s.size(); i++) {
    for (int j = i + 1; j < s.size(); j++) {
        if s[i:j] 包含 t 的所有字母：
            更新结果
    }
}
```
思路很直接，也很显然这个算法的复杂度是 O(N²)

## 滑动窗口

滑动窗口的思路如下：

1. 在字符串 s 中使用双指针的左右指针技巧，初始化 left = right = 0，把索引左闭右开区间 [left, right) 作为窗口。

> 注意：理论上也可以设计两端都开或者两端都闭的区间，但左闭右开的区间是最方便处理的。因为这样初始化 left = right = 0 时候，
区间 [0, 0) 中没有元素，但只要让 right 向右扩大一位，区间 [0, 1) 就有了一个元素 0 了。
> 
> 如果是两端都开的区间，那么 right 向右移动一位后 (0, 1) 还是没有元素。如果是两端都闭的区间，那么初始区间 [0, 0] 就包含了一个元素。这两种情况都会给边界处理带来不必要的麻烦。
 
2. 不断增加 right 指针扩大窗口 [left, right)，直到窗口中的字符串符合要求（包含 t 的所有字符）
3. 此时停止增加 right，转而不断增加 left 指针缩小窗口，直到窗口中的字符串不再符合要求（包含 t 的所有字符）。同时，每次增加 left，
都要更新一轮结果
4. 重复第 2 和第 3 步，直到 right 到达字符串 s 的尽头


第 2 步相当于在寻找一个“可行解”，然后第 3 步在优化这个“可行解”，最终找到最优解，也就是最短的覆盖子串。

左右指针轮流推进，窗口大小增增减减，窗口不断向右滑动。

实际到此题中，还要初始化两个哈希表 window 和 need，分别记录窗口中的字符和需要凑齐的字符。还需要一个 valid 变量表示窗口中满足 
need 条件的个数，如果 valid == need.size，则说明窗口满足条件，已经完全覆盖了 t。




# 代码
```php

class LeetCode0076 {


    /**
     * @param String $s
     * @param String $t
     * @return String
     */
    function minWindow($s, $t) {
        $lenS = strlen($s);
        $lenT = strlen($t);
        
        $need = $window = [];
        for($i = 0; $i < $lenT; $i++) {
            $need[$t[$i]]++;
        }
        
        $left = $right = 0;
        $valid = 0;
        // 记录最小覆盖子串的起始索引及长度
        $start = 0;
        $len = PHP_INT_MAX;
        while ($right < $lenS) {
            // c 是即将进窗口的字符
            $c = $s[$right];
            // 扩大窗口
            $right++;
            // 进行一系列数据更新
            if (isset($need[$c])) {
                $window[$c]++;
                if ($window[$c] == $need[$c]) {
                    $valid++;
                }
            }

            // 判断左侧是否要收缩
            while ($valid == count($need)) {
                // 这里更新最小覆盖子串
                if ($right - $left < $len) {
                    $start = $left;
                    $len = $right - $left;
                }
                // d 是要出窗口的字符;
                $d = $s[$left];
                $left++;
                if (isset($need[$d])) {
                    if ($window[$d] == $need[$d]) {
                        $valid--;
                    }
                    $window[$d]--;
                }
            }
        }
        return $len == PHP_INT_MAX ? "" : substr($s, $start, $len);
    }
}
```

### go
```go
func minWindow(s string, t string) string {
	lenS := len(s)
	lenT := len(t)

	need := make(map[byte]int)
	window := make(map[byte]int)
	for i := 0; i < lenT; i++ {
		need[t[i]]++
	}

	left, right := 0, 0
	valid := 0
	// 记录最小覆盖子串的起始索引及长度
	start := 0
	length := math.MaxInt32
	for right < lenS {
		// c 是即将进窗口的字符
		c := s[right]
		// 扩大窗口
		right++
		// 进行一系列数据更新
		if _, ok := need[c]; ok {
			window[c]++
			if window[c] == need[c] {
				valid++
			}
		}

		// 判断左侧是否要收缩
		for valid == len(need) {
			// 这里更新最小覆盖子串
			if right - left < length {
				start = left
				length = right - left
			}
			// d 是要出窗口的字符
			d := s[left]
			left++
			if _, ok := need[d]; ok {
				if window[d] == need[d] {
					valid--
				}
				window[d]--
			}
		}
	}
	if length == math.MaxInt32 {
		return ""
	}
	return s[start : start + length]
}
```

### java
```java
class LeetCode0076
{

    public String minWindow(String s, String t) {
        int lenS = s.length();
        int lenT = t.length();
        
        Map<Character, Integer> need = new HashMap<>();
        Map<Character, Integer> window = new HashMap<>();
        for (int i = 0; i < lenT; i++) {
            char c = t.charAt(i);
            need.put(c, need.getOrDefault(c, 0) + 1);
        }
        
        int left = 0, right = 0;
        int valid = 0;
        int start = 0, len = Integer.MAX_VALUE;
        while (right < lenS) {
            char c = s.charAt(right);
            right++;
            if (need.containsKey(c)) {
                window.put(c, window.getOrDefault(c, 0) + 1);
                if (window.get(c).equals(need.get(c))) {
                    valid++;
                }
            }

            while (valid == need.size()) {
                if (right - left < len) {
                    start = left;
                    len = right - left;
                }
                char d = s.charAt(left);
                left++;
                if (need.containsKey(d)) {
                    if (window.get(d).equals(need.get(d))) {
                        valid--;
                    }
                    window.put(d, window.get(d) - 1);
                }
            }
        }
        return len == Integer.MAX_VALUE ? "" : s.substring(start, start + len);
    }
}
```