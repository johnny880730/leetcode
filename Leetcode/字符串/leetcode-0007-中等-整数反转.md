# 7. 整数反转

# 题目
给你一个 32 位的有符号整数 x ，返回将 x 中的数字部分反转后的结果。

如果反转后整数超过 32 位的有符号整数的范围 [−2^31,  2^31 − 1] ，就返回 0。

假设环境不允许存储 64 位整数（有符号或无符号）。

https://leetcode.cn/problems/reverse-integer/

提示：
- -2<sup>31</sup> <= x <= 2<sup>31</sup> - 1


# 示例
```
输入：x = 123
输出：321
```
```
输入：x = -123
输出：-321
```
```
输入：x = 120
输出：21
```
```
输入：x = 0
输出：0
```


# 代码

### php
```php
class LeetCode0007 {

    function reverse($x) {
        $isMinus = $x < 0;
        $x = abs($x);
        $i = 0;
        $j = strlen($x) -1;
        $s = strval($x);
        while ($i < $j) {
            $tmp = $s[$i];
            $s[$i] = $s[$j];
            $s[$j] = $tmp;
            $i++;
            $j--;
        }
        if (intval($s) != 0) {
            $s = ltrim($s, '0');
        }
        if (intval($s) > 2147483647 || ($isMinus == true && intval($s) > 2147483648)) {
            return 0;
        }
        return $isMinus ? '-'.$s : $s;
    }
}
```

### go
```go
func reverse(x int) int {
    isMinus := x < 0
    if isMinus {
        x = 0 - x
    }
    str := strconv.Itoa(x)
    arr := strings.Split(str, "")
    left, right := 0, len(str) - 1
    for left < right {
        arr[left], arr[right] = arr[right], arr[left]
        left++
        right--
    }
    str = strings.Join(arr, "")
    x, _ = strconv.Atoi(str)
    if x != 0 {
        str = strings.TrimLeft(str, "0")
        x, _ = strconv.Atoi(str)
    }
    if (isMinus == true && x > 2147483648) || (isMinus == false && x > 2147483647) {
        return 0
    }
    res := x
    if isMinus {
        res = 0 - res
    }
    return res
}
```