# 28. 找出字符串中第一个匹配项的下标

# 题目
给定一个 haystack 字符串和一个 needle 字符串，在 haystack 字符串中找出 needle 字符串出现的第一个位置 (从 0 开始)。
如果 needle 不是 haystack 的一部分，则返回  -1 。

https://leetcode.cn/problems/find-the-index-of-the-first-occurrence-in-a-string/


说明:

当 needle 是空字符串时，应当返回什么值呢？这是一个在面试中很好的问题。

对于本题而言，当 needle 是空字符串时我们应当返回 0 。这与 C 语言的 strstr() 以及 Java 的 indexOf() 定义相符。

# 示例:
```
输入: haystack = "hello", needle = "ll"
输出: 2
```

# 示例
```
输入: haystack = "aaaaa", needle = "bba"
输出: -1
```


# 解析

这是一道 KMP 算法应用的题目。

点击查看 [KMP算法](../../各种算法/KMP算法.md)

