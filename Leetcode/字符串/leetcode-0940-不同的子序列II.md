## 940. 不同的子序列 II

# 题目
给定一个字符串 s，计算 s 的 不同非空子序列 的个数。因为结果可能很大，所以返回答案需要对 10^9 + 7 取余 。

字符串的 子序列 是经由原字符串删除一些（也可能不删除）字符但不改变剩余字符相对位置的一个新字符串。

例如，"ace" 是 "abcde" 的一个子序列，但 "aec" 不是。

https://leetcode.cn/problems/distinct-subsequences-ii

提示：
- 0 <= s.length, t.length <= 2000
- s 和 t 由英文字母组成

# 示例
```
输入：s = "abc"
输出：7
解释：7 个不同的子序列分别是 "a", "b", "c", "ab", "ac", "bc", 以及 "abc"。
```
```
输入：s = "aba"
输出：6
解释：6 个不同的子序列分别是 "a", "b", "ab", "ba", "aa" 以及 "aba"。
```
```
输入：s = "aaa"
输出：3
解释：3 个不同的子序列分别是 "a", "aa" 以及 "aaa"。
```




# 解析

#### 字符各不相同的情况

&emsp;&emsp;先来看 s 中字符各不相同的情况，例 "abc"

&emsp;&emsp;定义一个数组 all，初始化为 ['']（第一项为空字符的数组）。每当遍历 s 中的某个字符 x 时，all 数组更新的规则为：
**原来的 all 数组 + 新增的部分【all 数组的每一项末位接上 x 的字符串】**。

&emsp;&emsp;举例 s="abcd"，all=['']：
- i=0 即 x=a，all 为原来的 [''] + 新增的 [a] = ['', a]，共 2 项。
- i=1 即 x=b，all 为原来的 ['', a] + 新增的 [b, ab] = ['', a, b, ab]，共 4 项。
- i=2 即 x=c，all 为原来的 ['', a, b, ab] + 新增的 [c, ac, bc, abc] = ['', a, b, ab, c, ac, bc, abc]，共 8 项。
- i=3 即 x=d，all 为原来的 ['', a, b, ab, c, ac, bc, abc] + 新增的 [d, ad, bd, abd, cd, acd, bcd, abcd] 
  = ['', a, b, ab, c, ac, bc, abc, d, ad, bd, abd, cd, acd, bcd, abcd]，共 16 项。

#### 字符可能相同的情况
&emsp;&emsp;当 s 中的字符可能相同时，新的 all 的规则变更为：**原来的 all 数组 + 新增的部分【all 数组的每一项末位接上 x 的字符串】 - 修正部分**。
那么哪里是修正部分呢？修正部分就是指已经存在的、重复的子序列。

&emsp;&emsp;举例 s="abab"，all 初始化为 []：
- i=0 即 x=a，all 为原来的 [] + 新增的 [a] = ['', a]。新的 all 没有重复的部分，不需修正。，共 2 项。
- i=1 即 x=b，all 为原来的 ['', a] + 新增的 [b, ab] = ['', a, b, ab]。新的 all 没有重复的部分，不需修正，共 4 项。
- i=2 即 x=a，all 为原来的 ['', a, b, ab] + 新增的 [a, aa, ba, aba] = ['', a, b, ab, a, aa, ba, aba]。
  显而易见的存在重复项：a，要去掉此项，因此 i=2 时的 all 为  ['', a, b, ab, aa, ba, aba]，共 7 项。
- i=3 即 x=b，all 为原来的['', a, b, ab, aa, ba, aba] + 新增的 [b, ab, bb, abb, aab, bab, abab] 
  = ['', a, b, ab, aa, ba, aba, b, ab, bb, abb, aab, bab, abab]。存在重复项 b 和 ab，需要去掉，结果的 all 为
  ['', a, b, ab, aa, ba, aba, bb, abb, aab, bab, abab]，共 12 项
  
&emsp;&emsp;那么如何找到这个重复的子序列的数量呢？答案是：**修正部分 = 上一次遍历到这个字符时 all 里已这个字符结尾的数量**。比如上文的 "abab"，
遍历到第二个 a 时，修正部分就是遍历第一个 a 的时候的 all 为 ['', a] 中以 a 为结尾的数量，也就是 1 个。还有第二个 b 时，修正部分为遍历第一个 b 
的时候的 ['', a, b, ab] 中以 b 为结尾的数量，也就是 2 个。

&emsp;&emsp;那么如何确定以字符 x 结尾的字串的数量呢？实际上就是遍历到 x 时，前一部的 all 的数量。

&emsp;&emsp;通过上述可以得出，不看修正部分的话，每次新增的部分 newAdd 的数量和当前的 all 的数量是一样的，即 count(all)==count(newAdd)。
而 newAdd 部分是必定把当前字符 x 作为结尾加入到更新后的 all 中，可以用一个哈希表 hash 来记录每次遍历到字符 x 时，以字符 x 结尾的字串的数量，
也就是 hash[x] = count(all) （更新前的 all 的数量）

&emsp;&emsp;另外题意里不算空集，所以最后结果需要减 1。

# 代码
```php
class LeetCode0940
{
    public function distinctSubseqII($s) {
        if ($s === null || strlen($s) == 0) {
            return 0;
        }
        $m = 1000000007;
        $hash = array();
        $all = 1;       //一个字符都没遍历的时候，有一个空集
        $arr = str_split($s);
        foreach ($arr as $x) {
            $newAdd = $all;
            $curAll = $all;
            $curAll = ($curAll + $newAdd) % $m;
            $curAll = ($curAll - ($hash[$x] ?? 0) + $m) % $m;
            $all = $curAll;
            $hash[$x] = $newAdd;
        }
        return $all - 1;
    }
    
    // 忽略题目中 对 10^9 + 7 取余 的代码
    public function distinctSubseqII2($s) {
        if ($s === null || strlen($s) == 0) {
            return 0;
        }
        $hash = array();
        $all = 1;
        $arr = str_split($s);
        foreach ($arr as $x) {
            $newAdd = $all;
            $curAll = $all + $newAdd - ($hash[$x] ?? 0);
            $all = $curAll;
            $hash[$x] = $newAdd;
        }
        return $all - 1;
    }
    
    
}

```

