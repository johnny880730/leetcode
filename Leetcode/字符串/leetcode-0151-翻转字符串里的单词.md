# 151. 翻转字符串里的单词

# 题目
给你一个字符串 s ，逐个翻转字符串中的所有 单词 。

单词 是由非空格字符组成的字符串。s 中使用至少一个空格将字符串中的 单词 分隔开。

请你返回一个翻转 s 中单词顺序并用单个空格相连的字符串。

说明：
- 输入字符串 s 可以在前面、后面或者单词间包含多余的空格。
- 翻转后单词间应当仅用一个空格分隔。
- 翻转后的字符串中不应包含额外的空格。

https://leetcode.cn/problems/reverse-words-in-a-string/

# 示例：
```
输入：s = "the sky is blue"
输出："blue is sky the"
```

```
输入：s = "  hello world  "
输出："world hello"
解释：输入字符串可以在前面或者后面包含多余的空格，但是翻转后的字符不能包括。
```

```
输入：s = "a good   example"
输出："example good a"
解释：如果两个单词间有多余的空格，将翻转后单词间的空格减少到只含一个。
```

```
输入：s = "  Bob    Loves  Alice   "
输出："Alice Loves Bob"
```

```
输入：s = "Alice does not even like bob"
输出："bob like even not does Alice"
```

提示：
- 1 <= s.length <= 10^4
- s 包含英文大小写字母、数字和空格
- s 中 至少存在一个 单词

# 解析

## 双指针
算法解析：
- 倒序遍历字符串 s ，记录单词左右索引边界 i, j 。
- 每确定一个单词的边界，则将其添加至单词列表 arr 。
- 最终，将单词列表拼接为字符串，并返回即可。

一上来 s 去掉首尾空格后，i、j 此时指向的是最后一个字符。循环执行：
- 索引 i 从右向左搜索首个空格
- 添加单词 s[i + 1 : j + 1] 到 arr
- 索引 i 跳过两单词间的所有空格
- 执行 j = i，此时 j 指向下一个单词的尾字符

# 代码

### php
```php
class LeetCode0151 {
    
    function reverseWords($s) {
        $s = trim($s);
        $arr = [];
        $j = strlen($s) - 1;
        $i = $j;
        while ($i >= 0) {
            while ($i >= 0 && $s[$i] != ' ') {
                $i--;
            }
            $arr[] = substr($s, $i + 1, $j - ($i + 1) + 1) . ' ';
            while ($i >= 0 && $s[$i] == ' ') {
                $i--;
            }
            $j = $i;
        }
        $res = trim(join('', $arr));
        return $res;
    }
}
```

### java
```java
class LeetCode0151 {
    
    public String reverseWords(String s) {
        s = s.trim();
        int j = s.length() - 1, i = j;
        StringBuilder sb = new StringBuilder();
        while (i >= 0) {
            while (i >= 0 && s.charAt(i) != ' ') {
                i--;
            }
            sb.append(s.substring(i + 1, j + 1) + " ");
            while (i >= 0 && s.charAt(i) == ' ') {
                i--;
            }
            j = i;
        }
        return sb.toString().trim();
    }
}
```

### go
```go
func reverseWords(s string) string {
	s = strings.TrimLeft(s, " ")
	s = strings.TrimRight(s, " ")
	j := len(s) - 1
	i := j
	arr := make([]string, 0)
	for i >= 0 {
		for i >= 0 && s[i] != ' ' {
			i--
		}
		arr = append(arr, s[i+1:j+1]+" ")
		for i >= 0 && s[i] == ' ' {
			i--
		}
		j = i
	}
	res := strings.Join(arr, "")
	res = strings.TrimRight(res, " ")
	return res
}
```
