# 0125. 验证回文串

# 题目
给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。

说明：本题中，我们将空字符串定义为有效的回文串。

https://leetcode.cn/problems/valid-palindrome/description/

提示：
- 1 <= s.length <= 2 * 10^5
- 字符串 s 由 ASCII 字符组成

# 示例
```
输入: "A man, a plan, a canal: Panama"
输出: true
解释："amanaplanacanalpanama" 是回文串
```
```
输入: "race a car"
输出: false
解释："raceacar" 不是回文串
```


# 解析

## 双指针

分别定义左指针 left，指向字符串左边第一个元素；右指针 right 指向字符串右边第一个元素

左指针 left 还是右指针 right，其向前移动的情况有两种：
- 当前考察的字符不是字母或数字
- 当前指针 left 与指针 right 指向的字符相同

最后如果指针 left 与指针 right 指向的字符不同，则该字符串不是回文串。当指针 left 指向的位置大于指针 right 指向的位置时，
说明所有字符考察完毕，则所考察的字符串是回文串。

# 代码

### php
```php
class LeetCode0125 {
    public function main($arg) {
        return $this->isPalindrome($arg);
    }
    
    public function isPalindrome($s) {
        $s = strtolower($s);
        $i = 0;
        $j = strlen($s) - 1;
        while ($i < $j) {
            while ($i < $j && !ctype_alnum($s[$i])) {
                $i++;
            }
            while ($i < $j && !ctype_alnum($s[$j])) {
                $j--;
            }
            if ($s[$i] != $s[$j]) {
                return false;
            }
            $i++;
            $j--;
        }
        return true;
    }
}
```

### go
```go
func isPalindrome(s string) bool {
    s = strings.ToLower(s)
    i, j := 0, len(s) - 1
    for i < j {
        for i < j && !unicode.IsLetter(rune(s[i])) && !unicode.IsNumber(rune(s[i])) {
            i++
        }
        for i < j && !unicode.IsLetter(rune(s[j])) && !unicode.IsNumber(rune(s[j])) {
            j--
        }
        if s[i] != s[j] {
            return false
        }
        i++
        j--
    }
    return true
}
```
            
### java
```java
class Solution {
    public boolean isPalindrome(String s) {
        int i = 0, j = s.length() - 1;
        while (i < j) {
            while (i < j && !Character.isLetterOrDigit(s.charAt(i))) {
                i++;
            }
            while (i < j && !Character.isLetterOrDigit(s.charAt(j))) {
                j--;
            }
            if (Character.toLowerCase(s.charAt(i)) != Character.toLowerCase(s.charAt(j))) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
}
```