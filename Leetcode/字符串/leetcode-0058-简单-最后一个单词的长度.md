# 0058. 最后一个单词的长度

https://leetcode.cn/problems/length-of-last-word/

# 题目
给你一个字符串 s，由若干单词组成，单词前后用一些空格字符隔开。返回字符串中 最后一个 单词的长度。

单词 是指仅由字母组成、不包含任何空格字符的最大子字符串。

https://leetcode.cn/problems/length-of-last-word/

提示：
- 1 <= s.length <= 10^4
- s 仅有英文字母和空格 ' ' 组成
- s 中至少存在一个单词

# 示例
```
输入：s = "Hello World"
输出：5
解释：最后一个单词是“World”，长度为 5。
```
```
输入：s = "   fly me   to   the moon  "
输出：4
解释：最后一个单词是“moon”，长度为 4。
```
```
输入：s = "luffy is still joyboy"
输出：6
解释：最后一个单词是长度为 6 的“joyboy”。
```

# 解析
从右向左遍历，从第一个不是空格的字符开始计数，一旦开始计数，再遇到空格就结束了


# 代码

### php
```php
class LeetCode0058 {

    function lengthOfLastWord($s) {
        $len = strlen($s);
        $j = $len - 1;
        while ($s[$j] == ' ') {
            $j--;
        }

        $i = $j - 1;
        if ($i < 0 || $s[$i] == ' ') {
            return 1;
        }
        while ($s[$i] != ' ' && $i >= 0) {
            $i--;
        }
        return $j - $i;
    }

}
```

### java
```java
class LeetCode0058 {
    
    public int lengthOfLastWord(String s) {
        int length = s.length();
        int j = length - 1;
        while (j >= 0 && s.charAt(j) == ' ') {
            j--;
        }

        int i = j - 1;
        if (i < 0 || s.charAt(i) == ' ') {
            return 1;
        }
        while (i >= 0 && s.charAt(i) != ' ') {
            i--;
        }
        return j - i;
    }
}
```

### go
```go
func lengthOfLastWord(s string) int {
    length := len(s)
    j := length - 1
    for j >= 0 && s[j] == ' ' {
        j--
    }

    i := j - 1
    if i < 0 || s[i] == ' ' {
        return 1
    }
    for i >= 0 && s[i] != ' ' {
        i--
    }
    return j - i
}
```
