# 1071. 字符串的最大公因子

# 题目
对于字符串 s 和 t，只有在 s = t + ... + t（t 自身连接 1 次或多次）时，我们才认定 “t 能除尽 s”。

给定两个字符串 str1 和 str2 。返回 最长字符串 x，要求满足 x 能除尽 str1 且 x 能除尽 str2 。

提示：
- 1 <= str1.length, str2.length <= 1000
- str1 和 str2 由大写英文字母组成

# 示例
```
示例 1：

输入：str1 = "ABCABC", str2 = "ABC"
输出："ABC"
```
```
示例 2：

输入：str1 = "ABABAB", str2 = "ABAB"
输出："AB"
```
```
示例 3：

输入：str1 = "LEET", str2 = "CODE"
输出：""
```

# 解析

## gcd 算法
看到标题里面有最大公因子这个词，先要想起 gcd 算法，它用来求两个数的 **最大公约数**，以下是用 java 实现的 gcd 算法的代码：

```
// 使用递归的方式实现欧几里得算法
int gcd(int a, int b) {
    if (b == 0) {
        return a;
    }
    return gcd(b, a % b);
}
```

如果它们有公因子 abc，那么 str1 就是 m 个 abc 的重复，str2 是 n 个 abc 的重复，连起来就是 m + n 个 abc，好像 m + n 个 abc 和
n + m 个 abc 是一样的。

所以如果 str1 + str2 === str2 + str1 就意味着有解。

也很容易想到 str1 + str2 !== str2 + str1 也是无解的充要条件。

当确定有解的情况下，最优解是长度为 gcd(str1.length, str2.length) 的字符串。

这个理论最优长度是不是每次都能达到呢？是的。

因为如果能循环以它的约数为长度的字符串，自然也能够循环以它为长度的字符串，所以这个理论长度就是要找的最优解。



# 代码

### php
```php
class LeetCode1071 {

    /**
     * @param String $str1
     * @param String $str2
     * @return String
     */
    function gcdOfStrings($str1, $str2) {
        if ($str1 . $str2 !== $str2 . $str1) {
            return "";
        }
        $len = $this->_gcd(strlen($str1), strlen($str2));
        return substr($str1, 0, $len);
    }

    private function _gcd($m, $n) {
        if ($n == 0) {
            return $m;
        }
        return $this->_gcd($n, $m % $n);
    }
}
```

### java
```java
class LeetCode1071 {
    
    public String gcdOfStrings(String str1, String str2) {
        String concat1 = str1 + str2;
        String concat2 = str2 + str1;
        if (!concat1.equals(concat2)) {
            return "";
        }
        return str1.substring(0, _gcd(str1.length(), str2.length()));
    }

    private int _gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return _gcd(b, a % b);
    }
}
```

### go
```go
func gcdOfStrings(str1 string, str2 string) string {
    if (str1 + str2 != str2 + str1) {
        return ""
    }
    return str1[0:_gcd(len(str1), len(str2))]
}

func _gcd(a int, b int) int {
    if b == 0 {
        return a
    }
    return _gcd(b, a % b)
}
```

### go
```go
```
