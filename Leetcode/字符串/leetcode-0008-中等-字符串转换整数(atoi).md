# 8. 字符串转换整数 (atoi)

# 题目
请你来实现一个 myAtoi(string s) 函数，使其能将字符串转换成一个 32 位有符号整数（类似 C/C++ 中的 atoi 函数）。

函数 myAtoi(string s) 的算法如下：
1. 读入字符串并丢弃无用的前导空格
2. 检查下一个字符（假设还未到字符末尾）为正还是负号，读取该字符（如果有）。 确定最终结果是负数还是正数。 如果两者都不存在，则假定结果为正。
3. 读入下一个字符，直到到达下一个非数字字符或到达输入的结尾。字符串的其余部分将被忽略。
4. 将前面步骤读入的这些数字转换为整数（即，"123" -> 123， "0032" -> 32）。如果没有读入数字，则整数为 0 。必要时更改符号（从步骤 2 开始）。
5. 如果整数数超过 32 位有符号整数范围 [−2^31,  2^31 − 1] ，需要截断这个整数，使其保持在这个范围内。
   具体来说，小于 −2^31 的整数应该被固定为 −2^31 ，大于 2^31 − 1 的整数应该被固定为 2^31 − 1 。
6. 返回整数作为最终结果。

https://leetcode.cn/problems/string-to-integer-atoi/

注意：
- 本题中的空白字符只包括空格字符 ' ' 。
- 除前导空格或数字后的其余字符串外，请勿忽略 任何其他字符。

# 示例
```
输入：s = "42"
输出：42
解释：加粗的字符串为已经读入的字符，插入符号是当前读取的字符。
第 1 步："42"（当前没有读入字符，因为没有前导空格）
         ^
第 2 步："42"（当前没有读入字符，因为这里不存在 '-' 或者 '+'）
         ^
第 3 步："42"（读入 "42"）
           ^
解析得到整数 42 。
由于 "42" 在范围 [-2^31, 2^31 - 1] 内，最终结果为 42 。
```
```
输入：s = "   -42"
输出：-42
解释：
第 1 步："   -42"（读入前导空格，但忽视掉）
            ^
第 2 步："   -42"（读入 '-' 字符，所以结果应该是负数）
             ^
第 3 步："   -42"（读入 "42"）
               ^
解析得到整数 -42 。
由于 "-42" 在范围 [-2^31, 2^31 - 1] 内，最终结果为 -42 。
```
```
输入：s = "4193 with words"
输出：4193
解释：
第 1 步："4193 with words"（当前没有读入字符，因为没有前导空格）
         ^
第 2 步："4193 with words"（当前没有读入字符，因为这里不存在 '-' 或者 '+'）
         ^
第 3 步："4193 with words"（读入 "4193"；由于下一个字符不是一个数字，所以读入停止）
             ^
解析得到整数 4193 。
由于 "4193" 在范围 [-231, 231 - 1] 内，最终结果为 4193 。
```
```
输入：s = "words and 987"
输出：0
解释：
第 1 步："words and 987"（当前没有读入字符，因为没有前导空格）
         ^
第 2 步："words and 987"（当前没有读入字符，因为这里不存在 '-' 或者 '+'）
         ^
第 3 步："words and 987"（由于当前字符 'w' 不是一个数字，所以读入停止）
         ^
解析得到整数 0 ，因为没有读入任何数字。
由于 0 在范围 [-231, 231 - 1] 内，最终结果为 0 。
```
```
输入：s = "-91283472332"
输出：-2147483648
解释：
第 1 步："-91283472332"（当前没有读入字符，因为没有前导空格）
         ^
第 2 步："-91283472332"（读入 '-' 字符，所以结果应该是负数）
          ^
第 3 步："-91283472332"（读入 "91283472332"）
                     ^
解析得到整数 -91283472332 。
由于 -91283472332 小于范围 [-231, 231 - 1] 的下界，最终结果被截断为 -231 = -2147483648 。
```

# 解析

## 简单的解法
根据题意直接模拟即可
- 先去除前后空格
- 检测正负号
- 读入数字，并用字符串存储数字结果
- 将数字字符串转为整数，根据正负号转换整数结果
- 判断整数范围，返回最终结果

## 左程云 java 版本
首先检查 str 是否符合日常书写的整数形式，具体判断如下：
1. 如果 str 不以 - 开头，也不以数字字符开头，例如，str == "A12"，返回 false。
2. 如果 str 以 - 开头，但是 str 的长度为 1，即 str == "-"，返回 false。如果 str 的长度大于 1，但是 - 的后面紧跟着 0 ，
   例如，str == "-0" 或 "-012"，返回 false。
3．如果 str 以 0 开头，但是 str 的长度大于 1，例如，str == "023"，返回 false。
4．如果经过以上都没有返回，接下来检查 str[1..N-1] 是否都是数字字符，如果有一个不是数字字符，则返回 false。
   如果都是数字字符，说明 str 符合日常书写，返回 true
   
具体检查过程请参看如下代码中的 isValid() 方法。

如果 str 不符合日常书写的整数形式，根据题目要求，直接返回 0 即可。如果符合，继续往下走。

① 生成 4 个变量。
- 布尔型常量 posi。表示转换的结果是负数还是非负数，这完全由 str 开头的字符决定，如果以“-”开头，
  那么转换的结果一定是负数，则 posi 为 false，否则 posi 为 true。
- 整型常量 minq。minq = Integer.MIN_VALUE / 10，即 32 位整数最小值除以 10 得到的商，其意义稍后说明。
- 整型常量 minr。minr = Integer.MIN_VALUE % 10，即 32 位整数最小值除以 10 得到的余数，其意义稍后说明。
- 整型变量 res。转换的结果，初始时 res = 0。

② 32 位整数的最小值为 -2147483648，32 位整数的最大值为 2147483647。可以看出，**最小值的绝对值比最大值的绝对值大 1，
所以转换过程中的绝对值一律以负数的形式出现**。然后根据 posi 决定最后返回什么。
- 如果 str = "123"，转换完成后的结果是 -123，posi = true，所以最后返回 123。
- 如果 str = "-123"，转换完成后的结果是 -123，posi = false，所以最后返回 -123。
- 如果 str = "-2147483648"，转换完成后的结果是 -2147483648，posi = false，所以最后返回 -2147483648。
- 如果 str = "2147483648"，转换完成后的结果是 -2147483648，posi = true，此时发现-2147483648 变成 2147483648 
  会产生溢出，所以返回 0。
 
也就是说，既然负数比正数拥有更大的绝对值范围，那么转换过程中一律以负数的形式记录绝对值，最后再决定返回的数到底是什么。

③ 如果 str 以 - 开头，从 str[1] 开始从左往右遍历 str，否则从 str[0] 开始从左往右遍历 str。举例说明转换过程，
1. 比如 str = "123"：
  - 遍历到 1 时，res = res * 10 + (-1) = -1
  - 遍历到 2 时，res = res * 10 + (-2) = -12
  - 遍历到 3 时，res = res * 10 + (-3) = -123。
2. 比如 str = "-123"，字符'-'跳过
   - 从字符 1 开始遍历，res = res * 10 + (-1) = -1
   - 遍历到 2 时，res = res * 10 + (-2) = -12，
   - 遍历到 3 时，res = res * 10 + (-3) =-123
 
遍历的过程中如何判断 res 已经溢出了？假设当前字符为 a，那么 '0' - a 就是当前字符所代表的数字的负数形式，记为 cur。
1. 如果在 res 加上 cur 之前，发现 res 已经小于 minq，那么 res 加上 cur 之后一定会溢出，
   - 比如 str = "3333333333"，遍历完倒数第二个字符后，res == -333333333 < minq == -214748364，所以当遍历到最后一个字符时，res * 10 肯定会产生溢出。
2. 如果在 res 加上 cur 之前，发现 res 等于 minq，但又发现 cur 小于 minr，那么 res 加上 cur 之后一定会溢出。
   - 比如 str = "2147483649"，遍历完倒数第二个字符后，res == -214748364 == minq，当遍历到最后一个字符时发现有 res == minq，
     同时也发现 cur == -9 < minr ==-8，那么 res 加上 cur 之后一定会溢出。
3. 出现任何一种溢出情况时，直接返回 0

④ 遍历后得到的 res 根据 posi 的符号决定返回值。如果 posi 为 true，说明结果应该返回正，否则说明应该返回负。
如果 res 正好是 32 位整数的最小值，同时又有 posi 为 true，说明溢出，直接返回 0




# 代码

### php
```php
class LeetCode0008 {

    function myAtoi($s) {
        $s = trim($s);
        $positive = true;
        $start = 0;
        $len = strlen($s);
        if ($s == '') {
            return 0;
        }
        if ($s[0] == '-') {
            $start = 1;
            $positive = false;
        } else if ($s[0] == '+') {
            $start = 1;
            $positive = true;
        } else {
            if (ctype_digit($s[0]) === false) {
                return 0;
            }
        }
        $numString = '';
        for ($i = $start; $i < $len; $i++) {
            if (ctype_digit($s[$i])) {
                $numString .= $s[$i];    
            } else {
                break;
            }
        }
        if (strlen($numString) <= 0) {
            return 0;
        }
        $res = $numString;
        
        if ($positive == false) {
            $res = -$res;
            if ($res < -1 << 31) {
                return -1 << 31;
            }
        } else {
            if ($res > (1 << 31) - 1) {
                return (1 << 31) - 1;
            }
        }
        
        return intval($res);
    }
}
```

### go
```go
func myAtoi(s string) int {
    s = strings.Trim(s, " ")
    length := len(s)
    positive := true
    start := 0
    numString := ""
    if s == "" {
        return 0
    }
    if s[0] == '-' {
        start = 1
        positive = false
    } else if s[0] == '+' {
        start = 1
        positive = true
    } else if _, ok := strconv.ParseInt(string(s[0]), 10 ,32); ok != nil {
        return 0
    }
    
    for i := start; i < length; i++ {
        _, ok := strconv.ParseInt(string(s[i]), 10 ,32)
        if ok == nil {
            numString += string(s[i])
        } else {
            break
        }
    }
    if len(numString) <= 0 {
        return 0
    }
    res, _ := strconv.ParseInt(numString, 10 ,64)   // 这里的 bitSize 为 64，因为可能比 -1<<31 小
    if positive == false {
        res = -res
        if res < -1 << 31 {
            return -1 << 31
        }
    } else {
        if res > 1 << 31 - 1 {
            return 1 << 31 - 1
        }
    }
    
    return int(res)
}
```


### java
```java
// 左程云版
class LeetCode0008 {

    public static int myAtoi(String s) {
        if (s == null || s.equals("")) {
            return 0;
        }
        s = removeHeadZero(s.trim());
        if (s == null || s.equals("")) {
            return 0;
        }
        char[] str = s.toCharArray();
        if (!isValid(str)) {
            return 0;
        }
        // str 是符合日常书写的，正经整数形式
        boolean posi = str[0] == '-' ? false : true;
        int minq = Integer.MIN_VALUE / 10;
        int minr = Integer.MIN_VALUE % 10;
        int res = 0;
        int cur = 0;
        for (int i = (str[0] == '-' || str[0] == '+') ? 1 : 0; i < str.length; i++) {
            cur = '0' - str[i];
            if ((res < minq) || (res == minq && cur < minr)) {
                return posi ? Integer.MAX_VALUE : Integer.MIN_VALUE;
            }
            res = res * 10 + cur;
        }
        // res 负
        if (posi && res == Integer.MIN_VALUE) {
            return Integer.MAX_VALUE;
        }
        return posi ? -res : res;
    }

    public static String removeHeadZero(String str) {
        boolean r = (str.startsWith("+") || str.startsWith("-"));
        int s = r ? 1 : 0;
        for (; s < str.length(); s++) {
            if (str.charAt(s) != '0') {
                break;
            }
        }
        // s 已经到了第一个不是'0'字符的位置
        int e = -1;
        // 左<-右
        for (int i = str.length() - 1; i >= (r ? 1 : 0); i--) {
            if (str.charAt(i) < '0' || str.charAt(i) > '9') {
                e = i;
            }
        }
        // e 到了最左的 不是数字字符的位置
        return (r ? String.valueOf(str.charAt(0)) : "") + str.substring(s, e == -1 ? str.length() : e);
    }

    public static boolean isValid(char[] chas) {
        if (chas[0] != '-' && chas[0] != '+' && (chas[0] < '0' || chas[0] > '9')) {
            return false;
        }
        if ((chas[0] == '-' || chas[0] == '+') && chas.length == 1) {
            return false;
        }
        // 0 +... -... num
        for (int i = 1; i < chas.length; i++) {
            if (chas[i] < '0' || chas[i] > '9') {
                return false;
            }
        }
        return true;
    }
}
```
