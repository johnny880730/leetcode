# 459. 重复的子字符串
给定一个非空的字符串，判断它是否可以由它的一个子串重复多次构成。

提示：
- 1 <= s.length <= 10^4
- s 由小写英文字母组成

https://leetcode.cn/problems/repeated-substring-pattern/

# 示例
```
输入: "abab"
输出: True
解释: 可由子字符串 "ab" 重复两次构成。
```
```
示例 2:
输入: "aba"
输出: False
```
```
示例 3:
输入: "abcabcabcabc"
输出: True
解释: 可由子字符串 "abc" 重复四次构成。 (或者子字符串 "abcabc" 重复两次构成。)
```

# 解析

## 枚举
如果一个长度为 n 的字符串 s 可以由它的一个长度为 n′ 的子串 s′ 重复多次构成，那么：
- n 一定是 n′ 的倍数
- s′ 一定是 s 的前缀

因此，可以从小到大枚举 n′，并对字符串 s 进行遍历，进行上述的判断。

注意到一个小优化是，因为子串至少需要重复一次，所以 n′ 不会大于 n 的一半，只需要在 [1, n / 2] 的范围内枚举即可。

# 代码

### php
```php
class Solution0459 {

    // 从头开始由少到多获得子串去拼接
    function repeatedSubstringPattern($s) {
        for ($i = 1, $len = strlen($s); $i <= $len; $i++) {
            // 如果当前指针超过一半了那肯定没有符合要求的子串
            if ($i > floor($len / 2)) {
                return false;
            }
            // 取子串
            $tmp = substr($s, 0, $i);

            if ($len % $i != 0) {
                continue;
            }
            $newS = str_repeat($tmp, $len / $i);
            if ($newS == $s) {
                return true;
            }
        }
        return false;
    }
}
```

### java
```java
class LeetCode0459 {
    
    public boolean repeatedSubstringPattern(String s) {
        int len = s.length();
        for (int i = 0; i < len; i++) {
            if (i > len / 2) {
                return false;
            }
            if (len % (i + 1) != 0) {
                continue;
            }
            int tmpLen = len / (i + 1);
            if (tmpLen == 1) {
                return false;
            }
            String tmp = s.substring(0, i + 1);
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < tmpLen; j++) {
                sb.append(tmp);
            }
            if (sb.toString().equals(s)) {
                return true;
            }
        }
        
        return false;
    }
}
```

### go
```go
func repeatedSubstringPattern(s string) bool {
    len := len(s)
	for i := 0; i < len; i++ {
		if i > len / 2 {
			return false
		}
		if len % (i + 1) != 0 {
			continue
		}
		tmpLen := len / (i + 1)
		if tmpLen == 1 {
			return false
		}
		tmp := s[0 : i + 1]
		var sb strings.Builder
		for j := 0; j < tmpLen; j++ {
			sb.WriteString(tmp)
		}
		if sb.String() == s {
			return true
		}
	}

	return false
}
```