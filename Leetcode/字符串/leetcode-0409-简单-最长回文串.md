# 0409. 最长回文串

# 题目
给定一个包含大写字母和小写字母的字符串 s ，返回 通过这些字母构造成的 最长的回文串 。

在构造过程中，请注意 区分大小写 。比如 "Aa" 不能当做一个回文字符串。

提示：
- 1 <= s.length <= 2000
- s 只由小写 和/或 大写英文字母组成

https://leetcode.cn/problems/longest-palindrome/

# 示例
```
输入:s = "abccccdd"
输出:7
解释:
我们可以构造的最长的回文串是"dccaccd", 它的长度是 7。
```
```
输入:s = "a"
输出:1
```
```
输入:s = "aaaaaccc"
输出:7
```

# 解析
「回文串」是指倒序后和自身完全相同的字符串，即具有关于中心轴对称的性质。观察发现，
- 当回文串长度为偶数时，则所有字符都出现了偶数次；
- 当回文串长度为奇数时，则位于中心的字符出现了奇数次，其余所有字符出现偶数次；

根据以上分析，字符串能被构造成回文串的充要条件为：除了一种字符出现奇数次外，其余所有字符出现偶数次。判别流程如下：
- 借助一个 HashMap ，统计字符串 s 中各字符的出现次数；
- 遍历 HashMap ，统计构造回文串的最大长度，
    - 将当前字符的出现次数向下取偶数（即若为偶数则不变，若为奇数则减 1 ），出现偶数次则都可组成回文串，因此计入 res ；
    - 若此字符出现次数为奇数，则可将此字符放到回文串中心，因此将 odd 置 1 ；
- 返回 res + odd 即可。

# 代码

### php
```php
class LeetCode0409 {
    
    function longestPalindrome($s) {
        // 统计字符数量
        $map = array();
        for ($i = 0, $len = strlen($s); $i < $len; $i++) {
            if (!array_key_exists($s[$i], $map)) {
                $map[$s[$i]] = 0;
            }
            $map[$s[$i]]++;
        }
        // 统计最长回文长度
        $odd = false;
        $res = 0;
        foreach ($map as $ch => $cnt) {
            // 偶数都要，奇数只计算1次
            if ($cnt % 2 == 1) {
                $odd = true;
                $res += $cnt - 1;   // 比如 ccc，只能用2个c
            } else {
                $res += $cnt;
            }
        }
        
        // 如果有奇数个的话最后结果要加1
        return $odd ? $res + 1 : $res;
    }
}
```

### java
```java
class LeetCode0409 {

    public int longestPalindrome(String s) {
        // 统计字符数量
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            if (!map.containsKey(s.charAt(i))) {
                map.put(s.charAt(i), 0);
            }
            map.put(s.charAt(i), map.get(s.charAt(i)) + 1);
        }
        // 统计最长回文长度
        boolean odd = false;
        int res = 0;
        for (Character ch: map.keySet()) {
            int cnt = map.get(ch);
            if (cnt % 2 == 1) {
                odd = true;
                res += cnt - 1;     // 比如 ccc，只能用2个c
            } else {
                res += cnt;
            }
        }
        return odd ? res + 1 : res;
    }
}
```

### go
```go
func longestPalindrome(s string) int {
    // 统计字符数量
    hash := make(map[rune]int)
    for _, ch := range s {
        if _, ok := hash[ch]; ok {
            hash[ch]++
        } else {
            hash[ch] = 1
        }
    }
    // 统计最长回文长度
    odd := false
    res := 0
    for _, cnt := range hash {
        if cnt % 2 == 1 {
            odd = true
            res += cnt - 1       // 比如 ccc，只能用2个c
        } else {
            res += cnt
        }
    }
    if odd {
        return res + 1
    }
    return res
}
```
