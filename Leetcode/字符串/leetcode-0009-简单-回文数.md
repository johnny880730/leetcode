# 0009. 回文数

# 题目
给你一个整数 x ，如果 x 是一个回文整数，返回 true ；否则，返回 false 。

回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。例如，121 是回文，而 123 不是。


提示：
- -2^31 <= x <= 2^31 - 1

https://leetcode.cn/problems/palindrome-number/description/


# 示例
```
示例 1：
输入：x = 121
输出：true
```
```
示例 2：
输入：x = -121
输出：false
解释：从左向右读, 为 -121 。 从右向左读, 为 121- 。因此它不是一个回文数。
```
```
示例 3：
输入：x = 10
输出：false
解释：从右向左读, 为 01 。因此它不是一个回文数。
```
```
示例 4：
输入：x = -101
输出：false
```

# 解析

## 转成字符串
最好理解的一种解法就是先将 整数转为字符串 ，然后将字符串分割为数组，只需要循环数组的一半长度进行判断对应元素是否相等即可。

## 数学解法
通过取整和取余操作获取整数中对应的数字进行比较。

举个例子：1221 这个数字。
- 通过计算 1221 / 1000， 得首位 1
- 通过计算 1221 % 10， 可得末位 1
- 进行比较
- 再将 22 取出来继续比较


# 代码

### php
```php
class Solution0009 {

    // 数学解法
    function isPalindrome($x) {
        if ($x < 0) {
            return false;
        }
        // 计算 x 最大有几位
        $n = 1;
        while ($x / $n >= 10) {
            $n *= 10;
        }
        while ($x > 0) {
            $left = intval($x / $n);
            $right = $x % 10;
            if ($left != $right) {
                return false;
            }
            // x 变成去掉头尾后的数字 
            // 如：x = 1221 % 1000 / 10 => 221 / 10 = 22
            $x = intval(($x % $n) / 10);
            // 左右各去掉了一个数字，规模就小两位
            $n /= 100;
        }

        return true;
    }

}
```

### java
```java
class LeetCode0009 {
    
    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        // 计算 x 最大有几位
        int n = 1;
        while (x / n >= 10) {
            n *= 10;
        }
        while (x > 0) {
            int left = x / n;
            int right = x % 10;
            if (left != right) {
                return false;
            }
            // x 变成去掉头尾后的数字 
            // 如：x = 1221 % 1000 / 10 => 221 / 10 = 22
            x = (x % n) / 10;
            // 左右各去掉了一个数字，规模就小两位
            n /= 100;
        }
        
        return true;
    }
}
```

### go
```go
func isPalindrome(x int) bool {
    if x < 0 {
        return false;
    }
    // 计算 x 最大有几位
    n := 1
    for x / n >= 10 {
        n *= 10
    }
    for x > 0 {
        left := x / n
        right := x % 10
        if left != right {
            return false
        }
		// x 变成去掉头尾后的数字 
        // 如：x = 1221 % 1000 / 10 => 221 / 10 = 22
        x = (x % n) / 10;
        // 左右各去掉了一个数字，规模就小两位
        n /= 100
    }
	
    return true
}
```