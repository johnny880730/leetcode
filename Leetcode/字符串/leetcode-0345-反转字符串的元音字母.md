### 345. 反转字符串中的元音字母

# 题目
给你一个字符串 s ，仅反转字符串中的所有元音字母，并返回结果字符串。

元音字母包括 'a'、'e'、'i'、'o'、'u'，且可能以大小写两种形式出现。

https://leetcode.cn/problems/reverse-vowels-of-a-string/

提示：
- 1 <= s.length <= 3 * 10<sup>5</sup>
- s 由 可打印的 ASCII 字符组成

# 示例
```
输入：s = "hello"
输出："holle"
```
输入：s = "leetcode"
输出："leotcede"
```

# 代码
```php
$s = 'leetcode';
var_dump((new Solution345())->reverseVowels($s));

class Solution345
{
    // 对撞指针
    function reverseVowels($s)
    {
        $arr = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'];
        $len = strlen($s);
        $i = 0; $j = $len - 1;
        while ($i < $j) {
            while ($i < $len && !in_array($s[$i], $arr)) {
                $i++;
            }
            while ($j > 0 && !in_array($s[$j], $arr)) {
                $j--;
            }
            if ($i < $j) {
                $tmp = $s[$i];
                $s[$i] = $s[$j];
                $s[$j] = $tmp;
                $i++;
                $j--;
            }
        }
        return $s;
    }
}
```