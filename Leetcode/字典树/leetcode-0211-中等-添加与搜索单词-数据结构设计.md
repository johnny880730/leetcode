# 0211. 添加与搜索单词 - 数据结构设计

# 题目
请你设计一个数据结构，支持 添加新单词 和 查找字符串是否与任何先前添加的字符串匹配 。

实现词典类 WordDictionary ：
- WordDictionary() 初始化词典对象
- void addWord(word) 将 word 添加到数据结构中，之后可以对它进行匹配
- bool search(word) 如果数据结构中存在字符串与 word 匹配，则返回 true ；否则，返回  false 。word 中可能包含一些 '.' ，每个 . 都可以表示任何一个字母。

https://leetcode.cn/problems/design-add-and-search-words-data-structure/description/

提示：
- 1 <= word.length <= 25
- addWord 中的 word 由小写英文字母组成
- search 中的 word 由 '.' 或小写英文字母组成
- 最多调用 10000 次 addWord 和 search

# 示例
```
输入：
["WordDictionary","addWord","addWord","addWord","search","search","search","search"]
[[],["bad"],["dad"],["mad"],["pad"],["bad"],[".ad"],["b.."]]
输出：
[null,null,null,null,false,true,true,true]

解释：
WordDictionary wordDictionary = new WordDictionary();
wordDictionary.addWord("bad");
wordDictionary.addWord("dad");
wordDictionary.addWord("mad");
wordDictionary.search("pad"); // 返回 False
wordDictionary.search("bad"); // 返回 True
wordDictionary.search(".ad"); // 返回 True
wordDictionary.search("b.."); // 返回 True
```

# 解析

## 字典树
根据题意，WordDictionary 类需要支持添加单词和搜索单词的操作，可以使用字典树实现。

对于添加单词，将单词添加到字典树中即可。

对于搜索单词，从字典树的根结点开始搜索。由于待搜索的单词可能包含点号，因此在搜索过程中需要考虑点号的处理。对于当前字符是字母和点号的情况，分别按照如下方式处理：
- 如果当前字符是字母，则判断当前字符对应的子结点是否存在，如果子结点存在则移动到子结点，继续搜索下一个字符，如果子结点不存在则说明单词不存在，返回 false
- 如果当前字符是点号，由于点号可以表示任何字母，因此需要对当前结点的所有非空子结点继续搜索下一个字符。

重复上述步骤，直到返回 false 或搜索完给定单词的最后一个字符。

如果搜索完给定的单词的最后一个字符，则当搜索到的最后一个结点的 isEnd 为 true 时，给定的单词存在。

特别地，当搜索到点号时，只要存在一个非空子结点可以搜索到给定的单词，即返回 true。

# 代码

### php
```php
class Node0211
{
    public $isEnd = false;
    public $next = [];

    public function __construct($isEnd = false) {
        $this->isEnd = $isEnd;
    }
}

class WordDictionary_Leetcode0211
{
    private $root;

    public function __construct() {
        $this->root = new Node0211();
    }

    public function addWord($word) {
        $node = $this->root;
        $len = strlen($word);
        for ($i = 0; $i < $len; $i++) {
            $c = $word[$i];
            if (!array_key_exists($c, $node->next) || !$node->next[$c]) {
                $node->next[$c] = new Node0211();
            }
            $node = $node->next[$c];
        }
        $node->isEnd = true;
    }

    public function search($word) {
        return $this->_match($this->root, $word, 0);
    }

    private function _match(Node0211 $node, String $word, int $index) {
        if ($index == strlen($word)) {
            return $node->isEnd;
        }
        $c = $word[$index];
        if ($c != '.') {
            if (!array_key_exists($c, $node->next) || !$node->next[$c]) {
                return false;
            }
            return $this->_match($node->next[$c], $word, $index + 1);
        } else {
            foreach (array_keys($node->next) as $nextChar) {
                if ($this->_match($node->next[$nextChar], $word, $index + 1)) {
                    return true;
                }
            }
            return false;
        }
    }
}
```