# 0208. 实现 Trie (前缀树)

# 题目
Trie（发音类似 "try"）或者说 前缀树 是一种树形数据结构，用于高效地存储和检索字符串数据集中的键。这一数据结构有相当多的应用情景，例如自动补完和拼写检查。

请你实现 Trie 类：
- Trie()： 初始化前缀树对象。
- void insert(String word)： 向前缀树中插入字符串 word 。
- boolean search(String word)： 如果字符串 word 在前缀树中，返回 true（即，在检索之前已经插入）；否则，返回 false 。
- boolean startsWith(String prefix)： 如果之前已经插入的字符串 word 的前缀之一为 prefix ，返回 true ；否则，返回 false 。

https://leetcode.cn/problems/implement-trie-prefix-tree/description/

提示：
- 1 <= word.length, prefix.length <= 2000
- word 和 prefix 仅由小写英文字母组成
- insert、search 和 startsWith 调用次数 总计 不超过 3 * 10000 次


# 示例
```
输入
["Trie", "insert", "search", "search", "startsWith", "insert", "search"]
[[], ["apple"], ["apple"], ["app"], ["app"], ["app"], ["app"]]
输出
[null, null, true, false, true, null, true]
解释
Trie trie = new Trie();
trie.insert("apple");
trie.search("apple");   // 返回 True
trie.search("app");     // 返回 False
trie.startsWith("app"); // 返回 True
trie.insert("app");
trie.search("app");     // 返回 True
```

# 代码

### php
```php
class Trie {

    private $root;
    
    function __construct() {
        $this->root = new TrieNode();
    }

    /**
     * @param String $word
     * @return NULL
     */
    function insert($word) {
        if (!$word) {
            return;
        }
        $node = $this->root;
        for ($i = 0 ; $i < strlen($word); $i++) {
            $idx = ord($word[$i]) - ord('a');
            if (!array_key_exists($idx, $node->nexts) || $node->nexts[$idx] == null) {
                $node->nexts[$idx] = new TrieNode();
            }
            $node = $node->nexts[$idx];
            $node->pass++;
        }
        $node->end++;
    }

    /**
     * @param String $word
     * @return Boolean
     */
    function search($word) {
        if (!$word) {
            return false;
        }
        $node = $this->root;
        for ($i = 0; $i < strlen($word); $i++) {
            $idx = ord($word[$i]) - ord('a');
            if (!array_key_exists($idx, $node->nexts) || $node->nexts[$idx] == null) {
                return false;
            }
            $node = $node->nexts[$idx];
        }
        return $node->end > 0;
    }
    

    /**
     * @param String $prefix
     * @return Boolean
     */
    function startsWith($prefix) {
        if (!$prefix) {
            return false;
        }
        $node = $this->root;
        for($i = 0; $i < strlen($prefix); $i++) {
            $idx = ord($prefix[$i]) - ord('a');
            if (!array_key_exists($idx, $node->nexts) || $node->nexts[$idx] == null) {
                return false;
            }
            $node = $node->nexts[$idx];
        }
        return boolval($node->pass);
    }
}

class TrieNode {

    public $pass;   //有多少字符串经过这个节点
    public $end;    //有多少字符串以这个节点为结尾的
    public $nexts;  //指向下一个字符的数组
    
    public function __construct() {
        $this->pass = 0;
        $this->end = 0;
        $this->nexts = array();
    }
}
```

### java
```java
class Trie {

    private TrieNode root;

    public Trie() {
        this.root = new TrieNode();
    }
    
    public void insert(String word) {
        if (word.length() == 0) {
            return;
        }
        TrieNode node = this.root;
        for (int i = 0; i < word.length(); i++) {
            int idx = word.charAt(i) - 'a';
            if (!node.nexts.containsKey(idx) || node.nexts.get(idx) == null) {
                node.nexts.put(idx, new TrieNode());
            }
            node = node.nexts.get(idx);
            node.pass++;
        }
        node.end++;
    }
    
    public boolean search(String word) {
        if (word.length() == 0) {
            return false;
        }
        TrieNode node = this.root;
        for (int i = 0; i < word.length(); i++) {
            int idx = word.charAt(i) - 'a';
            if (!node.nexts.containsKey(idx) || node.nexts.get(idx) == null) {
                return false;
            }
            node = node.nexts.get(idx);
        }
        return node.end > 0 ;
    }
    
    public boolean startsWith(String prefix) {
        if (prefix.length() == 0) {
            return false;
        }
        TrieNode node = this.root;
        for (int i = 0; i < prefix.length(); i++) {
            int idx = prefix.charAt(i) - 'a';
            if (!node.nexts.containsKey(idx) || node.nexts.get(idx) == null) {
                return false;
            }
            node = node.nexts.get(idx);
        }
        return node.pass > 0;
    }
}

class TrieNode {

    public int pass;
    public int end;
    public HashMap<Integer, TrieNode> nexts;

    public TrieNode() {
        this.pass = 0;
        this.end = 0;
        this.nexts = new HashMap<>();
    }
}
```

### go
```go
type Trie struct {
    Root    *TrieNode
}

type TrieNode struct {
    Pass    int
    End     int
    Nexts   []*TrieNode
}

func Constructor() Trie {
    return Trie{Root: &TrieNode{
        Nexts: make([]*TrieNode, 26),
    }}
}


func (this *Trie) Insert(word string)  {
    length := len(word)
    node := this.Root
    for i := 0; i < length; i++ {
        idx := word[i]
        if node.Nexts[idx - 'a'] == nil {
            node.Nexts[idx - 'a'] = &TrieNode{Nexts: make([]*TrieNode, 26)}
        }
        node = node.Nexts[idx - 'a']
        node.Pass++
    }
    node.End++
}


func (this *Trie) Search(word string) bool {
    length := len(word)
    if length <= 0 {
        return false
    }
    node := this.Root
    for i := 0; i < length; i++ {
        idx := word[i]
        if node.Nexts[idx - 'a'] == nil {
            return false
        }
        node = node.Nexts[idx - 'a']
    }
    return node.End > 0
}


func (this *Trie) StartsWith(prefix string) bool {
    length := len(prefix)
    if length <= 0 {
        return false
    }
    node := this.Root
    for i := 0; i < length; i++ {
        idx := prefix[i]
        if node.Nexts[idx - 'a'] == nil {
            return false
        }
        node = node.Nexts[idx - 'a']
    }
    return node.Pass > 0
}
```
