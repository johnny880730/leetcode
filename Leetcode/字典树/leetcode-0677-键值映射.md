### 677. 键值映射

# 题目
设计一个 map ，满足以下几点:
- 字符串表示键，整数表示值
- 返回具有前缀等于给定字符串的键的值的总和

实现一个 MapSum 类：
- MapSum() 初始化 MapSum 对象
- void insert(String key, int val) 插入 key-val 键值对，字符串表示键 key ，整数表示值 val 。如果键 key 已经存在，
  那么原来的键值对 key-value 将被替代成新的键值对。
- int sum(string prefix) 返回所有以该前缀 prefix 开头的键 key 的值的总和。

https://leetcode.cn/problems/map-sum-pairs

提示：
- 1 <= key.length, prefix.length <= 50
- key 和 prefix 仅由小写英文字母组成
- 1 <= val <= 1000
- 最多调用 50 次 insert 和 sum

# 示例
```
输入：
["MapSum", "insert", "sum", "insert", "sum"]
[[], ["apple", 3], ["ap"], ["app", 2], ["ap"]]
输出：
[null, null, 3, null, 5]

解释：
MapSum mapSum = new MapSum();
mapSum.insert("apple", 3);  
mapSum.sum("ap");           // 返回 3 (apple = 3)
mapSum.insert("app", 2);    
mapSum.sum("ap");           // 返回 5 (apple + app = 3 + 2 = 5)
```


# 解析

&emsp;&emsp;


# 代码
```php
class Node_Leetcode_677 {
    public $value;
    public $next;
    public function __construct(int $value = 0) {
        $this->value = 0;
        $this->next = [];
    }
}

class MapSum_Leetcode_0677
{
    private $root;

    public function __construct() {
        $this->root = new Node_Leetcode_677();
    }

    public function insert(string $word, int $val) {
        $cur = $this->root;
        for ($i = 0; $i < strlen($word); $i++) {
            $c = $word[$i];
            if (!array_key_exists($c, $cur->next) || !$cur->next[$c]) {
                $cur->next[$c] = new Node_Leetcode_677();
            }
            $cur = $cur->next[$c];
        }
        $cur->value = $val;
    }

    public function sum(string $prefix) {
        $cur = $this->root;
        for ($i = 0; $i < strlen($prefix); $i++) {
            $c = $prefix[$i];
            if (!array_key_exists($c, $cur->next) || !$cur->next[$c]) {
                return 0;
            }
            $cur = $cur->next[$c];
        }
        return $this->sumNode($cur);
    }

    public function sumNode(Node_Leetcode_677 $node) {
        if (count($node->next) == 0) {
            return $node->value;
        }
        $res = $node->value;
        foreach (array_keys($node->next) as $c) {
            $res += $this->sumNode($node->next[$c]);
        }
        return $res;
    }
}
```