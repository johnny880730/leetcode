# 0212. 单词搜索 II

# 题目
给定一个 m x n 二维字符网格 board 和一个单词（字符串）列表 words， 返回所有二维网格上的单词 。

单词必须按照字母顺序，通过 相邻的单元格 内的字母构成，其中“相邻”单元格是那些水平相邻或垂直相邻的单元格。同一个单元格内的字母在一个单词中不允许被重复使用。

https://leetcode.cn/problems/word-search-ii/description/

提示：
- m == board.length
- n == board[i].length
- 1 <= m, n <= 12
- board[i][j] 是一个小写英文字母
- 1 <= words.length <= 3 * 10000
- 1 <= words[i].length <= 10
- words[i] 由小写英文字母组成
- words 中的所有字符串互不相同


# 示例
```
输入：board = [
    ["o","a","a","n"],
    ["e","t","a","e"],
    ["i","h","k","r"],
    ["i","f","l","v"]
], words = ["oath","pea","eat","rain"]
输出：["eat","oath"]
```

```
输入：board = [
    ["a","b"],
    ["c","d"]
], words = ["abcb"]
输出：[]
```

# 解析

## 前缀树
本题的解决方案主要实现两个机制：
1. 已经走过的格子不能重复走（同一个单词内），这样最好是深度优先遍历，这样方便给走过的字符打上标签
2. 走的过程中知道单词被走出来了

解决方案是根据 words 建立前缀树。

走 matrix 的时候，如果发现当前字符不是前缀树的开头，就可以直接跳过了。实际上就是根据前缀树明白了根据当前位置有没有必要往四个方向走。

# 代码

### php
```php
class LeetCode0212 {

    function findWords($board, $words) {
        // 构建前缀树
        $head = new TrieNode0212(); // 前缀树最顶端的头
        $set = array();
        foreach ($words as $word) {
            if (!array_key_exists($word, $set)) {
                $set[$word] = true;
                $this->_fillWord($head, $word);
            }
        }
        // 答案
        $res = [];
        // 沿途走过的字符收集起来 放到path里
        $path = [];
        for ($i = 0, $len1 = count($board); $i < $len1; $i++) {
            for ($j = 0, $len2 = count($board[0]); $j < $len2; $j++) {
                $this->_process($board, $i, $j, $path, $head, $res);
            }
        }
        return $res;
    }

    // 把一个单词加到前缀树
    protected function _fillWord($head, $word){
        $head->pass++;
        $node = $head;
        for ($i = 0, $len = strlen($word); $i < $len; $i++) {
            $index = ord($word[$i]) - ord('a');
            if (!$node->nexts[$index]) {
                $node->nexts[$index] = new TrieNode0212();
            }
            $node = $node->nexts[$index];
            $node->pass++;
        }
        $node->end++;
    }

    // 从 board[row][col] 位置的字符出发，之前的路径上走过的字符记录在 path 里
    // cur 还没有登上，有待检查能不能登上去的前缀树的节点
    // 如果找到了 words 里的某个单词，就记录在 res 里
    // 返回值：从 row,col 出发，一共找到了多少个单词
    protected function _process(&$board, $row, $col, $path, $cur, &$res){
        $s = $board[$row][$col];
        if ($s == '0') {        // 之前已经走过了 row,col 位置
            return 0;
        }
        $index = ord($s) - ord('a');
        // 如果没路，或者这条路上最终的字符串已经加入过结果里
        if ($cur->nexts[$index] == null || $cur->nexts[$index]->pass == 0) {
            return 0;
        }
        // 没有走回头路且能登上去
        $cur = $cur->nexts[$index];
        $path[] = $s;
        $fix = 0;       //从row col 出发后，后续一共搞定了多少答案
        // 当来到row col 位置，如果决定不往后走了，是不是已经搞定了某个字符串了
        if ($cur->end > 0) {
            $res[] = join('', $path);
            $cur->end--;
            $fix++;
        }
        // 往上下左右四个方向尝试
        $board[$row][$col] = '0';
        if ($row > 0) {
            $fix += $this->_process($board, $row - 1, $col, $path, $cur, $res);
        }
        if ($row < count($board) - 1) {
            $fix += $this->_process($board, $row + 1, $col, $path, $cur, $res);
        }
        if ($col > 0) {
            $fix += $this->_process($board, $row, $col - 1, $path, $cur, $res);
        }
        if ($col < count($board[0]) - 1) {
            $fix += $this->_process($board, $row, $col + 1, $path, $cur, $res);
        }
        // 回退
        $board[$row][$col] = $s;
        array_pop($path);
        $cur->pass -= $fix;

        return $fix;
    }
}

class TrieNode0212 {

    public $nexts;
    public $pass;
    public $end;

    function __construct() {
        $this->nexts = array_fill(0, 26, null);
        $this->pass = 0;
        $this->end = 0;
    }
}

```