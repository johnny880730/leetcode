# 0050. Pow(x, n)

# 题目
实现 pow(x, n) ，即计算 x 的 n 次幂函数（即，x^n ）。

https://leetcode.cn/problems/powx-n/description/

提示：
- -100.0 < x < 100.0
- -2^31 <= n <= 2^31 - 1
- -10^4 <= x^n <= 10^4

# 示例
```
输入：x = 2.00000, n = 10
输出：1024.00000
```
```
输入：x = 2.10000, n = 3
输出：9.26100
```
```
输入：x = 2.00000, n = -2
输出：0.25000
解释：2^-2 = 1/2² = 1/4 = 0.25
```


# 解析

## 快速幂

快速幂算法的本质是【分治算法】。举个例子，如果要计算 x^64，可以按照：
```
x -> x^2 -> x^4 -> x^8 -> x^16 -> x^32 -> x^64
```  
的顺序，从 x 开始，每次直接把上一次的结果进行平方，计算 6 次就可以得到 x^64 的值，而不需要对 x 乘 63 次 x。

再举一个例子，如果要计算 x^75，75 的二进制表达式为 1001011，也就是  
```
75 = 2^6 + 2^3 + 2^1 + 2^0 = 64 + 8 + 2 + 1
```  

算法步骤：
- 初始化 res = 1，tmp = x
- 当指数 n 的二进制的末尾是 1 时，res 加上当前的 tmp
- 然后无论指数的二进制末尾是否为 1，tmp 每次和自己相乘，也就是每次获得了当前 tmp 的平方数
- 然后指数 n 向右移动一位，直到 n 为零停止。

只要 n 为正整数都可适用上面的情况。当 n 不是正整数时，如计算 x^-7，可以先计算 x^7，然后求个倒数即可。

# 代码

### php
```php
class LeetCode0050 {

    // 本地能跑，但放leetcode就会不通过。但下面的 go 和 java 的能通过
    // chatgpt 说是因为 PHP 的浮点数精度的限制，当进行非常大的幂运算时，可能会出现舍入误差，导致结果不准确
    function myPow($x, $n) {
        return $n >= 0 ? $this->_getPow($x, $n) : 1.0 / $this->_getPow($x, 0 - $n);
    }

    function _getPow($x, $n) {
        $res = 1.0;
        $tmp = $x;
        while ($n > 0) {
            if (($n & 1) != 0) {
                $res *= $tmp;
            }
            $tmp *= $tmp;
            $n >>= 1;
        }
        return bcdiv($res, 1);
    }
}
```

### go
```go
func myPow(x float64, n int) float64 {
    if n >= 0 {
        return _getPow(x, n)
    }
    return 1.0 / _getPow(x, 0 - n)
}

func _getPow(x float64, N int) float64 {
    res := 1.0
    tmp := x
    for N > 0 {
        if (N & 1) != 0 {
            res *= tmp
        }
        tmp *= tmp
        N >>= 1
    }
    return res
}
```

### java
```java
class LeetCode0050 {

    public double myPow(double x, int n) {
        long N = n;
        return N >= 0 ? _getPow(x, N) : 1.0 / _getPow(x, 0 - N);
    }

    private double _getPow(double x, long N) {
        double res = 1.0;
        double tmp = x;
        while (N > 0) {
            if ((N & 1) != 0) {
                res *= tmp;
            }
            tmp *= tmp;
            N >>= 1;
        }
        return res;
    }
}
```
