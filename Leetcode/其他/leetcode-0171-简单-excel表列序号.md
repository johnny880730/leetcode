# 171. Excel 表列序号

# 题目
给你一个字符串 columnTitle ，表示 Excel 表格中的列名称。返回该列名称对应的列序号。例如，
```
    A -> 1
    B -> 2
    C -> 3
    ...
    Z -> 26
    AA -> 27
    AB -> 28
    ...
```

https://leetcode.cn/problems/excel-sheet-column-number/

提示
- 1 <= columnTitle.length <= 7
- columnTitle 仅由大写英文组成
- columnTitle 在范围 ["A", "FXSHRXW"] 内

# 示例
```
输入: columnTitle = "A"
输出: 1
```

```
输入: columnTitle = "AB"
输出: 28
```

```
输入: columnTitle = "ZY"
输出: 701
```

```
输入: columnTitle = "FXSHRXW"
输出: 2147483647
```

# 代码

### php
```php
class LeetCode0171 {

    // 26进制转换10进制
    function titleToNumber($columnTitle) {
        $res = 0;
        $len = strlen($columnTitle);
        for ($i = 0; $i < $len; $i++) {
            $res = $res * 26 + (ord($columnTitle[$i]) - ord('A')) + 1;
        }
        return $res;
    }
}
```

### go
```go
func titleToNumber(columnTitle string) int {
    length := len(columnTitle)
    res := 0
    for i := 0; i < length; i++ {
        res = res * 26 + int(columnTitle[i] - 'A') + 1
    }
    return res
}
```