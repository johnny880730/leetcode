# 315. 计算右侧小于当前元素的个数

# 题目
给你一个整数数组 nums ，按要求返回一个新数组 counts 。数组 counts 有该性质： counts[i] 的值是 nums[i] 右侧小于 nums[i] 的元素的数量。

https://leetcode.cn/problems/count-of-smaller-numbers-after-self

提示：
- 1 <= nums.length <= 10^5
- -10^4 <= nums[i] <= 10^4

# 示例
```
输入：nums = [5,2,6,1]
输出：[2,1,1,0] 
解释：
5 的右侧有 2 个更小的元素 (2 和 1)
2 的右侧仅有 1 个更小的元素 (1)
6 的右侧有 1 个更小的元素 (1)
1 的右侧有 0 个更小的元素
```

```
输入：nums = [-1]
输出：[0]
```

```
输入：nums = [-1,-1]
输出：[0,0]
```

# 解析

## 归并排序的思想
对于A(在左半部分)、B(在右半部分)两个降序数组合并的过程中，当 A 中的某一个元素 a 和 B 中的某一个元素 b 进行比较时，
如果确定 a 比 b 大，那么 a 比 B 中在 b 之后的元素都大，因此 B 中在 b 之后的元素以及 b 都是小于 a 的。

注意这里是降序归并！

搞懂了这个，这个题的核心思路便也出来了。只是题目中，对于数组 nums 的每个元素都要求出 count[i], 最简单的想法，
便是用一个 map 记录元素和下标的对应关系，然后在归并排序的过程中，我们能够求出这个元素的 count[i],但是这里存在一个问题，很可能元素会重复，
所以这里引入索引数组。但是很多题解以上来就使用索引数组，不仅增加了理解难度，还会让人迷失主要的核心思路。

4.降序归并排序加索引数组解决本题
加入了索引数组的代码如下，在归并的时候这里写得更清晰，将元素的临时数组tmp和元素的临时索引数组tmpIndex分开。代码挺好懂的，希望大家看完多点赞多关注，谢谢！


# 代码
```php
class LeetCode0315 {
    public $arr;
    public $res;
    
    
    function __construct(){
        $this->arr = [];
        $this->res = [];
    }
    

    /**
     * @param Integer[] $nums
     * @return Integer[]
     */
    function countSmaller($nums) {
        if (!$nums) {
            return $this->res;
        }
        $len = count($nums);
        $this->res = array_fill(0, $len, 0);
        if ($len < 2) {
            return $this->res;
        }
        $this->arr = array_fill(0, $len, null);
        for($i = 0; $i < $len; $i++) {
            $this->arr[$i] = new Node0315($nums[$i], $i);
        }

        $this->_process( 0, count($this->arr) - 1);
        return $this->res;
    }

    protected function _process( $l, $r){
        if ($l == $r) {
            return;
        }
        $mid = $l + (($r - $l) >> 1);
        $this->_process($l, $mid);
        $this->_process( $mid + 1, $r);
        $this->_merge( $l, $mid, $r);
    }

    protected function _merge( $l, $m, $r){
        $help = array_fill(0, $r - $l + 1, null);
        $i = count($help) - 1;
        $p1 = $m;       //左组最右的位置
        $p2 = $r;       //右组最右的位置
        while ($p1 >= $l && $p2 >= $m + 1) {
            if ($this->arr[$p1]->value > $this->arr[$p2]->value) {
                $this->res[$this->arr[$p1]->index] = $this->res[$this->arr[$p1]->index] + $p2 - $m;
            }
            $help[$i--] = $this->arr[$p1]->value > $this->arr[$p2]->value ? $this->arr[$p1--] : $this->arr[$p2--];
        }
        while ($p1 >= $l) {
            $help[$i--] = $this->arr[$p1--];
        }
        while ($p2 >= $m + 1) {
            $help[$i--] = $this->arr[$p2--];
        }
        for ($i = 0; $i < count($help); $i++) {
            $this->arr[$l + $i] = $help[$i];
        }
    }
}


class Node0315 {
    public $value;
    public $index;

    function __construct ($v, $i) {
        $this->value = $v;
        $this->index = $i;
    }
}


```