# 384. 打乱数组

# 题目
给你一个整数数组 nums ，设计算法来打乱一个没有重复元素的数组。打乱后，数组的所有排列应该是 等可能 的。

实现 Solution class:
- Solution(int[] nums) 使用整数数组 nums 初始化对象
- int[] reset() 重设数组到它的初始状态并返回
- int[] shuffle() 返回数组随机打乱后的结果

https://leetcode.cn/problems/shuffle-an-array

提示：
- 1 <= nums.length <= 50
- -10^6 <= nums[i] <= 10^6
- nums 中的所有元素都是 唯一的
- 最多可以调用 10^4 次 reset 和 shuffle


# 示例
```
输入
["Solution", "shuffle", "reset", "shuffle"]
[[[1, 2, 3]], [], [], []]
输出
[null, [3, 1, 2], [1, 2, 3], [1, 3, 2]]

解释
Solution solution = new Solution([1, 2, 3]);
solution.shuffle();    // 打乱数组 [1,2,3] 并返回结果。任何 [1,2,3]的排列返回的概率应该相同。例如，返回 [3, 1, 2]
solution.reset();      // 重设数组到它的初始状态 [1, 2, 3] 。返回 [1, 2, 3]
solution.shuffle();    // 随机返回数组 [1, 2, 3] 打乱后的结果。例如，返回 [1, 3, 2]
```

# 解析
java 里的 Math.random() 会等概率返回一个 [0, 1) 的 double 类型的小数，定义 R = Math.random()。
如果将 R 乘以一个 int 类型的数 k，可以实现等概率返回一个 [0, k) 的小数。将其取整型的话可以的概率得到 [0, k - 1] 的整数。
将这个方法定义为 f(k)。

定义数组 nums 长度为 N，
- 先调用 f(N) = i，将 nums[i] 和 nums[N - 1] 交换。
- 再调用 f(N - 1) = j，将 nums[j] 与 nums[N - 2] 交换
- 再调用 f(N - 2) = k，将 nums[k] 与 nums[N - 3] 交换
- 以此类推，直到就剩一个数字的范围停止

# 代码
```php
class LeetCode0384 {
    public $origin;         //存原始串
    public $shuffle;
    public $N;

    function __construct($nums) {
        $this->origin = $nums;
        $this->N = count($this->origin);
        $this->shuffle = array_fill(0, $this->N, 0);
        for ($i = 0; $i < $this->N; $i++ ) {
           $this->shuffle[$i] = $this->origin[$i]; 
        }
    }

    function reset() {
        return $this->origin;
    }

    function shuffle() {
        for ($i = $this->N - 1; $i >= 0; $i--) {
            $r = rand(0, $i);
            $tmp = $this->shuffle[$r];
            $this->shuffle[$r] = $this->shuffle[$i];
            $this->shuffle[$i] = $tmp;
        }
        return $this->shuffle;
    }
}
```

### go
```go
type Solution struct {
    OriginArr []int
    ShuffleArr []int
    N int
}

func Constructor(nums []int) Solution {
    length := len(nums)
    arr := make([]int, length)
    for i := 0; i < length; i++ {
        arr[i] = nums[i]
    }
    return Solution{
        OriginArr: nums,
        ShuffleArr: arr,
        N: len(nums),
    }
}

func (this *Solution) Reset() []int {
    return this.OriginArr
}

func (this *Solution) Shuffle() []int {
    r := rand.New(rand.NewSource(time.Now().UnixNano()))
    for i := this.N - 1; i >= 0; i-- {
        j := r.Intn(i + 1)
        this.ShuffleArr[i], this.ShuffleArr[j] = this.ShuffleArr[j], this.ShuffleArr[i]
    }
    return this.ShuffleArr
}
```