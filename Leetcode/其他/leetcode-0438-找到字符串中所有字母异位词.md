# 0438. 找到字符串中所有字母异位词


# 题目
给定两个字符串 s 和 p，找到 s 中所有 p 的 异位词 的子串，返回这些子串的起始索引。不考虑答案输出的顺序。

异位词 指由相同字母重排列形成的字符串（包括相同的字符串）。

https://leetcode.cn/problems/find-all-anagrams-in-a-string/description/



提示：
- 1 <= s.length, p.length <= 3 * 10^4
- s 和 p 仅包含小写字母

# 示例
```
示例 1:

输入: s = "cbaebabacd", p = "abc"
输出: [0,6]
解释:
起始索引等于 0 的子串是 "cba", 它是 "abc" 的异位词。
起始索引等于 6 的子串是 "bac", 它是 "abc" 的异位词。
```
```
示例 2:

输入: s = "abab", p = "ab"
输出: [0,1,2]
解释:
起始索引等于 0 的子串是 "ab", 它是 "ab" 的异位词。
起始索引等于 1 的子串是 "ba", 它是 "ab" 的异位词。
起始索引等于 2 的子串是 "ab", 它是 "ab" 的异位词。
```


# 解析
这个所谓的异位词，其实就是全排列。题目相当于，输入一个串 s，一个串 t，找到 s 中所有 t 的排列，返回他们的起始索引

## 滑动窗口
根据题目要求，需要在字符串 s 寻找字符串 p 的异位词。

因为字符串 p 的异位词的长度一定与字符串 p 的长度相同，所以可以在字符串 s 中构造一个长度为与字符串 p 的长度相同的滑动窗口，并在滑动中维护窗口中每种字母的数量；当窗口中每种字母的数量与字符串 p 中每种字母的数量相同时，则说明当前窗口为字符串 p 的异位词。

在算法的实现中，可以使用数组来存储字符串 p 和滑动窗口中每种字母的数量。

当字符串 s 的长度小于字符串 p 的长度时，字符串 s 中一定不存在字符串 p 的异位词。但是因为字符串 s 中无法构造长度与字符串 p 的长度相同的窗口，所以这种情况需要单独处理。


# 代码
```php

class LeetCode0438 {

    function findAnagrams($s, $t) {
        $lenS = strlen($s);
        $lenT = strlen($t);
        // 看看目标串有哪些字符，每个字符有多少个
        $need = $window = [];
        for ($i = 0; $i < $lenT; $i++) {
            $need[$t[$i]]++;
        }

        $res = [];
        $left = $right = 0;
        $valid = 0;
        while ($right < $lenS) {
            $c = $s[$right];
            $right++;
            // 当前字符是目标串的字符之一，进行窗口内数据的一系列更新
            if (isset($need[$c])) {
                $window[$c]++;
                if ($window[$c] == $need[$c]) {
                    $valid++;
                }
            }

            // 判断左侧是否要收缩
            while ($right - $left >= $lenT) {
                if ($valid == count($need)) {
                    $res[] = $left;
                }
                $d = $s[$left];
                $left++;
                if (isset($need[$d])) {
                    if ($window[$d] == $need[$d]) {
                        $valid--;
                    }
                    $window[$d]--;
                }
            }
        }
        
        return $res;
    }
}
```

### go
```go
func findAnagrams(s string, t string) []int {
	lenS := len(s)
	lenT := len(t)

	// 看看目标串有哪些字符，每个字符有多少个
	need := make(map[byte]int)
	window := make(map[byte]int)

	for i := 0; i < lenT; i++ {
		need[t[i]]++
	}

	var res []int
	left, right := 0, 0
	valid := 0

	for right < lenS {
		c := s[right]
		right++

		// 当前字符是目标串的字符之一，进行窗口内数据的一系列更新
		if _, ok := need[c]; ok {
			window[c]++
			if window[c] == need[c] {
				valid++
			}
		}

		// 判断左侧是否要收缩
		for right - left >= lenT {
			if valid == len(need) {
				res = append(res, left)
			}
			d := s[left]
			left++
			if _, ok := need[d]; ok {
				if window[d] == need[d] {
					valid--
				}
				window[d]--
			}
		}
	}

	return res
}
```

### java
```java
class LeetCode0438 
{

    public List<Integer> findAnagrams(String s, String t) {
        int lenS = s.length();
        int lenT = t.length();

        // 看看目标串有哪些字符，每个字符有多少个
        Map<Character, Integer> need = new HashMap<>();
        Map<Character, Integer> window = new HashMap<>();

        for (int i = 0; i < lenT; i++) {
            char c = t.charAt(i);
            need.put(c, need.getOrDefault(c, 0) + 1);
        }

        List<Integer> res = new ArrayList<>();
        int left = 0, right = 0;
        int valid = 0;

        while (right < lenS) {
            char c = s.charAt(right);
            right++;

            // 当前字符是目标串的字符之一，进行窗口内数据的一系列更新
            if (need.containsKey(c)) {
                window.put(c, window.getOrDefault(c, 0) + 1);
                if (window.get(c).equals(need.get(c))) {
                    valid++;
                }
            }

            // 判断左侧是否要收缩
            while (right - left >= lenT) {
                if (valid == need.size()) {
                    res.add(left);
                }
                char d = s.charAt(left);
                left++;
                if (need.containsKey(d)) {
                    if (window.get(d).equals(need.get(d))) {
                        valid--;
                    }
                    window.put(d, window.get(d) - 1);
                }
            }
        }

        return res;
    }
}
```
