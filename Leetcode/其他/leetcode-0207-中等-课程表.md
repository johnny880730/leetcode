# 0207. 课程表

# 题目
你这个学期必须选修 numCourses 门课程，记为 0 到 numCourses - 1 。

在选修某些课程之前需要一些先修课程。 先修课程按数组 prerequisites 给出，其中 prerequisites[i] = [ai, bi] ，表示如果要学习课程 ai 则 必须 先学习课程  bi 。

例如，先修课程对 [0, 1] 表示：想要学习课程 0 ，你需要先完成课程 1 。
请你判断是否可能完成所有课程的学习？如果可以，返回 true ；否则，返回 false 。

https://leetcode.cn/problems/course-schedule/description/

提示：
- 1 <= numCourses <= 2000
- 0 <= prerequisites.length <= 5000
- prerequisites[i].length == 2
- 0 <= ai, bi < numCourses
- prerequisites[i] 中的所有课程对 互不相同


# 示例
```
输入：numCourses = 2, prerequisites = [[1,0]]
输出：true
解释：总共有 2 门课程。学习课程 1 之前，你需要完成课程 0 。这是可能的。
```

```
输入：numCourses = 2, prerequisites = [[1,0],[0,1]]
输出：false
解释：总共有 2 门课程。学习课程 1 之前，你需要先完成​课程 0 ；并且学习课程 0 之前，你还应先完成课程 1 。这是不可能的。
```

# 解析

## 拓扑排序

示例：n = 6，先决条件表：[[3, 0], [3, 1], [4, 1], [4, 2], [5, 3], [5, 4]]

课 0, 1, 2 没有先修课，可以直接选。其余的课，都有两门先修课。用有向图来展现这种依赖关系（做事情的先后关系）：

![](./images/leetcode-0207-题解.png)

这种叫 有向无环图，把一个 有向无环图 转成 线性的排序 就叫 **拓扑排序**。

有向图有【入度】和【出度】的概念：如果存在一条有向边 A --> B，则这条边给 A 增加了 1 个出度，给 B 增加了 1 个入度。

所以，顶点 0、1、2 的入度为 0。顶点 3、4、5 的入度为 2。

每次只能选你能上的课程：
- 每次只能选入度为 0 的课，因为它不依赖别的课，是当下你能上的课。
- 假设选了 0，课 3 的先修课少了一门，入度由 2 变 1。
- 接着选 1，导致课 3 的入度变 0，课 4 的入度由 2 变 1。
- 接着选 2，导致课 4 的入度变 0。
- 现在，课 3 和课 4 的入度为 0。继续选入度为 0 的课……直到选不到入度为 0 的课。

具体操作：
- 让入度为 0 的课入列，它们是能直接选的课。
- 然后逐个出列，出列代表着课被选，需要减小相关课的入度。
- 如果相关课的入度变为 0，安排它入列、再出列……直到没有入度为 0 的课可入列。

用一个变量 count 记录入列的课程个数，最后判断 count 是否等于有依赖的课程数。

# 代码
```php
class Node0207 {
    public $name;       // 课程的编号
    public $in;         // 入度
    public $nexts;      // 邻居
    
    function __construct ($n) {
        $this->name = $n;
        $this->in = 0;
        $this->nexts = [];
    }
}

class LeetCode0207 {

    function canFinish($numCourses, $prerequisites) {
        if (!$prerequisites) {
            return true;
        }
        // hash   key:课程编号，val:课程node
        $nodes = array();
        foreach ($prerequisites as $arr) {
            $to = $arr[0];
            $from = $arr[1];
            if (!array_key_exists($to, $nodes)) {
                $nodes[$to] = new Node0207($to);
            }
            if (!array_key_exists($from, $nodes)) {
                $nodes[$from] = new Node0207($from);
            }
            $t = $nodes[$to];
            $f = $nodes[$from];
            // from 的邻居加上 to
            $f->nexts[] = $t;
            // to 的入度 +1
            $t->in++;
        }
        $needNum = count($nodes);  //有依赖关系的课程的数量
        $zeroInQueue = new SplDoublyLinkedList();
        // 第一批入度为 0 的课程 进队列
        foreach ($nodes as $node) {
            if ($node->in == 0) {
                $zeroInQueue->push($node);
            }
        }
        // 弹出的课程的数量
        $count = 0;
        while ($zeroInQueue->isEmpty() == false) {
            $cur = $zeroInQueue->shift();
            $count++;
            foreach ($cur->nexts as $next) {
                // 去掉弹出的课程的影响，此时入度变为0的话，进队列
                if (--$next->in == 0) {
                    $zeroInQueue->push($next);
                }
            }
        }
        // 如果弹出的课程的数量 == 实际有依赖课程的数量，说明课程可以实现
        return $count == $needNum;
    }
}
```