# 0470. 用 Rand7() 实现 Rand10()


# 题目
给定方法 rand7 可生成 [1, 7] 范围内的均匀随机整数，试写一个方法 rand10 生成 [1, 10] 范围内的均匀随机整数。

你只能调用 rand7() 且不能调用其他方法。请不要使用系统的 Math.random() 方法。

每个测试用例将有一个内部参数 n，即你实现的函数 rand10() 在测试时将被调用的次数。请注意，这不是传递给 rand10() 的参数。

提示：
- 1 <= n <= 10000

https://leetcode.cn/problems/implement-rand10-using-rand7/description/

# 示例
```
示例 1:

输入: 1
输出: [2]
```
```
示例 2:

输入: 2
输出: [2,8]
```
```
示例 3:

输入: 3
输出: [3,8,10]
```

# 解析
本题的解析看这一篇即可：[从最基础的降级如何做到均匀的生成随机数](../../各种算法/从最基础的讲起如何做到均匀的生成随机数.md)

简单的来说，前置知识点：
- (rand_X() - 1) × Y + rand_Y()，可以等概率的生成[1, X * Y]范围的随机数
- 要等概率的生成 [1, N] 的话，最简单的就是 rand_X() % 10 + 1，其中 X 必须是大于 10 且是 10 的倍数
- 拒绝采样


# 代码

### php
```php
class LeetCode0470 {

    function rand10() {
        while (1) {
            $a = rand7();
            $b = rand7();
            $num = ($a - 1) * 7 + $b;   // [1, 49]
            if ($num <= 40) {
                return $num % 10 + 1;
            }
            $a = $num - 40;             // rand9
            $b = rand7();
            $num = ($a - 1) * 7 + $b;   // [1, 63]
            if ($num <= 60) {
                return $num % 10 + 1;
            }

            $a = $num - 60;             // rand3
            $b = rand7();
            $num = ($a - 1) * 7 + $b;   // [1, 21]
            if ($num <= 20) {
                return $num % 10 + 1;
            }
        }
    }
    
}
```

### java
```java
class LeetCode0470 extends SolBase {
    
    public int rand10() {
        while (true) {
            int a = rand7();
            int b = rand7();
            int num = (a - 1) * 7 + b;  // rand [1, 49]
            if (num <= 40) {
                return num % 10 + 1;    // 拒绝采样
            }

            a = num - 40;               // rand9()
            b = rand7();
            num = (a - 1) * 7 + b;      // rand [1, 63]
            if (num <= 60) {
                return num % 10 + 1;
            }

            a = num - 60;               // rand3()
            b = rand7();
            num = (a - 1) * 7 + b;      // rand [1, 21]
            if (num <= 20) {
                return num % 10 + 1;
            }
        }
    }
}
```

### go
```go
func rand10() int {
    for {
        a := rand7()
        b := rand7()
        num := (a - 1) * 7 + b
        if num <= 40 {
            return num % 10 + 1
        }

        a = num - 40
        b = rand7()
        num = (a - 1) * 7 + b
        if num <= 60 {
            return num % 10 + 1
        }

        a = num - 60
        b = rand7()
        num = (a - 1) * 7 + b
        if num <= 20 {
            return num % 10 + 1
        }
    }
}
```
