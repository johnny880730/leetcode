# 0239. 滑动窗口的最大值

# 题目
给你一个整数数组 nums，有一个大小为 k 的滑动窗口从数组的最左侧移动到数组的最右侧。

你只可以看到在滑动窗口内的 k 个数字。滑动窗口每次只向右移动一位。返回滑动窗口中的最大值。

https://leetcode.cn/problems/sliding-window-maximum/description/

# 示例：
```
示例 1：

输入：nums = [1,3,-1,-3,5,3,6,7], k = 3
输出：[3,3,5,5,6,7]
解释：
滑动窗口的位置                最大值
---------------               -----
[1  3  -1] -3  5  3  6  7      3
1 [3  -1  -3] 5  3  6  7       3
1  3 [-1  -3  5] 3  6  7       5
1  3  -1 [-3  5  3] 6  7       5
1  3  -1  -3 [5  3  6] 7       6
1  3  -1  -3  5 [3  6  7]      7
```

```
示例 2：

输入：nums = [1], k = 1
输出：[1]
```


提示：
- 1 <= nums.length <= 100000
- -10000 <= nums[i] <= 10000
- 1 <= k <= nums.length

# 解析
本题的难点是如何求一个区间的最大值。暴力解法的时间复杂度为 O(n * k)。还有人可能会用一个大顶堆来存放这个窗口中的 k 个数字，
这样就可以知道最大值是多少了。但这个窗口是移动的，而大顶堆每次只能弹出最大值元素，无法移除其他元素，这样就造成大顶堆维护的不是滑动窗口的数值，
所以不能用大顶堆。

此时就需要一个队列，将窗口中的元素放入这个队列，随着窗口的移动，队列也一进一出，每次移动之后，告诉我们队列中的最大值是什么。

队列中的元素需要排序，而且将最大值放在出队口，否则就不知道哪个是最大值。如果把窗口中的元素都放入队列，那么窗口移动的时候，
队列需要弹出对应的元素。排序后的队列如何弹出窗口中要移除的元素（这个元素不一定是最大值）呢？

其实队列没有必要维护窗口中的所有元素，只需要维护有可能称为窗口中最大值的元素即可，同时保证队列的元素数值是由大到小排序的。

这个维护元素单调递减的队列就叫做单调队列，即单调递减或单调递增的队列。注意，不要以为实现单调队列就是对窗口中的元素进行排序，
如果是对元素进行排序，那么和优先级队列就没有区别了。

下面看下如何维护单调队列中的元素，对于窗口中的元素 [2, 3, 5, 1, 4]，只维护 [5, 4] 就够了，保持单调队列中的元素单调递减，
此时队列出口的元素就是窗口的最大元素。

![](./images/leetcode-0239-题解.png)

单调队列中的 [5, 4] 如何配合窗口维护呢？

设计单调队列的时候，pop 和 push 操作要保持如下原则：
1. pop()：如果窗口移除的元素等于单调队列的出口元素，那么队列弹出元素，否则不进行任何操作
2. push()：如果 push 的元素大于入口元素，那么就将队列入口的元素弹出，直到 push 的数值小于等于队列入口元素。

基于以上规则，每次窗口滑动的时候，只要取出口位置元素的值就是当前窗口的最大值。

为了更直观的感受到单调队列的工作过程，以题目示例为例，输入: nums = [1, 3, -1, -3, 5, 3, 6, 7], 和 k = 3，动画如下：

![](./images/leetcode-0239-演示.gif)

# 代码

### php
```php
class LeetCode0151 {

    function maxSlidingWindow($nums, $k) {
        $len = count($nums);
        $queue = new SplQueue();
        $res = [];
        for ($i = 0; $i < $len; $i++) {
            $cur = $nums[$i];
            while ($queue->isEmpty() == false && $cur > $queue->top()) {
                $queue->pop();          // top() 指的队尾元素
            }
            $queue->enqueue($cur);
            // 准备移动窗口
            if ($i >= $k) {
                $queue->bottom() != $nums[$i - $k] || $queue->dequeue();        // bottom() 指的队首元素
            }
            // 窗口OK
            if ($i >= $k - 1) {
                $res[] = $queue->bottom();
            }
        }
        return $res;
    }

}
```

### go
```go
func maxSlidingWindow(nums []int, k int) []int {
    queue := NewMyQueue()
    length := len(nums)
    res := make([]int, 0)
    // 先将前k个元素放入队列
    for i := 0; i < k; i++ {
        queue.Push(nums[i])
    }
    // 记录前k个元素的最大值
    res = append(res, queue.Front())
    
    for i := k; i < length; i++ {
        // 滑动窗口移除最前面的元素
        queue.Pop(nums[i-k])
        // 滑动窗口添加最后面的元素
        queue.Push(nums[i])
        // 记录最大值
        res = append(res, queue.Front())
    }
    return res
}

// 封装单调队列的方式解题
type MyQueue struct {
    queue []int
}

func NewMyQueue() *MyQueue {
    return &MyQueue{
        queue: make([]int, 0),
    }
}

func (m *MyQueue) Front() int {
    return m.queue[0]
}

func (m *MyQueue) Back() int {
    return m.queue[len(m.queue)-1]
}

func (m *MyQueue) Empty() bool {
    return len(m.queue) == 0
}

func (m *MyQueue) Push(val int) {
    for !m.Empty() && val > m.Back() {
        m.queue = m.queue[:len(m.queue)-1]
    }
    m.queue = append(m.queue, val)
}

func (m *MyQueue) Pop(val int) {
    if !m.Empty() && val == m.Front() {
        m.queue = m.queue[1:]
    }
}
```

### java
```java
class LeetCode0151 {

    public int[] maxSlidingWindow(int[] nums, int k) {
        int len = nums.length;
        Deque<Integer> queue = new ArrayDeque<>();
        int[] res = new int[len - k + 1];
        for (int i = 0; i < len; i++) {
            int cur = nums[i];
            while (!queue.isEmpty() && cur > queue.peekLast()) {
                queue.pollLast();
            }
            queue.offerLast(cur);
            if (i >= k) {
                if (queue.peekFirst() == nums[i - k]) {
                    queue.pollFirst();
                }
            }
            if (i >= k - 1) {
                res[i - k + 1] = queue.peekFirst();
            }
        }
        return res;
    }

}
```