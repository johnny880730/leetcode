# 204. 计数质数

# 题目
统计所有小于非负整数 n 的质数的数量。

https://leetcode.cn/problems/count-primes/

提示：
- 0 <= n <= 5 * 10^6

## 示例
```
输入：n = 10
输出：4
解释：小于 10 的质数一共有 4 个, 它们是 2, 3, 5, 7 。
```
```
输入：n = 0
输出：0
```
```
输入：n = 1
输出：0
```

# 解析

## 暴力枚举
如果 y 是 x 的因数，那么 x / y 也必然是 x 的因数，因此只要校验 y  或者 x / y 即可。如果每次选择校验两者中的较小数，不难发现，
较小数一定落在 [2, sqrt(x)] 之间，因此只需要校验 [2, sqrt(x)] 中的所有数即可。

个人 PS：这个方法在提交时候也会报超时

## 厄拉多塞筛法
初始化长度 O(n) 的标记数组，表示这个数组是否为质数。初始化所有的数都是质数。

从 2 开始将当前数字的倍数全都标记为合数。标记到 sqrt(n) 时停止即可。

注意每次找当前素数 x 的倍数时，是从 x ^ 2 开始的。因为如果 x > 2，那么 2 * x 肯定被素数 2 给过滤了，最小未被过滤的肯定是 x ^ 2 。

# 代码

```php

class LeetCode0204 {

    function countPrimes($n) {
        $res = 0;
        for ($i = 2; $i < $n; $i++) {
            $res += $this->_isPrime($i) ? 1 : 0;
        }
        return $res;
    }
    
    function _isPrime($x) {
        for ($i = 2; $i * $i <= $x; $i++) {
            if ($x % $i == 0) {
                return false;
            }
        }
        return true;
    }

    function countPrimes2($n) {
        $isPrime = array_fill(0, $n, true);
        // 从 2 开始枚举到 sqrt(n)
        for ($i = 2; $i * $i < $n; $i++) {
            if ($isPrime[$i]) {
                // 如果当前是素数 就把从 i*i 开始，i 的所有倍数都设置为 false。
                for ($j = $i * $i; $j < $n; $j += $i) {
                    $isPrime[$j] = false;
                }
            }
        }
        // 计数
        $res = 0;
        for ($i = 2; $i < $n; $i++) {
            if ($isPrime[$i]) {
                $res++;
            }
        }
        return $res;
    }
}
```

### go
```go
// 厄拉多塞筛法
func countPrimes(n int) int {
    isPrime := make([]bool, n)
    for i, _ := range isPrime {
        isPrime[i] = true
    }
    for i := 2; i * i < n; i++ {
        if isPrime[i] {
            for j := i * i; j < n; j += i {
                isPrime[j] = false
            }
        }
    }
    res := 0
    for i := 2; i < n; i++ {
        if isPrime[i] {
            res++
        }
    }
    return res
}
```
