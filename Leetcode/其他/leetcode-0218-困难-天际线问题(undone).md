# 218. 天际线问题

# 题目
城市的 天际线 是从远处观看该城市中所有建筑物形成的轮廓的外部轮廓。给你所有建筑物的位置和高度，请返回 由这些建筑物形成的 天际线 。

每个建筑物的几何信息由数组 buildings 表示，其中三元组 buildings[i] = [lefti, righti, heighti] 表示：
- lefti 是第 i 座建筑物左边缘的 x 坐标。
- righti 是第 i 座建筑物右边缘的 x 坐标。
- heighti 是第 i 座建筑物的高度。

你可以假设所有的建筑都是完美的长方形，在高度为 0 的绝对平坦的表面上。

天际线 应该表示为由 “关键点” 组成的列表，格式 [[x1,y1],[x2,y2],...] ，并按 x 坐标 进行 排序 。关键点是水平线段的左端点。
列表中最后一个点是最右侧建筑物的终点，y 坐标始终为 0 ，仅用于标记天际线的终点。此外，任何两个相邻建筑物之间的地面都应被视为天际线轮廓的一部分。

注意：输出天际线中不得有连续的相同高度的水平线。例如 [...[2 3], [4 5], [7 5], [11 5], [12 7]...] 是不正确的答案；
三条高度为 5 的线应该在最终输出中合并为一个：[...[2 3], [4 5], [12 7], ...]

https://leetcode.cn/problems/the-skyline-problem

提示：
- 1 <= buildings.length <= 10^4
- 0 <= lefti < righti <= 23^1 - 1
- 1 <= heighti <= 2^31 - 1
- buildings 按 lefti 非递减排序


# 示例
```
输入：buildings = [[2,9,10],[3,7,15],[5,12,12],[15,20,10],[19,24,8]]
输出：[[2,10],[3,15],[7,12],[12,0],[15,10],[20,8],[24,0]]
解释：
图 A 显示输入的所有建筑物的位置和高度，
图 B 显示由这些建筑物形成的天际线。图 B 中的红点表示输出列表中的关键点。
```
  
![](./images/leetcode-0218-示例.jpg)

# 解析
主要思想：X 轴从左到右时看最大高度的变化，轮廓线的产生就是最大高度的变化。

生成一个表，记录每个 X 位置的最大高度 height[X]，最后根据这个记录生成轮廓线。记录位置 X 的时候也只要记录大楼矗立的点即可，
这样就不用计算没有大楼的地方了，浪费资源。

那么如何记录最大高度的变化呢？比如有两幢楼 [3, 7, 4] 和 [4, 6, 6]。一个大楼生成两个对象,：
- 大楼 [3, 7, 4] 生成：
  - 对象1：left = 3，X = 3 这个点加了高度 4
  - 对象2：right = 7，X = 7 这个点减了高度 4
- 大楼 [4, 6, 6] 生成：
    - 对象1：left = 4，X = 4 这个点加了高度 6
    - 对象2：right = 6，X = 6 这个点减了高度 6

开始记录，假设记录数组为 arr。
- 位置 3 高度加了 4，arr = [4]
- 位置 4 高度加了 6，arr = [4, 6]
- 位置 6 高度减了 6，把 arr 里的 6 给抹去，arr = [3]
- 位置 7 高度减了 4，把 arr 里的 4 给抹去，arr = [] 

但实际操作的时候，除了要记录某个位置增加或者减去了某个高度之外，还要记录小高度的次数。
可以用一个 map 来记录，map 的键是变化的高度，值是次数。增加高度的时候 map[height]++，减去高度时候 map[height]--。

每个大楼生成两个对象后，一共有 2N 个对象。根据大楼的左边界排序，从左到右处理对象。

如果对同一位置有多次增加或减去高度的情况，要把这位置的都执行完最后看这个位置的高度。

还要警惕一种情况：宽度为 0 的 “纸片大楼”，如 [7, 7, 6]。对这样的大楼，规定表示增加高度的对象要在减去高度的对象前执行，这样 map 不会报错。
基于这一点原因，在给大楼生成的对象排序的时候，先根据左边界排序；左边界相同的情况下，增加高度的对象排在减去高度的对象的前面

也就是说，排序策略如下：
1. X 轴的位置从小到大
2. 如果 X 值相等，“增加” 的排在前，“减去” 的排在后
3. 如果上述两个都相等，无所谓前后顺序

# 代码
```java
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

// lintcode
public class BuildingOutline {

	// 描述高度变化的对象
	public static class Node {
		public int x; // x轴上的值
		public boolean isAdd;// true为加入，false为删除
		public int h; // 高度

		public Node(int x, boolean isAdd, int h) {
			this.x = x;
			this.isAdd = isAdd;
			this.h = h;
		}
	}

	// 排序的比较策略
	public static class NodeComparator implements Comparator<Node> {
		@Override
		public int compare(Node o1, Node o2) {
			if (o1.x != o2.x) {
				return o1.x - o2.x;
			}
			if (o1.isAdd != o2.isAdd) {
				return o1.isAdd ? -1 : 1;
			}
			return 0;
		}
	}

	// 全部流程的主方法
	// [s,e,h]
	// [s,e,h]
	// { {1,5,3} , {6,8,4}  .. ...  ...    }
	public static List<List<Integer>> buildingOutline(int[][] matrix) {
		int N = matrix.length;
		Node[] nodes = new Node[N << 1];
		for (int i = 0; i < matrix.length; i++) {
			nodes[i * 2] = new Node(matrix[i][0], true, matrix[i][2]);
			nodes[i * 2 + 1] = new Node(matrix[i][1], false, matrix[i][2]);
		}
		// 把描述高度变化的对象数组，按照规定的排序策略排序
		Arrays.sort(nodes, new NodeComparator());
		
		// TreeMap就是java中的红黑树结构，直接当作有序表来使用
		// key  某个高度  value  次数
		TreeMap<Integer, Integer> mapHeightTimes = new TreeMap<>();
		// key   x点，   value 最大高度
		TreeMap<Integer, Integer> mapXHeight = new TreeMap<>();
		
		for (int i = 0; i < nodes.length; i++) {
			// nodes[i]
			if (nodes[i].isAdd) { // 如果当前是加入操作
				if (!mapHeightTimes.containsKey(nodes[i].h)) { // 没有出现的高度直接新加记录
					mapHeightTimes.put(nodes[i].h, 1);
				} else { // 之前出现的高度，次数加1即可
					mapHeightTimes.put(nodes[i].h, mapHeightTimes.get(nodes[i].h) + 1);
				}
			} else { // 如果当前是删除操作
				if (mapHeightTimes.get(nodes[i].h) == 1) { // 如果当前的高度出现次数为1，直接删除记录
					mapHeightTimes.remove(nodes[i].h);
				} else { // 如果当前的高度出现次数大于1，次数减1即可
					mapHeightTimes.put(nodes[i].h, mapHeightTimes.get(nodes[i].h) - 1);
				}
			}
			// 根据mapHeightTimes中的最大高度，设置mapXvalueHeight表
			if (mapHeightTimes.isEmpty()) { // 如果mapHeightTimes为空，说明最大高度为0
				mapXHeight.put(nodes[i].x, 0);
			} else { // 如果mapHeightTimes不为空，通过mapHeightTimes.lastKey()取得最大高度
				mapXHeight.put(nodes[i].x, mapHeightTimes.lastKey());
			}
		}
		// ans为结果数组，每一个List<Integer>代表一个轮廓线，有开始位置，结束位置，高度，一共三个信息
		List<List<Integer>> ans = new ArrayList<>();
		// 一个新轮廓线的开始位置
		int start = 0;
		// 之前的最大高度
		int preHeight = 0;
		// 根据mapXvalueHeight生成ans数组
		for (Entry<Integer, Integer> entry : mapXHeight.entrySet()) {
		    // 当前位置
		    int curX = entry.getKey();
		    // 当前最大高度
		    int curMaxHeight = entry.getValue();
		    
		    if (ans.isEmpty() || ans.get(ans.size() - 1).get(1) != curMaxHeight) {
			    ans.add(new ArrayList<>(Arrays.asList(curX, curMaxHeight)));  
		    }
		}
		return ans;
	}

}

```