# 2520. 统计能整除数字的位数

# 题目
给你一个整数 num ，返回 num 中能整除 num 的数位的数目。

如果满足 nums % val == 0 ，则认为整数 val 可以整除 nums 。

提示：
- 1 <= num <= 10^9
- num 的数位中不含 0

# 示例
```
输入：num = 7
输出：1
解释：7 被自己整除，因此答案是 1 。
```
```
输入：num = 121
输出：2
解释：121 可以被 1 整除，但无法被 2 整除。由于 1 出现两次，所以返回 2 。
```
```
输入：num = 1248
输出：4
解释：1248 可以被它每一位上的数字整除，因此答案是 4 。
```

# 解析

根据题目要求，从低位到高位，依次判断除 nums 的余数是否为 0。统计所有余数为 0 的次数后返回


# 代码

### php
```php
class Leetcode2520 {

    function countDigits($num) {
        $res = 0;
        $n = $num;
        while ($n > 0) {
            $cur = $n % 10;
            if ($num % $cur == 0) {
                $res++;
            }
            $n = intval($n / 10);
        }
        
        return $res;
    }
}
```

### java
```java
public int countDigits(int num) {
    int res = 0;
    int n = num;
    while (n > 0) {
        int cur = n % 10;
        if (num % cur == 0) {
            res++;
        }
        n = n / 10;
    }
    return res;
}
```
