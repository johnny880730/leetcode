# 2525. 根据规则将箱子分类

# 题目
给你四个整数 length ，width ，height 和 mass ，分别表示一个箱子的三个维度和质量，请你返回一个表示箱子 类别 的字符串。

- 如果满足以下条件，那么箱子是 "Bulky" 的：
   - 箱子 至少有一个 维度大于等于 10^4 。
   - 或者箱子的 体积 大于等于 10^9 。
- 如果箱子的质量大于等于 100 ，那么箱子是 "Heavy" 的。
- 如果箱子同时是 "Bulky" 和 "Heavy" ，那么返回类别为 "Both" 。
- 如果箱子既不是 "Bulky" ，也不是 "Heavy" ，那么返回类别为 "Neither" 。
- 如果箱子是 "Bulky" 但不是 "Heavy" ，那么返回类别为 "Bulky" 。
- 如果箱子是 "Heavy" 但不是 "Bulky" ，那么返回类别为 "Heavy" 。

注意，箱子的体积等于箱子的长度、宽度和高度的乘积。

提示：
- 1 <= length, width, height <= 10^5
- 1 <= mass <= 1000

# 示例
```
输入：length = 1000, width = 35, height = 700, mass = 300
输出："Heavy"
解释：
箱子没有任何维度大于等于 104 。
体积为 24500000 <= 109 。所以不能归类为 "Bulky" 。
但是质量 >= 100 ，所以箱子是 "Heavy" 的。
由于箱子不是 "Bulky" 但是是 "Heavy" ，所以我们返回 "Heavy" 。
```
```
输入：length = 200, width = 50, height = 800, mass = 50
输出："Neither"
解释：
箱子没有任何维度大于等于 104 。
体积为 8 * 10^6 <= 10^9 。所以不能归类为 "Bulky" 。
质量小于 100 ，所以不能归类为 "Heavy" 。
由于不属于上述两者任何一类，所以我们返回 "Neither" 。
```

# 解析
没啥好说的，直接最简单的逻辑判断。

唯一需要注意的是：计算体积的时候可能会超过 int 的范围


# 代码

### php
```php
class Leetcode2525 {

    function categorizeBox($length, $width, $height, $mass) {
        $iMax = max($length, $width, $height);

        $isBulky = $iMax >= 10000 || $length * $width * $height >= 1000000000;
        $isHeavy = $mass >= 100;
        
        if ($isBulky && $isHeavy) {
            return 'Both';
        } else if (!$isBulky && !$isHeavy) {
            return 'Neither';
        } else if ($isBulky && !$isHeavy) {
            return 'Bulky';
        } else {
            return 'Heavy';
        }
    }
}
```

### java
```java
public String categorizeBox(int length, int width, int height, int mass) {
    long max = Math.max(length, Math.max(width, height));
    long volumn = 1L * length * width * height;

    boolean isBulky = max >= 10000 || volumn >= 1000000000;
    boolean isHeavy = mass >= 100;

    if (isBulky && isHeavy) {
        return "Both";
    } else if (!isBulky && !isHeavy) {
        return "Neither";
    } else if (isBulky && !isHeavy) {
        return "Bulky";
    } else {
        return "Heavy";
    }
}
```
