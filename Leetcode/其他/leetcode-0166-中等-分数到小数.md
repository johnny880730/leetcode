# 166. 分数到小数

# 题目
给定两个整数，分别表示分数的分子 numerator 和分母 denominator，以 字符串形式返回小数 。

如果小数部分为循环小数，则将循环的部分括在括号内。

如果存在多个答案，只需返回 任意一个 。

对于所有给定的输入，保证 答案字符串的长度小于 104 。

https://leetcode.cn/problems/fraction-to-recurring-decimal

提示：
- -2^31 <= numerator, denominator <= 2^31 - 1
- denominator != 0

# 示例
```
输入：numerator = 1, denominator = 2
输出："0.5"
```

```
输入：numerator = 2, denominator = 1
输出："2"
```

```
输入：numerator = 4, denominator = 333
输出："0.(012)"
```

# 解析



# 代码
```php
class LeetCode0166 {
 
     function fractionToDecimal($numerator, $denominator) {
        if ($numerator == 0) {
            return "0";
        }
        $res = (($numerator > 0) ^ ($denominator > 0) ? "-" : "");
        $num = abs($numerator);
        $den = abs($denominator);
        $res .= strval(floor($num / $den)); // 整数位置的字符
        $num %= $den;
        // 没有余数了直接返回
        if ($num == 0) {
            return $res;
        }
        $res .= ".";
        // 用一个map记录每个数出现在哪个位置，之后可以比对看是否重复了
        $map = [];
        $map[$num] = strlen($res);
        while ($num != 0) {
            $num *= 10;
            $res .= strval(floor($num / $den));
            $num %= $den;
            if (array_key_exists($num, $map)) {
                $index = $map[$num];
                $res = substr_replace($res, "(", $index, 0);
                $res .= ")";
                break;
            } else {
                $map[$num] = strlen($res);
            }
        }
        return $res;
     }
}

```