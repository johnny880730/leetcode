# 0146. LRU 缓存【中等】

# 题目
请你设计并实现一个满足 LRU (最近最少使用) 缓存 约束的数据结构。

实现 LRUCache 类：
- LRUCache(int capacity) 以 正整数 作为容量 capacity 初始化 LRU 缓存
- int get(int key) 如果关键字 key 存在于缓存中，则返回关键字的值，否则返回 -1 。
- void put(int key, int value) 如果关键字 key 已经存在，则变更其数据值 value ；如果不存在，则向缓存中插入该组 key-value 。如果插入操作导致关键字数量超过 capacity ，则应该 逐出 最久未使用的关键字。

函数 get 和 put 必须以 O(1) 的平均时间复杂度运行。

https://leetcode.cn/problems/lru-cache/description/

提示：
- 1 <= capacity <= 3,000
- 0 <= key <= 10,000
- 0 <= value <= 100,000
- 最多调用 2 * 100,000 次 get 和 put


# 示例
```
输入
["LRUCache", "put", "put", "get", "put", "get", "put", "get", "get", "get"]
[[2], [1, 1], [2, 2], [1], [3, 3], [2], [4, 4], [1], [3], [4]]
输出
[null, null, null, 1, null, -1, null, -1, 3, 4]

解释
LRUCache lRUCache = new LRUCache(2);
lRUCache.put(1, 1); // 缓存是 {1=1}
lRUCache.put(2, 2); // 缓存是 {1=1, 2=2}
lRUCache.get(1);    // 返回 1
lRUCache.put(3, 3); // 该操作会使得关键字 2 作废，缓存是 {1=1, 3=3}
lRUCache.get(2);    // 返回 -1 (未找到)
lRUCache.put(4, 4); // 该操作会使得关键字 1 作废，缓存是 {4=4, 3=3}
lRUCache.get(1);    // 返回 -1 (未找到)
lRUCache.get(3);    // 返回 3
lRUCache.get(4);    // 返回 4
```

# 解析

## 哈希表 + 双向链表

LRU 缓存机制可以通过哈希表辅以双向链表实现，用一个【哈希表】和一个【双向链表】维护所有在缓存中的键值对。
- 双向链表按照被使用的顺序存储了这些键值对，靠近尾部的键值对是最近使用的，而靠近头部的键值对是最久未使用的。
- 哈希表即为普通的哈希映射（HashMap），通过缓存数据的键映射到其在双向链表中的位置。

PS：头部代表最新还是尾部代表最新可以自己决定。这里是 头部 => 最旧，尾部 => 最新

具体方法如下：
- 对于 get 操作，首先判断 key 是否存在：
  - 如果 key 不存在，则返回 −1
  - 如果 key 存在，则 key 对应的节点是最近被使用的节点。通过哈希表定位到该节点在双向链表中的位置，并将其移动到双向链表的尾部，最后返回该节点的值。
- 对于 put 操作，首先判断 key 是否存在：
  - 如果 key 不存在，先判断双向链表的节点数是否超出容量，如果超出容量，则删除双向链表的头部节点，并删除哈希表中对应的项。
    然后使用 key 和 value 创建一个新的节点，在双向链表的尾部添加该节点，并将 key 和该节点添加进哈希表中。
  - 如果 key 存在，则与 get 操作类似，先通过哈希表定位，再将对应的节点的值更新为 value，并将该节点移到双向链表的尾部。

说明：

当新增数据的时候，哈希表的 key 是这个数据，生成一个节点 node，包含了这个数据的相关信息，哈希表的 val 是这个的内存地址。
将这个节点从尾部进入双向链表。

在这个解析的双链表里，哪个离头部更近，哪个就是时间点更早的东西，新的东西都是从尾部进入的。

查询的时候，首先通过哈希表来返回对应的节点来获得返回值。又因为能拿到节点的地址，不是通过遍历来找到它在链表中的位置，而是 O(1) 直接拿到这个节点，
将它从链表中抽离出来，放到尾部的最后，因为越靠近尾部说明该节点越晚操作。

如果超过新增数据的时候超过了容量，从双向链表的头部找到第一个节点，代表它是最早操作的，将它抽离，还要将它从哈希表中抹去记录。

# 代码
```php

class LRUCache0146 {

    private $cache;


    /**
     * @param Integer $capacity
     */
    function __construct($capacity) {
        $this->cache = new MyCache0146($capacity);
    }

    /**
     * @param Integer $key
     * @return Integer
     */
    function get($key) {
        $res = $this->cache->get($key);
        return $res === null ? -1 : $res;
    }

    /**
     * @param Integer $key
     * @param Integer $value
     * @return NULL
     */
    function put($key, $value) {
        $this->cache->set($key, $value);
    }
}

class Node0146 {
    public $key;
    public $value;
    public $last;
    public $next;

    public function __construct($key, $value) {
        $this->key = $key;
        $this->value = $value;
    }
}

class NodeDoubleLinkedList0146 {
    private $head;
    private $tail;

    function __construct() {
        $this->head = null;
        $this->tail = null;
    }
    
    // 增加节点
    function addNode($newNode) {
        if (!$newNode) {
            return;
        }
        if ($this->head == null) {      //双向链表一个节点也没有
            $this->head = $newNode;
            $this->tail = $newNode;
        } else {
            $this->tail->next = $newNode;   // 新节点挂在老尾巴的next
            $newNode->last = $this->tail;   // 新节点的last指向老尾巴
            $this->tail = $newNode;         // 把双端链表的tail更新成新节点，变成新尾巴
        }
    }

    // 移动节点到尾部
    function moveNodeToTail ($node) {
        // node 正好是尾部，不用操作
        if ($this->tail === $node) {
            return;
        }
        if ($this->head === $node) {
            // node 是头的情况
            $this->head = $node->next;      // head指针指向node的下一个
            $this->head->last = null;       // 新的head的last指针指向空
        } else {
            // node 是中间节点，前后节点跳过自己重连
            $node->last->next = $node->next;
            $node->next->last = $node->last;
        }
        $node->last = $this->tail;
        $node->next = null;
        $this->tail->next = $node;
        $this->tail = $node;
    }

    // 移除头节点并且返回
    function removeHead() {
         if ($this->head == null) {
             return null;
         }
         $res = $this->head;
         if ($this->head == $this->tail) {
             // 只有一个节点的时候，移除之后头和尾都要指向空
             $this->head = null;
             $this->tail = null;
         } else {
             $this->head = $res->next;
             $res->next = null;
             $this->head->last = null;
         }
         return $res;
    }
}

// node 存入map的时候，存的是一个内存地址。即使node中有很多文本也不会有很大影响
class MyCache0146 {
    private $keyNodeMap;
    private $nodeList;
    private $capacity;

    public function __construct($cap) {
        $this->keyNodeMap = array();
        $this->nodeList = new NodeDoubleLinkedList0146();
        $this->capacity = $cap;
    }

    public function get($key) {
        if (array_key_exists($key, $this->keyNodeMap)) {
            $node = $this->keyNodeMap[$key];
            // node 被操作过 要放到双向链表尾部
            $this->nodeList->moveNodeToTail($node);
            return $node->value;
        } 
        return null;
    }

    public function set($key, $value) {
        if (array_key_exists($key, $this->keyNodeMap)) {
            // 更新
            $node = $this->keyNodeMap[$key];
            $node->value = $value;
            $this->nodeList->moveNodeToTail($node);
        } else {
            // 新增
            if (count($this->keyNodeMap) == $this->capacity) {
                $this->removeMostUnusedCache();
            }
            $newNode = new Node0146($key, $value);
            $this->keyNodeMap[$key] = $newNode;
            $this->nodeList->addNode($newNode);
            
        }
    }

    private function removeMostUnusedCache() {
        $removeNode = $this->nodeList->removeHead();
        unset($this->keyNodeMap[$removeNode->key]);
    }
}
```

### java
```java
class LRUCache {

    private MyCache0146 cache;

    public LRUCache(int capacity) {
        this.cache = new MyCache0146(capacity);
    }

    public int get(int key) {
        Integer res = cache.get(key);
        return res == null ? -1 : res;
    }

    public void put(int key, int value) {
        cache.set(key, value);
    }
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.put(key,value);
 */

class Node0146 {
    public int key;
    public int value;
    public Node0146 last;
    public Node0146 next;

    public Node0146(int key, int value) {
        this.key = key;
        this.value = value;
    }
}

class DoubleLinkedList0146 {
    private Node0146 head;
    private Node0146 tail;

    public DoubleLinkedList0146() {
        this.head = null;
        this.tail = null;
    }

    // 增加节点
    public void addNode(Node0146 newNode) {
        if (newNode == null) {
            return;
        }
        if (head == null) {      //双向链表一个节点也没有
            head = newNode;
            tail = newNode;
        } else {
            tail.next = newNode;   // 新节点挂在老尾巴的next
            newNode.last = tail;   // 新节点的last指向老尾巴
            tail = newNode;         // 把双向链表的tail更新成新节点，变成新尾巴
        }
    }

    // 移动节点到尾部
    public void moveNodeToTail(Node0146 node) {
        // node 正好是尾部，不用操作
        if (tail == node) {
            return;
        }
        if (head == node) {
            // node 是头的情况
            head = node.next;      // head指针指向node的下一个
            head.last = null;       // 新的head的last指针指向空
        } else {
            // node 是中间节点，前后节点跳过自己重连
            node.last.next = node.next;
            node.next.last = node.last;
        }
        node.last = tail;
        node.next = null;
        tail.next = node;
        tail = node;
    }

    // 移除头节点并且返回
    public Node0146 removeHead() {
        if (head == null) {
            return null;
        }
        Node0146 res = head;
        if (head == tail) {
            // 只有一个节点的时候，移除之后头和尾都要指向空
            head = null;
            tail = null;
        } else {
            head = res.next;
            res.next = null;
            head.last = null;
        }
        return res;
    }
}

class MyCache0146 {
    private Map<Integer, Node0146> keyNodeMap;
    private DoubleLinkedList0146 nodeList;
    private int capacity;

    public MyCache0146(int cap) {
        this.keyNodeMap = new HashMap<>();
        this.nodeList = new DoubleLinkedList0146();
        this.capacity = cap;
    }

    public Integer get(int key) {
        if (keyNodeMap.containsKey(key)) {
            Node0146 node = keyNodeMap.get(key);
            // node 被操作过 要放到双向链表尾部
            nodeList.moveNodeToTail(node);
            return node.value;
        }
        return null;
    }

    public void set(int key, int value) {
        if (keyNodeMap.containsKey(key)) {
            // 更新
            Node0146 node = keyNodeMap.get(key);
            node.value = value;
            nodeList.moveNodeToTail(node);
        } else {
            // 新增
            if (keyNodeMap.size() == capacity) {
                removeMostUnusedCache();
            }
            Node0146 newNode = new Node0146(key, value);
            keyNodeMap.put(key, newNode);
            nodeList.addNode(newNode);
        }
    }

    private void removeMostUnusedCache() {
        Node0146 removeNode = nodeList.removeHead();
        keyNodeMap.remove(removeNode.key);
    }
}
```