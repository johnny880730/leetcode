# 763. 划分字母区间

# 题目
给你一个字符串 s 。我们要把这个字符串划分为尽可能多的片段，同一字母最多出现在一个片段中。

注意，划分结果需要满足：将所有划分结果按顺序连接，得到的字符串仍然是 s 。

返回一个表示每个字符串片段的长度的列表。

https://leetcode.cn/problems/partition-labels

提示：
- 1 <= s.length <= 500
- s 仅由小写英文字母组成

# 示例：
```
输入：s = "ababcbacadefegdehijhklij"
输出：[9,7,8]
解释：
划分结果为 "ababcbaca"、"defegde"、"hijhklij" 。
每个字母最多出现在一个片段中。
像 "ababcbacadefegde", "hijhklij" 这样的划分是错误的，因为划分的片段数较少。 
```

```
输入：s = "eccbbbbdec"
输出：[10]
```

# 解析

## 贪心
一想到分割字符串就想到了回溯，但本题其实不用回溯去暴力搜索。

题目要求同一字母最多出现在一个片段中，那么如何把同一个字母的都圈在同一个区间里呢？

如果没有接触过这种题目的话，还挺有难度的。

在遍历的过程中相当于是要找每一个字母的边界，如果找到之前遍历过的所有字母的最远边界，说明这个边界就是分割点了。
此时前面出现过所有字母，最远也就到这个边界了。

可以分为如下两步：
- 统计每一个字符最后出现的位置
- 从头遍历字符，并更新字符的最远出现下标，如果找到字符最远出现位置下标和当前下标相等了，则找到了分割点

![](./images/leetcode-0763-img1.png)


# 代码

### php
```php
class LeetCode0763 {

    function partitionLabels($s) {
        $map = [];              // i为字符，map[i]为字符出现的最后位置
        // 统计每一个字符最后出现的位置
        $size = strlen($s);
        for ($i = 0; $i < $size; $i++) {
            $map[$s[$i]] = $i;
        }
        $res = [];
        $left = $right = 0;
        for ($i = 0; $i < $size; $i++) {
            // 找到字符出现的最远边界
            $right = max($right, $map[$s[$i]]);
            
            if ($i == $right) {
                $res[] = $right - $left + 1;
                $left = $i + 1;
            }
        }
        return $res;
    }
}
```

### go
```go
func partitionLabels(s string) []int {
    size := len(s)
    hash := make([]int, 26)
    for i := 0; i < size; i++ {
        hash[s[i] - 'a'] = i
    }
    res := make([]int, 0)
    left, right := 0, 0
    for i := 0; i < size; i++ {
        right = max(hash[s[i] - 'a'], right)
        if i == right {
            res = append(res, right - left + 1)
            left = i + 1
        }
    }
    return res
}
    
func max(a, b int) int {
    if a > b {
        return a
    }
    return b
}
```

### java
```java
class LeetCode0763 {
    
    public List<Integer> partitionLabels(String s) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            map.put(s.charAt(i), i);
        }
        List<Integer> res = new ArrayList<>();
        int left = 0, right = 0;
        for (int i = 0; i < s.length(); i++) {
            right = Math.max(right, map.get(s.charAt(i)));
            if (i == right) {
                res.add(right - left + 1);
                left = i + 1;
            }
        }
        
        return res;
    }
}
```