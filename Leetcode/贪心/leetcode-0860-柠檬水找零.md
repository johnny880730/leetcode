# 860. 柠檬水找零

# 题目
在柠檬水摊上，每一杯柠檬水的售价为 5 美元。顾客排队购买你的产品，（按账单 bills 支付的顺序）一次购买一杯。

每位顾客只买一杯柠檬水，然后向你付 5 美元、10 美元或 20 美元。你必须给每个顾客正确找零，也就是说净交易是每位顾客向你支付 5 美元。

注意，一开始你手头没有任何零钱。

给你一个整数数组 bills ，其中 bills[i] 是第 i 位顾客付的账。如果你能给每位顾客正确找零，返回 true ，否则返回 false 。

https://leetcode.cn/problems/lemonade-change/

# 示例：
```
输入：bills = [5,5,5,10,20]
输出：true
解释：
前 3 位顾客那里，我们按顺序收取 3 张 5 美元的钞票。
第 4 位顾客那里，我们收取一张 10 美元的钞票，并返还 5 美元。
第 5 位顾客那里，我们找还一张 10 美元的钞票和一张 5 美元的钞票。
由于所有客户都得到了正确的找零，所以我们输出 true。
```

```
输入：bills = [5,5,10,10,20]
输出：false
解释：
前 2 位顾客那里，我们按顺序收取 2 张 5 美元的钞票。
对于接下来的 2 位顾客，我们收取一张 10 美元的钞票，然后返还 5 美元。
对于最后一位顾客，我们无法退回 15 美元，因为我们现在只有两张 10 美元的钞票。
由于不是每位顾客都得到了正确的找零，所以答案是 false。
```

```
输入：bills = [5,5,10]
输出：true
```

```
输入：bills = [10,10]
输出：false
```

提示：
- 1 <= bills.length <= 10^5
- bills[i] 不是 5 就是 10 或是 20

# 解析

## 贪心
只需要维护三个数值的金额，即 5、10、20。有如下三种情况：
1. 账单是 5，直接收下
2. 账单是 10，消耗一个 5，增加一个 10
3. 账单是 20，优先消耗一个 5 一个 10，如果不够那么消耗三个 5。

情况一和二是固定逻辑，唯一不确定的是情况三。

因为 10 元只能给 20 的找零，而 5 元可以给 10 元和 20 元都找零，所以 5 元更万能，所以遇到 20 元的情况优先用 10 元的找零。

局部最优：遇到 20 元，优先消耗 10 元的方案

全局最优：完成全部的找零

# 代码

### php
```php
class LeetCode0860 {

    function lemonadeChange($bills) {
        $five = $ten = $twenty = 0;
        foreach ($bills as $bill) {
            // 情况一
            if ($bill == 5) $five++;
            // 情况二
            if ($bill == 10) {
                if ($five <= 0) {
                    return false;
                }
                $ten++;
                $five--;
            }
            // 情况三
            if ($bill == 20) {
                // 优先消耗10元+5元的方案
                if ($five > 0 && $ten > 0) {
                    $five--;
                    $ten--;
                    $twenty++;  //这一行实际上没用，因为记录20没有意义，它也不用来找零
                } else if ($five >= 3) {
                    $five -= 3;
                    $twenty++;  //同理也可删除
                } else {
                    return false;
                }
            }
        }
        return true;
    }
}
```

### go
```go
func lemonadeChange(bills []int) bool {
	five, ten, twenty := 0, 0, 0
	for _, bill := range bills {
		if bill == 5 {
			five++
		}
		if bill == 10 {
			if five < 0 {
				return false
			}
			five--
			ten++
		}
		if bill == 20 {
			if ten > 0 && five > 0 {
				ten--
				five--
				twenty++
			} else if five >= 3 {
				five -= 3
				twenty++
			} else {
				return false
			}
		}
	}
	return true
}
```