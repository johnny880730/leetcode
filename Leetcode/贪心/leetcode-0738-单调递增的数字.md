# 738. 单调递增的数字

# 题目
给定一个非负整数 N，找出小于或等于 N 的最大的整数，同时这个整数需要满足其各个位数上的数字是单调递增。
（当且仅当每个相邻位数上的数字 x 和 y 满足 x <= y 时，我们称这个整数是单调递增的。）

https://leetcode.cn/problems/monotone-increasing-digits/

说明:
- 0 <= n <= 10^9

# 示例:
```
输入: N = 10
输出: 9
```

# 示例
```
输入: N = 1234
输出: 1234
```


```
输入: N = 332
输出: 299
```




# 解析

## 贪心
题目要求小于等于 N 的最大单调递增的整数，那么拿一个两位的数字来举例。

例如：98，一旦出现 strNum[i - 1] > strNum[i] 的情况（非单调递增），首先想让 strNum[i - 1]--，然后 strNum[i] 给为9，
这样这个整数就是 89，即小于 98 的最大的单调递增整数。

这一点如果想清楚了，这道题就好办了。

此时是从前向后遍历还是从后向前遍历呢？

从前向后遍历的话，遇到 strNum[i - 1] > strNum[i]的情况，让 strNum[i - 1]减一，但此时如果 strNum[i - 1] 减一了，可能又小于 strNum[i - 2]。
这么说有点抽象，举个例子，数字：332，从前向后遍历的话，那么就把变成了 329，此时 2 又小于了第一位的 3 了，真正的结果应该是 299。

那么从后向前遍历，就可以重复利用上次比较得出的结果了，从后向前遍历332的数值变化为：332 -> 329 -> 299

确定了遍历顺序之后，那么此时局部最优就可以推出全局，找不出反例，试试贪心。


# 代码

### php
```php
class LeetCode0738 {

    function monotoneIncreasingDigits($n) {
        // flag 用来标记赋值9从哪里开始
        // 设置为默认值，为了放置再flag没有被赋值的情况下执行第二个for循环
        $n = strval($n);
        $len = strlen($n);
        $flag = $len;
        for ($i = $len - 1; $i > 0; $i--) {
            if ($n[$i - 1] > $n[$i]) {
                $flag = $i;
                $n[$i - 1] = $n[$i - 1] - 1;
            }
        }
        for ($i = $flag; $i < $len; $i++) {
            $n[$i] = '9';
        }
        return intval($n);
    }
}
```

### go
```go
func monotoneIncreasingDigits(n int) int {
    ss := []byte(strconv.Itoa(n))
	size := len(ss)
	flag := size
	for i := size - 1; i > 0; i-- {
		if ss[i] < ss[i - 1] {
			flag = i
			ss[i - 1] -= 1
		}
	}
	for i := flag; i < size; i++ {
		ss[i] = '9'
	}
	res, _ := strconv.Atoi(string(ss))
	return res
}
```