# 45. 跳跃游戏 II

# 题目
给你一个非负整数数组nums ，你最初位于数组的第一个位置。

数组中的每个元素代表你在该位置可以跳跃的最大长度。

你的目标是使用最少的跳跃次数到达数组的最后一个位置。

假设你总是可以到达数组的最后一个位置。

https://leetcode.cn/problems/jump-game-ii

# 示例：
```
输入: nums = [2,3,1,1,4]
输出: 2
解释: 跳到最后一个位置的最小跳跃数是 2。
    从下标为 0 跳到下标为 1 的位置，跳1步，然后跳3步到达数组的最后一个位置。
```

```
输入: nums = [2,3,0,1,4]
输出: 2
```

提示：
- 1 <= nums.length <= 10^4
- 0 <= nums[i] <= 1000

# 解析

## 贪心
本题和 [《55. 跳跃游戏》](./leetcode-0055-中等-跳跃游戏.md) 相比难度增加了不少，但思路是相似的，还是要看最大覆盖范围。

要计算最小步数，那么就要想清楚什么时候步数一定要加一。

局部最优：在当前可移动距离固定的情况下，尽可能多走，如果还没到达终点，则步数加一。

整体最优：用最少步数到达终点。

真正解题的时候，要从覆盖范围出发，不管怎么调，在覆盖范围内一定是可以跳到的，以最小的步数增加覆盖范围。
一旦覆盖范围覆盖了终点，得到的就是最小步数。

这里需要统计两个覆盖范围，即当前这一步的最大覆盖范围和下一步的最大覆盖范围。

如果移动下标到达了当前这一步覆盖的最远距离，却还没有到达终点，那么就必须再走一步来增加覆盖范围，直到覆盖范围覆盖了终点。

![](./images/leetcode-0045-贪心.png)

图中覆盖范围的意义在于，只要是灰色的区域，最少两步一定可以到达，不用管具体怎么跳，反正一定可以跳到。

从图中可以看出，当移动下标达到了当前覆盖的最远距离下标时，步数就要加一来增加覆盖距离，最后的步数就是最少步数。

这里还有两种特殊情况需要考虑，当移动下标达到了当前覆盖的最远距离下标时：
- 如果当前覆盖的最远距离的下标不是终点，那么步数就加一，还需要继续移动
- 如果当前覆盖的最远距离的下标就是终点，那么步数不用加一，因为不需要再往后移动了

# 代码

### php
```php
class LeetCode0045 {

    public function jump($nums) {
        $len = count($nums);
        if ($len == 1) {
            return 0;
        }
        $res = 0;           // 记录走的最大步数
        $curCover = 0;      // 当前覆盖最远距离下标
        $nextCover = 0;     // 下一步覆盖最远距离下标
        for ($i = 0; $i < $len; $i++) {
            $nextCover = max($nextCover, $i + $nums[$i]);   // 更新下一步覆盖最远距离下标
            // 当前位置是覆盖范围的终点的时候
            if ($i == $curCover) {
                $res++;                         // 需要走一步
                $curCover = $nextCover;         // 更新当前覆盖最远距离下标（相当于加油了）
                if ($curCover >= $len - 1 ) {   // 当前覆盖最远距到达集合终点，不用做res++操作了，直接结束
                    break;
                }
            }
        }
        return $res;
    }

}
```

### go 
```go
func jump(nums []int) int {
    length := len(nums)
    if length <= 1 {
        return 0
    }
    res := 0
    curCover := 0
    nextCover := nums[0]
    for i := 0; i < length; i++ {
		if i + nums[i] > nextCover {
			nextCover = i + nums[i]
        }
		if i == curCover {
		    res++
			curCover = nextCover
			if curCover >= length - 1 {
			    break
            }
        }   
        
    }
    return res
}
```

### java
```java
class LeetCode0045 {

    public int jump(int[] nums) {
        if (nums.length == 1) {
            return 0;
        }
        int res = 0, curCover = 0, nextCover = 0;
        for (int i = 0; i <nums.length; i++) {
            nextCover = Math.max(nextCover, nums[i] + i);
            if (i == curCover) {
                res++;
                curCover = nextCover;
                if (curCover >= nums.length - 1) {
                    break;
                }
            }
        }
        return res;
    }
}
```
