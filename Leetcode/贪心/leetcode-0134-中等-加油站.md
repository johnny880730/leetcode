# 134. 加油站

# 题目
在一条环路上有 N 个加油站，其中第 i 个加油站有汽油 gas[i] 升。

你有一辆油箱容量无限的的汽车，从第 i 个加油站开往第 i + 1 个加油站需要消耗汽油 cost[i] 升。你从其中的一个加油站出发，开始时油箱为空。
如果你可以绕环路行驶一周，则返回出发时加油站的编号，否则返回 -1。

https://leetcode.cn/problems/gas-station/

说明:
- 如果题目有解，该答案即为唯一答案。
- 输入数组均为非空数组，且长度相同。
- 输入数组中的元素均为非负数。


# 示例:
```
输入:
gas  = [1,2,3,4,5]
cost = [3,4,5,1,2]
输出: 3
解释:
从 3 号加油站(索引为 3 处)出发，可获得 4 升汽油。此时油箱有 = 0 + 4 = 4 升汽油
开往 4 号加油站，此时油箱有 4 - 1 + 5 = 8 升汽油
开往 0 号加油站，此时油箱有 8 - 2 + 1 = 7 升汽油
开往 1 号加油站，此时油箱有 7 - 3 + 2 = 6 升汽油
开往 2 号加油站，此时油箱有 6 - 4 + 3 = 5 升汽油
开往 3 号加油站，你需要消耗 5 升汽油，正好足够你返回到 3 号加油站。
因此，3 可为起始索引。
```

```
输入:
gas  = [2,3,4]
cost = [3,4,3]
输出: -1
解释:
你不能从 0 号或 1 号加油站出发，因为没有足够的汽油可以让你行驶到下一个加油站。
我们从 2 号加油站出发，可以获得 4 升汽油。 此时油箱有 = 0 + 4 = 4 升汽油
开往 0 号加油站，此时油箱有 4 - 3 + 2 = 3 升汽油
开往 1 号加油站，此时油箱有 3 - 3 + 3 = 3 升汽油
你无法返回 2 号加油站，因为返程需要消耗 4 升汽油，但是你的油箱只有 3 升汽油。
因此，无论怎样，你都不可能绕环路行驶一周。
```

# 解析

## 贪心
每个加油站的剩余量 rest[i] = gas[i] - cost[i]。

首先如果总油量减去总消耗大于等于零那么一定可以跑完一圈，说明各个站点的加油站剩油量 rest[i] 相加一定是大于等于零的。

i 从 0 开始累加 rest[i]，和记为 curSum，一旦 curSum 小于零，说明 [0, i] 区间都不能作为起始位置，因为这个区间选择任何一个位置作为起点，
到 i 这里都会断油，那么起始位置从 i + 1 算起，再从零计算 curSum。

![](./images/leetcode-0134-img1.png)

那么为什么一旦 [0，i] 区间和为负数，起始位置就可以是 i+1 呢，i+1 后面就不会出现更大的负数？

如果出现更大的负数，就是更新 i，那么起始位置又变成新的 i+1了。

那有没有可能 [0，i] 区间选某一个作为起点，累加到 i 这里 curSum 是不会小于零呢？ 如图：

![](./images/leetcode-0134-img2.png)

如果 curSum < 0 说明 区间和1 + 区间和2 < 0， 那么假设从上图中的位置开始计数 curSum 不会小于0的话，就是区间和2 > 0。

区间和1 + 区间和2 < 0 同时区间和2 > 0，只能说明区间和1 < 0， 那么就会从假设的箭头初就开始从新选择其实位置了。

那么局部最优：当前累加 rest[i] 的和 curSum 一旦小于 0，起始位置至少要是 i + 1，因为从 i 之前开始一定不行。

全局最优：找到可以跑一圈的起始位置。

局部最优可以推出全局最优，找不出反例，试试贪心！



# 代码

### php
```php

class LeetCode0134 {
    
    function canCompleteCircuit($gas, $cost) {
        $curSum = 0;
        $totalSum = 0;
        $res = 0;
        $len = count($gas);
        for ($i = 0; $i < $len; $i++) {
            $curSum += $gas[$i] - $cost[$i];
            $totalSum += $gas[$i] - $cost[$i];
            if ($curSum < 0) {    // 当前累加rest[i]和 curSum一旦小于0
                $res = $i + 1;  // 起始位置更新为i+1
                $curSum = 0;      // curSum从0开始
            }
        }
        // 说明怎么走都不可能跑一圈了
        if ($totalSum < 0) 
            return -1; 
            
        return $res;
    }
}
```

### go
```go
func canCompleteCircuit(gas []int, cost []int) int {
    curSum := 0
    totalSum := 0
    res := 0
	length := len(gas)
    for i := 0; i < length; i++ {
        curSum += gas[i] - cost[i]
        totalSum += gas[i] - cost[i]
        if curSum < 0 {
            res = i + 1
            curSum = 0
        }
    }
    if totalSum < 0 {
        return -1	
    }
	
    return res
}
```

### java
```java
class LeetCode0134 {

    public int canCompleteCircuit(int[] gas, int[] cost) {
        int curSum = 0, totalSum = 0;
        int res = 0;
        for (int i = 0; i < gas.length; i++) {
            curSum += gas[i] - cost[i];
            totalSum += gas[i] - cost[i];
            if (curSum < 0) {
                res = i + 1;
                curSum = 0;
            }
        }
        if (totalSum < 0) {
            return -1;
        }
        return res;
    }
}
```
