# 55. 跳跃游戏

# 题目
给定一个非负整数数组 nums ，你最初位于数组的 第一个下标 。

数组中的每个元素代表你在该位置可以跳跃的最大长度。

判断你是否能够到达最后一个下标。

https://leetcode.cn/problems/jump-game


提示：
- 1 <= nums.length <= 3 * 10^4
- 0 <= nums[i] <= 10^5

# 示例：
```
输入：nums = [2,3,1,1,4]
输出：true
解释：可以先跳 1 步，从下标 0 到达下标 1, 然后再从下标 1 跳 3 步到达最后一个下标。
```

```
输入：nums = [3,2,1,0,4]
输出：false
解释：无论怎样，总会到达下标为 3 的位置。但该下标的最大跳跃长度是 0 ， 所以永远不可能到达最后一个下标。
```


# 解析

## 贪心
其实跳几步无所谓，关键在于可跳的覆盖范围。

不一定非要明确一次究竟跳几步，每次取最大的跳跃步数即可，即跳跃的覆盖范围。在这个范围内，无论怎么调，反正一定可以跳过来。

那么这个问题就转化为跳跃覆盖范围究竟可不可以覆盖到终点。

每次移动时取最大跳跃步数（得到最大覆盖范围），每移动一个单位，就更新最大覆盖范围。

局部最优：每次取最大跳跃步数（取最大覆盖范围）

全局最优：最后得到整体最大覆盖范围，看是否能到达终点


# 代码

### php
```php
class LeetCode0055 {

    public function canJump($nums) {
        if (!$nums || count($nums) < 2) {
            return true;
        }
        $cover = 0;     // 一开始只能覆盖第一个
        for ($i = 0; $i <= $cover; $i++) {      // 注意这里是小于等于cover
            $cover = max($i + $nums[$i], $cover);
            
            // 说明可以覆盖到终点了
            if ($cover >= count($nums) - 1) {
                return true;
            }
        }
        return false;
    }

}
```

### go
```go
func canJump(nums []int) bool {
    length := len(nums)
    if length < 2 {
        return true
    }
    cover := 0
    for i := 0; i <= cover; i++ {
        if i + nums[i] > cover {
            cover = i + nums[i]
        }
		if cover >= len(nums) - 1 {
			return true
        }  
    }
    return false
}
```

### java
```java
class LeetCode0055 {

    public boolean canJump(int[] nums) {
        if (nums.length < 2) {
            return true;
        }
        int cover = 0;
        for (int i = 0; i <= cover; i++) {
            cover = Math.max(i + nums[i], cover);
            if (cover >= nums.length - 1) {
                return true;
            }
        }
        return false;
    }
}
```

