# 435. 无重叠区间

# 题目
给定一个区间的集合 intervals ，其中 intervals[i] = [starti, endi] 。

返回 需要移除区间的最小数量，使剩余区间互不重叠 。

https://leetcode.cn/problems/non-overlapping-intervals

提示：
- 1 <= intervals.length <= 105
- intervals[i].length == 2
- -5 * 10^4 <= starti < endi <= 5 * 10^4

# 示例：
```
输入: intervals = [[1,2],[2,3],[3,4],[1,3]]
输出: 1
解释: 移除 [1,3] 后，剩下的区间没有重叠。
```

```
输入: intervals = [ [1,2], [1,2], [1,2] ]
输出: 2
解释: 你需要移除两个 [1,2] 来使剩下的区间没有重叠。
```

```
输入: intervals = [ [1,2], [2,3] ]
输出: 0
解释: 你不需要移除任何区间，因为它们已经是无重叠的了。
```


# 解析

## 贪心
此题和 《452. 用最少数量的箭引爆气球》 十分相像。

弓箭的数量就相当于是非交叉区间的数量，只要把弓箭那道题目代码里射爆气球的判断条件加个等号（认为 [0，1] [1，2] 不是相邻区间），
然后用总区间数减去弓箭数量就是要移除的区间数量了。

# 代码

### php
```php
class LeetCode0435 {

    public function eraseOverlapIntervals($intervals) {
        $len = count($intervals);
        if ($len <= 0) return 0;

        usort($intervals, function($i, $j){
            return $i[0] > $j[0];
        });
        
        $res = 1;
        for ($i = 1; $i < $len; $i++) {
            if ($intervals[$i][0] >= $intervals[$i - 1][1]) {
                $res++;
            } else {
                $intervals[$i][1] = min($intervals[$i][1], $intervals[$i - 1][1]);
            }
        }
        return $len - $res;
    }
}
```

### go
```go
func eraseOverlapIntervals(intervals [][]int) int {
	length := len(intervals)
	if length <= 0 {
		return 0
	}
	sort.Slice(intervals, func(i, j int) bool {
		return intervals[i][0] < intervals[j][0]
	})
	notCoverNum := 1
	for i := 1; i < length; i++ {
		if intervals[i][0] >= intervals[i - 1][1] {
			notCoverNum++
		} else {
			if intervals[i - 1][1] < intervals[i][1] {
				intervals[i][1] = intervals[i - 1][1]
			}
		}
	}
	return length - notCoverNum
}
```