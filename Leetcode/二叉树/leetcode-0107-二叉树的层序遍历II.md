### 107. 二叉树的层序遍历 II

# 题目
给你二叉树的根节点 root ，返回其节点值 自底向上的层序遍历 。 （即按从叶子节点所在层到根节点所在的层，逐层从左向右遍历）

https://leetcode.cn/problems/binary-tree-level-order-traversal-ii/

提示：
- 树中节点数目在范围 [0, 2000] 内
- -1000 <= Node.val <= 1000

# 示例
```
输入：root = [3,9,20,null,null,15,7]
输出：[[15,7],[9,20],[3]]
```

# 解析
&emsp;&emsp;在 [102. 二叉树的层序遍历](./二叉树/leetcode-0102-二叉树的层序遍历.md/) 的基础上结果反一下就行了


# 代码

```php
require './class/TreeNode.class.php';
$arr = [3,9,20,null,null,15,7];
$root = generateTreeByArray($arr);
(new LeetCode0107())->main($root);

class LeetCode0107
{
    // 用队列的 BFS
    function levelOrder($root)
    {
        $res = [];
        if ($root == null) {
            return $res;
        }

        $queue = new SplQueue();
        $queue->enqueue($root);

        $index = 0;
        while (!$queue->isEmpty()) {
            $size = $queue->count();
            for ($i = 0; $i < $size; $i++) {
                $node = $queue->dequeue();
                $res[$index][] = $node->val;
                if ($node->left) {
                    $queue->enqueue($node->left);
                }
                if ($node->right) {
                    $queue->enqueue($node->right);
                }
            }
            $index++;
        }
        return array_reverse($res);
    }

}
```