# 257. 二叉树的所有路径

# 题目
给定一个二叉树，返回所有从根节点到叶子节点的路径。

说明：叶子节点是指没有子节点的节点。

https://leetcode.cn/problems/binary-tree-paths

# 示例:
```
   1
 /   \
2     3
 \
  5

输出: ["1->2->5", "1->3"]

解释: 所有根节点到叶子节点的路径为: 1->2->5, 1->3
```

# 解析
本题是求从根节点到叶子节点的所有路径，所以需要使用前序遍历，这样才方便让父节点指向子节点，找到对应的路径。
这道题目涉及到回溯，因为要记录路径，需要回溯操作来回退一条路径从而进入另一条路径。

确定递归终止条件时候一般都习惯这么写：
```
if (cur == null) {
    终止处理逻辑
}
```

但这么写本题的终止条件会很麻烦，因为本题找到叶子节点后就要开始收集路径的处理逻辑了（也就是把路径放进结果数组）。
那么什么时候算是找到了叶子节点？当 cur 不为空且左右孩子都为空的时候，就找到了叶子节点。

使用 path 记录路径。因为是前序遍历，所以需要先处理中间节点，中间节点就是我们要记录的路径上的节点，先放进 path 中。
然后是递归和回溯的过程，那么在递归的时候，如果 cur 为空就不进行下一层的递归了，所以递归前要加上判断语句。

此时还需要做回溯的操作，因为 path 不能一直加入节点，当到达二叉树中一条边的尽头时，还要删除节点，然后才能加入新的节点。

# 代码

### php
```php
class LeetCode0257 {

    function binaryTreePaths($root) {
        $res = [];
        $path = [];
        if ($root == null) {
            return $res; 
        }
        $this->_traversal($root, $path, $res);
        return $res;
    }
    
    protected function _traversal($cur, &$path, &$res)
    {
        // 收集路径节点的值要写在这里，如果不写在这里，叶子节点的值会被pass
        $path[] = $cur->val;
                
        // 到了叶子节点。没判断 cur==null 因为递归前已经判断了
        if ($cur->left == null && $cur->right == null) {
            $sPath = '';
            for ($i = 0; $i < count($path) - 1; $i++) {
                $sPath .= strval($path[$i]);
                $sPath .= '->';
            }
            $sPath .= strval($path[count($path) - 1]);
            $res[] = $sPath;
        }
        if ($cur->left) {
            $this->_traversal($cur->left, $path, $res);
            array_pop($path);           // 回溯，将刚加入path的节点值给弹出去
        }
        if ($cur->right) {
            $this->_traversal($cur->right, $path, $res);
            array_pop($path);           // 回溯
        }
    }
}
```

### go
```go
func binaryTreePaths(root *TreeNode) []string {
    res := make([]string, 0)
    path := make([]string, 0)
    var travel func(node *TreeNode)

    travel = func(node *TreeNode) {
        path = append(path, strconv.Itoa(node.Val))

        if node.Left == nil && node.Right == nil {
            sPath := strings.Join(path, "->")
            res = append(res, sPath)
            return
        }
        if node.Left != nil {
            travel(node.Left)
            path = path[0:len(path) - 1]
        }
        if node.Right != nil {
            travel(node.Right)
            path = path[0:len(path) - 1]
        }
    }

    travel(root)
    return res
}
```