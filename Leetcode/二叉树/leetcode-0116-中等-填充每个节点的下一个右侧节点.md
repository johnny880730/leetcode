# 116. 填充每个节点的下一个右侧节点指针

# 题目
给定一个 完美二叉树 ，其所有叶子节点都在同一层，每个父节点都有两个子节点。二叉树定义如下：
```
struct Node {
    int val;
    Node *left;
    Node *right;
    Node *next;
}
```
填充它的每个 next 指针，让这个指针指向其下一个右侧节点。如果找不到下一个右侧节点，则将 next 指针设置为 NULL。

初始状态下，所有 next 指针都被设置为 NULL。

https://leetcode.cn/problems/populating-next-right-pointers-in-each-node

提示：
- 树中节点的数量在 [0, 2^12 - 1] 范围内
- -1000 <= node.val <= 1000
  
进阶：
- 你只能使用常量级额外空间。
- 使用递归解题也符合要求，本题中递归程序占用的栈空间不算做额外的空间复杂度。


# 示例
![](./images/leetcode-0116-实例.png)
```
输入：root = [1,2,3,4,5,6,7]
输出：[1,#,2,3,#,4,5,6,7,#]
解释：给定二叉树如图 A 所示，你的函数应该填充它的每个 next 指针，以指向其下一个右侧节点，
如图 B 所示。序列化的输出按层序遍历排列，同一层节点由 next 指针连接，'#' 标志着每一层的结束。
```

# 解析

## DFS
DFS 这棵树，从根节点 1 出发，向左递归到 2，再向左递归到 4。

这三个节点正好是每一层的第一个节点（类似链表头），用一个数组 pre 记录，即 pre[0] 为节点 1，pre[1] 为节点 2，pre[2] 为节点 4。
pre 的下标就是节点的深度。

继续递归到 5（深度为 2），从 pre[2] 中拿到节点 4，把 4 的 next 指向 555。然后更新 pre[2] 为节点 5，这样在后面递归到节点 7 时，
就可以从 pre[2] 中拿到节点 5，把 5 的 next 指向 7 了。

算法：
- 创建一个空数组 pre（因为一开始不知道二叉树有多深）。
- DFS 这棵二叉树，递归参数为当前节点 node，以及当前节点的深度 depth。每往下递归一层，就把 depth + 1。
- 如果 depth 等于 pre 数组的长度，说明 node 是这一层最左边的节点，把 node 添加到 pre 的末尾。
- 否则，把 pre[depth] 的 next 指向 node，然后更新 pre[depth] 为 node。
- 递归边界：如果 node 是空节点，直接返回。
- 递归入口：dfs(root, 0)。
- 最后返回 root。


# 代码

```php

class LeetCode0116 {

    // DFS
    public $pre = [];
    
    public function connect($root) {
        $this->_dfs($root, 0);
        return $root;
    }

    function _dfs($node, $depth) {
        if ($node == null) {
            return;
        }
        if ($depth == count($this->pre)) {
            $this->pre[] = $node;
        } else {
            $this->pre[$depth]->next = $node;
            $this->pre[$depth] = $node;
        }
        $this->_dfs($node->left, $depth + 1);
        $this->_dfs($node->right, $depth + 1);
    }
    
}
```

### java
```java
class LeetCode0116 {

    public List<Node> pre = new ArrayList<>();

    public Node connect(Node root) {
        dfs(root, 0);
        return root;
    }

    public void dfs(Node node, int depth) {
        if (node == null) {
            return;
        }
        if (depth == pre.size()) {
            pre.add(node);
        } else {
            pre.get(depth).next = node;
            pre.set(depth, node);
        }
        dfs(node.left, depth + 1);
        dfs(node.right, depth + 1);
    }
}
```
