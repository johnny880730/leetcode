# 0124. 二叉树中的最大路径和

# 题目
二叉树中的 路径 被定义为一条节点序列，序列中每对相邻节点之间都存在一条边。同一个节点在一条路径序列中 至多出现一次 。该路径 至少包含一个 节点，且不一定经过根节点。

路径和 是路径中各节点值的总和。

给你一个二叉树的根节点 root ，返回其 最大路径和 。

https://leetcode.cn/problems/binary-tree-maximum-path-sum/description/


提示：
- 树中节点数目范围是 [1, 3 * 10000]
- -1000 <= Node.val <= 1000

# 示例
```
输入：root = [1,2,3]
输出：6
解释：最优路径是 2 -> 1 -> 3 ，路径和为 2 + 1 + 3 = 6
```
```
输入：root = [-10,9,20,null,null,15,7]
输出：42
解释：最优路径是 15 -> 20 -> 7 ，路径和为 15 + 20 + 7 = 42
```


# 解析

## 递归

首先，考虑实现一个简化的函数 maxGain(node)，该函数计算二叉树中的一个节点的最大贡献值，具体而言，
就是在以该节点为根节点的子树中寻找以该节点为起点的一条路径，使得该路径上的节点值之和最大。

具体而言，该函数的计算如下；
- 空节点的最大贡献值等于 0。
- 非空节点的最大贡献值等于节点值与其子节点中的最大贡献值之和（对于叶节点而言，最大贡献值等于节点值）。

例如以下的二叉树：
```
    -10
   /   \
  9    20
      /   \
     15    7
```

叶节点 9、15、7 的最大贡献值分别为 9、15、7。

得到叶节点的最大贡献值之后，再计算非叶节点的最大贡献值。节点 20 的最大贡献值等于 20 + max(15, 7) = 35，节点 -10 的最大贡献值等于 
−10 + max(9, 35) = 25。

上述计算过程是递归的过程，因此，对根节点调用函数 maxGain，即可得到每个节点的最大贡献值。

根据函数 maxGain 得到每个节点的最大贡献值之后，如何得到二叉树的最大路径和？

对于二叉树中的一个节点，该节点的最大路径和取决于该节点的值与该节点的左右子节点的最大贡献值，如果子节点的最大贡献值为正，则计入该节点的最大路径和，
否则不计入该节点的最大路径和。维护一个全局变量 maxSum 存储最大路径和，在递归过程中更新 maxSum 的值，最后得到的 maxSum 的值即为二叉树中的最大路径和。

# 代码

### php

```php
class LeetCode0124 {

    private $res = PHP_INT_MIN;

    function maxPathSum($root) {
        $this->_maxGain($root);
        return $this->res;
    }

    protected function _maxGain($node) {
        if (!$node) {
            return 0;
        }
        // 递归计算左右子节点的maxGain
        $leftGain  = max(0, $this->_maxGain($node->left));
        $rightGain = max(0, $this->_maxGain($node->right));
        // 节点的最大路径和取决于该节点的值和左右子节点的 maxGain 值
        $sum = $node->val + $leftGain + $rightGain;
        // 更新答案
        $this->res = max($sum, $this->res);
        // 返回节点的 maxGain 值
        return $node->val + max($leftGain, $rightGain);
    }

}
```

### go
```go
func maxPathSum(root *TreeNode) int {
    var maxSum int = math.MinInt32
    var _maxGain func(node *TreeNode) int
    _maxGain = func(node *TreeNode) int {
        if node == nil {
            return 0
        }
        leftGain := max(_maxGain(node.Left), 0)
        rightGain := max(_maxGain(node.Right), 0)
        sum := node.Val + leftGain + rightGain
        maxSum = max(maxSum, sum)
        return node.Val + max(leftGain, rightGain)
    }
    _maxGain(root)
    return maxSum
}

func max(x, y int) int {
    if x > y {
        return x
    }
    return y
}
```

### java
```java
class LeetCode0124 {

    public int maxPathSum(TreeNode root) {
        int[] maxSum = new int[]{Integer.MIN_VALUE};
        _maxGain(root, maxSum);
        return maxSum[0];
    }

    private int _maxGain(TreeNode node, int[] maxSum) {
        if (node == null) {
            return 0;
        }
        int leftGain = Math.max(_maxGain(node.left, maxSum), 0);
        int rightGain = Math.max(_maxGain(node.right, maxSum), 0);
        int sum = node.val + leftGain + rightGain;
        maxSum[0] = Math.max(maxSum[0], sum);
        
        return node.val + Math.max(leftGain, rightGain);
    }
}
```