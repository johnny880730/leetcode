# 0117. 填充每个节点的下一个右侧节点指针 II


# 题目
给定一个二叉树：

```
struct Node {
    int val;
    Node *left;
    Node *right;
    Node *next;
}
```
填充它的每个 next 指针，让这个指针指向其下一个右侧节点。如果找不到下一个右侧节点，则将 next 指针设置为 NULL 。

初始状态下，所有 next 指针都被设置为 NULL 。

https://leetcode.cn/problems/populating-next-right-pointers-in-each-node-ii/description/

提示：
- 树中的节点数在范围 [0, 6000] 内
- -100 <= Node.val <= 100

# 示例
```
输入：root = [1,2,3,4,5,null,7]
输出：[1,#,2,3,#,4,5,7,#]
解释：给定二叉树如图 A 所示，你的函数应该填充它的每个 next 指针，以指向其下一个右侧节点，如图 B 所示。序列化输出按层序遍历顺序（由 next 指针连接），'#' 表示每层的末尾。
```
```
输入：root = []
输出：[]
```

# 解析

参考：[《116. 填充每个节点的下一个右侧节点指针》](./leetcode-0116-中等-填充每个节点的下一个右侧节点.md)。

和 116 的区别就是 116 是完美二叉树，117 的不是。但是不影响 116 的解题思路，把 116 的思路用在 117 上，一个字都不用改。

## DFS
DFS 这棵树，从根节点 1 出发，向左递归到 2，再向左递归到 4。

这三个节点正好是每一层的第一个节点（类似链表头），用一个数组 pre 记录，即 pre[0] 为节点 1，pre[1] 为节点 2，pre[2] 为节点 4。
pre 的下标就是节点的深度。

继续递归到 5（深度为 2），从 pre[2] 中拿到节点 4，把 4 的 next 指向 5。然后更新 pre[2] 为节点 5，这样在后面递归到节点 7 时，
就可以从 pre[2] 中拿到节点 5，把 5 的 next 指向 7 了。

算法：
- 创建一个空数组 pre（因为一开始不知道二叉树有多深）。
- DFS 这棵二叉树，递归参数为当前节点 node，以及当前节点的深度 depth。每往下递归一层，就把 depth + 1。
- 如果 depth 等于 pre 数组的长度，说明 node 是这一层最左边的节点，把 node 添加到 pre 的末尾。
- 否则，把 pre[depth] 的 next 指向 node，然后更新 pre[depth] 为 node。
- 递归边界：如果 node 是空节点，直接返回。
- 递归入口：dfs(root, 0)。
- 最后返回 root。

PS：代码百分百拷贝自 0116 题，也能通过。

# 代码

### php
```php
class Leetcode0117 {

    // DFS
    public $pre = [];
    
    public function connect($root) {
        $this->_dfs($root, 0);
        return $root;
    }

    function _dfs($node, $depth) {
        if ($node == null) {
            return;
        }
        if ($depth == count($this->pre)) {
            $this->pre[] = $node;
        } else {
            $this->pre[$depth]->next = $node;
            $this->pre[$depth] = $node;
        }
        $this->_dfs($node->left, $depth + 1);
        $this->_dfs($node->right, $depth + 1);
    }
}
```

### java
```java
class LeetCode0117 {

    public List<Node> pre = new ArrayList<>();

    public Node connect(Node root) {
        dfs(root, 0);
        return root;
    }

    public void dfs(Node node, int depth) {
        if (node == null) {
            return;
        }
        if (depth == pre.size()) {
            pre.add(node);
        } else {
            pre.get(depth).next = node;
            pre.set(depth, node);
        }
        dfs(node.left, depth + 1);
        dfs(node.right, depth + 1);
    }
}
```

### go
```go
func connect(root *Node) *Node {
    var pre []*Node
    _dfs(root, 0, &pre)
    return root
}

func _dfs(node *Node, depth int, pre *[]*Node) {
    if node == nil {
        return
    }
    if depth == len(*pre) {
        *pre = append(*pre, node)
    } else {
        (*pre)[depth].Next = node
        (*pre)[depth] = node
    }
    _dfs(node.Left, depth + 1, pre)
    _dfs(node.Right, depth + 1, pre)
}
```
