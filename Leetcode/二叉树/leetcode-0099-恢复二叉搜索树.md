### 99. 恢复二叉搜索树

# 题目
给你二叉搜索树的根节点 root ，该树中的 恰好 两个节点的值被错误地交换。请在不改变其结构的情况下，恢复这棵树 。

提示：
- 树上节点的数目在范围 [2, 1000] 内
- -2^31 <= Node.val <= 2^31 - 1

# 示例
```
输入：root = [1,3,null,null,2]
输出：[3,1,null,null,2]
解释：3 不能是 1 的左孩子，因为 3 > 1 。交换 1 和 3 使二叉搜索树有效。
```
```
输入：root = [3,1,4,null,null,2]
输出：[2,1,4,null,null,3]
解释：2 不能在 3 的右子树中，因为 2 < 3 。交换 2 和 3 使二叉搜索树有效。
```


# 解析

##### 显式中序遍历

&emsp;&emsp;需要考虑两个节点被错误地交换后对原二叉搜索树造成了什么影响。对于二叉搜索树，如果对其进行中序遍历，得到的值序列是递增有序的，
而如果错误地交换了两个节点，等价于在这个值序列中交换了两个值，破坏了值序列的递增性。

&emsp;&emsp;来看下如果在一个递增的序列中交换两个值会造成什么影响。假设有一个递增序列 a=[1,2,3,4,5,6,7]。如果交换两个不相邻的数字，例如 2 和 6，
原序列变成了 a=[1,6,3,4,5,2,7]，那么显然序列中有两个位置不满足递增，在这个序列中体现为 6>3，5>2，因此只要找到这两个位置，
即可找到被错误交换的两个节点。如果交换两个相邻的数字，例如 2 和 3，此时交换后的序列只有一个位置不满足递增。
因此整个值序列中不满足条件的位置或者有两个，或者有一个。

&emsp;&emsp;因此解题思路如下：
- 找到二叉搜索树中序遍历得到值序列的不满足条件的位置。
- 如果有两个，我们记为 i 和 j（i<j 且 a[i]>a[i+1] && a[j]>a[j+1]），那么对应被错误交换的节点即为 a[i] 对应的节点和 a[j+1] 对应的节点，
  分别记为 x 和 y。
- 如果有一个，记为 i，那么对应被错误交换的节点即为 a[i] 对应的节点和 a[i+1] 对应的节点，我们分别记为 x 和 y。
- 交换 x 和 y 两个节点即可

&emsp;&emsp;实现部分，开辟一个新数组 nums 来记录中序遍历得到的值序列，然后线性遍历找到两个位置 i 和 j，并重新遍历原二叉搜索树修改对应节点的值完成修复，

# 代码

```php
require_once './class/TreeNode.class.php';
$arr = [3,1,4,null,null,2];
$root = generateTreeByArray($arr);
(new LeetCode1046())->main($root);

class LeetCode0099
{
    public function main($root) {
        var_dump($this->recoverTree($root));
    }

    function recoverTree($root) {
        $nums = [];
        $this->_inorder($root, $nums);
        $swapped = $this->_find2Swapped($nums);
        $this->_recover($root, 2, $swapped[0], $swapped[1]);
        return $root;
    }

    protected function _inorder($node, &$nums) {
        if (!$node) return null;
        $this->_inorder($node->left, $nums);
        $nums[] = $node->val;
        $this->_inorder($node->right, $nums);
    }

    protected function _find2Swapped($nums) {
        $n = count($nums);
        $index1 = $index2 = -1;
        for ($i = 0; $i < $n - 1; $i++) {
            if ($nums[$i + 1] < $nums[$i]) {
                $index2 = $i + 1;
                if ($index1 == -1) {
                    $index1 = $i;
                } else {
                    break;
                }
            }
        }
        $x = $nums[$index1];
        $y = $nums[$index2];
        return array($x, $y);
    }

    protected function _recover($root, $count, $x, $y) {
        if ($root) {
            if ($root->val == $x || $root->val == $y) {
                $root->val = $root->val == $x ? $y : $x;
                if (--$count == 0) {
                    return;
                }
            }
            $this->_recover($root->right, $count, $x, $y);
            $this->_recover($root->left, $count, $x, $y);
        }
    }

}
```