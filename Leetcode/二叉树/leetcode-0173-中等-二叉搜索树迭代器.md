# 0173. 二叉搜索树迭代器

# 题目
实现一个二叉搜索树迭代器类 BSTIterator ，表示一个按中序遍历二叉搜索树（BST）的迭代器：
- BSTIterator(TreeNode root)：初始化 BSTIterator 类的一个对象。BST 的根节点 root 会作为构造函数的一部分给出。
  指针应初始化为一个不存在于 BST 中的数字，且该数字小于 BST 中的任何元素。
- boolean hasNext()： 如果向指针右侧遍历存在数字，则返回 true ；否则返回 false 。
- int next()：将指针向右移动，然后返回指针处的数字。

注意，指针初始化为一个不存在于 BST 中的数字，所以对 next() 的首次调用将返回 BST 中的最小元素。

你可以假设 next() 调用总是有效的，也就是说，当调用 next() 时，BST 的中序遍历中至少存在一个下一个数字。

https://leetcode.cn/problems/binary-search-tree-iterator/description/

提示：
- 树中节点的数目在范围 [1, 10000] 内
- 0 <= Node.val <= 1000000
- 最多调用 100000 次 hasNext 和 next 操作


# 示例
```
    7
   / \
  3  15
    /  \
   9   20    

输入
["BSTIterator", "next", "next", "hasNext", "next", "hasNext", "next", "hasNext", "next", "hasNext"]
[[[7, 3, 15, null, null, 9, 20]], [], [], [], [], [], [], [], [], []]
输出
[null, 3, 7, true, 9, true, 15, true, 20, false]

解释
BSTIterator bSTIterator = new BSTIterator([7, 3, 15, null, null, 9, 20]);
bSTIterator.next();    // 返回 3
bSTIterator.next();    // 返回 7
bSTIterator.hasNext(); // 返回 True
bSTIterator.next();    // 返回 9
bSTIterator.hasNext(); // 返回 True
bSTIterator.next();    // 返回 15
bSTIterator.hasNext(); // 返回 True
bSTIterator.next();    // 返回 20
bSTIterator.hasNext(); // 返回 False
```


# 解析

根据二叉搜索树的性质，不难发现，原问题等价于对二叉搜索树进行中序遍历。因此，可以采取与「94. 二叉树的中序遍历」类似的方法来解决这一问题。

可以直接对二叉搜索树做一次完全的递归遍历，获取中序遍历的全部结果并保存在数组中。随后，利用得到的数组本身来实现迭代器。

# 代码

### php

```php
class BSTIterator {
    
    private $idx = 0;
    private $arr = [];
    
    /**
     * @param TreeNode $root
     */
    function __construct($root) {
        $this->_inorder($root);
    }
    
    private function _inorder($node) {
        if (!$node) {
            return null;
        }
        $this->_inorder($node->left);
        $this->arr[] = $node->val;
        $this->_inorder($node->right);
    }

    /**
     * @return Integer
     */
    function next() {
        return $this->arr[$this->idx++];
    }

    /**
     * @return Boolean
     */
    function hasNext() {
        return $this->idx < count($this->arr);
    }
}
```