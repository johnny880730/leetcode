# 687. 最长同值路径

# 题目
给定一个二叉树的 root ，返回 最长的路径的长度 ，这个路径中的 每个节点具有相同值 。 这条路径可以经过也可以不经过根节点。

两个节点之间的路径长度 由它们之间的边数表示。

https://leetcode.cn/problems/longest-univalue-path

提示：
- 树的节点数的范围是 [0, 10^4]
- -1000 <= Node.val <= 1000
- 树的深度将不超过 1000

# 示例
```
        5
       /  \
      4    5
     / \    \
    1  1     5      
输入：root = [5,4,5,1,1,5]
输出：2
```

```
        1
       /  \
      4    5
     / \    \
    4  4     5    
输入：root = [1,4,5,4,4,5]
输出：2
```

# 解析

如何求以 X 为根节点的最长同值路径：
1. 如果与 X 无关，就是不路过 X 节点
   - 情况1：左树上最长路径
   - 情况2：右数上最长路径
2. 如果与 X 有关，
   - 情况3：只有 X 自己，路径就是 1
   - 情况4：如果左子树与 X 值相等，就看左子树为出发点的最大同值路径，往左扎下去
   - 情况5：如果右子树与 X 值相等，就看右子树为出发点的最大同值路径，往右扎下去
   - 情况6：既往左扎，又往右扎
   
这些情况的最大值就是所求结果
    

# 代码

```php
class LeetCode0687 {
    
    function longestUnivaluePath($root) {
        if ($root == null) {
            return 0;
        }
        return $this->_process($root)->max - 1;
    }
    
    protected function _process($x) {
        if (!$x) {
            return new Info0687(0, 0);
        }
        $left  = $x->left;
        $right = $x->right;
        $leftInfo  = $this->_process($left);    //左树的信息
        $rightInfo = $this->_process($right);   //右树的信息
        
        $len = 1;       //最起码路径有X节点自己，长度是1
        # 以X为出发点的情况
        // 看看往左扎下去
        if ($left != null && $left->val == $x->val) {
            $len = $leftInfo->len + 1;
        }
        // 看看往右扎下去
        if ($right != null && $right->val == $x->val) {
            $len = max($len, $rightInfo->len + 1);
        }
        # 不以X为出发点的情况
        // 先计算 [
        //  左树的最大距离 => leftInfo->max,
        //  右树的最大距离 => rightInfo->max,
        //  X自己、X只往左扎、X只往右扎 =>len
        // ]
        // 这五种情况的最大值
        $max = max($leftInfo->max, $rightInfo->max, $len);
        // 再计算 既能往左扎、又能往右扎的距离，和上面的比较取较大值
        if ($left != null && $right != null && $left->val == $x->val && $right->val == $x->val) {
            $max = max($max, $leftInfo->len + $rightInfo->len + 1);
        }
        return new Info0687($len, $max);
    }
    
}

// 假设以 X 节点为头的树，返回两个信息
class Info0687 {
    public $len = 0;        //路径必须以 X 为出发点，合法路径的最大距离
    public $max = 0;        //不要求路径以 X 为出发点，合法路径的最大距离
    
    function __construct($l, $m) {
        $this->len = $l;
        $this->max = $m;
    }
}
```