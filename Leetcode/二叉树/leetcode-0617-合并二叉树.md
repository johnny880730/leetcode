# 617. 合并二叉树

# 题目
给定两个二叉树，想象当你将它们中的一个覆盖到另一个上时，两个二叉树的一些节点便会重叠。

你需要将他们合并为一个新的二叉树。合并的规则是如果两个节点重叠，那么将他们的值相加作为节点合并后的新值，否则不为 NULL 的节点将直接作为新二叉树的节点。

https://leetcode.cn/problems/merge-two-binary-trees/

# 示例:
```
输入:
	Tree 1                     Tree 2
	
          1                         2
         / \                       / \
        3   2                     1   3
       /                           \   \
      5                             4   7
      
输出合并后的树:

	     3
	    / \
	   4   5
	  / \   \
	 5   4   7
	 
注意: 合并必须从两个树的根节点开始。
```

# 解析
针对二叉树使用递归法，就要考虑用哪种遍历方式。这里使用哪种都是可以的。这里用前序。

递归的终止条件：因为要传入两棵树，所以判断两棵树遍历的节点 t1 和 t2，如果 t1 为空，那么两个节点合并后应该为 t2（如果 t2 也为空，
那么合并之后就是 null）。如果 t2 为空同理。

确定单层递归的逻辑：这里可以重复利用 t1 这棵树，t1 就是合并之后树的根节点（修改了原来的树结构）。在单层递归中，就是把两棵树的元素加在一起。
所以 t1 的左子树就是合并 t1 左子树和 t2 左子树。t1 的右子树就是合并 t1 的右子树和 t2 的右子树。最终 t1 就是合并后的根节点。

# 代码

### php
```php
class LeetCode0617 {
    
    function mergeTrees($node1, $node2) {
        if ($node1 == null && $node2 == null) {
            return null;
        }
        if ($node1 == null) return $node2;
        if ($node2 == null) return $node1;
        // 修改了 node1 的数值和结构
        $node1->val += $node2->val;
        $node1->left = $this->mergeTrees($node1->left, $node2->left);
        $node1->right = $this->mergeTrees($node1->right, $node2->right);
        return $node1;
    }

}
```

### go
```go
func mergeTrees(root1 *TreeNode, root2 *TreeNode) *TreeNode {
	if root1 == nil && root2 == nil {
		return nil
	}
	if root1 == nil {
		return root2
	}
	if root2 == nil {
		return root1
	}
	root1.Val += root2.Val
	root1.Left = mergeTrees(root1.Left, root2.Left)
	root1.Right = mergeTrees(root1.Right, root2.Right)

	return root1
}
```
