# 297. 二叉树的序列化与反序列化

# 题目
序列化是将一个数据结构或者对象转换为连续的比特位的操作，进而可以将转换后的数据存储在一个文件或者内存中，同时也可以通过网络传输到另一个计算机环境，
采取相反方式重构得到原数据。

请设计一个算法来实现二叉树的序列化与反序列化。这里不限定你的序列 / 反序列化算法执行逻辑，
你只需要保证一个二叉树可以被序列化为一个字符串并且将这个字符串反序列化为原始的树结构。

https://leetcode.cn/problems/serialize-and-deserialize-binary-tree

提示：
- 树中结点数在范围 [0, 10^4] 内
- -1000 <= Node.val <= 1000

# 示例
```
输入：root = [1,2,3,null,null,4,5]
输出：[1,2,3,null,null,4,5]
```

# 解析

## 通过前序遍历实现

序列化：
- 初始化序列化结果的字符串 str=""
- 前序遍历二叉树，如果遇到空节点，就在 str 的末尾加上 “#!”，“#” 表示这个节点为空，“!” 表示一个值的结束
- 如果遇到不为空的节点，假设 val=3，就在 str 的末尾加上“3!”
- 前序遍历序列化的过程参考下面的 serialByPre 方法

反序列化，例如 “12!3!#!#!#!”，参考 reconByPreString 方法

---

## 通过层遍历实现

序列化
- 利用队列结构实现层遍历
- 代码参考下面的 serialByLevel 方法

反序列化
- 重做层遍历，遇到"#"就生成空节点，同时不把空节点放到队列里即可
- 代码参考下面的 reconByLevelString 方法

# 代码
```php
class LeetCode0297 {

    public function main($root) {
        // 方法一：通过前序遍历实现序列化和反序列化
        $serialByPre = $this->serialByPre($root) . PHP_EOL;
        $newRoot = $this->reconByPreString($serialByPre);
        echo $serialByPre . PHP_EOL;
        var_dump($newRoot);

        // 方法二：通过层遍历实现序列化和反序列化
        $serialByLevel = $this->serialByLevel($root) . PHP_EOL;
        $newRoot2 = $this->reconByLevelString($serialByLevel);
        echo $serialByLevel . PHP_EOL;
        var_dump($newRoot2);
    }

    // 通过前序遍历的序列化
    public function serialByPre($root) {
        if ($root == null) {
            return '#!';
        }
        $res = $root->val . '!';
        $res .= $this->serialByPre($root->left);
        $res .= $this->serialByPre($root->right);
        return $res;
    }

    // 前序遍历的反序列化
    public function reconByPreString($str) {
        $values = explode('!', $str);
        $queue = new SplQueue();
        for ($i = 0; $i != count($values); $i++) {
            $queue->enqueue($values[$i]);
        }
        return $this->reconPreOrder($queue);
    }

    protected function reconPreOrder(SplQueue $queue) {
        $value = $queue->dequeue();
        if ($value == '#') {
            return null;
        }
        $node = new TreeNode($value);
        $node->left = $this->reconPreOrder($queue);
        $node->right = $this->reconPreOrder($queue);
        return $node;
    }

    // 通过层遍历的序列化
    public function serialByLevel($root) {
        if ($root == null) {
            return '#!';
        }
        $res = $root->val . '!';
        $queue = new SplQueue();
        $queue->enqueue($root);
        while ($queue->isEmpty() == false) {
            $cur = $queue->dequeue();
            if ($cur->left) {
                $res .= $cur->left->val . '!';
                $queue->enqueue($cur->left);
            } else {
                $res .= '#!';
            }
            if ($cur->right) {
                $res .= $cur->right->val . '!';
                $queue->enqueue($cur->right);
            } else {
                $res .= '#!';
            }
        }
        return $res;
    }

    // 层遍历的反序列化
    public function reconByLevelString($str) {
        $values = explode('!', $str);
        $index = 0;
        $root = $this->generateNodeByString($values[$index++]);
        $queue = new SplQueue();
        if ($root != null) {
            $queue->enqueue($root);
        }
        while ($queue->isEmpty() == false) {
            $node = $queue->dequeue();
            $node->left = $this->generateNodeByString($values[$index++]);
            $node->right = $this->generateNodeByString($values[$index++]);
            if ($node->left != null) {
                $queue->enqueue($node->left);
            }
            if ($node->right != null) {
                $queue->enqueue($node->right);
            }
        }
        return $root;
    }

    protected function generateNodeByString($val) {
        if ($val == '#') {
            return null;
        }
        return new TreeNode($val);
    }
}
```