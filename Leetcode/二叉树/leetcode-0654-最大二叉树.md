# 654. 最大二叉树

# 题目
给定一个不重复的整数数组 nums 。 最大二叉树 可以用下面的算法从 nums 递归地构建:
1. 创建一个根节点，其值为 nums 中的最大值。
2. 递归地在最大值 左边 的 子数组前缀上 构建左子树。
3. 递归地在最大值 右边 的 子数组后缀上 构建右子树。

返回 nums 构建的 最大二叉树 。

https://leetcode.cn/problems/maximum-binary-tree

提示：
- 1 <= nums.length <= 1000
- 0 <= nums[i] <= 1000
- nums 中的所有整数 互不相同

# 示例
```
                6
         /             \       
        3                5
         \              /
          2            0
           \
            1

输入：nums = [3,2,1,6,0,5]
输出：[6,3,5,null,2,0,null,null,1]
解释：递归调用如下所示：
- [3,2,1,6,0,5] 中的最大值是 6 ，左边部分是 [3,2,1] ，右边部分是 [0,5] 。
    - [3,2,1] 中的最大值是 3 ，左边部分是 [] ，右边部分是 [2,1] 。
        - 空数组，无子节点。
        - [2,1] 中的最大值是 2 ，左边部分是 [] ，右边部分是 [1] 。
            - 空数组，无子节点。
            - 只有一个元素，所以子节点是一个值为 1 的节点。
    - [0,5] 中的最大值是 5 ，左边部分是 [0] ，右边部分是 [] 。
        - 只有一个元素，所以子节点是一个值为 0 的节点。
        - 空数组，无子节点。
```

# 解析
根据题目给出的例子，输入的数组为 [3, 2, 1, 6, 0, 5]，对于整棵树的根节点来说，其实在做这件事情：

```java
TreeNode constructMaximumBinaryTree([3, 2, 1, 6, 0, 5]) {
    // 找到数组中的最大值
    TreeNode root = new TreeNode(6);
    
    // 递归调用构造左右子树
    root.left = constructMaximumBinaryTree([3, 2, 1]);
    root.right = constructMaximumBinaryTree([0, 5]);
    
    return root;
}
```

再详细一点，就是如下的伪代码

```java
TreeNode constructMaximumBinaryTree(int[] nums) {
    if (nums is empty) {
        return null;
    }
    // 找到数组中的最大值
    int maxVal = Integer.MIN_VALUE;
    int index = -1;
    for (int i = 0; i < nums.length; i++) {
        if (nums[i] > maxVal) {
            maxVal = nums[i];
            index = i;
        }
    }
    
    TreeNode root = new TreeNode(maxVal);
    
    // 递归调用构造左右子树
    root.left = constructMaximumBinaryTree(nums[0 .. index - 1]);
    root.right = constructMaximumBinaryTree(nums[index + 1 .. nums.length - 1]);
    
    return root;
}
```

当前 nums 中的最大值就是根节点，然后根据索引递归调用左右数组构造左右子树即可。

明确了思路，可以写一个辅助函数 build，来控制 nums 的索引。


# 代码

### java
```java
// 主函数
TreeNode constructMaximumBinaryTree(int[] nums) {
    return build(nums, 0, nums.length - 1);
}

// 定义：将 nums[lo .. hi] 构造成符合条件的树，返回根节点
TreeNode build(int[] nums, int lo, int hi) {
    // base case
    if (lo > hi) {
        return null;
    }
    
    // 找到数组中的最大值和对应索引
    int maxVal = Integer.MIN_VALUE;
    int index = -1;
    for (int i = lo; i <= hi; i++) {
        if (nums[i] > maxVal) {
            maxVal = nums[i];
            index = i;
        }
    }
    
    // 构造根节点
    TreeNode root = new TreeNode(maxVal);
    
    // 递归调用构造左右子树
    root.left = build(nums, lo, index - 1);
    root.right = build(nums, index + 1, hi);
    
    return root;
}
```
