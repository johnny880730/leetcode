# 0226. 翻转二叉树【简单】

# 题目
给你一棵二叉树的根节点 root ，翻转这棵二叉树，并返回其根节点。

https://leetcode.cn/problems/invert-binary-tree/description/

提示：
- 树中节点数目范围在 [0, 100] 内
- -100 <= Node.val <= 100

# 示例
```
示例 1：

输入：

     4
   /   \
  2     7
 / \   / \
1   3 6   9
输出：

     4
   /   \
  7     2
 / \   / \
9   6 3   1
```

```
示例 2：

输入：root = [2,1,3]
输出：[2,3,1]
```

```
示例 3：

输入：root = []
输出：[]
```

# 解析

## 分解问题的思维模式
尝试对 invertTree 函数赋予一个定义：将以 root 为根的二叉树翻转，返回翻转后的二叉树的根节点

对于某一个节点 x 执行 invertTree(x)，能利用这个递归函数的定义做点啥呢？

可以用 invertTree(x.left) 先把 x 的左子树翻转，再用 invertTree(x.right) 把 x 的右子树翻转，最后把左右子树交换，
这恰好完成了以 x 为根的整棵二叉树的翻转，即完成了 invertTree(x) 的定义。

# 代码

### php
```php
class LeetCode0226 {

    function invertTree($root) {
        if ($root == null) {
            return null;
        }
        // 利用函数定义，先翻转左右子树
        $left = $this->invertTree2($root->left);
        $right = $this->invertTree2($root->right);
        
        // 交换左右子节点
        $root->left = $right;
        $root->right = $left;
        
        return $root;
    }
}
```

### go
```go
func invertTree(root *TreeNode) *TreeNode {
	if root == nil {
		return nil
	}
    left := invertTree(root.Left)
    right := invertTree(root.Right)

    root.Left = right
    root.Right = left

    return root
}
```

### java
```java
class LeetCode0226 {

    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        TreeNode left = invertTree(root.left);
        TreeNode right = invertTree(root.right);

        root.left = right;
        root.right = left;

        return root;
    }
    
}
```
