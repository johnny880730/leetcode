# 0530. 二叉搜索树的最小绝对差

# 题目
给你一棵所有节点为非负值的二叉搜索树，请你计算树中任意两节点的差的绝对值的最小值。

https://leetcode.cn/problems/minimum-absolute-difference-in-bst/description/


提示：
- 树中节点的数目范围是 [2, 10000]
- 0 <= Node.val <= 100000

# 示例
```
输入：

   1
    \
     3
    /
   2

输出：1
解释：最小绝对差为 1，其中 2 和 1 的差的绝对值为 1（或者 2 和 3）。
```

# 解析

## 中序遍历
二叉搜索树是有序的。

利用二叉搜索树的中序遍历，定义指针 pre 指向前一个节点，比较当前节点和 pre 节点的差值即可

# 代码

### php
```php
class LeetCode0530 {

    function getMinimumDifference($root) {
        $res = PHP_INT_MAX;
        $pre = null;
        $this->_dfs($root, $pre, $res);
        return $res;
    }

    function _dfs($node, &$pre, &$res) {
        if (!$node) {
            return;
        }
        $this->_dfs($node->left, $pre, $res);

        // 中序位置
        if ($pre === null) {
            $pre = $node;
        } else {
            $res = min($res, $node->val - $pre->val);
            $pre = $node;
        }

        $this->_dfs($node->right, $pre, $res);
    }
}
```

### go
```go
func getMinimumDifference(root *TreeNode) int {
    res := math.MaxInt64
    var pre *TreeNode
    var _dfs func(node *TreeNode)
        _dfs = func(node *TreeNode) {
        if node == nil {
            return
        }
        _dfs(node.Left)
        
        if pre != nil {
            diff := node.Val - pre.Val
            if diff < res {
                res = diff
            }
        }
        pre = node
        
        _dfs(node.Right)
    }
    
    _dfs(root)
    return res
}
```

### java
```java
class LeetCode0530 {

    TreeNode pre;
    int res;

    public int getMinimumDifference(TreeNode root) {
        res = Integer.MAX_VALUE;
        _dfs(root);
        return res;
    }

    private void _dfs(TreeNode node) {
        if (node == null) {
            return;
        }
        _dfs(node.left);
        if (pre == null) {
            pre = node;
        } else {
            res = Math.min(res, node.val - pre.val);
            pre = node;
        }

        _dfs(node.right);
    }
}
```
