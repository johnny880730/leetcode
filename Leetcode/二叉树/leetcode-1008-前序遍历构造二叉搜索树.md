### 1008. 前序遍历构造二叉搜索树

# 题目
给定一个整数数组，它表示 BST (即 二叉搜索树 )的 先序遍历 ，构造树并返回其根。

提示：
- 1 <= preorder.length <= 100
- 1 <= preorder[i] <= 10^8
- preorder 中的值 互不相同

# 示例
```
输入：preorder = [8,5,1,7,10,12]
输出：[8,5,10,1,7,null,12]
```
```
输入: preorder = [1,3]
输出: [1,null,3]
```


# 解析

&emsp;&emsp;二叉搜索树的中序序列是个递增的有序序列，所以可以对数组排序得到中序遍历的结果，然后问题就变成了根据前序和中序构造二叉树的问题。
详情参考 [105. 从前序与中序遍历序列构造二叉树](./二叉树/leetcode-0105-从前序与中序遍历序列构造二叉树.md/)


# 代码
```php
require './class/TreeNode.class.php';
$preorder = [8,5,1,7,10,12];
(new LeetCode1008())->main($preorder);

class LeetCode1008
{
    public function main($preorder)
    {
        var_dump($this->bstFromPreorder($preorder));
    }

    public function bstFromPreorder($pre)
    {
        $len = count($pre);
        $in = $pre;
        sort($in);

        return $this->_traversal($pre, 0, $len, $in, 0, $len);
    }

    protected function _traversal($preorder, $preBegin, $preEnd, $inorder, $inBegin, $inEnd)
    {
        if ($preBegin == $preEnd) {
            return null;
        }
        $rootValue = $preorder[$preBegin];      // 注意用 preBegin，不要用 0
        $root = new TreeNode($rootValue);

        if ($preEnd - $preBegin == 1) {
            return $root;
        }

        // 切割点
        $delimiterIndex =  array_search($rootValue, $inorder);

        // 切割中序数组 左闭右开
        $leftInBegin = $inBegin;
        $leftInEnd = $delimiterIndex;
        $rightInBegin = $delimiterIndex + 1;
        $rightInEnd = $inEnd;

        // 切割前序数组 左闭右开
        $leftPreBegin = $preBegin + 1;
        $leftPreEnd = $preBegin + 1 + $delimiterIndex - $inBegin;       // 终止位置是起始位置加上中序左区间的大小size
        $rightPreBegin = $preBegin + 1 + $delimiterIndex - $inBegin;
        $rightPreEnd = $preEnd;

        // 递归
        $root->left = $this->_traversal($preorder, $leftPreBegin, $leftPreEnd, $inorder, $leftInBegin, $leftInEnd);
        $root->right = $this->_traversal($preorder, $rightPreBegin, $rightPreEnd, $inorder, $rightInBegin, $rightInEnd);

        return $root;
    }

}
```