# 0106. 从中序与后序遍历序列构造二叉树

# 题目
根据一棵树的中序遍历与后序遍历构造二叉树。

注意：你可以假设树中没有重复的元素。

https://leetcode.cn/problems/construct-binary-tree-from-inorder-and-postorder-traversal/description/

提示:
- 1 <= inorder.length <= 3000
- postorder.length == inorder.length
- -3000 <= inorder[i], postorder[i] <= 3000
- inorder 和 postorder 都由 不同 的值组成
- postorder 中每一个值都在 inorder 中
- inorder 保证是树的中序遍历
- postorder 保证是树的后序遍历

# 示例
```
中序遍历 inorder = [9,3,15,20,7]
后序遍历 postorder = [9,15,7,20,3]
返回如下的二叉树：

    3
   / \
  9  20
    /  \
   15   7
```

# 解析
可以参考 [0105. 从前序与中序遍历序列构造二叉树](./leetcode-0105-中等-从前序与中序遍历序列构造二叉树.md)

先看后序和中序遍历的特点：
- 后序遍历和前序遍历相反，根节点对应的值是 preOrder 的最后一个元素

整体的算法框架和 《0105. 从前序与中序遍历序列构造二叉树》非常类似，这里也画出取索引位置的图

![](./images/leetcode-0106-img1.png)



# 代码

### php

```php
class LeetCode0106 {

    public $map;

    function buildTree($inorder, $postorder) {
        foreach ($inorder as $k => $v) {
            $this->map[$v] = $k;
        }
        return $this->_build($inorder, 0, count($inorder) - 1, $postorder, 0, count($postorder) - 1);
    }

    protected function _build($inorder, $inStart, $inEnd, $postorder, $postStart, $postEnd) {
        if ($inStart > $inEnd) {
            return null;
        }
        // 后序数组的最后一个元素，即根节点的值
        $rootVal = $postorder[$postEnd];
        // rootVal 在中序数组的索引
        $index = $this->map[$rootVal];
        
        // 左子树的节点个数
        $leftSize = $index - $inStart;
        
        $root = new TreeNode($rootVal);

        // 递归构造左右子树
        $root->left  = $this->_build($inorder, $inStart, $index - 1, $postorder, $postStart, $postStart + $leftSize - 1);
        $root->right = $this->_build($inorder, $index + 1, $inEnd, $postorder, $postStart + $leftSize, $postEnd - 1);

        return $root;
    }
}
```

### go
```go
func buildTree(inorder []int, postorder []int) *TreeNode {
    if len(inorder) == 0 {
        return nil
    }

    idxMap := make(map[int]int)
    for i, val := range inorder {
        idxMap[val] = i
    }

    return _build(inorder, 0, len(inorder)-1, postorder, 0, len(postorder)-1, idxMap)
}

func _build(inorder []int, inStart int, inEnd int, postorder []int, postStart int, postEnd int, idxMap map[int]int) *TreeNode {
    if inStart > inEnd {
        return nil
    }

    rootVal := postorder[postEnd]
    index := idxMap[rootVal]
    leftSize := index - inStart

    root := &TreeNode{Val: rootVal}
    root.Left = _build(inorder, inStart, index-1, postorder, postStart, postStart+leftSize-1, idxMap)
    root.Right = _build(inorder, index+1, inEnd, postorder, postStart+leftSize, postEnd-1, idxMap)

    return root
}
```

### java
```java
class LeetCode0105 {

    private Map<Integer, Integer> indexMap;

    public TreeNode buildTree(int[] inorder, int[] postorder) {
        // 构造哈希映射，帮助我们快速定位根节点
        indexMap = new HashMap<Integer, Integer>();
        for (int i = 0; i < inorder.length; ++i) {
            indexMap.put(inorder[i], i);
        }

        return _build(inorder, 0, inorder.length - 1, postorder, 0, inorder.length - 1);
    }

    private TreeNode _build(int[] inorder, int inStart, int inEnd,
                                    int[] postorder, int postStart, int postEnd) {
        if (inStart > inEnd) {
            return null;
        }

        // 后序遍历数组的最后一个元素就是当前的根节点
        int rootVal = postorder[postEnd];
        TreeNode root = new TreeNode(rootVal);

        // 根节点索引
        int index = indexMap.get(rootVal);

        // 左子树的节点数目
        int leftSize = index - inStart;

        // 递归构造左右子树
        root.left = _build(inorder, inStart, index - 1, postorder, postStart, postStart + leftSize - 1);
        root.right = _build(inorder, index + 1, inEnd, postorder, postStart + leftSize, postEnd - 1);

        return root;
    }
}
```