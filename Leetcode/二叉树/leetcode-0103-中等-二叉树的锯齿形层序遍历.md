# 103. 二叉树的锯齿形层序遍历

# 题目
给你二叉树的根节点 root ，返回其节点值的 锯齿形层序遍历 。（即先从左往右，再从右往左进行下一层遍历，以此类推，层与层之间交替进行）。

https://leetcode.cn/problems/binary-tree-zigzag-level-order-traversal/

提示：
- 树中节点数目在范围 [0, 2000] 内
- -100 <= Node.val <= 100

# 示例
```
输入：root = [3,9,20,null,null,15,7]
输出：[[3],[20,9],[15,7]]
```

# 解析

与 [「102. 二叉树的层序遍历」](./leetcode-0102-二叉树的层序遍历.md) 解法类似，只是需要记录当前的层深，根据层深的奇偶性，
当前层数组插入的顺序不同。

还可以用双端队列来实现，详见代码及注释。用变量来控制节点从头部进入还是从尾部进入
- 如果节点从头部进入，弹出的时候，子节点要按 **从左往右** 的顺序从队列的 **尾部** 进入队列
- 如果节点从尾部进入，弹出的时候，子节点要按 **从右往左** 的顺序从队列的 **头部** 进入队列

# 代码

### php
```php

class LeetCode0103 {

    function zigzagLevelOrder($root) {
        $res = [];
        if (!$root) {
            return $res;
        }
        $queue = new SplQueue();
        $queue->enqueue($root);;
        $left2Right = true ;
        while ($count = $queue->count()) {
            $currentLevel = [];
            for ($i = 0; $i < $count; $i++) {
                $node = $queue->dequeue();
                $currentLevel[] = $node->val;
                if ($node->left) {
                    $queue->enqueue($node->left);
                }
                if ($node->right) {
                    $queue->enqueue($node->right);
                }
            }
            if (!$left2Right) {
                $l = 0;
                $r = count($currentLevel) - 1;
                while ($l < $r) {
                    $tmp = $currentLevel[$l];
                    $currentLevel[$l] = $currentLevel[$r];
                    $currentLevel[$r] = $tmp;
                    $l++;
                    $r--;
                }
            }
            $res[] = $currentLevel;
            $left2Right = !$left2Right;
        }
        return $res;
    }
    
    // 用双端队列实现
    function zigzagLevelOrder2($root) {
        $res = [];
        if (!$root) {
            return $res;
        }
        $queue = new SplDoublyLinkedList();
        // 从头部放入第一个元素
        $queue->unshift($root);
        $isHead = true; 
        while ($queue->isEmpty() == false) {
            $size = $queue->count();
            $curLevel = [];
            if ($isHead) {
                // 从头进入的，从头弹出，子节点按从左往右的顺序从尾进入
                for ($i = 0; $i < $size; $i++) {
                    $node = $queue->shift();
                    $curLevel[] = $node->val;
                    if ($node->left) {
                        $queue->push($node->left);
                    }
                    if ($node->right) {
                        $queue->push($node->right);
                    }
                }
            } else {
                // 从尾进入的，从尾弹出，子节点按从右往左的顺序从头进入
                for ($i = 0; $i < $size; $i++) {
                    $node = $queue->pop();
                    $curLevel[] = $node->val;
                    if ($node->right) {
                        $queue->unshift($node->right);
                    }
                    if ($node->left) {
                        $queue->unshift($node->left);
                    }
                }
            }
            $res[] = $curLevel;
            $isHead = !$isHead;
        }
        return $res;
    }

}
```

### java
```java
class LeetCode0103 {

    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        boolean left2right = true;
        while (!queue.isEmpty()) {
            int size = queue.size();
            List<Integer> curLevel = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                TreeNode cur = queue.poll();
                curLevel.add(cur.val);
                if (cur.left != null) {
                    queue.offer(cur.left);
                }
                if (cur.right != null) {
                    queue.offer(cur.right);
                }
            }
            if (left2right == false) {
                int l = 0, r = curLevel.size() - 1;
                while (l < r) {
                    int tmp = curLevel.get(l);
                    curLevel.set(l, curLevel.get(r));
                    curLevel.set(r, tmp);
                    l++;
                    r--;
                }
            }
            res.add(curLevel);
            left2right = !left2right;
        }
        return res;
    }
}
```

### go
```go
func zigzagLevelOrder(root *TreeNode) [][]int {
    res := [][]int{}
    if root == nil {
        return res
    }
    q := []*TreeNode{root}
    left2Right := true
    for len(q) > 0 {
        level := []int{}
        count := len(q)
        for i := 0; i < count; i++ {
            cur := q[0]
            q = q[1:]
            level = append(level, cur.Val)
            if cur.Left != nil {
                q = append(q, cur.Left)
            }
            if cur.Right != nil {
                q = append(q, cur.Right)
            }
        }
        if left2Right == false {
            for l, r := 0, len(level) - 1; l < r; {
                level[l], level[r] = level[r], level[l]
                l++
                r--
            }
        }
        left2Right = !left2Right
        res = append(res, level)
    }
    
    return res
}
```
