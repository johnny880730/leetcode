# 0108. 将有序数组转换为二叉搜索树

# 题目
将一个按照升序排列的有序数组，转换为一棵高度平衡二叉搜索树。

本题中，一个高度平衡二叉树是指一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过 1。

https://leetcode.cn/problems/convert-sorted-array-to-binary-search-tree/description/

# 示例:
```
给定有序数组: [-10,-3,0,5,9],

一个可能的答案是：[0,-3,9,-10,null,5]，它可以表示下面这个高度平衡二叉搜索树：

      0
     / \
   -3   9
   /   /
 -10  5
```

# 解析
根据数组构造一个二叉树，本质上就是寻找分割点，将分割点作为当前节点，然后递归处理左区间和右区间。

基于有序数组构造 BST，寻找分割点就比较容易了。分割点就是数组中间位置的节点。如果数组的长度为偶数，中间节点有两个，
那么取哪一个作为分割点呢？取哪个都可以，只不过会构成不同的 平衡二叉搜索树。所以这道题目的答案不是唯一的。

确定递归函数的参数和返回值：参数是传入的数组、左下标 left 和右下标 right。构造二叉树的时候尽量不要重新定义左右区间数组，
而是用下标来操作原数组。这里定义的左闭右闭区间，在不断分割的过程中，也会坚持左闭右闭的原则。

确定递归终止条件：由于是左闭右闭的区间，所以当左边界大于右边界的时候，该区间对应的节点是空节点。

确定单层递归的逻辑：首先去数组中间元素的位置，以中间位置的元素构造节点。接着划分区间，下一层左区间的构造节点赋值给 root 的左孩子，
下一层右区间构造的节点赋值给 root 的右孩子，最后返回 root 节点。


# 代码

### php
```php
class LeetCode0108 {

    function sortedArrayToBST($nums) {
        return $this->_traversal($nums, 0, count($nums) - 1);
    }

    protected function _traversal($nums, $left, $right) {
        if ($left > $right) {
            return null;
        }
        $mid = $left + (($right - $left) >> 1);
        $root = new TreeNode($nums[$mid]);
        $root->left = $this->_traversal($nums, $left, $mid - 1);
        $root->right = $this->_traversal($nums, $mid + 1, $right);

        return $root;
    }

}
```

### go
```go
func sortedArrayToBST(nums []int) *TreeNode {
    var _traversal func(nums []int, left int, right int) *TreeNode
    _traversal = func(nums []int, left int, right int) *TreeNode {
        if left > right {
            return nil
        }
        mid := left + ((right - left) >> 1)
        node := &TreeNode{Val: nums[mid]}
        node.Left = _traversal(nums, left, mid - 1)
        node.Right = _traversal(nums, mid + 1, right)
        return node
    }
    
	root := _generate(nums, 0, len(nums) - 1)
    return root
}
```

### java
```java
class LeetCode0108 {

    public TreeNode sortedArrayToBST(int[] nums) {
        return _traversal(nums, 0, nums.length - 1);
    }

    protected TreeNode _traversal(int[] nums, int left, int right) {
        if (left > right) {
            return null;
        }
        int mid = left + ((right - left) >> 1);
        TreeNode node = new TreeNode(nums[mid]);
        node.left = _traversal(nums, left, mid - 1);
        node.right = _traversal(nums, mid + 1, right);
        
        return node;
    }
}
```
