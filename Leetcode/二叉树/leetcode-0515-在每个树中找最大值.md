### 515. 在每个树行中找最大值

# 题目
您需要在二叉树的每一行中找到最大的值。

https://leetcode.cn/problems/find-largest-value-in-each-tree-row/

提示：
- 二叉树的节点个数的范围是 [0,10^4]
- -2^31 <= Node.val <= 2^31 - 1

# 示例
```
输入:

          1
         / \
        3   2
       / \   \
      5   3   9

输出: [1, 3, 9]
```

# 解析
&emsp;&emsp;广度优先 BFS 同第102题

# 代码

```php
require_once './class/TreeNode.class.php';
$arr = [1,3,2,5,3,null,9];
$root = generateTreeByArray($arr);
(new LeetCode1046())->main($root);

class LeetCode0515
{
    public function main($root) {
        var_dump($this->largestValues($root));
    }

    function largestValues($root) {
        $res = [];
        if ($root == null) {
            return $res;
        }
        $queue = new SplQueue();
        $queue->enqueue($root);
        $level = 0;
        while ($queue->isEmpty() == false) {
            $count = $queue->count();
            for ($i = $count; $i > 0; $i--) {
                $node = $queue->dequeue();
                $res[$level] = max($node->val, $res[$level]);
                $node->left  != null && $queue->enqueue($node->left);
                $node->right != null && $queue->enqueue($node->right);
            }
            $level++;
        }
        return $res;
    }

}
```