# 0100. 相同的树

# 题目
给你两棵二叉树的根节点 p 和 q ，编写一个函数来检验这两棵树是否相同。

如果两个树在结构上相同，并且节点具有相同的值，则认为它们是相同的。

https://leetcode.cn/problems/same-tree/description/

提示：
- 两棵树上的节点数目都在范围 [0, 100] 内
- -10000 <= Node.val <= 10000

# 示例
```
输入：p = [1,2,3], q = [1,2,3]
输出：true
```
```
输入：p = [1,2], q = [1,null,2]
输出：false
```
```
输入：p = [1,2,1], q = [1,1,2]
输出：false
```

# 解析
在 [101. 对称二叉树](./leetcode-0101-简单-对称二叉树.md)，讲到对于二叉树是否对称，
要比较的是根节点的左子树与右子树是不是相互翻转的，理解这一点就知道了要比较的是两个树（这两个树是根节点的左右子树），
所以在递归遍历的过程中，也是要同时遍历两棵树。

理解这一本质之后，就会发现，求二叉树是否对称，和求二叉树是否相同几乎是同一道题目。

要比较的是两个树是否是相互相同的，参数也就是两个树的根节点。要比较两个节点数值相不相同，首先要把两个节点为空的情况弄清楚，
否则后面比较数值的时候就会操作空指针了。

节点为空的情况有：
- tree1 为空，tree2 不为空，不对称，return false
- tree1 不为空，tree2 为空，不对称 return false
- tree1，tree2 都为空，对称，return true

此时已经排除掉了节点为空的情况，那么剩下的就是 tree1 和 tree2 不为空的时候：
- tree1、tree2 都不为空，比较节点数值，不相同就 return false

此时 tree1、tree2 节点不为空，且数值也不相同的情况也处理了。



# 代码

### php
```php
class LeetCode0100 {

    public function isSameTree($p, $q) {
        if ($p == null && $q == null) {
            return true;
        }
        if ( $p == null || $q == null) {
            return false;
        }
        if ($p->val != $q->val) {
            return false;
        }

        return $this->isSameTree($p->left, $q->left) && $this->isSameTree($p->right, $q->right);
    }
}
```

### go
```go
func isSameTree(p *TreeNode, q *TreeNode) bool {
    if p == nil && q == nil {
        return true
    }
    if p == nil || q == nil {
        return false
    }
    if p.Val != q.Val {
        return false
    }

    return isSameTree(p.Left, q.Left) && isSameTree(p.Right, q.Right)
}
```

### java
```java
class LeetCode0100 {

    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        }
        if (p == null || q == null) {
            return false;
        }
        if (p.val != q.val) {
            return false;
        }
        
        return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }

}
```