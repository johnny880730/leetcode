# 0129. 求根节点到叶节点数字之和

# 题目
给你一个二叉树的根节点 root ，树中每个节点都存放有一个 0 到 9 之间的数字。每条从根节点到叶节点的路径都代表一个数字：
例如，从根节点到叶节点的路径 1 -> 2 -> 3 表示数字 123 。计算从根节点到叶节点生成的 所有数字之和 。

叶节点 是指没有子节点的节点。

https://leetcode.cn/problems/sum-root-to-leaf-numbers/description/

提示：
- 树中节点的数目在范围 [1, 1000] 内
- 0 <= Node.val <= 9
- 树的深度不超过 10

# 示例
```
输入：root = [1,2,3]
输出：25
解释：
从根到叶子节点路径 1->2 代表数字 12
从根到叶子节点路径 1->3 代表数字 13
因此，数字总和 = 12 + 13 = 25
```
```
输入：root = [4,9,0,5,1]
输出：1026
解释：
从根到叶子节点路径 4->9->5 代表数字 495
从根到叶子节点路径 4->9->1 代表数字 491
从根到叶子节点路径 4->0 代表数字 40
因此，数字总和 = 495 + 491 + 40 = 1026
```


# 解析

这道题中，二叉树的每条从根节点到叶子节点的路径都代表一个数字。其实，每个节点都对应一个数字，等于其父节点对应的数字乘以 10 再加上该节点的值
（这里假设根节点的父节点对应的数字是 0）。只要计算出每个叶子节点对应的数字，然后计算所有叶子节点对应的数字之和，即可得到结果。

## DFS
深度优先搜索是很直观的做法。从根节点开始，遍历每个节点，如果遇到叶子节点，则将叶子节点对应的数字加到数字之和。

如果当前节点不是叶子节点，则计算其子节点对应的数字，然后对子节点递归遍历。

## 递归 + 回溯
这里要遍历整个二叉树，且需要返回值做逻辑处理，所有返回值为 void，

参数只需要把根节点传入，此时还需要定义两个全局变量，一个是 result，记录最终结果，一个是 path。

递归什么时候终止呢？当然是遇到叶子节点，此时要收集结果了，通知返回本层递归，

采用前中后序都不无所谓， 因为也没有中间几点的处理逻辑。

这里主要是当左节点不为空，path 收集路径，并递归左孩子，右节点同理。

但别忘了回溯。

# 代码

### php
```php
class LeetCode0129 {

    // DFS
    function sumNumbers($root) {
        return $this->_dfs($root, 0);
    }

    protected function _dfs($node, $prevSum) {
        if (!$node) {
            return 0;
        }
        $sum = $prevSum * 10 + $node->val;
        if (!$node->left && !$node->right) {
            return $sum;
        } else {
            return $this->_dfs($node->left, $sum) + $this->_dfs($node->right, $sum);
        }
    }
    
    // 递归 + 回溯
    function sumNumbers2($root) {
        $this->path = [];
        $this->res = 0;
        if (!$root) {
            return 0;
        }
        $this->path[] = $root->val;
        $this->_traversal($root);
        return $this->res;
    }

    function _traversal($node) {
        // 遇到叶子节点
        if (!$node->left && !$node->right) {
            $sum = 0;
            for ($i = 0; $i < count($this->path); $i++) {
                $sum = $sum * 10 + $this->path[$i];
            }
            $this->res += $sum;
            return;
        }
        if ($node->left) {
            // 处理
            $this->path[] = $node->left->val;
            // 递归
            $this->_traversal($node->left);
            // 回溯
            array_pop($this->path);
        }
        if ($node->right) {
            $this->path[] = $node->right->val;
            $this->_traversal($node->right);
            array_pop($this->path);
        }
        return;
    }

}
```

### go
```go
// DFS
func sumNumbers(root *TreeNode) int {
	return _dfs(root, 0)
}

func _dfs(node *TreeNode, prevSum int) int {
	if node == nil {
		return 0
	}
	sum := prevSum * 10 + node.Val
	if node.Left == nil && node.Right == nil {
		return sum
	}
	return _dfs(node.Left, sum) + _dfs(node.Right, sum)
}

// 递归 + 回溯
func sumNumbers2(root *TreeNode) int {
    path := make([]int, 0)
    res := 0
    if root == nil {
        return 0
    }

    var _traversal func(node *TreeNode)
    _traversal = func(node *TreeNode) {
        if node.Left == nil && node.Right == nil {
            sum := 0
            for i := 0; i < len(path); i++ {
                sum = sum * 10 + path[i]
            }
            res += sum
            return
        }
        
        if node.Left != nil {
            path = append(path, node.Left.Val)
            _traversal(node.Left)
            path = path[: len(path) - 1]
        }
        if node.Right != nil{
            path = append(path, node.Right.Val)
            _traversal(node.Right)
            path = path[: len(path) - 1]
        }
        return
    }
    
    path = append(path, root.Val)
    _traversal(root)
    return res

}
```

### java
```java
class LeetCode0129 {

    // DFS
    public int sumNumbers(TreeNode root) {
        return _dfs(root, 0);
    }

    protected int _dfs(TreeNode node, int prevSum) {
        if (node == null) {
            return 0;
        }
        int sum = prevSum * 10 + node.val;
        if (node.left == null && node.right == null) {
            return sum;
        }

        return _dfs(node.left, sum) + _dfs(node.right, sum);
    }

}
```