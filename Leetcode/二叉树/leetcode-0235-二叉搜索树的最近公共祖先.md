# 257. 二叉树的所有路径

# 题目
给定一个二叉搜索树, 找到该树中两个指定节点的最近公共祖先。

百度百科中最近公共祖先的定义为：“对于有根树 T 的两个结点 p、q，
最近公共祖先表示为一个结点 x，满足 x 是 p、q 的祖先且 x 的深度尽可能大（一个节点也可以是它自己的祖先）。”

https://leetcode.cn/problems/lowest-common-ancestor-of-a-binary-search-tree

# 例如
```
给定如下二叉搜索树: root = [6,2,8,0,4,7,9,null,null,3,5]

      6
   /     \
  2       8
 / \     / \
0   4   7   9
   / \
  3   5
```

# 解析
二叉搜索树是有序的，在有序树中，如何判断要给节点的左子树中有 p、右子树中有 q 呢？

在从上到下遍历 BST 的过程中，如果 cur 节点的数值在 [p, q] 区间，则说明该节点 cur 就是最近公共祖先。

可以采用前序遍历（这里没有中间节点的处理逻辑，哪种遍历方式都可以）。

单层递归的逻辑：遍历 BST 的过程就是寻找区间 [p->val, q->val]，如果 cur->val > p->val，同时 cur->val > q->val，
那么应该向左遍历（说明目标区间在右子树上）。需要注意的是，此时不知道 p 和 q 谁大，所以既要判断 cur->val 是否大于 p->val，
还要判断 cur->val 是否大于 q->val。

# 代码

### php
```php
class LeetCode0235 {
    
    function lowestCommonAncestor($root, $p, $q) {
        return $this->_traversal($root, $p, $q);
    }
    
    protected function _traversal($cur, $p, $q) {
        if ($cur == null) {
            return null;
        }
        // p、q的值都小于根，则从左子树中找
        if ($p->val < $cur->val && $q->val < $cur->val) {
            $left = $this->_traversal($cur->left, $p, $q); 
            if ($left != null) {
                return $left;
            }
        }
        // p、q的值都大于根，则从右子树中找
        if ($p->val > $cur->val && $q->val > $cur->val) {
            $right = $this->_traversal($cur->right, $p, $q);
            if ($right != null) {
                return $right;
            }
        }
        
        return $cur;
    }
}
```

### go
```go
func lowestCommonAncestor(root, p, q *TreeNode) *TreeNode {
    var _traversal func(cur, p, q *TreeNode) *TreeNode
    _traversal = func(cur, p, q *TreeNode) *TreeNode {
        if cur == nil {
            return nil
        }
        if p.Val < cur.Val && q.Val < cur.Val {
            left := _traversal(cur.Left, p, q)
            if left != nil {
                return left
            }
        }
        if p.Val > cur.Val && q.Val > cur.Val {
            right := _traversal(cur.Right, p, q)
            if right != nil {
                return right
            }
        }
        return cur
    }
    
    return _traversal(root, p, q)
}
```