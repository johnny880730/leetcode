# 1382. 将二叉搜索树变平衡

# 题目
给你一棵二叉搜索树，请你返回一棵 平衡后 的二叉搜索树，新生成的树应该与原来的树有着相同的节点值。如果有多种构造方法，请你返回任意一种。

如果一棵二叉搜索树中，每个节点的两棵子树高度差不超过 1 ，我们就称这棵二叉搜索树是 平衡的 。

https://leetcode.cn/problems/balance-a-binary-search-tree

提示：
- 树节点的数目在 [1, 10^4] 范围内。
- 1 <= Node.val <= 10^5

# 示例
```
输入：root = [1,null,2,null,3,null,4,null,null]
输出：[2,1,3,null,null,null,4]
解释：这不是唯一的正确答案，[3,1,4,null,2,null,null] 也是一个可行的构造方案。
```
```
输入: root = [2,1,3]
输出: [2,1,3]
```

# 解析
这道题目，可以中序遍历把二叉树转变为有序数组，然后在根据有序数组构造平衡二叉搜索树。

建议做这道题之前，先看如下两篇题解：
- [98. 验证二叉搜索树](../top-interview-questions/leetcode-0098-中等-验证二叉搜索树.md)
- [108. 将有序数组转换为二叉搜索树](../top-interview-questions/leetcode-0108-简单-将有序数组转换为二叉搜索树.md)

这两道题目做过之后，本题分分钟就可以做出来了。


# 代码

### php
```php
class Leetcode1382 {

    public $arr = [];

    function balanceBST($root) {
        $this->arr = [];
        $this->_traversal($root);
        return $this->_getTree(0, count($this->arr) - 1);
    }

    // 有序树转成有序数组
    function _traversal($node) {
        if (!$node) {
            return;
        }
        $this->_traversal($node->left);
        $this->arr[] = $node->val;
        $this->_traversal($node->right);
    }

    // 有序数组转平衡二叉树
    function _getTree($left, $right) {
        if ($left > $right) {
            return null;
        }
        $mid = $left + (($right - $left) >> 1);
        $node = new TreeNode($this->arr[$mid]);
        $node->left = $this->_getTree($left, $mid - 1);
        $node->right = $this->_getTree($mid + 1, $right);

        return $node;
    }
    
}
```

### go
```go
func balanceBST(root *TreeNode) *TreeNode {
	arr := make([]int, 0)

	var _traversal func(node *TreeNode)
	_traversal = func(node *TreeNode) {
		if node == nil {
			return
		}
		_traversal(node.Left)
		arr = append(arr, node.Val)
		_traversal(node.Right)
	}

	var _getTree func(left, right int) *TreeNode
	_getTree = func(left, right int) *TreeNode {
		if left > right {
			return nil
		}
		mid := left + ((right - left) >> 1)
		node := &TreeNode{Val: arr[mid]}
		node.Left = _getTree(left, mid - 1)
		node.Right = _getTree(mid + 1, right)
		return node
	}
	
	_traversal(root)
	return _getTree(0, len(arr) - 1)
}
```