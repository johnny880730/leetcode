# 0105. 从前序与中序遍历序列构造二叉树

# 题目
给定一棵树的前序遍历 preorder 与中序遍历  inorder。请构造二叉树并返回其根节点。

https://leetcode.cn/problems/construct-binary-tree-from-preorder-and-inorder-traversal/description/

# 示例:
```
Input: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]
Output: [3,9,20,null,null,15,7]
```

# 示例
```
Input: preorder = [-1], inorder = [-1]
Output: [-1]
```

提示:
- 1 <= preorder.length <= 3000
- inorder.length == preorder.length
- -3000 <= preorder[i], inorder[i] <= 3000
- preorder 和 inorder 均无重复元素
- inorder 均出现在 preorder
- preorder 保证为二叉树的前序遍历序列
- inorder 保证为二叉树的中序遍历序列

# 解析
此题是经典题目，面试、笔试中常考。

首先思考对根节点做什么？肯定要想办法确定根节点的值，把根节点做出来，然后递归左右子树即可。

先来回顾下，前序遍历和中序遍历的结果有什么特点？前序遍历的第一个值 preOrder[0] 就是根节点的值。关键在于如果通过根节点的值，
将 preOrder 和 inOrder 数组划分成两半，构造根节点的左右子树。

![](./images/leetcode-0105-img1.png)

左右子树对应的 inOrder 数组的起始索引和终止索引比较容易确定

![](./images/leetcode-0105-img2.png)

而 preOrder 数组，如何确定左右数组对应的起始索引和终止索引？这个可以通过左子树的节点数推导出来。

假设左子树的节点数是 leftSize，那么 preOrder 数组上的索引情况是这样的。

![](./images/leetcode-0105-img3.png)


根据根节点的 value 找到其所在 inOrder 数组的位置，令该位置为 index，可得左子树的长度为 `leftSize = index - 1 - inStart + 1 = index - inStart`

左子树 root.left 的索引位置判断：
- 在 preOrder 数组的位置：
  - preStart 是根节点，左子树的起点就是 `preStart + 1`
  - 又左半边的长度是 leftSize，假设左子树的终点为 X，有 X - (preStart + 1) + 1 = leftSize ，所以 X = `leftSize + preStart`，即左子树的终点位置
- 在 inOrder 数组的位置：
  - 起始位置在 inStart。由于根节点在 inorder 的索引就是 index，所以终止位置是 index - 1
 

右子树 root.right 的索引位置判断：
- 在 preOrder 数组的位置：
  - 由于 root.left 在 preOrder 数组的终点位置是 leftSize + preStart，所以右子树在 preOrder 的开始位置就是 leftSize + preStart + 1。
  - 右子树的在 preOrder 的终点位置就是 `preEnd`
- 在 inOrder 数组的位置：
  - root 节点的索引是 index，那么右子树在 inOrder 的起始位置是 `index + 1`
  - 结束位置是 `inEnd`
 


# 代码

### php
```php
class LeetCode0105 {

    public $map;    // 中序遍历，in[i] => i

    public function buildTree($pre, $in){
        foreach ($in as $k => $v) {
            $this->map[$v] = $k;
        }
        return $this->_build($pre, 0, count($pre) - 1, $in, 0, count($in) - 1);
    }
    
    protected function _build($pre, $preStart, $preEnd, $in, $inStart, $inEnd) {
        if ($preStart > $preEnd) {
            return null;
        }
        // root 就是前序遍历第一个元素
        $rootVal = $pre[$preStart];
        // rootVal 在中序遍历结果中的索引
        $index = $this->map[$rootVal];
        
        // 左半边的长度
        $leftSize = ($index - 1) - $inStart + 1;
        
        $root = new TreeNode($rootVal);
        
        // 递归构造左右子树
        $root->left = $this->_build($pre, $preStart + 1, $preStart + $leftSize, $in, $inStart, $index - 1);
        $root->right = $this->_build($pre, $preStart + $leftSize + 1, $preEnd, $in, $index + 1, $inEnd);
    
        return $root;
    }

}
```

### go
```go
var hash map[int]int

func buildTree(preorder []int, inorder []int) *TreeNode {
    hash = make(map[int]int)

    for k, v := range inorder {
        hash[v] = k
    }

    return _build(preorder, 0, len(preorder) - 1, inorder, 0, len(inorder) - 1)
}

func _build(preorder []int, preStart int, preEnd int, inorder []int, inStart int, inEnd int) *TreeNode {
    if preStart > preEnd {
        return nil
    }
    rootVal := preorder[preStart]
    idx, _ := hash[rootVal]
    leftSize := idx - inStart

    node := &TreeNode{rootVal, nil, nil}
    node.Left = _build(preorder, preStart + 1, preStart + leftSize, inorder, inStart, idx - 1)
    node.Right = _build(preorder, preStart + leftSize + 1, preEnd, inorder, idx + 1, inEnd)

    return node
}
```

### java
```java
class LeetCode0105 {

  private Map<Integer, Integer> map = new HashMap<>();
  
  public TreeNode buildTree(int[] preorder, int[] inorder) {
      for (int i = 0; i < inorder.length; i++) {
          map.put(inorder[i], i);
      }
      return _build(preorder, 0, preorder.length - 1, inorder, 0, inorder.length - 1);
  }
  
  private TreeNode _build(int[] preorder, int preStart, int preEnd, int[] inorder, int inStart, int inEnd) {
      if (preStart > preEnd) {
          return null;
      }
      int rootVal = preorder[preStart];
      int idx = map.get(rootVal);
      int leftSize = idx - 1 -inStart + 1;
  
      TreeNode node = new TreeNode(rootVal);
  
      node.left  = _build(preorder, preStart + 1, preStart + leftSize, inorder, inStart, idx - 1);
      node.right = _build(preorder, preStart + leftSize + 1, preEnd, inorder, idx + 1, inEnd);
  
      return node;
  }
  
}
```
