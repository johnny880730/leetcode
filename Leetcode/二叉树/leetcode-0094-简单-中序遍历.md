# 0094. 二叉树的中序遍历【简单】

# 题目
给定一个二叉树的根节点 root ，返回它的 中序 遍历。

https://leetcode.cn/problems/binary-tree-inorder-traversal/description/

提示：
- 树中节点数目在范围 [0, 100] 内
- -100 <= Node.val <= 100

进阶：递归算法很简单，你可以通过迭代算法完成吗？

# 示例：
```
示例 1：

输入：root = [1,null,2,3]
输出：[1,3,2]
```

```
示例 2：

输入：root = []
输出：[]
```

```
示例 3：

输入：root = [1]
输出：[1]
```

# 解析

## 迭代法
使用迭代法处理元素的过程中，涉及以下两个操作：
1. 处理：将元素放入结果数组
2. 访问：遍历节点

为什么前序遍历的代码不能和中序遍历的代码通用呢？因为前序遍历的顺序是中左右，先访问和处理的元素是中间节点，所以才能写出相对简洁的代码，
要访问的元素和要处理的元素顺序是一致的，都是中间节点。

中序遍历的顺序是左、中、右，先访问的是二叉树顶部的节点，然后一层一层向下访问，直到到达树左面的底部，再开始处理节点（也就是放入结果数组），
这就造成了处理顺序和访问顺序是不一致的。

在使用迭代法实现中序遍历时，就需要借用指针的遍历来访问节点，使用栈处理节点上的元素。

## morris 遍历
morris 遍历的过程：
1. 当前节点 cur，一开始 cur 指向整棵树的头：
2. 若 cur 无左树，直接向右移动：cur = cur.right
3. 若 cur 有左树，找到左树最右的节点，记位 mostRight
    1. 若 mostRight 的右指针指向 null，让 mostRight 的右指针指向 cur，然后 cur 向左子树移动，即 cur = cur.left
    2. 若 mostRight 的右指针指向 cur，让 mostRight 的右指针指向 null，然后 cur 向右子树移动，即 cur = cur.right
4. cur 指向 null 的时候遍历停止

# 代码

### php
```php

class LeetCode0145 {

    // 递归
    function inorderTraversal($root) {
        $res = [];
        $this->_inorder($root, $res);
        return $res;
    }

    function _inorder($root, &$res) {
        if (!$root) {
            return;
        }
        $this->_inorder($root->left, $res);
        $res[] = $root->val;
        $this->_inorder($root->right, $res);
    }

    // 非递归
    function inorderTraversal2($root) {
        $res = [];
        $stack = new SplStack();
        $cur = $root;
        while ($cur != null || $stack->isEmpty() == false) {
            if ($cur != null) {
                $stack->push($cur);
                $cur = $cur->left;
            } else {
                $cur = $stack->pop();
                $res[] = $cur->val;
                $cur = $cur->right;
            }
        }
        return $res;
    }
    
    // morris 中序遍历
    function inorderTraversal3($root) {
        $res = [];
        if ($root == null) {
            return $res;
        }
        $cur = $root;
        while ($cur != null) {
            $mostRight = $cur->left;
            if ($mostRight != null) {
                while ($mostRight->right != null && $mostRight->right != $cur) {
                    $mostRight = $mostRight->right;
                }
                if ($mostRight->right == null) {
                    $mostRight->right = $cur;
                    $cur = $cur->left;
                    continue;
                } else {
                    $mostRight->right = null;
                }
            }
            $res[] = $cur->val;
            $cur = $cur->right;
        }
        return $res;
    }
}
```

### go
```go
// 普通中序遍历
func inorderTraversal(root *TreeNode) (res []int) {
    var _inOrder func(node *TreeNode)
    _inOrder = func(node *TreeNode) {
        if node == nil {
            return
        }
        _inOrder(node.Left)
        res = append(res, node.Val)
        _inOrder(node.Right)
    }
    _inOrder(root)
    return res
}

// morris 中序遍历
func inorderTraversal2(root *TreeNode) (res []int) {
    if root == nil {
        return res
    }
    cur := root
    for cur != nil {
        mostRight := cur.Left
        if mostRight != nil {
            for mostRight.Right != nil && mostRight.Right != cur {
                mostRight = mostRight.Right
            }
            if mostRight.Right == nil {
                mostRight.Right = cur
                cur = cur.Left
                continue
            } else {
                mostRight.Right = nil
            }
        }
        res = append(res, cur.Val)
        cur = cur.Right
    }
    return res
}
```

### java
```java
// 迭代法
public List<Integer> inorderTraversal(TreeNode root) {
    List<Integer> res = new ArrayList<>();
    Stack<TreeNode> stack = new Stack<>();
    TreeNode cur = root;
    while (cur != null || !stack.isEmpty()) {
        if (cur != null) {
            stack.push(cur);
            cur = cur.left;
        } else {
            cur = stack.pop();
            res.add(cur.val);
            cur = cur.right;
        }
    }
    return res;
}


// morris遍历
public List<Integer> inorderTraversal(TreeNode root) {
    List<Integer> res = new ArrayList<>();
    if (root == null) {
        return res;
    }
    TreeNode cur = root;
    while (cur != null) {
        TreeNode mostRight = cur.left;
        if (mostRight != null) {
            while (mostRight.right != null && mostRight.right != cur) {
                mostRight = mostRight.right;
            }
            if (mostRight.right == null) {
                mostRight.right = cur;
                cur = cur.left;
                continue;
            } else {
                mostRight.right = null;
            }
        }
        res.add(cur.val);
        cur = cur.right;
    }
    
    return res;
}
```
