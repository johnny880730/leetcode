# 513. 找树左下角的值

# 题目
给定一个二叉树的 根节点 root，请找出该二叉树的 最底层 最左边 节点的值。

假设二叉树中至少有一个节点。

https://leetcode.cn/problems/find-bottom-left-tree-value/

提示:
- 二叉树的节点个数的范围是 [1,10^4]
- -2^31 <= Node.val <= 2^31 - 1

# 示例
```
输入: root = [2,1,3]
输出: 1
```
```
输入: [1,2,3,4,null,5,6,null,null,7]
输出: 7
```

# 解析

### BFS
使用广度优先搜索遍历每一层的节点。在遍历一个节点时，需要先把它的非空右子节点放入队列，然后再把它的非空左子节点放入队列，
这样才能保证从右到左遍历每一层的节点。广度优先搜索所遍历的最后一个节点的值就是最底层最左边节点的值。

### 递归法
粗看的话，这道题目用递归的话就就一直向左遍历，最后一个就是答案呗？没有这么简单，一直向左遍历到最后一个，它未必是最后一行啊。

来分析一下题目：在树的最后一行找到最左边的值。首先要是最后一行，然后是最左边的值。如果使用递归法，如何判断是最后一行呢，
其实就是深度最大的叶子节点一定是最后一行。所以要找深度最大的叶子节点。

那么如何找最左边的呢？可以使用前序遍历（当然中序，后序都可以，因为本题没有 中间节点的处理逻辑，只要左优先就行），保证优先左边搜索，
然后记录深度最大的叶子节点，此时就是树的最后一行最左边的值。

本题还需要两个全局变量，maxDepth 用来记录最大深度，res 记录最大深度最左节点的数值。

当遇到叶子节点的时候，就需要统计一下最大的深度了，所以需要遇到叶子节点来更新最大深度。

在找最大深度的时候，递归的过程中依然要使用回溯

# 代码

### php
```php
class LeetCode0513 {

    // BFS
    function findBottomLeftValue($root) {
        $queue = new SplQueue();
        $queue->enqueue($root);
        $res = 0;
        while (!$queue->isEmpty()) {
            $cur = $queue->dequeue();
            if ($cur->right) {
                $queue->enqueue($cur->right);
            }
            if ($cur->left) {
                $queue->enqueue($cur->left);
            }
            $res = $cur->val;
        }
        return $res;
    }

}
```

### go
```go
// 递归法
func findBottomLeftValue(root *TreeNode) int {
    maxDepth := math.MinInt64
    var res int
    
    var _traversal func(cur *TreeNode, depth int)
        _traversal = func(cur *TreeNode, depth int) {
        if cur.Left == nil && cur.Right == nil {
            if depth > maxDepth {
            maxDepth = depth
            res = cur.Val
            }
        }
    
        if cur.Left != nil {
            depth++
            _traversal(cur.Left, depth)
            depth--
        }
        if cur.Right != nil {
            depth++
            _traversal(cur.Right, depth)
            depth--
        }
    }
    
    _traversal(root, 0)
    return res
}
```