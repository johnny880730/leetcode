# 0113. 路径总和 II

# 题目
给你二叉树的根节点 root 和一个整数目标和 targetSum ，找出所有 从根节点到叶子节点 路径总和等于给定目标和的路径。

叶子节点 是指没有子节点的节点。

https://leetcode.cn/problems/path-sum-ii/description/


提示：
- 树中节点总数在范围 [0, 5000] 内
- -1000 <= Node.val <= 1000
- -1000 <= targetSum <= 1000

# 示例：
```
输入：root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
输出：[[5,4,11,2],[5,8,4,5]]
```

```
输入：root = [1,2,3], targetSum = 5
输出：[]
```

```
输入：root = [1,2], targetSum = 0
输出：[]
```


# 解析


## 回溯

问完成一件事情的所有解决方案，一般采用回溯算法（深度优先遍历）完成。做回溯算法问题一般先画图，好在这就是一个树形问题，题目已经给我们画好了示意图。

根据这个问题的特点，我们可以采用 先序遍历 的方式：先使用 sum 减去当前结点（如果非空）的值，然后再递归处理左子树和右子树。

如果到了叶子结点，sum 恰好等于叶子结点的值，我们就得到了一个符合条件的列表（从根结点到当前叶子结点的路径）。

归纳一下递归终止条件：
- 如果遍历到的结点为空结点，返回；
- 如果遍历到的叶子结点，且 sum 恰好等于叶子结点的值。


# 代码

### php
```php
class LeetCode0113 {
    private $res = [];
    private $path = [];
    
    function pathSum($root, $sum) {
        if ($root == null) {
            return $this->res;
        }
        $this->path[] = $root->val;         // 把根节点放入路径
        $this->_backtrack($root, $sum - $root->val);
        return $this->res;
    }

    protected function _backtrack($cur, $sum)
    {
        if (!$cur->left && !$cur->right && $sum == 0) {
            $this->res[] = $this->path;
            return;
        }
        if (!$cur->left && !$cur->right) {
            return;
        }

        if ($cur->left) {
            $this->path[] = $cur->left->val;
            $sum -= $cur->left->val;
            $this->_backtrack($cur->left, $sum);
            $sum += $cur->left->val;
            array_pop($this->path);
        }
        if ($cur->right) {
            $this->path[] = $cur->right->val;
            $sum -= $cur->right->val;
            $this->_backtrack($cur->right, $sum);
            $sum += $cur->right->val;
            array_pop($this->path);
        }
        return;
    }
}
```

### java
```java
class LeetCode0113 {
    
    public List<List<Integer>> res = new ArrayList<>();
    public List<Integer> path = new ArrayList<>();

    public List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        if (root == null) {
            return this.res;
        }
        this.path.add(root.val);
        _backtrack(root, targetSum - root.val);
        return this.res;
    }
    
    private void _backtrack(TreeNode cur, int sum) {
        if (cur.left == null && cur.right == null && sum == 0) {
            this.res.add(new ArrayList<>(this.path));
            return;
        }
        if (cur.left == null && cur.right == null) {
            return;
        }
        if (cur.left != null) {
            this.path.add(cur.left.val);
            sum -= cur.left.val;
            _backtrack(cur.left, sum);
            sum += cur.left.val;
            this.path.remove(this.path.size() - 1);
        }
        if (cur.right != null) {
            this.path.add(cur.right.val);
            sum -= cur.right.val;
            _backtrack(cur.right, sum);
            sum += cur.right.val;
            this.path.remove(this.path.size() - 1);
        }
    }
}
```

### go
```go
func pathSum(root *TreeNode, sum int) [][]int {
    res := [][]int{}
    path := []int{}
    if root == nil {
        return res
    }
    path = append(path, root.Val)

	_backtrack(root, sum - root.Val, &res, &path)
	return res
}

func _backtrack(cur *TreeNode, sum int, res *[][]int, path *[]int) {
    if cur.Left == nil && cur.Right == nil && sum == 0 {
        temp := make([]int, len(*path))
		copy(temp, *path)
		*res = append(*res, temp)
        return
    }
	if cur.Left == nil && cur.Right == nil {
		return
	}

    if cur.Left != nil {
        *path = append(*path, cur.Left.Val)
	    sum -= cur.Left.Val
        _backtrack(cur.Left, sum, res, path)
        sum += cur.Left.Val
        *path = (*path)[:len(*path) - 1]
    }
    if cur.Right != nil {
        *path = append(*path, cur.Right.Val)
	    sum -= cur.Right.Val
        _backtrack(cur.Right, sum, res, path)
        sum += cur.Right.Val
        *path = (*path)[:len(*path) - 1]
    }
}
```
