# 1038. 从二叉搜索树到更大和树


# 题目
给定一个二叉搜索树 root (BST)，请将它的每个节点的值替换成树中大于或者等于该节点值的所有节点值之和。

提醒一下， 二叉搜索树 满足下列约束条件：
- 节点的左子树仅包含键 小于 节点键的节点。
- 节点的右子树仅包含键 大于 节点键的节点。
- 左右子树也必须是二叉搜索树。

https://leetcode.cn/problems/binary-search-tree-to-greater-sum-tree/

提示：
- 树中的节点数在 [1, 100] 范围内。
- 0 <= Node.val <= 100
- 树中的所有值均 不重复 。

# 示例
```
输入：[4,1,6,0,2,5,7,null,null,null,3,null,null,null,8]
输出：[30,36,21,36,35,26,15,null,null,null,33,null,null,null,8]
```
```
输入：root = [0,null,1]
输出：[1,null,1]
```

# 解析

## DFS
为了算出节点值之和，必须先访问所有节点值大于当前节点值的节点。

为了算出根节点修改后的值，应当先把右子树的所有点遍历一遍（因为二叉搜索树右子树的节点值都大于根节点的值），得到右子树所有点的节点值之和，再加上根节点的值。
这样就确定了递归的顺序：右子树-根-左子树。

算法
- 初始 tmp = 0。
- 从根节点开始递归，先递归右子树。
- 右子树递归结束后，把当前节点的值加到 tmp 中，然后用 tmp 替换当前节点的值。
- 然后递归左子树。
- 递归边界：递归到空节点时返回。


# 代码

### php
```php
class Leetcode1038 {

    private $tmp = 0;

    function bstToGst($root) {
        $this->_dfs($root);
        return $root;
    }

    function _dfs($node) {
        if ($node == null) {
            return;
        }
        $this->_dfs($node->right);
        $this->tmp += $node->val;
        $node->val = $this->tmp;
        $this->_dfs($node->left);
    }
}
```

### java
```java
class LeetCode1038 {

    private int tmp = 0;

    public TreeNode bstToGst(TreeNode root) {
        _dfs(root);
        return root;
    }

    private void _dfs(TreeNode node) {
        if (node == null) {
            return;
        }
        _dfs(node.right);
        tmp += node.val;
        node.val = tmp;
        _dfs(node.left);
    }
}
```

### go
```go
func bstToGst(root *TreeNode) *TreeNode {
    tmp := 0
    var _dfs func(*TreeNode)
    _dfs = func(node *TreeNode) {
        if node == nil {
            return
        }
        _dfs(node.Right)
        tmp += node.Val
        node.Val = tmp
        _dfs(node.Left)
    }
    _dfs(root)
    return root
}
```
