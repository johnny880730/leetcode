# 700. 二叉搜索树中的搜索

# 题目
给定二叉搜索树（BST）的根节点和一个值。 你需要在 BST 中找到节点值等于给定值的节点。 返回以该节点为根的子树。 如果节点不存在，则返回 NULL。

https://leetcode.cn/problems/search-in-a-binary-search-tree/

# 示例
```
给定二叉搜索树:

        4
       / \
      2   7
     / \
    1   3

和值: 2
你应该返回如下子树:

      2
     / \
    1   3

```

# 解析
二叉搜索树是有序树，其特性如下：
- 若它的左子树不空，则左子树上所有节点的值均小于它的根节点的值
- 若它的右子树不空，则右子树上所有节点的值均小于它的根节点的值
- 它的左、右子树也分别为二叉搜索树

确定单层递归的逻辑：因为二叉搜索树的节点是有序的，所以可以有方向的搜索。如果 root->val 大于 val，则搜索左子树；
如果 root->val 小于 val，则搜索右子树，如哦高没有搜索到目标节点，返回 null。 


# 代码

### php
```php
class LeetCode0700 {

    function searchBST($root, $val)
    {
        if ($root == null) {
            return null;
        }
        if ($root->val == $val) {
            return $root;
        }
        if ($root->val < $val) {
            return $this->searchBST($root->right, $val);
        }
        if ($root->val > $val) {
            return $this->searchBST($root->left, $val);
        }
        return null;

    }

}
```

### go
```go
func searchBST(root *TreeNode, val int) *TreeNode {
    if root == nil {
        return nil
    }
	if root.Val == val {
		return root
    }
    if root.Val > val {
        return searchBST(root.Left, val)
    }
    if root.Val < val {
        return searchBST(root.Right, val)
    }
    return nil
}
```
