# 501. 二叉搜索树中的众数

# 题目
给定一个有相同值的二叉搜索树（BST），找出 BST 中的所有众数（出现频率最高的元素）。

假定 BST 有如下定义：
- 结点左子树中所含结点的值小于等于当前结点的值
- 结点右子树中所含结点的值大于等于当前结点的值
- 左子树和右子树都是二叉搜索树

https://leetcode.cn/problems/find-mode-in-binary-search-tree/

# 示例
```
给定 BST [1,null,2,2],

   1
    \
     2
    /
   2
返回[2].

提示：如果众数超过1个，不需考虑输出顺序
```

进阶：你可以不使用额外的空间吗？（假设由递归产生的隐式调用栈的开销不被计算在内）

# 解析
对 BST 进行中序遍历。如果从头遍历有序数组中的元素，那么一定是相邻的两个元素作比较，然后输出出现频率最高的元素。
那么如何在二叉树上查找出现频率最高的元素呢？

定义一个指针指向前一个结点，这样 cur（当前节点）才能和 pre（前一个节点）作比较。当 pre 初始化为 null，就知道比较的是第一个元素。

此时又有问题了，如果是数组，如何处理呢？

首先遍历一遍数组，找出最大频率 maxCount，然后重新遍历一遍数组，把出现频率为 maxCount 的元素放入集合（重复的元素可能有多个）。
这种方式遍历了两次数组，用这种方式遍历两次 BST，也可以把结果集合算出来。

---

其实只需要遍历一次就可以找到所有的众数。

如果 count（频率）等于 maxCount（最大频率），则把这个元素加入结果集（result 数组）。但怎么能轻易的将元素放入 result 数组呢？
万一这个 maxCount 不是真正的最大频率呢？所以要做这个操作：当 count > maxCount，不仅要更新 maxCount，还要清空结果集，
因为结果集之前的元素都失效了。

# 代码

### php
```php
class LeetCode0501 {

    public $res = []; 
    public $count = 0;
    public $maxCount = 0;
    public $pre = null;
    
    public function findMode($root) {
        $this->_searchBST($root);
        return $this->res;
    }
    

    public function _searchBST($cur) {
        if ($cur == null) {
            return;
        }
        $this->_searchBST($cur->left);
        
        if ($this->pre == null) {       // 第一个节点
            $this->count = 1;
        } else if ($this->pre->val == $cur->val) {  // 与前一个节点的数值相同 
            $this->count++;
        } else {        // 与前一个节点的数值不同
            $this->count = 1;
        }
        $this->pre = $cur;      // 更新上一个节点
        
        if ($this->count == $this->maxCount) {      // 如果和最大值相同，放入结果集
            $this->res[] = $cur->val;
        }
        
        if ($this->count > $this->maxCount) {       // 如果计数大于最大频率
            $this->maxCount = $this->count;         // 更新最大频率
            $this->res = [];                        // 清空结果集
            $this->res[] = $cur->val;               // 结果集放入当前数据
        }
        
        $this->_searchBST($cur->right);
        return;
    }
}
```

### go
```go
func findMode(root *TreeNode) []int {
	res := make([]int, 0)
	count := 1
	max := 1
	var prev *TreeNode
	var travel func(node *TreeNode)

	travel = func(node *TreeNode) {
		if node == nil {
			return
		}
		travel(node.Left)
		
		if prev != nil && prev.Val == node.Val {
			count++
		} else {
			count = 1
		}
		if count >= max {
			if count > max && len(res) > 0 {
				res = []int{node.Val}
			} else {
				res = append(res, node.Val)
			}
			max = count
		}
		prev = node
		
		travel(node.Right)
	}

	travel(root)
	return res
}
```