# 0098. 验证二叉搜索树

# 题目
给你一个二叉树的根节点 root ，判断其是否是一个有效的二叉搜索树。

有效 二叉搜索树定义如下：
- 节点的左子树只包含 小于 当前节点的数。
- 节点的右子树只包含 大于 当前节点的数。
- 所有左子树和右子树自身必须也是二叉搜索树。

https://leetcode.cn/problems/validate-binary-search-tree/description/

提示：
- 树中节点数目范围在[1, 10^4] 内
- -2^31 <= Node.val <= 2^31 - 1


# 示例：
```
输入：root = [2,1,3]
输出：true
```

```
输入：root = [5,1,4,null,null,3,6]
输出：false
解释：根节点的值是 5 ，但是右子节点的值是 4 。
```

# 解析
采用中序遍历时，输出的二叉搜索树节点的数组是有序序列。基于这个特性，验证二叉搜索树就相当于判断一个序列是不是递增的。

注意：本题的 BST 不能有重复元素。

# 代码

### php
```php
class LeetCode0098 {

    function isValidBST($root) {
        $arr = $this->_inorderTraversal($root);
        for ($i = 1, $len = count($arr); $i < $len; $i++) {
            if ($arr[$i] <= $arr[$i - 1]) {
                return false;
            }
        }
        return true;
    }

    // morris 中序遍历
    function _inorderTraversal($root) {
        $res = [];
        if ($root == null) {
            return $res;
        }
        $cur = $root;
        while ($cur != null) {
            $mostRight = $cur->left;
            if ($mostRight != null) {
                while ($mostRight->right != null && $mostRight->right != $cur) {
                    $mostRight = $mostRight->right;
                }
                if ($mostRight->right == null) {
                    $mostRight->right = $cur;
                    $cur = $cur->left;
                    continue;
                } else {
                    $mostRight->right = null;
                }
            }
            $res[] = $cur->val;
            $cur = $cur->right;
        }
        return $res;
    }
}
```

### go
```go
func isValidBST(root *TreeNode) bool {
    // morris 遍历
    var _inOrder func(node *TreeNode) (res []int)
    _inOrder = func(node *TreeNode) (res []int) {
        if node == nil {
            return res
        }
        cur := node
        for cur != nil {
            mostRight := cur.Left
            if mostRight != nil {
                for mostRight.Right != nil && mostRight.Right != cur {
                    mostRight = mostRight.Right
                }
                if mostRight.Right == nil {
                    mostRight.Right = cur
                    cur = cur.Left
                    continue
                } else {
                    mostRight.Right = nil
                }
            }
            res = append(res, cur.Val)
            cur = cur.Right
        }
        return res
    }
	
    arr := _inOrder(root)
    length := len(arr)
    for i := 1; i < length; i++ {
        if arr[i] <= arr[i - 1] {
            return false
        }
    }
    return true
}

// 双指针判断
func isValidBST(root *TreeNode) bool {
    var pre *TreeNode
    var _isValid func(root *TreeNode) bool
    _isValid = func(root *TreeNode) bool {
        if root == nil {
            return true
        }
        leftRes := _isValid(root.Left)
        
        if pre != nil && pre.Val >= root.Val {
            return false
        }
        pre = root
        
        rightRes := _isValid(root.Right)
        
        return leftRes && rightRes
    }
    
    return _isValid(root)
}
```

### java
```java
class LeetCode0098 {

    public boolean isValidBST(TreeNode root) {
        List<Integer> arr = _inorder(root);
        for (int i = 1; i < arr.size(); i++) {
            if (arr.get(i - 1) >= arr.get(i) ) {
                return false;
            }
        }
        return true;
    }

    public List<Integer> _inorder(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        TreeNode cur = root;
        while (cur != null) {
            TreeNode mostRight = cur.left;
            if (mostRight != null) {
                while (mostRight.right != null && mostRight.right != cur) {
                    mostRight = mostRight.right;
                }
                if (mostRight.right == null) {
                    mostRight.right = cur;
                    cur = cur.left;
                    continue;
                } else {
                    mostRight.right = null;
                }
            }
            res.add(cur.val);
            cur = cur.right;
        }
        
        return res;
    }
}
```

