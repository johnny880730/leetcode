# 968. 监控二叉树

# 题目
给定一个二叉树，我们在树的节点上安装摄像头。

节点上的每个摄影头都可以监视其父对象、自身及其直接子对象。

计算监控树的所有节点所需的最小摄像头数量。

https://leetcode.cn/problems/binary-tree-cameras


提示：
- 给定树的节点数的范围是 [1, 1000]。
- 每个节点的值都是 0。

# 示例
```
       0
      / 
     C
    / \
   0    0
   
输入：[0,0,null,0,0]
输出：1
解释：如图所示，一台摄像头（标识 C 处）足以监控所有节点。   
```
```
            0
           /
          C
         /
        0
       /
      C
     /
    0       

输入：[0,0,null,0,null,0,null,null,0]
输出：2
解释：需要至少两个摄像头（标识 C 处）来监视树的所有节点。 上图显示了摄像头放置的有效位置之一。
```

# 解析
这道题目其实不是那么好理解的，题目举的示例不是很典型，会误以为摄像头必须要放在中间，其实放哪里都可以只要覆盖了就行。

这道题目难在两点：
- 需要确定遍历方式
- 需要状态转移的方程

做动态规划的时候，只要最难的地方在于确定状态转移方程，至于遍历方式无非就是在数组或者二维数组上。

本题并不是动态规划，其本质是贪心，但仍要确定状态转移方式，而且要在树上进行推导，所以难度就上来了。

##### Q1：确定遍历方式

在安排选择摄像头的位置的时候，要从底向上进行推导，因为尽量让叶子节点的父节点安装摄像头，这样摄像头的数量才是最少的 ，这也是本道贪心的原理所在！

如何从低向上推导呢？就是后序遍历也就是左右中的顺序，这样就可以从下到上进行推导了。

##### Q2：需要状态转移的方程

确定了遍历顺序，再看看这个状态应该如何转移，先来看看每个节点可能有几种状态，可以说有如下三种：
1. 该节点无覆盖，用数字 0 表示
2. 本节点有摄像头，用数字 1 表示
3. 本节点有覆盖，用数字 2 表示

那么空节点究竟是哪一种状态呢？ 空节点表示无覆盖？ 表示有摄像头？还是有覆盖呢？

回归本质，为了让摄像头数量最少，要尽量让叶子节点的父节点安装摄像头，这样才能摄像头的数量最少。

那么空节点不能是无覆盖的状态，这样叶子节点就可以放摄像头了，空节点也不能是有摄像头的状态，这样叶子节点的父节点就没有必要放摄像头了，
而是可以把摄像头放在叶子节点的爷爷节点上。所以空节点的状态只能是有覆盖，这样就可以在叶子节点的父节点放摄像头了

##### Q3：递推关系

主要分为四类情况

情况一：左右节点都有覆盖

左孩子有覆盖，右孩子有覆盖，那么此时中间节点应该就是无覆盖的状态了。

![](./images/leetcode-0968-img1.png)

情况二：左右节点至少有一个无覆盖的情况

如果是以下情况，则中间节点（父节点）应该放摄像头：
- left == 0 && right == 0 左右节点无覆盖
- left == 1 && right == 0 左节点有摄像头，右节点无覆盖
- left == 0 && right == 1 左节点有无覆盖，右节点摄像头
- left == 0 && right == 2 左节点无覆盖，右节点覆盖
- left == 2 && right == 0 左节点覆盖，右节点无覆盖

这个不难理解，毕竟有一个孩子没有覆盖，父节点就应该放摄像头。此时摄像头的数量要加一

情况三：左右节点至少有一个有摄像头

如果是以下情况，其实就是 左右孩子节点有一个有摄像头了，那么其父节点就应该是2（覆盖的状态）
- left == 1 && right == 2 左节点有摄像头，右节点有覆盖
- left == 2 && right == 1 左节点有覆盖，右节点有摄像头
- left == 1 && right == 1 左右节点都有摄像头

从这个代码中，可以看出，如果 left == 1, right == 0 怎么办？其实这种条件在情况 2 中已经判断过了，root 的状态应该是 1，表示要摄像头

情况四：头结点没有覆盖

以上都处理完了，递归结束之后，可能头结点 还有一个无覆盖的情况，如下图：

![](./images/leetcode-0968-img2.png)

所以递归结束之后，还要判断根节点，如果没有覆盖

# 代码

### php
```php
class LeetCode0968 {

    protected $result = 0;
    
    public function minCameraCover($root) {
        if ($this->_traversal($root) == 0) {
            $this->result++;
        }
        return $this->result;
    }
    
    protected function _traversal($cur) {
        // 空节点，认为是有覆盖
        if (!$cur) {
            return 2;
        }
        
        $left = $this->_traversal($cur->left);
        $right = $this->_traversal($cur->right);
        
        // 情况1：左右都有覆盖
        if ($left == 2 && $right == 2) {
            return 0;
        }
        
        // 情况2：左右节点至少有一个无覆盖的情况
        if ($left == 0 || $right == 0) {
            $this->result++;
            return 1;
        }
        
        // 情况3：左右节点至少有一个有摄像头
        if ($left == 1 || $right == 1) {
            return 2;
        }
        
        // 以上已经包含了除了分支四的所有情况，因此实际上逻辑不会走到这里的
        return -1;
    }

}
```

### go
```go
func minCameraCover(root *TreeNode) int {
	res := 0
	var _traversal func(cur *TreeNode) int
	_traversal = func(cur *TreeNode) int {
		if cur == nil {
			return 2
		}
		left := _traversal(cur.Left)
		right := _traversal(cur.Right)

		if left == 2 && right == 2 {
			return 0
		}

		if left == 0 || right == 0 {
			res++
			return 1
		}

		if left == 1 || right == 1 {
			return 2
		}

		return -1
	}

	rootRes := _traversal(root)
	if rootRes == 0 {
		res++
	}
	return res
}
```