# 0236. 二叉树的最近公共祖先

# 题目
给定一个二叉树, 找到该树中两个指定节点的最近公共祖先。

百度百科中最近公共祖先的定义为：“对于有根树 T 的两个节点 p、q，最近公共祖先表示为一个节点 x，
满足 x 是 p、q 的祖先且 x 的深度尽可能大（一个节点也可以是它自己的祖先）。”

https://leetcode.cn/problems/lowest-common-ancestor-of-a-binary-tree/description/

# 示例：
```
输入：root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 1
输出：3
解释：节点 5 和节点 1 的最近公共祖先是节点 3 。
```

```
输入：root = [3,5,1,6,2,0,8,null,null,7,4], p = 5, q = 4
输出：5
解释：节点 5 和节点 4 的最近公共祖先是节点 5 。因为根据定义最近公共祖先节点可以为节点本身。
```

```
输入：root = [1,2], p = 1, q = 2
输出：1
```

提示：
- 树中节点数目在范围 [2, 100000] 内。
- -10^9 <= Node.val <= 10^9
- 所有 Node.val 互不相同 。
- p != q
- p 和 q 均存在于给定的二叉树中。

# 解析
首先想到的是如果能自底向上查找就好了，这样就可以找到公共祖先了。那么二叉树如何自底向上查找呢？

回溯，二叉树回溯的过程就是自底向上。后序遍历就符合回溯的过程，最先处理的一定是叶子节点。

如何判断一个节点是不是 p、q 的公共祖先呢？

如果找到一个节点，发现左子树出现节点 p，右子树出现节点 q；或者左子树出现 q，或者右子树出现 p，那么该节点就是 p 和 q 的最近公共祖先。

使用后序遍历，在回溯的过程中，自底向上遍历节点，一旦发现符合这个条件的节点，那么该节点就是最近公共节点了。

值得注意的是，本题的递归函数有返回值，这是因为回溯的过程中需要通过递归函数的返回值判断某个节点是不是公共祖先节点，但在本题中依然要遍历树的左右节点。

递归函数有返回值的情况下：如果搜索一条边，那么在递归函数的返回值不为空的时候，立刻返回；如果搜索整个树，则直接用变量 left、right 保存返回值，
接下来的逻辑就是用变量 left、right 判断节点是否为公共节点，也就是后序遍历中处理中间节点的逻辑（也是回溯）。

如果 left 和 right 都不为空，则说明此时 root 就是最近公共节点。

如果 left 为空、right 不为空，则返回 right，说明目标节点是通过 right 返回的，反之亦然。

如果 left、right 都为空，那么返回 left 或者 right 都可以，也就是返回空。

# 代码

### php
```php
class LeetCode0236 {

    public function lowestCommonAncestor($root, $p, $q) {
        if ($root == $p || $root == $q || $root == null) {
            return $root;
        }
        $left  = $this->lowestCommonAncestor($root->left, $p, $q);
        $right = $this->lowestCommonAncestor($root->right, $p, $q);
        if ($left != null && $right != null) {
            return $root;
        }
        if ($left == null && $right != null) {
            return $right;
        }
        if ($left != null && $right == null) {
            return $left;
        }
        // left right 都为空
        return null;
    }

}
```

### java
```java
class LeetCode0236 {
    
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == p || root == q || root == null) {
            return root;
        }
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);
        if (left != null && right != null) {
            return root;
        }
        if (left == null && right != null) {
            return right;
        }
        if (left != null && right == null) {
            return left;
        }

        return null;
    }
}
```

### go
```go
func lowestCommonAncestor(root, p, q *TreeNode) *TreeNode {
    if root == p || root == q || root == nil {
        return root
    }
    left := lowestCommonAncestor(root.Left, p, q)
    right := lowestCommonAncestor(root.Right, p, q)
    if left != nil && right != nil {
        return root
    }
    if left == nil && right != nil {
        return right
    }
    if left != nil && right == nil {
        return left
    }
    
    return nil
}
```
