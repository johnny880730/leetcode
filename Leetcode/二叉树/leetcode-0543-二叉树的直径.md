# 0543. 二叉树的直径【简单】

# 题目
给定一棵二叉树，你需要计算它的直径长度。一棵二叉树的直径长度是任意两个结点路径长度中的最大值。这条路径可能穿过也可能不穿过根结点。

注意：两结点之间的路径长度是以它们之间边的数目表示。

https://leetcode.cn/problems/diameter-of-binary-tree/description/

提示：
- 树中节点数目在范围 [1, 10,000] 内
- -100 <= Node.val <= 100

# 示例 
```
示例 1：

给定二叉树

          1
         / \
        2   3
       / \
      4   5
返回 3, 它的长度是路径 [4,2,1,3] 或者 [5,2,1,3]。
```

# 解析

## DFS
首先知道一条路径的长度为该路径经过的节点数减一，所以求直径（即求路径长度的最大值）等效于求路径经过节点数的最大值减一。

而任意一条路径均可以被看作由某个节点为起点，从其左儿子和右儿子向下遍历的路径拼接得到。

假设知道对于该节点的左儿子向下遍历经过最多的节点数 L （即以左儿子为根的子树的深度） 和其右儿子向下遍历经过最多的节点数 R 
（即以右儿子为根的子树的深度），那么以该节点为起点的路径经过节点数的最大值即为 L + R + 1。

记节点 node 为起点的路径经过节点数的最大值为 d_node，那么二叉树的直径就是所有节点 d_node 的最大值减一。

最后的算法流程为：
- 定义一个递归函数 depth(node) 计算 d_node，函数返回该节点为根的子树的深度。
- 先递归调用左儿子和右儿子求得它们为根的子树的深度 leftDepth 和 rightDepth ，则该节点为根的子树的深度即为 max(leftDepth, rightDepth) + 1
- 该节点的 d_node 值为 leftDepth + rightDepth + 1。递归搜索每个节点并设一个全局变量 ans 记录 d_node 的最大值，最后返回 ans - 1 即为树的直径

# 代码

### php
```php
class LeetCode0543 {

    private $res = 1;

    function diameterOfBinaryTree($root) {
        $this->_depth($root);
        return $this->res - 1;
    }

    protected function _depth($node) {
        if (!$node) {
            return 0;
        }
        // 左儿子为根的子树的深度
        $lDepth = $this->_depth($node->left);
        // 右儿子为根的子树的深度
        $rDepth = $this->_depth($node->right);
        // 计算d_node即 L深度+R深度+1，更新ans
        $this->res = max($this->res, $lDepth + $rDepth + 1);
        // 返回该节点为根的子树的深度
        return max($lDepth, $rDepth) + 1;
    }

}
```

### go
```go
func diameterOfBinaryTree(root *TreeNode) int {
    res := 1
    var _depth func(node *TreeNode) int
    _depth = func(node *TreeNode) int {
        if node == nil {
            return 0;
        }
        leftDepth := _depth(node.Left)
        rightDepth := _depth(node.Right)
        res = Max(res, leftDepth + rightDepth + 1)

        return Max(leftDepth, rightDepth) + 1
    }

    _depth(root)
    
    return res - 1
}

func Max(a int, b int) int {
    if a > b {
        return a
    }
    return b
}
```

### java
```java
class LeetCode0543 {

    int res = 1;

    public int diameterOfBinaryTree(TreeNode root) {
        _depth(root);
        return res - 1;
    }

    private int _depth(TreeNode node) {
        if (node == null) {
            return 0;
        }
        int leftDepth = _depth(node.left);
        int rightDepth = _depth(node.right);
        res = Math.max(res, leftDepth + rightDepth + 1);

        return Math.max(leftDepth, rightDepth) + 1;
    }

}
```
