# 110. 平衡二叉树

# 题目
给定一个二叉树，判断它是否是高度平衡的二叉树。

本题中，一棵高度平衡二叉树定义为：

一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过 1 。

https://leetcode.cn/problems/balanced-binary-tree/

# 示例：
```
         3
        / \
       9   20
           / \
          15  7
          
输入：root = [3,9,20,null,null,15,7]
输出：true
```


```
                 1
                / \
               2   2
              / \
             3   3
            / \
           4   4
           
输入：root = [1,2,2,3,3,null,null,4,4]
输出：false
```

```
输入：root = []
输出：true
```

提示：
- 树中的节点数在范围 [0, 5000] 内
- -10^4 <= Node.val <= 10^4

# 解析
强调以下两点：
- 二叉树节点的深度：从根节点到该节点的最长简单路径边的条数（或者节点数）
- 二叉树节点的高度：从该节点到叶子节点的最长简单路径边的条数（或者节点数）

关于根节点的深度究竟是 1 还是 0，不同的地方有不一样的标准，leetcode 的题目中都是以节点为一度，即根节点深度是 1。
但维基百科上定义用边为一度，即根节点的深度是 0，暂时以 leetcode 为准（毕竟要在这上面刷题）。

因为求深度可以从上到下去查 所以需要前序遍历（中左右），而高度只能从下到上去查，所以只能后序遍历（左右中）

递归的时候，如何标记左右子树的差值是否大于 1 呢？

如果当前以传入节点为根节点的二叉树已经不是二叉平衡树了，那么返回高度就没有意义了，可以返回 -1 来标记该二叉树不是平衡树。

如何判断当前以传入节点为根节点的二叉树是不是平衡二叉树呢？当然是看左子树的高度和右子树的高度之差。分别求出左右子树的高度，
如果差值小于等于 1，则返回当前二叉树的高度，否则返回 -1，表示已经不是二叉平衡树了。


# 代码

### php
```php
class LeetCode0110 {

    function isBalanced($node) {
        return $this->_getDepth($node) == -1 ? false : true;
    }

    protected function _getDepth($node) {
        if ($node == null) {
            return 0;
        }
        $leftDepth  = $this->_getDepth($node->left);
        if ($leftDepth == -1) {
            return -1;          // 说明左子树已经不是二叉平衡树
        }
        $rightDepth = $this->_getDepth($node->right);
        if ($rightDepth == -1) {
            return -1;          // 说明右子树已经不是二叉平衡树
        }
        return abs($leftDepth - $rightDepth) > 1 ? -1 : 1 + max($leftDepth, $rightDepth);
    }

}
```

### java
```java
class LeetCode0110 {
    
    public boolean isBalanced(TreeNode root) {
        return _getDepth(root) == -1 ? false : true;
    }

    private int _getDepth(TreeNode node) {
        if (node == null) {
            return 0;
        }
        int leftDepth = _getDepth(node.left);
        if (leftDepth == -1) {
            return -1;
        }
        int rightDepth = _getDepth(node.right);
        if (rightDepth == -1) {
            return -1;
        }
        return Math.abs(leftDepth - rightDepth) > 1 ? -1 : 1 + Math.max(leftDepth, rightDepth);
    }
}
```

### go
```go
func isBalanced(root *TreeNode) bool {
    var _getDepth func(node *TreeNode) int
    _getDepth = func(node *TreeNode) int {
        if node == nil {
            return 0
        }
        leftDepth, rightDepth := _getDepth(node.Left), _getDepth(node.Right)
        if leftDepth == -1 || rightDepth == -1 {
            return -1
        }
		if leftDepth - rightDepth > 1 || rightDepth - leftDepth > 1 {
		    return -1	
        }
        return 1 + max(leftDepth, rightDepth)
    }
    
    rootDepth := _getDepth(root)
    if rootDepth == -1 {
        return false
    } else {
        return true
    }

}

func max(a, b int) int {
	if a > b {
		return a 
    }
	return b
}
```