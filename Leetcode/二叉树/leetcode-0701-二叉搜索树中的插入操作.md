# 701. 二叉搜索树中的插入操作

# 题目
给定二叉搜索树（BST）的根节点 root 和要插入树中的值 value，将值插入二叉搜索树。 

返回插入后二叉搜索树的根节点。 输入数据保证 ，新值和原始二叉搜索树中的任意节点值都不同。

注意，可能存在多种有效的插入方式，只要树在插入后仍保持为二叉搜索树即可。 你可以返回任意有效的结果 。

https://leetcode.cn/problems/insert-into-a-binary-search-tree


# 示例
```
输入：root = [40,20,60,10,30,50,70], val = 25
输出：[40,20,60,10,30,50,70,null,null,25]
```
```
输入：root = [4,2,7,1,3], val = 5
输出：[4,2,7,1,3,5]
```
```
输入：root = [4,2,7,1,3,null,null,null,null,null,null], val = 5
输出：[4,2,7,1,3,5]
```


# 解析
这道题目其实是一道简单题目，但是题目中的提示：有多种有效的插入方式，还可以重构二叉搜索树，一下子吓退了不少人，瞬间感觉题目复杂了很多。

其实可以不考虑题目中提示所说的改变树的结构的插入方式。

只要按照二叉搜索树的规则去遍历，遇到空节点就插入节点就可以了



# 代码

### php
```php
class LeetCode0701 {

    public function insertIntoBST($root, $val) {
        if ($root == null) {
            return new TreeNode($val);
        }
        if ($val < $root->val) {
            $root->left = $this->insertIntoBST($root->left, $val);
        }
        if ($val > $root->val) {
            $root->right = $this->insertIntoBST($root->right, $val);
        }
        return $root;
    }

}
```

### go
```go
func insertIntoBST(root *TreeNode, val int) *TreeNode {
	if root == nil {
		return &TreeNode{Val: val}
	}

	if val < root.Val {
		root.Left = insertIntoBST(root.Left, val)
	}
	if val > root.Val {
		root.Right = insertIntoBST(root.Right, val)
	}

	return root
}
```