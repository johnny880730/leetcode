### 589. N 叉树的前序遍历

# 题目
给定一个 N 叉树，返回其节点值的 前序遍历 。

N 叉树 在输入中按层序遍历进行序列化表示，每组子节点由空值 null 分隔（请参见示例）。

进阶：递归法很简单，你可以使用迭代法完成此题吗?

https://leetcode.cn/problems/n-ary-tree-preorder-traversal/

# 示例 
```
输入：root = [1,null,3,2,4,null,5,6]
输出：[1,3,5,6,2,4]
```
```
输入：root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
输出：[1,2,3,6,7,11,14,4,8,12,5,9,13,10]
```


提示：
- 节点总数在范围 [0, 104]内
- 0 <= Node.val <= 104
- n 叉树的高度小于或等于 1000


# 解析
&emsp;&emsp;方法：迭代

&emsp;&emsp;使用栈来帮助得到前序遍历，需要保证栈顶的节点就是我们当前遍历到的节点。

&emsp;&emsp;首先把根节点入栈，因为根节点是前序遍历中的第一个节点。随后每次从栈顶取出一个节点 u，它是我们当前遍历到的节点，
并把 u 的所有子节点逆序推入栈中。例如 u 的子节点从左到右为 v1, v2, v3，那么推入栈的顺序应当为 v3, v2, v1，
这样就保证了下一个遍历到的节点（即 u 的第一个子节点 v1）出现在栈顶的位置。

# 代码
```php
class LeetCode0589
{
    // 递归
    function preorder($root)
    {
        $arr = [];
        $this->dfs($root, $arr);
        return $arr;
    }

    function dfs($node, &$arr)
    {
        if (!$node) {
            return;
        }
        $arr[] = $node->val;
        foreach ($node->children as $child) {
            $this->dfs($child, $arr);
        }
    }
    
    // 迭代
    function preorder2($root)
    {
        $stack = new SplStack();
        $stack->push($root);
        $res = [];
        while ($stack->isEmpty() == false) {
            $node = $stack->pop();
            $res[] = $node->val;
            if ($node->children) {
                for ($i = count($node->children) - 1; $i >= 0; $i--) {
                    $stack->push($node->children[$i]);
                }  
            }
        }
        return $res;
    }
}
```