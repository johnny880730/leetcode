# 0230. 二叉搜索树中第K小的元素

# 题目
给定一个二叉搜索树的根节点 root ，和一个整数 k ，请你设计一个算法查找其中第 k 个最小元素（从 1 开始计数）。

https://leetcode.cn/problems/kth-smallest-element-in-a-bst/description/

提示：
- 树中的节点数为 n 。
- 1 <= k <= n <= 10000
- 0 <= Node.val <= 10000

# 示例
```
输入：root = [3,1,4,null,2], k = 1
输出：1
```
```
输入：root = [5,3,6,2,4,null,null,1], k = 3
输出：3
```


# 解析

朴素的做法是先对二叉树进行一次完整遍历，将所有节点存入列表中，最后对列表排序后返回目标值。树的遍历可以使用 DFS 或 BFS。


# 代码

### php
```php
class LeetCode0230 {

    // 中序遍历 + 排序
    private $arr = [];
    
    function kthSmallest($root, $k) {
        $this->_inorder($root);
        return $this->arr[$k - 1];
    }

    protected function _inorder($root) {
        if (!$root) {
            return null;
        }
        $this->_inorder($root->left);
        $this->arr[] = $root->val;
        $this->_inorder($root->right);
    }

}
```

### go
```go
func kthSmallest(root *TreeNode, k int) int {
    arr := []int{}
    var _inOrder func(root *TreeNode)
    _inOrder = func(root *TreeNode) {
        if root == nil {
            return
        }
        _inOrder(root.Left)
        arr = append(arr, root.Val)
        _inOrder(root.Right)
    }
    
    _inOrder(root)
    return arr[k - 1]
}
```

### java
```java
class LeetCode0230 {

    public ArrayList<Integer> arr = new ArrayList<>();

    public int kthSmallest(TreeNode root, int k) {
        _inorder(root);
        return arr.get(k - 1);
    }

    private void _inorder(TreeNode node) {
        if (node == null) {
            return;
        }
        _inorder(node.left);
        arr.add(node.val);
        _inorder(node.right);
    }
}
```
