# 0637. 二叉树的层平均值

# 题目
给定一个非空二叉树的根节点 root , 以数组的形式返回每一层节点的平均值。与实际答案相差 10^-5 以内的答案可以被接受。

https://leetcode.cn/problems/average-of-levels-in-binary-tree/

提示：
- 树中节点数量在 [1, 10000] 范围内
- -2^31 <= Node.val <= 2^31 - 1

# 示例
```
示例 1：

输入：
    3
   / \
  9  20
    /  \
   15   7
输出：[3, 14.5, 11]
解释：
第 0 层的平均值是 3 ,  第1层是 14.5 , 第2层是 11 。因此返回 [3, 14.5, 11] 。
```

# 解析

## BFS
从根节点开始搜索，每一轮遍历同一层的全部节点，计算该层的节点数以及该层的节点值之和，然后计算该层的平均值。

用层次遍历的做法，广度优先搜索使用队列存储待访问节点，只要确保在每一轮遍历时，队列中的节点是同一层的全部节点即可。具体做法如下：
- 初始时，将根节点加入队列；
- 每一轮遍历时，将队列中的节点全部取出，计算这些节点的数量以及它们的节点值之和，并计算这些节点的平均值，然后将这些节点的全部非空子节点加入队列，重复上述操作直到队列为空，遍历结束。

由于初始时队列中只有根节点，满足队列中的节点是同一层的全部节点，每一轮遍历时都会将队列中的当前层节点全部取出，并将下一层的全部节点加入队列，因此可以确保每一轮遍历的是同一层的全部节点。

具体实现方面，可以在每一轮遍历之前获得队列中的节点数量 size，遍历时只遍历 size 个节点，即可满足每一轮遍历的是同一层的全部节点。

# 代码
```php
class LeetCode0637 {

    // BFS
    function averageOfLevels($root) {
        $queue = new SplQueue();
        $queue->enqueue($root);

        $res = [];
        while ($count = $queue->count()) {
            $sum = 0;
            for ($i = 0; $i < $count; $i++) {
                $node = $queue->dequeue();
                $sum += $node->val;
                if ($node->left) {
                    $queue->enqueue($node->left);
                }
                if ($node->right) {
                    $queue->enqueue($node->right);
                }
            }
            $res[] = round($sum / $count, 5);
        }

        return $res;
    }

}
```

### go
```go
func averageOfLevels(root *TreeNode) []float64 {
    queue := list.New()
    queue.PushBack(root)
    res := make([]float64, 0)
    for queue.Len() > 0 {
        qLen := queue.Len()
        sum := 0
        for i := 0; i < qLen; i++ {
            node := queue.Remove(queue.Front()).(*TreeNode)
            sum += node.Val
            if node.Left != nil {
                queue.PushBack(node.Left)
            }
            if node.Right != nil {
                queue.PushBack(node.Right)
            }
        }
        res = append(res, float64(sum)/float64(qLen))
    }

    return res
}
```

### java
```java
class LeetCode0637 {

    public List<Double> averageOfLevels(TreeNode root) {
        List<Double> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            double sum = 0;
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.poll();
                sum += node.val;
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
            }
            res.add(sum / size);
        }

        return res;
    }
}
```

