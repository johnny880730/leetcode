# 0101. 对称二叉树【简单】

# 题目
给你一个二叉树的根节点 root ， 检查它是否轴对称。

https://leetcode.cn/problems/symmetric-tree/description/

提示：
- 树中节点数目在范围 [1, 1000] 内
- -100 <= Node.val <= 100

# 示例
```
示例 1：

输入：root = [1,2,2,3,4,4,3]
输出：true
```

```
示例 2：

输入：root = [1,2,2,null,3,null,3]
输出：false
```

# 解析
首先要想清楚，判断是否对称要比较的是哪两个节点，要比较的可不是左右节点。

判断是否对称，要比较的是根节点的左子树与右子树是不是相互翻转的，理解这一点就知道了其实要比较的是两棵树（这两棵树是根节点的左右子树），
所以在递归遍历的过程中，也需要同时遍历这两棵树。

那么如何比较呢？要比较的是两棵子树的里侧和外侧的元素是否相等。

如果同时满足下面的条件，两个树互为镜像：
- 它们的两个根结点具有相同的值
- 每个树的右子树都与另一个树的左子树镜像对称

# 代码

### php
```php
class LeetCode0101 {
   
    public function isSymmetric($root) {
        return $this->_isMirror($root, $root);
    }

    // head1 是原始树，head2 是翻面树
    protected function _isMirror($head1, $head2) {
        if ($head1 == null && $head2 == null) {
            return true;
        }
        if ($head1 != null && $head2 != null) {
            return $head1->val == $head2->val
                && $this->_isMirror($head1->left, $head2->right)
                && $this->_isMirror($head1->right, $head2->left);
        }
        // 一个为空一个不为空 false
        return false;
    }
}
```

### go
```go
func isSymmetric(root *TreeNode) bool {
    var _isMirror func(node1 *TreeNode, node2 *TreeNode) bool
    _isMirror = func(node1 *TreeNode, node2 *TreeNode) bool {
        if node1 == nil && node2 == nil {
            return true
        }
        if node1 != nil && node2 != nil {
            res := node1.Val == node2.Val
            res = res && _isMirror(node1.Left, node2.Right)
            res = res && _isMirror(node1.Right, node2.Left)
            return res
        }
        return false
        
    }
    return _isMirror(root, root)
}
```

### java
```java
class LeetCode0101 {

    public boolean isSymmetric(TreeNode root) {
        return _isMirror(root, root);
    }

    protected boolean _isMirror(TreeNode head1, TreeNode head2) {
        if (head1 == null && head2 == null) {
            return true;
        }
        if (head1 != null && head2 != null) {
            return head1.val == head2.val
                && _isMirror(head1.left, head2.right)
                && _isMirror(head1.right, head2.left);
        }

        return false;
    }
    
}
```
