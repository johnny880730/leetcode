### 590. N 叉树的后序遍历

# 题目
给定一个 n 叉树的根节点 root ，返回 其节点值的 后序遍历 。

n 叉树 在输入中按层序遍历进行序列化表示，每组子节点由空值 null 分隔（请参见示例）。

https://leetcode.cn/problems/n-ary-tree-postorder-traversal

提示：
- 节点总数在范围 [0, 10^4] 内
- 0 <= Node.val <= 10^4
- n 叉树的高度小于或等于 1000
# 示例
```
输入：root = [1,null,3,2,4,null,5,6]
输出：[5,6,3,2,4,1]
```
```
输入：root = [1,null,2,3,4,5,null,null,6,7,null,8,null,9,10,null,null,11,null,12,null,13,null,null,14]
输出：[2,6,14,11,7,3,12,8,4,13,9,10,5,1]
```


# 解析

&emsp;&emsp;在后序遍历中，会先遍历一个节点的所有子节点，再遍历这个节点本身。

&emsp;&emsp;例如当前的节点为 u，它的子节点为 v1, v2, v3 时，那么后序遍历的结果为：
> [children of v1], v1, [children of v2], v2, [children of v3], v3, u

&emsp;&emsp;其中 [children of vk] 表示以 vk 为根节点的子树的后序遍历结果（不包括 vk）。

&emsp;&emsp;将结果反转，得到
> u, v3, [children of v3]', v2, [children of v2]', v1, [children of v1]'

&emsp;&emsp;其中 [a]' 表示 [a] 的反转。

&emsp;&emsp;此时可以发现结果和前序遍历非常类似，只不过前序遍历中对子节点的遍历顺序是 v1, v2, v3，而这里是 v3, v2, v1。

&emsp;&emsp;因此可以使用和 N叉树的前序遍历 相同的方法，使用一个栈来得到后序遍历。首先把根节点入栈。

&emsp;&emsp;每次从栈顶取出一个节点 u 时，就把 u 的所有子节点顺序推入栈中。例如 u 的子节点从左到右为 v1, v2, v3，
那么推入栈的顺序应当为 v1, v2, v3，这样就保证了下一个遍历到的节点（即 u 的第一个子节点 v3）出现在栈顶的位置。
在遍历结束之后，我们把遍历结果反转，就可以得到后序遍历。

# 代码
```php
class LeetCode0590
{
    // 递归
    function postorder($root)
    {
        $arr = [];
        $this->dfs($root, $arr);
        return $arr;
    }

    function dfs($node, &$arr)
    {
        if (!$node) {
            return;
        }
        foreach ($node->children as $child) {
            $this->dfs($child, $arr);
        }
        $arr[] = $node->val;
    }
    
    // 迭代
    public function postorder2($root) {
        $res = [];
        if ($root == null) {
            return $res;
        }

        $stack = new SplStack();
        $stack->push($root);
        while ($stack->isEmpty() == false) {
            $node = $stack->pop();
            $res[] = $node->val;
            if ($node->children) {
                for ($i = 0; $i < count($node->children); $i++) {
                    $stack->push($node->children[$i]);
                }
            }
        }
        return array_reverse($res);
    }
}

```