# 0104. 二叉树的最大深度【简单】

# 题目
给定一个二叉树，找出其最大深度。

二叉树的深度为根节点到最远叶子节点的最长路径上的节点数。

说明：叶子节点是指没有子节点的节点。

https://leetcode.cn/problems/maximum-depth-of-binary-tree/description/

提示：
- 树中节点的数量在 [0, 10,000] 区间内。
- -100 <= Node.val <= 100

# 示例
```
示例 1：

给定二叉树 [3,9,20,null,null,15,7]，

    3
   / \
  9  20
    /  \
   15   7
   
返回它的最大深度3 。
```

```
示例 2：

输入：root = [1,null,2]
输出：2
```

# 解析

对二叉树来说，高度和深度是相反的表示，深度是从上到下数的，而高度是从下往上数。因此根节点的高度，就是二叉树的最大深度。

通过递归函数的返回值计算树的高度，需要使用后序遍历。因为后续遍历是【左右中】的顺序，左右子节点可以将自己的高度返回给父节点，
父节点将这个高度再 +1 就是自己的高度了。

先求左子树的高度，再求右子树的高度，最后取左右高度中的最大值再 +1 （因为算上当前中间节点）就是当前节点为根节点的树的深度。

# 代码

### php
```php
class LeetCode0104 {

    function maxDepth($root) {
        if ($root == null) {
            return 0;
        }
        $leftDepth  = $this->maxDepth($root->left);
        $rightDepth = $this->maxDepth($root->right);
        return max($leftDepth, $rightDepth) + 1;
    }
    
}
```

### go
```go
func maxDepth(root *TreeNode) int {
    if root == nil {
        return 0
    }
    leftDepth := maxDepth(root.Left)
    rightDepth := maxDepth(root.Right)
	
    rootDepth := leftDepth
    if leftDepth < rightDepth {
        rootDepth = rightDepth
    }
	
    return rootDepth + 1
}
```

### java
```java
class LeetCode0104 {

    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int leftDepth = maxDepth(root.left);
        int rightDepth = maxDepth(root.right);
        return Math.max(leftDepth, rightDepth) + 1;
    }
}
```
