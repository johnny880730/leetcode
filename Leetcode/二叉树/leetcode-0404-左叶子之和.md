# 404. 左叶子之和

# 题目
给定二叉树的根节点 root ，返回所有左叶子之和。

https://leetcode.cn/problems/sum-of-left-leaves/

提示:
- 节点数在 [1, 1000] 范围内
- -1000 <= Node.val <= 1000

# 示例
```
    3
   / \
  9  20
    /  \
   15   7

在这个二叉树中，有两个左叶子，分别是 9 和 15，所以返回 24
```



# 解析
一个节点为「左叶子」节点，当且仅当它是某个节点的左子节点，并且它是一个叶子结点。因此可以考虑对整棵树进行遍历，当遍历到节点 node 时，
如果它的左子节点是一个叶子结点，那么就将它的左子节点的值累加计入答案。

遍历整棵树的方法这里采用了深度优先搜索（DFS）。在 DFS 时，加个参数，指名当前是左还是右儿子。


# 代码

### php
```php
class LeetCode0404 {

    public function sumOfLeftLeaves($root) {
        $res = [];
        $this->getLeaf($root, $res);
        return array_sum($res);
    }

    public function getLeaf($node, &$res, $isLeft = false)  {
        if ($node == null) {
            return;
        }
        // 是叶子节点且是“左”叶子，数组扩充
        if ($node->left == null && $node->right == null && $isLeft) {
            $res[] = $node->val;
            return;
        }
        if ($node->left) {
            $this->getLeaf($node->left, $res, true);
        }
        if ($node->right) {
            $this->getLeaf($node->right, $res, false);
        }

        return;
    }
}
```

### go
```go
func sumOfLeftLeaves(root *TreeNode) int {

    arr := make([]int, 0)
    var getLeaf func(node *TreeNode, isLeft bool)
    
    getLeaf = func(node *TreeNode, isLeft bool) {
        if node == nil {
            return
        }
        if node.Left == nil && node.Right == nil && isLeft {
            arr = append(arr, node.Val)	
            return
        }
        if node.Left != nil {
            getLeaf(node.Left, true)
        }
        if node.Right != nil {
            getLeaf(node.Right, false)
        }
    }
    
    getLeaf(root, false)
    res := 0
    for _, v := range arr {
        res += v
    }
    return res
}
```
