# 889. 根据前序和后序遍历构造二叉树

# 题目
给定两个整数数组，preorder 和 postorder ，其中 preorder 是一个具有 无重复 值的二叉树的前序遍历，postorder 是同一棵树的后序遍历，重构并返回二叉树。

如果存在多个答案，您可以返回其中 任何 一个。

https://leetcode.cn/problems/construct-binary-tree-from-preorder-and-postorder-traversal

提示：
- 1 <= preorder.length <= 30
- 1 <= preorder[i] <= preorder.length
- preorder 中所有值都 不同
- postorder.length == preorder.length
- 1 <= postorder[i] <= postorder.length
- postorder 中所有值都 不同
- 保证 preorder 和 postorder 是同一棵二叉树的前序遍历和后序遍历

# 示例
```
输入：preorder = [1,2,4,5,3,6,7], postorder = [4,5,2,6,7,3,1]
输出：[1,2,3,4,5,6,7]
```

# 解析
参考：
- [105. 从前序与中序遍历序列构造二叉树](../top-interview-questions/leetcode-0105-中等-从前序与中序遍历序列构造二叉树.md)
- [106. 从中序与后序遍历序列构造二叉树](./leetcode-0106-从中序与后序遍历序列构造二叉树.md)


构建二叉树的套路很简单，可以先查看上面的参考题目。

先找到根节点，然后找到并递归构造左右子树即可。这道题，可以确定根节点，但是无法确切的知道左右子树各有哪些节点。

但是用后序遍历和前序遍历结果还原二叉树，解法逻辑上和前两道题差别不大，也是通过控制左右子树的索引来构建
1. 前序遍历结果的第一个元素或者后序遍历结果的最后一个元素确定为根节点的值
2. 前序遍历结果的第二个元素作为左子树的根节点的值
3. 在后序遍历结果中寻找左子树根节点的值，从而确定了左子树的索引边界，进而确定右子树的索引边界，递归构造左右子树即可

![](./images/leetcode-0889-img1.png)

# 代码

```php

class LeetCode0889 {

    public $map;        //存储后序遍历中值到索引的映射

    function constructFromPrePost($pre, $post) {
        foreach ($post as $k => $v) {
            $this->map[$v] = $k;
        }
        return $this->_build($pre, 0, count($pre) - 1, $post, 0, count($post) - 1);
    }

    protected function _build($pre, $preStart, $preEnd, $post, $postStart, $postEnd) {
        if ($preStart > $preEnd) {
            return null;
        }
        if ($preStart == $preEnd) {
            return new TreeNode($pre[$preStart]);
        }
        
        // root 的值就是前序数组的第一个元素
        $rootVal = $pre[$preStart];
        
        // root.left  的值就是前序数组的第二个元素
        // 通过前序和后序遍历构造二叉树的关键在于通过左子树的根节点
        // 确定 preorder 和 postorder 中左右子树的元素区间
        $leftRootVal = $pre[$preStart + 1];
        // leftRootVal 在后续数组中的索引
        $index = $this->map[$leftRootVal];
        // 左子树的元素个数
        $leftSize = $index - $postStart + 1;
        
        $root = new TreeNode($rootVal);
        
        // 递归构造左右子树
        $root->left = $this->_build($pre, $preStart + 1, $preStart + $leftSize, $post, $postStart, $index);
        $root->right = $this->_build($pre, $preStart + $leftSize + 1, $preEnd, $post, $index + 1, $postEnd - 1);
        
        return $root;
    }

}
```
