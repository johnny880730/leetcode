# 437. 路径总和III

# 题目
给定一个二叉树的根节点 root ，和一个整数 targetSum ，求该二叉树里节点值之和等于 targetSum 的 路径 的数目。

路径 不需要从根节点开始，也不需要在叶子节点结束，但是路径方向必须是向下的（只能从父节点到子节点）。

https://leetcode.cn/problems/path-sum-iii

提示：
- 二叉树的节点个数的范围是 [0,1000]
- -10^9 <= Node.val <= 10^9
- -1000 <= targetSum <= 1000
# 示例
```
输入：root = [10,5,-3,3,2,null,11,3,-2,null,1], targetSum = 8
输出：3
解释：和等于 8 的路径有 3 条，分别为：[5,3]、[5,2,1]、[-3, 11]。
```
```
输入：root = [5,4,8,11,null,13,4,7,2,null,null,5,1], targetSum = 22
输出：3
```


# 解析

## DFS + 前缀和

定义节点的前缀和为：由根结点到当前结点的路径上所有节点的和。利用先序遍历二叉树，记录下根节点 root 到当前节点 p 的路径上除当前节点以外所有节点的前缀和，
在已保存的路径前缀和中查找是否存在前缀和刚好等于当前节点到根节点的前缀和 cur 减去 targetSum。

对于空路径也需要保存预先处理一下，此时因为空路径不经过任何节点，因此它的前缀和为 0。

假设根节点为 root，当前刚好访问节点 node，则此时从根节点 root 到节点 node 的路径（无重复节点）刚好为 
root -> p1 -> p2 -> ... -> pk -> node，此时我们可以已经保存了节点 p1, p2, p3 ... pk 的前缀和，并且计算出了节点 node 的前缀和。

假设当前从根节点 root 到节点 node 的前缀和为 cur，则此时在已保存的前缀和查找是否存在前缀和刚好等于 cur - targetSum。
假设从根节点 root 到节点 node 的路径中存在节点 pi 到根节点 root 的前缀和为 cur-targetSum，则节点 pi+1到 node 的路径上所有节点的和一定为 targetSum。

利用深度搜索遍历树，当退出当前节点时，需要及时更新已经保存的前缀和。

# 代码

### php
```php

class LeetCode0437 {

    function pathSum($root, $targetSum) {
        $prefix = [0 => 1];
        return $this->_dfs($root, $prefix, 0, $targetSum);
    }

    protected function _dfs($node, &$prefix, $cur, $targetSum) {
        if (!$node) {
            return 0;
        }
        $ret = 0;
        $cur += $node->val;
        $ret += $prefix[$cur - $targetSum] ?? 0;
        $prefix[$cur] += 1;
        $ret += $this->_dfs($node->left, $prefix, $cur, $targetSum);
        $ret += $this->_dfs($node->right, $prefix, $cur, $targetSum);
        $prefix[$cur] -= 1;

        return $ret;
    }

}
```

### go
```go
var prefix map[int]int

func pathSum(root *TreeNode, targetSum int) int {
    prefix = make(map[int]int)
    prefix[0] = 1;

    return _dfs(root, prefix, 0, targetSum)
}

func _dfs(node *TreeNode, prefix map[int]int, cur int, targetSum int) int {
    if node == nil {
        return 0;
    }
    res := 0;
    cur += node.Val
    prefixVal, ok := prefix[cur - targetSum]; if ok {
        res += prefixVal
    }
    _, ok = prefix[cur]; if ok {
        prefix[cur] += 1
    } else {
        prefix[cur] = 1
    }

    res += _dfs(node.Left, prefix, cur, targetSum)
    res += _dfs(node.Right, prefix, cur, targetSum)

    prefix[cur] -= 1

    return res
}
```
