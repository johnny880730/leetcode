# 0199. 二叉树的右视图

# 题目

给定一个二叉树的 根节点 root，想象自己站在它的右侧，按照从顶部到底部的顺序，返回从右侧所能看到的节点值。

https://leetcode.cn/problems/binary-tree-right-side-view/description/

提示:
- 二叉树的节点个数的范围是 [0, 100]
- -100 <= Node.val <= 100


# 示例

```
输入: [1,2,3,null,5,null,4]
输出: [1,3,4]
```

```
输入: [1,null,3]
输出: [1,3]
```

```
输入: []
输出: []
```

# 解析

## BFS

可以对二叉树进行层次遍历，那么对于每层来说，最右边的结点一定是最后被遍历到的。二叉树的层次遍历可以用广度优先搜索实现。

执行广度优先搜索，左结点排在右结点之前，这样，对每一层都从左到右访问。因此，只保留每个深度最后访问的结点，就可以在遍历完整棵树后得到每个深度最右的结点。

# 代码

### php
```php
class LeetCode0199 {

    function rightSideView($root) {
        $res = [];
        if (!$root) {
            return $res;
        }
        $queue = new SplQueue();
        $queue->enqueue($root);
        while ($count = $queue->count();) {
            for ($i = 1; $i <= $count; $i++) {
                $cur = $queue->dequeue();
                if ($i == $count) {
                    $res[] = $cur->val;
                }
                $cur->left != null && $queue->enqueue($cur->left);
                $cur->right != null && $queue->enqueue($cur->right);
            }
        }
        return $res;
    }

}
```

### java
```java
class LeetCode0199 {

    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);                      // offer() 从尾部插入
        while (queue.isEmpty() == false) {
            int cnt = queue.size();
            for (int i = 1; i <= cnt; i++) {
                TreeNode cur = queue.poll();    // poll() 从头部取出
                if (i == cnt) {
                    res.add(cur.val);
                }
                if (cur.left != null) {
                    queue.offer(cur.left);
                }
                if (cur.right != null) {
                    queue.offer(cur.right);
                }
            }
        }

        return res;
    }
}
```

### go
```go
func rightSideView(root *TreeNode) []int {
    res := []int{}
    if root == nil {
        return res
    }
    queue := make([]*TreeNode, 0)
    queue = append(queue, root)
    for len(queue) > 0 {
        cnt := len(queue)
        for i := 1; i <= cnt; i++ {
            cur := queue[0];
            queue = queue[1:]
            if i == cnt {
                res = append(res, cur.Val)
            }
            if cur.Left != nil {
                queue = append(queue, cur.Left)
            }
            if cur.Right != nil {
                queue = append(queue, cur.Right)
            }
        }
    }
    return res
}
```
