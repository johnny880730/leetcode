# 450. 删除二叉搜索树中的节点

# 题目
给定一个二叉搜索树的根节点 root 和一个值 key，删除二叉搜索树中的 key 对应的节点，并保证二叉搜索树的性质不变。

返回二叉搜索树（有可能被更新）的根节点的引用。

一般来说，删除节点可分为两个步骤：
1. 首先找到需要删除的节点；
2. 如果找到了，删除它。

https://leetcode.cn/problems/delete-node-in-a-bst

提示:
- 节点数的范围 [0, 10^4].
- -10^5 <= Node.val <= 10^5
- 节点值唯一
- root 是合法的二叉搜索树
- -10^5 <= key <= 10^5

# 示例
```
输入：root = [5,3,6,2,4,null,7], key = 3
输出：[5,4,6,2,null,null,7]
解释：给定需要删除的节点值是 3，所以我们首先找到 3 这个节点，然后删除它。
一个正确的答案是 [5,4,6,2,null,null,7], 如下图所示。
另一个正确答案是 [5,2,6,null,4,null,7]。
```
```
输入: root = [5,3,6,2,4,null,7], key = 0
输出: [5,3,6,2,4,null,7]
解释: 二叉树不包含值为 0 的节点
```
```
输入: root = [], key = 0
输出: []
```



# 解析
确定递归函数的参数和返回值：通过递归函数的返回值删除节点。

确定递归终止条件：遇到空节点就返回，也说明没找到删除的节点，遍历到空节点后直接返回了。

确定单层递归的逻辑：在平衡二叉树中删除节点可能遇到的情况有以下五种：
- 第一种情况：没找到删除的节点，遍历到空节点直接返回
- 找到了删除的节点。
   - 第二种情况：左右孩子都为空（叶子节点），直接删除节点，返回 null
   - 第三种情况：左孩子为空，右孩子不为空，删除节点，右孩子补位，返回右孩子为根节点
   - 第四种情况：右孩子为空，左孩子不为空，删除节点，左孩子补位，返回左孩子为根节点
   - 第五种情况：**左右孩子都不为空，将删除节点的左子树的头节点放到删除节点的右子树的最左面节点的左孩子上，返回删除节点的右孩子为新的根节点。**

第五种情况有点难以理解，请看下图的例子，其中删除的是节点 7：

![](./images/leetcode-0450-题解.png)

图里，删除的是节点 7。它左子树的头节点为节点 5，右子树的最左面节点是节点 8。

将删除节点的左子树的头节点（节点 5）放到删除节点的右子树的最左面节点的左孩子（节点 8）上的过程见图（中图），最后删除节点 7（右图）。
这样就实现了删除节点 7 的逻辑。

# 代码

### php
```php
class LeetCode0450 {

    public function deleteNode($root, $key) {
        // 情况一：没找到删除的节点，遍历到空节点就直接返回了
        if ($root == null) {
            return $root;
        }
        if ($root->val == $key) {
            if ($root->left == null && $root->right == null) {
                // 情况二：左右孩子都为空，直接删除节点，返回 null 为根节点
                return null;
            } else if ($root->left == null) {
                // 情况三：左孩子为空，右孩子不为空，删除节点，右孩子补位，返回右孩子为根节点
                return $root->right;
            } else if ($root->right == null) {
                // 情况四：右孩子为空，左孩子不为空，删除节点，左孩子补位，返回左孩子为根节点
                return $root->left;
            } else {
                // 情况五：左右孩子都不为空，将删除 节点的左子树放到删除节点的右孩子的最左面节点的左孩子的位置
                // 返回删除节点的右孩子为新的根节点
                
                // 查找右子树最左面的节点
                $cur = $root->right;        
                while ($cur->left != null) {
                    $cur = $cur->left;
                }
                // 把要删除的节点（root）的左子树放在 cur 的左孩子的位置
                $cur->left = $root->left;
                // 此时的情况有点像情况三【左为空+右不为空】的情况，返回右孩子
                return $root->right;
            }
        }
        if ($root->val > $key) {
            $root->left = $this->deleteNode($root->left, $key);
        }
        if ($root->val < $key) {
            $root->right = $this->deleteNode($root->right, $key);
        }
        return $root;
    }
    
}
```

### go
```go
func deleteNode(root *TreeNode, key int) *TreeNode {
    if root == nil {
        return nil
    }
    if key == root.Val {
        if root.Left == nil && root.Right == nil {
            return nil
        } else if root.Left == nil {
            return root.Right
        } else if root.Right == nil {
            return root.Left
        } else {
            cur := root.Right
            for cur.Left != nil {
                cur = cur.Left
            }
            cur.Left = root.Left
            return root.Right
        }
    }

    if key < root.Val {
        root.Left = deleteNode(root.Left, key)
    }
    if key > root.Val {
        root.Right = deleteNode(root.Right, key)
    }

    return root
}
```