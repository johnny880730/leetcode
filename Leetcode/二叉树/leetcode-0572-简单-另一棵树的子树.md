# 0572. 另一棵树的子树

# 题目
给你两棵二叉树 root 和 subRoot 。检验 root 中是否包含和 subRoot 具有相同结构和节点值的子树。如果存在，返回 true ；否则，返回 false 。

二叉树 tree 的一棵子树包括 tree 的某个节点和这个节点的所有后代节点。tree 也可以看做它自身的一棵子树。

提示：
- root 树上的节点数量范围是 [1, 2000]
- subRoot 树上的节点数量范围是 [1, 1000]
- -10^4 <= root.val <= 10^4
- -10^4 <= subRoot.val <= 10^4

https://leetcode.cn/problems/subtree-of-another-tree/

# 示例
```
输入：root = [3,4,5,1,2], subRoot = [4,1,2]
输出：true
```
```
输入：root = [3,4,5,1,2,null,null,null,null,0], subRoot = [4,1,2]
输出：false
```

# 解析

## DFS
深度优先搜索枚举 s 中的每一个节点，判断这个点的子树是否和 t 相等。

判断两个树是否相等的三个条件（是「与」的关系）：
- 当前两个数的根节点值相等
- 并且，s 的左子树和 t 的左子树相等
- 并且，s 的右子树和 t 的右子树相等

判断 t 是否为 s 的子树也有三个条件（是「或」的关系）：
- 当前两棵树相等
- 或者，t 是 s 的左子树
- 或者，t 是 s 的右子树


需要做一次深度优先搜索来检查，即让两个指针一开始先指向该节点和 t 的根，
然后同步移动两根指针来同步遍历这两棵树，判断对应位置是否相等。



# 代码

### php
```php
class LeetcodeXXXX {
    
    /**
     * @param TreeNode $root
     * @param TreeNode $subRoot
     * @return Boolean
     */
    function isSubtree($root, $subRoot) {
        if (!$root && !$subRoot) {
            return true;
        }
        if (!$root || !$subRoot) {
            return false;
        }
        // 判断是否子树，【或】的关系
        return $this->_check($root, $subRoot) || $this->isSubtree($root->left, $subRoot) || $this->isSubtree($root->right, $subRoot);
    }

    // 判断两个树是否相等的三个条件，「与」的关系
    function _check($s, $t) {
        if ($s == null && $t == null) {
            return true;
        }
        if ($s == null || $t == null || $s->val != $t->val) {
            return false;
        }
        return $this->_check($s->left, $t->left) && $this->_check($s->right, $t->right);
    }
}
```

### java
```java
class LeetCode0572 {
    
    public boolean isSubtree(TreeNode root, TreeNode subRoot) {
        if (root == null && subRoot == null) {
            return true;
        }
        if (root == null || subRoot == null) {
            return false;
        }
        // 判断是否子树，【或】的关系
        return _check(root, subRoot) || isSubtree(root.left, subRoot) || isSubtree(root.right, subRoot);
    }

    // 判断两个树是否相等的三个条件，「与」的关系
    private boolean _check(TreeNode s, TreeNode t) {
        if (s == null && t == null) {
            return true;
        }
        if (s == null || t == null || s.val != t.val) {
            return false;
        }
        return _check(s.left, t.left) && _check(s.right, t.right);
    }
}
```

### go
```go
func isSubtree(root *TreeNode, subRoot *TreeNode) bool {
	if root == nil && subRoot == nil {
		return true
    }
    if root == nil || subRoot == nil {
        return false
    }
    // 判断是否子树，【或】的关系
    return _check(root, subRoot) || isSubtree(root.Left, subRoot) || isSubtree(root.Right, subRoot)
    
}

// 判断两个树是否相等的三个条件，「与」的关系
func _check(s *TreeNode, t *TreeNode) bool {
    if s == nil && t == nil {
        return true
    }
    if s == nil || t == nil || s.Val != t.Val {
        return false
    }
    return _check(s.Left, t.Left) && _check(s.Right, t.Right)
}
```
