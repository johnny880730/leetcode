# 0114. 二叉树展开为链表

# 题目
给你二叉树的根结点 root ，请你将它展开为一个单链表：
- 展开后的单链表应该同样使用 TreeNode ，其中 right 子指针指向链表中下一个结点，而左子指针始终为 null 。
- 展开后的单链表应该与二叉树 先序遍历 顺序相同。

https://leetcode.cn/problems/flatten-binary-tree-to-linked-list

提示：
- 树中结点数在范围 [0, 2000] 内
- -100 <= Node.val <= 100

# 示例
```
输入：root = [1,2,5,3,4,null,6]
输出：[1,null,2,null,3,null,4,null,5,null,6]
```

# 解析
这里要看下题目给出的函数签名：

```java
void flatten(TreeNode root);
```

注意该函数的签名，返回类型是 void，也就是说题目希望是原地把二叉树给拉平成链表，所以无法通过简单的二叉树遍历来解决了


## 分解问题的思想
尝试给出题目提供的 flatten 函数的定义：

```java
// 定义：输入节点 root，然后 root 为根的二叉树就会被拉平成一根链表
void flatten(TreeNode root);
```

有了这个函数定义，如何按要求拉成一个链表呢？

对于一个节点 x，可以执行以下流程：
1. 先利用 flatten(x.left) 和 flatten(x.right) 将 x 左右子树拉平
2. 将 x 的右子树接到左子树下方，然后将整个左子树作为右子树

图示如下：

![](./images/leetcode-0114-img1.png)

这样，以 x 为根的整棵二叉树就被拉平了，也完成了 flatten() 函数的定义


# 代码

### php
```php
class LeetCode0114 {

    function flatten($root) {
        if ($root == null) {
            return;
        }

        // 利用定义 把左右子树拉平
        $this->flatten($root->left);
        $this->flatten($root->right);

        // 后序位置
        // 1. 左右子树已经被拉平成一条链表
        $left = $root->left;
        $right = $root->right;

        // 2. 将左子树作为右子树
        $root->left = null;
        $root->right = $left;

        // 3. 将原先的右子树接到当前右子树的末端
        $p = $root;
        while ($p->right != null) {
            $p = $p->right;
        }
        $p->right = $right;
    }
}
```

### go
```go
func flatten(root *TreeNode)  {
    if root == nil {
        return
    }
    flatten(root.Left)
    flatten(root.Right)

    left := root.Left
    right := root.Right

    root.Left = nil
    root.Right = left

    p := root
    for p.Right != nil {
        p = p.Right
    }
    p.Right = right
}
```

### java
```java
class LeetCode0114 {

    public void flatten(TreeNode root) {
        if (root == null) {
            return;
        }
        flatten(root.left);
        flatten(root.right);

        TreeNode left = root.left;
        TreeNode right = root.right;

        root.left = null;
        root.right = left;

        TreeNode p = root;
        while (p.right != null) {
            p = p.right;
        }
        p.right = right;
    }
    
}
```
