# 0222. 完全二叉树的节点个数

# 题目
给你一棵 完全二叉树 的根节点 root ，求出该树的节点个数。

完全二叉树 的定义如下：在完全二叉树中，除了最底层节点可能没填满外，其余每层节点数都达到最大值，并且最下面一层的节点都集中在该层最左边的若干位置。
若最底层为第 h 层，则该层包含 1~ 2^h 个节点。

https://leetcode.cn/problems/count-complete-tree-nodes/description/

提示：
- 树中节点的数目范围是[0, 5 * 10000]
- 0 <= Node.val <= 5 * 10000
- 题目数据保证输入的树是 完全二叉树

# 示例
```
输入：root = [1,2,3,4,5,6]
输出：6
```
```
输入：root = []
输出：0
```
```
输入：root = [1]
输出：1
```

# 解析
在完全二叉树中，除了最底层节点可能没填满外，其余每层节点数都达到最大值，并且最下面一层的节点都集中在该层最左边的若干位置。

若最底层为第 h 层，则该层包含 1 ~ 2 ^ (h - 1)  个节点。

完全二叉树只有两种情况
- 情况一：就是满二叉树
- 情况二：最后一层叶子节点没有满

对于情况一，可以直接用 2 ^ 树深度 - 1 来计算，注意这里根节点深度为 1。

对于情况二，分别递归左孩子，和右孩子，递归到某一深度一定会有左孩子或者右孩子为满二叉树，然后依然可以按照情况 1 来计算。

如果整个树不是满二叉树，就递归其左右孩子，直到遇到满二叉树为止，用公式计算这个子树（满二叉树）的节点数量。

这里关键在于如何去判断一个左子树或者右子树是不是满二叉树呢？

在完全二叉树中，如果递归向左遍历的深度等于递归向右遍历的深度，那说明就是满二叉树


# 代码

### php
```php
class LeetCode0222 {

    public function countNodes($root) {
        $left = $root;
        $right = $root;
        $leftHeight = $rightHeight = 0;
        while ($left) {
            $left = $left->left;
            $leftHeight++;
        }
        while ($right) {
            $right = $right->right;
            $rightHeight++;
        }
        if ($leftHeight == $rightHeight) {
            // 左右相等 是一个满二叉树
            return pow(2, $leftHeight) - 1;
        }
        return 1 + $this->countNodes($root->left) + $this->countNodes($root->right);
    }

}
```

### go
```go
func countNodes(root *TreeNode) int {
    if root == nil {
        return 0
    }
    left, right := root.Left, root.Right
    leftHeight, rightHeight := 0, 0
    for left != nil {
        left = left.Left
        leftHeight++
    }
    for right != nil {
        right = right.Right
        rightHeight++
    }
    if leftHeight == rightHeight {
        return (2 << leftHeight) - 1
    }
    
    return 1 + countNodes(root.Left) + countNodes(root.Right)
}
```

### java
```java
class LeetCode0222 {

    public int countNodes(TreeNode root) {
        if (root == null) {
            return 0;
        }
        TreeNode left = root, right = root;
        int leftHeight = 0, rightHeight = 0;
        while (left != null) {
            left = left.left;
            leftHeight++;
        }
        while (right != null) {
            right = right.right;
            rightHeight++;
        }
        if (leftHeight == rightHeight) {
            return (1 << leftHeight) - 1;
        }
        return 1 + countNodes(root.left) + countNodes(root.right);
    }
    
}
```