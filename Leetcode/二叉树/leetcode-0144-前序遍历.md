# 144. 二叉树的前序遍历

# 题目
给定一个二叉树的根节点 root ，返回它的 前序 遍历。

https://leetcode.cn/problems/binary-tree-preorder-traversal

# 示例：
```
输入：root = [1,null,2,3]
输出：[1,2,3]
```

```
输入：root = []
输出：[]
```

```
输入：root = [1]
输出：[1]
```

```
输入：root = [1,2]
输出：[1,2]
```

```
输入：root = [1,null,2]
输出：[1,2]
```

# 解析
前序遍历是中、左、右，每次先处理中间节点，先将根节点加入栈，然后将右孩子加入栈，最后将左孩子加入栈。

为何要先将右孩子加入栈再将左孩子加入栈呢？因为这样出栈的顺序才是中、左、右。注意空节点不入栈。

# 代码

### php
```php
class LeetCode0144 {

    // 递归
    function preorderTraversal($root) {
        $res = [];
        $this->preorder($root, $res);
        return $res;
    }

    function preorder($root, &$res) {
        if (!$root) {
            return;
        }
        $res[] = $root->val;
        $this->preorder($root->left, $res);
        $this->preorder($root->right, $res);

    }

    // 非递归，顺序为 根、右、左
    function preorderUnRes($root) {
        $res = [];
        $stack = new SplStack();
        if ($root == null) {
            return $res;
        }
        $stack->push($root);
        while ($stack->isEmpty() == false) {
            $node = $stack->pop();
            $res[] = $node->val;
            if ($node->right) $stack->push($node->right);
            if ($node->left) $stack->push($node->left);
        }
        return $res;
    }
}
```

### go
```go
func preorderTraversal(root *TreeNode) []int {
    res := make([]int, 0)
	var _preOrder func(root *TreeNode)
	_preOrder = func(root *TreeNode) {
	    if root == nil {
		    return
        }
		res = append(res, root.Val)
		_preOrder(root.Left)
		_preOrder(root.Right)
    }
	_preOrder(root)
	return res
}
```