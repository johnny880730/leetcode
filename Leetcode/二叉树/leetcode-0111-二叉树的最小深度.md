# 111. 二叉树的最小深度

# 题目
给定一个二叉树，找出其最小深度。

最小深度是从根节点到最近叶子节点的最短路径上的节点数量。

说明:叶子节点是指没有子节点的节点。

https://leetcode.cn/problems/minimum-depth-of-binary-tree

# 示例:
```
给定二叉树[3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7

返回它的最小深度 2.
```


# 解析
因为要比较递归返回的结果，遍历顺序是后序遍历。

注意：最小深度是从根节点到最近叶子节点的最短路径上的节点数量。左右孩子都为空的节点才是叶子节点。

最大深度很容易理解，而最小深度容易有一个误区，如下图：

![](./images/leetcode-0111-题解.png)

在递归里确定单层递归逻辑的时候，可能会写出如下代码
```
leftDepth = getDepth(node->left);
rightDepth = getDepth(node->right);
return 1 + min(leftDepth, rightDepth);
```

上述代码计算的最小深度出了错误，如果这么求解，那么没有左孩子的分支会作为最小深度。因此结论如下：
- 如果左子树为空，右子树不为空，则说明最小深度是右子树的深度 +1。
- 如果右子树为空，左子树不为空，则最小深度是左子树的深度 +1。
- 如果左右子树都不为空，那么返回左右子树深度的最小值 +1。

# 代码

### php
```php
class LeetCode0111 {

    // DFS
    function minDepth($root) {
        if ($root == null)
            return 0;

        if (!$root->left)
            return 1 + $this->minDepth($root->right);
        if (!$root->right)
            return 1 + $this->minDepth($root->left);

        $leftMinDepth = $this->minDepth($root->left);
        $rightMinDepth = $this->minDepth($root->right);
        return 1 + min($leftMinDepth, $rightMinDepth);
    }

    // BFS
    function minDepth2($root) {
        if ($root == null) {
            return 0;
        }
        $queue = new SplQueue();
        $queue->enqueue([$root, 1]);        //1=初始深度
        while (!$queue->isEmpty()) {
            list($node, $depth) = $queue->dequeue();
            if ($node->left == null && $node->right == null) {
                return $depth;
            }
            if ($node->left != null) {
                $queue->enqueue([$node->left, $depth + 1]);
            }
            if ($node->right != null) {
                $queue->enqueue([$node->right, $depth + 1]);
            }
        }
        return 0;
    }
}
```

### go
```go
func minDepth(root *TreeNode) int {
	if root == nil {
		return 0
	}
	if root.Left == nil {
		return 1 + minDepth(root.Right)
	}
	if root.Right == nil {
		return 1 + minDepth(root.Left)
	}
	left := minDepth(root.Left)
	right := minDepth(root.Right)
	return 1 + min(left, right)
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
```