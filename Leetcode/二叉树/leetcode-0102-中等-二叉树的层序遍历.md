# 0102. 二叉树的层序遍历【中等】

# 题目
给你一个二叉树，请你返回其按 层序遍历 得到的节点值。 （即逐层地，从左到右访问所有节点）。

https://leetcode.cn/problems/binary-tree-level-order-traversal

提示：
- 树中节点数目在范围 [0, 2000] 内
- -1000 <= Node.val <= 1000

# 示例
```
二叉树：[3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7
返回其层次遍历结果：

[
  [3],
  [9,20],
  [15,7]
]
```

# 解析
层序遍历借用一个辅助数据结构【队列】来实现，队列先进先出，符合一层一层遍历的逻辑，而使用栈先进后出适合模拟深度优先遍历，也就是递归的逻辑。

# 代码

### php
```php

class LeetCode0102 {

    // 用队列实现 BFS
    function levelOrder($root) {
        $res = [];
        if ($root == null) {
            return $res;
        }

        $queue = new SplQueue();
        $queue->enqueue($root);
        while (!$queue->isEmpty()) {
            $size = $queue->count();
            $levelRes = [];
            for ($i = 0; $i < $size; $i++) {
                $node = $queue->dequeue();
                $levelRes[] = $node->val;
                if ($node->left) {
                    $queue->enqueue($node->left);
                }
                if ($node->right) {
                    $queue->enqueue($node->right);
                }
            }
            $res[] = $levelRes;
        }
        return $res;
    }
}
```

### go
```go
func levelOrder(root *TreeNode) [][]int {
    var res [][]int
    if root == nil {
        return res
    }
    
    var _levelOrder func(node *TreeNode)
    _levelOrder = func(node *TreeNode) {
        queue := []*TreeNode{}
        queue = append(queue, root)
        for len(queue) > 0 {
            count := len(queue)
            levelRes := []int{}
            for i := 0; i < count; i++ {
                cur := queue[0]
                queue = queue[1:]
                levelRes = append(levelRes, cur.Val)
                if cur.Left != nil {
                    queue = append(queue, cur.Left)
                }
                if cur.Right != nil {
                    queue = append(queue, cur.Right)
                }
            }
            res = append(res, levelRes)
        }
    }
    
    _levelOrder(root)
    return res
}
```

### java
```java
class LeetCode0102 {

    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            List<Integer> level = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                TreeNode cur = queue.poll();
                level.add(cur.val);
                if (cur.left != null) {
                    queue.add(cur.left);
                }
                if (cur.right != null) {
                    queue.add(cur.right);
                }
            }
            res.add(level);
        }
        return res;
    }
}
```
