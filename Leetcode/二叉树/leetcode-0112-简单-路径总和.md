# 0112. 路径总和

# 题目

给定一个二叉树和一个目标和，判断该树中是否存在根节点到叶子节点的路径，这条路径上所有节点值相加等于目标和。

说明:叶子节点是指没有子节点的节点。

https://leetcode.cn/problems/path-sum/description/

# 示例:
```
给定如下二叉树，以及目标和 sum = 22，

              5
             / \
            4   8
           /   / \
          11  13  4
         /  \      \
        7    2      1
        
返回 true, 因为存在目标和为 22 的根节点到叶子节点的路径 5->4->11->2。
```

# 解析

## DFS
可以使用深度优先遍历的模式（使用前、中、后序遍历都可以，因为中间节点没有需要处理的逻辑）来遍历二叉树

确定递归的终止条件：如何统计这一条路径的和呢？不要累加节点的数值然后判断是否等于目标和，那样代码写起来比较麻烦，
可以将计数器 sum 初始化为目标和，然后每次减去遍历路径节点上的数值。如果最后 sum == 0，同时遍历到了叶子节点，说明得到了目标和。
如果遍历到了叶子节点，且 sum != 0，那么就是没找到等于目标和的路径。

递归的过程中不要让空节点进入递归函数。注意这里还是要包含回溯逻辑的。

# 代码

### php
```php
class LeetCode0112 {

    function hasPathSum($root, $sum) {
        if ($root == null) {
            return false;
        }
        if ($root->left == null && $root->right == null) {
            return $sum == $root->val;
        }
        return $this->hasPathSum($root->left, $sum - $root->val) || $this->hasPathSum($root->right, $sum - $root->val);
    }
    
}
```

### go
```go
func hasPathSum(root *TreeNode, targetSum int) bool {
	if root == nil {
		return false
	}
    if root.Left == nil && root.Right == nil {
        return targetSum == root.Val
    }

    return hasPathSum(root.Left, targetSum - root.Val) || hasPathSum(root.Right, targetSum - root.Val)
}
```

### java
```java
class LeetCode0112 {

    public boolean hasPathSum(TreeNode root, int targetSum) {
        if (root == null) {
            return false;
        }
        if (root.left == null && root.right == null) {
            return targetSum == root.val;
        }

        return hasPathSum(root.left, targetSum - root.val) || hasPathSum(root.right, targetSum - root.val);
    }
    
}
```
