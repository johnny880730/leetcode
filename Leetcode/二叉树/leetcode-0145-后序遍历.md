# 145. 二叉树的后序遍历

# 题目 
给定一个二叉树的根节点 root ，返回它的 后序 遍历。

https://leetcode.cn/problems/binary-tree-postorder-traversal

# 示例：
```
输入：root = [1,null,2,3]
输出：[3,2,1]
```

```
输入：root = []
输出：[]
```

```
输入：root = [1]
输出：[1]
```

提示：
- 树中节点的数目在范围 [0, 100] 内
- -100 <= Node.val <= 100

# 解析
后序遍历的顺序是左、右、中，只需要调整前序遍历的代码顺序，变成中、右、左的遍历顺序，然后反转结果数组，数据结果的顺序就是左右中了。

# 代码

### php
```php
class LeetCode0145 {

    // 递归
    function postorderTraversal($root) {
        $res = [];
        $this->postorder($root, $res);
        return $res;
    }

    function postorder($root, &$res) {
        if (!$root) {
            return;
        }
        $this->postorder($root->left, $res);
        $this->postorder($root->right, $res);
        $res[] = $root->val;
    }

    // 非递归
    function postorderUnRes($root) {
        $res = [];
        $stack = new SplStack();
        if ($root == null) return $res;
        $stack->push($root);
        while ($stack->isEmpty() == false) {
            $node = $stack->pop();
            $res[] = $node->val;
            if ($node->left) $stack->push($node->left);
            if ($node->right) $stack->push($node->right);
        }
        return array_reverse($res);
    }
}
```

### go
```go
func postorderTraversal(root *TreeNode) []int {
    res := make([]int, 0)
    var _postOrder func(root *TreeNode)
    _postOrder = func(root *TreeNode) {
        if root == nil {
            return
        }
        _postOrder(root.Left)
        _postOrder(root.Right)
        res = append(res, root.Val)
    }
    _postOrder(root)
    return res
}
```
