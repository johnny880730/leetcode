# 538. 把二叉搜索树转换为累加树

# 题目
给出二叉搜索树的根节点，该树的节点值各不相同，请你将其转换为累加树（Greater Sum Tree），
使每个节点 node 的新值等于原树中大于或等于 node.val 的值之和。

提醒一下，二叉搜索树满足下列约束条件：
- 节点的左子树仅包含键 小于 节点键的节点。
- 节点的右子树仅包含键 大于 节点键的节点。
- 左右子树也必须是二叉搜索树。

https://leetcode.cn/problems/convert-bst-to-greater-tree

提示：
- 树中的节点数介于 0 和 10^4 之间。
- 每个节点的值介于 -10^4 和 10^4 之间。
- 树中的所有值 互不相同 。
- 给定的树为二叉搜索树。

# 示例
```
输入：[4,1,6,0,2,5,7,null,null,null,3,null,null,null,8]
输出：[30,36,21,36,35,26,15,null,null,null,33,null,null,null,8]
```
```
输入：root = [0,null,1]
输出：[1,null,1]
```
```
输入：root = [1,0,2]
输出：[3,3,2]
```
```
输入：root = [3,2,4,1]
输出：[7,9,4,10]
```

# 解析
这是一棵二叉搜索树，是有序的啊。那么有序的元素如何求累加呢？

换一个角度来看，这就是一个有序数组[2, 5, 13]，求从后到前的累加数组，也就是[20, 18, 13]，是不是感觉这就简单了。

为什么变成数组就是感觉简单了呢？

因为数组大家都知道怎么遍历啊，从后向前，挨个累加就完事了，这换成了二叉搜索树，看起来就别扭了一些是不是。

那么知道如何遍历这个二叉树，也就迎刃而解了，从树中可以看出累加的顺序是 **右中左** ，所以我们需要反中序遍历这个二叉树，
中节点的处理逻辑就是让 cur 的数值加上前一个节点的数值就可以了。

# 代码

### php
```php
class LeetCode0538 {
    
    protected $pre = 0;
    
    public function convertBST($root) {
        $this->_traversal($root);
        return $root;
    }
    
    protected function _traversal($cur) {
        if (!$cur) {
            return;
        }
        $this->_traversal($cur->right);
        
        $cur->val += $this->pre;
        $this->pre = $cur->val;
        
        $this->_traversal($cur->left);
    }

}
```

### java
```java
class LeetCode0538 {

    public int pre = 0;

    public TreeNode convertBST(TreeNode root) {
        _dfs(root);
        return root;
    }

    private void _dfs(TreeNode node) {
        if (node == null) {
            return;
        }
        _dfs(node.right);
        node.val += this.pre;
        this.pre = node.val;
        _dfs(node.left);
    }
}
```

### go
```go
func convertBST(root *TreeNode) *TreeNode {
	pre := 0
	var _traversal func(cur *TreeNode)
	_traversal = func(cur *TreeNode) {
		if cur == nil {
			return
		}
		// 右中左的顺序
		_traversal(cur.Right)

		cur.Val += pre
		pre = cur.Val

		_traversal(cur.Left)
	}

	_traversal(root)
	return root
}
```