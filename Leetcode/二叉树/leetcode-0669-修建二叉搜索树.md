# 669. 修剪二叉搜索树

# 题目
给你二叉搜索树的根节点 root ，同时给定最小边界low 和最大边界 high。

通过修剪二叉搜索树，使得所有节点的值在[low, high]中。

修剪树不应该改变保留在树中的元素的相对结构（即，如果没有被移除，原有的父代子代关系都应当保留）。 可以证明，存在唯一的答案。

所以结果应当返回修剪好的二叉搜索树的新的根节点。注意，根节点可能会根据给定的边界发生改变。

https://leetcode.cn/problems/trim-a-binary-search-tree


# 示例
```
输入：root = [1,0,2], low = 1, high = 2
输出：[1,null,2]
```
```
输入：root = [3,0,4,null,2,null,null,1], low = 1, high = 3
输出：[3,2,null,1]
```
```
输入：root = [1], low = 1, high = 2
输出：[1]
```
```
输入：root = [1,null,2], low = 1, high = 3
输出：[1,null,2]
```
```
输入：root = [1,null,2], low = 2, high = 4
输出：[2]
```

# 解析
确定递归函数的参数和返回值：为什么要返回值呢？因为要遍历整棵树，所以不需要返回值也可以完成修建的操作。
但是有返回值更方便，可以通过递归函数的返回值来删除节点。

确定递归终止条件：修建的操作并不是在终止条件下进行的，所以遇到空节点返回即可。

确定单层递归的逻辑：如果当前节点的元素小于 low 的数组，那么应该递归右子树，并返回右子树符合条件的头节点。
如果当前节点的元素大于 high 的数值，那么应该递归左子树，并返回左子树符合条件的头节点。接下来将下一层递归处理左子树的结果赋值给 root->left、
处理右子树的结果赋值给 root->right，最后返回 root 节点。

# 代码

### php
```php
class LeetCode0669 {

    public function trimBST($root, $low, $high) {
        if ($root == null) {
            return null;
        }
        if ($root->val < $low) {
            // 寻找符合 [low, high] 区间的节点
            $right = $this->trimBST($root->right, $low, $high);
            return $right;
        }
        if ($root->val > $high) {
            // 寻找符合 [low,high] 区间的节点
            $left = $this->trimBST($root->left, $low, $high);
            return $left;
        }
        $root->left = $this->trimBST($root->left, $low, $high);
        $root->right = $this->trimBST($root->right, $low, $high);
        return $root;
    }

}
```

### go
```go
func trimBST(root *TreeNode, low int, high int) *TreeNode {
	var _traversal func(root *TreeNode, low, high int) *TreeNode
	_traversal = func(root *TreeNode, low, high int) *TreeNode {
		if root == nil {
			return nil
		}
		if root.Val < low {
			return _traversal(root.Right, low, high)
		}
		if root.Val > high {
			return _traversal(root.Left, low, high)
		}

		root.Left = _traversal(root.Left, low, high)
		root.Right = _traversal(root.Right, low, high)
		return root
	}

	return _traversal(root, low, high)
}
```