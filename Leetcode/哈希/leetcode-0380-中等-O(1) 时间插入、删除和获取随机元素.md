# 380. O(1) 时间插入、删除和获取随机元素

# 题目
实现 RandomizedSet 类：
- RandomizedSet()： 初始化 RandomizedSet 对象
- bool insert(int val)： 当元素 val 不存在时，向集合中插入该项，并返回 true ；否则，返回 false 。
- bool remove(int val)： 当元素 val 存在时，从集合中移除该项，并返回 true ；否则，返回 false 。
- int getRandom()： 随机返回现有集合中的一项（测试用例保证调用此方法时集合中至少存在一个元素）。每个元素应该有 相同的概率 被返回。

你必须实现类的所有函数，并满足每个函数的 平均 时间复杂度为 O(1) 。

https://leetcode.cn/problems/insert-delete-getrandom-o1

提示：
- -2^31 <= val <= 2^31 - 1
- 最多调用 insert、remove 和 getRandom 函数 2 * 10^5 次
- 在调用 getRandom 方法时，数据结构中 至少存在一个 元素。


## 示例
```
输入
["RandomizedSet", "insert", "remove", "insert", "getRandom", "remove", "insert", "getRandom"]
[[], [1], [2], [2], [], [1], [2], []]
输出
[null, true, false, true, 2, true, false, 2]

解释
RandomizedSet randomizedSet = new RandomizedSet();
randomizedSet.insert(1); // 向集合中插入 1 。返回 true 表示 1 被成功地插入。
randomizedSet.remove(2); // 返回 false ，表示集合中不存在 2 。
randomizedSet.insert(2); // 向集合中插入 2 。返回 true 。集合现在包含 [1,2] 。
randomizedSet.getRandom(); // getRandom 应随机返回 1 或 2 。
randomizedSet.remove(1); // 从集合中移除 1 ，返回 true 。集合现在包含 [2] 。
randomizedSet.insert(2); // 2 已在集合中，所以返回 false 。
randomizedSet.getRandom(); // 由于 2 是集合中唯一的数字，getRandom 总是返回 2 。
```

# 解析

## 双哈希表

由于要求都是 O(1) 的时间，哈希表是最符合要求的。准备两个哈希表 map1、map2，再准备一个 size 记录个数，其中：
- map1 存储 val => index 的关系
- map2 存储 index => val 的关系

取数据的方法 getRandom() 直接生成 map2 中 0 到 index 的随机数，获取对应的 val 返回即可，这样是时间复杂度就是 O(1)。

调用 insert() 增加 val 时，去 map1 查看是否已经存在：
- 若存在直接返回 false
- 若不存在，新增 val 时，在 map1 新增一条记录，索引为 val，值就是当前的 size，然后 size++，返回结果 true。

调用 remove() 移除 val 时，去 map1 查看是否已经存在：
- 若不存在直接返回 false
- 若存在，移除 val 时先获取这个 val 对应的 index，删除 map1、map2 对应的元素。删除后可能会在 map2 中留下 “坑洞”，
  导致 getRandom() 时随机出的索引在 map2 中不存在，因此把 map2 中排在最后的元素去代替被删除的元素，让 map2 中是索引
  依旧保持连续，这样 getRandom() 方法就不会出现问题


# 代码

### php
```php
class LeetCode0380 {
    protected $val2index;       //map1
    protected $index2val;       //map2
    protected $size;

    function __construct() {
        $this->val2index = array();
        $this->index2val = array();
        $this->size = 0;
    }

    function insert($val) {
        if (array_key_exists($val, $this->val2index)) {
            return false;
        }
        $this->val2index[$val] = $this->size;
        $this->index2val[$this->size] = $val;
        $this->size++;
        return true;
    }

    function remove($val) {
        if (!array_key_exists($val, $this->val2index)) {
            return false;
        }
        $idx = $this->val2index[$val];
        unset($this->val2index[$val]);
        unset($this->index2val[$idx]);
        if ($idx != $this->size - 1) {
            $lastIdx = $this->size - 1;
            $lastVal = $this->index2val[$lastIdx];
            unset($this->index2val[$lastIdx]);
            unset($this->val2index[$lastVal]);
            $this->val2index[$lastVal] = $idx;
            $this->index2val[$idx] = $lastVal;
        }
        $this->size--;
        return true;
    }

    function getRandom() {
        $idx = mt_rand(0, $this->size - 1);
        return $this->index2val[$idx];
    }
}
```

### go 
```go
type RandomizedSet struct {
    Val2Index map[int]int
    Index2Val map[int]int
    Size int
}


func Constructor() RandomizedSet {
    return RandomizedSet{
        Val2Index: make(map[int]int),
        Index2Val: make(map[int]int),
        Size: 0,
    }
}


func (this *RandomizedSet) Insert(val int) bool {
    _, ok := this.Val2Index[val]
    if ok {
        return false
    }
    this.Val2Index[val] = this.Size
    this.Index2Val[this.Size] = val
    this.Size++
    return true
}


func (this *RandomizedSet) Remove(val int) bool {
    _, ok := this.Val2Index[val]
    if !ok {
        return false
    }
    idx := this.Val2Index[val]
    delete(this.Val2Index, val)
    delete(this.Index2Val, idx)
    if idx != this.Size - 1 {
        lastIdx := this.Size - 1
        lastVal, _ := this.Index2Val[lastIdx]
        delete(this.Val2Index, lastVal)
        delete(this.Index2Val, lastIdx)
        this.Val2Index[lastVal] = idx
        this.Index2Val[idx] = lastVal
    }
    this.Size--
    return true
}


func (this *RandomizedSet) GetRandom() int {
    idx := rand.Intn(this.Size)
    return this.Index2Val[idx]

}
```