# 387. 字符串中的第一个唯一字符

# 题目
给定一个字符串，找到它的第一个不重复的字符，并返回它的索引。如果不存在，则返回 -1。

提示：你可以假定该字符串只包含小写字母。

https://leetcode.cn/problems/first-unique-character-in-a-string/

提示：
- 1 <= s.length <= 10^5
- s 只包含小写字母

# 示例
```
s = "leetcode"
返回 0
```
```
s = "loveleetcode"
返回 2
```

# 代码

### php
```php
class LeetCode0387 {
    
    function firstUniqChar($s) {
        $hash = array_count_values(str_split($s));
        for ($i = 0; $i < strlen($s); $i++) {
            if ($hash[$s[$i]] == 1) {
                return $i;
            }
        }
        return -1;
    }
}
```

### go
```go
func firstUniqChar(s string) int {
    length := len(s)
    hash := make(map[string]int)
    for i := 0; i < length; i++ {
        hash[string(s[i])]++
    }
    for i := 0; i < length ; i++ {
        if hash[string(s[i])] == 1 {
            return i
        }
    }
    return -1
}
```