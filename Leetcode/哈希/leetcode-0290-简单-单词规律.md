# 0290. 单词规律

# 题目
给定一种规律 pattern 和一个字符串 str ，判断 str 是否遵循相同的规律。

这里的 遵循 指完全匹配，例如， pattern 里的每个字母和字符串 str 中的每个非空单词之间存在着双向连接的对应规律。

https://leetcode.cn/problems/word-pattern/

提示：
- 1 <= pattern.length <= 300
- pattern 只包含小写英文字母
- 1 <= s.length <= 3000
- s 只包含小写英文字母和 ' '
- s 不包含 任何前导或尾随对空格
- s 中每个单词都被 单个空格 分隔

# 示例
```
输入: pattern = "abba", str = "dog cat cat dog"
输出: true
```
```
输入:pattern = "abba", str = "dog cat cat fish"
输出: false
```
```
输入: pattern = "aaaa", str = "dog cat cat dog"
输出: false
```

# 思路

## 哈希
思路同 [0205. 同构字符串](./leetcode-0205-同构字符串.md)，使用两个 map 保存 s[i] 到 t[j] 和 t[j] 到 s[i] 的映射关系，如果发现对应不上，立刻返回 false

# 代码

### php
```php
class Solution290 {

    function wordPattern($pattern, $s) {
        $arr = explode(' ', $s);
        $len = count($arr);
        if (strlen($pattern) != $len) {
            return false;
        }
        $map1 = $map2 = [];
        for ($i = 0; $i < $len; $i++) {
            if (!array_key_exists($pattern[$i], $map1)) {
                $map1[$pattern[$i]] = $arr[$i];
            }
            if (!array_key_exists($arr[$i], $map2)) {
                $map2[$arr[$i]] = $pattern[$i];
            }
            if ($map1[$pattern[$i]] != $arr[$i] || $map2[$arr[$i]] != $pattern[$i]) {
                return false;
            }
        }

        return true;
    }

}
```

### go
```go
func wordPattern(pattern string, s string) bool {
    arr := strings.Split(s, " ")
    length := len(arr)
    if length != len(pattern) {
        return false
    }
    map1 := make(map[byte]string)
    map2 := make(map[string]byte)
    for i := 0; i < length; i++ {
        if _, ok := map1[pattern[i]]; !ok {
            map1[pattern[i]] = arr[i]
        }
        if _, ok := map2[arr[i]]; !ok {
            map2[arr[i]] = pattern[i]
        }
        if map1[pattern[i]] != arr[i] || map2[arr[i]] != pattern[i] {
            return false
        }
    }
    
    return true
}
```

### java
```java
class LeetCode0290 {

    public boolean wordPattern(String pattern, String s) {
        String[] arr = s.split(" ");
        int length = arr.length;
        if (length != pattern.length()) {
            return false;
        }
        Map<Character, String> map1 = new HashMap<>();
        Map<String, Character> map2 = new HashMap<>();
        for (int i = 0; i < length; i++) {
            if (!map1.containsKey(pattern.charAt(i))) {
                map1.put(pattern.charAt(i), arr[i]);
            }
            if (!map2.containsKey(arr[i])) {
                map2.put(arr[i], pattern.charAt(i));
            }
            if (!map1.get(pattern.charAt(i)).equals(arr[i]) || !map2.get(arr[i]).equals(pattern.charAt(i))) {
                return false;
            }
        }
        
        return true;
    }
}


