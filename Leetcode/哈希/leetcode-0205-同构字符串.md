# 205. 同构字符串

# 题目
给定两个字符串 s 和 t，判断它们是否是同构的。

如果 s 中的字符可以按某种映射关系替换得到 t ，那么这两个字符串是同构的。

每个出现的字符都应当映射到另一个字符，同时不改变字符的顺序。不同字符不能映射到同一个字符上，相同字符只能映射到同一个字符上，字符可以映射到自己本身。

https://leetcode.cn/problems/isomorphic-strings/description/

提示：
- 1 <= s.length <= 5 * 10000
- t.length == s.length
- s 和 t 由任意有效的 ASCII 字符组成


# 示例
```
输入：s = "egg", t = "add"
输出：true
```
```
输入：s = "foo", t = "bar"
输出：false
```
```
输入：s = "paper", t = "title"
输出：true
```

# 解析

## 哈希
字符串没有说都是小写字母之类的，所以用数组不合适了，用 map 来做映射。

使用两个 map 保存 s[i] 到 t[j] 和 t[j] 到 s[i] 的映射关系，如果发现对应不上，立刻返回 false

# 代码

### php
```php
class Leetcode0205 {

    function isIsomorphic($s, $t) {
        $map1 = $map2 = [];
        $len = strlen($s);
        for ($i = 0; $i < $len; $i++) {
            if (!array_key_exists($s[$i], $map1)) {
                $map1[$s[$i]] = $t[$i];
            }
            if (!array_key_exists($t[$i], $map2)) {
                $map2[$t[$i]] = $s[$i];
            }

            if ($map1[$s[$i]] != $t[$i] || $map2[$t[$i]] != $s[$i]) {
                return false;
            }
        }
        return true;
    }

}
```

### go
```go
func isIsomorphic(s string, t string) bool {
    map1 := make(map[byte]byte)
    map2 := make(map[byte]byte)
    for i := range s {
        if _, ok := map1[s[i]]; !ok {
            map1[s[i]] = t[i]
        }
        if _, ok := map2[t[i]]; !ok {
            map2[t[i]] = s[i]
        }

        if map1[s[i]] != t[i] || map2[t[i]] != s[i] {
            return false
        }
    }
    return true
}
```

### java
```java
class Solution {
    public boolean isIsomorphic(String s, String t) {
        Map<Character, Character> map1 = new HashMap<>();
        Map<Character, Character> map2 = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            if (!map1.containsKey(s.charAt(i))) {
                map1.put(s.charAt(i), t.charAt(i));
            }
            if (!map2.containsKey(t.charAt(i))) {
                map2.put(t.charAt(i), s.charAt(i));
            }

            if (map1.get(s.charAt(i)) != t.charAt(i) || map2.get(t.charAt(i)) != s.charAt(i)) {
                return false;
            }
        }
        return true;
    }
}
```