### 692. 前K个高频单词

# 题目
给定一个单词列表 words 和一个整数 k ，返回前 k 个出现次数最多的单词。

返回的答案应该按单词出现频率由高到低排序。如果不同的单词有相同出现频率， 按字典顺序 排序。

https://leetcode.cn/problems/top-k-frequent-words


提示：
- 1 <= words.length <= 500
- 1 <= words[i] <= 10
- words[i] 由小写英文字母组成。
- k 的取值范围是 [1, 不同 words[i] 的数量]


# 示例
```
输入: words = ["i", "love", "leetcode", "i", "love", "coding"], k = 2
输出: ["i", "love"]
解析: "i" 和 "love" 为出现次数最多的两个单词，均为2次。注意，按字母顺序 "i" 在 "love" 之前。
```
```
输入: ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"], k = 4
输出: ["the", "is", "sunny", "day"]
解析: "the", "is", "sunny" 和 "day" 是出现次数最多的四个单词，出现次数依次为 4, 3, 2 和 1 次。
```


# 解析

##### 哈希 + 排序

&emsp;&emsp;可以预处理出每一个单词出现的频率，然后依据每个单词出现的频率降序排序，最后返回前 k 个字符串即可。

&emsp;&emsp;具体地，利用哈希表记录每一个字符串出现的频率，然后将哈希表中所有字符串进行排序，排序时，如果两个字符串出现频率相同，
那么让两字符串中字典序较小的排在前面，否则让出现频率较高的排在前面。最后只需要保留序列中的前 k 个字符串即可。

# 代码

```php
$words = ["the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"];
$k = 4;
(new LeetCode1046())->main($words, $k);

class LeetCode0692
{
    public function main($words, $k) {
        var_dump($this->topKFrequent($words, $k));
    }

    function topKFrequent($words, $k) {
        $map = [];
        foreach ($words as $word) {
            if (isset($map[$word])) {
                $map[$word]['num']++;
            } else {
                $map[$word] = ['num' => 1, 'word' => $word];
            }
        }
        $values = array_values($map);
        $aWord = array_column($values, 'word');
        $aNum = array_column($values, 'num');
        array_multisort($aNum, SORT_DESC, $aWord, SORT_ASC, $values);

        $res = [];
        for($i = 0; $i < $k; $i++) {
            $res[] = $values[$i]['word'];
        }
        return $res;
    }
}
```