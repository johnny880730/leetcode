# 260. 只出现一次的数字 III


# 题目
给你一个整数数组 nums，其中恰好有两个元素只出现一次，其余所有元素均出现两次。 找出只出现一次的那两个元素。你可以按 任意顺序 返回答案。

进阶：设计并实现线性时间复杂度的算法且仅使用常量额外空间来解决此问题。

提示：
- 2 <= nums.length <= 3 * 10^4
- -2^31 <= nums[i] <= 2^31 - 1
- 除两个只出现一次的整数外，nums 中的其他数字都出现两次

# 示例
```
输入：nums = [1,2,1,3,2,5]
输出：[3,5]
解释：[5, 3] 也是有效的答案。
```
```
输入：nums = [-1,0]
输出：[-1,0]
```
```
输入：nums = [0,1]
输出：[1,0]
```

# 解析

## 哈希表
可以使用一个哈希映射统计数组中每一个元素出现的次数。

在统计完成后，对哈希映射进行遍历，将所有只出现了一次的数放入答案中。


# 代码

### php
```php
class Leetcode0260 {

    function singleNumber($nums) {
        $map = array();
        foreach ($nums as $num) {
            if (array_key_exists($num, $map)) {
                unset($map[$num]);
            } else {
                $map[$num] = true;
            }
        }
        $res = [];
        foreach ($map as $k => $v) {
            $res[] = $k;
        }
        return $res;
    }
}
```

### java
```java
public int[] singleNumber(int[] nums) {
    HashMap<Integer, Boolean> map = new HashMap<>();
    for (int num: nums) {
        if (map.containsKey(num)) {
            map.remove(num);
        } else {
            map.put(num, true);
        }
    }
    int[] res = new int[2];
    int idx = 0;
    for (Map.Entry<Integer, Boolean> entry : map.entrySet()) {
        res[idx++] = entry.getKey();
    }
    return res;
}
```
