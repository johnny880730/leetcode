# 350. 两个数组的交集 II

# 题目
给定两个数组，编写一个函数来计算它们的交集。

输出结果中每个元素出现的次数，应与元素在两个数组中出现的次数一致。
我们可以不考虑输出结果的顺序。

进阶:
如果给定的数组已经排好序呢？你将如何优化你的算法？
- 如果 nums1 的大小比 nums2 小很多，哪种方法更优？
- 如果 nums2 的元素存储在磁盘上，磁盘内存是有限的，并且你不能一次加载所有的元素到内存中，你该怎么办？

https://leetcode.cn/problems/intersection-of-two-arrays-ii/

提示：
- 1 <= nums1.length, nums2.length <= 1000
- 0 <= nums1[i], nums2[i] <= 1000

# 示例 
```
输入: nums1 = [1,2,2,1], nums2 = [2,2]
输出: [2,2]
```

```
输入: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
输出: [4,9]
```

# 解析

## 哈希
由于同一个数字在两个数组中都可能出现多次，因此需要用哈希表存储每个数字出现的次数。对于一个数字，
其在交集中出现的次数等于该数字在两个数组中出现次数的最小值。

首先遍历第一个数组，并在哈希表中记录第一个数组中的每个数字以及对应出现的次数，然后遍历第二个数组，对于第二个数组中的每个数字，
如果在哈希表中存在这个数字，则将该数字添加到答案，并减少哈希表中该数字出现的次数。

为了降低空间复杂度，首先遍历较短的数组并在哈希表中记录每个数字以及对应出现的次数，然后遍历较长的数组得到交集。

## 排序 + 双指针
如果两个数组是有序的，则可以使用双指针的方法得到两个数组的交集。

首先对两个数组进行排序，然后使用两个指针遍历两个数组。

初始时，两个指针分别指向两个数组的头部。每次比较两个指针指向的两个数组中的数字，如果两个数字不相等，
则将指向较小数字的指针右移一位，如果两个数字相等，将该数字添加到答案，并将两个指针都右移一位。当至少有一个指针超出数组范围时，遍历结束。

# 代码

```php
class LeetCode0350 {

    // 哈希
    function intersect($nums1, $nums2) {
        $hash1 = $hash2 = [];
        foreach ($nums1 as $num) {
            if (!array_key_exists($num, $hash1)) {
                $hash1[$num] = 0;
            }
            $hash1[$num]++;
        }
        foreach ($nums2 as $num) {
            if (!array_key_exists($num, $hash2)) {
                $hash2[$num] = 0;
            }
            $hash2[$num]++;
        }
        $res = [];
        foreach ($hash1 as $num => $n1) {
            if (array_key_exists($num, $hash2)) {
                $n2 = $hash2[$num];
                $n = min($n1, $n2);
                while ($n > 0) {
                    $res[] = $num;
                    $n--;
                }
            }
        }
        return $res;
    }

    // 排序 + 双指针
    function intersect2($nums1, $nums2) {
        $n1 = count($nums1);
        $n2 = count($nums2);
        if ($n1 == 0 || $n2 == 0)
            return [];

        sort($nums1);
        sort($nums2);

        $p1  = $p2 = 0;
        $ans = [];
        while ($p1 < $n1 && $p2 < $n2) {
            if ($nums1[$p1] == $nums2[$p2]) {
                $ans[] = $nums2[$p2];
                $p1++;
                $p2++;
            } elseif ($nums1[$p1] < $nums2[$p2]) {
                $p1++;
            } else {
                $p2++;
            }
        }

        return $ans;
    }
}
```

### java
```java
class LeetCode0350 {
    
    // 哈希
    public int[] intersect(int[] nums1, int[] nums2) {
        Map<Integer, Integer> map1 = new HashMap<>();
        for (int num : nums1) {
            map1.put(num, map1.getOrDefault(num, 0) + 1);
        }
        Map<Integer, Integer> map2 = new HashMap<>();
        for (int num : nums2) {
            map2.put(num, map2.getOrDefault(num, 0) + 1);
        }

        List<Integer> list = new ArrayList<>();
        for (int key : map1.keySet()) {
            if (map2.containsKey(key)) {
                int n = Math.min(map1.get(key), map2.get(key));
                while (n > 0) {
                    list.add(key);
                    n--;
                }
            }
        }
        int[] res = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            res[i] = list.get(i);
        }
        return res;
    }

    // 排序 + 双指针
    public int[] intersect2(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int p1 = 0, p2 = 0;

        List<Integer> list = new ArrayList<>();
        while (p1 < nums1.length && p2 < nums2.length) {
            if (nums1[p1] == nums2[p2]) {
                list.add(nums1[p1]);
                p1++;
                p2++;
            } else if (nums1[p1] < nums2[p2]) {
                p1++;
            } else {
                p2++;
            }
        }
        int[] res = new int[list.size()];
        int idx = 0;
        for (int i = 0; i < list.size(); i++) {
            res[idx++] = list.get(i);
        }

        return res;
    }
    
    
}
```

### go
```go
// 哈希
func intersect(nums1 []int, nums2 []int) []int {
    hash1 := make(map[int]int)
    for _, num := range nums1 {
        hash1[num]++
    }
    hash2 := make(map[int]int)
    for _, num := range nums2 {
        hash2[num]++
    }
    res := make([]int, 0)
    for num, n1 := range hash1 {
        if n2, ok := hash2[num]; ok {
            n := n1
            if n2 < n {
                n = n2
            }
            for n > 0 {
                res = append(res, num)
                n--
            }
        }
    }
    return res
}

// 排序 + 双指针
func intersect(nums1 []int, nums2 []int) []int {
    len1, len2 := len(nums1), len(nums2)
    if len1 == 0 || len2 == 0 {
        return []int{}
    }
    sort.Ints(nums1)
    sort.Ints(nums2)
    
    res := make([]int, 0)
    p1, p2 := 0, 0
    for p1 < len1 && p2 < len2 {
        if nums1[p1] == nums2[p2] {
            res = append(res, nums1[p1])
            p1++
            p2++
        } else if nums1[p1] < nums2[p2] {
            p1++
        } else {
            p2++
        }
    }
    return res
}
```