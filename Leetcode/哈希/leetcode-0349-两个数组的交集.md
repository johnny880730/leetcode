# 349. 两个数组的交集

# 题目
给定两个数组，编写一个函数来计算它们的交集。

https://leetcode.cn/problems/intersection-of-two-arrays

# 示例:
```
输入: nums1 = [1,2,2,1], nums2 = [2,2]
输出: [2]
```

# 示例
```
输入: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
输出: [9,4]
```
说明:
- 输出结果中的每个元素一定是唯一的。
- 我们可以不考虑输出结果的顺序。


# 代码

### php
```php
class LeetCode0349 {

    function intersection($nums1, $nums2) {
        $return = $hash = [];
        foreach ($nums1 as $num) {
            if (!isset($return[$num])) {
                $hash[$num] = 1;
            }
        }
        foreach ($nums2 as $num) {
            if (isset($hash[$num])) {
                $return[] = $num;
                unset($hash[$num]);
            }
        }
        return $return;
    }
}
```

### go
```go
func intersection(nums1 []int, nums2 []int) []int {
    res := make([]int, 0)
    hash := make(map[int]struct{})
    for _, num := range nums1 {
        if _, ok := hash[num]; !ok {
            hash[num] = struct{}{}
        }
    }
    for _, num := range nums2 {
        if _, ok := hash[num]; ok {
            res = append(res, num)
            delete(hash, num)
        }
    }
    return res
}
```
