# 1207. 独一无二的出现次数

# 题目
给你一个整数数组 arr，请你帮忙统计数组中每个数的出现次数。

如果每个数的出现次数都是独一无二的，就返回 true；否则返回 false。

https://leetcode.cn/problems/unique-number-of-occurrences/

提示：
- 1 <= arr.length <= 1000
- -1000 <= arr[i] <= 1000

# 示例
```
输入：arr = [1,2,2,1,1,3]
输出：true
解释：在该数组中，1 出现了 3 次，2 出现了 2 次，3 只出现了 1 次。没有两个数的出现次数相同。
```
```
输入：arr = [1,2]
输出：false
```
```
输入：arr = [-3,0,1,-3,1,1,1,-3,10,0]
输出：true
```

# 解析
这道题目数组在是哈希法中的经典应用

本题强调了-1000 <= arr[i] <= 1000，那么就可以用数组来做哈希，arr[i] 作为哈希表（数组）的下标，那么 arr[i] 可以是负数，
怎么办？负数不能做数组下标。

此时可以定义一个 size = 2000 的数组，例如int count[2002];，统计的时候，将 arr[i] 统一加 1000，这样就可以统计 arr[i] 的出现频率了。

题目中要求的是是否有相同的频率出现，那么需要再定义一个哈希表（数组）用来记录频率是否重复出现过，bool fre[1002];
定义布尔类型的就可以了，因为题目中强调 1 <= arr.length <= 1000，所以哈希表大小为 1000 就可以了。

![](./images/leetcode-1207-img1.png)

# 代码
```php
class Leetcode1207 {
    
    function uniqueOccurrences($arr) {
        $hashNum = $hashFre = [];
        foreach ($arr as $v) {
            if (!array_key_exists($v, $hashNum)) {
                $hashNum[$v] = 1;
            } else {
                $hashNum[$v]++;
            }
        }
        foreach ($hashNum as $num => $fre) {
            if (array_key_exists($fre, $hashFre)) {
                return false;
            }
            $hashFre[$fre] = true;
        }
        return true;
    }

}
```

### go
```go
func uniqueOccurrences(arr []int) bool {
    hashNum := make(map[int]int)
    for _, v := range arr {
        if _, ok := hashNum[v]; ok {
            hashNum[v]++
        } else {
            hashNum[v] = 1
        }
    }
    hashFre := make(map[int]bool)
    for _, fre := range hashNum {
        if _, ok := hashFre[fre]; ok {
            return false
        }
        hashFre[fre] = true
    }
    return true
}
```