
# 0001. 两数之和

# 题目
给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出和为目标值 target  的那两个整数，并返回它们的数组下标。

你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。

你可以按任意顺序返回答案。

https://leetcode.cn/problems/two-sum/description/

# 示例:
```
示例 1：

输入：nums = [2,7,11,15], target = 9
输出：[0,1]
解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
```
```
示例 2：

输入：nums = [3,2,4], target = 6
输出：[1,2]
```
```
示例 3：

输入：nums = [3,3], target = 6
输出：[0,1]
```

# 解析

## 哈希表

使用哈希表，可以将寻找 target - x 的时间复杂度做到 O(1)。

这样创建一个哈希表，对于每一个 x，我们首先查询哈希表中是否存在 target - x，然后将 x 插入到哈希表中，即可保证不会让 x 和自己匹配。

# 代码

### php
```php
class LeetCode0001 {

    public function twoSum($nums, $target) {
        $hash = [];
        foreach ($nums as $key => $val) {
            $diff = $target - $val;
            if (array_key_exists($diff, $hash)) {
                return [$hash[$diff], $key];
            }
            $hash[$val] = $key;
        }
        return [-1, -1];
    }
}
```

### go
```go
func twoSum(nums []int, target int) []int {
    numMap := make(map[int]int)
    for i, v := range nums {
        diff := target - v
        if _, ok := numMap[diff]; ok {
            return []int{numMap[diff], i}
        }
        numMap[v] = i
    }
    return []int{}
}
```

### java
```java
class LeetCode0001 {

    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(target - nums[i])) {
                return new int[]{i, map.get(target - nums[i])};
            }
            map.put(nums[i], i);
        }
        return new int[]{0};
    }
}
```
