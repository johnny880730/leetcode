# 242. 有效的字母异位词

# 题目
给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。

注意：若 s 和 t 中每个字符出现的次数都相同，则称 s 和 t 互为字母异位词。

https://leetcode.cn/problems/valid-anagram/description/

提示:
- 1 <= s.length, t.length <= 5 * 104
- s 和 t 仅包含小写字母

# 示例
```
输入: s = "anagram", t = "nagaram"
输出: true
```

```
输入: s = "rat", t = "car"
输出: false
```

# 解析

## 哈希
数组其实就是一个简单的哈希表，而且本题中的字符串均为小写字母，可以定义一个叫做 record、长度为 26 的数组来记录字符出现的次数，初始化为 0，因为字符 a ~ z 的 ASCII 值也是 26 个连续的数值。字符 a 映射为索引 0，字符 z 映射为索引 25。

对字符串 s 每个字符映射的索引做加 1 操作即可。这样字符串 s 中字符出现的次数就统计出来了。

再对字符串 t 中出现的字符映射到索引上的数值做减1操作。

如果 record 数组中所有元素都为 0，说明 s 中的字符可以通过改变顺序的方式变成 t。

如果 record 数组中有一个元素不为 0，说明 s 和 t 中一定有谁多了或少了字符，即返回 false。


# 代码

### php
```php
class LeetCode0242 {

    function isAnagram($s, $t) {
        $len1 = strlen($s);
        $len2 = strlen($t);
        if ($len1 != $len2) {
            return false;
        }
        $hash = array_fill(0, 26, 0);
        for ($i = 0; $i < $len1; $i++) {
            $hash[ord($s[$i]) - ord('a')]++;
        }
        for ($i = 0; $i < $len2; $i++) {
            $idx = ord($t[$i]) - ord('a'); 
            $hash[$idx]--;
            if ($hash[$idx] < 0) {
                return false;
            }
            
        }
        return true;
    }
}
```

### go
```go
func isAnagram(s string, t string) bool {
    len1, len2 := len(s), len(t)
    if len1 != len2 {
        return false
    }
    hash := make([]int, 26)
    for i := 0; i < len1; i++ {
        hash[s[i] - 'a']++
    }
    for i := 0; i < len2; i++ {
        hash[t[i] - 'a']--
        if hash[t[i] - 'a'] < 0 {
            return false
        }
    }
    return true
}
```

### java
```java
class LeetCode0242 {

    public boolean isAnagram(String s, String t) {
        int len1 = s.length(), len2 = t.length();
        if (len1 != len2) {
            return false;
        }
        int[] hash = new int[26];
        for (int i = 0; i < len1; i++) {
            hash[s.charAt(i) - 'a']++;
        }
        for (int i = 0; i < len2; i++) {
            hash[t.charAt(i) - 'a']--;
            if (hash[t.charAt(i) - 'a'] < 0) {
                return false;
            }
        }
        return true;
    }
}