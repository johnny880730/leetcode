# 1726. 同积元组

# 题目

给你一个由 不同 正整数组成的数组 nums ，请你返回满足 a * b = c * d 的元组 (a, b, c, d) 的数量。

其中 a、b、c 和 d 都是 nums 中的元素，且 a != b != c != d 。

提示：

- 1 <= nums.length <= 1000
- 1 <= nums[i] <= 10^4
- nums 中的所有元素 互不相同



# 示例

```
输入：nums = [2,3,4,6]
输出：8
解释：存在 8 个满足题意的元组：
(2,6,3,4) , (2,6,4,3) , (6,2,3,4) , (6,2,4,3)
(3,4,2,6) , (4,3,2,6) , (3,4,6,2) , (4,3,6,2)
```

```
输入：nums = [1,2,4,5,10]
输出：16
解释：存在 16 个满足题意的元组：
(1,10,2,5) , (1,10,5,2) , (10,1,2,5) , (10,1,5,2)
(2,5,1,10) , (2,5,10,1) , (5,2,1,10) , (5,2,10,1)
(2,10,4,5) , (2,10,5,4) , (10,2,4,5) , (10,2,4,5)
(4,5,2,10) , (4,5,10,2) , (5,4,2,10) , (5,4,10,2)
```

# 解析

## 哈希

根据题目的意思，每找到四个数 (a, b, c, d)，满足 a * b = c * d， 因为可以交换位置，实际上可以得到八个符合条件的元组。

- a 与 b 可以交换
- c 与 d 可以交换
- 前两个数的整体可以和后两个数的整体可以交换

其实只需要使用一个哈希表，存储每个可能出现的乘积以及乘积对应出现的次数即可。

比如 [2, 3, 4, 6]，可能出现的乘积有 6、8、12、18、24。且每个乘积出现的次数为：

- map[6] = 1
- map[8] = 1
- map[12] = 2
- map[18] = 1
- map[24] = 1

乘积出现 1 次，说明只存在 2 个数的乘积为该数，不足以形成四元组。乘积出现次数 >= 2， 说明存在至少 2 对数（至少 4 个数）的乘积为该数，
结果为 1 * 8 = 8。

求出了每个乘积出现的次数之后，然后开始排列组合，任意选两对乘积为该数的数对，两对刚好可以组成四元组。

所以问题就变成了：从 n 组数对中任取 2 对进行组合。公式就是：$C_n^2$



最后，每个四元组内部有八种组合，所以再乘以 8。结果就是
$$
C_n^2 * 8 = \frac{n!}{2!*(n-2)!} * 8 = \frac{n * (n - 1)}{2} * 8 = n * (n - 1) * 4
$$




# 代码



### php

```php
class Leetcode1726 {
    
    function tupleSameProduct($nums) {
        $hash = array();
        $len = count($nums);
        for ($i = 0; $i < $len; $i++) {
            for ($j = $i + 1; $j < $len; $j++) {
                $p = $nums[$i] * $nums[$j];
                if (array_key_exists($p, $hash)) {
                    $hash[$p]++;
                } else {
                    $hash[$p] = 1;
                }
            }
        }
        $res = 0;
        foreach ($hash as $key => $val) {
            if ($val >= 2) {
                $res += $val * ($val - 1) * 4;
            }
        }
        
        return $res;
    }
}
```

### java

```java
public int tupleSameProduct(int[] nums) {
    HashMap<Integer, Integer> hash = new HashMap<>();
    for(int i = 0; i < nums.length; i++) {
        for (int j = i + 1; j < nums.length; j++) {
            int p = nums[i] * nums[j];
            if (hash.containsKey(p)) {
                hash.put(p, hash.get(p) + 1);
            } else {
                hash.put(p, 1);
            }
        }
    }
    int res = 0;
    for (Map.Entry<Integer, Integer> entry: hash.entrySet()) {
        int n = entry.getValue();
        if (n >= 2) {
            res += n * (n - 1) * 4;
        }
    }
    return res;
}
```
