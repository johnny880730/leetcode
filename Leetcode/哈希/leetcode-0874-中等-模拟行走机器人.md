# 0874. 模拟行走机器人

# 题目
机器人在一个无限大小的 XY 网格平面上行走，从点 (0, 0) 处开始出发，面向北方。该机器人可以接收以下三种类型的命令 commands ：
- -2 ：向左转 90 度
- -1 ：向右转 90 度
- 1 <= x <= 9 ：向前移动 x 个单位长度

在网格上有一些格子被视为障碍物 obstacles 。第 i 个障碍物位于网格点  obstacles[i] = (xi, yi) 。

机器人无法走到障碍物上，它将会停留在障碍物的前一个网格方块上，并继续执行下一个命令。

返回机器人距离原点的 最大欧式距离 的 平方 。（即，如果距离为 5 ，则返回 25 ）

注意：
- 北方表示 +Y 方向。
- 东方表示 +X 方向。
- 南方表示 -Y 方向。
- 西方表示 -X 方向。
- 原点 [0,0] 可能会有障碍物。

https://leetcode.cn/problems/walking-robot-simulation/description/

提示：
- 1 <= commands.length <= 10^4
- commands[i] 的值可以取 -2、-1 或者是范围 [1, 9] 内的一个整数。
- 0 <= obstacles.length <= 10^4
- -3 * 10^4 <= xi, yi <= 3 * 10^4
- 答案保证小于 2^31

# 示例
```
示例 1：

输入：commands = [4,-1,3], obstacles = []
输出：25
解释：
机器人开始位于 (0, 0)：
1. 向北移动 4 个单位，到达 (0, 4)
2. 右转
3. 向东移动 3 个单位，到达 (3, 4)
距离原点最远的是 (3, 4) ，距离为 32 + 42 = 25
```
```
示例 2：

输入：commands = [4,-1,4,-2,4], obstacles = [[2,4]]
输出：65
解释：机器人开始位于 (0, 0)：
1. 向北移动 4 个单位，到达 (0, 4)
2. 右转
3. 向东移动 1 个单位，然后被位于 (2, 4) 的障碍物阻挡，机器人停在 (1, 4)
4. 左转
5. 向北走 4 个单位，到达 (1, 8)
距离原点最远的是 (1, 8) ，距离为 12 + 82 = 65
```
```
示例 3：

输入：commands = [6,-1,-1,6], obstacles = []
输出：36
解释：机器人开始位于 (0, 0):
1. 向北移动 6 个单位，到达 (0, 6).
2. 右转
3. 右转
4. 向南移动 6 个单位，到达 (0, 0).
机器人距离原点最远的点是 (0, 6)，其距离的平方是 62 = 36 个单位。
```

# 解析

## 模拟 + 哈希表
题目给出一个在点 (0, 0)，并面向北方的机器人。现在有一个大小为 n 的命令数组 commands 来操作机器人的移动，和一个大小为 m 的障碍物数组 obstacles。
现在通过 commands 来模拟机器人的移动，并用一个哈希表来存储每一个障碍物放置点。

当机器人的指令为向前移动时，尝试往前移动对应的次数。若往前一个单位不为障碍物放置点（即不在哈希表中），则机器人向前移动一个单位，否则机器人保持原位不变。

在机器人移动的过程中记录从原点到机器人所有经过的整数路径点的最大欧式距离的平方即为最后的答案。

在代码实现的过程中，对于机器人转向和向前移动的操作，可以用一个方向数组 dirs = {[-1, 0], [0, 1], [1, 0], [0, -1]} 来表示，方向分别代表左上右下。

若当前机器人的坐标为 (x, y)，当前方向的标号为 d：
- 则往前移动一单位的操作为 x = x + dirs[d][0]，y = y + dirs[d][1]
- 向左转的操作为 d = (d + 3) % 4
- 向右转的操作为 d = (d + 1 ) % 4


# 代码

### php
```php
class LeetCode0874 {

    /**
     * @param Integer[] $commands
     * @param Integer[][] $obstacles
     * @return Integer
     */
    function robotSim($commands, $obstacles) {
        $moves = [[-1, 0], [0, 1], [1, 0], [0, -1]];    //顺序：左上右下
        $hash = array();
        foreach ($obstacles as $o) {
            $k = join(',', $o);
            $hash[$k] = true;
        }
        $res = 0;
        $d = 1;                 // 一开始朝上（北）
        $px = $py = 0;          // 初始位置 (0, 0)
        foreach ($commands as $com) {
            // 左转
            if ($com == -2) {
                $d = ($d + 3) % 4;
            }
            // 右转
            else if ($com == -1) {
                $d = ($d + 1) % 4;
            }
            // 移动
            else {
                for ($i = 0; $i < $com; $i++) {
                    $nextPos = [$px + $moves[$d][0], $py + $moves[$d][1]];
                    $nextKey = join(',', $nextPos);
                    if (array_key_exists($nextKey, $hash)) {
                        break;
                    }
    
                    $px += $moves[$d][0];
                    $py += $moves[$d][1];
    
                    $res = max($res, $px * $px + $py * $py);
                }
            }
        }
        return $res;
    }
}
```

### java
```java
class LeetCode0874 {
    
    public int robotSim(int[] commands, int[][] obstacles) {
        int[][] moves = new int[][]{{-1, 0}, {0, 1}, {1, 0}, {0, -1}};      //顺序：左上右下
        Map<String, Boolean> map = new HashMap<>();
        for (int[] o: obstacles) {
            String k = Arrays.toString(o).replace("[", "").replace("]", "");
            map.put(k, true);
        }
        int d = 1;              // 一开始朝上（北）
        int px = 0, py = 0;     // 初始位置 (0, 0)
        int res = 0;
        for (int com: commands) {
            // 左转
            if (com == -2) {
                d = (d + 3) % 4;
            }
            // 右转
            else if (com == -1) {
                d = (d + 1) % 4;
            }
            // move
            else {
                for (int i = 0; i < com; i++) {
                    int[] nextPos = new int[]{px + moves[d][0], py + moves[d][1]};
                    String nextKey = Arrays.toString(nextPos).replace("[", "").replace("]", "");
                    if (map.containsKey(nextKey)) {
                        break;
                    }
                    px += moves[d][0];
                    py += moves[d][1];
                    
                    res = Math.max(res, px * px + py * py);
                }
            }
        }
        
        return res;
    }
}
```

### go
```go
func robotSim(commands []int, obstacles [][]int) int {
    moves := [][]int{{-1, 0}, {0, 1}, {1, 0}, {0, -1}}      //顺序：左上右下
	hash := make(map[string]bool)
	for _, o := range obstacles {
		stringArr := make([]string, 2)
		for k, v := range o {
			stringArr[k] = strconv.Itoa(v)
		}
		key := strings.Join(stringArr, ",")
		hash[key] = true
	}
	d := 1                  // 一开始朝上（北）
	px, py := 0, 0          // 初始位置 (0, 0)
	res := 0
	for _, com := range commands {
		if com == -2 {
            // 左转
			d = (d + 3) % 4
		} else if com == -1 {
            // 右转
			d = (d + 1) % 4
		} else {
			// move
			for i := 0; i < com; i++ {
				nextPos := []int{px + moves[d][0], py + moves[d][1]}
				nextPosString := make([]string, 2)
				for k, v := range nextPos {
					nextPosString[k] = strconv.Itoa(v)
				}
				nextKey := strings.Join(nextPosString, ",")
				if _, ok := hash[nextKey]; ok {
					break
				}
				px += moves[d][0]
				py += moves[d][1]

				if px*px+py*py > res {
					res = px*px + py*py
				}

			}
		}
	}

	return res
}
```
