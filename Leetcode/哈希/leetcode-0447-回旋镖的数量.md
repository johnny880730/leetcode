### 447. 回旋镖的数量

# 题目
给定平面上 n 对 互不相同 的点 points ，其中 points[i] = [xi, yi] 。回旋镖 是由点 (i, j, k) 表示的元组 ，
其中 i 和 j 之间的距离和 i 和 k 之间的欧式距离相等（需要考虑元组的顺序）。

返回平面上所有回旋镖的数量。

https://leetcode.cn/problems/number-of-boomerangs

提示：
- n == points.length
- 1 <= n <= 500
- points[i].length == 2
- -10<sup>4</sup> <= xi, yi <= 10<sup>4</sup>
- 所有点都 互不相同


## 示例
```
输入：points = [[0,0],[1,0],[2,0]]
输出：2
解释：两个回旋镖为 [[1,0],[0,0],[2,0]] 和 [[1,0],[2,0],[0,0]]
```
```
输入：points = [[1,1],[2,2],[3,3]]
输出：2
```
```
输入：points = [[1,1]]
输出：0
```

# 解析

#### 暴力 + 哈希

&emsp;&emsp;题目所描述的回旋镖可以视作一个 V 型的折线。可以枚举每个 points[i]，将其当作 V 型的拐点。
设 points 中有 m 个点到 points[i] 的距离均相等，我们需要从这 m 点中选出 2 个点当作回旋镖的 2 个端点，
由于题目要求考虑元组的顺序，因此方案数即为在 m 个元素中选出 2 个不同元素的排列数，即：m * (m-1)

&emsp;&emsp;据此，可以遍历 points，计算并统计所有点到 points[i] 的距离，将每个距离的出现次数记录在哈希表中，
然后遍历哈希表，并用上述公式计算并累加回旋镖的个数。

&emsp;&emsp;在代码实现时，可以直接保存距离的平方，避免复杂的开方运算。

# 代码

```php
$points = [[0,0],[1,0],[2,0]];
(new LeetCode0220)->main($points);

class LeetCode0447
{
    function main($points)
    {
        print_r($this->numberOfBoomerangs($points));
    }

    function numberOfBoomerangs($points)
    {
        $res = 0;
        $len = count($points);
        for ($i = 0; $i < $len; $i++) {
            $record = [];
            for ($j = 0; $j < $len; $j++) {
                if ($i == $j) {
                    continue;
                }
                $dis = $this->_getDistance($points[$i], $points[$j]);
                $record[$dis]++;
            }
            foreach ($record as $dis => $val) {
                if ($val >= 2) {
                    $res += $val * ($val - 1);
                }
            }
        }
        return $res;
    }

    // 获取两个点之间的距离
    // 这里不开根号因为小数会有误差，不开根号不会影响到获取的“距离”
    function _getDistance($p1, $p2)
    {
        return ($p1[0] - $p2[0]) * ($p1[0] - $p2[0]) + ($p1[1] - $p2[1]) * ($p1[1] - $p2[1]);
    }
}
```