# 0128. 最长连续序列


# 题目
给定一个未排序的整数数组 nums ，找出数字连续的最长序列（不要求序列元素在原数组中连续）的长度。

请你设计并实现时间复杂度为 O(n) 的算法解决此问题。

https://leetcode.cn/problems/longest-consecutive-sequence/description/

提示：
- 0 <= nums.length <= 100000
- -10^9 <= nums[i] <= 10^9

# 示例
```
示例 1：

输入：nums = [100,4,200,1,3,2]
输出：4
解释：最长数字连续序列是 [1, 2, 3, 4]。它的长度为 4。
```
```
示例 2：

输入：nums = [0,3,7,2,5,8,4,6,0,1]
输出：9
```

# 解析

## 哈希表
对于数组中存在的连续序列，为了统计每个连续序列的长度，我们希望直接定位到每个连续序列的起点，从起点开始遍历每个连续序列，从而获得长度。

![](./images/leetcode-0128-image.png)

那么如何获取到每个连续序列的起点呢，或者说什么样的数才是一个连续序列的起点？

答案是这个数的前一个数不存在于数组中，因为我们需要能够快速判断当前数 num 的前一个数 num - 1 是否存在于数组中。

同时当我们定位到起点后，我们就要遍历这个连续序列，什么时候是终点呢？

答案是当前数 num 的后一个数 num + 1 不存在于数组中，因此我们需要能够快速判断当前数 num 的后一个数 num + 1 是否存在于数组中。

为了实现上述需求，可以使用哈希表来记录数组中的所有数，以实现对数值的快速查找。



# 代码

### php
```php
class LeetCode0128 {

    function longestConsecutive($nums) {
        $res = 0;
        $hash = [];
        // 将数组中的值加入哈希表中
        foreach ($nums as $num) {
            $hash[$num] = true;
        }
        // 连续序列的长度
        $tmpLen = 0;
        foreach ($nums as $num) {
            // 如果当前的数是一个连续序列的起点，统计这个连续序列的长度
            if (!isset($hash[$num - 1])) {
                $tmpLen = 1;
                // 不断查找连续序列，直到num的下一个数不存在于数组中
                while (isset($hash[++$num])) {
                    $tmpLen++;
                }
                $res = max($res, $tmpLen);
            }
        }
        
        return $res;
    }
}
```

### go
```go
func longestConsecutive(nums []int) int {
    res := 0
    hash := make(map[int]bool)
    for _, num := range nums {
        hash[num] = true
    }
    tmpLen := 0
    for _, num := range nums {
        if _, ok := hash[num-1]; !ok {
            tmpLen = 1
            for {
                num++
                if _, ok := hash[num]; ok {
                    tmpLen++
                } else {
                    break
                }
            }
            res = max(res, tmpLen)
        }
    }
    return res
}

func max(a, b int) int {
    if a > b {
        return a
    }
    return b
}
```

### java
```java
public int longestConsecutive(int[] nums) {
    int res = 0;
    Set<Integer> numSet = new HashSet<>();
    for (int num: nums) {
        numSet.add(num);
    }
    int tmpLen = 0;
    for (int num: nums) {
        if (!numSet.contains(num - 1)) {
            tmpLen = 1;
            while (numSet.contains(++num)) {
              tmpLen++;
            }
            res = Math.max(res, tmpLen);
        }
    }
    return res;
}
```