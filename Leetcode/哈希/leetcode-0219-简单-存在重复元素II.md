# 219. 存在重复元素 II

# 题目
给定一个整数数组和一个整数 k，判断数组中是否存在两个不同的索引 i 和 j，使得 nums [i] = nums [j]，
并且 i 和 j 的差的 绝对值 至多为 k。

https://leetcode.cn/problems/contains-duplicate-ii/description/

    
提示
- 1 <= nums.length <= 100000
- -10^9 <= nums[i] <= 10^9
- 0 <= k <= 100000


## 示例
```
输入: nums = [1,2,3,1], k = 3
输出: true
```
```
输入: nums = [1,0,1,1], k = 1
输出: true
```
```
输入: nums = [1,2,3,1,2,3], k = 2
输出: false
```


# 解析

## 哈希
从左到右遍历数组 nums，当遍历到下标 i 时，如果存在下标 j < i 使得 nums[i] = nums[j]，则当 i − j ≤ k 时即找到了两个符合要求的下标 j 和 i。

如果在下标 i 之前存在多个元素都和 nums[i] 相等，为了判断是否存在满足 nums[i] = nums[j] 且 i − j ≤ k 的下标 jjj，应该在这些元素中寻找下标最大的元素，
将最大下标记为 j，判断 i − j ≤ k 是否成立。

因此，当遍历到下标 i 时，如果在下标 i 之前存在与 nums[i] 相等的元素，应该在这些元素中寻找最大的下标 j，判断 i − j ≤ k 是否成立。

可以使用哈希表记录每个元素的最大下标。从左到右遍历数组 nums，当遍历到下标 i 时，进行如下操作：
- 如果哈希表中已经存在和 nums[i] 相等的元素且该元素在哈希表中记录的下标 j 满足 i − j ≤ k，返回 true
- 将 nums[i] 和下标 i 存入哈希表，此时 i 是 nums[i] 的最大下标。

上述两步操作的顺序不能改变，因为当遍历到下标 i 时，只能在下标 i 之前的元素中寻找与当前元素相等的元素及该元素的最大下标。

当遍历结束时，如果没有遇到两个相等元素的下标差的绝对值不超过 k，返回 false


# 代码

### php
```php
class LeetCode0219 {

    // 哈希表
    function containsNearbyDuplicate($nums, $k) {
        $hash = [];
        $len = count($nums);
        for ($i = 0; $i < $len; $i++) {
            if (isset($hash[$nums[$i]]) && $i - $hash[$nums[$i]] <= $k) {
                return true;
            }
            $hash[$nums[$i]] = $i;
        }
        return false;
    }
}
```

### go
```go
func containsNearbyDuplicate(nums []int, k int) bool {
    hash := make(map[int]int, len(nums))
    for i, v := range nums {
        if _, ok := hash[v]; ok && i - hash[v] <= k {
            return true
        }
        hash[v] = i
    }
    return false
}
```
    
### java
```java
class LeetCode0219 {
    
    public boolean containsNearbyDuplicate(int[] nums, int k) {
        Map<Integer, Integer> hash = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; ++i) {
            if (hash.containsKey(nums[i]) && i - hash.get(nums[i]) <= k) {
                return true;
            }
            hash.put(nums[i], i);
        }
        return false;
    }
}
```