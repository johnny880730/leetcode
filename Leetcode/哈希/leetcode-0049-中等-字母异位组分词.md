# 0049. 字母异位词分组

# 题目
给你一个字符串数组，请你将 字母异位词 组合在一起。可以按任意顺序返回结果列表。

字母异位词 是由重新排列源单词的字母得到的一个新单词，所有源单词中的字母通常恰好只用一次。

https://leetcode.cn/problems/group-anagrams/description/

提示：
- 1 <= strs.length <= 10000
- 0 <= strs[i].length <= 100
- strs[i] 仅包含小写字母

# 示例
```
示例 1:

输入: strs = ["eat", "tea", "tan", "ate", "nat", "bat"]
输出: [["bat"],["nat","tan"],["ate","eat","tea"]]
```
```
示例 2:

输入: strs = [""]
输出: [[""]]
```
```
示例 3:

输入: strs = ["a"]
输出: [["a"]]
```

# 解析

## 排序 + 哈希

由于互为字母异位词的两个字符串包含的字母相同，因此对两个字符串分别进行排序之后得到的字符串一定是相同的，
故可以将排序之后的字符串作为哈希表的键。

# 代码

### php
```php
class LeetCode0049 {

    function groupAnagrams($strs) {
        $res = [];
        foreach ($strs as $str) {
            $tmp = str_split($str);
            sort($tmp);
            $s = join('', $tmp);
            $res[$s][] = $str;
        }
        return array_values($res);
    }

}
```

### go
```go
func groupAnagrams(strs []string) [][]string {
    strMap := make(map[string][]string)
    for _, str := range strs {
        arr := strings.Split(str, "")
        sort.Strings(arr)
        ss := strings.Join(arr, "")
        strMap[ss] = append(strMap[ss], str)
    }
    var res [][]string
    for _, val := range strMap {
        res = append(res, val)
    }
    return res
}
```

### java
```java
class LeetCode0049 {

    public List<List<String>> groupAnagrams(String[] strs) {
        HashMap<String, List<String>> map = new HashMap<>();
        for (String str: strs) {
            char[] chars = str.toCharArray();
            Arrays.sort(chars);
            String key = String.valueOf(chars);
            List<String> list = map.getOrDefault(key, new ArrayList<>());
            list.add(str);
            map.put(key, list);
        }
        List<List<String>> res = new ArrayList<>();
        return new ArrayList<List<String>>(map.values());
    }
}
```
