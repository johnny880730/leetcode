# 0383. 赎金信

# 题目
给你两个字符串：ransomNote 和 magazine ，判断 ransomNote 能不能由 magazine 里面的字符构成。

如果可以，返回 true ；否则返回 false 。

magazine 中的每个字符只能在 ransomNote 中使用一次。

https://leetcode.cn/problems/ransom-note/description/


提示：
- 1 <= ransomNote.length, magazine.length <= 10000
- ransomNote 和 magazine 由小写英文字母组成

# 示例
```
输入：ransomNote = "a", magazine = "b"
输出：false
```
```
输入：ransomNote = "aa", magazine = "ab"
输出：false
```
```
输入：ransomNote = "aa", magazine = "aab"
输出：true
```

# 解析

## 哈希
可以用一个哈希表或长度为 26 的数组 cnt 记录字符串 magazine 中所有字符出现的次数。

然后遍历字符串 ransomNote，对于其中的每个字符 c，我们将其从 cnt 的次数减 1，如果减 1 之后的次数小于 0，说明 c 在 magazine 中出现的次数不够，因此无法构成 ransomNote，返回 false 即可。

否则，遍历结束后，说明 ransomNote 中的每个字符都可以在 magazine 中找到对应的字符，因此返回 true

# 代码

### php
```php
class LeetCode0383 {

    /**
     * @param String $ransomNote
     * @param String $magazine
     * @return Boolean
     */
    function canConstruct($ransomNote, $magazine) {
        $cnt = [];
        foreach (str_split($magazine) as $c) {
            $cnt[$c]++;
        }
        foreach (str_split($ransomNote) as $c) {
            $cnt[$c]--;
            if ($cnt[$c] < 0) {
                return false;
            }
        }
        return true;
    }
}
```

### java
```java
class LeetCode0383 {

    /**
     * @param ransomNote
     * @param magazine
     * @return
     */
    public boolean canConstruct(String ransomNote, String magazine) {
        int[] cnt = new int[26];
        for (char c : magazine.toCharArray()) {
            cnt[c - 'a']++;
        }
        for (char c : ransomNote.toCharArray()) {
            cnt[c - 'a']--;
            if (cnt[c - 'a'] < 0) {
                return false;
            }
        }
        return true;
    }
}
```

### go
```go
func canConstruct(ransomNote string, magazine string) bool {
    cnt := [26]int{}
    for _, c := range magazine {
        cnt[c - 'a']++
    }
    for _, c := range ransomNote {
        cnt[c - 'a']--
        if cnt[c - 'a'] < 0 {
            return false
        }
    }
    return true
}
```
 