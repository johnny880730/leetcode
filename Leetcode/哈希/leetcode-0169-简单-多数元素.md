# 169. 多数元素

# 题目
给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数大于  n/2 的元素。

你可以假设数组是非空的，并且给定的数组总是存在多数元素。

https://leetcode.cn/problems/majority-element

提示：
- n == nums.length
- 1 <= n <= 5 * 10^4
- -10^9 <= nums[i] <= 10^9

# 示例
```
输入: [3,2,3]
输出: 3
```

```
输入: [2,2,1,1,1,2,2]
输出: 2
```

# 解析

## 哈希表
我们知道出现次数最多的元素大于 floor(n / 2) 次，所以可以用哈希表来快速统计每个元素出现的次数。

使用哈希映射来存储每个元素以及出现的次数。对于哈希映射中的每个键值对，键表示一个元素，值表示该元素出现的次数。
- 用一个循环遍历数组 nums 并将数组中的每个元素加入哈希映射中。
- 遍历哈希映射中的所有键值对，返回值最大的键。
  - 同样也可以在遍历数组 nums 时候使用打擂台的方法，维护最大的值，这样省去了最后对哈希映射的遍历。


# 代码

### php
```php
class LeetCode0169 {
    
    function majorityElement($nums) {
        $res = null;
        $maxNum = 0;
        $hash = [];
        foreach ($nums as $val) {
            if (!isset($hash[$val])) {
                $hash[$val] = 0;
            }
            $hash[$val]++;

            if ($hash[$val] > $maxNum) {
                $res = $val;
                $maxNum = $hash[$val];
            }
        }

        return $res;
    }
}
```

### java
```java
class Leetcode0169 {
    
    public int majorityElement(int[] nums) {
        int maxNum = 0;
        int res = 0;
        HashMap<Integer, Integer> hash = new HashMap();
        for (int val: nums) {
            int cur = hash.getOrDefault(val, 0);
            cur++;
            hash.put(val, cur);

            if (cur > maxNum) {
                maxNum = cur;
                res = val;
            }
        }

        return res;
    }
}
```

### go
```go
func majorityElement(nums []int) int {
    maxNum, res := 0, 0
    hash := make(map[int]int)
    for _, val := range nums {
        cur := hash[val]
        cur++
        hash[val] = cur

        if cur > maxNum {
            maxNum = cur
            res = val
        }
    }

    return res
}
```