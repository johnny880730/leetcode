# 0019. 删除链表的倒数第 N 个结点【中等】

# 题目
给你一个链表，删除链表的倒数第 n 个结点，并且返回链表的头结点。

进阶：你能尝试使用一趟扫描实现吗？

https://leetcode.cn/problems/remove-nth-node-from-end-of-list/description/

提示：
- 链表中结点的数目为 sz
- 1 <= sz <= 30
- 0 <= Node.val <= 100
- 1 <= n <= sz

# 示例：
```
示例 1：

输入：head = [1,2,3,4,5], n = 2
输出：[1,2,3,5]
```

```
示例 2：

输入：head = [1], n = 1
输出：[]
```

```
示例 3：

输入：head = [1,2], n = 1
输出：[1]
```

# 解析

## 双指针
思路：本题也是双指针的经典应用。如果要删除倒数第 n 个节点，则让快指针移动 n 步，然后让快慢指针同时移动，直到快指针指向链表末尾，
删除慢指针所指向的节点就可以了。

删除链表中的倒数第 n 个节点分为如下几步：
1. 设置虚拟头节点
2. 定义 fast 指针和 slow 指针，初始值为虚拟头节点 dummyHead。
3. fast 先移动 n + 1 步。为什么是 n + 1 呢？因为只有这样，fast 和 slow 同时移动的时候，slow 才能指向删除节点的上一个节点（方便做删除操作）
4. fast 和 slow 同时移动，直到 fast 指向末尾。
5. 删除 slow 指向的下一个节点。 

# 代码

### php
```php
class LeetCode0019 {

    // 双指针
    public function removeNthFromEnd($head, $n) {
        $dummy = new ListNode(null);
        $dummy->next = $head;

        $slow = $fast = $dummy;
        // fast 先走 n 步
        for ($i = 0; $i < $n; $i++) {
            $fast = $fast->next;
        }
        $fast = $fast->next;    // fast 再走一步，为了让 slow 指向被删除节点的前一格节点
        while ($fast) {
            $fast = $fast->next;
            $slow = $slow->next;
        }
        $slow->next = $slow->next->next;
        return $dummy->next;
    }
}
```

### go
```go
func removeNthFromEnd(head *ListNode, n int) *ListNode {
    dummy := &ListNode{}
    dummy.Next = head
    
    fast, slow := dummy, dummy
    for i := 0; i < n; i++ {
        fast = fast.Next
    }
    fast = fast.Next
    for fast != nil {
        fast = fast.Next
        slow = slow.Next
    }
    slow.Next = slow.Next.Next
    return dummy.Next
}
```

### java
```java
class LeetCode0019 {

    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode dummy = new ListNode();
        dummy.next = head;
        ListNode fast = dummy, slow = dummy;
        while (n > 0) {
            fast = fast.next;
            n--;
        }
        fast = fast.next;
        while (fast != null) {
            fast = fast.next;
            slow = slow.next;
        }
        slow.next = slow.next.next;
        return dummy.next;
    }
}
```
