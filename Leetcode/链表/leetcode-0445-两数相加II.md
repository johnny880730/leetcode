### 445. 两数相加 II

# 题目
给你两个 非空 链表来代表两个非负整数。数字最高位位于链表开始位置。它们的每个节点只存储一位数字。将这两数相加会返回一个新的链表。

你可以假设除了数字 0 之外，这两个数字都不会以零开头。

https://leetcode.cn/problems/add-two-numbers-ii

# 示例
```
输入：l1 = [7,2,4,3], l2 = [5,6,4]
输出：[7,8,0,7]
```
# 示例
```
输入：l1 = [2,4,3], l2 = [5,6,4]
输出：[8,0,7]
```

提示：
- 链表的长度范围为 [1, 100]
- 0 <= node.val <= 9
- 输入数据保证链表代表的数字无前导 0

# 解析
&emsp;&emsp;主要难点在于链表中数位的顺序与我们做加法的顺序是相反的，为了逆序处理所有数位，可以使用栈。

&emsp;&emsp;把所有数字压入栈中，再依次取出相加。计算过程中需要注意进位的情况。

# 代码

```php
require './class/ListNode.class.php';
$l1 = [9,9,9,9,9,9,9]; $l2 = [9,9,9,9];
$head1 = array2LinkList($l1);
$head2 = array2LinkList($l2);
(new LeetCode0445())->main($head1, $head2);

class LeetCode0445
{

    function main($l1, $l2)
    {
        fetchNode($this->addTwoNumbers($l1, $l2));
    }


    function addTwoNumbers($l1, $l2)
    {
        $stack1 = new SplStack();
        $stack2 = new SplStack();
        while ($l1) {
            $stack1->push($l1->val);
            $l1 = $l1->next;
        }
        while ($l2) {
            $stack2->push($l2->val);
            $l2 = $l2->next;
        }
        $carry = 0;
        $ans = null;
        while ($stack1->isEmpty() == false || $stack2->isEmpty() == false || $carry > 0) {
            $a = $stack1->isEmpty() ? 0 : $stack1->pop();
            $b = $stack2->isEmpty() ? 0 : $stack2->pop();
            $sum = $a + $b + $carry;
            $carry = floor($sum / 10);
            $sum = $sum % 10;
            $node = new ListNode($sum);
            $node->next = $ans;
            $ans = $node;
        }
        return $ans;
    }

}
```