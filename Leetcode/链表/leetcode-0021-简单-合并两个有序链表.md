# 0021. 合并两个有序链表

# 题目
将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。

https://leetcode.cn/problems/merge-two-sorted-lists/description/

提示：
- 两个链表的节点数目范围是 [0, 50]
- -100 <= Node.val <= 100
- l1 和 l2 均按 非递减顺序 排列

# 示例
```
示例 1：

输入：l1 = [1,2,4], l2 = [1,3,4]
输出：[1,1,2,3,4,4]
```

# 解析

1、一开始设置一个虚拟节点，它的值为 -1，它的值可以设置为任何的数，因为我们根本不需要使用它的值。

2、设置一个指针 cur，指向虚拟节点，在后续的操作过程中，会移动 cur 的位置，让它指向所有已经排好序的节点里面的最后一个节点位置。

3、借助 while 循环，不断的比较 list1 和 list2 中当前节点值的大小，直到 list1 或者 list2 遍历完毕为止。

4、在比较过程中，如果 list1 当前节点的值小于等于了 list2 当前节点的值，让 cur 指向节点的 next 指针指向这个更小值的节点，即 list1 上面的当前节点，同时继续访问 list1 的后续节点。

5、如果 list1 当前节点的值大于了 list2 当前节点的值，让 cur 指向节点的 next 指针指向这个更小值的节点，即 list2 上面的当前节点，同时继续访问 list2 的后续节点。

6、每次经过 4 、5 的操作之后，都会让 cur 向后移动，因为需要保证 cur 指向所有已经排好序的节点里面的最后一个节点位置。

# 代码

### php
```php
class LeetCode0021 {

    function mergeTwoLists($l1, $l2) {
        // 虚拟头结点
        $dummy = new ListNode(null);
        
        // 设置一个指针，指向虚拟结点
        $cur = $dummy;
        
        // 通过一个循环，不断的比较 list1 和 list2 中当前结点值的大小，直到 list1 或者 list2 遍历完毕为止
        while ($l1 != null && $l2 != null) {
            if ($l1->val <= $l2->val) {
                $cur->next = $l1;
                $l1 = $l1->next;
            } else {
                $cur->next = $l2;
                $l2 = $l2->next;
            }
            $cur = $cur->next;
        }
        // 跳出循环后，list1 或者 list2 中可能有剩余的结点没有被观察过
        // 直接把剩下的结点加入到 cur 的 next 指针位置
        // 合并后 l1 和 l2 最多只有一个没合并完，直接将链表末尾指向未合并完的部分
        $cur->next = $l1 == null ? $l2 : $l1;
        
        // 最后返回虚拟结点的 next 指针
        return $dummy->next;
    }
}
```

### go
```go
func mergeTwoLists(list1 *ListNode, list2 *ListNode) *ListNode {
    dummy := &ListNode{}
    cur := dummy
    for list1 != nil && list2 != nil {
        v1 := list1.Val
        v2 := list2.Val
        if v1 < v2 {
            cur.Next = &ListNode{Val: v1}
            list1 = list1.Next
        } else {
            cur.Next = &ListNode{Val: v2}
            list2 = list2.Next
        }
        cur = cur.Next
    }
    if list1 != nil {
        cur.Next = list1
    }
    if list2 != nil {
        cur.Next = list2
    }
    
    return dummy.Next
}
```

### java
```java
class LeetCode0021 {

    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode dummy = new ListNode();
        ListNode cur = dummy;
        while (list1 != null && list2 != null) {
            if (list1.val <= list2.val) {
                cur.next = new ListNode(list1.val);
                list1 = list1.next;
            } else {
                cur.next = new ListNode(list2.val);
                list2 = list2.next;
            }
            cur = cur.next;
        }
        cur.next = list1 == null ? list2 : list1;

        return dummy.next;
    }
}

```
