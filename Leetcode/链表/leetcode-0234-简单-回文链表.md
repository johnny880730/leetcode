# 0234. 回文链表

# 题目 
请判断一个链表是否为回文链表。

进阶：你能否用 O(n) 时间复杂度和 O(1) 空间复杂度解决此题？

https://leetcode.cn/problems/palindrome-linked-list/description/

提示：
- 链表中节点数目在范围[1, 100,000] 内
- 0 <= Node.val <= 9


# 示例:
```
输入: 1->2
输出: false
```

# 示例
```
输入: 1->2->2->1
输出: true
```

# 解析
最简单的方法就是将链表的值复制到数组列表中，再使用双指针法判断。

时间复杂度：O(n)。空间复杂度：O(n)，其中 n 指的是链表的元素个数，因为使用了一个数组列表存放链表的元素值。

不用其他数据结构，只用有限的几个变量，额外空间复杂度 O(1)。过程如下：
1. 改变链表右半区的结构，使整个右半区反转，最后指向中间节点。
2. 将左半区的第一个节点（也就是原来的头节点）记为 leftStart，右半区反转之后的最右边的节点(原来的最后节点）记为 rightStart。
3. leftStart 和 rightStart 同时向中间点移动，移动每一步时比较 leftStart 和 rightStart的值。如果都一样就是回文结构。
4. 不管最后返回的是 true 还是 false，还原链表到原来的样子。（这步是否可做可不做？）
5. 返回结果

![](./images/leetcode-0234-题解.gif)

# 代码

### php

```php

class LeetCode0234 {
    
    // 数组法 空间复杂度O(n)
    function isPalindrome($head) {
        $arr = [];
        while ($head) {
            $arr[] = $head->val;
            $head = $head->next;
        }
        $i = 0;
        $j = count($arr) - 1;
        while ($i < $j) {
            if ($arr[$i] != $arr[$j]) {
                return false;
            }
            $i++;
            $j--;
        }
        return true;
    }
    
    // 进阶 空间复杂度 O(1)
    function isPalindrome($head) {
        // 找到前半部分链表的尾节点 为翻转后半部分做准备
        $leftEnd = $this->_getMiddleNode($head);
        
        // 反转后半部分
        $rightStart = $this->_reverseList($leftEnd->next);       
        
        // 判断是否回文
        $i = $head;
        $j = $rightStart;
        while ($i !== null && $j !== null) {
            if ($i->val != $j->val) {
                return false;
            }
            $i = $i->next;
            $j = $j->next;
        }
        return true;
    }
    
    // 获取链表的中间节点，迭代法
    function _getMiddleNode($head) {
        // 快慢指针 
        $slow = $fast = $head;
        // 快指针移动两步，慢指针移动一步
        while ($fast->next != null && $fast->next->next != null) {
            $slow = $slow->next;
            $fast = $fast->next->next;
        }
        // 慢指针的位置就是中间节点
        return $slow;
    }
    
    // 反转链表 [leetcode-0206]
    function _reverseList($node) {
        $pre = null;
        $cur = $node;
        while ($cur != null) {
            $tmp = $cur->next;
            $cur->next = $pre;
            $pre = $cur;
            $cur = $tmp;
        }
        return $pre;
    }
}
```

### go
```go
func isPalindrome(head *ListNode) bool {
    leftEnd := _getMiddleNode(head)
    rightStart := _reverseList(leftEnd.Next)
    i, j := head, rightStart
    for i != nil && j != nil {
        if i.Val != j.Val {
            return false
        }
        i = i.Next
        j = j.Next
    }
    return true
}

func _getMiddleNode(head *ListNode) *ListNode {
    slow, fast := head, head
    for fast.Next != nil && fast.Next.Next != nil {
        slow = slow.Next
        fast = fast.Next.Next
    }
    return slow
}

func _reverseList(node *ListNode) *ListNode {
    if node == nil || node.Next == nil {
        return node
    }
    rev := _reverseList(node.Next)
    node.Next.Next = node
    node.Next = nil
    return rev
}
```

### java
```java
public boolean isPalindrome(ListNode head) {
    ListNode leftEnd = _getMiddleNode(head);
    ListNode rightStart = _reverseList(leftEnd.next);
    ListNode i = head, j = rightStart;
    while (i != null && j != null) {
        if (i.val != j.val) {
            return false;
        }
        i = i.next;
        j = j.next;
    }
    return true;
}

protected ListNode _getMiddleNode(ListNode node) {
    ListNode fast = node, slow = node;
    while (fast.next != null && fast.next.next != null) {
        fast = fast.next.next;
        slow = slow.next;
    }
    return slow;
}

protected ListNode _reverseList(ListNode node) {
    ListNode pre = null, cur = node;
    while (cur != null) {
        ListNode nxt = cur.next;
        cur.next = pre;
        pre = cur;
        cur = nxt;
    }
    return pre;
}
```
