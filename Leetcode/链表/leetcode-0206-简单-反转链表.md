# 0206. 反转链表

# 题目 
给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。

https://leetcode.cn/problems/reverse-linked-list/description/

# 示例：
```
示例 1：

输入：head = [1,2,3,4,5]
输出：[5,4,3,2,1]
```

```
示例 2：

输入：head = [1,2]
输出：[2,1]
```

```
示例 3：

输入：head = []
输出：[]
```

提示：
- 链表中节点的数目范围是 [0, 5000]
- -5000 <= Node.val <= 5000

# 解析
如果定义一个新的链表实现链表的翻转，则是对内存空间的浪费。其实只需要改变链表 next 指针的指向，直接将链表翻转即可。

## 双指针法
首先定义一个 cur 指针，指向头节点。再定义一个 pre 指针，初始化为 null。为什么是 null 呢？因为 pre 作为 cur 的前一个节点，在反转后的链表就是尾节点了，就是 null 了。

接下来开始反转。

首先使用 tmp 保存 cur->next 节点。为什么要保存这个节点呢？因为接下来要改变 cur->next 的指向，将 cur->next 指向 pre。

然后循环执行以上的罗，继续移动 pre 和 cur 指针。

最后 cur 指针指向了 null，循环结束，链表也反转完毕。此时我们返回 pre 指针即可。pre 指针指向了新的头节点。

## 递归
以 1 -> 2 -> 3 -> 4 为例，当前节点为 head，递归调用方法，递归停止条件为 head == null || head.next == null，返回当前节点 head，代码如下

```
if head == nil || head.Next == nil {
    return head
}
cur := reverseList2(head.Next)
head.Next.Next = head
head.Next = nil
return cur
```

流程：
1. 节点 1，调用方法记为 reverseList(node1)，节点 1 的 next 存在，开始递归
2. 节点 2，调用方法记为 reverseList(node2)，节点 2 的 next 存在，继续递归
3. 节点 3，调用方法记为 reverseList(node3)，节点 3 的 next 存在，继续递归
4. 节点 4，调用方法记为 reverseList(node4)，节点 4 的 next 不存在，返回当前节点4
5. 回到 reverseList(node3)，此时 head 为节点 3，cur 为节点 4。将节点 3 的 next 节点（节点4）的 next 指针指向自己（节点3），将自己的 next 指向空。
此时链表为 1 -> 2 -> 3 <- 4，返回节点 4
6. 回到 reverseList(node2)，此时 head 为节点 2，cur 为节点 4。将节点 2 的 next 节点（节点3）的 next 指针指向自己（节点2），将自己的 next 指向空。
此时链表为 1 -> 2 <- 3 <- 4，返回节点 4
7. 回到 reverseList(node1)，此时 head 为节点 1，cur 为节点 4。将节点 1 的 next 节点（节点2）的 next 指针指向自己（节点1），将自己的 next 指向空。
此时链表为 1 <- 2 <- 3 <- 4，返回节点 4
8. 此时已经没有了递归函数栈，返回节点 4，也就是反转后的链表头






# 代码

### php
```php

class LeetCode0206 {

    function reverseList($head) {
        if (!$head) {
            return null;
        }

        $prev = new ListNode(null);
        $cur = $head;
        while ($cur) {
            // 保存 cur 的后一节点
            $tmp = $cur->next;

            // cur节点的 next 指针指向 pre
            // 也就是从 pre -> cur 变成 pre <- cur
            $cur->next = $prev;

            // pre 和 cur 指针都向后移动
            // 注意要先移动 pre
            $prev = $cur;
            $cur = $tmp;
        }
        // 原来的头节点现在已经成了尾节点，所以next指针指向空（感觉可写可不写）
        $head->next = null;
        
        // 出循环的时候，cur 为空，pre 为原来的尾节点，返回 pre 即可
        return $prev;
    }
    
    // 递归
    function reverseList2($head)
    {
        if ($head == null || $head->next == null) {
            return $head;
        }
        
        $rev = $this->reverseList2($head->next);
        $head->next->next = $head;  // 设置当前节点的下一个节点的next指针指向自己
        $head->next = null;         // 由于递归，即使这里设置当前节点的next为null，也会再弹出函数栈时重新指向为前一个节点
        return $rev;
    }
}
```

### go
```go
// 双指针
func reverseList(head *ListNode) *ListNode {
    if head == nil {
        return head
    }
    prev := &ListNode{}
    cur := head
    for cur != nil {
        tmp := cur.Next
        cur.Next = prev
        prev = cur
        cur = tmp
    }
    head.Next = nil
        
    return prev
}

// 递归
func reverseList2(head *ListNode) *ListNode {
    if head == nil || head.Next == nil {
        return head
    }
    rev := reverseList2(head.Next)
    head.Next.Next = head
    head.Next = nil
    return rev
}
```

### java
```java
class LeetCode0206 {

    // 双指针
    public ListNode reverseList(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode prev = new ListNode();
        ListNode cur = head;
        while (cur != null) {
            ListNode tmp = cur.next;
            cur.next = prev;
            prev = cur;
            cur = tmp;
        }

        head.next = null;

        return prev;
    }
}
```

# 递归反转的图示
初始链表如下：

![](./images/leetcode-0206-递归图示1.png)


输入 reverse(head) 后，会在这里进行递归：
```php
$rev = $this->reverse($head->next);
```

![](./images/leetcode-0206-递归图示2.png)


根据函数定义，reverse 函数会返回反转之后的头结点，用变量 rev 接收了。

接下来的代码是：
```php
$head->next->next = $head
```

![](./images/leetcode-0206-递归图示3.png)





这个 reverse($head->next) 执行完成后，链表就变成了如下这样：

![](./images/leetcode-0206-递归图示4.png)


最后 
```php
$head->next = null;
return $res
```

![](./images/leetcode-0206-递归图示5.png)


这样整个链表就反转过来了





