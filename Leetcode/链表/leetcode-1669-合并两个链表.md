### 1669. 合并两个链表

# 题目
给你两个链表 list1 和 list2，它们包含的元素分别为 n 个和m 个。

请你将 list1 中下标从 a 到 b 的全部节点都删除，并将 list2 接在被删除节点的位置。

https://leetcode.cn/problems/merge-in-between-linked-lists

# 示例
```
输入：list1 = [0,1,2,3,4,5], a = 3, b = 4, list2 = [1000000,1000001,1000002]
输出：[0,1,2,1000000,1000001,1000002,5]
解释：删除 list1 中下标为 3 和 4 的两个节点，并将 list2 接在该位置。
```
```
输入：list1 = [0,1,2,3,4,5,6], a = 2, b = 5, list2 = [1000000,1000001,1000002,1000003,1000004]
输出：[0,1,1000000,1000001,1000002,1000003,1000004,6]
```

提示：
- 3 <= list1.length <= 104
- 1 <= a <= b < list1.length - 1
- 1 <= list2.length <= 104



# 解析

##### 快慢指针

&emsp;&emsp;快慢指针。快指针先走 b-a 步，然后慢指针再开始和快指针一起走 a 步。走的过程中，记录一下慢指针的前一个节点指针，用来后面做拼接用。

# 代码
```php
require './class/ListNode.class.php';
$list1 = [0, 1, 2, 3, 4, 5, 6];
$a     = 2;
$b     = 5;
$list2 = [1000000, 1000001, 1000002, 1000003, 1000004];
$head1 = array2LinkList($list1);
$head2 = array2LinkList($list2);
(new LeetCode1669())->main($list1, $a, $b, $list2);

class LeetCode1669
{

    function main($list1, $a, $b, $list2)
    {
        fetchNode($this->mergeInBetween($list1, $a, $b, $list2));
    }


    function mergeInBetween($list1, $a, $b, $list2)
    {
        $fast = $list1;     // 快指针
        $slow = $list1;     // 慢指针
        $prev = null;       // 慢指针的前一个节点

        for ($i = 0; $i < ($b - $a); $i++) {        // 快指针先走b-a步
            $fast = $fast->next;
        }

        for ($i = 0; $i < $a; $i++) {       // 快慢指针同时移动
            $prev = $slow;                  // 更新prev节点
            $slow = $slow->next;
            $fast = $fast->next;
        }

        $list2Tail = $this->_getTail($list2);    // 找到list2的尾节点
        if ($prev != null) {                    // a不是第一个节点的情况
            $prev->next      = $list2;
            $list2Tail->next = $fast->next;
            return $list1;
        } else {                                // a是第一个节点的情况
            $list2Tail->next = $fast;
            return $list2;
        }
    }

    // 找到链表的尾节点返回
    protected function _getTail($list){
        while($list->next!=null){
            $list=$list->next;
        }

        return $list;
    }

}
```