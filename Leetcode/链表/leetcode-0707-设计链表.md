# 707. 设计链表

# 题目
你可以选择使用单链表或者双链表，设计并实现自己的链表。

单链表中的节点应该具备两个属性：val 和 next 。val 是当前节点的值，next 是指向下一个节点的指针/引用。

如果是双向链表，则还需要属性 prev 以指示链表中的上一个节点。假设链表中的所有节点下标从 0 开始。


实现 MyLinkedList 类：
- MyLinkedList() 初始化 MyLinkedList 对象。
- int get(int index) 获取链表中下标为 index 的节点的值。如果下标无效，则返回 -1 。
- void addAtHead(int val) 将一个值为 val 的节点插入到链表中第一个元素之前。在插入完成后，新节点会成为链表的第一个节点。
- void addAtTail(int val) 将一个值为 val 的节点追加到链表中作为链表的最后一个元素。
- void addAtIndex(int index, int val) 将一个值为 val 的节点插入到链表中下标为 index 的节点之前。如果 index 等于链表的长度，那么该节点会被追加到链表的末尾。如果 index 比长度更大，该节点将 不会插入 到链表中。
- void deleteAtIndex(int index) 如果下标有效，则删除链表中下标为 index 的节点。

https://leetcode.cn/problems/design-linked-list/

提示：
- 0 <= index, val <= 1000
- 请不要使用内置的 LinkedList 库。
- 调用 get、addAtHead、addAtTail、addAtIndex 和 deleteAtIndex 的次数不超过 2000 。

# 示例
```
输入
["MyLinkedList", "addAtHead", "addAtTail", "addAtIndex", "get", "deleteAtIndex", "get"]
[[], [1], [3], [1, 2], [1], [1], [1]]
输出
[null, null, null, null, 2, null, 3]

解释
MyLinkedList myLinkedList = new MyLinkedList();
myLinkedList.addAtHead(1);
myLinkedList.addAtTail(3);
myLinkedList.addAtIndex(1, 2);    // 链表变为 1->2->3
myLinkedList.get(1);              // 返回 2
myLinkedList.deleteAtIndex(1);    // 现在，链表变为 1->3
myLinkedList.get(1);              // 返回 3
```

# 解析
这是练习链表操作非常好的一道题目，这六个接口覆盖了链表的常见操作。

采用设置虚拟头节点的方法实现这六个接口。

# 代码

### php
```php
class Leetcode0707 {

    private $_dummyHead;
    private $_size = 0;

    function __construct() {
        $this->_dummyHead = new ListNode(null);
    }

    /**
     * @param Integer $index
     * @return Integer
     */
    function get($index) {
        // 越界检测, index从0开始计算
        if ($index < 0 || $index > ($this->_size - 1)) {
            return -1;
        }
        $cur = $this->_dummyHead->next;
        while ($index--) {
            $cur = $cur->next;
        }
        echo $cur->val . PHP_EOL;
    }

    /**
     * 在链表最前面插入一个节点，插入完成后，新插入的节点为链表的新的头结点
     * @param Integer $val
     * @return NULL
     */
    function addAtHead($val) {
        $newNode = new ListNode($val);
        $newNode->next = $this->_dummyHead->next;
        $this->_dummyHead->next = $newNode;
        $this->_size++;
    }

    /**
     * 在链表最后面添加一个节点
     * @param Integer $val
     * @return NULL
     */
    function addAtTail($val) {
        $newNode = new ListNode($val);
        $cur = $this->_dummyHead;
        while ($cur->next) {
            $cur = $cur->next;
        }
        $cur->next = $newNode;
        $this->_size++;
    }

    /**
     * @param Integer $index
     * @param Integer $val
     * @return NULL
     */
    function addAtIndex($index, $val) {
        if ($index > $this->_size) {
            return ;
        }
        $newNode = new ListNode($val);
        $cur = $this->_dummyHead;
        while ($index--) {
            $cur = $cur->next;
        }
        $newNode->next = $cur->next;
        $cur->next = $newNode;
        $this->_size++;
    }

    /**
     * @param Integer $index
     * @return NULL
     */
    function deleteAtIndex($index)
    {
        if ($index >= $this->_size || $index < 0) {
            return;
        }
        $cur = $this->_dummyHead;
        while ($index--) {
            $cur = $cur->next;
        }
        $cur->next = $cur->next->next;
        $this->_size--;

    }
}

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * $obj = MyLinkedList();
 * $ret_1 = $obj->get($index);
 * $obj->addAtHead($val);
 * $obj->addAtTail($val);
 * $obj->addAtIndex($index, $val);
 * $obj->deleteAtIndex($index);
 */

```

### go
```go
type MyLinkedList struct {
    dummy *ListNode
    size  int
}


func Constructor() MyLinkedList {
    return MyLinkedList{dummy: &ListNode{}, size: 0}
}


func (this *MyLinkedList) Get(index int) int {
    if index < 0 || index > this.size - 1 {
        return -1
    }
    cur := this.dummy.Next
    for index > 0 {
        cur = cur.Next
        index--
    }
    return cur.Val
}


func (this *MyLinkedList) AddAtHead(val int)  {
    newNode := &ListNode{Val: val}
    newNode.Next = this.dummy.Next
    this.dummy.Next = newNode
    this.size++
}


func (this *MyLinkedList) AddAtTail(val int)  {
    newNode := &ListNode{Val: val}
    cur := this.dummy
    for cur.Next != nil {
        cur = cur.Next
    }
    cur.Next = newNode
    this.size++
}


func (this *MyLinkedList) AddAtIndex(index int, val int)  {
    if index > this.size {
        return
    }
    cur := this.dummy
    for index > 0 {
        cur = cur.Next
        index--
    }
    newNode := &ListNode{Val: val}
    newNode.Next = cur.Next
    cur.Next = newNode
    this.size++
}


func (this *MyLinkedList) DeleteAtIndex(index int)  {
    if index < 0 || index >= this.size {
        return
    }
    cur := this.dummy
    for index > 0 {
        cur = cur.Next
        index--
    }
    cur.Next = cur.Next.Next
    this.size--
}


/**
 * Your MyLinkedList object will be instantiated and called as such:
 * obj := Constructor();
 * param_1 := obj.Get(index);
 * obj.AddAtHead(val);
 * obj.AddAtTail(val);
 * obj.AddAtIndex(index,val);
 * obj.DeleteAtIndex(index);
 */
``` 

