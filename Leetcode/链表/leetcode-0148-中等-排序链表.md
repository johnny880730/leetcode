# 0148. 排序链表【中等】

# 题目 
给你链表的头结点 head ，请将其按 升序 排列并返回 排序后的链表 。

进阶：你可以在 O(nlogn) 时间复杂度和常数级空间复杂度下，对链表进行排序吗？

https://leetcode.cn/problems/sort-list/description/

提示：
- 链表中节点的数目在范围 [0, 5 * 10,000] 内
- -100,000 <= Node.val <= 100,000

# 示例：
```
示例 1：

输入：head = [4,2,1,3]
输出：[1,2,3,4]
```

```
示例 2：

输入：head = [-1,5,3,4,0]
输出：[-1,0,3,4,5]
```

```
示例 3：

输入：head = []
输出：[]
```

# 解析

题目的进阶问题要求达到 O(nlogn) 的时间复杂度和 O(1) 的空间复杂度，时间复杂度是 O(nlogn) 的排序算法包括【归并排序】、【堆排序】和【快速排序】
（快速排序的最差时间复杂度是 O(n²)，其中最适合链表的排序算法是归并排序。

归并排序基于分治算法。最容易想到的实现方式是自顶向下的递归实现，考虑到递归调用的栈空间，自顶向下归并排序的空间复杂度是 O(logn)。
如果要达到 O(1) 的空间复杂度，则需要使用自底向上的实现方式。


## 自底向上归并排序
使用自底向上的方法实现归并排序，则可以达到 O(1) 的空间复杂度。其中合并的基本操作如下：
1. 长度为 1 的链表和长度为 1 的链表合并后，形成一个长度为 2 的链表
2. 长度为 2 的链表和长度为 2 的链表合并后，形成一个长度为 4 的链表
3. ……

而由于合并过程中操作的是链表，所以需要有【断链】和【重新连接】的过程。

![](./images/leetcode-0148-img1.png)

具体操作步骤如下：

1、先获取链表长度，基于这个长度才能知道后续合并到什么时候截止

2、设置三个指针 prev、 cur、 next
  - prev 表示已经排序好的链表的【尾节点】。
  - cur 一开始设置为准备排序的那些节点的【首节点】，然后向后移动，获取相应的节点，到达所有正在准备排序的那些节点的【尾节点】位置。
  - next 表示下一批需要排序的那些节点的【首节点】。

![](./images/leetcode-0148-img2.png)

3、断开 prev 与 cur 的连接，再断开 cur 与 next 的连接。

![](./images/leetcode-0148-img3.png)

4、把 cur 访问的这些节点划分为两个区域，区域的长度取决于此时进行到了长度为多少的链表进行合并操作，一个是左链表，一个是右链表，把这两个链表进行合并操作

![](./images/leetcode-0148-img4.png)

5、合并成功之后，prev 移动到尾部，cur 来到 next 的位置，继续后面的归并操作。

6、这样一轮下来，已经把长度为 2 的链表和长度为 2 的链表合并，形成了一个长度为 4 的链表。

![](./images/leetcode-0148-img5.png)

7、接下来，只需要执行上述同样的操作，唯一的修改点在于合并的子链表长度变成了 4。

![](./images/leetcode-0148-img6.png)

完整流程如下：

![](./images/leetcode-0148-题解.gif)

# 代码

### php
```php

class LeetCode0148 {

    function sortList($head) {

        // 链表的总长度
        $len = 0;

        // 从链表的头节点开始访问
        $node = $head;

        // 利用 while ，可以统计出节点的个数
        while ($node != null) {
            $len++;
            $node = $node->next;    
        }

        // 设置虚拟头节点
        $dummy = new ListNode(-1, $head);

        // 利用for循环，执行合并的排序
        // 长度为 1 的链表和长度为 1 的链表合并后，形成一个长度为 2 的链表
        // 长度为 2 的链表和长度为 2 的链表合并后，形成一个长度为 4 的链表
        // 长度为 4 的链表和长度为 4 的链表合并后，形成一个长度为 8 的链表
        // 也有可能长度为 8 的链表和长度为 5 的链表，形成一个长度为 13 的链表
        for ($subLen = 1; $subLen < $len; $subLen *= 2) {
            // 归并过程分成三个步骤
            // 1. 不停的划分，直到无法划分为止
            // 2. 两两合并
            // 3. 每次合并之后的结果需要连接起来

            // 每次都把结果连接到 dummy，因此先记录一下
            // prev 表示已经排序好的链表的【尾节点】
            $prev = $dummy;

            // dummy 后面的节点才是原链表的节点，需要把它们进行划分
            // cur 表示正在排序的那些节点的【尾节点】，先初始化
            $cur = $dummy->next;

            // while 循环寻找出划分后子链表的头节点
            while ($cur != null) {
                // 每次都是两个子链表进行合并

                // 1. 先寻找出【左子链表】，长度为 subLen
                $head1 = $cur;

                // 通过 for 循环，找出 subLen 个节点
                // cur 索引为 0 ，需要再寻找 subLen - 1 个节点
                for ($i = 1; $i < $subLen && $cur->next != null; $i++) {
                    $cur = $cur->next;        
                }

                // 2. 再寻找出【右子链表】，长度最多为 subLen，甚至有可能为 0 
                $head2 = $cur->next;

                // 找到后将【左子链表】与【右子链表】链接断开
                $cur->next = null;

                // cur 来到【右子链表】头部
                $cur = $head2;

                // 通过 for 循环，找出【右子链表】的那些节点
                // 【右子链表】的节点数可能达不到 subLen，甚至 1 个或 0 个
                for ($i = 1; $i < $subLen && $cur != null && $cur->next != null; $i++) {
                    $cur = $cur->next;
                }
                
                // 获取到【右子链表】之后，需要把它和后续断开
                // next 变量表示接下来要排序的那些节点的头节点
                $next = null;

                // 如果 cur != null，说明【右子链表】的个数达到了 subLen 个，并且还有后续节点
                if ($cur != null) {

                    // 记录一下后面节点
                    $next = $cur->next;

                    // 再将【右子链表】与 next 断开
                    $cur->next = null;
                }

                // 将【左子链表】与【右子链表】合并
                $merged = $this->_merge2List($head1, $head2);

                // 合并之后的结果需要连接到前一个链表
                $prev->next = $merged;

                // prev来到链表的尾部，是下一个即将合成链表之后的前一个链表的尾节点
                while ($prev->next != null) {
                    $prev = $prev->next;
                }

                // cur 来到 next ，处理后面的节点
                $cur = $next;
            }
        }

        return $dummy->next;
    }

    // 合并两个已排序的链表
    private function _merge2List($l1, $l2) {
        $dummy = new ListNode(null);
        $cur = $dummy;
        while ($l1 != null && $l2 != null) {
            if ($l1->val <= $l2->val) {
                $cur->next = $l1;
                $l1 = $l1->next;
            } else {
                $cur->next = $l2;
                $l2 = $l2->next;
            }
            $cur = $cur->next;
        }
        $cur->next = $l1 == null ? $l2 : $l1;
        return $dummy->next;
    }
}

```

### go
```go
func sortList(head *ListNode) *ListNode {
    length := 0
    node := head

    for node != nil {
        length++
        node = node.Next
    }
    dummy := &ListNode{}
    dummy.Next = head

    for subLen := 1; subLen < length; subLen *= 2 {
        prev := dummy
        cur := dummy.Next

        for cur != nil {
            head1 := cur
            for i := 1; i < subLen && cur.Next != nil; i++ {
                cur = cur.Next
            }

            head2 := cur.Next
            cur.Next = nil
            cur = head2

            for i := 1; i < subLen && cur != nil && cur.Next != nil; i++ {
                cur = cur.Next
            }
            
            var next *ListNode

            if cur != nil {
                next = cur.Next
                cur.Next = nil
            }

            merged := _merge2List(head1, head2)
            prev.Next = merged

            for prev.Next != nil {
                prev = prev.Next
            }

            cur = next;
        }
    }

    return dummy.Next
}


func _merge2List(head1 *ListNode, head2 *ListNode) *ListNode {
    dummy := &ListNode{Val: 0}
    cur := dummy
    for head1 != nil && head2 != nil {
        if head1.Val <= head2.Val {
            cur.Next = head1
            head1 = head1.Next
        } else {
            cur.Next = head2
            head2 = head2.Next
        }
        cur = cur.Next
    }
    if head1 == nil {
        cur.Next = head2
    } else {
        cur.Next = head1
    }
    return dummy.Next
}
```

### java
```java
public ListNode sortList(ListNode head) {
    int len = _getListLen(head);

    ListNode dummy = new ListNode(-1, head);
    for (int subLen = 1; subLen < len; subLen *= 2) {
        ListNode prev = dummy;
        ListNode cur = prev.next;

        while (cur != null) {
            ListNode head1 = cur;
            for (int i = 1; i < subLen && cur.next != null; i++) {
                cur = cur.next;
            }

            ListNode head2 = cur.next;
            cur.next = null;
            cur = head2;
            for (int i = 1; i < subLen && cur != null && cur.next != null; i++) {
                cur = cur.next;
            }

            ListNode next = null;
            if (cur != null) {
                next = cur.next;
                cur.next = null;
            }

            ListNode merged = _merge2List(head1, head2);
            prev.next = merged;
            while (prev.next != null) {
                prev = prev.next;
            }
            cur = next;
        }
    }

    return dummy.next;
    
}

private int _getListLen(ListNode head) {
    int len = 0;
    ListNode node = head;
    while (node != null) {
        len++;
        node = node.next;
    }
    return len;
}

private ListNode _merge2List(ListNode h1, ListNode h2) {
    ListNode dummy = new ListNode();
    ListNode cur = dummy;
    while (h1 != null && h2 != null) {
        if (h1.val <= h2.val) {
            cur.next = h1;
            h1 = h1.next;
        } else {
            cur.next = h2;
            h2 = h2.next;
        }
        cur = cur.next;
    }
    cur.next = h1 == null ? h2 : h1;
    return dummy.next;
}
```
