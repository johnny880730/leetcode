# 0086. 分隔链表

# 题目
给你一个链表的头节点 head 和一个特定值 x ，请你对链表进行分隔，使得所有 小于 x 的节点都出现在 大于或等于 x 的节点之前。

你应当 保留 两个分区中每个节点的初始相对位置。

https://leetcode.cn/problems/partition-list/description/

提示：
- 链表中节点的数目在范围 [0, 200] 内
- -100 <= Node.val <= 100
- -200 <= x <= 200

# 示例
```
输入：head = [1,4,3,2,5,2], x = 3
输出：[1,2,2,4,3,5]
```
```
输入：head = [2,1], x = 2
输出：[1,2]
```

# 解析


# 代码
直观来说只需维护两个链表 small 和 large 即可，small 链表按顺序存储所有小于 x 的节点，large 链表按顺序存储所有大于等于 x 的节点。

遍历完原链表后，只要将 small 链表尾节点指向 large 链表的头节点即能完成对链表的分隔。

为了实现上述思路，设 smallDummy 和 largeDummy 分别为两个链表的虚拟头节点，这样做的目的是为了更方便地处理头节点为空的边界条件。

同时设 small 和 large 节点指向对应链表的头节点。开始时 small = smallDummy，large = largeDummy。随后，从前往后遍历链表，
判断当前链表的节点值是否小于 x，如果小于就将 small 的 next 指针指向该节点，否则将 large 的 next 指针指向该节点。

遍历结束后，将 large 的 next 指针置空，这是因为当前节点复用的是原链表的节点，而其 next 指针可能指向一个小于 x 的节点，
需要切断这个引用。同时将 small 的 next 指针指向 largeDummy 的 next 指针指向的节点，即真正意义上的 large 链表的头节点。
最后返回 smallHead 的 next 指针即为要求的答案。

# 代码

### php
```php
class LeetCode0086 {

    function partition($head, $x) {
        $smallDummy = new ListNode(0);
        $largeDummy = new ListNode(0);
        $small = $smallDummy;
        $large = $largeDummy;
        while ($head) {
            if ($head->val < $x) {
                $small->next = $head;
                $small = $small->next;
            } else {
                $large->next = $head;
                $large = $large->next;
            }
            $head = $head->next;
        }
        $large->next = null;
        $small->next = $largeDummy->next;

        return $smallDummy->next;
    }

}
```

### go
```go
func partition(head *ListNode, x int) *ListNode {
    smallDummy := &ListNode{}
    largeDummy := &ListNode{}
    small, large := smallDummy, largeDummy
    for head != nil {
        if head.Val < x {
            small.Next = head
            small = small.Next
        } else {
            large.Next = head
            large = large.Next
        }
        head = head.Next
    }
    large.Next = nil
    small.Next = largeDummy.Next

    return smallDummy.Next
}
```

### java
```java
class LeetCode0086 {

    public ListNode partition(ListNode head, int x) {
        ListNode smallDummy = new ListNode(0);
        ListNode largeDummy = new ListNode(0);
        ListNode small = smallDummy;
        ListNode large = largeDummy;
        while (head != null) {
            if (head.val < x) {
                small.next = head;
                small = small.next;
            } else {
                large.next = head;
                large = large.next;
            }
            head = head.next;
        }
        large.next = null;
        small.next = largeDummy.next;
        
        return smallDummy.next;
    }
}
```