# 328. 奇偶链表

# 题目
给定单链表的头节点 head，将所有索引为奇数的节点和索引为偶数的节点分别组合在一起，然后返回重新排序的列表。

第一个节点的索引被认为是 奇数 ， 第二个节点的索引为偶数 ，以此类推。

请注意，偶数组和奇数组内部的相对顺序应该与输入时保持一致。

你必须在 O(1) 的额外空间复杂度和 O(n) 的时间复杂度下解决这个问题。

https://leetcode.cn/problems/odd-even-linked-list

# 示例
```
输入: head = [1,2,3,4,5]
输出: [1,3,5,2,4]
```
```
输入: head = [2,1,3,5,6,4,7]
输出: [2,3,6,7,1,5,4]
```

# 解析
分离节点后合并

对于原始链表，每个节点都是奇数节点或偶数节点。头节点是奇数节点，头节点的后一个节点是偶数节点，相邻节点的奇偶性不同。
因此可以将奇数节点和偶数节点分离成奇数链表和偶数链表，然后将偶数链表连接在奇数链表之后，合并后的链表即为结果链表。

原始链表的头节点 head 也是奇数链表的头节点以及结果链表的头节点，head 的后一个节点是偶数链表的头节点。令 evenHead = head.next，
则 evenHead 是偶数链表的头节点。

维护两个指针 odd 和 even 分别指向奇数节点和偶数节点，初始时 odd = head，even = evenHead。
通过迭代的方式将奇数节点和偶数节点分离成两个链表，每一步首先更新奇数节点，然后更新偶数节点。
1. 更新奇数节点时，奇数节点的 next 指针需要指向偶数节点的后一个节点，因此令 odd.next = even.next，
   然后令 odd = odd.next，此时 odd 变成 even 的后一个节点。
2. 更新偶数节点时，偶数节点的 next 指针需要指向奇数节点的后一个节点，因此令 even.next = odd.next，
   然后令 even = even.next，此时 even 变成 odd 的后一个节点。

在上述操作之后，即完成了对一个奇数节点和一个偶数节点的分离。重复上述操作，直到全部节点分离完毕。
全部节点分离完毕的条件是 even 为空节点或者 even.next 为空节点，此时 odd 指向最后一个奇数节点（即奇数链表的最后一个节点）。

最后令 odd.next = evenHead，将偶数链表连接在奇数链表之后，即完成了奇数链表和偶数链表的合并，结果链表的头节点仍然是 head。


# 代码

### php
```php
class LeetCode0024 {

    function oddEvenList($head) {
        // 边界情况处理，如果为空或者只有一个节点，返回 head 即可
        if ($head == null || $head->next == null) {
            return $head;
        }
        
        // 设置一个指针，指向头节点， odd 代表奇数节点
        $odd = $head;
        
        // 设置一个指针，指向头节点的下一个节点， even 代表偶数节点
        $evenHead = $head->next;
        
        // 设置一个指针，指向偶数节点的头节点，最终会让奇数节点的尾节点的 next 指向它
        $even = $evenHead;
        
        // 从偶数节点的头节点开始遍历
        // 如果当前节点为空，或者后一节点为空，说明链表已经遍历完毕
        while ($even && $even->next){
            // even.next 必然是奇数节点
            // 所以让 odd 的 next 指针指向 even.next 这个奇数节点
            // 这样 odd 就都是奇数了
            $odd->next = $even->next;
            
            // 让 odd 移动到最新的由奇数节点组成的链表的尾部
            $odd = $odd->next;
            
            // 此时 odd.next 必然是偶数节点
            // 所以让 even 的 next 指向 even.next 这个偶数节点
            $even->next = $odd->next;
            
            // 让 even 移动到最新的由偶数节点组成的链表的尾部
            $even = $even->next;
        }
        // 此时链表已遍历完毕，odd 上都是奇数节点，even 都是偶数节点
        // 根据题目要求 奇数节点在偶数节点之前
        // 所以让奇数节点的尾节点的 next 指针指向第一个偶数节点
        $odd->next = $evenHead;
        
        // 返回原链表的头节点即可
        // 因为头节点没有发生过变化，它是奇数节点，也是第一个奇数节点
        return $head;
    }
}
```

### go
```go
func oddEvenList(head *ListNode) *ListNode {
    if head == nil || head.Next == nil {
        return head
    }
    odd, even := head, head.Next
    evenHead := even
    for even != nil && even.Next != nil {
        odd.Next = even.Next
        odd = odd.Next
        even.Next = odd.Next
        even = even.Next
    }
    odd.Next = evenHead
    return head
}
```