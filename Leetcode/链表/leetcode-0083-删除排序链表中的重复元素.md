# 83. 删除排序链表中的重复元素

# 题目
存在一个按升序排列的链表，给你这个链表的头节点 head ，请你删除所有重复的元素，使每个元素 只出现一次 。

返回同样按升序排列的结果链表。

https://leetcode.cn/problems/remove-duplicates-from-sorted-list/

# 示例：
```
输入：head = [1,1,2]
输出：[1,2]
```

# 解析

## 普通遍历
由于给定的链表是排好序的，因此重复的元素在链表中出现的位置是连续的，因此我们只需要对链表进行一次遍历，就可以删除重复的元素。

具体地，指针 cur 指向链表的头节点，随后开始对链表进行遍历。如果当前 cur 与 cur.next 对应的元素相同，那么我们就将 cur.next 从链表中移除；
否则说明链表中已经不存在其它与 cur 对应的元素相同的节点，因此可以将 cur 指向 cur.next。

当遍历完整个链表之后，我们返回链表的头节点即可。

## 快慢指针
如同 [0026. 删除有序数组中的重复项](../top-interview-questions/leetcode-0026-简单-删除有序数组中的重复项.md) 一样，0026 是用的数组，
而本题是链表。也是用快慢指针来处理。

区别就是原本是数组赋值操作改成了操作链表。

来自 labuladong 的图示：

![](./images/leetcode-0083-题解.gif)

# 代码

### php
```php
class LeetCode0083 {

    function deleteDuplicates($head) {
        // 从链表的头节点开始访问每个节点
        $cur = $head;
        
        // 访问过程中，只要当前节点和下一个节点有值，就不断访问下去
        while ($cur != null && $cur->next != null) {
            // 当前节点和下一个节点有两种关系
            if ($cur->val == $cur->next->val) {
                // 1. 值相同，此时要删除重复元素。由于已经是排序链表，所以只需要跳过后面这个重复的节点即可
                $cur->next = $cur->next->next;
            } else {
                // 2. 值不相同，那么 cur 这个节点可以保留，继续访问后面的节点
                $cur = $cur->next;
            }
        }
        
        // 返回链表的头节点就是结果
        return $head;
    }
    
    // 快慢指针
    function deleteDuplicates2($head) {
         if (!$head) {
            return null;
         }
         $fast = $slow = $head;
         while ($fast) {
            if ($fast->val != $slow->val) {
                // nums[slow] = nums[fast]
                $slow->next = $fast;
                // slow++
                $slow = $slow->next;
            }
            // fast++
            $fast = $fast->next;
         }
         // 断开slow与后面的关联
         $slow->next = null;
         return $head;
    }
}
```

### go
```go
func deleteDuplicates(head *ListNode) *ListNode {
    cur := head
    for cur != nil && cur.Next != nil {
        if cur.Val == cur.Next.Val {
            cur.Next = cur.Next.Next
        } else {
            cur = cur.Next
        }
    }
    return head
}
```
