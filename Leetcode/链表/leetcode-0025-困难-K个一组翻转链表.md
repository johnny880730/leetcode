# 0025. K 个一组翻转链表【困难】

# 题目
给你一个链表，每 k 个节点一组进行翻转，请你返回翻转后的链表。

k 是一个正整数，它的值小于或等于链表的长度。

如果节点总数不是 k 的整数倍，那么请将最后剩余的节点保持原有顺序。

进阶：

你可以设计一个只使用常数额外空间的算法来解决此问题吗？
你不能只是单纯的改变节点内部的值，而是需要实际进行节点交换。

https://leetcode.cn/problems/reverse-nodes-in-k-group/description/

# 示例
```
输入：head = [1,2,3,4,5], k = 2
输出：[2,1,4,3,5]
```
```
输入：head = [1,2,3,4,5], k = 3
输出：[3,2,1,4,5]
```

提示：
- 列表中节点的数量在范围 sz 内
- 1 <= sz <= 5000
- 0 <= Node.val <= 1000
- 1 <= k <= sz

# 解析

1、对于原链表来说，除了头结点之外，其余的所有结点都有前面一个结点指向它，那么为了避免在操作这些结点（比如移动、删除操作）过程中需要判断当前结点是否是头结点，可以在原链表的前面增加一个虚拟结点，这样就可以使得原链表里面的所有结点的地位都是一样的了。

2、接下来，设置两个指针，都指向虚拟头结点的位置，一个指针是 pre，它在后续的操作过程中会始终指向每次要翻转的链表的头结点的【上一个结点】，
另外一个指针是 end，它在后续的操作过程中会始终指向每次要翻转的链表的尾结点。

3、以 k 为 3 作为示例，end 不断的向后移动，当 end 来到 3 这个结点的时候，已经寻找出来需要翻转的那 3 个结点，
并且 pre 依旧指向 dummy，因为 1 这个结点是翻转区域的头结点，前面那个则是上一个结点，因此 pre 还停留在原地。

![](./images/leetcode-0025-img1.png)

4、那么需要去翻转 1 、2 、3 这个结点，为了避免翻转过程会影响其它区域的结点，这个时候就需要把翻转区域和其它区域断开连接，
同时又为了保证翻转成功之后能顺利连接回去，因此需要记录一下后续的结点，这个结点也就是 end->next ，接下来再断开准备翻转链表区域的前后区域，
如下图所示。

![](./images/leetcode-0025-img2.png)

5、而 1 、2 、3 的翻转过程可以直接套用反转链表这题的思路和代码。

6、该区域的结点翻转成功之后，连接回去，回到第 2 点提到的 pre 这个指针是始终指向每次要翻转的链表的头结点的【上一个结点】，实际上也就是已经翻转区域的尾结点位置，所以 pre 需要来到 end 的位置，即 1 这个结点位置。

![](./images/leetcode-0025-img3.png)

7、接下来，继续让 end 向后移动，找到 k 个结点来。

![](./images/leetcode-0025-img4.png)

8、每次 end 寻找出 k 个结点之后，都会执行同样的逻辑，记录接下来需要翻转区域的头结点， 断开前后的区域，翻转本区域的结点，再让 pre 来到 end 的位置，end 来到 start 的位置。

9、最后，直到 end 遍历不到 k 个结点或者指向了 null 的时候也就完成了整个翻转过程。

# 代码

### php
```php
class LeetCode0025 {

    function reverseKGroup($head, $k) {
        // 一开始设置一个虚拟结点，它的值为 -1，它的值可以设置为任何的数，因为我们根本不需要使用它的值
        $dummy = new ListNode(-1);

        // 虚拟头结点的下一结点指向 head 结点
        // 如果原链表是  1 -->  2 -->  3
        // 那么加上虚拟头结点就是  -1 -->  1 -->  2 -->  3
        $dummy->next = $head;

        // 设置一个指针，指向此时的虚拟结点，pre 表示每次要翻转的链表的头结点的【上一个结点】
        // pre: -1 -->  1 -->  2 -->  3
        $pre = $dummy;

        // 设置一个指针，指向此时的虚拟结点，end 表示每次要翻转的链表的尾结点
        // end: -1 -->  1 -->  2 -->  3
        $end = $dummy;

        // 通过 while 循环，不断的找到翻转链表的尾部
        while ( $end->next != null) {

            // 通过 for 循环，找到【每一组翻转链表的尾部】
            // 由于原链表按照 k 个一组进行划分会可能出现有一组的长度不足 k 个
            // 比如原链表 1 -->  2 -->  3 -->  4 -->  5
            // k = 2，划分了三组 1 -->  2， 3 -->  4， 5
            // 所以得确保 end 不为空才去找它的 next 指针，否则 null->next 会报错
            for ($i = 0 ; $i < $k && $end != null ; $i++) {
                // end 不断的向后移动，移动 k 次到达【每一组翻转链表的尾部】
                $end = $end->next ;
            }

            // 如果发现 end == null，说明此时翻转的链表的结点数小于 k ，保存原有顺序就行
            if ($end == null) {
                // 直接跳出循环，只执行下面的翻转操作
                break;
            }


            // next 表示【待翻转链表区域】后面的第一个结点
            $next = $end->next;

            // 【待翻转链表区域】的最尾部next指针先断开
            $end->next = null ;

            // start 表示【待翻转链表区域】里面的第一个结点
            $start = $pre->next;

            // 【待翻转链表区域】的最头部结点和前面断开
            $pre->next = null;

            // 这个时候，【待翻转链表区域】的头结点是 start，尾结点是 end
            // 开始执行【反转链表】操作
            // 原先是 start --> ...--> end
            // 现在变成了 end --> ...--> start

            // 要翻转的链表的头结点的【上一个结点】的 next 指针指向这次翻转的结果
            $pre->next = $this->_reverseList($start);

            // 接下来的操作是在为【待翻转链表区域】的反转做准备

            // 原先是 start --> ...--> end
            // 现在已经变成了 end --> ...--> start
            // 【已翻转链表区域】里面的尾结点的 next 指针指向【下一个待翻转链表区域】里面的第一个结点
            $start->next = $next ;

            // 原先是 start --> ...--> end
            // 现在已经变成了 end --> ...--> start
            // pre 表示每次要翻转的链表的头结点的【上一个结点】
            $pre = $start;

            // 将 end 重置为【待翻转链表区域】的头结点的上一个结点。
            $end = $start;
        }
        return $dummy->next;
    }

    // 翻转链表 同【Leetcode0206】
    private function _reverseList($node) {
        if ($node == null || $node->next == null) {
            return $node;
        }
        $prev = new ListNode();
        $cur = $node;
        while ($cur) {
            $tmp = $cur->next;
            $cur->next = $prev;
            $prev = $cur;
            $cur = $tmp;
        }
        return $prev;
    }
}
```

### go
```go
func reverseKGroup(head *ListNode, k int) *ListNode {
    dummy := &ListNode{Val: -1}
    dummy.Next = head
    pre := dummy
    end := dummy
    for end.Next != nil {
        for i := 0; i < k && end != nil; i++ {
            end = end.Next
        }
        if end == nil {
            break
        }
        next := end.Next
        end.Next = nil
        start := pre.Next
        pre.Next = nil
        pre.Next = _reverseList(start)
        start.Next =  next
        
        pre = start
        end = start;
    }
    return dummy.Next
}

func _reverseList(head *ListNode) *ListNode {
    if head == nil {
        return head
    }
    prev := &ListNode{}
    cur := head
    for cur != nil {
        tmp := cur.Next
        cur.Next = prev
        prev = cur
        cur = tmp
    }
    head.Next = nil
        
    return prev
}
```

### java
```java
public ListNode reverseKGroup(ListNode head, int k) {
    ListNode dummy = new ListNode(-1, head);
    ListNode pre = dummy, end = dummy;
    while (end.next != null) {
        for (int i = 0; i < k && end != null; i++) {
            end = end.next;
        }
        if (end == null) {
            break;
        }

        ListNode nxt = end.next;
        end.next = null;

        ListNode start = pre.next;
        pre.next = null;

        pre.next = _reverseList(start);

        start.next = nxt;

        pre = start;

        end = start;
    }

    return dummy.next;
}

protected ListNode _reverseList(ListNode head) {
    if (head == null) {
        return null;
    }
    ListNode prev = new ListNode();
    ListNode cur = head;
    while (cur != null) {
        ListNode tmp = cur.next;
        cur.next = prev;
        prev = cur;
        cur = tmp;
    }

    head.next = null;

    return prev;
}
```
