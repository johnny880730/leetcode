# 0024. 两两交换链表中的节点【中等】

# 题目
给定一个链表，两两交换其中相邻的节点，并返回交换后的链表。

你不能只是单纯的改变节点内部的值，而是需要实际的进行节点交换。

https://leetcode.cn/problems/swap-nodes-in-pairs/description/

提示：
- 链表中节点的数目在范围 [0, 100] 内
- 0 <= Node.val <= 100

# 示例
```
示例 1：

输入：head = [1,2,3,4]
输出：[2,1,4,3]
```
```
示例 2：

输入：head = []
输出：[]
```

# 解析

## ① 递归
可以通过递归的方式实现两两交换链表中的节点。

递归的终止条件是链表中没有节点，或者链表中只有一个节点，此时无法进行交换。

如果链表中至少有两个节点，则在两两交换链表中的节点之后，原始链表的头节点变成新的链表的第二个节点，原始链表的第二个节点变成新的链表的头节点。
链表中的其余节点的两两交换可以递归地实现。在对链表中的其余节点递归地两两交换之后，更新节点之间的指针关系，即可完成整个链表的两两交换。

用 head 表示原始链表的头节点（新的链表的第二个节点），用 newHead 表示新的链表的头节点（原始链表的第二个节点），
则原始链表中的其余节点的头节点是 newHead.next。令 head.next = swapPairs(newHead.next)，表示将其余节点进行两两交换，
交换后的新的头节点为 head 的下一个节点。然后令 newHead.next = head，即完成了所有节点的交换。最后返回新的链表的头节点 newHead。

## ② 遍历
创建虚拟头结点 dummyHead，令 dummyHead.next = head。令 cur 表示当前到达的节点，初始时 cur = dummyHead。每次需要交换 cur 后面的两个节点。

如果 cur 的后面没有节点或者只有一个节点，则没有更多的节点需要交换，因此结束交换。否则，获得 cur 后面的两个节点 node1 和 node2，通过更新节点的指针关系实现两两交换节点。

具体而言，交换之前的节点关系是 cur -> node1 -> node2，交换之后的节点关系要变成 cur -> node2 -> node1，因此需要进行如下操作。
```
cur.next = node2
node1.next = node2.next
node2.next = node1
```

完成上述操作之后，节点关系即变成 cur -> node2 -> node1。再令 cur = node1，对链表中的其余节点进行两两交换，直到全部节点都被两两交换。

两两交换链表中的节点之后，新的链表的头节点是 dummyHead.next，返回新的链表的头节点即可

![](./images/leetcode-0024-解析.png)

# 代码

### php
```php
class LeetCode0024 {

    // 递归
    function swapPairs2($head) {
        if ($head == null || $head->next == null) {
            return $head;
        }
        $newHead = $head->next;
        $head->next = $this->swapPairs($newHead->next);
        $newHead->next = $head;
        return $newHead;
    }

    // 遍历
    function swapPairs($head) {
        // dummy -> 1 -> 2 -> 3 -> 4 -> null
        $dummyHead = new ListNode(0, $head);
        $cur = $dummyHead;
        while ($cur->next != null && $cur->next->next != null) {
            // node1 是 节点 1、node2 是 节点 2
            $node1 = $cur->next;
            $node2 = $cur->next->next;

            // 将 cur 的 next 指向节点 2
            // 当前就是 dummy 和 节点 1 都指向节点 2【dummy -> 2、1 -> 2】
            $cur->next = $node2;

            // node2（也就是节点 2）的 next 是节点 3，把节点 1 的 next 指向节点 3【1 -> 3】
            $node1->next = $node2->next;

            // 把节点 2 指向节点 1【2 -> 1】
            // 变成了 dummy -> 2 -> 1 -> 3 -> 4
            $node2->next = $node1;

            // 将 cur 更新成节点 1，继续后面两个节点 3 和 4 的交换
            $cur = $node1;
        }
        return $dummyHead->next;
    }
}
```

### go
```go
func swapPairs(head *ListNode) *ListNode {
    dummyHead := &ListNode{0, head}
    cur := dummyHead
    for cur.Next != nil && cur.Next.Next != nil {
        node1 := cur.Next           //取出节点1
        node2 := cur.Next.Next      //取出节点2
        cur.Next = node2            //cur.Next指向节点2
        node1.Next = node2.Next     //节点1.Next指向节点2.Next，也就是节点3或者null
        node2.Next = node1          //节点2.Next指向节点1
        cur = node1                 //更新cur到节点1
    }
    return dummyHead.Next
}
```

### java
```java
public ListNode swapPairs(ListNode head) {
    ListNode dummy = new ListNode(0, head);
    ListNode cur = dummy;
    while (cur.next != null && cur.next.next != null) {
        ListNode n1 = cur.next;
        ListNode n2 = cur.next.next;
        cur.next = n2;
        n1.next = n2.next;
        n2.next = n1;
        cur = n1;
    }
    return dummy.next;
}
```
