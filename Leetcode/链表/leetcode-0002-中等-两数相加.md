# 0002. 两数相加【中等】

# 题目
给你两个非空 的链表，表示两个非负的整数。它们每位数字都是按照逆序的方式存储的，并且每个节点只能存储一位数字。

请你将两个数相加，并以相同形式返回一个表示和的链表。

你可以假设除了数字 0 之外，这两个数都不会以 0 开头。

https://leetcode.cn/problems/add-two-numbers/description/

# 示例
```
示例 1：

输入：l1 = [2,4,3], l2 = [5,6,4]
输出：[7,0,8]
解释：342 + 465 = 807.
```

```
示例 2：

输入：l1 = [0], l2 = [0]
输出：[0]
```

```
示例 3：

输入：l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
输出：[8,9,9,9,0,0,0,1]
```


# 解析

由于输入的两个链表都是逆序存储数字的位数的，**因此个位数字就在链表头，方便进行相加运算**，因此两个链表中同一位置的数字可以直接相加。

同时遍历两个链表，逐位计算它们的和，并与当前位置的进位值相加。

具体而言，如果当前两个链表处相应位置的数字为 n1、n2，进位值为 carry，则它们的和为 n1 + n2 + carry

其中，答案链表处相应位置的数字为 (n1 + n2 + carry) % 10，

而新的进位值为 floor( (n1 + n2 + carry) / 10 )

如果两个链表的长度不同，则可以认为长度短的链表的后面有若干个 0 。

此外，如果链表遍历结束后，有 carry > 0，还需要在答案链表的后面附加一个节点，节点的值为 carry。

![](./images/leetcode-0002-题解.gif)

# 代码

### php
```php
class LeetCode0002 {

    function addTwoNumbers($l1, $l2) {
        // 虚拟头节点
        $dummy = new ListNode();

        // l1 和 l2 有可能为空，所以先默认结果链表从虚拟头结点位置开始
        $cur = $dummy;

        // 设置一个进位，初始化为 0
        // 两个个位数相加，进位只能是 1 或者 0
        // 比如 7 + 8 = 15，进位是 1
        // 比如 2 + 3 = 6，没有进位，或者说进位是 0
        $carry = 0;

        // 同时遍历 l1 和 l2
        // 只要此时 l1 和 l2 两个链表中的任意链表中节点有值，就一直加下去
        // 直到两个链表中的所有节点都遍历完毕为止
        while ($l1 || $l2) {
            // l1 节点的值
            $n1 = $l1 != null ? $l1->val : 0;

            // l2 节点的值
            $n2 = $l2 != null ? $l2->val : 0;

            // 每一位计算的同时需要考虑上一位的进位情况
            $sum = $n1 + $n2 + $carry;

            // 进位情况
            $carry = floor($sum / 10);

            // 构建一个节点用来存放，并放入结果链表
            $cur->next = new ListNode($sum % 10);
            $cur = $cur->next;
            
            
            if ($l1) {
                $l1 = $l1->next;
            }
            if ($l2) {
                $l2 = $l2->next;
            }
        }
        if ($carry > 0) {
            $cur->next = new ListNode($carry);
        }
        return $dummy->next;
    }
}
```


### go
```go
func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
    dummy := &ListNode{}
    cur := dummy
    sum, carry := 0, 0
    for l1 != nil || l2 != nil {
        n1, n2 := 0, 0
        if l1 != nil {
            n1 = l1.Val
        }
        if l2 != nil {
            n2 = l2.Val
        }
        sum = n1 + n2 + carry
        carry = (n1 + n2 + carry) / 10

        cur.Next = &ListNode{Val: sum % 10}
        cur = cur.Next

        if l1 != nil {
            l1 = l1.Next
        }
        if l2 != nil {
            l2 = l2.Next
        }
    }
    if carry > 0 {
        cur.Next = &ListNode{Val: carry}
    }
    return dummy.Next
}
```

### java
```java
class LeetCode0002 {

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode dummyHead = new ListNode();
        ListNode cur = dummyHead;
        int n1 = 0;
        int n2 = 0;
        int carry = 0;
        int sum = 0;
        while (l1 != null || l2 != null) {
            n1 = l1 != null ? l1.val : 0;
            n2 = l2 != null ? l2.val : 0;
            sum = n1 + n2 + carry;
            carry = sum / 10;
            cur.next = new ListNode(sum % 10);
            cur = cur.next;

            if (l1 != null) {
                l1 = l1.next;
            }
            if (l2 != null) {
                l2 = l2.next;
            }
        }
        if (carry > 0) {
            cur.next = new ListNode(carry);
        }
        return dummyHead.next;
    }
}
```
