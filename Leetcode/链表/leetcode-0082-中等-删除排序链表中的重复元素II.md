# 0082. 删除排序链表中的重复元素 II

# 题目
给定一个已排序的链表的头 head ， 删除原始链表中所有重复数字的节点，只留下不同的数字 。返回 已排序的链表 。

https://leetcode.cn/problems/remove-duplicates-from-sorted-list-ii/description/

提示：
- 链表中节点数目在范围 [0, 300] 内
- -100 <= Node.val <= 100
- 题目数据保证链表已经按升序 排列

# 示例
```
输入：head = [1,2,3,3,4,4,5]
输出：[1,2,5]
```
```
输入：head = [1,1,1,2,3]
输出：[2,3]
```

# 解析
由于给定的链表是排好序的，因此 **重复的元素在链表中出现的位置是连续的**，因此我们只需要对链表进行一次遍历，就可以删除重复的元素。
由于链表的头节点可能会被删除，因此我们需要额外使用一个虚拟节点（dummy node）指向链表的头节点。

从指针 cur 指向链表的虚拟节点，随后开始对链表进行遍历。如果当前 cur.next 与 cur.next.next 对应的元素相同，
那么就需要将 cur.next 以及所有后面拥有相同元素值的链表节点全部删除。我们记下这个元素值 val，随后不断将 cur.next 从链表中移除，
直到 cur.next 为空节点或者其元素值不等于 val 为止。此时，我们将链表中所有元素值为 val 的节点全部删除。

如果当前 cur.next 与 cur.next.next 对应的元素不相同，那么说明链表中只有一个元素值为 cur.next 的节点，那么我们就可以将 cur 指向 cur.next。

当遍历完整个链表之后，返回链表的的虚拟节点的下一个节点 dummy.next 即可。

# 代码

###
```php
class LeetCode0082 {

    function deleteDuplicates($head) {
        if (!$head) {
            return $head;
        }
        // 虚拟头节点
        $dummy = new ListNode(-1, $head);
        
        // 从虚拟头节点位置开始访问
        $cur = $dummy;
        
        // 只要当前节点的下一个节点和下下个节点都存在，就继续访问下去
        while ($cur->next && $cur->next->next) {
            // 遍历过程中会遇到两种情况
            if ($cur->next->val == $cur->next->next->val) {
                // 1. 下一个节点与下下个节点的值相等，那么说明与这个节点值相等的所有节点都应该被删掉
                
                // 先记录下这个值 value
                $value = $cur->next->val;
                
                // 利用 while 循环，不断找出与 value 相等的节点，跳过这个节点
                while ($cur->next && $cur->next->val == $value) {
                    $cur->next = $cur->next->next;
                }
            } else {
                // 2. 不相等的情况继续访问下个节点
                $cur = $cur->next;
            }
        }
        return $dummy->next;
    }

}
```

### java
```java
class LeetCode0082 {
    
    public ListNode deleteDuplicates(ListNode head) {
        ListNode dummy = new ListNode(0, head);
        ListNode cur = dummy;
        while (cur.next != null && cur.next.next != null) {
            if (cur.next.val == cur.next.next.val) {
                int val = cur.next.val;
                while (cur.next != null && cur.next.val == val) {
                    cur.next = cur.next.next;
                }
            } else {
                cur = cur.next;
            }
        }
        return dummy.next;
    }
}
```

### go
```go
func deleteDuplicates(head *ListNode) *ListNode {
    if head == nil {
        return head
    }
    dummy := &ListNode{}
    dummy.Next = head
    cur := dummy
    for cur.Next != nil && cur.Next.Next != nil {
        if cur.Next.Val == cur.Next.Next.Val {
            value := cur.Next.Val
            for cur.Next != nil && cur.Next.Val == value {
                cur.Next = cur.Next.Next
            }
        } else {
            cur = cur.Next
        }
    }
    return dummy.Next
}
```