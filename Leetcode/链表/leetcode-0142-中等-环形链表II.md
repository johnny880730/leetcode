# 0142. 环形链表 II 【中等】
给定一个链表，返回链表开始入环的第一个节点。 如果链表无环，则返回 null。

为了表示给定链表中的环，我们使用整数 pos 来表示链表尾连接到链表中的位置（索引从 0 开始）。
如果 pos 是 -1，则在该链表中没有环。
注意，pos 仅仅是用于标识环的情况，并不会作为参数传递到函数中。

说明：不允许修改给定的链表。

https://leetcode.cn/problems/linked-list-cycle-ii/description/

# 示例：
```
示例 1：

输入：head = [3,2,0,-4], pos = 1
输出：返回索引为 1 的链表节点
解释：链表中有一个环，其尾部连接到第二个节点。
```

```
示例 2：

输入：head = [1,2], pos = 0
输出：返回索引为 0 的链表节点
解释：链表中有一个环，其尾部连接到第一个节点。
```

```
示例 3：

输入：head = [1], pos = -1
输出：返回 null
解释：链表中没有环。
```


提示：
- 链表中节点的数目范围在范围 [0, 10,000] 内
- -100,000 <= Node.val <= 100,000
- pos 的值为 -1 或者链表中的一个有效索引



# 解析
这道题目不仅考察对链表的操作，而且还需要一些数学运算，主要考察两个知识点：
- 判断链表是否有环
- 如果有环，如何找到这个环的入口

## 判断链表是否有环
可以使用快慢指针法，分别定义 fast 和 slow 指针，从头节点出发，fast 指针每次移动两个节点，slow 每次移动一个节点，
如果两个指针在途中相遇，则说明这个链表有环。

为什么这么做（如果有环）一定会在环内相遇而不会永远错开呢？

因为快指针一定先进入环中，如果快慢指针相遇，那么一定是在环中相遇，这是毋庸置疑的。

为什么快慢指针一定会相遇呢？可以画一个环，让快指针在任意一个节点开始追赶慢指针。因为快指针每次移动两个节点，慢指针每次移动一个节点，
相对于慢指针来说，快指针是一个节点一个节点的靠近慢指针的，所以两者一定会重合。

## 寻找环的入口
此时已经可以判断链表是否有环了，接下来要找这个环的入口。

假设从头节点到环的入口节点的节点数为 x，环入口节点到快慢指针相遇节点的节点数为 y，从相遇节点再到环的入口的节点数为 z。如下图所示：

![](./images/leetcode-0142-题解.png)

当快慢指针相遇时，慢指针移动的节点数为 x + y，快指针移动的节点数为 x + y + n(y + z)，n 的含义为快指针在环内移动了 n 圈才遇到慢指针，
y + z 为一圈内节点的个数。

因为快指针是一步移动两个节点，慢指针是一步移动一个节点，所以快指针移动步数 = 慢指针移动步数 * 2，即 (x + y) × 2 = x + y + n(y + z)。
两边消掉一个 (x + y)，得到 x + y = n(y + z)。

因为要找环的入口，所以要计算的是 x。x = n(y + z) - y，再提取一个 (y + z)，得到 x = (n - 1)(y + z) + z。注意这里 n 一定是大于等于1 的，
因为快指针至少要多移动一圈才能遇到慢指针。

这个公式说明了什么呢？先以 n = 1 的情况为例，快指针在环内移动了一圈后遇到了慢指针，这时候公式化解为 x = z。

这就意味着，一个节点从头节点出发，另一个节点从相遇节点出发，这两个指针每次一起只移动一个节点，那么这两个指针相遇的节点就是环的入口节点。

在相遇节点处顶一个指针 index1，在头节点处定一个指针 index2，让他们同时移动，每次移动一个节点，那么相遇的地方就是环的入口节点。

如果 n > 1 呢？也就是快指针在环中移动了 n 圈之后才遇到慢指针。

其实这种情况和 n = 1 的效果是一样的，一样可以通过该方法找到环的入口节点，只不过 index1 指针在环内多移动了 n - 1 圈，然后遇到 index2，
相遇点依旧是环的入口节点。

# 代码

### php
```php
class LeetCode0142 {

    // 快慢指针，有环的话会相遇。相遇后一个指针回到首处，继续一步一步走，直到再次相遇
    function detectCycle($head) {
        if (!$head || !$head->next) {
            return null;
        }
        $slow = $head;
        $fast = $head;
        while (1) {
            if ($fast == null || $fast->next == null) {
                return null;
            }
            $fast = $fast->next->next;
            $slow = $slow->next;
            if ($fast === $slow) {
                break;
            }
        }
        $fast = $head;
        while ($slow !== $fast) {
            $fast = $fast->next;
            $slow = $slow->next;
        }
        return $slow;
    }
}
```

### java
```java
class LeetCode0142 {

    public ListNode detectCycle(ListNode head) {
        if (head == null || head.next == null) {
            return null;
        }
        ListNode fast = head, slow = head;
        while (true) {
            if (fast == null || fast.next == null) {
                return null;
            }
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) {
                break;
            }
        }
        fast = head;
        while (slow != fast) {
            slow = slow.next;
            fast = fast.next;
        }
        return fast;
    }
}
```

### go
```go
func detectCycle(head *ListNode) *ListNode {
    if head == nil || head.Next == nil {
        return nil
    }
    fast, slow := head, head
    for {
        if fast == nil || fast.Next == nil {
            return nil
        }
        fast = fast.Next.Next
        slow = slow.Next
        if fast == slow {
            break
        }
    }
    fast = head
    for fast != slow {
        fast = fast.Next
        slow = slow.Next
    }
    return fast
}
```
