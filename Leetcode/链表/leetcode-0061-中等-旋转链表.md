# 0061. 旋转链表

# 题目
给你一个链表的头节点 head ，旋转链表，将链表每个节点向右移动 k 个位置。

https://leetcode.cn/problems/rotate-list/description/

提示：
- 链表中节点的数目在范围 [0, 500] 内
- -100 <= Node.val <= 100
- 0 <= k <= 2 * 10^9

# 示例：
```
输入：head = [1,2,3,4,5], k = 2
输出：[4,5,1,2,3]
```

```
输入：head = [0,1,2], k = 4
输出：[2,0,1]
```

# 解析
给定链表的长度为 len，注意到当向右移动的次数 k ≥ len 时，仅需要向右移动 k % len 次即可。因为每 len 次移动都会让链表变为原状。
这样可以知道，新链表的最后一个节点为原链表的第 (len - 1) - (k % len) 个节点（从 0 开始计数）。

这样，可以先将给定的链表连接成环，然后将指定位置断开。

具体代码中，我们首先计算出链表的长度 len，并找到该链表的末尾节点，将其与头节点相连。这样就得到了闭合为环的链表。
然后找到新链表的最后一个节点（即原链表的第 (len-1)-(k%n) 个节点），将当前闭合为环的链表断开，即可得到所需要的结果。

特别地，当链表长度不大于 1，或者 k 为 n 的倍数时，新链表将与原链表相同，无需进行任何处理。

# 代码

### php
```php
class LeetCode0061 {

    function rotateRight($head, $k) {
        if ($k == 0 || $head == null || $head->next == null) {
            return $head;
        }
        // 计算链表长度
        $len = 1;
        $node = $head;
        while ($node->next != null) {
            $node = $node->next;
            $len++;
        }
        // 计算实际只要转几次
        $add = $len - $k % $len;
        if ($add == $len) {
            return $head;
        }
        // 末尾的节点的 next 指针指向 head，形成环
        $node->next = $head;
        while ($add-- > 0) {
            $node = $node->next;
        }
        // 此时 node->next 指向的是新链表的头节点，将 node 节点的 next 指针切断。
        $ret = $node->next;
        $node->next = null;
        return $ret;
    }

}
```

### go
```go
func rotateRight(head *ListNode, k int) *ListNode {
    if k == 0 || head == nil || head.Next == nil {
        return head
    }
    // 计算链表长度
    len := 1
    node := head
    for node.Next != nil {
        node = node.Next
        len++
    }
    // 计算实际只要转几次
    add := len - k % len
    if add == len {
        return head
    }
    // 末尾的节点的 next 指针指向 head，形成环
    node.Next = head
    for add > 0 {
        node = node.Next
        add--
    }
    // 此时 node->next 指向的是新链表的头节点，将 node 节点的 next 指针切断。
    ret := node.Next
    node.Next = nil
    return ret
}
```

### java
```java
class LeetCode0061 {

    public ListNode rotateRight(ListNode head, int k) {
        if (k == 0 || head == null || head.next == null) {
            return head;
        }
        // 计算链表长度
        int len = 1;
        ListNode node = head;
        while (node.next != null) {
            node = node.next;
            len++;
        }
        // 计算实际只要转几次
        int add = len - k % len;
        if (add == len) {
            return head;
        }
        // 末尾的节点的 next 指针指向 head，形成环
        node.next = head;
        while (add-- > 0) {
            node = node.next;
        }
        // 此时 node->next 指向的是新链表的头节点，将 node 节点的 next 指针切断。
        ListNode ret = node.next;
        node.next = null;
        return ret;
    }

}
```
