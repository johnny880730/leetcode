### 147. 对链表进行插入排序

# 题目 
给定单个链表的头 head ，使用 插入排序 对链表进行排序，并返回 排序后链表的头 。

插入排序算法的步骤:
1. 插入排序是迭代的，每次只移动一个元素，直到所有元素可以形成一个有序的输出列表。
2. 每次迭代中，插入排序只从输入数据中移除一个待排序的元素，找到它在序列中适当的位置，并将其插入。
3. 重复直到所有输入数据插入完为止。

https://leetcode.cn/problems/insertion-sort-list


# 示例
```
输入: head = [4,2,1,3]
输出: [1,2,3,4]
```
```
输入: head = [-1,5,3,4,0]
输出: [-1,0,3,4,5]
```

提示：
- 列表中的节点数在 [1, 5000]范围内
- -5000 <= Node.val <= 5000

# 解析
&emsp;&emsp;从前往后找插入点

&emsp;&emsp;插入排序的基本思想是，维护一个有序序列，初始时有序序列只有一个元素，每次将一个新的元素插入到有序序列中，将有序序列的长度增加 1，
直到全部元素都加入到有序序列中。

&emsp;&emsp;如果是数组的插入排序，则数组的前面部分是有序序列，每次找到有序序列后面的第一个元素（待插入元素）的插入位置，
将有序序列中的插入位置后面的元素都往后移动一位，然后将待插入元素置于插入位置。

&emsp;&emsp;对于链表而言，插入元素时只要更新相邻节点的指针即可，不需要像数组一样将插入位置后面的元素往后移动，因此插入操作的时间复杂度是 O(1)，
但是找到插入位置需要遍历链表中的节点，时间复杂度是 O(n)，因此链表插入排序的总时间复杂度仍然是 O(n²)，其中 n 是链表的长度。

&emsp;&emsp;对于单向链表而言，只有指向后一个节点的指针，因此需要从链表的头节点开始往后遍历链表中的节点，寻找插入位置。

&emsp;&emsp;对链表进行插入排序的具体过程如下。
1. 首先判断给定的链表是否为空，若为空，则不需要进行排序，直接返回。
2. 创建虚拟头节点 dummyHead，令 dummyHead.next = head。引入虚拟节点是为了便于在 head 节点之前插入节点。
3. 维护 lastSorted 为链表的已排序部分的最后一个节点，初始时 lastSorted = head。
4. 维护 curr 为待插入的元素，初始时 curr = head.next。
5. 比较 lastSorted 和 curr 的节点值。
   - 若 lastSorted.val <= curr.val，说明 curr 应该位于 lastSorted 之后，将 lastSorted 后移一位，curr 变成新的 lastSorted。
   - 否则，从链表的头节点开始往后遍历链表中的节点，寻找插入 curr 的位置。令 prev 为插入 curr 的位置的前一个节点，完成对 curr 的插入：
6. 令 curr = lastSorted.next，此时 curr 为下一个待插入的元素。
7. 重复第 5 步和第 6 步，直到 curr 变成空，排序结束。
8. 返回 dummyHead.next，为排序后的链表的头节点。

# 代码

```php
require_once './class/ListNode.class.php';
$arr1  = [4,2,1,3];
$head = array2LinkList($arr1);
(new LeetCode0147)->main($head);

class LeetCode0147
{
    public function main($head)
    {
        fetchNode($this->insertionSortList($head));
    }

    // 从前往后找插入点
    function insertionSortList($head)
    {
        if (!$head) {
            return $head;
        }
        $dummy = new ListNode(0);
        $dummy->next = $head;
        $lastSorted = $head;
        $cur = $head->next;
        while ($cur) {
            if ($lastSorted->val <= $cur->val) {
                $lastSorted = $lastSorted->next;
            } else {
                $prev = $dummy;
                while ($prev->next->val <= $cur->val) {
                    $prev = $prev->next;
                }
                $lastSorted->next = $cur->next;
                $cur->next = $prev->next;
                $prev->next = $cur;
            }
            $cur = $lastSorted->next;
        }

        return $dummy->next;

    }

}
```