# 143. 重排链表

# 题目
给定一个单链表 L 的头节点 head ，单链表 L 表示为：
```
L0 → L1 → … → Ln - 1 → Ln
```

请将其重新排列后变为：
```
L0 → Ln → L1 → Ln - 1 → L2 → Ln - 2 → …
```

不能只是单纯的改变节点内部的值，而是需要实际的进行节点交换。

https://leetcode.cn/problems/reorder-list/

# 示例：
```
输入：head = [1,2,3,4]
输出：[1,4,2,3]
```

```
输入：head = [1,2,3,4,5]
输出：[1,5,2,4,3]
```



提示：
- 链表的长度范围为 [1, 5 * 104]
- 1 <= node.val <= 1000


# 解析

## 寻找链表中点 + 链表逆序 + 合并链表（重要）
注意到目标链表即为将原链表的左半端和反转后的右半端合并后的结果。

这样任务即可划分为三步：
- 找到原链表的中点（参考 [「876. 链表的中间结点」](./leetcode-0876-简单-链表的中间结点.md)）。
    - 可以使用快慢指针来 O(N) 地找到链表的中间节点
- 将原链表的右半端反转（参考 [「206. 反转链表」](./leetcode-0206-简单-反转链表.md)）。
    - 可以使用迭代法实现链表的反转。
- 将原链表的两端合并。
    - 因为两链表长度相差不超过 1，因此直接合并即可。

# 代码

### php
```php
class LeetCode0143 {

    /**
     * @param ListNode $head
     * @return NULL
     */
    function reorderList($head) {
        if (!$head) {
            return;
        }
        // 找中间点
        $mid = $this->_findMiddle($head);
        $n1 = $head;
        $n2 = $mid->next;
        $mid->next = null;
        // 反转后半
        $n2 = $this->_reverseList($n2);
        // 合并
        $this->_mergeList($n1, $n2);
    }

    private function _findMiddle($node) {
        $slow = $fast = $node;
        while ($fast != null && $fast->next != null) {
            $slow = $slow->next;
            $fast = $fast->next->next;
        }
        return $slow;
    }

    private function _reverseList($head) {
        $prev = null;
        $cur = $head;
        while ($cur) {
            $nxt = $cur->next;
            $cur->next = $prev;
            $prev = $cur;
            $cur = $nxt;
        }
        return $prev;
    }

    private function _mergeList(&$n1, &$n2) {
        $tmp1 = $tmp2 = null;
        while ($n1 && $n2) {
            $tmp1 = $n1->next;
            $tmp2 = $n2->next;

            $n1->next = $n2;
            $n1 = $tmp1;

            $n2->next = $n1;
            $n2 = $tmp2;
        }
    }

}
```

### java
```java
class LeetCode0143 {
    
    public void reorderList(ListNode head) {
        if (head == null) {
            return;
        }
        ListNode mid = _findMiddle(head);
        ListNode n1 = head, n2 = mid.next;
        mid.next = null;
        n2 = _reverseList(n2);
        _mergeList(n1, n2);
    }

    private ListNode _findMiddle(ListNode head) {
        ListNode fast = head, slow = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }
        return slow;
    }

    private ListNode _reverseList(ListNode node) {
        ListNode prev = null;
        ListNode cur = node;
        while (cur != null) {
            ListNode nxt = cur.next;
            cur.next = prev;
            prev = cur;
            cur = nxt;
        }
        return prev;
    }

    private void _mergeList(ListNode n1, ListNode n2) {
        ListNode tmp1 = null, tmp2 = null;
        while (n1 != null && n2 != null) {
            tmp1 = n1.next;
            tmp2 = n2.next;

            n1.next = n2;
            n1 = tmp1;

            n2.next = n1;
            n2 = tmp2;
        }
    }
}
```

### go
```go
func reorderList(head *ListNode)  {
    if head == nil {
        return
    }
    mid := _findMiddle(head)
    n1, n2 := head, mid.Next
    mid.Next = nil
    n2 = _reverseList(n2)
    _mergeList(n1, n2)
}

func _findMiddle(head *ListNode) *ListNode {
    slow, fast := head, head
    for fast != nil && fast.Next != nil {
        slow = slow.Next
        fast = fast.Next.Next
    }
    return slow
}

func _reverseList(node *ListNode) *ListNode {
    var prev *ListNode
    cur := node
    for cur != nil {
        nxt := cur.Next
        cur.Next = prev
        prev = cur
        cur = nxt
    }
    return prev
}

func _mergeList(n1 *ListNode, n2 *ListNode) {
    var tmp1 , tmp2 *ListNode
    for n1 != nil && n2 != nil {
        tmp1 = n1.Next
        tmp2 = n2.Next
        
        n1.Next = n2
        n1 = tmp1
        
        n2.Next = n1
        n2 = tmp2
    }
}
```
