# 203. 移除链表元素

# 题目
给你一个链表的头节点 head 和一个整数 val ，请你删除链表中所有满足 Node.val == val 的节点，并返回 新的头节点 。

![](./images/leetcode-0203-题图.jpg)

https://leetcode.cn/problems/remove-linked-list-elements/

# 示例：
```
输入：head = [1,2,6,3,4,5,6], val = 6
输出：[1,2,3,4,5]
```

```
输入：head = [], val = 1
输出：[]
```

```
输入：head = [7,7,7,7], val = 7
输出：[]
```

提示：
列表中的节点数目在范围 [0, 10^4] 内
- 1 <= Node.val <= 50
- 0 <= val <= 50

# 解析
来看下删除元素的操作。删除元素就是让节点的 next 指针直接指向下一个节点的下一个节点。因为单链表的特殊性，
所以需要找到操作节点的前一个节点。

如果删除的是头节点，又该怎么办呢？这里涉及到如下两种链表操作方式：
- 直接使用原来的链表执行删除操作
- 设置一个虚拟头节点再执行删除操作

第一种操作：直接使用原来的链表删除节点。删除头节点和删除其他节点的操作是不一样的，因为链表的其他节点都是通过前一个节点来删除当前节点的，
而头节点没有前一个节点。

那么如何删除头节点呢？其实只要将头节点向后移动一位就可以了，这样就从链表中删除了一个头节点。

那么有没有一个统一的逻辑来删除链表的节点呢？

其实可以设置一个虚拟头节点，这样原链表的所有节点都可以按照统一的方式删除了。

给链表添加了一个虚拟头节点并将其设为新的头节点，此时删除这个旧头节点的方式酒和删除链表中其他节点的方式一样了。

在题目中，返回头节点的时候，别忘了 dummyNode->next 才是新的头节点。

# 代码

### php
```php
class LeetCode0203 {

    // 虚拟头节点（推荐）
    function removeElements($head, $val) {
        $dummyHead = new ListNode();
        $dummyHead->next = $head;
        $cur = $dummyHead;
        while ($cur->next) {
            if ($cur->next->val == $val) {
                $cur->next = $cur->next->next;
            } else {
                $cur = $cur->next;
            }
        }
        return $dummyHead->next;
    }
    
    // 递归
    function removeElements2($head, $val)
    {
        if (!$head) {
            return null;
        }
        $res = $this->removeElements2($head->next, $val);
        if ($head->val == $val) {
            return $res;
        } else {
            $head->next = $res;
            return $head;
        }
    }
}
```

### go
```go
func removeElements(head *ListNode, val int) *ListNode {
    dummy := &ListNode{Val: -1}
    dummy.Next = head
    cur := dummy
    for cur.Next != nil {
        if cur.Next.Val == val {
            cur.Next = cur.Next.Next
        } else {
            cur = cur.Next 
        }
    }
    return dummy.Next
}
```
