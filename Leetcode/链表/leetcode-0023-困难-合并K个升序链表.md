# 0023. 合并K个升序链表【困难】

# 题目
给你一个链表数组，每个链表都已经按升序排列。

请你将所有链表合并到一个升序链表中，返回合并后的链表。

https://leetcode.cn/problems/merge-k-sorted-lists/description/

提示：
- k == lists.length
- 0 <= k <= 10000
- 0 <= lists[i].length <= 500
- -10000 <= lists[i][j] <= 10000
- lists[i] 按 升序 排列
- lists[i].length 的总和不超过 10000


# 示例
```
示例 1：

输入：lists = [[1,4,5],[1,3,4],[2,6]]
输出：[1,1,2,3,4,4,5,6]
解释：链表数组如下：
[
  1->4->5,
  1->3->4,
  2->6
]
将它们合并到一个有序链表中得到。
1->1->2->3->4->4->5->6
```
```
示例 2：

输入：lists = []
输出：[]
```

```
示例 3：

输入：lists = [[]]
输出：[]
```

提示：
- k == lists.length
- 0 <= k <= 10,000
- 0 <= lists[i].length <= 500
- -10,000 <= lists[i][j] <= 10,000
- lists[i] 按 升序 排列
- lists[i].length 的总和不超过 10,000

# 解析

## 小根堆

利用小根堆解决：
- 首先将所有链表头加入小根堆
- 然后每次遍历取出小根堆中的最上元素（最小）
- 然后将该节点加入结果链表中
- 将取出的这个链表后移，如果不为空则继续加入堆中；如果为空（该链表全为空）则直接跳出循环遍历其他链表
- 将结果链表也后移，为下一个节点的加入做准备
- 最终返回结果链表真正的头结点即可

## 顺序合并

可以想到一种最朴素的方法：用一个变量 ans 来维护以及合并的链表，第 i 次循环把第 i 个链表和 ans 合并，答案保存到 ans 中。

合并两个有序链表的内容就是 [21. 合并两个有序链表](./leetcode-0021-简单-合并两个有序链表.md/)

# 代码

### php
```php

class LeetCode0023 {

    // 小根堆
    function mergeKLists($lists) {
        if (!$lists) {
            return null;
        }
        // 建立小根堆
        $heap = new SplMinHeap();
        
        // 将每个链表的头节点放入小根堆
        foreach ($lists as $h) {
            if ($h) {
                $heap->insert($h);
            }
        }
        
        // 小根堆为空，直接返回空
        if ($heap->isEmpty()) {
            return null;
        }
        
        // 到这里小根堆至少有一个元素，将它取出记为 head
        // 因为 head 肯定是所有链表节点中的最小的元素，所以它也就是最后要返回的节点
        // 如果这个 head 有后继节点的话也放入小根堆里
        $head = $heap->extract();
        $cur = $head;
        if ($cur->next) {
            $heap->insert($cur->next);
        }
        
        // 只要小根堆还存在元素，都从其顶部取出元素（也就是值最小的节点）接到结果链表中
        while ($heap->isEmpty() == false) {
            $node = $heap->extract();
            $cur->next = $node;
            $cur = $node;
            if ($node->next) {
                $heap->insert($node->next);
            }
        }
        return $head;
    }


}
```

### go
```go
// 两两合并
func mergeKLists(lists []*ListNode) *ListNode {
    var res *ListNode
    length := len(lists)
    for i := 0; i < length; i++ {
        res = _merge2List(res, lists[i])
    }
    return res
}

func _merge2List(l1 *ListNode, l2 *ListNode) *ListNode {
    dummy := &ListNode{}
    cur := dummy
    for l1 != nil && l2 != nil {
        if l1.Val < l2.Val {
            cur.Next = l1
            l1 = l1.Next
        } else {
            cur.Next = l2
            l2 = l2.Next
        }
        cur = cur.Next
    }
    if l1 != nil {
        cur.Next = l1
    }
    if l2 != nil {
        cur.Next = l2
    }
    return dummy.Next
}
```

### java
```java
// 小根堆
class LeetCode0023-1 {

    public static class ListNodeComparator implements Comparator<ListNode> {
        @Override
        public int compare(ListNode o1, ListNode o2) {
            return o1.val - o2.val;
        }
    }

    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }
        // 小根堆
        PriorityQueue<ListNode> minHeap = new PriorityQueue<>(new ListNodeComparator());
        for (int i = 0; i < lists.length; i++) {
            if (lists[i] != null) {
                minHeap.add(lists[i]);
            }
        }
        if (minHeap.isEmpty()) {
            return null;
        }

        ListNode head = minHeap.poll();
        ListNode cur = head;
        if (cur.next != null) {
            minHeap.add(cur.next);
        }
        while (!minHeap.isEmpty()) {
            ListNode node = minHeap.poll();
            cur.next = node;
            cur = node;
            if (node.next != null) {
                minHeap.add(node.next);
            }
        }
        return head;
    }
}


// 顺序合并
class LeetCode0023-2 {
    public ListNode mergeKLists(ListNode[] lists) {
        ListNode res = null;
        for (int i = 0; i < lists.length; i++) {
            res = _merge2List(res, lists[i]);
        }
        return res;
    }

    private ListNode _merge2List(ListNode l1, ListNode l2) {
        ListNode dummy = new ListNode();
        ListNode cur = dummy;
        while (l1 != null && l2 != null) {
            if (l1.val <= l2.val) {
                cur.next = l1;
                l1 = l1.next;
            } else {
                cur.next = l2;
                l2 = l2.next;
            }
            cur = cur.next;
        }
        cur.next = l1 == null ? l2 : l1;
        return dummy.next;
    }

}
```
