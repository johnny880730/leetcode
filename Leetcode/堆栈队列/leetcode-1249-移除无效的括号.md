### 1249. 移除无效的括号

# 题目
给你一个由 '('、')' 和小写字母组成的字符串 s。

你需要从字符串中删除最少数目的 '(' 或者 ')'  （可以删除任意位置的括号)，使得剩下的「括号字符串」有效。

请返回任意一个合法字符串。

有效「括号字符串」应当符合以下  任意一条  要求：
- 空字符串或只包含小写字母的字符串
- 可以被写作  AB（A  连接  B）的字符串，其中  A  和  B  都是有效「括号字符串」
- 可以被写作  (A)  的字符串，其中  A  是一个有效的「括号字符串」

https://leetcode.cn/problems/minimum-remove-to-make-valid-parentheses

提示：
- 1 <= s.length <= 105
- s[i] 可能是 '('、')' 或英文小写字母

# 示例
```
```
```
```


# 解析

&emsp;&emsp;首先分析 有效 字符串的含义。当且仅当 满足以下条件时，字符串中的括号是平衡的：
- 字符串中有相同数量的 "(" 和 ")"。
- 从左至右遍历字符串，统计当前 "(" 和 ")" 的数量，永远不会出现 ")" 的数量大于 "(" 的数量，称 count("(") - count(")") 为字符串的余量。

&emsp;&emsp;从左向右扫描字符串，每次遇到 "(" 时，余量递增，遇到 ")" 时，余量递减。

&emsp;&emsp;应该让每个 ")" 都与离它最近且尚未匹配的 "(" 匹配。如何在代码中做到这一点？需要知道未匹配的 "(" 的索引。

&emsp;&emsp;使用 栈，每次遇到 "("，都将它的索引压入栈中。每次遇到 ")"，都从栈中移除一个索引，用该 ")" 与栈顶的 "(" 匹配。
栈的深度等于余量。需要执行以下操作：
- 如果在栈为空时遇到 ")"，则删除该 ")"，防止余量为负。
- 如果扫描到字符串结尾有 "("，则删除它，防止余量不为 0。

&emsp;&emsp;删除无效的 ")" 后，"(" 的数量是确保满足 count("(") == count(")") 的最小值。

&emsp;&emsp;将思路的所有内容转换为两步算法：
1. 确定所有需要删除字符的索引。
2. 根据删除字符的索引创建一个新字符串。

&emsp;&emsp;如上所述，使用 栈 存储所有 "(" 的索引。扫描到字符串末尾时，栈中剩余的所有索引都是没有匹配的 "("。还需要使用一个 集合 跟踪不匹配的 ")"。
然后根据索引删除每个需要删除的字符，返回重新创建的字符串。

&emsp;&emsp;尽管可以在 O(n)O(n) 的时间内完成删除操作，但是必须小心，因为一些意外情况会导致 O(n^2)。如果在面试中出现这些错误非常不好。
一些 O(n) 的操作有时也会被误认为是 O(1)O(1)。
- 在 字符串 的 任意一个位置 添加、删除或更改一个字符的操作都是 O(n) 的，因为 String 是 不可变的。每次修改整个字符串都会重建。
- 从 list，vector，array 的非最后一个位置添加或删除元素也是 O(n) 的，因为在其他索引操作需要创建新的空间或者填充空间。
- 检查一个元素是否在 list 中，常使用 线性查找，即使二分查找也要 O(logn) 的复杂度，这并不是理想的效率。

&emsp;&emsp;一种安全策略是遍历字符串，然后将要保留的字符插入到 list（Python）或 StringBuilder（Java）中。遍历完所有字符后，
只需要一步 O(n) 的操作将其转换为字符串。

&emsp;&emsp;检查某个元素是否在 集合 中需要 O(1)。如果需要删除的所有索引都在集合中，那么可以遍历字符串的每个索引，检查当前索引是否在集合中。
如果不是，则将该索引处的字符添加到 StringBuilder 中。


# 代码

```php
$s = 'lee(t(c)o)de)';
(new LeetCode1046())->main($s);

class LeetCode1249
{
    public function main($s) {
        var_dump($this->minRemoveToMakeValid($s));
    }

    function minRemoveToMakeValid($s) {
        $indexesToRemove = array();
        $stack = new SplStack();
        for ($i = 0, $len = strlen($s); $i < $len; $i++) {
            if ($s[$i] == '(') {
                $stack->push($i);
            } else if ($s[$i] == ')') {
                if ($stack->isEmpty()) {
                    $indexesToRemove[$i] = true;
                } else {
                    $stack->pop();
                }
            }
        }
        while ($stack->isEmpty() == false) {
            $indexesToRemove[$stack->pop()] = true;
        }
        $res = '';
        for ($i = 0; $i < $len; $i++) {
            if (!array_key_exists($i, $indexesToRemove)) {
                $res .= $s[$i];
            }
        }
        return $res;
    }
}
```