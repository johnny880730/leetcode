# 0215. 数组中的第K个最大元素

# 题目
给定整数数组 nums 和整数 k，请返回数组中第 k 个最大的元素。

请注意，你需要找的是数组排序后的第 k 个最大的元素，而不是第 k 个不同的元素。

https://leetcode.cn/problems/kth-largest-element-in-an-array/description/

提示：
- 1 <= k <= nums.length <= 100000 
- -10000 <= nums[i] <= 10000

# 示例
```
输入: [3,2,1,5,6,4] 和 k = 2
输出: 5
```
```
输入: [3,2,3,1,2,4,5,5,6] 和 k = 4
输出: 4
```


# 解析

## 大根堆
建立一个大根堆，做 k − 1 次删除操作后堆顶元素就是要找的答案。

在很多语言中，都有优先队列或者堆的的容器可以直接使用，但是在面试中，面试官更倾向于让面试者自己实现一个堆。所以建议掌握这里大根堆的实现方法，在这道题中尤其要搞懂「建堆」、「调整」和「删除」的过程。


# 代码

### php
```php
class LeetCode0215 {

    function findKthLargest($nums, $k) {
        $num = 0;
        $heap = new SplMaxHeap();
        foreach ($nums as $num) {
            $heap->insert($num);
        }
        for (; $k > 0; $k--) {
            $num = $heap->extract();
        }
        return $num;
    }
}
```

### go
```go
func findKthLargest(nums []int, k int) int {
    TopKSplit(nums, len(nums)-k, 0, len(nums)-1)
    return nums[len(nums)-k]
}

func Parition(nums []int, start, stop int)int{
    if start >= stop{
        return -1
    }
    pivot := nums[start]
    l, r := start, stop
    for l < r{
        for l < r && nums[r] >= pivot{
            r--
        }
        nums[l] = nums[r]
        for l < r && nums[l] < pivot{
            l++
        }
        nums[r] = nums[l]
    }
    // 循环结束，l与r相等
    // 确定基准元素pivot在数组中的位置
    nums[l] = pivot
    return l
}

func TopKSplit(nums []int, k, start, stop int){
    if start < stop{
        index := Parition(nums, start, stop)
        if index == k{
            return
        }else if index < k{
            TopKSplit(nums, k, index+1, stop)
        }else{
            TopKSplit(nums, k, start, index-1)
        }
    }
}
```

### java
```java
class LeetCode0215 {
    
    public int findKthLargest(int[] nums, int k) {
        Queue<Integer> maxHeap = new PriorityQueue<>((a, b) -> b - a);
        for (int n: nums) {
            maxHeap.offer(n);
        }
        int res = 0;
        for (; k > 0; k--) {
            res = maxHeap.poll();
        }
        return res;
    }
}
```