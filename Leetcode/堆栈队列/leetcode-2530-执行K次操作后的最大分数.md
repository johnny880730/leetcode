# 2530. 执行 K 次操作后的最大分数

# 题目
给你一个下标从 0 开始的整数数组 nums 和一个整数 k 。你的 起始分数 为 0 。

在一步 操作 中：
- 选出一个满足 0 <= i < nums.length 的下标 i ，
- 将你的 分数 增加 nums[i] ，并且
- 将 nums[i] 替换为 ceil(nums[i] / 3) 。
- 返回在 恰好 执行 k 次操作后，你可能获得的最大分数。

向上取整函数 ceil(val) 的结果是大于或等于 val 的最小整数。

https://leetcode.cn/problems/maximal-score-after-applying-k-operations

提示：
- 1 <= nums.length, k <= 10^5
  1 <= nums[i] <= 10^9

# 示例
```
输入：nums = [10,10,10,10,10], k = 5
输出：50
解释：对数组中每个元素执行一次操作。最后分数是 10 + 10 + 10 + 10 + 10 = 50 。
```
```
输入：nums = [1,10,3,3,3], k = 3
输出：17
解释：可以执行下述操作：
第 1 步操作：选中 i = 1 ，nums 变为 [1,4,3,3,3] 。分数增加 10 。
第 2 步操作：选中 i = 1 ，nums 变为 [1,2,3,3,3] 。分数增加 4 。
第 3 步操作：选中 i = 2 ，nums 变为 [1,1,1,3,3] 。分数增加 3 。
最后分数是 10 + 4 + 3 = 17 。
```

# 解析

## 贪心 + 大顶堆
在一次操作中，会将 nums[i] 变成 ceil(nums[i] / 3)，并且增加 nums[i] 的得分。由于：
- 数组中其它的元素不会变化；
- 对于两个不同的元素 nums[i] 和 nums[j]，如果 nums[i] <= nums[j]，在对它们都进行一次操作后，nums[i] <= nums[j] 仍然成立；
  这就说明，每一次操作都应当贪心地选出当前最大的那个元素。

因此，可以使用一个大根堆（优先队列）来维护数组中所有的元素。在每一次操作中，取出堆顶的元素 x，将答案增加 x，再将 ceil(x / 3) 放回大根堆中即可。

# 代码

### php
```php
class Leetcode2530 {

    function maxKelements($nums, $k) {
        $heap = new SplMaxHeap();
        foreach ($nums as $num) {
            $heap->insert($num);
        }
        $res = 0;
        for ($i = 0; $i < $k; $i++) {
            $cur = $heap->extract();
            $res += $cur;
            $heap->insert(ceil($cur / 3));
        }
        return $res;
    }
}
```

### java
```java
public long maxKelements(int[] nums, int k) {
    // 大顶堆
    Queue<Integer> bigHeap = new PriorityQueue<>((a, b) -> b - a);
    for (int num: nums) {
        bigHeap.offer(num);
    }
    long res = 0;
    while (k > 0) {
        int cur = bigHeap.poll();
        res += cur;
        bigHeap.offer((cur + 2) / 3);
        k--;
    }
    return res;
}
```
