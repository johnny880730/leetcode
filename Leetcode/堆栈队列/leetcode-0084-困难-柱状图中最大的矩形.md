# 84. 柱状图中最大的矩形

# 题目
给定 n 个非负整数，用来表示柱状图中各个柱子的高度。每个柱子彼此相邻，且宽度为 1 。

求在该柱状图中，能够勾勒出来的矩形的最大面积。

https://leetcode.cn/problems/largest-rectangle-in-histogram/

提示：
- 1 <= heights.length <=10<sup>5</sup>
- 0 <= heights[i] <= 10<sup>4</sup>

# 示例
```
输入：heights = [2,1,5,6,2,3]
输出：10
解释：最大的矩形面积为 10
```

```
heights = [2,4]
输出： 4
```

# 解析
本题和 《42. 接雨水》，是遥相呼应的两道题目，建议都要仔细做一做，原理上有很多相同的地方，但细节上又有差异，更可以加深对单调栈的理解！

## 单调栈
本地单调栈的解法和接雨水的题目是遥相呼应的。

为什么这么说呢，《42. 接雨水》 是找每个柱子左右两边第一个大于该柱子高度的柱子，而本题是找每个柱子左右两边第一个小于该柱子的柱子。

这里就涉及到了单调栈很重要的性质，就是单调栈里的顺序，是从小到大还是从大到小。

在 《42. 接雨水》 中接雨水的单调栈从栈头（元素从栈头弹出）到栈底的顺序应该是从小到大的顺序。
那么因为本题是要找每个柱子左右两边第一个小于该柱子的柱子，所以从栈头（元素从栈头弹出）到栈底的顺序应该是从大到小的顺序！

举个例子，如图：

![](./images/leetcode-0084-img1.png)

只有栈里从大到小的顺序，才能保证栈顶元素找到左右两边第一个小于栈顶元素的柱子。

所以本题单调栈的顺序正好与接雨水反过来。

此时应该可以发现其实就是栈顶和栈顶的下一个元素以及要入栈的三个元素组成了要求最大面积的高度和宽度

理解这一点，对单调栈就掌握的比较到位了。

主要就是分析清楚如下三种情况：
- 情况一：当前遍历的元素 heights[i] 大于栈顶元素 heights[st.top()] 的情况
- 情况二：当前遍历的元素 heights[i] 等于栈顶元素 heights[st.top()] 的情况
- 情况三：当前遍历的元素 heights[i] 小于栈顶元素 heights[st.top()] 的情况

另外，建议给 height 数组首尾各加入一个元素 0。为什么这么做呢？

首先来说末尾为什么要加元素0？

如果数组本身就是升序的，例如[2, 4, 6, 8]，那么入栈之后都是单调递减，一直都没有走情况三计算结果的哪一步，所以最后输出的就是 0 了。 如图：

![](./images/leetcode-0084-img2.png)

那么结尾加一个 0，就会让栈里的所有元素，走到情况三的逻辑。

开头为什么要加元素 0？

如果数组本身是降序的，例如 [8, 6, 4, 2]，在 8 入栈后，6 开始与 8 进行比较，此时我们得到 mid（8），right（6），但是得不到 left。

因为将 8 弹出之后，栈里没有元素了，那么为了避免空栈取值，直接跳过了计算结果的逻辑。

之后又将 6 加入栈（此时 8 已经弹出了），然后就是 4 与栈口元素 8 进行比较，周而复始，那么计算的最后结果 result 就是 0 。 如图所示：

![](./images/leetcode-0084-img2.png)

所以需要在 height 数组前后各加一个元素0。




# 代码
```php
class LeetCode0084 {
    
    function largestRectangleArea($heights) {
        array_unshift($heights, 0);
        $heights[] = 0;
        $res = 0;
        $st = new SplStack();
        $st->push(0);
        for ($i = 1; $i < count($heights); $i++) {
            while (!$st->isEmpty() && $heights[$i] < $heights[$st->top()]) {
                $mid = $st->pop();
                $w = $i - $st->top() - 1;
                $h = $heights[$mid];
                $res = max($res, $w * $h);
            }
            $st->push($i);
        }
        return $res;
    }
}

```

### go
```go
func largestRectangleArea(heights []int) int {
    // 首尾加 0
    heights = append([]int{0}, heights...)
    heights = append(heights, 0)
    
    st := make([]int, 0)
    res := 0
    st = append(st, 0)
    for i := 1; i < len(heights); i++ {
        for len(st) > 0 && heights[i] < heights[st[len(st) - 1]] {
            mid := st[len(st) - 1]
            st = st[:len(st) - 1]
            w := i - st[len(st) - 1] - 1
            h := heights[mid]
            res = max(res, w * h)
        }
        st = append(st, i)
    }
    return res
}

func max(a, b int) int {
    if a > b {
        return a
    }
    return b
}
```