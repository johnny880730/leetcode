# 347. 前 K 个高频元素

# 题目
给你一个整数数组 nums 和一个整数 k ，请你返回其中出现频率前 k 高的元素。你可以按 任意顺序 返回答案。

https://leetcode.cn/problems/top-k-frequent-elements/

# 示例:
```
输入: nums = [1,1,1,2,2,3], k = 2
输出: [1,2]
```

# 示例
```
输入: nums = [1], k = 1
输出: [1]
```

提示：
- 1 <= nums.length <= 10^5
- k 的取值范围是 [1, 数组中不相同的元素的个数]
- 题目数据保证答案唯一，换句话说，数组中前 k 个高频元素的集合是唯一的

进阶：你所设计算法的时间复杂度 必须 优于 O(n log n) ，其中 n 是数组大小。

# 解析
本题主要涉及三部分内容：
1. 统计元素出现的次数
2. 对次数排序
3. 找出前 k 个高频元素

首先统计元素出现的次数，可以使用 map 进行统计。然后对次数进行排序，这里可以使用一种容器适配器即优先级队列。

优先级队列就是一个“披着队列外衣”的堆，因为优先级队列对外提供的接口只有从队头取元素，从队尾添加元素，再无其他取元素的方式，很像一个队列。

优先级队列中的元素自动按照元素的权值排列，它是如何有序排列的呢？

默认情况下，priority_queue 利用 max-heap（大顶堆）完成对元素的排序。


# 代码

### php
```php
class LeetCode0347 {

    // 优先级队列
    public function topKFrequent($nums, $k) {
        $map = [];
        foreach ($nums as $item) {
            $map[$item]++;
        }
        $pq = new SplPriorityQueue();
        foreach ($map as $num => $freq) {
            $pq->insert($num, $freq);
        }
        $res = [];
        for ($i = 0; $i < $k; $i++) {
            $res[] = $pq->extract();
        }
        return $res;
    }
}
```


### go
```go
// 解法一：将频率记录到哈希表并进行排序
func topKFrequent1(nums []int, k int) []int {
    res := make([]int, 0)
    hash := make(map[int]int)
    // 利用哈希表将元素和出现次数放入hash表中
    for _, v := range nums {
        hash[v]++
    }
    // 将哈希表里面的key存入res切片中
    for k, _ := range hash {
        res = append(res, k)
    }
    // 对切片进行排序 规则是按照出现次数由高到低排序
    sort.Slice(res, func(i, j int) bool {
        return hash[res[i]] > hash[res[j]]
    })

    return res[:k]
}

// 解法二：自建小顶堆
func topKFrequent2(nums []int, k int) []int {
    //记录每个元素出现的次数
    map_num := map[int]int{}
    for _, item := range nums{
        map_num[item]++
    }
    h := &IHeap{}
    heap.Init(h)
    //所有元素入堆，堆的长度为k
    for key, value := range map_num{
        heap.Push(h, [2]int{key,value})
        if h.Len() > k{
            heap.Pop(h)
        }
    }
    res := make([]int, k)
    //按顺序返回堆中的元素
    for i := 0; i < k; i++{
        res[k - i - 1] = heap.Pop(h).([2]int)[0]
    }
    return res
}

//构建小顶堆
type IHeap [][2]int

func (h IHeap) Len()int {
    return len(h)
}

func (h IHeap) Less (i, j int) bool {
    return h[i][1] < h[j][1]
}

func (h IHeap) Swap(i,j int) {
    h[i], h[j] = h[j], h[i]
}

func (h *IHeap) Push(x interface{}){
    *h = append(*h, x.([2]int))
}

func (h *IHeap) Pop() interface{}{
    old := *h
    n := len(old)
    x := old[n-1]
    *h = old[0 : n-1]
    return x
}
```

### java
```java
class LeetCode0347 {
    
    public int[] topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int n: nums) {
            if (!map.containsKey(n)) {
                map.put(n, 1);
            } else {
                map.put(n, map.get(n) + 1);
            }
        }
        PriorityQueue<Integer> pq = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return map.get(o1) - map.get(o2);
            }
        });
        for (Integer key: map.keySet()) {
            if (pq.size() < k) {
                pq.add(key);
            } else if (map.get(key) > map.get(pq.peek())) {
                pq.remove();
                pq.add(key);
            }
        }

        int[] res = new int[k];
        for (int i = 0; i < k; i++) {
            res[i] = pq.remove();
        }
        return res;

    }
}
```
