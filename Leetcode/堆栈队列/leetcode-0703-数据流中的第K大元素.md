### 703. 数据流中的第 K 大元素

# 题目
设计一个找到数据流中第 k 大元素的类（class）。注意是排序后的第 k 大元素，不是第 k 个不同的元素。

请实现 KthLargest 类：
- KthLargest(int k, int[] nums) 使用整数 k 和整数流 nums 初始化对象。
- int add(int val) 将 val 插入数据流 nums 后，返回当前数据流中第 k 大的元素。

https://leetcode.cn/problems/kth-largest-element-in-a-stream/

提示：
- 1 <= k <= 10<sup>4</sup>
- 0 <= nums.length <= 10<sup>4</sup>
- -10<sup>4</sup> <= nums[i] <= 10<sup>4</sup>
- -10<sup>4</sup> <= val <= 10<sup>4</sup>
- 最多调用 add 方法 10<sup>4</sup> 次
- 题目数据保证，在查找第 k 大元素时，数组中至少有 k 个元素

# 示例
```
```
```
输入：
["KthLargest", "add", "add", "add", "add", "add"]
[[3, [4, 5, 8, 2]], [3], [5], [10], [9], [4]]
输出：
[null, 4, 5, 5, 8, 8]

解释：
KthLargest kthLargest = new KthLargest(3, [4, 5, 8, 2]);
kthLargest.add(3);   // return 4
kthLargest.add(5);   // return 5
kthLargest.add(10);  // return 5
kthLargest.add(9);   // return 8
kthLargest.add(4);   // return 8
```


# 解析

##### 小根堆
&emsp;&emsp;本题的操作步骤如下：
1. 使用大小为 K 的小根堆，在初始化的时候，保证堆中的元素个数不超过 K 。
2. 在每次 add() 的时候，将新元素 push() 到堆中，如果此时堆中的元素超过了 K，那么需要把堆中的最小元素（堆顶）pop() 出来。
3. 此时堆中的最小元素（堆顶）就是整个数据流中的第 K 大元素。

&emsp;&emsp;问答：
1. 为什么使用小根堆？  
因为我们需要在堆中保留数据流中的前 K 大元素，使用小根堆能保证每次调用堆的 pop() 函数时，从堆中删除的是堆中的最小的元素（堆顶）。
2. 为什么能保证堆顶元素是第 K 大元素？  
因为小根堆中保留的一直是堆中的前 K 大的元素，堆的大小是 K，所以堆顶元素是第 K 大元素。
3. 每次 add() 的时间复杂度是多少？  
每次 add() 时，调用了堆的 push() 和 pop() 方法，两个操作的时间复杂度都是 log(K).


# 代码
```php
class KthLargest
{
    /**
     * 构造存放K个元素的小顶堆，小顶堆的顶就是第K大的元素
     * @param Integer $k
     * @param Integer[] $nums
     */
    function __construct($k, $nums)
    {
        $this->arr  = $nums;
        $this->k    = $k;
        $this->heap = new SplMinHeap();

        //创建一个含有k个元素的最小堆
        foreach ($nums as $key => $val) {
            if ($this->heap->count() < $k) {
                $this->heap->insert($val);
            } else if ($val > $this->heap->top()) {
                $this->heap->extract();
                $this->heap->insert($val);
            }
        }
    }

    /**
     * @param Integer $val
     * @return Integer
     */
    function add($val)
    {
        $this->arr[] = $val;
        //维持k个元素的最小堆
        if ($this->heap->count() < $this->k) {
            $this->heap->insert($val);
        } else if ($val > $this->heap->top()) {
            $this->heap->extract();
            $this->heap->insert($val);
        }
        return $this->heap->top();
    }
}

/**
 * Your KthLargest object will be instantiated and called as such:
 * $obj = KthLargest($k, $nums);
 * $ret_1 = $obj->add($val);
 */
```