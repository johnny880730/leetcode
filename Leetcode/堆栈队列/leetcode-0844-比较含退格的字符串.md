# 844. 比较含退格的字符串

# 题目
给定 S 和 T 两个字符串，当它们分别被输入到空白的文本编辑器后，判断二者是否相等，并返回结果。 # 代表退格字符。

注意：如果对空文本输入退格字符，文本继续为空。

提示：
- 1 <= S.length <= 200
- 1 <= T.length <= 200
- S 和 T 只含有小写字母以及字符 '#'。

https://leetcode.cn/problems/backspace-string-compare/

# 示例
```
输入：S = "ab#c", T = "ad#c"
输出：true
解释：S 和 T 都会变成 “ac”。
```
```
输入：S = "ab##", T = "c#d#"
输出：true
解释：S 和 T 都会变成 “”。
```
```
输入：S = "a##c", T = "#a#c"
输出：true
解释：S 和 T 都会变成 “c”。
```
```
输入：S = "a#c", T = "b"
输出：false
解释：S 会变成 “c”，但 T 仍然是 “b”。
```

# 解析

## 栈
这道题目一看就是要使用栈的节奏，这种匹配（消除）问题也是栈的擅长所在

## 从后向前双指针
当然还可以有使用 O(1) 的空间复杂度来解决该问题。

同时从后向前遍历 S 和 T（i 初始为 S 末尾，j 初始为 T 末尾），记录 # 的数量，模拟消除的操作，如果 # 用完了，就开始比较 S[i] 和 T[j]。



# 代码

### php
```php
class LeetCode0844 {

    // 利用栈
    function backspaceCompare($s, $t) {
        $s = $this->stack($s);
        $t = $this->stack($t);
        return $s === $t;
    }

    function stack($str) {
        $len = strlen($str);
        $stack = new SplStack();
        for ($i = 0; $i < $len; $i++) {
            if ($str[$i] == '#') {
                if (!$stack->isEmpty()) {
                    $stack->pop();
                }
            } else {
                $stack->push($str[$i]);
            }
        }
        $res = '';
        while (!$stack->isEmpty()) {
            $res .= $stack->pop();
        }
        return $res;
    }
    
    // 从后向前双指针
    function backspaceCompare2($s, $t) {
        $sSkipNum = $tSkipNum = 0;  // 记录S和T的#数量
        $i = strlen($s) - 1;
        $j = strlen($t) - 1;
        while (1) {
            // 从后向前，消除S的#
            while ($i >= 0) {
                if ($s[$i] == '#') {
                    $sSkipNum++;
                } else {
                    if ($sSkipNum > 0) {
                        $sSkipNum--;
                    } else {
                        break;
                    }
                }
                $i--;
            }
            
            // 从后向前，消除T的#
            while ($j >= 0) {
                if ($t[$j] == '#') {
                    $tSkipNum++;
                } else {
                    if ($tSkipNum > 0) {
                        $tSkipNum--;
                    } else {
                        break;
                    }
                }
                $j--;
            }
            // S 或者T 遍历到头了
            if ($i < 0 || $j < 0) {
                break;
            }

            if ($s[$i] != $t[$j]) {
                return false;
            }
            $i--;
            $j--;
        }
        // 说明S和T同时遍历完毕
        if ($i == -1 && $j == -1) {
            return true;
        }
        return false;
    }
}
```

### go
```go
// 栈
func backspaceCompare(s string, t string) bool {
	ss, tt := _fromStack(s), _fromStack(t)
	return ss == tt
}

func _fromStack(str string) string {
	stack := make([]byte, 0)
	for i, j := 0, len(str); i < j; i++ {
		if str[i] == '#' {
			if len(stack) > 0 {
				stack = stack[ : len(stack) - 1]
			}
		} else {
			stack = append(stack, str[i])
		}
	}
	arr := make([]string, 0)
	for k := range stack {
		arr = append(arr, string(stack[k]))
	}
	res := strings.Join(arr, "")
	return res
}

// 从后向前双指针
func backspaceCompare2(s string, t string) bool {
    sSkipNum, tSkipNum := 0, 0
    i, j := len(s) - 1, len(t) - 1
    for true {
        for i >= 0 {
            if s[i] == '#' {
                sSkipNum++
            } else {
                if sSkipNum > 0 {
                    sSkipNum--
                } else {
                    break
                }
            }
            i--
        }
        
        for j >= 0 {
            if t[j] == '#' {
                tSkipNum++
            } else {
                if tSkipNum > 0 {
                    tSkipNum--
                } else {
                    break
                }
            }
            j--
        }
    
        if i < 0 || j < 0 {
            break
        }
    
        if s[i] != t[j] {
            return false
        }
        i--
        j--
    }
    if i == -1 && j == -1 {
        return true
    }
    return false
}
```