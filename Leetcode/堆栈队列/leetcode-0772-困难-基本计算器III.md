# 772. 基本计算器 III

# 题目
实现一个基本的计算器来计算简单的表达式字符串。

表达式字符串只包含非负整数，算符 +、-、*、/ ，左括号 ( 和右括号 ) 。整数除法需要 向下截断 。

你可以假定给定的表达式总是有效的。所有的中间结果的范围为 [-2^31, 2^31 - 1] 。

https://leetcode.cn/problems/basic-calculator-iii/

提示：
- 1 <= s <= 10^4
- s 由整数、'+'、'-'、'*'、'/'、'(' 和 ')' 组成
- s 是一个 有效的 表达式

# 示例
```
输入：s = "1+1"
输出：2
```
```
输入：s = "6-4/2"
输出：4
```
```
输入：s = "2*(5+5*2)/3+(6/2+8)"
输出：21
```
```
输入：s = "(2+6*3+5-(3*14/7+2)*5)+3"
输出：-12
```
```
输入：s = "0"
输出：0
```


# 解析


# 代码

```php

class LeetCode0772 {

    function calculate($s) {
        return $this->_value($s, 0)[0]; 
    }
    
    // 从 str[i...]往下算，遇到字符串终止位置或者右括号就停止
    // 返回两个值，长度为2的数组：
    // 0) 负责的这一段 结果是多少
    // 1) 负责的这一段 计算到了哪个位置
    function _value($str, $i) {
        $stack = new SplStack();
        $cur = 0;
        $bra = [];
        while ($i < strlen($str) && $str[$i] != ')') {
            if ($str[$i] == ' ') {
                $i++;
                continue;
            }
            if (ord($str[$i]) >= ord('0') && ord($str[$i]) <= ord('9')) {
                $cur = $cur * 10 + ord($str[$i++]) - ord('0');
            } else if ($str[$i] != '(') {   //遇到的是运算符号
                $this->_addNum($stack, $cur);
                $stack->push($str[$i++]);
                $cur = 0;
            } else {    // 遇到左括号了
                $bra = $this->_value($str, $i + 1);
                $cur = $bra[0];
                $i = $bra[1] + 1;
            }
        }
        $this->_addNum($stack, $cur);
        return array($this->_getNum($stack), $i);
    }
    
    function _addNum($stack, $num) {
        if ($stack->isEmpty == false) {
            $cur = 0; 
            $top = $stack->pop();
            if ($top == '+' || $top == '-') {
                $stack->push($top);
            } else {
                $cur = intval($stack->pop());
                $num = $top == '*' ? ($cur * $num) : intval($cur / $num);
            }
        }
        $stack->push($num);
    }
    
    function _getNum($stack) {
        $res = 0;
        $add = true;
        while ($stack->isEmpty() == false) {
            $cur = $stack->pop();
            if ($cur == '+') {
                $add = true;
            } else if ($cur == '-') {
                $add = false;
            } else {
                $num = intval($cur);
                $res += $add ? $num : (-$num);
            }
        }
        return $res;
    }
}
```