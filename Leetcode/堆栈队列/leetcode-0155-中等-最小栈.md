# 0155. 最小栈

# 题目
设计一个支持 push ，pop ，top 操作，并能在常数时间内检索到最小元素的栈。

实现 MinStack 类:
- MinStack()：初始化堆栈对象。
- void push(int val)：将元素val推入堆栈。
- void pop()：删除堆栈顶部的元素。
- int top()：获取堆栈顶部的元素。
- int getMin()：获取堆栈中的最小元素。

https://leetcode.cn/problems/min-stack/description/

提示：
- -2^31 <= val <= 2^31 - 1
- pop、top 和 getMin 操作总是在 非空栈 上调用
- push, pop, top, getMin 最多被调用 3 * 10^4 次

# 示例
```
输入：
["MinStack","push","push","push","getMin","pop","top","getMin"]
[[],[-2],[0],[-3],[],[],[],[]]

输出：
[null,null,null,null,-3,null,0,-2]

解释：
MinStack minStack = new MinStack();
minStack.push(-2);
minStack.push(0);
minStack.push(-3);
minStack.getMin();   --> 返回 -3.
minStack.pop();
minStack.top();      --> 返回 0.
minStack.getMin();   --> 返回 -2.
```

# 解析

设计一个数据结构，使得每个元素 a 与其相应的最小值 m 时刻保持一一对应。因此可以使用一个辅助栈，与元素栈同步插入与删除，
用于存储与每个元素对应的最小值。
- 当一个元素要入栈时，取当前辅助栈的栈顶存储的最小值，与当前元素比较得出最小值，将这个最小值插入辅助栈中；
- 当一个元素要出栈时，把辅助栈的栈顶元素也一并弹出；
- 在任意一个时刻，栈内元素的最小值就存储在辅助栈的栈顶元素中。

# 代码

### php
```php

class MinStack {
    public $data;
    public $min;

    /**
     */
    function __construct() {
        $this->data = new SplStack();
        $this->min = new SplStack();
    }

    /**
     * @param Integer $val
     * @return NULL
     */
    function push($val) {
        $this->data->push($val);

        if ($this->min->isEmpty() || $this->min->top()>$val) {
            $this->min->push($val);
        } else {
            $this->min->push($this->min->top());
        }
    }

    /**
     * @return NULL
     */
    function pop() {
        $this->min->pop();
        return $this->data->pop();
    }

    /**
     * @return Integer
     */
    function top() {
        return $this->data->top();
    }

    /**
     * @return Integer
     */
    function getMin() {
        return $this->min->top();
    }
}
```

### go
```go
type MinStack struct {
    Data []int
    Min []int
}


func Constructor() MinStack {
    return MinStack{
        Data: []int{},
        Min: []int{},
    }
}   


func (this *MinStack) Push(val int)  {
    this.Data = append(this.Data, val)

    if len(this.Min) == 0 || this.Min[len(this.Min) - 1] > val {
        this.Min = append(this.Min, val)
    } else {
        this.Min = append(this.Min, this.Min[len(this.Min) - 1])
    }
}


func (this *MinStack) Pop()  {
    this.Data = this.Data[:len(this.Data) - 1]
    this.Min = this.Min[:len(this.Min) - 1]
}


func (this *MinStack) Top() int {
    return this.Data[len(this.Data) - 1]
}


func (this *MinStack) GetMin() int {
    return this.Min[len(this.Min) - 1]
}
```

### java
```java
class MinStack {

    private Deque<Integer> data;

    private Deque<Integer> min;

    public MinStack() {
        this.data = new LinkedList<>();
        this.min = new LinkedList<>();
    }
    
    public void push(int val) {
        this.data.push(val);

        if (this.min.isEmpty() || this.min.peek() > val) {
            this.min.push(val);
        } else {
            this.min.push(this.min.peek());
        }
    }
    
    public void pop() {
        this.min.pop();
        
        this.data.pop();

    }
    
    public int top() {
        return this.data.peek();
    }
    
    public int getMin() {
        return this.min.peek();
    }
}
```