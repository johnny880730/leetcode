# 378. 有序矩阵中第 K 小的元素

# 题目
给你一个 n × n 矩阵 matrix ，其中每行和每列元素均按升序排序，找到矩阵中第 k 小的元素。
请注意，它是排序后的第 k 小元素，而不是第 k 个不同的元素。

你必须找到一个内存复杂度优于 O(n²) 的解决方案。

https://leetcode.cn/problems/kth-smallest-element-in-a-sorted-matrix

提示：
- n == matrix.length
- n == matrix[i].length
- 1 <= n <= 300
- -109 <= matrix[i][j] <= 10<sup>9</sup>
- 题目数据 保证 matrix 中的所有行和列都按 非递减顺序 排列
- 1 <= k <= n²


# 示例
```
输入：matrix = [[1,5,9],[10,11,13],[12,13,15]], k = 8
输出：13
解释：矩阵中的元素为 [1,5,9,10,11,12,13,13,15]，第 8 小元素是 13
```
```
输入：matrix = [[-5]], k = 1
输出：-5
```


# 解析

## 小根堆
建立一个小根堆，以矩阵的值作为排序规则，存放的数据是 node，这个 node 记录了值 + 行号 + 列号。

首先将矩阵的最左上角的数字生成 node 塞入小根堆。然后从小根堆弹出这个 node （这个 node 实际也就是 top1 了），
根据行号和列号将它右方和下方的数字生成新的 node 放入小根堆。因为根据题意，当前位置之后最小的数只有从右方和下方两个数产生。
注意需要保证每个位置的数字只进入小根堆一次，用一个 set 数组来记录即可。

每次弹出一个节点，将它的右方和下方的数字生成 node 放入小根堆，同时记录弹出节点的数量。当数量等于 K 时，弹出的节点就是答案了。


# 代码
```php
class LeetCode0378 {

    function kthSmallest($matrix, $k) {
        $N = count($matrix);
        $M = count($matrix[0]);
        $heap = new MyHeap0378();
        $set = array_fill(0, $N, array_fill(0, $M, false));
        $heap->insert(new Node0378($matrix[0][0], 0, 0));
        $set[0][0] = true;
        $count = 0;
        $res = null;
        while ($heap->isEmpty() == false) {
            $res = $heap->extract();
            if (++$count == $k) {
                break;
            }
            $row = $res->row;
            $col = $res->col;
            if ($row + 1 < $N && !$set[$row + 1][$col]) {
                $heap->insert(new Node0378($matrix[$row + 1][$col], $row + 1, $col));
                $set[$row + 1][$col] = true;
            }
            if ($col + 1 < $N && !$set[$row][$col + 1]) {
                $heap->insert(new Node0378($matrix[$row][$col + 1], $row, $col + 1));
                $set[$row][$col + 1] = true;
            }
            
        }
        return $res->value;
    }

}

class Node0378 {
    public $value;
    public $row;
    public $col;
    
    function __construct($v, $r, $c) {
        $this->value = $v;
        $this->row = $r;
        $this->col = $c;
    }
}

class MyHeap0378 extends SplMinHeap {

    public function compare($obj1, $obj2)     {
        return $obj2->value - $obj1->value;
    }
}
```