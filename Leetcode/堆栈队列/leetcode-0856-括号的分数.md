### 856. 括号的分数

# 题目
给定一个平衡括号字符串  S，按下述规则计算该字符串的分数：
- () 得 1 分。
- AB 得  A + B  分，其中 A 和 B 是平衡括号字符串。
- (A) 得  2 * A  分，其中 A 是平衡括号字符串。

https://leetcode.cn/problems/score-of-parentheses

提示：

# 示例
```
输入： "()"
输出： 1
```
```
输入： "(())"
输出： 2
```
```
输入： "()()"
输出： 2
```
```
输入： "(()(()))"
输出： 6
```

# 解析

&emsp;&emsp;字符串 S 中的每一个位置都有一个“深度”，即该位置外侧嵌套的括号数目。例如，字符串 (()(.())) 中的 . 的深度为 2，
因为它外侧嵌套了 2 层括号。

&emsp;&emsp;用一个栈来维护当前所在的深度，以及每一层深度的得分。当遇到一个左括号 ( 时，将深度加一，并且新的深度的得分置为 0。
当遇到一个右括号 ) 时，将当前深度的得分乘二并加到上一层的深度。这里有一种例外情况，如果遇到的是 ()，那么只将得分加一。

&emsp;&emsp;下面给出了字符串 (()(())) 每次对应的栈的情况：
- [0, 0] (
- [0, 0, 0] ((
- [0, 1] (()
- [0, 1, 0] (()(
- [0, 1, 0, 0] (()((
- [0, 1, 1] (()(()
- [0, 3] (()(())
- [6] (()(()))


# 代码

```php
$s = '(()(()))';
(new LeetCode1046())->main($s);

class LeetCode0856
{
    public function main($s) {
        var_dump($this->scoreOfParentheses($s));
    }

    function scoreOfParentheses($s) {
        $stack = new SplStack();
        $stack->push(0);;
        for($i = 0, $len = strlen($s); $i < $len; $i++) {
            if ($s[$i] == '(') {
                $stack->push(0);
            } else {
                $v = $stack->pop();
                $w = $stack->pop();
                $stack->push($w + max(2 * $v, 1));
            }
        }
        return $stack->pop();
    }
}
```