### 402. 移掉 K 位数字

# 题目
给你一个以字符串表示的非负整数 num 和一个整数 k ，移除这个数中的 k 位数字，使得剩下的数字最小。

请你以字符串形式返回这个最小的数字。

https://leetcode.cn/problems/remove-k-digits/solution/yi-diao-kwei-shu-zi-by-leetcode-solution/

提示：
- 1 <= k <= num.length <= 10^5
- num 仅由若干位数字（0 - 9）组成
- 除了 0 本身之外，num 不含任何前导零

# 示例
```
输入：num = "1432219", k = 3
输出："1219"
解释：移除掉三个数字 4, 3, 和 2 形成一个新的最小的数字 1219 。
```
```
输入：num = "10200", k = 1
输出："200"
解释：移掉首位的 1 剩下的数字为 200. 注意输出不能有任何前导零。
```


# 解析

##### 贪心 + 单调栈

&emsp;&emsp;对于两个相同长度的数字序列，最左边不同的数字决定了这两个数字的大小，例如，对于 A=1axxx，B=1bxxx，如果 a>b 则 A>B。
基于此，可以知道，若要使得剩下的数字最小，需要保证靠前的数字尽可能小。

&emsp;&emsp;举一个简单的例子，例如 425，k=1，那么从左到右，有 4、2 和 5 三个选择。将每一个数字和它的左邻居进行比较。
从 2 开始，2 小于它的左邻居 4。假设保留数字 4，那么所有可能的组合都是以数字 4（即 42，45）开头的。相反，如果移掉 4，留下 2，
得到的是以 2 开头的组合（即 25），这明显小于任何留下数字 4 的组合。因此我们应该移掉数字 4。如果不移掉数字 4，则之后无论移掉什么数字，都不会得到最小数。

&emsp;&emsp;基于上述分析，可以得出「删除一个数字」的贪心策略：

&emsp;&emsp;给定一个长度为 n 的数字序列 D<sub>0</sub>D<sub>1</sub>D<sub>2</sub>D<sub>3</sub>...D<sub>n-1</sub>，
从左到右找到第一个位置 i (i>0)，使得 D<sub>i</sub><D<sub>i-1</sub>，并删去 D<sub>i-1</sub>。
如果不存在，说明整个数字序列单调不降，删去最后一个数字即可。

&emsp;&emsp;基于此，可以每次对整个数字序列执行一次这个策略；删去一个字符后，剩下的 n−1 长度的数字序列就形成了新的子问题，可以继续使用同样的策略，
直至删除 k 次。

&emsp;&emsp;然而暴力的实现复杂度最差会达到 O(nk)（考虑整个数字序列是单调不降的），因此需要加速这个过程。

&emsp;&emsp;考虑从左往右增量的构造最后的答案。可以用一个栈维护当前的答案序列，栈中的元素代表截止到当前位置，删除不超过 k 次个数字后，所能得到的最小整数。
根据之前的讨论：在使用 k 个删除次数之前，栈中的序列从栈底到栈顶单调不降。

&emsp;&emsp;因此，对于每个数字，如果该数字小于栈顶元素，就不断地弹出栈顶元素，直到
- 栈为空 
- 或者新的栈顶元素不大于当前数字
- 或者已经删除了 k 位数字

&emsp;&emsp;上述步骤结束后还需要针对一些情况做额外的处理：
- 如果删除了 m 个数字且 m<k，这种情况下需要从序列尾部删除额外的 k−m 个数字。
- 如果最终的数字序列存在前导零，要删去前导零。
- 如果最终数字序列为空，应该返回 0。

&emsp;&emsp;最终，从栈底到栈顶的答案序列即为最小数。

&emsp;&emsp;考虑到栈的特点是后进先出，如果通过栈实现，则需要将栈内元素依次弹出然后进行翻转才能得到最小数。为了避免翻转操作，可以使用双端队列代替栈的实现。

# 代码

```php
$arg = "1432219"; $k = 3;
(new LeetCode1046())->main($arg, $k);

class LeetCode0402
{
    public function main($arg, $k) {
        var_dump($this->removeKdigits($arg, $k));
    }

    function removeKdigits($num, $k) {
        require_once './class/DoubleEndedQueue.class.php';
        $deque = new DoubleEndedQueue();
        $len = strlen($num);
        for ($i = 0; $i < $len; $i++) {
            $digit = $num[$i];
            while ($deque->isEmpty() == false && $k > 0 && $deque->getLast() > $digit) {
                $deque->removeLast();
                $k--;
            }
            $deque->addLast($digit);
        }
        for ($i = 0; $i < $k; $i++) {
            $deque->removeLast();
        }
        $res = '';
        $leadingZero = true;
        while ($deque->isEmpty() == false) {
            $digit = $deque->removeFirst();
            if ($leadingZero && $digit == '0') {
                continue;
            }
            $leadingZero = false;
            $res .= $digit;
        }
        return strlen($res) == 0 ? '0' : $res;
    }
}
```