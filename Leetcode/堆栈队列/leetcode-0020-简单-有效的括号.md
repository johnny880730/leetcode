# 20. 有效的括号

# 题目
给定一个只包括 '('，')'，'{'，'}'，'['，']'的字符串，判断字符串是否有效。

有效字符串需满足：
- 左括号必须用相同类型的右括号闭合。
- 左括号必须以正确的顺序闭合。
- 注意空字符串可被认为是有效字符串。

https://leetcode.cn/problems/valid-parentheses/description/

提示：
- 1 <= s.length <= 10<sup>4</sup>
- s 仅由括号 '()[]{}' 组成

# 示例
```
输入: "()"
输出: true
```

```
输入: "()[]{}"
输出: true
```

```
输入: "(]"
输出: false
```

```
输入: "([)]"
输出: false
```

```
输入: "{[]}"
输出: true
```

# 解析

## 栈
由于栈结构的特殊性，非常适合处理对称匹配类的问题。可以在写代码之前下先分析字符串中的括号有几种不匹配的情况。

这里有三种不匹配的情况：
1. 情况一，左方向的括号多余了，所以不匹配，如 “([{}]()”
2. 情况二，括号没有多余，但是括号的类型不匹配，如 “[{(}}]”
3. 情况三，右方向的括号多余了，所以不匹配，如 “[{}])”

代码只要覆盖了这三种情况，就不会出问题。
1. 已经遍历了字符串，但是栈不为空，说明有相应的左括号，但没有右括号，所以返回 false
2. 在遍历的过程中，发现栈中没有要匹配的字符，返回 false。
3. 在遍历的过程中栈已经为空，没有匹配的字符了，说明右括号没有找到对应的左括号，返回 false。

那么什么时候说明左右括号全都匹配了呢？如果遍历字符串之后，栈是空的，说明全部匹配了

还有个技巧，在匹配左括号的时候，右括号先入栈，这时只需要比较当前元素和栈顶是否相等即可，比左括号先入栈的代码要简单得多。

# 代码

### php
```php
class LeetCode0020 {

    // 栈
    function isValid($s)     {
        $stack = new SplStack();
        for ($i = 0; $i < strlen($s); $i++) {
            $cha = $s[$i];
            if ($cha == '(' || $cha == '[' || $cha == '{') {
                $stack->push($cha);
            } else {
                if ($stack->isEmpty()) {
                    return false;
                }
                $top = $stack->pop();
                if (($cha == ')' && $top != '(') || ($cha == ']' && $top != '[') || ($cha == '}' && $top != '{')) {
                    return false;
                }
            }
        }
        return $stack->isEmpty();
    }
}
```

### go
```go
func isValid(s string) bool {
    stack := []byte{}
    for i, length := 0, len(s); i < length; i++ {
        if s[i] == '(' || s[i] == '[' || s[i] == '{' {
            stack = append(stack, s[i])
        } else {
            if len(stack) <= 0 {
                return false
            }
            top := stack[len(stack) - 1]
            stack = stack[0 : len(stack) - 1]
            if (s[i] == ']' && top != '[') || (s[i] == ')' && top != '(') || (s[i] == '}' && top != '{') {
                return false
            }
        }
    }
    return !(len(stack) > 0)
}
```

### java
```java
class LeetCode0020 {

    public boolean isValid(String s) {
        String[] ss = s.split("");
        Stack<String> st = new Stack<>();
        for (String c : ss) {
           if (c.equals("(") || c.equals("[") || c.equals("{")) {
               st.push(c);
           } else {
               if (st.isEmpty()) {
                   return false;
               }
               String top = st.pop();
               if ((c.equals("}") && !top.equals("{")) || (c.equals("]") && !top.equals("[")) || (c.equals(")") && !top.equals("("))) {
                   return false;
               }
           }
        }

        return st.isEmpty();
    }
}
```
