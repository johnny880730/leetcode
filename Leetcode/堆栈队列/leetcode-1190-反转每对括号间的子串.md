### 1190. 反转每对括号间的子串

# 题目
给出一个字符串 s（仅含有小写英文字母和括号）。

请你按照从括号内到外的顺序，逐层反转每对匹配括号中的字符串，并返回最终的结果。

注意，您的结果中 不应 包含任何括号。

https://leetcode.cn/problems/reverse-substrings-between-each-pair-of-parentheses


提示：
- 0 <= s.length <= 2000
- s 中只有小写英文字母和括号
- 题目测试用例确保所有括号都是成对出现的

# 示例
```
输入：s = "(abcd)"
输出："dcba"
```
```
输入：s = "(u(love)i)"
输出："iloveu"
解释：先反转子字符串 "love" ，然后反转整个字符串。
```
```
输入：s = "(ed(et(oc))el)"
输出："leetcode"
解释：先反转子字符串 "oc" ，接着反转 "etco" ，然后反转整个字符串。
```

# 解析

&emsp;&emsp;对于括号序列相关的题目，通用的解法是使用递归或栈。这里用的栈。

&emsp;&emsp;从左到右遍历该字符串，使用字符串 res 记录当前层所遍历到的小写英文字母。对于当前遍历的字符：
- 如果是左括号，将 res 插入到栈中，并将 res 置为空，进入下一层；
- 如果是右括号，则说明遍历完了当前层，需要将 res 反转，返回给上一层。具体地，将栈顶字符串弹出，然后将反转后的 res 拼接到栈顶字符串末尾，
  将结果赋值给 res。
- 如果是小写英文字母，将其加到 res 末尾。

- 注意到仅在遇到右括号时才进行字符串处理，这样可以保证是按照从括号内到外的顺序处理字符串。

# 代码

```php
$s = 'a(bcdefghijkl(mno)p)q';
(new LeetCode1046())->main($s);

class LeetCode1190
{
    public function main($s) {
        echo $this->reverseParentheses($s);
    }

    function reverseParentheses($s) {
        $res = '';
        $stack = new SplStack();
        for ($i = 0, $len = strlen($s); $i < $len; $i++) {
            $ch = $s[$i];
            if ($ch == '(') {
                $stack->push($res);
                $res = '';
            } else if ($ch == ')') {
                $res = strrev($res);
                $res = $stack->pop() . $res;
            } else {
                $res .= $ch;
            }
        }
        return $res;
    }
}
```