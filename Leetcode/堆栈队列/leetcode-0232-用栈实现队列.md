# 232. 用栈实现队列

# 题目
请你仅使用两个栈实现先入先出队列。队列应当支持一般队列支持的所有操作（push、pop、peek、empty）：

实现 MyQueue 类：
- void push(int x) 将元素 x 推到队列的末尾
- int pop() 从队列的开头移除并返回元素
- int peek() 返回队列开头的元素
- boolean empty() 如果队列为空，返回 true ；否则，返回 false


说明：
- 你只能使用标准的栈操作 —— 也就是只有 push to top, peek/pop from top, size, 和 is empty 操作是合法的。
- 你所使用的语言也许不支持栈。你可以使用 list 或者 deque（双端队列）来模拟一个栈，只要是标准的栈操作即可。


进阶：

你能否实现每个操作均摊时间复杂度为 O(1) 的队列？换句话说，执行 n 个操作的总时间复杂度为 O(n) ，即使其中一个操作可能花费较长时间。

https://leetcode.cn/problems/implement-queue-using-stacks/

# 示例
```
输入：
["MyQueue", "push", "push", "peek", "pop", "empty"]
[[], [1], [2], [], [], []]
输出：
[null, null, null, 1, 1, false]

解释：
MyQueue myQueue = new MyQueue();
myQueue.push(1); // queue is: [1]
myQueue.push(2); // queue is: [1, 2] (leftmost is front of the queue)
myQueue.peek(); // return 1
myQueue.pop(); // return 1, queue is [2]
myQueue.empty(); // return false
```

提示：
- 1 <= x <= 9
- 最多调用 100 次 push、pop、peek 和 empty
- 假设所有操作都是有效的 （例如，一个空的队列不会调用 pop 或者 peek 操作）


# 解析
这是一道模拟题，不涉及具体算法，考察的就是对栈和队列的掌握程度

使用栈模拟队列的行为，仅仅用一个栈是不行的，所以需要两个栈，一个输入栈，一个输出栈，这里要注意两者的关系。

在 push 数据的时候，只要将数据放进输入栈即可。

但在 pop 数据的时候，操作就复杂一些，如果输出栈为空，则把进栈数据全部导入输出栈。如果输出栈不为空，则直接从输出栈中弹出数据即可。

如何判断队列为空呢？如果输入栈和输出栈都为空，则说明模拟的队列为空了

pop() 和 peek() 两个函数的功能类似，在代码实现上也是类似的，可以思考如何复用代码。

# 代码

### php
```php
class LeetCode0232 {

    private $stack;
    private $help;

    /**
     * Initialize your data structure here.
     */
    function __construct() {
        $this->stack = new SplStack();
        $this->help  = new SplStack();
    }

    /**
     * Push element x to the back of queue.
     * @param Integer $x
     * @return NULL
     */
    function push($x) {
        $this->stack->push($x);
    }

    /**
     * Removes the element from in front of queue and returns that element.
     * @return Integer
     */
    function pop() {
        if ($this->help->isEmpty()) {
            while ($this->stack->isEmpty() == false) {
                $this->help->push($this->stack->pop());
            }
        }
        return $this->help->pop();
    }

    /**
     * Get the front element.
     * @return Integer
     */
    function peek() {
        $res = $this->pop();    // 直接使用已有的 pop
        $this->help->push($res);
        return $res;
    }

    /**
     * Returns whether the queue is empty.
     * @return Boolean
     */
    function empty() {
        return $this->stack->isEmpty() && $this->help->isEmpty();
    }
}
```

### go
```go
type MyQueue struct {
    stackIn  []int //输入栈
    stackOut []int //输出栈
}

func Constructor() MyQueue {
    return MyQueue{
        stackIn:  make([]int, 0),
        stackOut: make([]int, 0),
    }
}

// 往输入栈做push
func (this *MyQueue) Push(x int) {
    this.stackIn = append(this.stackIn, x)
}

// 在输出栈做pop，pop时如果输出栈数据为空，需要将输入栈全部数据导入，如果非空，则可直接使用
func (this *MyQueue) Pop() int {
    inLen, outLen := len(this.stackIn), len(this.stackOut)
    if outLen == 0 {
        if inLen == 0 {
            return -1
        }
        for i := inLen - 1; i >= 0; i-- {
            this.stackOut = append(this.stackOut, this.stackIn[i])
        }
        this.stackIn = []int{}      //导出后清空
        outLen = len(this.stackOut) //更新长度值
    }
    val := this.stackOut[outLen-1]
    this.stackOut = this.stackOut[:outLen-1]
    return val
}

func (this *MyQueue) Peek() int {
    val := this.Pop()
    if val == -1 {
        return -1
    }
    this.stackOut = append(this.stackOut, val)
    return val
}

func (this *MyQueue) Empty() bool {
    return len(this.stackIn) == 0 && len(this.stackOut) == 0
}
```