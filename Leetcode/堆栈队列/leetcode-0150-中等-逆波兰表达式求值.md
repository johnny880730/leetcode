# 0150. 逆波兰表达式求值

# 题目
根据 逆波兰表示法，求表达式的值。

有效的算符包括 +、-、*、/ 。每个运算对象可以是整数，也可以是另一个逆波兰表达式。

说明：
- 整数除法只保留整数部分。
- 给定逆波兰表达式总是有效的。换句话说，表达式总会得出有效数值且不存在除数为 0 的情况。

https://leetcode.cn/problems/evaluate-reverse-polish-notation/description/


提示：
- 1 <= tokens.length <= 10000
- tokens[i] 要么是一个算符（"+"、"-"、"*" 或 "/"），要么是一个在范围 [-200, 200] 内的整数

# 示例：
```
输入：tokens = ["2","1","+","3","*"]
输出：9
解释：该算式转化为常见的中缀算术表达式为：((2 + 1) * 3) = 9
```

```
输入：tokens = ["4","13","5","/","+"]
输出：6
解释：该算式转化为常见的中缀算术表达式为：(4 + (13 / 5)) = 6
```

```
输入：tokens = ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]
输出：22
解释：
该算式转化为常见的中缀算术表达式为：
  ((10 * (6 / ((9 + 3) * -11))) + 17) + 5
= ((10 * (6 / (12 * -11))) + 17) + 5
= ((10 * (6 / -132)) + 17) + 5
= ((10 * 0) + 17) + 5
= (0 + 17) + 5
= 17 + 5
= 22
```




逆波兰表达式：

逆波兰表达式是一种后缀表达式，所谓后缀就是指算符写在后面。
- 平常使用的算式则是一种中缀表达式，如 ( 1 + 2 ) * ( 3 + 4 ) 。
- 该算式的逆波兰表达式写法为 ( ( 1 2 + ) ( 3 4 + ) * ) 。

逆波兰表达式主要有以下两个优点：
- 去掉括号后表达式无歧义，上式即便写成 1 2 + 3 4 + * 也可以依据次序计算出正确结果。
- 适合用栈操作运算：遇到数字则入栈；遇到算符则取出栈顶两个数字进行计算，并将结果压入栈中。


# 解析

## 栈
逆波兰表达式严格遵循「从左到右」的运算。计算逆波兰表达式的值时，使用一个栈存储操作数，从左到右遍历逆波兰表达式，进行如下操作：
- 如果遇到数字，则将数字入栈；
- 如果遇到运算符，则将两个操作数出栈，其中先出栈的是右操作数，后出栈的是左操作数，使用运算符对两个操作数进行运算，将运算得到的新操作数入栈。
  
整个逆波兰表达式遍历完毕之后，栈内只有一个元素，该元素即为逆波兰表达式的值。

以['4', '13', '5', '/', '+'] 为例，当遍历到 '/' 时，就可以处理 '13' '5' '/'，得到结果 2。继续遍历，遇到 '+' 时，
就可以 '4', '2' '+'，得到结果 6。


# 代码

### php
```php

class LeetCode0150
{
    public function evalRPN($tokens) {
        $stack = new SplStack();
        foreach ($tokens as $item) {
            if (!in_array($item, ['+', '-', '*', '/'])) {
                $stack->push($item);
                continue;
            }
            $num1 = $stack->pop();
            $num2 = $stack->pop();
            if ($item == '+') $stack->push($num2 + $num1);
            if ($item == '-') $stack->push($num2 - $num1);
            if ($item == '*') $stack->push($num2 * $num1);
            if ($item == '/') $stack->push(intval($num2 / $num1));
        }
        return $stack->pop();
    }
}
```

### go
```go
func evalRPN(tokens []string) int {
    stack := []int{}
    for _, token := range tokens {
        val, err := strconv.Atoi(token)
        if err == nil {
            stack = append(stack, val)
        } else {
            num1, num2 := stack[len(stack) - 2], stack[len(stack) - 1]
            stack = stack[:len(stack) - 2]
            switch token {
            case "+":
                stack = append(stack, num1 + num2)
            case "-":
                stack = append(stack, num1 - num2)
            case "*":
                stack = append(stack, num1 * num2)
            case "/":
                stack = append(stack, num1 / num2)
            }
        }
    }
    return stack[0]
}
```

### java
```java
class LeetCode0150 {

    public int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        for (String item : tokens) {
            if (!(item.equals("+") || item.equals("-") || item.equals("*") || item.equals("/"))) {
                stack.push(Integer.parseInt(item));
                continue;
            }
            int num2 = stack.pop();
            int num1 = stack.pop();

            if (item.equals("+")) {
                stack.push(num1 + num2);
            } else if (item.equals("-")) {
                stack.push(num1 - num2);
            } else if (item.equals("*")) {
                stack.push(num1 * num2);
            } else {
                stack.push(num1 / num2);
            }
        }

        return stack.pop();
    }
}