# 0224. 基本计算器

# 题目
给你一个字符串表达式 s ，请你实现一个基本计算器来计算并返回它的值。

注意:不允许使用任何将字符串作为数学表达式计算的内置函数，比如 eval() 。

https://leetcode.cn/problems/basic-calculator/description/

提示：
- 1 <= s.length <= 3 * 100000
- s 由数字、'+'、'-'、'('、')'、和 ' ' 组成
- s 表示一个有效的表达式
- '+' 不能用作一元运算(例如， "+1"  和 "+(2 + 3)"  无效)
- '-' 可以用作一元运算(即 "-1"  和 "-(2 + 3)"  是有效的)
- 输入中不存在两个连续的操作符
- 每个数字和运行的计算将适合于一个有符号的 32位 整数

# 示例
```
输入：s = "1 + 1"
输出：2
```
```
输入：s = "(1+(4+5+2)-3)+(6+8)"
输出：23
```


# 解析

## 栈
由于字符串除了数字与括号外，只有加号和减号两种运算符。因此，如果展开表达式中所有的括号，则得到的新表达式中，数字本身不会发生变化，
只是每个数字前面的符号会发生变化。

因此，考虑使用一个取值为 {−1, +1} 的整数 sign 代表「当前」的符号。根据括号表达式的性质，它的取值：
- 与字符串中当前位置的运算符有关；
- 如果当前位置处于一系列括号之内，则也与这些括号前面的运算符有关：每当遇到一个以 - 号开头的括号，则意味着此后的符号都要被「翻转」。

考虑到第二点，需要维护一个栈 stack，其中栈顶元素记录了当前位置所处的每个括号所「共同形成」的符号。例如，对于字符串 1 + 2 + (3 - (4 + 5))：
- 扫描到 1 + 2 时，由于当前位置没有被任何括号所包含，则栈顶元素为初始值 +1；
- 扫描到 1 + 2 + (3 时，当前位置被一个括号所包含，该括号前面的符号为 + 号，因此栈顶元素依然 +1；
- 扫描到 1 + 2 + (3 - (4 时，当前位置被两个括号所包含，分别对应着 + 号和 − 号，由于 + 号和 − 号合并的结果为 − 号，因此栈顶元素变为 -1。
- 在得到栈 stack 之后， sign 的取值就能够确定了：
  - 如果当前遇到了 + 号，则更新 sign <- stack.top()；
  - 如果遇到了遇到了 - 号，则更新 sign <- −stack.top()。

然后，每当遇到 ( 时，都要将当前的 sign 取值压入栈中；每当遇到 ) 时，都从栈中弹出一个元素。这样，能够在扫描字符串的时候，即时地更新 stack 中的元素。



# 代码

### php
```php
class LeetCode0224 {

    function calculate($s) {
        $stack = new SplStack();
        $stack->push(1);
        $sign = 1;

        $res = 0;
        $n = strlen($s);
        $i = 0;
        while ($i < $n) {
            if ($s[$i] == ' ') {
                $i++;
            } else if ($s[$i] == '+') {
                $sign = $stack->top();
                $i++;
            } else if ($s[$i] == '-') {
                $sign = -$stack->top();
                $i++;
            } else if ($s[$i] == '(') {
                $stack->push($sign);
                $i++;
            } else if ($s[$i] == ')') {
                $stack->pop();
                $i++;
            } else {
                $num = 0;
                while ($i < $n && ctype_digit($s[$i])) {
                    $num = $num * 10 + ord($s[$i]) - ord('0');
                    $i++;
                }
                $res += $sign * $num;
            }
        }
        return $res;
    }
}
```

### go
```go
func calculate(s string) int {
    stack := []int{}
	stack = append(stack, 1)
	sign := 1

	res := 0
	n := len(s)
	i := 0
	for i < n {
		if s[i] == ' ' {
			i++
		} else if s[i] == '+' {
			sign = stack[len(stack)-1]
			i++
		} else if s[i] == '-' {
			sign = -stack[len(stack)-1]
			i++
		} else if s[i] == '(' {
			stack = append(stack, sign)
			i++
		} else if s[i] == ')' {
			stack = stack[:len(stack)-1]
			i++
		} else {
			num := 0
			for i < n && strings.ContainsRune("0123456789", rune(s[i])) {
				digit, _ := strconv.Atoi(string(s[i]))
				num = num*10 + digit
				i++
			}
			res += sign * num
		}
	}
	return res
}
```

### java
```java
class LeetCode0224 {
    
    public int calculate(String s) {
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        int sign = 1;

        int res = 0;
        int n = s.length();
        int i = 0;
        while (i < n) {
            if (s.charAt(i) == ' ') {
                i++;
            } else if (s.charAt(i) == '+') {
                sign = stack.peek();
                i++;
            } else if (s.charAt(i) == '-') {
                sign = -stack.peek();
                i++;
            } else if (s.charAt(i) == '(') {
                stack.push(sign);
                i++;
            } else if (s.charAt(i) == ')') {
                stack.pop();
                i++;
            } else {
                int num = 0;
                while (i < n && Character.isDigit(s.charAt(i))) {
                    num = num * 10 + (s.charAt(i) - '0');
                    i++;
                }
                res += sign * num;
            }
        }
        return res;
    }
}
```