# 225. 用队列实现栈

# 题目
请你仅使用两个队列实现一个后入先出（LIFO）的栈，并支持普通栈的全部四种操作（push、top、pop 和 empty）。

实现 MyStack 类：
- void push(int x) 将元素 x 压入栈顶。
- int pop() 移除并返回栈顶元素。
- int top() 返回栈顶元素。
- boolean empty() 如果栈是空的，返回 true ；否则，返回 false 。


注意：
- 你只能使用队列的基本操作 —— 也就是 push to back、peek/pop from front、size 和 is empty 这些操作。
- 你所使用的语言也许不支持队列。 你可以使用 list （列表）或者 deque（双端队列）来模拟一个队列 , 只要是标准的队列操作即可。
  
https://leetcode.cn/problems/implement-stack-using-queues/

# 示例
```
输入：
["MyStack", "push", "push", "top", "pop", "empty"]
[[], [1], [2], [], [], []]
输出：
[null, null, null, 2, 2, false]

解释：
MyStack myStack = new MyStack();
myStack.push(1);
myStack.push(2);
myStack.top(); // 返回 2
myStack.pop(); // 返回 2
myStack.empty(); // 返回 False
```


提示：
- 1 <= x <= 9
- 最多调用100 次 push、pop、top 和 empty
- 每次调用 pop 和 top 都保证栈不为空


# 解析

### 双队列模拟栈
队列的规则而是先进先出，把一个队列中的数据导入另一个队列，数据的顺序并没有变。所以用栈实现队列和用队列实现栈的思路是不一样的，
取决于这两个数据结构的性质。如果用两个队列实现栈，那么两个队列就没有输入队列和输出队列的关系，另一个队列完全是用来备份的。

用两个队列 q1 和 q2 实现栈的功能，模拟入栈用 q1。

模拟出栈的时候 q2 的作用就是备份，把 q1 中除队列中的最后一个元素外的所有元素都备份到 q2 中，即把 q1 备份到 q2 中。

然后弹出最后的元素，再把元素从 q2 导回 q1。

**下面的 php 代码是这种解法**

### 单队列模拟栈
其实这道题目就是用一个队列就够了。

一个队列在模拟栈弹出元素的时候只要将队列头部的元素（除了最后一个元素外） 重新添加到队列尾部，此时再去弹出元素就是栈的顺序了。

**下面的 go 代码是这种解法**


# 代码

### php
```php
class LeetCode0225 {

    private $q1;
    private $q2;

    /**
     * Initialize your data structure here.
     */
    function __construct() {
        $this->q1 = new SplQueue();
        $this->q2  = new SplQueue();
    }

    /**
     * Push element x onto stack.
     * @param Integer $x
     * @return NULL
     */
    function push($x) {
        $this->q1->enqueue($x);
    }

    /**
     * Removes the element on top of the stack and returns that element.
     * @return Integer
     */
    function pop() {
        if ($this->q1->isEmpty()) {
            return null;
        }
        while ($this->q1->count() != 1) {
            $this->q2->enqueue($this->q1->dequeue());
        }
        $res = $this->q1->dequeue();
        $this->swapQueue();
        return $res;
    }

    public function swapQueue()
    {
        $tmp = $this->q2;
        $this->q2 = $this->q1;
        $this->q1 = $tmp;
    }

    /**
     * Get the top element.
     * @return Integer
     */
    function top() {
        if ($this->q1->isEmpty()) {
            throw new Exception('empty');
        }
        while ($this->q1->count() != 1) {
            $this->q2->enqueue($this->q1->dequeue());
        }
        $res = $this->q1->dequeue();
        $this->q2->enqueue($res);
        $this->swapQueue();
        return $res;
    }

    /**
     * Returns whether the stack is empty.
     * @return Boolean
     */
    function empty() {
        return $this->q1->isEmpty();
    }
}
```

### go
```go
type MyStack struct {
    queue []int     //创建一个队列
}


func Constructor() MyStack {
    return MyStack{   //初始化
        queue: make([]int,0),
    }
}


func (this *MyStack) Push(x int)  {
    //添加元素
    this.queue = append(this.queue, x)
}


func (this *MyStack) Pop() int {
    //判断长度
    n := len(this.queue) - 1
    //除了最后一个，其余的都重新添加到队列里
    for n != 0{ 
        val := this.queue[0]
        this.queue = this.queue[1:]
        this.queue = append(this.queue, val)
        n--
    }
    //弹出元素
    val := this.queue[0]
    this.queue = this.queue[1:]
    return val
}


func (this *MyStack) Top() int {
    // 利用Pop函数，弹出来的元素重新添加
    val := this.Pop()
    this.queue = append(this.queue, val)
    return val
}


func (this *MyStack) Empty() bool {
    return len(this.queue) == 0
}
```
