# 0295. 数据流的中位数

# 题目
中位数是有序列表中间的数。如果列表长度是偶数，中位数则是中间两个数的平均值。

例如，
- [2, 3, 4] 的中位数是 3
- [2, 3] 的中位数是 (2 + 3) / 2 = 2.5

实现 MedianFinder 类：
- MedianFinder() 初始化 MedianFinder 对象。
- void addNum(int num) 将数据流中的整数 num 添加到数据结构中。
- double findMedian() 返回到目前为止所有元素的中位数。与实际答案相差 10-5 以内的答案将被接受。

进阶:
1. 如果数据流中所有整数都在 0 到 100 范围内，你将如何优化你的算法？
2. 如果数据流中 99% 的整数都在 0 到 100 范围内，你将如何优化你的算法？

https://leetcode.cn/problems/find-median-from-data-stream/description/


# 示例
```
输入
["MedianFinder", "addNum", "addNum", "findMedian", "addNum", "findMedian"]
[[], [1], [2], [], [3], []]
输出
[null, null, null, 1.5, null, 2.0]

解释
MedianFinder medianFinder = new MedianFinder();
medianFinder.addNum(1);    // arr = [1]
medianFinder.addNum(2);    // arr = [1, 2]
medianFinder.findMedian(); // 返回 1.5 ((1 + 2) / 2)
medianFinder.addNum(3);    // arr[1, 2, 3]
medianFinder.findMedian(); // return 2.0
```

# 解析
这里设计的结构有两个堆，一个大根堆，一个小根堆。
- 大根堆中含有接收的所有数中较小的一半，并且按大根堆的方式组织起来，那么这个堆的堆顶就是较小的一半数中最大的那个。
- 小根堆中含有接收的所有数中较大的一半，并且按小根堆的方式组织起来，那么这个堆的堆顶就是较大的一半数中最小的那个。

具体流程如下：
1. 第一个出现的数直接进入大根堆
2. 以后每一个新出现的数记为 cur，判断 cur 是否小于或等于大根堆的堆顶。如果是，cur 进入大根堆；如果不是，cur 进入小根堆。
3. 每一个数加入完成后，判断大根堆和小根堆的大小。如果个数较多的堆比个数较少的堆拥有数的个数超过 1，从个数较多的堆中弹出堆顶放入另一个堆。
4. 任何时候想得到所有数字的中位数，一定可以由两个堆的堆顶得到。

---

下面举例说明，大根堆 maxHeap，小根堆 minHeap，初始都为空，假设依次吐出的数字为：5、3、6、7。
- 当得到 5 时，直接进大根堆。maxHeap = [5]，minHeap = []
- 当得到 3 时，发现 3 小于等于大根堆的堆顶 5，所以进入大根堆。maxHeap = [5, 3]，minHeap=[]。  
  此时发现大根堆的数量为 2，小根堆的为 0，超过了1，所以大根堆堆顶弹出进入小根堆。maxHeap = [3]，minHeap = [5]。
- 当得到 6 时，发现 6 大于大根堆的堆顶 3，进入小根堆。maxHeap = [3]，minHeap = [5, 6]。两个堆的数量相比没有超过 1，不调整。
- 当得到 7 时，发现 7 大于大根堆的堆顶 3，进入小根堆。maxHeap = [3]，minHeap = [5, 6, 7]。  
  此时两个堆的数量相比超过了 1，小根堆堆顶弹出进入大根堆。maxHeap = [5, 3]，minHeap = [6, 7]。

通过观察会发现每次新加的数进入具体的一个堆，经过调整之后，大根堆中含有接受的所有数中较小的一半，小根堆含有较大的一半。这样通过两个堆的堆顶，
总可以得到所有数的中位数。取得中位数的操作时间复杂度为 O(1)，同时根据堆的性质，不管是大根堆还是小根堆，往其中加入一个新的数，
以及调整堆的代价都是 O(logN)。


# 代码

### php
```php
class LeetCode0295 {

    private $maxHeap;
    private $minHeap;

    public function __construct() {
        $this->maxHeap = new SplMaxHeap();
        $this->minHeap = new SplMinHeap();
    }

    public function addNum($num) {
        // 第一个数默认放到大根堆里
        if ($this->maxHeap->isEmpty()) {
            $this->maxHeap->insert($num);
            return;
        }
        // 之后每次从流中获取新数据时，如果小于等于大根堆的堆顶，放入大根堆；大于等于大根堆的堆顶就放入小根堆
        if ($num <= $this->maxHeap->top()) {
            $this->maxHeap->insert($num);
        } else {
            $this->minHeap->insert($num);
        }
        // 新数据放入某个堆后都需要保证两个堆中的数量差值不能大于1，如果数量差大于1，匀一个到另一个堆里即可
        if ($this->maxHeap->count() == $this->minHeap->count() + 2) {
            $this->minHeap->insert($this->maxHeap->extract());
        }
        if ($this->minHeap->count() == $this->maxHeap->count() + 2) {
            $this->maxHeap->insert($this->minHeap->extract());
        }
    }

    // 从两个堆中获取中位数
    public function findMedian() {
        $minSize = $this->minHeap->count();
        $maxSize = $this->maxHeap->count();

        $minHeapHead = $this->minHeap->isEmpty() ? 0 : $this->minHeap->top();
        $maxHeapHead = $this->maxHeap->isEmpty() ? 0 : $this->maxHeap->top();
        if ((($minSize + $maxSize) & 1) == 0) {
            return ($minHeapHead + $maxHeapHead) / 2;
        }
        return $maxSize > $minSize ? $maxHeapHead : $minHeapHead;
    }

}
```

### java
```java
class MedianFinder {

    private PriorityQueue<Integer> maxHeap;

    private PriorityQueue<Integer> minHeap;

    public MedianFinder() {
        this.maxHeap = new PriorityQueue<>((o1, o2) -> o2 - o1);
        this.minHeap = new PriorityQueue<>((o1, o2) -> o1 - o2);
    }
    
    public void addNum(int num) {
        if (this.maxHeap.isEmpty()) {
            this.maxHeap.add(num);
            return;
        }
        if (num <= this.maxHeap.peek()) {
            this.maxHeap.add(num);
        } else {
            this.minHeap.add(num);
        }
        if (this.maxHeap.size() >= this.minHeap.size() + 2) {
            this.minHeap.add(this.maxHeap.poll());
        }
        if (this.minHeap.size() >= this.maxHeap.size() + 2) {
            this.maxHeap.add(this.minHeap.poll());
        }
    }
    
    public double findMedian() {
        int minSize = this.minHeap.size();
        int maxSize = this.maxHeap.size();

        int minHeapHead = this.minHeap.size() == 0 ? 0 : this.minHeap.peek();
        int maxHeapHead = this.maxHeap.size() == 0 ? 0 : this.maxHeap.peek();
        if (((minSize + maxSize) & 1) == 0) {
            return ((double)minHeapHead + maxHeapHead) / 2;
        }
        return maxSize > minSize ? (double)maxHeapHead : (double)minHeapHead;
    }
}

```