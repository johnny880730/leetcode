# 394. 字符串解码

# 题目
给定一个经过编码的字符串，返回它解码后的字符串。

编码规则为: k[encoded_string]，表示其中方括号内部的 encoded_string 正好重复 k 次。注意 k 保证为正整数。

你可以认为输入字符串总是有效的；输入字符串中没有额外的空格，且输入的方括号总是符合格式要求的。

此外，你可以认为原始数据不包含数字，所有的数字只表示重复的次数 k ，例如不会出现像 3a 或 2[4] 的输入。

https://leetcode.cn/problems/decode-string

提示：
- 1 <= s.length <= 30
-  s 由小写英文字母、数字和方括号 '[]' 组成
-  s 保证是一个 有效 的输入。
-  s 中所有整数的取值范围为 [1, 300]

# 示例
```
输入：s = "3[a]2[bc]"
输出："aaabcbc"
```
```
输入：s = "3[a2[c]]"
输出："accaccacc"
```
```
输入：s = "2[abc]3[cd]ef"
输出："abcabccdcdcdef"
```
```
输入：s = "abc3[cd]xyz"
输出："abccdcdcdxyz"
```

# 解析

## 栈
数字存放在数字栈，字符串存放在字符串栈，遇到右括号时候弹出一个数字栈，字母栈弹到左括号为止


# 代码

### php
```php
class LeetCode0394 {

    function decodeString($s) {
        $numStack = new SplStack();
        $strStack = new SplStack();
        $str = '';      //当前正在累积的字符串
        for ($i = 0, $len = strlen($s); $i < $len; $i++) {
            if (ctype_digit($s[$i])) {
                // 当前位是数字
                $n = intval($s[$i]);
                // 数字可能有多位
                while (ctype_digit($s[++$i])) {
                    $n = 10 * $n + $s[$i];
                }
                //加入数字栈
                $numStack->push($n);
                // 往后退一步(for循环处有自增操作)
                --$i;
            } else if ($s[$i] == '[') {
                //将当前累积的字符串入栈
                $strStack->push($str);
                $str = '';
            } else if ($s[$i] == ']') {
                //将当前字符串按数字栈栈顶元素为倍数进行扩展
                $num = $numStack->pop();
                $str = str_repeat($str, $num);
                //字符串栈栈顶元素弹出来并与当前字符串拼接，作为新的当前正在累积的字符串
                $str = $strStack->pop() . $str;
            } else {
                //当前字符串继续累积
                $str .= $s[$i];
            }
        }
        return $str;
    }
}
```