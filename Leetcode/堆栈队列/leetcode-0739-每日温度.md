# 739. 每日温度

# 题目
给定一个整数数组 temperatures ，表示每天的温度，返回一个数组 answer ，其中 answer[i] 是指在第 i 天之后，才会有更高的温度。
如果气温在这之后都不会升高，请在该位置用 0 来代替。

https://leetcode.cn/problems/daily-temperatures

提示：
- 1 <= temperatures.length <= 10^5
- 30 <= temperatures[i] <= 100

# 示例
```
输入: temperatures = [73,74,75,71,69,72,76,73]
输出: [1,1,4,2,1,1,0,0]
```
```
输入: temperatures = [30,40,50,60]
输出: [1,1,1,0]
```


# 解析

首先想到的当然是暴力解法，两层for循环，把至少需要等待的天数就搜出来了。时间复杂度是 O(n^2)

那么接下来在来看看使用单调栈的解法。

那么就会问了：怎么能想到用单调栈呢？ 什么时候用单调栈呢？

通常是一维数组，要寻找任一个元素的右边或者左边第一个比自己大或者小的元素的位置，此时就要想到可以用单调栈了。时间复杂度为 O(n)。

例如本题其实就是找找到一个元素右边第一个比自己大的元素，此时就应该想到用单调栈了。

## 单调栈

那么单调栈的原理是什么呢？为什么时间复杂度是 O(n) 就可以找到每一个元素的右边第一个比它大的元素位置呢？

单调栈的本质是空间换时间，因为在遍历的过程中需要用一个栈来记录右边第一个比当前元素高的元素，优点是整个数组只需要遍历一次。

更直白来说，就是用一个栈来记录遍历过的元素，因为遍历数组的时候，不知道之前都遍历了哪些元素，以至于遍历一个元素找不到是不是之前遍历过一个更小的，
所以需要用一个容器（这里用单调栈）来记录遍历过的元素。

在使用单调栈的时候首先要明确如下几点：

1、单调栈里存放的元素是什么？

单调栈里只需要存放元素的下标 i 就可以了，如果需要使用对应的元素，直接 T[i] 就可以获取。

2、单调栈里元素是递增呢？ 还是递减呢？

以下关于顺序的描述都是 从栈头到栈底的顺序，因为单纯的说从左到右或者从前到后，不说栈头朝哪个方向的话，一定比较懵。

这里要使用递增循序（再强调一下是指从栈头到栈底的顺序），因为只有递增的时候，栈里要加入一个元素 i 的时候，
才知道栈顶元素在数组中右面第一个比栈顶元素大的元素是 i。

即：**如果求一个元素右边第一个更大元素，单调栈就是递增的，如果求一个元素右边第一个更小元素，单调栈就是递减的。**

使用单调栈主要有三个判断条件。
- 当前遍历的元素 T[i] 小于栈顶元素 T[st.top()] 的情况
- 当前遍历的元素 T[i] 等于栈顶元素 T[st.top()] 的情况
- 当前遍历的元素 T[i] 大于栈顶元素 T[st.top()] 的情况

把这三种情况分析清楚了，也就理解透彻了。

用 temperatures = [73, 74, 75, 71, 71, 72, 76, 73] 为例来逐步分析，输出应该是 [1, 1, 4, 2, 1, 1, 0, 0]。
- i = 0，一开始栈为空，直接加入栈。此时 stack 从头到底为 [0(73)]
- i = 1，由于 T[1] > 栈头元素 T[0]，为了保持单调递增栈，栈头元素弹出，为 T[0]，记录 res[0] = i - 0 = 1 - 0 = 1。栈已经清空，
  当前 i 入栈。此时栈头到栈底为 [1(74)]
- i = 2，由于 T[2] > 栈头元素 T[1]，为了保持单调递增栈，栈头元素弹出，为 T[1]，记录 res[1] = i - 1 = 2 - 1 = 1，栈已经清空，
  当前 i 入栈。此时栈头到栈底为 [2(75)]
- i = 3，由于 T[3] < 栈头元素 T[2]，当前 i 直接入栈。此时栈头到栈底为 [3(71), 2(75)]
- i = 4，由于 T[4] == 栈头元素 T[3]，当前 i 直接入栈。此时栈头到栈底为 [4(71), 3(71), 2(75)]
- i = 5，由于 T[5] > 栈头元素 T[4]，为了保持单调递增栈，栈头元素弹出，为 T[4]，记录 res[4] = i - 4 = 5 - 4 = 1。
  此时栈头到栈底为 [3(71), 2(75)]
- i = 5，由于 T[5] > 栈头元素 T[3]，为了保持单调递增栈，栈头元素弹出，为 T[3]，记录 res[3] = i - 3 = 5 - 3 = 2。
  此时栈头到栈底为 [2(75)]
- i = 5，由于 T[5] < 栈头元素 T[2]，当前 i 入栈。此时栈头到栈底为 [5(72), 2(75)]
- i = 6，由于 T[6] > 栈头元素 T[5]，为了保持单调递增栈，栈头元素弹出，为 T[5]，记录 res[1] = i - 5 = 6 - 5 = 1。
  此时栈头到栈底为 [2(75)]
- i = 6，由于 T[6] > 栈头元素 T[2]，为了保持单调递增栈，栈头元素弹出，为 T[2]，记录 res[1] = i - 2 = 6 - 2 = 4。栈已经清空，
  当前 i 入栈。此时栈头到栈底为 [6(76)]
- i = 7，由于 T[7] > 栈头元素 T[6]，当前 i 直接入栈。此时栈头到栈底为 [7(73), 6(76)]
- 遍历已经结束。栈里还存在元素说明右侧没有比他更大的元素了，直接记录为 0 即可。做法就是定义结果数组 result 的时候初始化为 0。

通过以上过程就会发现：只有单调栈递增（从栈头到栈底顺序），就是求右边第一个比自己大的，单调栈递减的话，就是求右边第一个比自己小的。

# 代码

### php
```php
class LeetCode0739 {

    function dailyTemperatures($temperatures) {
        $len = count($temperatures);
        $res = array_fill(0, $len, 0);
        $stack = new SplStack();
        // 单调递增栈
        for ($i = 0; $i < $len; $i++) {
            $num = $temperatures[$i];
            while ($stack->isEmpty() == false && $num > $temperatures[$stack->top()]) {
                $prevIndex = $stack->pop();
                $res[$prevIndex] = $i - $prevIndex;
            }
            $stack->push($i);
        }
        return $res;
    }
}
```

### go
```go
func dailyTemperatures(temperatures []int) []int {
    size := len(temperatures)
	res := make([]int, size)
	stack := make([]int, 0)
	for i := 0; i < size; i++ {
		for len(stack) > 0 && temperatures[i] > temperatures[stack[len(stack) - 1]] {
			idx := stack[len(stack) - 1]
			stack = stack[: len(stack) - 1]
			res[idx] = i - idx
		}
		stack = append(stack, i)
	}
	return res
}
```

### java
```java
class LeetCode0739 {
    
    public int[] dailyTemperatures(int[] temperatures) {
        int[] res = new int[temperatures.length];
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < temperatures.length; i++) {
            while (stack.size() > 0 && temperatures[i] > temperatures[stack.peek()]) {
                int idx = stack.pop();
                res[idx] = i - idx;
            }
            stack.push(i);
        }
        return res;
    }
}
```