# 496. 下一个更大元素 I

# 题目
nums1 中数字 x 的 下一个更大元素 是指 x 在  nums2 中对应位置 右侧 的 第一个 比 x 大的元素。

给你两个 没有重复元素 的数组  nums1 和  nums2 ，下标从 0 开始计数，其中 nums1 是 nums2 的子集。

对于每个 0 <= i < nums1.length ，找出满足 nums1[i] == nums2[j] 的下标 j ，并且在 nums2 确定 nums2[j] 的 下一个更大元素 。
如果不存在下一个更大元素，那么本次查询的答案是 -1 。

返回一个长度为  nums1.length 的数组 ans 作为答案，满足 ans[i] 是如上所述的 下一个更大元素 。

https://leetcode.cn/problems/next-greater-element-i

提示：
- 1 <= nums1.length <= nums2.length <= 1000
- 0 <= nums1[i], nums2[i] <= 10^4
- nums1和nums2中所有整数 互不相同
- nums1 中的所有整数同样出现在 nums2 中


# 示例
```
输入：nums1 = [4,1,2], nums2 = [1,3,4,2].
输出：[-1,3,-1]
解释：nums1 中每个值的下一个更大元素如下所述：
- 4 ，不存在下一个更大元素，所以答案是 -1 。
- 1 ，下一个更大元素是 3 。
- 2 ，。不存在下一个更大元素，所以答案是 -1 。
```
```
输入：nums1 = [2,4], nums2 = [1,2,3,4].
输出：[3,-1]
解释：nums1 中每个值的下一个更大元素如下所述：
- 2 ，下一个更大元素是 3 。
- 4 ，不存在下一个更大元素，所以答案是 -1 。
```


# 解析
在 《739. 每日温度》 中是求每个元素下一个比当前元素大的元素的位置。本题则是说 nums1 是 nums2 的子集，找 nums1 中的元素在 nums2 
中下一个比当前元素大的元素。看上去和 《739. 每日温度》 就如出一辙了。几乎是一样的，但是这么绕了一下，其实还上升了一点难度。

需要对单调栈使用的更熟练一些，才能顺利的把本题写出来。

从题目示例中可以看出最后是要求 nums1 的每个元素在 nums2 中下一个比当前元素大的元素，那么就要定义一个和 nums1 一样大小的数组 result 来存放结果。

这么定义这个 result 数组初始化应该为多少呢？题目说如果不存在对应位置就输出 -1 ，所以 result 数组如果某位置没有被赋值，那么就应该是是-1，所以就初始化为-1。

在遍历 nums2 的过程中，要判断 nums2[i] 是否在 nums1中 出现过，因为最后是要根据 nums1 元素的下标来更新 result 数组。

注意题目中说是两个没有重复元素 的数组 nums1 和 nums2。没有重复元素，就可以用 map 来做映射了。根据数值快速找到下标，
还可以判断 nums2[i] 是否在 nums1 中出现过。

使用单调栈，首先要想单调栈是从大到小还是从小到大。

本题 《739. 每日温度》 是一样的。栈头到栈底的顺序，要从小到大，也就是保持栈里的元素为递增顺序。只要保持递增，才能找到右边第一个比自己大的元素。

接下来就要分析如下三种情况，一定要分析清楚。
- 情况一：当前遍历的元素 T[i] 小于栈顶元素 T[st.top()] 的情况 
  - 此时满足递增栈（栈头到栈底的顺序），所以直接入栈。
- 情况二：当前遍历的元素 T[i] 等于栈顶元素 T[st.top()] 的情况
  - 如果相等的话，依然直接入栈，因为我们要求的是右边第一个比自己大的元素，而不是大于等于
- 情况三：当前遍历的元素 T[i] 大于栈顶元素 T[st.top()] 的情况
  - 此时如果入栈就不满足递增栈了，这也是找到右边第一个比自己大的元素的时候。

判断栈顶元素是否在 nums1 里出现过，（注意栈里的元素是 nums2 的元素），如果出现过，开始记录结果。

记录结果这块逻辑有一点小绕，要清楚，此时栈顶元素在 nums2 数组中右面第一个大的元素是 nums2[i]（即当前遍历元素）。


# 代码

### php
```php

class LeetCode0496 {

    function nextGreaterElement($nums1, $nums2) {
        $len1 = count($nums1);
        $res = array_fill(0, $len1, -1);
        if ($len1 <= 0) {
            return $res;
        }
        // key: 下标所指元素；val：下标i
        $map = array();
        foreach ($nums1 as $k => $v) {
            $map[$v] = $k;
        }
        
        $stack = new SplStack();
        $stack->push(0);
        for ($i = 1; $i < count($nums2); $i++) {
            while (!$stack->isEmpty() && $nums2[$i] > $nums2[$stack->top()]) {
                $top = $stack->pop();
                if (array_key_exists($nums2[$top], $map)) {
                    $idx = $map[$nums2[$top]];
                    $res[$idx] = $nums2[$i];
                } 
            }
            $stack->push($i);
        }
        return $res;
    }
}
```

### go
```go
func nextGreaterElement(nums1 []int, nums2 []int) []int {
    res := make([]int, len(nums1))
    for k := range res {
        res[k] = -1
    }
    mp := make(map[int]int)
    for k, v := range nums1 {
        mp[v] = k
    }

    st := make([]int, 0)
    st = append(st, 0)
    for i := 1; i < len(nums2); i++ {
        for len(st) > 0 && nums2[i] > nums2[st[len(st) - 1]] {
            top := st[len(st) - 1]
            if _, ok := mp[nums2[top]]; ok {
                idx := mp[nums2[top]]
                res[idx] = nums2[i]
            }
            st = st[: len(st) - 1]
        }
        st = append(st, i)
    }
    return res
}
```