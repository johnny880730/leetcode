# 71. 简化路径

# 题目
给你一个字符串 path ，表示指向某一文件或目录的 Unix 风格 绝对路径 （以 '/' 开头），请你将其转化为更加简洁的规范路径。

在 Unix 风格的文件系统中，一个点（.）表示当前目录本身；此外，两个点 （..） 表示将目录切换到上一级（指向父目录）；
两者都可以是复杂相对路径的组成部分。任意多个连续的斜杠（即，'//'）都被视为单个斜杠 '/' 。 
对于此问题，任何其他格式的点（例如，'...'）均被视为文件/目录名称。

请注意，返回的 规范路径 必须遵循下述格式：
- 始终以斜杠 '/' 开头。
- 两个目录名之间必须只有一个斜杠 '/' 。
- 最后一个目录名（如果存在）不能 以 '/' 结尾。
- 此外，路径仅包含从根目录到目标文件或目录的路径上的目录（即，不含 '.' 或 '..'）。

返回简化后得到的 规范路径 。

https://leetcode.cn/problems/simplify-path/description/

提示：
- 1 <= path.length <= 3000
- path 由英文字母，数字，'.'，'/' 或 '_' 组成。
- path 是一个有效的 Unix 风格绝对路径。


# 示例
```
输入：path = "/home/"
输出："/home"
解释：注意，最后一个目录名后面没有斜杠。 
```
```
输入：path = "/../"
输出："/"
解释：从根目录向上一级是不可行的，因为根目录是你可以到达的最高级。
```
```
输入：path = "/a/./b/../../c/"
输出："/c"
```


# 解析

## 栈

首先将给定的字符串 path 根据 / 分割成一个由若干字符串组成的列表，记为 arr。根据题目中规定的「规范路径的下述格式」，arr 中包含的字符串只能为以下几种：
- 空字符串。例如当出现多个连续的 /，就会分割出空字符串
- 一个点 .
- 两个点 ..
- 只包含英文字母、数字或 _ 的目录名。

对于「空字符串」以及「一个点」，实际上无需对它们进行处理，因为「空字符串」没有任何含义，而「一个点」表示当前目录本身，无需切换目录。

对于「两个点」或者「目录名」，则可以用一个栈（这里用的双端队列）来维护路径中的每一个目录名。当遇到「两个点」时，
需要将目录切换到上一级，因此只要栈【队列】不为空，就弹出栈顶的目录。当遇到「目录名」时，就把它放入栈。

这样一来，只需要遍历 arr 中的每个字符串并进行上述操作即可。在所有的操作完成后，将从栈底到栈顶的字符串用 / 进行连接，
再在最前面加上 / 表示根目录，就可以得到简化后的规范路径。

# 代码

### php
```php
class LeetCode0071 {

    function simplifyPath($path) {
        $arr = explode('/', $path);
        $stack = [];
        foreach ($arr as $item) {
            if (!in_array($item, ['', '.', '..'])) {
                $stack[] = $item;
            } else if ($item == '..' && count($stack) > 0) {
                array_pop($stack);
            }
            if ($item == '' || $item == '.') {
                continue;
            }
        }
        return '/' . join('/' $stack);
    }
}
```

### go
```go
func simplifyPath(path string) string {
    arr := strings.Split(path, "/")
    stack := []string{}
    for _, item := range arr {
        if item != "" && item != "." && item != ".." {
            stack = append(stack, item)
        } else if item == ".." && len(stack) > 0 {
            stack = stack[ : len(stack) - 1]
        }
    }
    return "/" + strings.Join(stack, "/")
}
```

### java
```java
class LeetCode0071 {

    public String simplifyPath(String path) {
        String[] arr = path.split("/");
        Stack<String> stack = new Stack<>();
        for (String item : arr) {
            if (!item.equals("") && !item.equals(".") && !item.equals("..")) {
                stack.push(item);
            } else if (item.equals("..") && !stack.isEmpty()) {
                stack.pop();
            }
        }
        StringBuilder result = new StringBuilder("/");
        while (!stack.isEmpty()) {
            result.insert(1, stack.pop() + "/");
        }
        if (result.length() > 1) {
            result.deleteCharAt(result.length() - 1);
        }
        return result.toString();
    }
}
```