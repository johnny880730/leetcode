# 503. 下一个更大元素 II

# 题目
给定一个循环数组 nums （ nums[nums.length - 1] 的下一个元素是 nums[0] ），返回 nums 中每个元素的 下一个更大元素 。

数字 x 的 下一个更大的元素 是按数组遍历顺序，这个数字之后的第一个比它更大的数，这意味着你应该循环地搜索它的下一个更大的数。如果不存在，则输出 -1 。

https://leetcode.cn/problems/next-greater-element-ii

提示：
- 1 <= nums.length <= 10^4
- -10^9 <= nums[i] <= 10^9

# 示例
```
输入: nums = [1,2,1]
输出: [2,-1,2]
解释: 第一个 1 的下一个更大的数是 2；
数字 2 找不到下一个更大的数； 
第二个 1 的下一个最大的数需要循环搜索，结果也是 2。
```
```
输入: nums = [1,2,3,4,3]
输出: [2,3,4,-1,4]
```


# 解析
做本题之前建议先做 《739. 每日温度》 和 《496. 下一个更大元素 I》。

这道题和 《739. 每日温度》。

不过，本题要循环数组了。如何处理循环数组？有个思路是直接把两个数组拼接在一起，然后使用单调栈求下一个最大值。

这么做确实可以。将两个 nums 数组拼接在一起，使用单调栈计算出每一个元素的下一个最大值，最后再把结果集即 result 数组 resize 
到原数组大小就可以了。

这种写法确实比较直观，但做了很多无用操作，例如修改了 nums 数组，而且最后还要把 result 数组 resize 回去。

resize 倒是不费时间，是 O(1) 的操作，但扩充 nums 数组相当于多了 一个O(n) 的操作。

其实也可以不扩充 nums，而是在遍历的过程中模拟走了两边 nums。

方法就是 for 循环的时候，i 最大取到两倍的数组长度。靠 i % len(nums) 操作来防止 i 溢出。

# 代码

### php
```php
class LeetCode0503 {

    function nextGreaterElements($nums) {
        $len = count($nums);
        $res = array_fill(0, $len, -1);
        $stack = new SplStack();
        $stack->push(0);
        for ($i = 1; $i < 2 * $len; $i++) {
            while (!$stack->isEmpty() && $nums[$i % $len] > $nums[$stack->top()]) {
                $res[$stack->pop()] = $nums[$i % $len];
            }
            $stack->push($i % $len);
        }
        return $res;
    }
}
```

### go
```go
func nextGreaterElements(nums []int) []int {
    size := len(nums)
    res := make([]int, size)
    for k := range res {
        res[k] = -1
    }
    st := make([]int, 0)
    st = append(st, 0)
    for i := 1; i < 2 * size; i++ {
        for len(st) > 0 && nums[i % size] > nums[st[len(st) - 1]] {
            top := st[len(st) - 1]
            res[top] = nums[i % size]
            st = st[: len(st) - 1]
        }
        st = append(st, i % size)
    }
    return res
}
```