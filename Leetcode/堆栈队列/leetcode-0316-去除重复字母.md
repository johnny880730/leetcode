### 316. 去除重复字母

# 题目
给你一个字符串 s ，请你去除字符串中重复的字母，使得每个字母只出现一次。

需保证 返回结果的字典序最小（要求不能打乱其他字符的相对位置）。

https://leetcode.cn/problems/remove-duplicate-letters/

提示：
- 1 <= s.length <= 10<sup>4</sup>
- s 由小写英文字母组成

# 示例
```
输入：s = "bcabc"
输出："abc"
```
```
输入：s = "cbacdcbc"
输出："acdb"
```


# 解析

##### 贪心 + 单调栈

&emsp;&emsp;先考虑一个简单的问题：给定一个字符串 s，如何去掉其中的一个字符 ch，使得得到的字符串字典序最小呢？
答案是：找出最小的满足 s<sub>i</sub>>s<sub>i+1</sub> 的下标 i，并去除字符 s<sub>i</sub>。为了叙述方便，称这样的字符为「关键字符」。

&emsp;&emsp;一个直观的思路是：在字符串 s 中找到「关键字符」，去除它，然后不断进行这样的循环。但是这种朴素的解法会创建大量的中间字符串，有必要寻找一种更优的方法。

&emsp;&emsp;从前向后扫描原字符串。每扫描到一个位置，就尽可能地处理所有的「关键字符」。假定在扫描位置 s<sub>i-1</sub> 之前的所有「关键字符」都已经被去除完毕，
在扫描字符 s<sub>i</sub> 时，新出现的「关键字符」只可能出现在 s<sub>i</sub> 或者其后面的位置。

&emsp;&emsp;于是，使用单调栈来维护去除「关键字符」后得到的字符串，单调栈满足栈底到栈顶的字符递增。如果栈顶字符大于当前字符 s<sub>i</sub>，
说明栈顶字符为「关键字符」，故应当被去除。去除后，新的栈顶字符就与 s<sub>i</sub> 相邻了，继续比较新的栈顶字符与 s<sub>i</sub> 的大小。
重复上述操作，直到栈为空或者栈顶字符不大于 s<sub>i</sub>。

&emsp;&emsp;还遗漏了一个要求：原字符串 s 中的每个字符都需要出现在新字符串中，且只能出现一次。为了让新字符串满足该要求，之前讨论的算法需要进行以下两点的更改。
- 在考虑字符 s<sub>i</sub> 时，如果它已经存在于栈中，则不能加入字符 s<sub>i</sub>。为此，需要记录每个字符是否出现在栈中。
- 在弹出栈顶字符时，如果字符串在后面的位置上再也没有这一字符，则不能弹出栈顶字符。
  为此，需要记录每个字符的剩余数量，当这个值为 0 时，就不能弹出栈顶字符了。

# 代码

```php
$arg = "cbacdcbc";
(new LeetCode1046())->main($arg);

class LeetCode0316
{
    public function main($arg) {
        var_dump($this->removeDuplicateLetters($arg));
    }

    function removeDuplicateLetters($s) {
        $vis = array_fill(0, 26, false);
        $num = array_fill(0, 26, 0);
        for ($i = 0, $len = strlen($s); $i < $len; $i++) {
            $num[ord($s[$i]) - ord('a')]++;
        }
        $res = '';
        for ($i = 0; $i < $len; $i++) {
            $ch = $s[$i];
            if (!$vis[ord($s[$i]) - ord('a')]) {
                while (strlen($res) > 0 && $res[strlen($res) - 1] > $ch) {
                    if ($num[ord($res[strlen($res) - 1]) - ord('a')] > 0) {
                        $vis[ord($res[strlen($res) - 1]) - ord('a')] = false;
                        $res = substr($res, 0, strlen($res) - 1);
                    } else {
                        break;
                    }
                }
                $vis[ord($ch) - ord('a')] = true;
                $res .= $ch;
            }
            $num[ord($ch) - ord('a')] -= 1;
        }
        return $res;
    }
}
```