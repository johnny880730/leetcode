# 0200. 岛屿数量

# 题目
给你一个由 '1'（陆地）和 '0'（水）组成的的二维网格，请你计算网格中岛屿的数量。

岛屿总是被水包围，并且每座岛屿只能由水平方向和/或竖直方向上相邻的陆地连接形成。

此外，你可以假设该网格的四条边均被水包围。

https://leetcode.cn/problems/number-of-islands/description/

提示：
- m == grid.length
- n == grid[i].length
- 1 <= m, n <= 300
- grid[i][j] 的值为 '0' 或 '1'

# 示例
```
输入：grid = [
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]
输出：1
```

```
输入：grid = [
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]
输出：3
```

# 解析

## DFS
主要思路是 dfs，先遍历

查到的第一个陆地标记为 '2'，开始深度搜索，查找周围的陆地，如果有就标记为 '2'。

全部标记完成以后就找到了一块陆地，下次再找到 '1' 则是第二块陆地了。

使用指针，节省空间，dfs 前先判断是否是陆地，节省时间。

# 代码

### php
```php

class LeetCode0200 {

    function numIslands($grid) {
        $res = 0;
        $len1 = count($grid);
        $len2 = count($grid[0]);
        for ($i = 0; $i < $len1; $i++) {
            for ($j = 0; $j < $len2; $j++) {
                if ($grid[$i][$j] == '1') {
                    $res++;
                    $this->_dfs($grid, $i, $j, $len1, $len2);
                }    
            }
        }
        return $res;
    }

    // 目前来到 grid[i][j]，经历上下左右的感染过程 
    function _dfs(&$grid, $i, $j, $len1, $len2) {
        if ($i < 0 || $i >= $len1 || $j < 0 || $j >= $len2 || $grid[$i][$j] != '1') {
            return;
        }
        $grid[$i][$j] = '2';
        $this->_dfs($grid, $i + 1, $j, $len1, $len2);
        $this->_dfs($grid, $i - 1, $j, $len1, $len2);
        $this->_dfs($grid, $i, $j + 1, $len1, $len2);
        $this->_dfs($grid, $i, $j - 1, $len1, $len2);
    }
}
```

### go
```go
func numIslands(grid [][]byte) int {
    res := 0
    len1, len2 := len(grid), len(grid[0])
    for i := 0; i < len1; i++ {
        for j := 0; j < len2; j++ {
            if grid[i][j] == '1' {
                res++
                _dfs(grid, i, j, len1, len2)
            }
        }
    }
    return res
}

func _dfs(grid [][]byte, i int, j int, len1 int, len2 int) {
    if i < 0 || i >= len1 || j < 0 || j >= len2 || grid[i][j] != '1' {
        return
    }
    grid[i][j] = '2'
    _dfs(grid, i + 1, j, len1, len2)
    _dfs(grid, i - 1, j, len1, len2)
    _dfs(grid, i, j + 1, len1, len2)
    _dfs(grid, i, j - 1, len1, len2)
}
```

### java
```java
class LeetCode0200 {

    public int numIslands(char[][] grid) {
        int res = 0;
        int len1 = grid.length, len2 = grid[0].length;
        for (int i = 0; i < len1; i++) {
            for (int j = 0; j < len2; j++) {
                if (grid[i][j] == '1') {
                    res++;
                    _dfs(grid, i, j, len1, len2);
                }
            }
        }

        return res;
    }

    private void _dfs(char[][] grid, int i, int j, int len1, int len2) {
        if (i < 0 || j < 0 || i >= len1 || j >= len2 || grid[i][j] != '1') {
            return;
        }
        grid[i][j] = '2';
        _dfs(grid, i + 1, j, len1, len2);
        _dfs(grid, i - 1, j, len1, len2);
        _dfs(grid, i, j + 1, len1, len2);
        _dfs(grid, i, j - 1, len1, len2);
    }
}
```
