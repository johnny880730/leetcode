# 0130. 被围绕的区域

# 题目
给你一个 m x n 的矩阵 board ，由若干字符 'X' 和 'O' 组成，捕获 所有 被围绕的区域：
- 连接：一个单元格与水平或垂直方向上相邻的单元格连接。
- 区域：连接所有 '0' 的单元格来形成一个区域。
- 围绕：如果您可以用 'X' 单元格 连接这个区域，并且区域中没有任何单元格位于 board 边缘，则该区域被 'X' 单元格围绕。。

https://leetcode.cn/problems/surrounded-regions/description/

提示：
- m == board.length
- n == board[i].length
- 1 <= m, n <= 200
- board[i][j] 为 'X' 或 'O'

# 示例
```
输入：board = [
    ["X","X","X","X"],
    ["X","O","O","X"],
    ["X","X","O","X"],
    ["X","O","X","X"]
]
输出：[
    ["X","X","X","X"],
    ["X","X","X","X"],
    ["X","X","X","X"],
    ["X","O","X","X"]
]
解释：被围绕的区间不会存在于边界上，换句话说，任何边界上的 'O' 都不会被填充为 'X'。 
任何不在边界上，或不与边界上的 'O' 相连的 'O' 最终都会被填充为 'X'。如果两个元素在水平或垂直方向相邻，则称它们是“相连”的。
```

# 解析

## DFS
本题给定的矩阵中有三种元素：
- 字母 X；
- 被字母 X 包围的字母 O；
- 没有被字母 X 包围的字母 O。

本题要求将所有被字母 X 包围的字母 O 都变为字母 X ，但很难判断哪些 O 是被包围的，哪些 O 不是被包围的。

注意到题目解释中提到：任何边界上的 O 都不会被填充为 X。 可以想到，所有的不被包围的 O 都直接或间接与边界上的 O 相连。

可以利用这个性质判断 O 是否在边界上，具体地说：
- 对于每一个边界上的 O，以它为起点，标记所有与它直接或间接相连的字母 O
- 最后遍历这个矩阵，对于每一个字母：
  - 如果该字母被标记过，则该字母为没有被字母 X 包围的字母 O，我们将其还原为字母 O；
  - 如果该字母没有被标记过，则该字母为被字母 X 包围的字母 O，我们将其修改为字母 X。

可以用深度优先搜索，先搜索边界上的 O 以及与边界相连的 O，将其先标记为 #。

最后遍历一遍 board，将所有 # 变换为 O，将所有 O 变换为 X。

# 代码

```php
class LeetCode0130 {

    function solve(&$board) {
        $len1 = count($board);
        $len2 = count($board[0]);
        // 检测每一行的第一个和最后一个
        for ($i = 0; $i < $len1; $i++) {
            $this->_dfs($board, $i, 0);
            $this->_dfs($board, $i, $len2 - 1);
        }
        // 检测每一列的第一个和最后一个
        for ($i = 0; $i < $len2; $i++) {
            $this->_dfs($board, 0, $i);
            $this->_dfs($board, $len1 - 1, $i);
        }
        for ($i = 0; $i < $len1; $i++) {
            for ($j = 0; $j < $len2; $j++) {
                if ($board[$i][$j] == '#') {
                    $board[$i][$j] = 'O';
                } else if ($board[$i][$j] == 'O') {
                    $board[$i][$j] = 'X';
                }
            }
        }
    }
    
    function _dfs(&$board, $i, $j) {
        if ($i < 0 || $i >= count($board) || $j < 0 || $j >= count($board[0]) || $board[$i][$j] != 'O') {
            return;
        }
        $board[$i][$j] = '#';
        $this->_dfs($board, $i + 1, $j);
        $this->_dfs($board, $i - 1, $j);
        $this->_dfs($board, $i, $j + 1);
        $this->_dfs($board, $i, $j - 1);
    }
}

```

### go
```go
func solve(board [][]byte)  {
    var _dfs func(board [][]byte, i, j int)
    _dfs = func(board [][]byte, i, j int) {
        if i < 0 || i >= len(board) || j < 0 || j >= len(board[0]) || board[i][j] != 'O' {
            return
        }
        board[i][j] = '#'
        _dfs(board, i + 1, j)
        _dfs(board, i - 1, j)
        _dfs(board, i, j + 1)
        _dfs(board, i, j - 1)
    }

    len1, len2 := len(board), len(board[0])
    for i := 0; i < len1; i++ {
        _dfs(board, i, 0)
        _dfs(board, i, len2 - 1)
    }
    for i := 0; i < len2; i++ {
        _dfs(board, 0, i)
        _dfs(board, len1 - 1, i)
    }
    for i := 0; i < len1; i++ {
        for j := 0; j < len2; j++ {
            if board[i][j] == '#' {
                board[i][j] = 'O'
            } else if board[i][j] == 'O' {
                board[i][j] = 'X'
            }
        }
    }
}
```

### java
```java
class Solution {

    public void solve(char[][] board) {
        int row = board.length;
        int col = board[0].length;
        for (int i = 0; i < row; i++) {
            _dfs(board, i, 0);
            _dfs(board, i, col - 1);
        }
        for (int i = 0; i < col; i++) {
            _dfs(board, 0, i);
            _dfs(board, row - 1, i);
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (board[i][j] == '#') {
                    board[i][j] = 'O';
                } else if (board[i][j] == 'O') {
                    board[i][j] = 'X';
                }
            }
        }
    }

    protected void _dfs(char[][] board, int i, int j) {
        if (i < 0 || i >= board.length) {
            return;
        }
        if (j < 0 || j >= board[0].length) {
            return;
        }
        if (board[i][j] != 'O') {
            return;
        }
        board[i][j] = '#';
        _dfs(board, i + 1, j);
        _dfs(board, i - 1, j);
        _dfs(board, i, j + 1);
        _dfs(board, i, j - 1);
    }
}

```
