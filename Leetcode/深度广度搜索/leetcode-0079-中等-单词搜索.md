# 0079. 单词搜索

# 题目
给定一个 m x n 二维字符网格 board 和一个字符串单词 word 。如果 word 存在于网格中，返回 true ；否则，返回 false 。

单词必须按照字母顺序，通过相邻的单元格内的字母构成，其中“相邻”单元格是那些水平相邻或垂直相邻的单元格。同一个单元格内的字母不允许被重复使用。

https://leetcode.cn/problems/word-search/description/

进阶：你可以使用搜索剪枝的技术来优化解决方案，使其在 board 更大的情况下可以更快解决问题？

提示：
- m == board.length
- n = board[i].length
- 1 <= m, n <= 6
- 1 <= word.length <= 15
- board 和 word 仅由大小写英文字母组成

# 示例
```
输入：board = [
    ["A","B","C","E"],
    ["S","F","C","S"],
    ["A","D","E","E"]
], word = "ABCCED"
输出：true
```

```
输入：board = [
    ["A","B","C","E"],
    ["S","F","C","S"],
    ["A","D","E","E"]
], word = "SEE"
输出：true
```

```
输入：board = [
    ["A","B","C","E"],
    ["S","F","C","S"],
    ["A","D","E","E"]
], word = "ABCB"
输出：false
```

# 解析

## DFS + 回溯

主要思想：从 [0, 0] 点作为出发点看看能否走出 word，不能的话从 [0, 1] 点作为出发点看能否走出 word，以此类推。

每次走的时候，查看坐标是否越界、查看当前字符是否为 word 对应位置的字符。如果都通过，就从当前网址往上下左右四个方向移动一格。

如何防止回到已经走过的格子：用变量 tmp 记录当前格子的字符，将当前格子的字符改写成别的无关的字符，走完之后回溯的时候再改回来即可。


# 代码

### php
```php
class LeetCode0079 {
    
    function exist($board, $word) {
        for ($i = 0, $len1 = count($board); $i < $len1; $i++) {
            for ($j = 0, $len2 = count($board[0]); $j < $len2; $j++) {
                if ($this->_walk($board, $i, $j, $word, 0)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    // 目前到达了 board[i][j]，word 还剩下 [k..] 需要搞定
    // 看看如果从 board[i][j]，能不能搞定 word[k..]
    protected function _walk(&$board, $i, $j, $word, $k){
        if ($k == strlen($word)) {
            return true;
        }
        // 检测越界
        if ($i < 0 || $i >= count($board) || $j < 0 || $j >= count($board[0])) {
            return false;
        }
        // 检测当前字符是否符合
        if ($board[$i][$j] != $word[$k]) {
            return false;
        }
        $tmp = $board[$i][$j];
        // 改掉当前位置的字符，说明已经走过了
        $board[$i][$j] = 0;
        $res = $this->_walk($board, $i - 1, $j, $word, $k + 1)
            || $this->_walk($board, $i + 1, $j, $word, $k + 1)
            || $this->_walk($board, $i, $j - 1, $word, $k + 1)
            || $this->_walk($board, $i, $j + 1, $word, $k + 1);
        // [i][j] 位置恢复原状
        $board[$i][$j] = $tmp;
        return $res;
        
    }
}
```

### go
```go
func exist(board [][]byte, word string) bool {
    var _walk func(board [][]byte, i int, j int, word string, idx int) bool
    _walk = func(board [][]byte, i int, j int, word string, idx int) bool {
        if idx == len(word) {
            return true
        }
        if i < 0 || i >= len(board) || j < 0 || j >= len(board[0]) {
            return false
        }
        if board[i][j] != word[idx] {
            return false
        }
        tmp := board[i][j]
        board[i][j] = '0'
        res := _walk(board, i + 1, j, word, idx + 1)
        res = res || _walk(board, i - 1, j, word, idx + 1)
        res = res || _walk(board, i, j + 1, word, idx + 1)
        res = res || _walk(board, i, j - 1, word, idx + 1)
        board[i][j] = tmp

        return res
    }
    
    len1, len2 := len(board), len(board[0])
    for i := 0; i < len1; i++ {
        for j := 0; j < len2; j++ {
            if _walk(board, i, j, word, 0) {
                return true
            }
        }
    }

    return false
}
```

### java
```java
class LeetCode0079 {

    public boolean exist(char[][] board, String word) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (_walk(board, i, j, word, 0)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected boolean _walk(char[][] board, int i, int j, String word, int k) {
        if (k == word.length()) {
            return true;
        }
        if (i < 0 || i >= board.length || j < 0 || j >= board[0].length) {
            return false;
        }
        if (board[i][j] != word.charAt(k)) {
            return false;
        }
        char tmp = word.charAt(k);
        board[i][j] = '#';
        boolean res = _walk(board, i + 1, j, word, k + 1)
                   || _walk(board, i, j + 1, word, k + 1)
                   || _walk(board, i - 1, j, word, k + 1)
                   || _walk(board, i, j - 1, word, k + 1);
        board[i][j] = tmp;
        return res;
    }
}
```
