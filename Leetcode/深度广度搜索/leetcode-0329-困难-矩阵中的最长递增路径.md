# 329. 矩阵中的最长递增路径

# 题目
给定一个 m x n 整数矩阵matrix ，找出其中 最长递增路径 的长度。

对于每个单元格，你可以往上，下，左，右四个方向移动。 你 不能 在 对角线 方向上移动或移动到 边界外（即不允许环绕）。

https://leetcode.cn/problems/longest-increasing-path-in-a-matrix

提示：
- m == matrix.length
- n == matrix[i].length
- 1 <= m, n <= 200
- 0 <= matrix[i][j] <= 2^31 - 1

# 示例
```
输入：matrix = [[9,9,4],[6,6,8],[2,1,1]]
输出：4 
解释：最长递增路径为[1, 2, 6, 9]。
```

```
输入：matrix = [[3,4,5],[3,2,6],[2,2,1]]
输出：4 
解释：最长递增路径是[3, 4, 5, 6]。注意不允许在对角线方向上移动。
```

```
输入：matrix = [[1]]
输出：1
```

# 解析

## 记忆化深度优先搜索
将矩阵看成一个有向图，每个单元格对应图中的一个节点，如果相邻的两个单元格的值不相等，则在相邻的两个单元格之间存在一条从较小值指向较大值的有向边。
问题转化成在有向图中寻找最长路径。

深度优先搜索是非常直观的方法。从一个单元格开始进行深度优先搜索，即可找到从该单元格开始的最长递增路径。对每个单元格分别进行深度优先搜索之后，
即可得到矩阵中的最长递增路径的长度。

但是如果使用朴素深度优先搜索，时间复杂度是指数级，会超出时间限制，因此必须加以优化。

朴素深度优先搜索的时间复杂度过高的原因是进行了大量的重复计算，同一个单元格会被访问多次，每次访问都要重新计算。
由于同一个单元格对应的最长递增路径的长度是固定不变的，因此可以使用记忆化的方法进行优化。用矩阵 dp 作为缓存矩阵，
已经计算过的单元格的结果存储到缓存矩阵中。

使用记忆化深度优先搜索，当访问到一个单元格 (i, j) 时，如果 dp[i][j] != 0，说明该单元格的结果已经计算过，则直接从缓存中读取结果，
如果 dp[i][j] = 0，说明该单元格的结果尚未被计算过，则进行搜索，并将计算得到的结果存入缓存中。

遍历完矩阵中的所有单元格之后，即可得到矩阵中的最长递增路径的长度。

# 代码
```php
class LeetCode0329 {

    function longestIncreasingPath($matrix) {
        $res = 0;
        $len1 = count($matrix);
        $len2 = count($matrix[0]);
        $dp = array_fill(0, $len1, array_fill(0, $len2, 0));
        // dp[i][j] 如果为 0，说明没算过，因此每个位置的最小值起码为 1（也就是自己）
        
        for($i = 0; $i < $len1; $i++) {
            for($j = 0; $j < $len2; $j++) {
                $res = max($res, $this->_process($matrix, $i, $j, $dp));
            }
        }
        return $res;
    }
    
    // 从matrix[i][j]出发，可以走上下左右四个方向，走出的最长递增链是多长，返回
    protected function _process($matrix, $i, $j, &$dp) {
        if ($dp[$i][$j] != 0) {
            return $dp[$i][$j];
        }
        // 不越界，往四个方向能走出最长的后续是多少。if语句已提前判断是否越界
        $next = 0;
        if ($i > 0 && $matrix[$i - 1][$j] > $matrix[$i][$j]) {
            $next = $this->_process($matrix, $i - 1, $j, $dp);
        }
        if ($i + 1 < count($matrix) && $matrix[$i + 1][$j] > $matrix[$i][$j]) {
            $next = max($next, $this->_process($matrix, $i + 1, $j, $dp));
        }
        if ($j > 0 && $matrix[$i][$j - 1] > $matrix[$i][$j]) {
            $next = max($next, $this->_process($matrix, $i, $j - 1, $dp));
        }
        if ($j + 1 < count($matrix[0]) && $matrix[$i][$j + 1] > $matrix[$i][$j]) {
            $next = max($next, $this->_process($matrix, $i, $j + 1, $dp));
        }
        $dp[$i][$j] = 1 + $next;
        
        // 自己也是递增链的一份子
        return 1 + $next; 
    }
}
```