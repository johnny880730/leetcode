# 0054. 螺旋矩阵

# 题目
给你一个 m 行 n 列的矩阵 mapRowix ，请按照 **顺时针螺旋顺序** ，返回矩阵中的所有元素。

https://leetcode.cn/problems/spiral-matrix/description/

提示：
- 

# 示例
```
示例 1：

输入：mapRowix = [
    [1,2,3],
    [4,5,6],
    [7,8,9]
]
输出：[1,2,3,6,9,8,7,4,5]
```

```
示例 2：

输入：mapRowix = [
        [1,2,3,4],
        [5,6,7,8],
        [9,10,11,12]
]
输出：[1,2,3,4,8,12,11,10,9,5,6,7]
```

# 解析

本题关键在于设计一种逻辑容易理解、代码易于实现的转圈遍历方式。

这里介绍一种矩阵处理方式，该方式不仅可用这道题，也适合很多其他的面试题，就是矩阵分圈处理。

在矩阵中用左上角的坐标 (pRow, pCol) 和右下角的坐标 (qRow, qCol) 就可以表示一个子矩阵。比如这道题的矩阵，
当 (pRow,pCol) = (0, 0)、(qRow, qCol) = (3, 3)，这个子矩阵表示的就是整个矩阵。此时这个矩阵的外层部分如下：

```
1    2   3    4
5             8
9            12
13  14  15   16
```
如果把这个子矩阵的外层打印出来，就是：1 2 3 4 8 12 16 15 14 13 9 5。

如果令 pRow 和 pCol 都加 1，qRow 和 qCol 都减 1，就往内层缩了一圈。此时表示的子矩阵如下：

```
6     7
10   11 
```

再把这个子矩阵打印出来，结果为：6 7 11 10。

此时若再缩一圈，发现左上角坐标跑到了右下角坐标的右方或下方，整个过程停止。这里需要注意内圈是一条横线或者一条竖线的情况。
已经打印的结果连起来就是要求的打印结果。



# 代码

### php

```php
class LeepCode0054 {
    
    function spiralOrder($matrix) {
        $pRow = 0;
        $pCol = 0;
        $qRow = count($matrix) - 1;
        $qCol = count($matrix[0]) - 1;
        $res = [];
        while ($pRow <= $qRow && $pCol <= $qCol) {
            $this->_printEdge($matrix, $pRow++, $pCol++, $qRow--, $qCol--, $res);
        }
        return $res;
    }
    
    protected function _printEdge($matrix, $pRow, $pCol, $qRow, $qCol, &$res)
    {
        if ($pRow == $qRow) {
            // 子矩阵只有一行的时候，从左往右
            for ($i = $pCol; $i <= $qCol; $i++) {
                $res[] = $matrix[$pRow][$i];
            }
        } else if ($pCol == $qCol) {
            // 子矩阵只有一列的时候，从上往下
            for ($i = $pRow; $i <= $qRow; $i++) {
                $res[] = $matrix[$i][$pCol];
            }
        } else {
            // 一般情况
            $curR = $pRow;
            $curC = $pCol;
            while ($curC != $qCol) {
                $res[] = $matrix[$curR][$curC];
                $curC++;
            }
            while ($curR != $qRow) {
                $res[] = $matrix[$curR][$curC];
                $curR++;
            }
            while ($curC != $pCol) {
                $res[] = $matrix[$curR][$curC];
                $curC--;
            }
            while ($curR != $pRow) {
                $res[] = $matrix[$curR][$curC];
                $curR--;
            }
        }
    }
}
```

### java
```java
class LeetCode0054 {

    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> res = new ArrayList<>();
        int pRow = 0, pCol = 0;
        int qRow = matrix.length - 1, qCol = matrix[0].length - 1;
        while (pRow <= qRow && pCol <= qCol) {
            _printEdge(matrix, pRow++, pCol++, qRow--, qCol--, res);
        }
        return res;
    }

    private void _printEdge(int[][] matrix, int pRow, int pCol, int qRow, int qCol, List<Integer> res) {
        if (pRow == qRow) {
            for (int i = pCol; i <= qCol; i++) {
                res.add(matrix[pRow][i]);
            }
        } else if (pCol == qCol) {
            for (int i = pRow; i <= qRow; i++) {
                res.add(matrix[i][pCol]);
            }
        } else {
            int curCol = pCol, curRow = pRow;
            while (curCol != qCol) {
                res.add(matrix[curRow][curCol]);
                curCol++;
            }
            while (curRow != qRow) {
                res.add(matrix[curRow][curCol]);
                curRow++;
            }
            while (curCol != pCol) {
                res.add(matrix[curRow][curCol]);
                curCol--;
            }
            while (curRow != pRow) {
                res.add(matrix[curRow][curCol]);
                curRow--;
            }
        }
        return res;
    }
}
```

### go
```go
var res []int

func spiralOrder(matrix [][]int) []int {
    res = []int{}
    pRow, pCol := 0, 0
    qRow, qCol := len(matrix) - 1, len(matrix[0]) - 1
    for pRow <= qRow && pCol <= qCol {
        _print(matrix, pRow, pCol, qRow, qCol)
        pRow++
        pCol++
        qRow--
        qCol--
    }
    return res
}

func _print(matrix [][]int, pRow, pCol, qRow, qCol int) {
    if pRow == qRow {
        for i := pCol; i <= qCol; i++ {
            res = append(res, matrix[pRow][i])
        }
    } else if pCol == qCol {
        for i := pRow; i <= qRow; i++ {
            res = append(res, matrix[i][pCol])
        }
    } else {
        curRow := pRow
        curCol := pCol
        for curCol != qCol {
            res = append(res, matrix[curRow][curCol])
            curCol++
        }
        for curRow != qRow {
            res = append(res, matrix[curRow][curCol])
            curRow++
        }
        for curCol != pCol {
            res = append(res, matrix[curRow][curCol])
            curCol--
        }
        for curRow != pRow {
            res = append(res, matrix[curRow][curCol])
            curRow--
        }
    }
}
```
