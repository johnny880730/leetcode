# 517. 超级洗衣机

# 题目
假设有 n 台超级洗衣机放在同一排上。开始的时候，每台洗衣机内可能有一定量的衣服，也可能是空的。

在每一步操作中，你可以选择任意 m (1 <= m <= n) 台洗衣机，与此同时将每台洗衣机的一件衣服送到相邻的一台洗衣机。

给定一个整数数组 machines 代表从左至右每台洗衣机中的衣物数量，请给出能让所有洗衣机中剩下的衣物的数量相等的 最少的操作步数 。
 如果不能使每台洗衣机中衣物的数量相等，则返回 -1 。

https://leetcode.cn/problems/super-washing-machines

提示：
- n == machines.length
- 1 <= n <= 10<sup>4</sup>
- 0 <= machines[i] <= 10<sup>5</sup>

## 示例
```
输入：machines = [1,0,5]
输出：3
解释：
第一步:    1     0 <-- 5    =>    1     1     4
第二步:    1 <-- 1 <-- 4    =>    2     1     3    
第三步:    2     1 <-- 3    =>    2     2     2
```
```
输入：machines = [0,3,0]
输出：2
解释：
第一步:    0 <-- 3     0    =>    1     2     0    
第二步:    1     2 --> 0    =>    1     1     1 
```
```
输入：machines = [0,2,0]
输出：-1
解释：
不可能让所有三个洗衣机同时剩下相同数量的衣物。
```

# 解析
由于最终是要让所有洗衣机衣服相等，因此无解的情况很好分析，如果衣服数量不能整除洗衣机数量的话，则返回 -1，否则必然有解。

由于每次操作都可以选任意台机器进行，不难猜想到最小移动次数为所有机器的「最小运输衣服数量」中的最大值。

令洗衣机共有 n 台，衣服一共 sum 件。对某台洗衣机 arr[i] 来说，以它为分界线，可以获得它的左边衣服数量和它的右边衣服数量，
也就可以得到它的左边衣服数量和最终衣服数量的差值，同理右边也可以：
- 当某一侧欠衣服 a 件，另一侧富余衣服 b 件时，它需要操作 max(a, b) 次才能获得最终结果
- 当某一侧富余衣服 a 件，另一侧也富余衣服 b 件时，它需要  max(a, b) 次才能获得最终结果
- 当某一侧欠衣服 a 件，另一侧也欠衣服 b 件时，它需要 a + b 次才能获得最终结果，因为左右都靠 arr[i] 来搬运，而一次只能搬一件，所以结果为 a + b。

算出每台洗衣机需要搬的次数，结果就是其中的最大值

# 代码
```php
class LeetCode0517 {

    public function findMinMoves($arr) { 
        if (empty($arr)) {
            return 0;
        }
        $size = count($arr);
        $sum = array_sum($arr);
        if ($sum % $size != 0) {
            return -1;
        }
        $avg = $sum / $size;
        $leftSum = 0;
        $res = 0;
        for ($i = 0; $i < $size; $i++) {
            // i 号机器是中间的，左边是 (0 ~ i-1)，右边是 (i+1 ~ N-1)
            $leftRest = $leftSum - $i * $avg;
            $rightRest = ($sum - $leftSum - $arr[$i]) - ($size - $i - 1) * $avg;
            if ($leftRest < 0 && $rightRest < 0) {
                $res = max($res, abs($leftRest) + abs($rightRest));
            } else {
                $res = max($res, max(abs($leftRest), abs($rightRest)));
            }
            $leftSum += $arr[$i];
        }
        return $res;
    }
}
```