# 724. 寻找数组的中心下标

# 题目
给你一个整数数组 nums ，请计算数组的 中心下标 。

数组 中心下标 是数组的一个下标，其左侧所有元素相加的和等于右侧所有元素相加的和。

如果中心下标位于数组最左端，那么左侧数之和视为 0 ，因为在下标的左侧不存在元素。这一点对于中心下标位于数组最右端同样适用。

如果数组有多个中心下标，应该返回 最靠近左边 的那一个。如果数组不存在中心下标，返回 -1 。

https://leetcode.cn/problems/find-pivot-index

提示：
1 <= nums.length <= 10^4
-1000 <= nums[i] <= 1000



# 示例
```
输入：nums = [1, 7, 3, 6, 5, 6]
输出：3
解释：
中心下标是 3 。
左侧数之和 sum = nums[0] + nums[1] + nums[2] = 1 + 7 + 3 = 11 ，
右侧数之和 sum = nums[4] + nums[5] = 5 + 6 = 11 ，二者相等。
```
```
输入：nums = [1, 2, 3]
输出：-1
解释：
数组中不存在满足此条件的中心下标。
```
```
输入：nums = [2, 1, -1]
输出：0
解释：
中心下标是 0 。
左侧数之和 sum = 0 ，（下标 0 左侧不存在元素），
右侧数之和 sum = nums[1] + nums[2] = 1 + -1 = 0 。
```

# 解析
做两个数组
- 数组 A 为前缀和，计算nums[i]左侧所有元素相加之和
- 数组 B 为后缀和，计算nums[i]右侧所有元素相加之和

如果 A[i] == B[i] return i;


# 代码

### php
```php
class Leetcode0724 {

    function pivotIndex($nums) {
        $len = count($nums);
        $A = array_fill(0, $len, 0);
        $B = array_fill(0, $len, 0);
        for ($i = 1; $i < $len; $i++) {
            $A[$i] = $A[$i - 1] + $nums[$i - 1];
        }
        for ($i = $len - 2; $i >= 0; $i--) {
            $B[$i] = $B[$i + 1] + $nums[$i + 1];
        }
        for ($i = 0; $i < $len; $i++) {
            if ($A[$i] == $B[$i]) {
                return $i;
            }
        }
        return -1;
    }
    
}
```

### go
```go
func pivotIndex(nums []int) int {
    size := len(nums)
    A, B := make([]int, size), make([]int, size)
    for i := 1; i < size; i++ {
        A[i] = A[i - 1] + nums[i - 1]
    }
    for i := size - 2; i >= 0; i-- {
        B[i] = B[i + 1] + nums[i + 1]
    }
    for i := 0; i < size; i++ {
        if A[i] == B[i] {
            return i
        }
    }
    return -1
}
```