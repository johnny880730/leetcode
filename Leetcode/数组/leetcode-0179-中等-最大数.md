# 179. 最大数

# 题目
给定一组非负整数 nums，重新排列每个数的顺序（每个数不可拆分）使之组成一个最大的整数。

注意：输出结果可能非常大，所以你需要返回一个字符串而不是整数。

https://leetcode.cn/problems/largest-number

提示：
- 

# 示例
```
输入：nums = [10,2]
输出："210"
```
```
输入：nums = [3,30,34,5,9]
输出："9534330"
```

# 解析

## 贪心
比较策略：两个字符串 a 和 b，如果 a.b >= b.a 就把 a 放在前，否则就把 b 放在前


# 代码

### php
```php

class LeetCode0179 {

    function largestNumber($nums) {
        usort($nums, function($a, $b) {
            $aa = strval($a);
            $bb = strval($b);
            return ($aa . $bb) <= ($bb . $aa);
        });
        $res = join('', $nums);
        if ($res == 0) {
            return '0';
        } 
        return $res;
    }
}

```

### go
```go
func largestNumber(nums []int) string {
    sort.Slice(nums, func(i, j int) bool {
        s1, s2 := strconv.Itoa(nums[i]), strconv.Itoa(nums[j])
        tmp1, tmp2 := s1 + s2, s2 + s1
        return tmp1 > tmp2
    })
    if nums[0] == 0 {
        return "0"
    }
    res := ""
    for i := 0; i < len(nums); i++ {
        res += strconv.Itoa(nums[i])
    }

    return res
}
```