# 0066. 加一

# 题目

给定一个由整数组成的非空数组所表示的非负整数，在该数的基础上加一。最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。

你可以假设除了整数 0 之外，这个整数不会以零开头。

https://leetcode.cn/problems/plus-one/description/

提示：
- 1 <= digits.length <= 100
- 0 <= digits[i] <= 9

# 示例
```
输入: [1,2,3]
输出: [1,2,4]
解释: 输入数组表示数字 123。
```

```
输入: [4,3,2,1]
输出: [4,3,2,2]
解释: 输入数组表示数字 4321。
```

# 解析

这道题把整个数组看成了一个整数，然后个位数加 1。问题的实质是利用数组模拟加法运算。如果个位数不为 9 的话，
直接把个位数加 1 就好。如果个位数为 9 的话，还要考虑进位。

根据题意加一，没错就是加一这很重要，因为它是只加一的所以有可能的情况就只有两种：
- 除 9 之外的数字加一；
- 数字 9。

加一得十进一位，个位数为 0， 加法运算如果不出现进位就运算结束了，且进位只会是一。

所以只需要判断有没有进位并模拟出它的进位方式，如十位数加 1 个位数置为 0，如此循环直到判断没有再进位就退出循环返回结果。

然后还有一些特殊情况就是当出现 99、999 之类的数字时，循环到最后也需要进位，出现这种情况时需要手动将它进一位。



# 代码

### php
```php
class Solution0066 {

    function plusOne($digits) {
        $n = count($digits);
        for ($i = $n - 1; $i >= 0; $i--) {
            if ($digits[$i] < 9) {
                $digits[$i]++;
                return $digits;
            }
            $digits[$i] = 0;
        }
        $res = array_fill(0, $n + 1, 0);
        $res[0] = 1;
        return $res;
    }
}
```

### go
```go
func plusOne(digits []int) []int {
    length := len(digits)
    for i := length - 1; i >= 0; i-- {
        if digits[i] < 9 {
            digits[i]++
            return digits
        }
        digits[i] = 0
    }
    return append([]int{1}, digits...)
}
```

### java
```java
class LeetCode0066 {
    public int[] plusOne(int[] digits) {
        for (int i = digits.length - 1; i >= 0; i--) {
            if (digits[i] < 9) {
                digits[i]++;
                return digits;
            }
            digits[i] = 0;
        }
        digits = new int[digits.length + 1];
        digits[0] = 1;
        return digits;
    }
}
```
