# 18. 四数之和

# 题目
给你一个由 n 个整数组成的数组 nums ，和一个目标值 target 。

请你找出并返回满足下述全部条件且不重复的四元组 [nums[a], nums[b], nums[c], nums[d]] ：
- 0 <= a, b, c, d < n
- a、b、c 和 d 互不相同
- nums[a] + nums[b] + nums[c] + nums[d] == target
- 你可以按 任意顺序 返回答案 。

https://leetcode.cn/problems/4sum/

提示：
- 1 <= nums.length <= 200
- -10^9 <= nums[i] <= 10^9
- -10^9 <= target <= 10^9

# 示例：
```
输入：nums = [1,0,-1,0,-2,2], target = 0
输出：[[-2,-1,1,2],[-2,0,0,2],[-1,0,0,1]]
```

```
输入：nums = [2,2,2,2,2], target = 8
输出：[[2,2,2,2]]
```


# 解析
四数之和与三数之和是一个思路，都是使用双指针法，基本解法就是在三数之和的解法基础上再套一层 for 循环。

但有些细节需要注意。例如，在剪枝的时候不需要判断 nums[k] > target 就返回，三数之和中，当 nums[i] > 0 就返回，
因为 0 已经是确定的数了，四数之和的 target 是任意值。

三数之和的双指针解法是一层 for 循环遍历，得到的 nums[i] 为确定值，然后循环内有 left 和 right 下标作为双指针，找到结果。

四数之和的双指针解法是两层 for 循环，得到的 nums[k] + nums[i] 为确定值，依然循环 left 和 right 下标作为双指针，找到结果。

三数之和的时间复杂度为 O(n²)，四数之和的时间复杂度为 O(n³)。

同理，五数之和、六数之和等都采用这种解法。

# 代码

### php

```php
class LeetCode018 {

    function fourSum($nums, $target) {
        sort($nums);
        $res = [];
        $len = count($nums);
        for ($k = 0; $k < $len; $k++) {
            // 去重
            if ($k > 0 && $nums[$k] == $nums[$k - 1]) {
                continue;
            }
            for ($i = $k + 1; $i < $len; $i++) {
                // 正确去重方法
                if ($i > $k + 1 && $nums[$i] == $nums[$i - 1]) {
                    continue;
                }
                $left = $i + 1;
                $right = $len - 1;
                while ($right > $left) {
                    if ($nums[$k] + $nums[$i] + $nums[$left] + $nums[$right] > $target) {
                        $right--;
                    } else if ($nums[$k] + $nums[$i] + $nums[$left] + $nums[$right] < $target) {
                        $left++;
                    } else {
                        $res[] = [$nums[$k], $nums[$i], $nums[$left], $nums[$right]];
                        // 去重逻辑应该放在找到一个四元组之后
                        while ($right > $left && $nums[$right] == $nums[$right - 1]) $right--;
                        while ($right > $left && $nums[$left] == $nums[$left + 1]) $left++;

                        // 找到答案时，双指针同时收缩
                        $right--;
                        $left++;
                    }
                }

            }
        }
        return $res;
    }

}
```

### go
```go
func fourSum(nums []int, target int) [][]int {
	sort.Ints(nums)
	length := len(nums)
	if length < 4 {
		return nil
	}
	res := make([][]int, 0)
	for k := 0; k < length; k++ {
		if k > 0 && nums[k] == nums[k - 1] {
			continue
		}
		for i := k + 1; i < length; i++ {
			if i > k + 1 && nums[i] == nums[i - 1] {
				continue
			}
			left, right := i + 1, length - 1
			for left < right {
				if nums[k] + nums[i] + nums[left] + nums[right] > target {
					right--
				} else if nums[k] + nums[i] + nums[left] + nums[right] < target {
					left++
				} else {
					res = append(res, []int{nums[k], nums[i], nums[left], nums[right]})
					for left < right && nums[left] == nums[left + 1] {
						left++
					}
					for left < right && nums[right] == nums[right - 1] {
						right--
					}
					left++
					right--
				}
			}
		}
	}
	return res
}
```
