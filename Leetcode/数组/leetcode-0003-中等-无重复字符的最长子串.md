# 0003. 无重复字符的最长子串

# 题目
给定一个字符串，请你找出其中不含有重复字符的 **最长子串** 的长度。

提示：
- 0 <= s.length <= 50000
- s 由英文字母、数字、符号和空格组成

https://leetcode.cn/problems/longest-substring-without-repeating-characters/description/

# 示例
```
示例 1:

输入: "abcabcbb"
输出: 3
解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
```
```
示例 2:

输入: "bbbbb"
输出: 1
解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
```
```
示例 3:
输入: "pwwkew"
输出: 3
解释: 因为无重复字符的最长子串是"wke"，所以其长度为 3。
    请注意，你的答案必须是 子串 的长度，"pwke"是一个子序列，不是子串。
```

# 解析

## ① 滑动窗口 + 哈希表
其实就是一个队列，比如例题中的 abcabcbb，进入这个队列（窗口）为 abc 满足题目要求，当再进入 a，队列变成了 abca，这时候不满足要求。所以要移动这个队列

如何移动？只要把队列的左边的元素移出就行了，直到满足题目要求！

一直维持这样的队列，找出队列出现最长的长度时候，求出解！


## ② 动态规划
定义 dp[i]：子串必须以 i 位置结尾的情况下，最长的无重复字符的子串是多长

从左往右遍历，遍历当前 dp[i] 的时候，前面的 dp[i - 1]、dp[i - 2] 等等都已经求过了，它们可以加速求当前的 dp[i]。

如果 str[i] 的字符是 "a"，必须以 i 位置字符结尾的子串它从 i 开始往左推能推多远看哪些因素？
- 上次字符 "a" 出现的位置。如果上次字符 a 出现的位置是 k，那么这个子串肯定是推不到位置 k 的。这个位置记为 p1。
  通过一张 map 统计每个字符最近出现的位置来实现。
- 此时已经知道 dp[i - 1] 的值，而 dp[i - 1] 表示必须以 str[i - 1] 结尾的情况下往左推的距离，那么现在以 str[i] 开始往左推肯定突破不了 dp[i- 1] 的边界位置。这个位置记为 p2。
- p1 和 p2 两个位置哪个距离 i 更近哪个就是 str[i] 能推到的距离。


# 代码

### php
```php
class LeetCode0003 {

    // 滑动窗口
    function lengthOfLongestSubstring($s) {
        $len = strlen($s);
        $window = [];
        $left = $right = 0;
        $res = 0;
        while ($right < $len) {
            $c = $s[$right];
            $window[$c]++;
            while ($window[$c] > 1) {
                $d = $s[$left];
                $left++;
                $window[$d]--;
            }
            $res = max($res, $right - $left + 1);

            $right++;
        }
        return $res;
    }
    
    // dp
    function lengthOfLongestSubstring2($s) {
        if (!$s) {
            return 0;
        }
        $len = strlen($s);
        $dp = array_fill(0, $len, 0);
        $dp[0] = 1;
        $map = [];
        $map[ord($s[0])] = 0;
        $res = 1;
        for ($i = 1; $i < $len; $i++) {
            $k = ord($s[$i]);
            $old = array_key_exists($k, $map) ? $map[$k] : -1;
            $dp[$i] = min($i - $old, $dp[$i - 1] + 1);
            $res = max($res, $dp[$i]);
            $map[$k] = $i;
        }
        return $res;
    }

}
```

### go
```go
// 滑动窗口
func lengthOfLongestSubstring(s string) int {
    length := len(s);
    str := strings.Split(s, "")
    freq := make(map[string]int)
    left, right, res := 0, 0, 0
    for right < length {
        c1 := str[right]
        freq[c1]++
        for freq[c1] > 1 {
            c2 := str[left]
            left++
            freq[c2]--
        }
        res = Max(res, right - left + 1)

        right++
    }
    return res
}

// DP
func lengthOfLongestSubstring2(s string) int {
    if s == "" {
        return 0
    }
    length := len(s)
    window := make(map[byte]int)
    left, right := 0, -1
    res := 0
    for left < length {
        if right + 1 < length && window[s[right + 1]] == 0 {
            right++
            window[s[right]]++
        } else {
            window[s[left]]--
            left++
        }
        res = Max(res, right - left + 1)
    }
    return res
}

// Max ：支持 int 类型的 max 函数
func Max(num1 int, num2 int) int {
    if num1 >= num2 {
        return num1
    }
    return num2
}

```

### java
```java
class LeetCode0003 {

    public int lengthOfLongestSubstring(String s) {
        HashMap<Character, Integer> map = new HashMap<>();
        int length = s.length();
        int left = 0, right = 0, res = 0;
        while (right < length) {
            Character c1 = s.charAt(right);
            if (!map.containsKey(c1)) {
                map.put(c1, 1);
            } else {
                map.put(c1, map.get(c1) + 1);
            }
            
            while (map.get(c1) > 1) {
                Character c2 = s.charAt(left);
                left++;
                map.put(c2, map.get(c2) - 1);
            }
            res = Math.max(res, right - left + 1);

            right++;
        }
        
        return res;
    }
}
```
