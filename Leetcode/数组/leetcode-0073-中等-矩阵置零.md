# 0073. 矩阵置零

# 题目
给定一个 m x n 的矩阵，如果一个元素为 0 ，则将其所在行和列的所有元素都设为 0 。请使用 原地 算法。

https://leetcode.cn/problems/set-matrix-zeroes/description/

提示：
- m == matrix.length
- n == matrix[0].length
- 1 <= m, n <= 200
-  -2^31 <= matrix[i][j] <= 2^31 - 1

进阶：
- 一个直观的解决方案是使用 O(m*n) 的额外空间，但这并不是一个好的解决方案。
- 一个简单的改进方案是使用 O(m+n) 的额外空间，但这仍然不是最好的解决方案。
- 你能想出一个仅使用常量空间的解决方案吗？


## 示例
```
输入：matrix = [[1,1,1],[1,0,1],[1,1,1]]
输出：[[1,0,1],[0,0,0],[1,0,1]]
```

```
输入：matrix = [[0,1,2,0],[3,4,5,2],[1,3,1,5]]
输出：[[0,0,0,0],[0,4,5,0],[0,3,1,0]]
```

# 解析

## 标记数组

用一个变量 row0Zero 标记第 0 行是否有 0，默认 false。只遍历第一行，如果有 0 就为 true。

再用个变量 col0Zero 标记第 0 列是否有 0，默认 false。只遍历第一列，如果有 0 就为 true。

遍历从第 1 行第 1 列的值开始。假设 matrix[3][7] = 0（即第 3 行第 7 列的值为 0），那么将第 0 行第 7 列的值 和 第 3 行第 0 列的值设为 0。
代表的意思是：
- 在第 0 行里，如果第 j 列是 0，那么整个 j 列都为 0
- 在第 0 列里，如果第 i 行是 0，那么整个 i 行要变 0

重新从第 1 行第 1 列的值开始，如果 matrix[i][0] == 0 或者 matrix[0][j] == 0，这个单元格就要变 0。

回头看第 0 行，如果 rol0Zero == true 则将第 0 行全部写 0。同理操作第 0 列。

---

举例，如下矩阵 ：
```
1   3   2   7   0   3
5   6   3   2   9   0
6   1   2   7   6   5
7   4   0   3   9   2
9   5   4   3   6   1
2   7   6   2   9   5
```

因为第 0 行存在 0 所以 row0Zero = true；第 0 列不存在 0 所以 col0Zero = false。从 matrix[1][1] 开始遍历。
- 可见 matrix[1][5] = 0，因此将 matrix[0][5] 和 matrix[1][0] 设置为 0，意思就是代表第 5 列和第 1 行最后都要变成 0。
- 还有 matrix[3][2] = 0，因此 matrix[0][2] 和 matrix[3][0] 设置为 0，意思就是代表第 2 列和第 3 行最后都要变成 0。

重新从 matrix[1][1] 开始遍历，由于有 matrix[0][5] = matrix[0][2] = 0，所以第 2 列和第 5 列都是 0。又有 
matrix[1][0] = matrix[3][0] = 0，所以第 1 行和第 3 行都是 0。

回头看 row0Zero，由于是 true，所以第 0 行全改写为 0。但 col0Zero 为 false，不用操作。流程结束，结果为 

```
0   0   0   0   0   0
0   0   0   0   0   0
6   1   0   7   0   0
0   0   0   0   0   0
9   5   0   3   0   0
2   7   0   2   0   0
```



# 代码

### php
```php
class LeetCode0073 {

    function setZeroes(&$matrix) {
        $row0Zero = $col0Zero = false;
        // 看第 0 行是否有 0
        for ($i = 0; $i < count($matrix[0]); $i++) {
            if ($matrix[0][$i] == 0) {
                $row0Zero = true;
                break;
            }
        }
        // 看第 0 列是否有 0
        for ($j = 0; $j < count($matrix); $j++) {
            if ($matrix[$j][0] == 0) {
                $col0Zero = true;
                break;
            }
        }
        // 从 [1][1] 开始遍历，如果有 0，将 [i][0] 和 [0][j] 位置写 0
        for ($i = 1; $i < count($matrix); $i++) {
            for ($j = 1; $j < count($matrix[0]); $j++) {
                if ($matrix[$i][$j] == 0) {
                    $matrix[$i][0] = 0;
                    $matrix[0][$j] = 0;
                }
            }
        }
        // 重新从 [1][1] 开始遍历，如果 [i][0] == 0 或者 [0][j] == 0，单元格就是 0
        for ($i = 1; $i < count($matrix); $i++) {
            for ($j = 1; $j < count($matrix[0]); $j++) {
                if ($matrix[$i][0] == 0 || $matrix[0][$j] == 0) {
                    $matrix[$i][$j] = 0;
                }
            }
        }
        // 回看第 0 行，如果有 0 则 该行全部是 0
        if ($row0Zero) {
            for ($i = 0; $i < count($matrix[0]); $i++) {
                $matrix[0][$i] = 0;
            }
        }
        // 回看第 0 列，如果有 0 则 该列全部是 0
        if ($col0Zero) {
            for ($i = 0; $i < count($matrix); $i++) {
                $matrix[$i][0] = 0;
            }
        }
    }
}
```

### go 
```go
func setZeroes(matrix [][]int)  {
    row0Zero, col0Zero := false, false
    rowNum, colNum := len(matrix), len(matrix[0])
    for col := 0; col < colNum; col++ {
        if matrix[0][col] == 0 {
            row0Zero = true
            break
        }
    }
    for row := 0; row < rowNum; row++ {
        if matrix[row][0] == 0 {
            col0Zero = true
            break
        }
    }
    for i := 1; i < rowNum; i++ {
        for j := 1; j < colNum; j++ {
            if matrix[i][j] == 0 {
                matrix[i][0] = 0
                matrix[0][j] = 0
            }
        }
    }
    for i := 1; i < rowNum; i++ {
        for j := 1; j < colNum; j++ {
            if matrix[i][0] == 0 || matrix[0][j] == 0 {
                matrix[i][j] = 0
            }
        }
    }
    if row0Zero == true {
        for i := 0; i < colNum; i++ {
            matrix[0][i] = 0
        }
    }
    if col0Zero == true {
        for i := 0; i < rowNum; i++ {
            matrix[i][0] = 0
        }
    }
}
```

### java
```java
class LeetCode0073 {

    public void setZeroes(int[][] matrix) {
        boolean row0Zero = false, col0Zero = false;
        for (int i = 0; i < matrix[0].length; i++) {
            if (matrix[0][i] == 0) {
                row0Zero = true;
                break;
            }
        }
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i][0] == 0) {
                col0Zero = true;
                break;
            }
        }
        for (int i = 1; i < matrix.length; i++) {
            for (int j = 1; j < matrix[0].length; j++) {
                if (matrix[i][j] == 0) {
                    matrix[i][0] = 0;
                    matrix[0][j] = 0;
                }
            }
        }
        for (int i = 1; i < matrix.length; i++) {
            for (int j = 1; j < matrix[0].length; j++) {
                if (matrix[i][0] == 0 || matrix[0][j] == 0) {
                    matrix[i][j] = 0;
                }
            }
        }
        if (row0Zero) {
            for (int i = 0; i < matrix[0].length; i++) {
                matrix[0][i] = 0;
            }
        }
        if (col0Zero) {
            for (int i = 0; i < matrix.length; i++) {
                matrix[i][0] = 0;
            }
        }
    }
}
```
