# 36. 有效的数独

# 题目
请你判断一个 9 x 9 的数独是否有效。只需要 根据以下规则 ，验证已经填入的数字是否有效即可。
- 数字 1-9 在每一行只能出现一次。
- 数字 1-9 在每一列只能出现一次。
- 数字 1-9 在每一个以粗实线分隔的 3x3 宫内只能出现一次。（请参考示例图）

注意：
- 一个有效的数独（部分已被填充）不一定是可解的。
- 只需要根据以上规则，验证已经填入的数字是否有效即可。
- 空白格用 '.' 表示。

https://leetcode.cn/problems/valid-sudoku/

提示：
- board.length == 9
- board[i].length == 9
- board[i][j] 是一位数字（1-9）或者 '.'

## 示例
```
输入：board = 
[["5","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
输出：true
```

```
输入：board = 
[["8","3",".",".","7",".",".",".","."]
,["6",".",".","1","9","5",".",".","."]
,[".","9","8",".",".",".",".","6","."]
,["8",".",".",".","6",".",".",".","3"]
,["4",".",".","8",".","3",".",".","1"]
,["7",".",".",".","2",".",".",".","6"]
,[".","6",".",".",".",".","2","8","."]
,[".",".",".","4","1","9",".",".","5"]
,[".",".",".",".","8",".",".","7","9"]]
输出：false
解释：除了第一行的第一个数字从 5 改为 8 以外，空格内其他数字均与 示例1 相同。 但由于位于左上角的 3x3 宫内有两个 8 存在, 因此这个数独是无效的。
```

# 解析
事先准备：
1. 准备 rows[i][j]，表示第 i 行的数字 j 是否已经出现，默认 false。
2. 准备 cols[i][j]，表示第 i 列的数字 j 是否已经出现，默认 false。
3. 准备 boxes[i][j]，表示第 i 个格子的数字 j 是否已经出现，默认 false。

循环数独盘，如果格子里不是 '.' 符号，判断在 rows、cols、boxes 的对应数字是否已经出现，已经出现的话就是 false；整个流程没问题就是 true。



# 代码

### php
```php
class LeetCode0036 {

    function isValidSudoku($board) {
        $rows  = array_fill(0, 9, array_fill(0, 10, false));
        $cols  = array_fill(0, 9, array_fill(0, 10, false));
        $boxes = array_fill(0, 9, array_fill(0, 10, false));
        for ($i = 0; $i < 9; $i++) {
            for ($j = 0; $j < 9; $j++) {
                // 获取 点[i, j] 属于第几个桶
                $box = 3 * floor($i / 3) + floor( $j / 3);
                if ($board[$i][$j] != '.') {
                    $num = intval($board[$i][$j]);
                    // 如果这个数字在 第i行 或者 第j列 或者 第box个桶已经出现过，返回false
                    if ($rows[$i][$num] || $cols[$j][$num] || $boxes[$box][$num]) {
                        return false;
                    }
                    $rows[$i][$num] = true;
                    $cols[$j][$num] = true;
                    $boxes[$box][$num] = true;
                }
            }
        }
        return true;
    }
}
```

### go
```go
func isValidSudoku(board [][]byte) bool {
    rows := [9][10]bool{}
    cols := [9][10]bool{}
    boxes := [9][10]bool{}
    for i := 0; i < 9; i++ {
        for j := 0; j < 9; j++ {
            if board[i][j] != '.' {
                box := 3 * (i / 3) + j / 3
                num, _ := strconv.Atoi(string(board[i][j]))
                if rows[i][num] || cols[j][num] || boxes[box][num] {
                    return false
                }
                rows[i][num] = true
                cols[j][num] = true
                boxes[box][num] = true
            }
        }
    }
    return true
}
```

### java
```java
class LeetCode0036 {

    public boolean isValidSudoku(char[][] board) {
        boolean[][] row = new boolean[9][10];
        boolean[][] col = new boolean[9][10];
        boolean[][] box = new boolean[9][10];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (board[i][j] == '.') {
                    continue;
                }
                int boxNum = 3 * (i / 3) + j / 3;
                int num = board[i][j] - '1';
                if (row[i][num] || col[j][num] || box[boxNum][num]) {
                    return false;
                }
                row[i][num] =  true;
                col[j][num] =  true;
                box[boxNum][num] =  true;
            }
        }
        return true;
    }
}
```
