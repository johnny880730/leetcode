# 0042. 接雨水

# 题目
给定 n 个非负整数表示每个宽度为 1 的柱子的高度图，计算按此排列的柱子，下雨之后能接多少雨水。

![题图](./images/leetcode-0042-题图.png)

https://leetcode.cn/problems/trapping-rain-water/description/


提示：
- n == height.length
- 1 <= n <= 2 * 10^4
- 0 <= height[i] <= 10^5

# 示例：
```
示例 1：

输入：height = [0,1,0,2,1,0,1,3,2,1,2,1]
输出：6
解释：上面是由数组 [0,1,0,2,1,0,1,3,2,1,2,1] 表示的高度图，在这种情况下，可以接 6 个单位的雨水（蓝色部分表示雨水）。
```

```
示例 2：

输入：height = [4,2,0,3,2,5]
输出：9
```

# 解析

对于下标 i，下雨后水能到达的最大高度等于下标 i 两边的最大高度的最小值，下标 i 处能接的雨水量等于下标 i 处的水能到达的最大高度减去 height[i]。

![](./images/leetcode-0042-img2.png)

如果按照列来计算的话，宽度一定是 1 了，再把每一列的雨水的高度求出来就可以了。

要从头遍历一遍所有的列，然后求出每一列雨水的体积，相加之后就是总雨水的体积了。

首先从头遍历所有的列，并且要注意第一个柱子和最后一个柱子不接雨水

## ① 动态规划

当前位置的雨水面积 = ( min(左边柱子最高, 右边柱子最高) - 当前柱子高度) * 单位宽度（这里是 1）。

为了得到两边的最高高度，使用了双指针来遍历，每到一个柱子都向两边遍历一遍。这其实是有重复计算的。
将每个位置的左边最高高度记录到一个数组 maxLeft，将右边最高高度记录到另一个数组 maxRight，避免了重复计算，这时就用到了动态规划。

当前位置的左边最高高度是前一个位置的左边最高高度和本高度比较后的最大值。
- 从左向右遍历：maxLeft[i] = max( height[i], maxLeft[i - 1] )
- 从右向左遍历：maxRight[i] = max( height[i], marRight[i + 1] )

---

## ② 单调栈

单调栈就是保持栈内元素有序，需要自己维持顺序，没有现成的容器可以用。

通常是一维数组，要寻找任一个元素的右边或者左边第一个比自己大或者小的元素的位置，此时就要想到可以用单调栈了。

而接雨水这道题目，正需要寻找一个元素，右边最大元素以及左边最大元素，来计算雨水面积准备工作

那么本题使用单调栈有如下几个问题：

1、首先要清楚单调栈是按照行方向来计算雨水

2、单调栈内元素的顺序从大到小还是从小到大呢？

从栈头（元素从栈头弹出）到栈底的顺序应该是从小到大的顺序。

因为一旦发现添加的柱子高度大于栈头元素了，此时就出现凹槽了，栈头元素就是凹槽底部的柱子，栈头第二个元素就是凹槽左边的柱子，
而添加的元素就是凹槽右边的柱子。

![](./images/leetcode-0042-img3.png)

3、遇到相同高度的柱子怎么办。

遇到相同的元素，更新栈内下标，就是将栈里元素（旧下标）弹出，将新元素（新下标）加入栈中。

例如 5 5 1 3 这种情况。如果添加第二个 5 的时候就应该将第一个 5 的下标弹出，把第二个 5 添加到栈中。

因为要求宽度的时候，如果遇到相同高度的柱子，需要使用最右边的柱子来计算宽度。

![](./images/leetcode-0042-img4.png)

4、栈里要保存什么数值

使用单调栈，也是通过 长 * 宽 来计算雨水面积的。

长就是通过柱子的高度来计算，宽是通过柱子之间的下标来计算，

那么栈里有没有必要存一个pair<int, int>类型的元素，保存柱子的高度和下标呢。

其实不用，栈里就存放下标就行，想要知道对应的高度，通过 height[stack.top()] 就知道弹出的下标对应的高度了。

#### 单调栈处理逻辑
以下操作过程和 《739. 每日温度》 也是一样的，建议先做 《739. 每日温度》

以下逻辑主要就是三种情况
- 情况一：当前遍历的元素（柱子）高度小于栈顶元素的高度 height[i] < height[st.top()]
- 情况二：当前遍历的元素（柱子）高度等于栈顶元素的高度 height[i] == height[st.top()]
- 情况三：当前遍历的元素（柱子）高度大于栈顶元素的高度 height[i] > height[st.top()]

先将下标 0 的柱子加入到栈中，st.push(0)。 栈中存放遍历过的元素，所以先将下标 0 加进来。

然后开始从下标 1 开始遍历所有的柱子。如果当前遍历的元素（柱子）高度小于栈顶元素的高度，就把这个元素加入栈中，
因为栈里本来就要保持从小到大的顺序（从栈头到栈底）。

如果当前遍历的元素（柱子）高度等于栈顶元素的高度，要跟更新栈顶元素，因为遇到相相同高度的柱子，需要使用最右边的柱子来计算宽度。

如果当前遍历的元素（柱子）高度大于栈顶元素的高度，此时就出现凹槽了，如图所示：

![](./images/leetcode-0042-img3.png)

取栈顶元素，将栈顶元素弹出，这个就是凹槽的底部，也就是中间位置，下标记为 mid，对应的高度为 height[mid]（就是图中的高度 1）。

此时的栈顶元素 st.top()，就是凹槽的左边位置，下标为 st.top()，对应的高度为 height[st.top()]（就是图中的高度 2）。

当前遍历的元素 i，就是凹槽右边的位置，下标为 i，对应的高度为 height[i]（就是图中的高度 3）。

此时应该可以发现其实就是栈顶和栈顶的下一个元素以及要入栈的元素，三个元素来接水。

那么雨水高度是 min(左边高度, 右边高度) - 底部高度，代码为：int h = min(height[st.top()], height[i]) - height[mid];

雨水的宽度是 凹槽右边的下标 - 凹槽左边的下标 - 1（因为只求中间宽度），代码为：int w = i - st.top() - 1 ;

当前凹槽雨水的体积就是：h * w。


# 代码
```php

class LeetCode0042 {

    function trap($height) {
        $len = count($height);
        $maxLeft = $maxRight = array_fill(0, $len, 0);
        // 记录每个柱子左边柱子的最大高度
        $maxLeft[0] = $height[0];
        for ($i = 1; $i < $len; $i++) {
            $maxLeft[$i] = max($height[$i], $maxLeft[$i - 1]);
        }
        // 记录每个柱子右边柱子的最大高度
        $maxRight[$len - 1] = $height[$len - 1];
        for ($i = $len - 2; $i >= 0; $i--) {
            $maxRight[$i] = max($height[$i], $maxRight[$i + 1]);
        }
        // 求和
        $res = 0;
        for ($i = 1; $i < $len - 1; $i++) {
            $h = min($maxLeft[$i], $maxRight[$i]) - $height[$i];
            if ($h > 0) {
                $res += $h;
            }
        }

        return $res;
    }
    
    // 不用辅助数组
    function trap2($height) {
        if (!$height) {
            return 0; 
        }
        $len = count($height);
        $left = 1;
        $leftMax = $height[0];
        $right = $len - 2;
        $rightMax = $height[$len - 1];
        $res = 0;
        while ($left <= $right) {
            if ($leftMax <= $rightMax) {
                $res += max(0, $leftMax - $height[$left]);
                $leftMax = max($leftMax, $height[$left++]);
            } else {
                $res += max(0, $rightMax - $height[$right]);
                $rightMax = max($rightMax, $height[$right--]);
            }
        }
        return $res;
    }

}
```

### go
```go
func trap(height []int) int {
    length := len(height)
    maxLeft := make([]int, length)
    maxRight := make([]int, length)
    maxLeft[0] = height[0]
    for i := 1; i < length; i++ {
        if height[i] > maxLeft[i - 1] {
            maxLeft[i] = height[i]
        } else {
            maxLeft[i] = maxLeft[i - 1]
        }
    }
    maxRight[length - 1] = height[length - 1]
    for i := length - 2; i >= 0; i-- {
        if height[i] > maxRight[i + 1] {
            maxRight[i] = height[i]
        } else {
            maxRight[i] = maxRight[i + 1]
        }
    }
    res := 0
    for i := 1; i < length - 1; i++ {
        min := maxRight[i]
        if maxLeft[i] < maxRight[i] {
            min = maxLeft[i]
        }
        h := min - height[i]
        if h > 0 {
            res += h
        }
    }
    return res
}

// 单调栈
func trap(height []int) int {
    size := len(height)
    if size <= 2 {
        return 0
    }
    res := 0
    st := make([]int, 0)
    st = append(st, 0)
    for i := 1; i < size; i++ {
        if height[i] < height[st[len(st) - 1]] {
            st = append(st, i)
        } else if height[i] == height[st[len(st) - 1]] {
            st = st[: len(st) - 1]
            st = append(st, i)
        } else {
            for len(st) > 0 && height[i] > height[st[len(st) - 1]] {
                mid := st[len(st) - 1]
                st = st[: len(st) - 1]
                if len(st) > 0 {
                    h := min(height[i], height[st[len(st) - 1]]) - height[mid]
                    w := i - st[len(st) - 1] - 1
                    res += h * w
                }
            }
            st = append(st, i)
        }
    }
    return res
}
    
func min(a, b int ) int {
    if a < b {
        return a
    }
    return b
}
```

### java
```java
class LeetCode0042-1 {

    public int trap(int[] height) {
        int[] maxLeft = new int[height.length], maxRight = new int[height.length];

        maxLeft[0] = height[0];
        for (int i = 1; i < height.length; i++) {
            maxLeft[i] = Math.max(maxLeft[i - 1], height[i]);
        }

        maxRight[height.length - 1] = height[height.length - 1];
        for (int i = height.length - 2; i >= 0; i--) {
            maxRight[i] = Math.max(maxRight[i + 1], height[i]);
        }

        int res = 0;
        for (int i = 1; i < height.length - 1; i++) {
            int h = Math.min(maxLeft[i], maxRight[i]) - height[i];
            if (h > 0) {
                res += h;
            }
        }
        return res;
    }
}
```
