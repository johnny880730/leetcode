# 0015. 三数之和

# 题目
给定一个包含 n 个整数的数组 nums，判断 nums 中是否存在三个元素 a，b，c ，使得 a + b + c = 0 ？找出所有满足条件且不重复的三元组。

注意：答案中不可以包含重复的三元组。

https://leetcode.cn/problems/3sum/description/

# 示例
```
示例 1：

输入：nums = [-1,0,1,2,-1,-4]
输出：[[-1,-1,2],[-1,0,1]]
解释：
nums[0] + nums[1] + nums[2] = (-1) + 0 + 1 = 0 。
nums[1] + nums[2] + nums[4] = 0 + 1 + (-1) = 0 。
nums[0] + nums[3] + nums[4] = (-1) + 2 + (-1) = 0 。
不同的三元组是 [-1,0,1] 和 [-1,-1,2] 。
注意，输出的顺序和三元组的顺序并不重要。
```
```
示例 2：

输入：nums = [0,1,1]
输出：[]
解释：唯一可能的三元组和不为 0 。
```
```
示例 3：

输入：nums = [0,0,0]
输出：[[0,0,0]]
解释：唯一可能的三元组和为 0 。
```

# 解析

## 排序 + 双指针

首先将数组排序，然后有一层 for 循环，i 从下标 0 的地方开始，同时定一个下标 left 定义在 i + 1 的位置上，定义下标 right 在数组结尾的位置上。

在数组中找到 a = nums[i]，b = nums[left]，c = nums[right]，使得 a + b + c = 0。

接下来如何移动 left 和 right呢
   - 如果 nums[i] + nums[left] + nums[right] > 0，说明此时三数之和大了，因为数组是排序后了，
     所以 right 下标就应该向左移动，这样才能让三数之和小一些。
   - 如果 nums[i] + nums[left] + nums[right] < 0，说明此时三数之和小了，left 就向右移动，才能让三数之和大一些，直到 left 与 right 相遇为止。

时间复杂度：O(n²)。

### 去重逻辑的思考

#### a 的去重
其实主要考虑三个数的去重。 a, b, c, 对应的就是 nums[i]，nums[left]，nums[right]。a 如果重复了怎么办，a 是 nums 里遍历的元素，那么应该直接跳过去。

但这里有一个问题，是判断 nums[i] 与 nums[i + 1] 是否相同，还是判断 nums[i] 与 nums[i - 1] 是否相同？这一样吗？其实不一样！
都是和 nums[i]进行比较，是比较它的前一个，还是比较他的后一个？

如果写法是这样：if (nums[i] == nums[i + 1]) { continue; }，那就把三元组中出现重复元素的情况直接 pass 掉了。 例如 {-1, -1 ,2} 这组数据，
当遍历到第一个 -1 的时候，判断下一个也是 -1，那这组数据就 pass 了。

那么应该这么写：if (i > 0 && nums[i] == nums[i - 1]) { continue; }，这么写就是当前使用 nums[i]，判断前一位是不是一样的元素，
在看 {-1, -1 ,2} 这组数据，当遍历到第一个 -1 的时候，只要前一位没有 -1，那么 {-1, -1 ,2} 这组数据一样可以收录到结果集里。

#### b 与 c 的去重
去重逻辑应该放在找到一个三元组之后，对 b 和 c 去重


# 代码

### php
```php
class LeetCode0015 {

    function threeSum($nums) {
        sort($nums);
        $res = [];
        $len = count($nums);
        // 找出 a + b + c = 0
        for ($i = 0; $i < $len - 2; $i++) {
            // 排序之后如果第一个元素已经大于零，那么无论如何组合都不可能凑成三元组，直接返回结果就可以了
            if ($nums[$i] > 0) {
                break;
            }
            // 错误去重方法，会漏掉[-1,-1,2]的情况
            /*
            if ($nums[$i] == $nums[$i+1]) {
                continue;
            }
            */
            // 正确去重方法
            if ($i > 0 && $nums[$i] == $nums[$i - 1]) {
                continue;
            }
            $left = $i + 1;
            $right = $len - 1;
            while ($left < $right) {
                // 如果去重复逻辑放在这里，可能直接导致 right<=left，从而漏掉了 0,0,0 这种三元组
                /*
                while (right > left && nums[right] == nums[right - 1]) right--;
                while (right > left && nums[left] == nums[left + 1]) left++;
                */
                $sum = $nums[$i] + $nums[$left] + $nums[$right];
                if ($sum < 0) {
                    $left++;
                    while ($left < $right && $nums[$left] == $nums[$left - 1]) {
                        $left++;
                    }
                } else if ($sum > 0) {
                    $right--;
                    while ($left < $right && $nums[$right] == $nums[$right + 1]) {
                        $right--;
                    }
                } else {
                    $res[] = [$nums[$i], $nums[$left], $nums[$right]];
                    // 找到答案时，双指针同时收缩
                    $left++;
                    $right--;
                    // 去重逻辑应该放在找到一个三元组之后
                    while ($left < $right && $nums[$left] == $nums[$left - 1]) {
                        $left++;
                    }
                    while ($left < $right && $nums[$right] == $nums[$right + 1]) {
                        $right--;
                    }
                }
            }
        }
        return $res;
    }

}
```

### go
```go
func threeSum(nums []int) [][]int {
    length := len(nums)
    if length < 3 {
        return [][]int{}
    }
    sort.Ints(nums)

    res := [][]int{}
    for i := 0; i < length - 2; i++ {
        if nums[i] > 0 {
            break
        }
        if i > 0 && nums[i] == nums[i - 1] {
            continue
        }
        left := i + 1
        right := length - 1
        for left < right {
            sum := nums[i] + nums[left] + nums[right]
            if sum < 0 {
                left++
                for left < right && nums[left] == nums[left - 1] {
                    left++
                }
            } else if sum > 0 {
                right--
                for left < right && nums[right] == nums[right + 1] {
                    right--
                }
            } else {
                res = append(res, []int{nums[i], nums[left], nums[right]})
                left++
                right--
                for left < right && nums[left] == nums[left - 1] {
                    left++
                }
                for left < right && nums[right] == nums[right + 1] {
                    right--
                }
            }
        }

    }

    return res
}
```

### java
```java
class LeetCode0015 {

    public List<List<Integer>> threeSum(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> res = new ArrayList<>();
        for (int i = 0; i < nums.length - 2; i++) {
            if (nums[i] > 0) {
                break;
            }
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            int left = i + 1, right = nums.length - 1;
            while (left < right) {
                int sum = nums[i] + nums[left] + nums[right];
                if (sum < 0) {
                    left++;
                    while (left < right && nums[left] == nums[left - 1]) {
                        left++;
                    }
                } else if (sum > 0) {
                    right--;
                    while (left < right && nums[right] == nums[right + 1]) {
                        right--;
                    }
                } else {
                    List<Integer> tmp = new ArrayList<>();
                    tmp.add(nums[i]);
                    tmp.add(nums[left]);
                    tmp.add(nums[right]);

                    res.add(tmp);

                    left++;
                    right--;

                    while (left < right && nums[left] == nums[left - 1]) {
                        left++;
                    }
                    while (left < right && nums[right] == nums[right + 1]) {
                        right--;
                    }
                }
            }
        }

        return res;
    }
}
```
