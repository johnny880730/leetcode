# 0581. 最短无序连续子数组

# 题目
给你一个整数数组 nums ，你需要找出一个 连续子数组 ，如果对这个子数组进行升序排序，那么整个数组都会变为升序排序。

请你找出符合题意的 最短 子数组，并输出它的长度。


提示：
- 1 <= nums.length <= 10^4
- -10^5 <= nums[i] <= 10^5

https://leetcode.cn/problems/shortest-unsorted-continuous-subarray/description/

# 示例
```
示例 1：

输入：nums = [2,6,4,8,10,9,15]
输出：5
解释：你只需要对 [6, 4, 8, 10, 9] 进行升序排序，那么整个表都会变为升序排序。
```
```
示例 2：

输入：nums = [1,2,3,4]
输出：0
```
```
示例 3：

输入：nums = [1]
输出：0
```

# 解析

## 一次遍历
将给定的数组 nums 表示为三段子数组拼接的形式：左段、中段、右段。

其中【中段】是无序的，但满足最小值大于【左段】的最大值，最大值小于【右段】的最小值

![img1](./images/leetcode-0581-img1.png)

只考虑中段数组，假设其左边界为 L，有边界为 R，有：
- nums[R] 不可能为 nums[L, R] 的最大值，否则应该将 R 划入右段数组
- nums[L] 不可能为 nums[L, R] 的足校之，否则应该将 L 划入左段数组

很明显有：
- nums[L, R] 的最大值，就是 nums[0, R] 的最大值，设其为 max
- nums[L, R] 的最小值，就是 nums[L, length - 1] 的最小值，设其为 min

那么有：
- nums[R] < max < nums[R + 1] < nums[R + 2] ... 也就是说，从左往右遍历，最右侧的小于 max 的为有边界
- nums[L] > min > nums[L - 1] > nums[L - 2] ... 也就是说，从右往左遍历，最左侧的大于 min 的为左边界


# 代码

### php
```php
class LeetCode0581 {

    /**
     * @param Integer[] $nums
     * @return Integer
     */
    function findUnsortedSubarray($nums) {
        $len = count($nums);
        $min = $nums[$len - 1];
        $max = $nums[0];
        $left = 0;
        $right = -1;
        for ($i = 0; $i < $len; $i++) {
            // 从左到右维持最大值，寻找右边界
            if ($nums[$i] < $max) {
                $right = $i;
            } else {
                $max = $nums[$i];
            }
            // 从右到左维持最小值，寻找左边界
            if ($nums[$len - $i - 1] > $min) {
                $left = $len - $i - 1;
            } else {
                $min = $nums[$len - $i - 1];
            }
        }

        return $right - $left + 1;
    }
}
```

### java
```java
class LeetCode0581 {

    public int findUnsortedSubarray(int[] nums) {
        int len = nums.length;
        int min = nums[len - 1], max = nums[0];
        int left = 0, right = -1;

        for (int i = 0; i < len; i++) {
            // 从左到右维持最大值，寻找右边界
            if (nums[i] < max) {
                right = i;
            } else {
                max = nums[i];
            }
            // 从右到左维持最小值，寻找左边界
            if (nums[len - i - 1] > min) {
                left = len - i - 1;
            } else {
                min = nums[len - i - 1];
            }
        }
        return right - left + 1;
    }
}
```

### go
```go
func findUnsortedSubarray(nums []int) int {
    len := len(nums)
    min, max := nums[len - 1], nums[0]
    left, right := 0, -1
    
    for i := 0; i < len; i++ {
        // 从左到右维持最大值，寻找右边界
        if nums[i] < max {
            right = i
        } else {
            max = nums[i]
        }
        // 从右到左维持最小值，寻找左边界
        if nums[len - i - 1] > min {
            left = len - i - 1;
        } else {
            min = nums[len - i - 1]
        }
    }
    
    return right - left + 1
}

```


