# 59. 螺旋矩阵 II

# 题目
给你一个正整数 n ，生成一个包含 1 到 n2 所有元素，且元素按顺时针顺序螺旋排列的 n x n 正方形矩阵 matrix 。

```
01 → 02 → 03 → 04
               ↓
12 → 13 → 14   05
↑         ↓    ↓
11   16 ← 15   06
↑              ↓
10 ← 09 ← 08 ← 07
```

https://leetcode.cn/problems/spiral-matrix-ii/

# 示例：
```
输入：n = 3
输出：[[1,2,3],[8,9,4],[7,6,5]]
```

```
输入：n = 1
输出：[[1]]
```

提示：
- 1 <= n <= 20

# 解析
如何打印出这个螺旋排列的正方形矩阵呢？

模拟顺时针“画”矩阵的过程：
- 从左到右填充上行
- 从上到下填充右列
- 从右到左填充下行
- 从下到上填充左列

由外向内一圈一圈的画下去，可以发现这里的边界条件非常多，如果不按照固定规则遍历，一定会各种犯错，所以一定要坚持循环不变量原则。

矩阵的四条边都要坚持一致的左闭右开或者左开右闭的原则，这样才能按照统一的规则画下来。

![](./images/leetcode-0059.png)

图中每一个箭头覆盖的长度表示一条边遍历的长度，可以看出每一个拐角处的处理规则，在拐角处开始画另一条新的边，这里的左闭右开就是“不变量”，
在循环中坚持这个“不变量”，才能顺利的把这个圈画出来。

# 代码

### php
```php
class LeetCode0059 {

    // 将拐角当作另一条边的开始处
    public function main($n) {
        $res = array_fill(0, $n, array_fill(0, $n , 0));

        // 每一圈的开始位置
        $startX = $startY = 0;

        // 打印的数字，根据题意从 1 开始
        $num = 1;

        // 一共要跑的圈数：n=3跑一圈，4跑两圈，5跑两圈，6跑三圈，n为奇数的话中间都有个一个中心点需要注意
        $loop = $n >> 1;

        // 每一圈循环，需要控制每一条边遍历的长度
        $offset = 1;

        while ($loop > 0) {
            $i = $startX;
            $j = $startY;

            // 每一圈的top边
            for (; $j < $startY + $n - $offset; $j++) {
                $res[$i][$j] = $num++;
            }

            // 每一圈的right边
            for (; $i < $startX + $n - $offset; $i++) {
                $res[$i][$j] = $num++;
            }

            // 每一圈的bottom边
            for (; $j > $startY; $j--) {
                $res[$i][$j] = $num++;
            }

            // 每一圈的left边
            for (; $i > $startX; $i--) {
                $res[$i][$j] = $num++;
            }

            // 下一圈比前一圈就缩了一格，因此开始位置往右下方走一格，如 [0,0] => [1,1]
            $startX += 1;
            $startY += 1;

            //每缩一圈，每一条边少了两个单元格
            $offset += 2;

            $loop--;
        }

        // n为奇数的话中心点的处理
        if ($n % 2 == 1) {
            // 中心点，n=3中心点[1][1]，n=5的话就是[3][3]
            $mid = $n >> 1;
            $res[$mid][$mid] = $num;
        }

        return $res;
    }
}
```

### go
```go
func generateMatrix(n int) [][]int {
    res := make([][]int, n)
    for i := 0; i < n; i++ {
        res[i] = make([]int, n)
    }

    startX, startY := 0, 0
    offset := 1
    loop := n >> 1
    num := 1

    for loop > 0 {
        i, j := startX, startY

        for ; j < startY + n - offset; j++ {
            res[i][j] = num
            num++
        }

        for ; i < startX +n - offset; i++ {
            res[i][j] = num
            num++
        }

        for ; j > startY; j-- {
            res[i][j] = num
            num++
        }

        for ; i > startX; i-- {
            res[i][j] = num
            num++
        }

        startX++
        startY++

        offset += 2

        loop--
    }

    if n % 2 > 0 {
        mid := n >> 1
        res[mid][mid] = num
    }

    return res
}
```
