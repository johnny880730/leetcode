# 922. 按奇偶排序数组 II

# 题目
给定一个非负整数数组 A， A 中一半整数是奇数，一半整数是偶数。

对数组进行排序，以便当 A[i] 为奇数时，i 也是奇数；当 A[i] 为偶数时， i 也是偶数。

你可以返回任何满足上述条件的数组作为答案。

https://leetcode.cn/problems/sort-array-by-parity-ii/

提示：
- 2 <= A.length <= 20000
- nums.length 是偶数
- nums 中一半是偶数
- 0 <= A[i] <= 1000


# 示例
```
输入：[4,2,5,7]
输出：[4,5,2,7]
解释：[4,7,2,5]，[2,5,4,7]，[2,7,4,5] 也会被接受。
```


# 解析

## 方法一
把 A 数组放进偶数数组和奇数数组，最后合并到结果数组。时间复杂度 O(n)


## 方法二
方法一是建了两个辅助数组，而且 A 数组还相当于遍历了两次，用辅助数组的好处就是思路清晰，优化一下就是不用这两个辅助数组


# 代码

### php
```php
class Leetcode0922 {

    // 方法一
    function sortArrayByParityII($nums) {
        $len = count($nums);
        $aEven = $aOdd = [];
        // 把A数组放进偶数数组，和奇数数组
        foreach ($nums as $num) {
            if ($num & 1) {
                $aOdd[] = $num;
            } else {
                $aEven[] = $num;
            }
        }
        // 把偶数数组，奇数数组分别放进result数组中
        $res = [];
        for ($i = 0; $i < $len / 2; $i++) {
            $res[] = $aEven[$i];
            $res[] = $aOdd[$i];
        }
        return $res;
    }
    
    // 方法二
    function sortArrayByParityII_2($nums) {
        $len = count($nums);
        $res = array_fill(0, $len, 0);
        $even = 0;          // 偶数下标
        $odd = 1;           // 奇数下标
        for ($i = 0; $i < $len; $i++) {
            if ($nums[$i] & 1) {
                $res[$odd] = $nums[$i];
                $odd += 2;
            } else {
                $res[$even] = $nums[$i];
                $even += 2;
            }
        }
        return $res;
    }

}
```

### go
```go

```