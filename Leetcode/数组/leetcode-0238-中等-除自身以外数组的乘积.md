# 0238. 除自身以外数组的乘积

# 题目
给你一个整数数组 nums，返回数组 answer，其中 answer[i] 等于 nums 中除 nums[i] 之外其余各元素的乘积。

题目数据保证数组 nums之 中任意元素的全部前缀元素和后缀的乘积都在 32 位整数范围内。

请不要使用除法，且在 O(n) 时间复杂度内完成此题。

https://leetcode.cn/problems/product-of-array-except-self/description/

提示：
- 2 <= nums.length <= 10^5
- -30 <= nums[i] <= 30
- 保证 数组nums之中任意元素的全部前缀元素和后缀的乘积都在 32 位 整数范围内


# 示例
```
输入: nums = [1,2,3,4]
输出: [24,12,8,6]
```
```
输入: nums = [-1,1,0,-3,3]
输出: [0,0,9,0,0]
```

# 解析

## 左右乘积列表（不用除法）
不必将所有数字的乘积除以给定索引处的数字得到相应的答案，而是利用索引左侧所有数字的乘积和右侧所有数字的乘积（即前缀与后缀）相乘得到答案。

- 初始化两个空数组 L 和 R。对于给定索引 i，L[i] 代表的是 i 左侧所有数字的乘积（不包括自己），R[i] 代表的是 i 右侧所有数字的乘积（不包括自己）。
- 需要用两个循环来填充 L 和 R 数组的值。对于数组 L，L[0] 应该是 1，因为第一个元素的左边没有元素。对于其他元素：L[i] = L[i - 1] * nums[i - 1]。
- 同理，对于数组 R，R[n - 1] 应为 1。n 指的是输入数组的大小。其他元素：R[i] = R[i + 1] * nums[i + 1]。
- 当 R 和 L 数组填充完成，只需要在输入数组上迭代，且索引 i 处的值为：L[i] * R[i]。
  

## 可以用除法的方法
先把 nums 中所有不是 0 的数乘起来，定为 all。遍历过程中记录下出现 0 的次数，定为 zeroes.

0 的数量 >= 2 的话，结果就都是 0 了。


# 代码

### php
```php
class LeetCode0238 {

    // 不能用除法
    function productExceptSelf($nums) {
        $len = count($nums);
        $left = $right = array_fill(0, $len, 0);
        $left[0] = $right[$len - 1] = 1;

        for ($i = 1; $i < $len; $i++) {
            $left[$i] = $nums[$i - 1] * $left[$i - 1];
        }
        for ($i = $len - 2; $i >= 0; $i--) {
            $right[$i] = $nums[$i + 1] * $right[$i + 1];
        }

        $res = array_fill(0, $len, 0);
        for ($i = 0; $i < $len; $i++) {
            $res[$i] = $left[$i] * $right[$i];
        }

        return $res;
    }
    
    // 能用除法
    function productExceptSelf2($nums) {
        $zeroes = 0;
        $all = 1;
        foreach ($nums as $num) {
            if ($num == 0) {
                $zeroes++;
            } else {
                $all *= $num;
            }
        }
        $len = count($nums);
        $res = array_fill(0, $len - 1, 0);;
        if ($zeroes > 1) {
            return $res; 
        }
        if ($zeroes == 0) {
            for ($i = 0; $i < $len; $i++) {
                $res[$i] = $all / $nums[$i];
            }
        } else {
            for ($i = 0; $i < $len; $i++) {
                $res[$i] = $nums[$i] == 0 ? $all : 0;
            }
        }
        return $res;
        
    }
    
    
}
```

### java
```java
class LeetCode0238 {

    public int[] productExceptSelf(int[] nums) {
        int[] left = new int[nums.length];
        int[] right = new int[nums.length];
        left[0] = right[nums.length - 1] = 1;
        for (int i = 1; i < nums.length; i++) {
            left[i] = left[i - 1] * nums[i - 1];
        }
        for (int i = nums.length - 2; i >= 0; i--) {
            right[i] = right[i + 1] * nums[i + 1];
        }

        int[] res = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            res[i] = left[i] * right[i];
        }

        return res;
    }
    
}
```

### go
```go
func productExceptSelf(nums []int) []int {
    length := len(nums)
    left, right := make([]int, length), make([]int, length)
    left[0], right[length - 1] = 1, 1
    for i := 1; i < length; i++ {
        left[i] = nums[i - 1] * left[i - 1]
    }
    for i := length - 2; i >= 0; i-- {
        right[i] = nums[i + 1] * right[i + 1]
    }

    res := make([]int, length)
    for i := 0; i < length; i++ {
        res[i] = left[i] * right[i]
    }
    
    return res
}
```
