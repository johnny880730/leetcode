# 26. 删除排序数组中的重复项

# 题目
给定一个排序数组，你需要在原地删除重复出现的元素，使得每个元素只出现一次，返回移除后数组的新长度。

不要使用额外的数组空间，你必须在原地修改输入数组并在使用 O(1) 额外空间的条件下完成。

https://leetcode.cn/problems/remove-duplicates-from-sorted-array

提示：
- 1 <= nums.length <= 3 * 104
- 10^4 <= nums[i] <= 10^4
- nums 已按 升序 排列

# 示例
```
给定数组 nums = [1,1,2],
函数应该返回新的长度 2, 并且原数组 nums 的前两个元素被修改为 1, 2。
你不需要考虑数组中超出新长度后面的元素。
```
```
给定 nums = [0,0,1,1,1,2,2,3,3,4],
函数应该返回新的长度 5, 并且原数组 nums 的前五个元素被修改为 0, 1, 2, 3, 4。
你不需要考虑数组中超出新长度后面的元素。
```

# 解析

## 双指针

首先注意数组是有序的，那么重复的元素一定会相邻。

要求删除重复元素，实际上就是将不重复的元素移到数组的左侧。

考虑用 2 个指针，一个在前记作 fast，一个在后记作 slow 算法流程如下：

1. 比较 fast 和 slow 位置的元素是否相等。
  - 如果相等，fast 后移 1 位
  - 如果不相等，将 fast 位置的元素复制到 slow + 1 位置上，slow 后移一位，fast 后移 1 位
  - 重复上述过程，直到 fast 等于数组长度。
2. 返回 slow + 1，即为新数组长度。

来自 labuladong 的图示：  
![](./images/leetcode-0026-题解.gif)

# 代码

### php
```php
class LeetCode0026 {

    function removeDuplicates(&$nums) {
        $len = count($nums);
        if ($len == 0) {
            return 0;
        }
        $fast = $slow = 0;
        while ($fast < $len) {
            if ($nums[$fast] != $nums[$slow]) {
                $slow++;
                $nums[$slow] = $nums[$fast];
            }
            $fast++;
        }
        return $slow + 1;
    }
}
```

### go
```go
func removeDuplicates(nums []int) int {
    if len(nums) <= 0 {
        return 0
    }
    length := len(nums)
    fast := 0
    slow := 0
    for fast < length {
        if nums[fast] != nums[slow] {
            slow++
            nums[slow] = nums[fast]
        }
        fast++
    }
    return slow + 1
}
```

### java
```java
class LeetCode0026 {

    public int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int fast = 0, slow = 0;
        while (fast < nums.length) {
            if (nums[fast] != nums[slow]) {
                slow++;
                nums[slow] = nums[fast];
            }
            fast++;
        }
        return slow + 1;
    }
}
```
