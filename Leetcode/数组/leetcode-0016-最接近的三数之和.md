### 16. 最接近的三数之和

# 题目
给你一个长度为 n 的整数数组 nums 和 一个目标值 target。请你从 nums 中选出三个整数，使它们的和与 target 最接近。

返回这三个数的和。

假定每组输入只存在恰好一个解。

https://leetcode.cn/problems/3sum-closest/

提示：
- 3 <= nums.length <= 1000
- -1000 <= nums[i] <= 1000
- -10<sup>4</sup> <= target <= 10<sup>4</sup>

# 示例
```
输入：nums = [-1,2,1,-4], target = 1
输出：2
解释：与 target 最接近的和是 2 (-1 + 2 + 1 = 2) 。
```

```
输入：nums = [0,0,0], target = 1
输出：0
```

# 解析

#### 排序 + 双指针

&emsp;&emsp;题目要求找到与目标值 target 最接近的三元组，这里的「最接近」即为差值的绝对值最小。可以考虑直接使用三重循环枚举三元组，
找出与目标值最接近的作为答案，时间复杂度为 O(N³)。然而本题的 N 最大为 1000，会超出时间限制。

&emsp;&emsp;那么如何进行优化呢？首先考虑枚举第一个元素 a，对于剩下的两个元素 b 和 c，希望它们的和最接近 target−a。对于 b 和 c，
如果它们在原数组中枚举的范围（既包括下标的范围，也包括元素值的范围）没有任何规律可言，那么还是只能使用两重循环来枚举所有的可能情况。
因此，可以考虑对整个数组进行升序排序，这样一来：
- 假设数组的长度为 n，先枚举 a，它在数组中的位置为 i
- 为了防止重复枚举，在位置 [i+1, n) 的范围内枚举 b 和 c

&emsp;&emsp;当知道了 b 和 c 可以枚举的下标范围，并且知道这一范围对应的数组元素是有序（升序）的，那么是否可以对枚举的过程进行优化呢？

&emsp;&emsp;答案是可以的。借助双指针，就可以对枚举的过程进行优化。用 pb 和 pc 分别表示指向 b 和 cc 的指针，初始时，pb 指向位置 i+1，即左边界；
pc 指向位置 n−1，即右边界。在每一步枚举的过程中，用 a+b+c 来更新答案，并且：
- 如果 a+b+c >= target，那么就将 pc 向左移动一个位置
- 如果 a+b+c < target，那么就将 pb 向右移动一个位置

&emsp;&emsp;另外当枚举到恰好等于 target 的 a+b+c 时，可以直接返回 target 作为答案，因为不会有再比这个更接近的值了。

# 代码

```php
$arr = [
    ['nums' => [-1, 2, 1, -4], 'target' => 1],
    ['nums' => [0, 0, 0], 'target' => 1],
];
(new Solution0016())->main($arr);

class Solution0016
{

    function main($arr)
    {
        foreach ($arr as $item) {
            $res = $this->threeSumClosest($item['nums'], $item['target']);
            var_dump($item, $res);
            echo '======================' . PHP_EOL;
        }
    }

    function threeSumClosest($nums, $target)
    {
        sort($nums);
        $n = count($nums);
        $best = 1000000;

        for ($i = 0; $i < $n; $i++) {
            // nums[i] 假设为 a
            // 保证和上一次枚举的元素不相等
            if ($i > 0 && $nums[$i] == $nums[$i - 1]) {
                continue;
            }
            // 使用双指针枚举 b 和 c
            $j = $i + 1;
            $k = $n - 1;
            while ($j < $k) {
                $sum = $nums[$i] + $nums[$j] + $nums[$k];
                // 如果和为 target 直接返回答案
                if ($sum == $target) {
                    return $target;
                }
                // 根据差值的绝对值来更新答案
                if (abs($sum - $target) < abs($best - $target)) {
                    $best = $sum;
                }
                if ($sum > $target) {
                    // 如果和大于 target，移动 c 对应的指针
                    $k0 = $k - 1;
                    // 移动到下一个不相等的元素
                    while ($j < $k0 && $nums[$k0] == $nums[$k]) {
                        --$k0;
                    }
                    $k = $k0;
                } else {
                    // 如果和小于 target，移动 b 对应的指针
                    $j0 = $j + 1;
                    // 移动到下一个不相等的元素
                    while ($j0 < $k && $nums[$j0] == $nums[$j]) {
                        ++$j0;
                    }
                    $j = $j0;
                }
            }
        }
        return $best;
    }

}
```