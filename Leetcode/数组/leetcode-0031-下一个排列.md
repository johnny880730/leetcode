# 31. 下一个排列

# 题目
实现获取 下一个排列 的函数，算法需要将给定数字序列重新排列成字典序中下一个更大的排列（即，组合出下一个更大的整数）。

如果不存在下一个更大的排列，则将数字重新排列成最小的排列（即升序排列）。

必须 原地 修改，只允许使用额外常数空间。

https://leetcode.cn/problems/next-permutation/

提示：
1 <= nums.length <= 100
0 <= nums[i] <= 100

## 示例
```
输入：nums = [1,2,3]
输出：[1,3,2]
```
```
输入：nums = [3,2,1]
输出：[1,2,3]
```
```
输入：nums = [1,1,5]
输出：[1,5,1]
```

# 解析

从右往左遍历，如果总是有 左数 >= 右数，那么此时这个全排列就是最大的，如：[5,4,3,2,2,1,1]

如果不是最大的全排列，从右往左寻找第一个降序的数，找到它的位置 idx1。如：[4,7,6,3,2,1]，4 的位置就是 idx1。
而此时就算 4 的左边存在其他数字 XXX，也不用管。因为下一个排列肯定有 4 大的，要做的只是调整遍历过的数字就行。

在 4 的右侧找到最小的且比 4 大的数 idx2（且这个idx2 要尽量靠右），将 idx2 位置的数与 idx1 位置的数交换，此时结果为 [6,7,4,3,2,1]，
也就是 6 和 4 的位置交换了。最后将 idx1 右侧的数逆序一下，就是最后的结果，也就是 [6,1,2,3,4,7]。

# 代码

### php
```php
class LeetCode0031 {

    function nextPermutation(&$nums) {
        $len = count($nums);
        // 获取从右往左第一次降序的位置
        $firstLess = -1;
        for ($i = $len - 2; $i >= 0; $i--) {
            if ($nums[$i] < $nums[$i + 1]) {
                $firstLess = $i;
                break;
            }
        }
        if ($firstLess < 0) {
            // 没找到第一次降序的，说明本身已经最大，直接逆序即可
            $this->_reverse($nums, 0, $len - 1);
        } else {
            // 找到最靠右的，比 nums[firstLess]大的数，获取它的位置
            $rightMore = -1;
            for ($i = $len - 1; $i > $firstLess; $i--) {
                if ($nums[$i] > $nums[$firstLess]) {
                    $rightMore = $i;
                    break;
                }
            }
            if ($rightMore >= 0) {
                // 找到的话将它与 firstLess 的数交换
                $t = $nums[$firstLess];
                $nums[$firstLess] = $nums[$rightMore];
                $nums[$rightMore] = $t;
            }
            // 最后firstLess 右侧的数逆序
            $this->_reverse($nums, $firstLess + 1, $len - 1);
        }
    }

    protected function _reverse(&$nums, $start, $end)
    {
        while ($start < $end) {
            $tmp = $nums[$start];
            $nums[$start] = $nums[$end];
            $nums[$end] = $tmp;
            $start++;
            $end--;
        }
    }
}
```

### java
```java
class Leetcode0031 {
    
    public void nextPermutation(int[] nums) {
        int firstLess = -1;
        for (int i = nums.length - 2; i >= 0; i--) {
            if (nums[i] < nums[i + 1]) {
                firstLess = i;
                break;
            }
        }
        if (firstLess < 0) {
            _reverse(nums, 0, nums.length - 1);
        } else {
            int rightMore = -1;
            for (int i = nums.length - 1; i >= firstLess; i--) {
                if (nums[i] > nums[firstLess]) {
                    rightMore = i;
                    break;
                }
            }
            if (rightMore >= 0) {
                int tmp = nums[firstLess];
                nums[firstLess] = nums[rightMore];
                nums[rightMore] = tmp;
            }
            _reverse(nums, firstLess + 1, nums.length - 1);
        }
    }

    private void _reverse(int[] nums, int i, int j) {
        while (i < j) {
            int tmp = nums[i];
            nums[i] = nums[j];
            nums[j] = tmp;
            i++;
            j--;
        }
    }
}
```

### go
```go
func nextPermutation(nums []int)  {
    var _reverse func(nums []int, i int, j int)
    _reverse = func(nums []int, i int, j int) {
        for i < j {
            nums[i], nums[j] = nums[j], nums[i]
            i++
            j--
        }
    }
    
    firstLess := -1
    for i := len(nums) - 2; i >= 0; i-- {
        if nums[i] < nums[i + 1] {
            firstLess = i
            break
        }
    }
    if firstLess < 0 {
        _reverse(nums, 0, len(nums) - 1)
    } else {
        rightMore := -1
        for i := len(nums) - 1; i > firstLess; i-- {
            if nums[i] > nums[firstLess] {
                rightMore = i
                break
            }
        }
        if rightMore >= 0 {
            nums[firstLess], nums[rightMore] = nums[rightMore], nums[firstLess]
        }
        _reverse(nums, firstLess + 1, len(nums) - 1)
    }
}
```