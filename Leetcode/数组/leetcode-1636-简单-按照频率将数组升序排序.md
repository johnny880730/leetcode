# 1636. 按照频率将数组升序排序

# 题目
给你一个整数数组 nums ，请你将数组按照每个值的频率 升序 排序。如果有多个值的频率相同，请你按照数值本身将它们 降序 排序。

请你返回排序后的数组。

提示：
- 1 <= nums.length <= 100
- -100 <= nums[i] <= 100

https://leetcode.cn/problems/sort-array-by-increasing-frequency/description/

# 示例
```
示例 1：

输入：nums = [1,1,2,2,2,3]
输出：[3,1,1,2,2,2]
解释：'3' 频率为 1，'1' 频率为 2，'2' 频率为 3 。
```
```
示例 2：

输入：nums = [2,3,1,3,2]
输出：[1,3,3,2,2]
解释：'2' 和 '3' 频率都为 2 ，所以它们之间按照数值本身降序排序。
```
```
示例 3：

输入：nums = [-1,1,-6,4,5,-6,1,4,1]
输出：[5,-1,4,4,-6,-6,1,1,1]
```

# 解析

## 哈希 + 排序 + 模拟
先使用哈希表进行词频统计，再以二元组 (x, cnt) 的形式转存到数组 list 中（其中 x 为对应的 nums 中的数值，cnt 为数值 x 在 nums 中的出现次数），
再根据题目给定对 list 进行排序，最后再构造出答案

# 代码

### php
```php
class LeetCode1636 {

    /**
     * @param Integer[] $nums
     * @return Integer[]
     */
    function frequencySort($nums) {
        // 计算频次
        $map = array();
        foreach ($nums as $num) {
            if (!array_key_exists($num, $map)) {
                $map[$num] = 0;
            }
            $map[$num]++;
        }
        
        $list = array();
        foreach ($map as $num => $cnt) {
            $list[] = [$num, $cnt];
        }
        // 根据 频次升序 + 同频次的话数字降序的规则给list排序
        usort($list, function($o1, $o2){
            if ($o1[1] < $o2[1]) {
                return -1;
            } else if ($o1[1] > $o2[1]) {
                return 1;
            } else {
                if ($o1[0] > $o2[0]) {
                    return -1;
                } else if ($o1[0] < $o2[0]) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        // 写入结果
        $res = [];
        $idx = 0;
        foreach ($list as $item) {
            for ($i = 0; $i < $item[1]; $i++) {
                $res[$idx++] = $item[0];
            }
        }

        return $res;
    }
}
```

### java
```java
class LeetCode1636 {
    public int[] frequencySort(int[] nums) {
        // 计算频次
        Map<Integer, Integer> map = new HashMap<>();
        for (int num: nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        List<int[]> list = new ArrayList<>();
        for (int key: map.keySet()) {
            list.add(new int[]{key, map.get(key)});
        }
        // 根据 频次升序 + 同频次的话数字降序的规则给list排序
        Collections.sort(list, (o1, o2) -> {
            if (o1[1] < o2[1]) {
                return -1;
            } else if (o1[1] > o2[1]) {
                return 1;
            } else {
                if (o1[0] > o2[0]) {
                    return -1;
                } else if (o1[0] < o2[0]) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        // 写入结果
        int[] res = new int[nums.length];
        int idx = 0;
        for (int[] item: list) {
            for (int j = 0; j < item[1]; j++) {
                res[idx++] = item[0];
            }
        }

        return res;
    }
}
```

### go
```go
func frequencySort(nums []int) []int {
    freqMap := make(map[int]int)
	for _, num := range nums {
		freqMap[num]++
	}
	type freqPair struct {
		value int
		count int
	}
	// 构建一个切片用于排序
	list := make([]freqPair, len(freqMap))
	for num, cnt := range freqMap {
		list = append(list, freqPair{value: num, count: cnt})
	}

	sort.Slice(list, func(i, j int) bool {
		if list[i].count != list[j].count {
			// 第一项小的在前面
			return list[i].count < list[j].count
		}
		// 第一项相同，第二项更大的在前面
		return list[i].value > list[j].value
	})

	res := make([]int, len(nums))
	idx := 0
	for _, item := range list {
		for i := 0; i < item.count; i++ {
			res[idx] = item.value
			idx++
		}
	}

	return res
}
```
