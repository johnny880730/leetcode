# 2656. K 个元素的最大和


# 题目
给你一个下标从 0 开始的整数数组 nums 和一个整数 k 。你需要执行以下操作 恰好 k 次，最大化你的得分：
- 从 nums 中选择一个元素 m 。
- 将选中的元素 m 从数组中删除。
- 将新元素 m + 1 添加到数组中。
- 你的得分增加 m 。

请你返回执行以上操作恰好 k 次后的最大得分。

提示：
- 1 <= nums.length <= 100
- 1 <= nums[i] <= 100
- 1 <= k <= 100

# 示例
```
输入：nums = [1,2,3,4,5], k = 3
输出：18
解释：我们需要从 nums 中恰好选择 3 个元素并最大化得分。
第一次选择 5 。和为 5 ，nums = [1,2,3,4,6] 。
第二次选择 6 。和为 6 ，nums = [1,2,3,4,7] 。
第三次选择 7 。和为 5 + 6 + 7 = 18 ，nums = [1,2,3,4,8] 。
所以我们返回 18 。
18 是可以得到的最大答案。
```
```
输入：nums = [5,5,5], k = 2
输出：11
解释：我们需要从 nums 中恰好选择 2 个元素并最大化得分。
第一次选择 5 。和为 5 ，nums = [5,5,6] 。
第二次选择 6 。和为 6 ，nums = [5,5,7] 。
所以我们返回 11 。
11 是可以得到的最大答案。
```

# 解析

注意到，要使得最终的得分最大，应该尽可能地使得每次选择的元素最大。因此，第一次选择数组中的最大元素 x，第二次选择 x + 1，第三次选择 x + 2。
以此类推，直到第 k 次选择 x + k − 1。

这样的选择方式可以保证每次选择的元素都是当前数组中的最大值，因此最终的得分也是最大的。答案即为 k 个 x 的和加上 0 + 1 + 2 + ⋯ + ( k − 1 )，
即 k * x + (k − 1) * k / 2 。

# 代码

### php
```php
class Leetcode2656 {

    function maximizeSum($nums, $k) {
        $x = max($nums);
        return $k * $x + $k * ($k - 1) / 2;
    }
    
}
```

### java
```java
public int maximizeSum(int[] nums, int k) {
    int x = 0;
    for (int v: nums) {
        x = Math.max(x, v);
    }
    return k * x + (k - 1) * k / 2;
}
```

### go
```go
func maximizeSum(nums []int, k int) int {
    x := 0;
    for _, v := range nums {
        x = Max(x, v)
    }
    return k * x + (k - 1) * k / 2;
}

// Max ：支持 int 类型的 max 函数
func Max(num1 int, num2 int) int {
    if num1 >= num2 {
        return num1
    }
    return num2
}
```
