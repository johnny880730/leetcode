# 304. 二维区域和检索 - 矩阵不可变
给定一个二维矩阵 matrix，以下类型的多个请求：
- 计算其子矩形范围内元素的总和，该子矩阵的 左上角 为 (row1, col1) ，右下角 为 (row2, col2) 。


实现 NumMatrix 类：
- NumMatrix(int[][] matrix) 给定整数矩阵 matrix 进行初始化
- int sumRegion(int row1, int col1, int row2, int col2) 返回 左上角 (row1, col1) 、右下角 (row2, col2) 所描述的子矩阵的元素 总和 。

https://leetcode.cn/problems/range-sum-query-2d-immutable/

提示：
- m == matrix.length
- n == matrix[i].length
- 1 <= m, n <= 200
- -105 <= matrix[i][j] <= 10^5
-  0 <= row1 <= row2 < m
-  0 <= col1 <= col2 < n
-  最多调用 10^4 次 sumRegion 方法


# 示例：
```
输入: 
["NumMatrix","sumRegion","sumRegion","sumRegion"]
[[[[3,0,1,4,2],[5,6,3,2,1],[1,2,0,1,5],[4,1,0,1,7],[1,0,3,0,5]]],[2,1,4,3],[1,1,2,2],[1,2,2,4]]
输出: 
[null, 8, 11, 12]

解释:
NumMatrix numMatrix = new NumMatrix([[3,0,1,4,2],[5,6,3,2,1],[1,2,0,1,5],[4,1,0,1,7],[1,0,3,0,5]]);
numMatrix.sumRegion(2, 1, 4, 3);
numMatrix.sumRegion(1, 1, 2, 2);
numMatrix.sumRegion(1, 2, 2, 4);
```


# 解析

# 代码

## 前缀和
只要确定了一个矩阵的左上角和右下角，就可以确定这个矩阵。矩阵左上角为坐标原点 (0, 0)。

注意任意一个子矩阵的元素和可以转化成它周边几个大矩阵的元素和的运算。见下图：

![](./images/leetcode-0304-图解.png)

可以维护一个二维的 preSum 数组，专门记录以 (0, 0) 点为顶点的矩阵的元素之和，就可以用几次加减运算出任何一个子矩阵的元素和。

### php

```php
class NumMatrix {
    protected $preSum = [];

    function __construct($matrix) {
        $m = count($matrix); 
        $n = count($matrix[0]);
        if ($m == 0 || $n == 0) {
            return;
        }
        $this->preSum = array_fill(0, $m + 1, array_fill(0, $n + 1, 0));
        for ($i = 1; $i <= $m; $i++) {
            for ($j = 1; $j <= $n; $j++) {
                // 计算子矩阵 [0, 0, i, j] 的元素和
                $this->preSum[$i][$j] = $this->preSum[$i - 1][$j] + $this->preSum[$i][$j - 1] + $matrix[$i - 1][$j - 1] - $this->preSum[$i - 1][$j - 1];
            }
        }

    }

    function sumRegion($row1, $col1, $row2, $col2) {
        return $this->preSum[$row2 + 1][$col2 + 1] - $this->preSum[$row1][$col2 + 1] - $this->preSum[$row2 + 1][$col1] + $this->preSum[$row1][$col1];
    }
}

```
