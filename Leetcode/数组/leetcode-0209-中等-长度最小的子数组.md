# 209. 长度最小的子数组

# 题目
给定一个含有 n 个正整数的数组和一个正整数 target 。

找出该数组中满足其和 ≥ target 的长度最小的 连续子数组 [numsl, numsl+1, ..., numsr-1, numsr] ，
并返回其长度。如果不存在符合条件的子数组，返回 0 。

https://leetcode.cn/problems/minimum-size-subarray-sum/description/

提示：
- 1 <= target <= 10^9
- 1 <= nums.length <= 10^5
- 1 <= nums[i] <= 10^5


# 示例：
```
输入：target = 7, nums = [2,3,1,2,4,3]
输出：2
解释：子数组 [4,3] 是该条件下的长度最小的子数组。
```

```
输入：target = 4, nums = [1,4,4]
输出：1
```

```
输入：target = 11, nums = [1,1,1,1,1,1,1,1]
输出：0
```


# 解析

## 滑动窗口
所谓滑动窗口，就是不断地调节子数组的起始位置和终止位置，从而得出我们想要的结果。

滑动窗口也可以理解为双指针法的一种，只不过这种解法更像是一个窗口的移动。在本题中实现滑动窗口，主要是确定以下三点：
- 窗口内的元素是什么
- 如何移动窗口的起始位置
- 如何移动窗口的终止位置

窗口内的元素：保持窗口内数值总和大于等于 target 的长度最小的连续子数组

移动窗口的起始位置：如果当前窗口的值大于 target，则窗口向前移动（也就是窗口缩小了）。

移动窗口的终止位置：窗口的结束位置就是 for 循环遍历数组的指针。

可以发现滑动窗口的精妙之处在于根据当前子数组和的大小，不断调节子数组的起始位置，从而将时间复杂度从 O(n²) 降到 O(n)。

以题目中的示例来举例，s = 7， 数组是 2，3，1，2，4，3，来看一下查找的过程，最后找到 4，3 是最短距离。

![](./images/leetcode-0209.gif)

# 代码

### php
```php
class LeetCode0209 {

    public function minSubArrayLen($nums, $target) {
        $res = PHP_INT_MAX;
        //滑动窗口的和
        $sum = 0;

        $left = 0;

        for ($right = 0, $len = count($nums); $right < $len; $right++) {
            $sum += $nums[$right];

            // 一旦窗口里的和 >=target，开始计算窗口长度，并从左侧弹出元素
            while ($sum >= $target) {
                // 子序列的长度
                $subLength = $right - $left + 1;

                // 更新结果
                $res = $res < $subLength ? $res : $subLength;

                // left 位置的出窗口后要从 sum 减去
                $sum -= $nums[$left];

                // 窗口缩小
                $left++;
            }
        }
        return $res == PHP_INT_MAX ? 0 : $res;
    }

}
```

### go
```go
func minSubArrayLen(target int, nums []int) int {
    length := len(nums)
    res := math.MaxInt32
    left, sum := 0, 0
    for right := 0; right < length; right++ {
        sum += nums[right]
        for sum >= target {
            subLen := right - left + 1
            if subLen < res {
                res = subLen
            }
            sum -= nums[left]
            left++
        }
    }
    if res == math.MaxInt32 {
        return 0
    } else {
        return res
    }
}
```

### java
```java
class Solution {
    public int minSubArrayLen(int target, int[] nums) {
        int len = nums.length;
        int res = Integer.MAX_VALUE;
        int left = 0;
        int sum = 0;
        for (int right = 0; right < len; right++) {
            sum += nums[right];
            while (sum >= target) {
                int subLen = right - left + 1;
                if (subLen < res) {
                    res = subLen;
                }
                sum -= nums[left];
                left++;
            }
        }
        return res == Integer.MAX_VALUE ? 0 : res;
    }
}
```
