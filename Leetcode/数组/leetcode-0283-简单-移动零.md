# 0283. 移动零

# 题目
给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。

请注意 ，必须在不复制数组的情况下原地对数组进行操作。

https://leetcode.cn/problems/move-zeroes/description/

提示：
- 1 <= nums.length <= 10^4
- -2^31 <= nums[i] <= 2^31 - 1

# 示例
```
示例 1：

输入: nums = [0,1,0,3,12]
输出: [1,3,12,0,0]
```
```
示例 2：

输入: nums = [0]
输出: [0]
```

# 解析

## 双指针
这道题目，使用暴力的解法，可以两层 for 循环，模拟数组删除元素（也就是向前覆盖）的过程。

双指针法在数组移除元素中，可以达到 O(n) 的时间复杂度。

相当于对整个数组移除元素 0，然后 slowIndex 之后都是移除元素 0 的冗余元素，把这些元素都赋值为 0 就可以了。

![](./images/leetcode-0283-题解.gif)

# 代码

### php
```php
class LeetCode0283 {

    public function moveZeroes(&$nums) {
        $len = count($nums);
        $slow = $fast = 0;
        while ($fast < $len) {
            if ($nums[$fast]) {
                $nums[$slow++] = $nums[$fast];
            }
            $fast++;
        }
        // 将slow之后的冗余元素赋值为0
        for ($i = $slow; $i < $len; $i++) {
            $nums[$i] = 0;
        }
    }
    
}
```

### go
```go
func moveZeroes(nums []int)  {
    size := len(nums)
    slow := 0
    for fast := 0; fast < size; fast++ {
        if nums[fast] != 0 {
            nums[slow] = nums[fast]
            slow++
        }
    }
    for i := slow; i < size; i++ {
        nums[i] = 0
    }
}
```

### java
```java
public void moveZeroes(int[] nums) {
    int fast = 0, slow = 0;
    while (fast < nums.length) {
        if (nums[fast] != 0) {
            nums[slow++] = nums[fast];
        }
        fast++;
    }
    for (int i = slow; i < nums.length; i++) {
        nums[i] = 0;
    }
}
```
