# 0560. 和为 K 的子数组


# 题目
给你一个整数数组 nums 和一个整数 k ，请你统计并返回 该数组中和为 k 的子数组的个数 。

子数组是数组中元素的连续非空序列。

提示：
- 1 <= nums.length <= 20000
- -1000 <= nums[i] <= 1000
- -10^7 <= k <= 10^7

https://leetcode.cn/problems/subarray-sum-equals-k/description/

# 示例
```
示例 1：

输入：nums = [1,1,1], k = 2
输出：2
```
```
示例 2：

输入：nums = [1,2,3], k = 3
输出：2
```

# 解析

## 前缀和 + 哈希表
这是一道经典的前缀和运用题。

统计以每一个 nums[i] 为结尾，和为 k 的子数组数量即是答案。

我们可以预处理前缀和数组 sum（前缀和数组下标默认从 1 开始），对于求解以某一个 nums[i] 为结尾的，和为 k 的子数组数量，
本质上是求解在 [0, i] 中，sum 数组中有多少个值为 sum[i + 1] − k 的数，这可以在遍历过程中使用「哈希表」进行同步记录。

# 代码

### php
```php
class Leetcode0560 {

    function subarraySum($nums, $k) {
        $res = 0;

        $len = count($nums);
        $sum =  array_fill(0, $len + 1, 0);
        for ($i = 1; $i <= $len; $i++) {
            $sum[$i] = $sum[$i - 1] + $nums[$i - 1];
        }
        
        $map = [];
        $map[0] = 1;
        for ($i = 1; $i <= $len; $i++) {
            $diff = $sum[$i] - $k;
            if (isset($map[$diff])) {
                $res += $map[$diff];
            }
            $map[$sum[$i]]++;
        }
        
        return $res;
    }
}
```

### java
```java
 public int subarraySum(int[] nums, int k) {
    int len = nums.length, res = 0;
    int[] sum = new int[len + 1];
    for (int i = 1; i <= len; i++) {
        sum[i] = sum[i - 1] + nums[i - 1];
    }

    Map<Integer, Integer> map = new HashMap<>();
    map.put(0, 1);
    for (int i = 1; i <= len; i++) {
        int diff = sum[i] - k;
        res += map.getOrDefault(diff, 0);
        map.put(sum[i], map.getOrDefault(sum[i], 0) + 1);
    }

    return res;
}
```

### go
```go
func subarraySum(nums []int, k int) int {
    length, res := len(nums), 0
    sum := make([]int, length + 1)
    for i := 1; i <= length; i++ {
        sum[i] = sum[i - 1] + nums[i - 1]
    }

    myMap := make(map[int]int)
    myMap[0] = 1
    for i := 1; i <= length; i++ {
        diff := sum[i] - k
        if val, ok := myMap[diff]; ok {
            res += val
        }
        if _, ok := myMap[sum[i]]; ok {
            myMap[sum[i]] += 1
        } else {
            myMap[sum[i]] = 1
        }
    }

    return res
}
```
