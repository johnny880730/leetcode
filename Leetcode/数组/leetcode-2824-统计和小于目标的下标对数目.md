# 2824. 统计和小于目标的下标对数目


# 题目
给你一个下标从 0 开始长度为 n 的整数数组 nums 和一个整数 target ，请你返回满足 0 <= i < j < n 且 nums[i] + nums[j] < target 的下标对 (i, j) 的数目。

https://leetcode.cn/problems/count-pairs-whose-sum-is-less-than-target/

提示：
- 1 <= nums.length == n <= 50
- -50 <= nums[i], target <= 50

# 示例
```
输入：nums = [-1,1,2,3,1], target = 2
输出：3
解释：总共有 3 个下标对满足题目描述：
- (0, 1) ，0 < 1 且 nums[0] + nums[1] = 0 < target
- (0, 2) ，0 < 2 且 nums[0] + nums[2] = 1 < target 
- (0, 4) ，0 < 4 且 nums[0] + nums[4] = 0 < target
注意 (0, 3) 不计入答案因为 nums[0] + nums[3] 不是严格小于 target 。
```
```
输入：nums = [-6,2,5,-2,-7,-1,3], target = -2
输出：10
解释：总共有 10 个下标对满足题目描述：
- (0, 1) ，0 < 1 且 nums[0] + nums[1] = -4 < target
- (0, 3) ，0 < 3 且 nums[0] + nums[3] = -8 < target
- (0, 4) ，0 < 4 且 nums[0] + nums[4] = -13 < target
- (0, 5) ，0 < 5 且 nums[0] + nums[5] = -7 < target
- (0, 6) ，0 < 6 且 nums[0] + nums[6] = -3 < target
- (1, 4) ，1 < 4 且 nums[1] + nums[4] = -5 < target
- (3, 4) ，3 < 4 且 nums[3] + nums[4] = -9 < target
- (3, 5) ，3 < 5 且 nums[3] + nums[5] = -3 < target
- (4, 5) ，4 < 5 且 nums[4] + nums[5] = -8 < target
- (4, 6) ，4 < 6 且 nums[4] + nums[6] = -4 < target
```

# 解析

## 双指针
根据题目要求，只需找到满足要求的两个不同的下标对 (i, j) 即可。设当前数组的长度为 n，将数组进行排序后，对于给定的下标 i，
只需要找到连续子数组 nums[i + 1, ⋯, n − 1 中满足 nums[i] + nums[j] < target 的下标 j 的数目。

如果 i < j 且满足 nums[i] + nums[j] < target  时，则此时 i 与区间 [i + 1, j ] 之间的下标都能组成合法的下标对，
可以参考题目「三数之和」中的「双指针」思想，每次固定左 i 时，此时 nums[j] 需要缩小，j 需要往前移动，直到找到满足 
nums[i] + nums[j] < target 为止，此时满足题意且含有下标 i 的下标对数目为 j − i，按照上述策略找到所有符合要求的下标对即可。

时间复杂度：O(nlogn)，其中 n 表示数组 nums 的长度。排序需要的时间复为 O(nlogn)，双指针遍历需要的时间为 O(n)，因此总的时间复杂度为 O(nlogn)。

空间复杂度：O(logn)，其中 n 表示数组 nums 的长度。排序需要的空间为 O(logn)，除此以外不需要额外的空间。


# 代码

### php
```php
class Leetcode2824 {

    function countPairs($nums, $target) {
        sort($nums);
        $res = 0;
        $len = count($nums);
        for ($i = 0, $j = $len - 1; $i < $j; $i++) {
            while($i < $j && $nums[$i] + $nums[$j] >= $target) {
                $j--;
            }
            $res += $j - $i;
        }
        return $res;

    }
}
```

### java
```java
public int countPairs(List<Integer> nums, int target) {
    Collections.sort(nums);
    int res = 0;
    for (int i = 0, j = nums.size() - 1; i < j; i++) {
        while (i < j && nums.get(i) + nums.get(j) >= target) {
            j--;
        }
        res += j - i;
    }
    return res;
}
```
### go
```go
func countPairs(nums []int, target int) int {
    sort.Ints(nums)
    res := 0
    for i, j := 0, len(nums) - 1; i < j; i++ {
        for i < j && nums[i] + nums[j] >= target {
            j--
        }
        res += j - i
    }
    return res
}
```
