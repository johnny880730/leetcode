# 2103. 环和杆


# 题目
总计有 n 个环，环的颜色可以是红、绿、蓝中的一种。这些环分别穿在 10 根编号为 0 到 9 的杆上。

给你一个长度为 2n 的字符串 rings ，表示这 n 个环在杆上的分布。rings 中每两个字符形成一个 颜色位置对 ，用于描述每个环：
- 第 i 对中的 第一个 字符表示第 i 个环的 颜色（'R'、'G'、'B'）。
- 第 i 对中的 第二个 字符表示第 i 个环的 位置，也就是位于哪根杆上（'0' 到 '9'）。

例如，"R3G2B1" 表示：共有 n == 3 个环，红色的环在编号为 3 的杆上，绿色的环在编号为 2 的杆上，蓝色的环在编号为 1 的杆上。

找出所有集齐 全部三种颜色 环的杆，并返回这种杆的数量。

提示：
- 

# 示例
```
输入：rings = "B0B6G0R6R0R6G9"
输出：1
解释：
- 编号 0 的杆上有 3 个环，集齐全部颜色：红、绿、蓝。
- 编号 6 的杆上有 3 个环，但只有红、蓝两种颜色。
- 编号 9 的杆上只有 1 个绿色环。
因此，集齐全部三种颜色环的杆的数目为 1 。
```
```
输入：rings = "B0R0G0R9R0B0G0"
输出：1
解释：
- 编号 0 的杆上有 6 个环，集齐全部颜色：红、绿、蓝。
- 编号 9 的杆上只有 1 个红色环。
因此，集齐全部三种颜色环的杆的数目为 1 。
```
```
输入：rings = "G4"
输出：0
解释：
只给了一个环，因此，不存在集齐全部三种颜色环的杆。
```

# 解析

## 模拟
可以遍历字符串中的每个颜色位置对，来模拟套环的过程。

对于每个杆，只在意它上面有哪些颜色的环，而不在意具体的数量。因此可以用一个大小为 10 × 3 的二维数组来维护十个杆子的状态。

在数组的第二维，下标 0, 1, 2 的数值分别表示是否有红、绿、蓝颜色的环，值为 1 则表示有，值为 0 则表示没有。

每次遇到一个环，就修改对应杆相应颜色的状态为 1。最后遍历所有的杆，统计三个颜色状态都为 1 的杆的个数，并返回该个数作为答案。




# 代码

### php
```php
class Leetcode2103 {

    function countPoints($rings) {
            $colorMap = ['R' => 0, 'G' => 1, 'B' => 2];
            $arr = array_fill(0, 10, array_fill(0, 3, 0));
            $len = strlen($rings);
            for ($i = 0; $i < $len; $i += 2) {
                $color = $rings[$i];
                $poleNum = $rings[$i + 1];
                $arr[$poleNum][$colorMap[$color]] = 1;
            }
            $res = 0;
            for ($i = 0; $i < 10; $i++) {
                if ($arr[$i][0] > 0 && $arr[$i][1] > 0 && $arr[$i][2] > 0) {
                    $res++;
                }
            }
            
            return $res;
        }
    }
}
```

### java
```java
public int countPoints(String rings) {
    int[][] arr = new int[10][3];
    int len = rings.length();
    for (int i = 0; i < len; i += 2) {
        char color = rings.charAt(i);
        int poleNum = rings.charAt(i + 1) - '0';

        int idx = 0;
        if (color == 'R') {
            idx = 2;
        } else if (color == 'G') {
            idx = 1;
        }

        arr[poleNum][idx] = 1;
    }
    int res = 0;
    for (int i = 0; i < 10; i++) {
        if (arr[i][0] > 0 && arr[i][1] > 0 && arr[i][2] > 0) {
            res++;
        }
    }
    
    return res;
}
```
