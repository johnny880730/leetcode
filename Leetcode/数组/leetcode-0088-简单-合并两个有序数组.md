# 88. 合并两个有序数组

# 题目
给你两个有序整数数组 nums1 和 nums2，请你将 nums2 合并到 nums1 中，使 nums1 成为一个有序数组。

初始化 nums1 和 nums2 的元素数量分别为 m 和 n 。你可以假设 nums1 的空间大小等于 m + n，这样它就有足够的空间保存来自 nums2 的元素。

https://leetcode.cn/problems/merge-sorted-array/

进阶：你可以设计实现一个时间复杂度为 O(m + n) 的算法解决此问题吗？

提示：
- nums1.length == m + n
- nums2.length == n
- 0 <= m, n <= 200
- 1 <= m + n <= 200
- -10^9 <= nums1[i], nums2[j] <= 10^9


# 示例
```
输入：nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
输出：[1,2,2,3,5,6]
```
```
输入：nums1 = [1], m = 1, nums2 = [], n = 0
输出：[1]
```

# 解析
设置个指针指向 nums1 的最后，从后往前比较 num1 的有效区域和 nums2 的有效区域，谁大就往里填。相等的话填谁的都行。有一个越界了就都停止。

# 代码

### php
```php
class LeetCode0088
{
    function merge(&$nums1, $m, $nums2, $n) {
        $index = count($nums1) - 1;
        $m--;
        $n--;
        while ($m >= 0 && $n >= 0) {
            if ($nums1[$m] >= $nums2[$n]) {
                $nums1[$index--] = $nums1[$m--];
            } else {
                $nums1[$index--] = $nums2[$n--];
            }
        }
        while ($m >= 0) {
            $nums1[$index--] = $nums1[$m--];
        }
        while ($n >= 0) {
            $nums1[$index--] = $nums2[$n--];
        }
    }
}
```

### java
```java
class LeetCode0088 {

    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int idx = nums1.length - 1;
        m--;
        n--;
        while (m >= 0 && n >= 0) {
            if (nums1[m] >= nums2[n]) {
                nums1[idx--] = nums1[m--];
            } else {
                nums1[idx--] = nums2[n--];
            }
        }
        while (m >= 0) {
            nums1[idx--] = nums1[m--];
        }
        while (n >= 0) {
            nums1[idx--] = nums2[n--];
        }
    }
}
```

### go
```go
func merge(nums1 []int, m int, nums2 []int, n int)  {
    idx := len(nums1) - 1
	m--
	n--
    for m >= 0 && n >= 0 {
        if nums1[m] >= nums2[n] {
            nums1[idx] = nums1[m]
			m--
        } else {
            nums1[idx] = nums2[n]
            n--
		}
        idx--
    }
    for m >= 0 {
        nums1[idx] = nums1[m]
        m--
        idx--
    }
    for n >= 0 {
        nums1[idx] = nums2[n]
        n--
        idx--
    }
}
```
