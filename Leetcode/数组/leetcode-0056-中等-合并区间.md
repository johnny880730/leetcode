# 0056. 合并区间

# 题目
以数组 intervals 表示若干个区间的集合，其中单个区间为 intervals[i] = [starti, endi] 。
请你合并所有重叠的区间，并返回一个不重叠的区间数组，该数组需恰好覆盖输入中的所有区间。

提示：
- 1 <= intervals.length <= 10000
- intervals[i].length == 2
- 0 <= starti <= endi <= 10000

https://leetcode.cn/problems/merge-intervals/description/

# 示例：
```
输入：intervals = [[1,3],[2,6],[8,10],[15,18]]
输出：[[1,6],[8,10],[15,18]]
解释：区间 [1,3] 和 [2,6] 重叠, 将它们合并为 [1,6].
```

```
输入：intervals = [[1,4],[4,5]]
输出：[[1,5]]
解释：区间 [1,4] 和 [4,5] 可被视为重叠区间。
```


# 解析

## 贪心

先根据左边界从小到大排序，以第 0 个集合作为基础，初始化 start = intervals[0][0], end = intervals[0][1]。然后从第 1 个集合开始遍历，
- 如果左边界比 end 小，意味着之前的范围能把当前的吞掉全部或者部分。之后看右边界是否能把 end 给推高
    - 如果不能推高，这个集合可以不用管，继续遍历下一个集合。
    - 如果右边界可以推高 end 就更新 end。
- 如果左边界比 end 大，那么生成之前的 [start, end] 范围生成为答案的一部分，将新的左右边界更新为 start、end

注意上面的流程里，将 [start, end] 加入结果的时机是当前的左边界大于 end，意味着最后一对 [start, end] 没有加入结果，需要将这对结果加入结果，
不然会遗漏。

---

例如：intervals = [ [1, 5], [1, 4], [1, 6], [2, 4], [3, 9], [12, 14] ]，初始化 start = intervals[0][0] = 1, end = intervals[0][1] = 5：
- 遍历到 [1, 4]，左边界 1 比 end 小，右边界 4 比 end 小所以无法推高，略过。此时 [start, end] = [1, 5]
- 遍历到 [1, 6]，左边界 1 比 end 小，右边界 6 比 end 大可以推高，更新 end = 6。此时 [start, end] = [1, 6]
- 遍历到 [2, 4]，左边界 2 比 end 小，右边界 4 比 end 小所以无法推高，略过。此时 [start, end] = [1, 6]
- 遍历到 [3, 9]，左边界 3 比 end 小，右边界 9 比 end 大可以推高，更新 end = 9。此时 [start, end] = [1, 9]
- 遍历到 [12, 14]，左边界 12 比 end 大，将原来的 [start ,end] 加入结果并更新 [start, end] = [12, 14]
- 遍历结束，将最后一对 [start, end] 加入结果。因此结果为 [[1, 9], [12, 14]]


# 代码

### php

```php
class LeetCode0056 {
    
    function merge($arr) {
        $res = [];
        $len = count($arr);
        // 根据左边界从小到大排序
        usort($arr, function($a, $b) {
            return $a[0] > $b[0];
        });
        $start = $arr[0][0];
        $end   = $arr[0][1];
        for ($i = 1; $i < $len; $i++) {
            if ($arr[$i][0] <= $end) {
                // 如果左边界 小于等于 end，意味着之前的范围能把当前的吞掉全部或者部分
                // 之后看右边界是否能把 end 给推高
                $end = max($end, $arr[$i][1]);
            } else {
                $res[] = [$start, $end];
                $start = $arr[$i][0];
                $end   = $arr[$i][1];
            }
        }
        
        $res[] = [$start, $end];
        
        return $res;
    }
}
```

### go
```go
func merge(intervals [][]int) [][]int {
    res := [][]int{}
    length := len(intervals)
    if length == 0 {
        return res
    }
    sort.Slice(intervals, func(i, j int) bool {
        return intervals[i][0] < intervals[j][0]
    })
    start := intervals[0][0]
    end := intervals[0][1]
    for i := 1; i < length; i++ {
        if intervals[i][0] > end {
            res = append(res, []int{start, end})
            start = intervals[i][0]
            end = intervals[i][1]
        } else {
            if intervals[i][1] > end {
                end = intervals[i][1]
            }
        }
    }
    res = append(res, []int{start, end})
    return res
}
```

### java
```java
class LeetCode0056 {

    public static class Range {
        int start;
        int end;

        public Range(int s, int e) {
            start = s;
            end = e;
        }
    }

    public static class RangeComparator implements Comparator<Range> {
        @Override
        public int compare(Range o1, Range o2) {
            return o1.start - o2.start;
        }
    }

    public int[][] merge(int[][] intervals) {
        if (intervals.length == 0) {
            return new int[0][0];
        }
        Range[] arr = new Range[intervals.length];
        for (int i = 0; i < intervals.length; i++) {
            arr[i] = new Range(intervals[i][0], intervals[i][1]);
        }
        Arrays.sort(arr, new RangeComparator());
        List<Range> ans = new ArrayList<>();
        int s = arr[0].start;
        int e = arr[0].end;
        for (int i = 1; i < intervals.length; i++) {
            if (arr[i].start <= e) {
                e = Math.max(e, arr[i].end);
            } else {
                ans.add(new Range(s, e));
                s = arr[i].start;
                e = arr[i].end;
            }
        }
        ans.add(new Range(s, e));

        int[][] res = new int[ans.size()][2];
        for (int i = 0; i < ans.size(); i++) {
            res[i] = new int[]{ans.get(i).start, ans.get(i).end};
        }
        return res;
    }
}
```
