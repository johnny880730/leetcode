# 228. 汇总区间

# 题目
给定一个无重复元素的有序整数数组 nums 。

返回 恰好覆盖数组中所有数字 的 最小有序 区间范围列表。也就是说，nums 的每个元素都恰好被某个区间范围所覆盖，并且不存在属于某个范围但不属于 nums 的数字 x 。

列表中的每个区间范围 [a,b] 应该按如下格式输出：
- "a->b" ，如果 a != b
- "a" ，如果 a == b

https://leetcode.cn/problems/summary-ranges/description/

提示：
- 0 <= nums.length <= 20
- 2^31 <= nums[i] <= 2^31 - 1
- nums 中的所有值都 互不相同
- nums 按升序排列


# 示例
```
输入：nums = [0,1,2,4,5,7]
输出：["0->2","4->5","7"]
解释：区间范围是：
[0,2] --> "0->2"
[4,5] --> "4->5"
[7,7] --> "7"
```
```
输入：nums = [0,2,3,4,6,8,9]
输出：["0","2->4","6","8->9"]
解释：区间范围是：
[0,0] --> "0"
[2,4] --> "2->4"
[6,6] --> "6"
[8,9] --> "8->9"
```

# 解析

## 遍历
从数组的位置 0 出发，向右遍历。每次遇到相邻元素之间的差值大于 1 时，就找到了一个区间。遍历完数组之后，就能得到一系列的区间的列表。

在遍历过程中，维护下标 low 和 high 分别记录区间的起点和终点，对于任何区间都有 low ≤ high。

当得到一个区间时，根据 low 和 high 的值生成区间的字符串表示
- 当 low < high 时，区间的字符串表示为 "low→high"
- 当 low = high 时，区间的字符串表示为 "low"

# 代码

### php
```php
class LeetCode0228 {

    function summaryRanges($nums) {
        $res = [];
        $len = count($nums);
        $i = 0;
        while ($i < $len) {
            $low = $high = $i;
            $i++;
            while ($i < $len && $nums[$i] == $nums[$i - 1] + 1) {
                $i++;
            }
            $high = $i - 1;
            $res[] = $low == $high ? strval($nums[$low]) : $nums[$low] . '->' . $nums[$high];
        }

        return $res;
    }

}
```

### go
```go
func summaryRanges(nums []int) []string {
    res := []string{}
    len := len(nums)
    i := 0
    for i < len {
        low, high := i, i
        i++
        for i < len && nums[i] == nums[i - 1] + 1 {
            i++
        }
        high = i - 1

        if low == high {
            res = append(res, strconv.Itoa(nums[low]))
        } else {
            res = append(res, strconv.Itoa(nums[low]) + "->" + strconv.Itoa(nums[high]))
        }
    }

    return res
}
```

### java
```java
class LeetCode0228 {

    public List<String> summaryRanges(int[] nums) {
        List<String> res = new ArrayList<>();
        int len = nums.length;
        int i = 0;
        while (i < len) {
            int low = i, high = i;
            i++;
            while (i < len && nums[i] == nums[i - 1] + 1) {
                i++;
            }
            high = i - 1;
            if (low == high) {
                res.add(String.valueOf(nums[low]));
            } else {
                res.add(nums[low] + "->" + nums[high]);
            }
        }

        return res;
    }

}
```