# 0189. 轮转数组

# 题目
给你一个数组，将数组中的元素向右轮转 k 个位置，其中 k 是非负数。

进阶：
- 尽可能想出更多的解决方案，至少有 三种 不同的方法可以解决这个问题。
- 你可以使用空间复杂度为 O(1) 的 原地 算法解决这个问题吗？

提示：
- 1 <= nums.length <= 10^5
- -2^31 <= nums[i] <= 2^31 - 1
- 0 <= k <= 105
 
https://leetcode.cn/problems/rotate-array/description/

# 示例
```
示例 1：

输入: nums = [1,2,3,4,5,6,7], k = 3
输出: [5,6,7,1,2,3,4]
解释:
向右轮转 1 步: [7,1,2,3,4,5,6]
向右轮转 2 步: [6,7,1,2,3,4,5]
向右轮转 3 步: [5,6,7,1,2,3,4]
```

```
示例 2：

输入：nums = [-1,-100,3,99], k = 2
输出：[3,99,-1,-100]
解释: 
向右轮转 1 步: [99,-1,-100,3]
向右轮转 2 步: [3,99,-1,-100]
```

# 解析

## 翻转数组
通过参数 k 将数组分为左右两部分：
1. 整个数组逆序一次
2. 左部分（前 k 个元素，也就是 [0, k - 1] 区间）逆序
3. 右部分（[k, n - 1] 区间）逆序

需要注意的是，本题还有一个小陷阱，题目输入中，如果 k > nums.size 应该怎么办？

举个例子，例如，[1, 2, 3, 4, 5, 6, 7] 如果右移动 15 次的话，是 [7, 1, 2, 3, 4, 5, 6] 。所以其实就是右移 k % nums.size() 次，即：15 % 7 = 1

# 代码

### php
```php
class LeetCode0189 {

    function rotate(&$nums, $k) {
        $len = count($nums);
        $k = $k % $len;
        if ($k == 0) {
            return; 
        }
        $this->_reverse($nums, 0, $len - 1);
        $this->_reverse($nums, 0, $k - 1);
        $this->_reverse($nums, $k, $len - 1);
    }
    
    function _reverse(&$nums, $left, $right) {
        while ($left < $right) {
            $tmp = $nums[$left];
            $nums[$left] = $nums[$right];
            $nums[$right] = $tmp;
            $left++;
            $right--;
        }
    }
    
}
```

### go
```go
func rotate(nums []int, k int)  {
    length := len(nums)
    k = k % length
    if k == 0 {
        return
    }
    _reverse(nums, 0, length - 1)
    _reverse(nums, 0, k - 1)
    _reverse(nums, k, length - 1)
}

func _reverse(nums []int, left int, right int) {
    for left < right {
        nums[left], nums[right] = nums[right], nums[left]
        left++
        right--
    }
}
```

### java
```java
class LeetCode0189
{

    public void rotate(int[] nums, int k) {
        k = k % nums.length;
        _reverse(nums, 0, nums.length - 1);
        _reverse(nums, 0, k - 1);
        _reverse(nums, k, nums.length - 1);
    }

    protected void _reverse(int[] nums, int left, int right) {
        while (left < right) {
            int tmp = nums[left];
            nums[left] = nums[right];
            nums[right] = tmp;
            left++;
            right--;
        }
    }
    
}
```
