# 75. 颜色分类

# 题目
给定一个包含红色、白色和蓝色、共 n 个元素的数组 nums ，原地对它们进行排序，使得相同颜色的元素相邻，并按照红色、白色、蓝色顺序排列。

我们使用整数 0、 1 和 2 分别表示红色、白色和蓝色。

必须在不使用库的sort函数的情况下解决这个问题。

https://leetcode.cn/problems/sort-colors

提示：
- n == nums.length
- 1 <= n <= 300
- nums[i] 为 0、1 或 2

# 示例
```
输入：nums = [2,0,2,1,1,0]
输出：[0,0,1,1,2,2]
```
```
输入：nums = [2,0,1]
输出：[0,1,2]
```


# 解析

## 计数排序

可以统计出数组中 0, 1, 2 的个数，再根据它们的数量，重写整个数组


# 代码

### php

```php
class LeetCode0075 {
    
    // 计数排序
    function sortColors(&$nums) {
        $hash = [0 => 0, 1 => 0, 2 => 0];
        foreach ($nums as $num) {
            $hash[$num]++;
        }
        $idx = 0;
        foreach ($hash as $num => $cnt) {
            for ($i = 0; $i < $cnt; $i++) {
                $nums[$idx++] = $num;
            }
        }
    }
        
}
```

### java
```java
class Leetcode0075 {
    
    public void sortColors(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int n : nums) {
            int v = map.getOrDefault(n, 0);
            v++;
            map.put(n, v);
        }
        int i = 0;
        for (int k: map.keySet()) {
            for (int tmp = 0; tmp < map.get(k); tmp++) {
                nums[i] = k;
                i++;
            }
        }
    }
}
```

### go
```go
func sortColors(nums []int)  {
    color := make([]int, 3)
    for _, v := range nums {
        color[v]++
    }
    i := 0
    for num, cnt := range color {
        for j := 0; j < cnt; j++ {
            nums[i] = num
            i++
        }
    }
}
```
