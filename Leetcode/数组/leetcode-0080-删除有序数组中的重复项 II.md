# 0080. 删除有序数组中的重复项 II

# 题目
给你一个有序数组 nums ，请你【原地】删除重复出现的元素，使得出现次数超过两次的元素只出现两次 ，返回删除后数组的新长度。

不要使用额外的数组空间，你必须在原地修改输入数组 并在使用 O(1) 额外空间的条件下完成。

https://leetcode.cn/problems/remove-duplicates-from-sorted-array-ii

提示：
- 1 <= nums.length <= 3 * 10^4
- -10^4 <= nums[i] <= 10^4
- nums 已按升序排列

# 示例
```
输入：nums = [1,1,1,2,2,3]
输出：5, nums = [1,1,2,2,3]
解释：函数应返回新长度 length = 5, 并且原数组的前五个元素被修改为 1, 1, 2, 2, 3。 不需要考虑数组中超出新长度后面的元素。
```
```
输入：nums = [0,0,1,1,1,1,2,3,3]
输出：7, nums = [0,0,1,1,2,3,3]
解释：函数应返回新长度 length = 7, 并且原数组的前七个元素被修改为 0, 0, 1, 1, 2, 3, 3。不需要考虑数组中超出新长度后面的元素。
```

# 解析

## 双指针
通过移动快指针 fast 来找到新的元素，然后将新元素复制到慢指针 slow 的位置，从而在原地删除重复项。

当找到一个与 nums[slow - k] 不同的元素时，就将新元素复制到 nums[slow]. `



# 代码

### php
```php
class LeetCode0080 {

    function removeDuplicates(&$nums) {
        $k = 2;
        $len = count($nums);
        if ($len <= $k) {
            return $len;
        }

        $slow = $fast = $k;
        while ($fast < $len) {
            if ($nums[$fast] != $nums[$slow - $k]) {
                $nums[$slow] = $nums[$fast];
                $slow++;
            }
            $fast++;
        }
    }

}
```

### java
```java
class LeetCode0080 {
    
    public int removeDuplicates(int[] nums) {
        int k = 2;
        if (nums.length <= k) {
            return nums.length;
        }

        int fast = k, slow = k;
        while (fast < nums.length) {
            if (nums[fast] != nums[slow - k]) {
                nums[slow] = nums[fast];
                slow++;
            }
            fast++;
        }
        return slow;
    }

}
```

### go
```go
func removeDuplicates(nums []int) int {
    k := 2
    length := len(nums)
    if length <= k {
        return length
    }
    fast, slow := k, k
    for fast < length {
        if nums[fast] != nums[slow - k] {
            nums[slow] = nums[fast]
            slow++
        }
        fast++
    }
    return slow
}
```
