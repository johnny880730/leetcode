# 2216. 美化数组的最少删除数


# 题目
给你一个下标从 0 开始的整数数组 nums ，如果满足下述条件，则认为数组 nums 是一个 美丽数组 ：
- nums.length 为偶数
- 对所有满足 i % 2 == 0 的下标 i ，nums[i] != nums[i + 1] 均成立

注意，空数组同样认为是美丽数组。

你可以从 nums 中删除任意数量的元素。当你删除一个元素时，被删除元素右侧的所有元素将会向左移动一个单位以填补空缺，而左侧的元素将会保持 不变 。

返回使 nums 变为美丽数组所需删除的 最少 元素数目。

https://leetcode.cn/problems/minimum-deletions-to-make-array-beautiful/

提示：
- 1 <= nums.length <= 10^5
- 0 <= nums[i] <= 10^5

# 示例
```
输入：nums = [1,1,2,3,5]
输出：1
解释：可以删除 nums[0] 或 nums[1] ，这样得到的 nums = [1,2,3,5] 是一个美丽数组。可以证明，要想使 nums 变为美丽数组，至少需要删除 1 个元素。
```
```
输入：nums = [1,1,2,2,3,3]
输出：2
解释：可以删除 nums[0] 和 nums[5] ，这样得到的 nums = [1,2,2,3] 是一个美丽数组。可以证明，要想使 nums 变为美丽数组，至少需要删除 2 个元素。
```

# 解析
使用变量 cnt 代表已删除的元素个数，由于每次删除元素，剩余元素都会往前移动，因此当前下标为 i − cnt。

处理 nums 过程中，若当前下标为偶数，且与下一位置元素相同，那么当前元素需被删除，令 cnt 自增。

最终数组长度为 n − cnt，若长度为奇数，需要再额外删除结尾元素（cnt 再加一），否则 cnt 即为答案。


# 代码

### php
```php
class Leetcode2216 {

    function minDeletion($nums) {
        $len = count($nums);
        $cnt = 0;
        for ($i = 0; $i < $len; $i++) {
            if (($i - $cnt) % 2 == 0 && $i + 1 < $len && $nums[$i] == $nums[$i + 1]) {
                $cnt++;
            }
        }
        return ($len - $cnt) % 2 == 0 ? $cnt : $cnt + 1;
    }
}
```

### java
```java
public int minDeletion(int[] nums) {
    int len = nums.length;
    int cnt = 0;
    for (int i = 0; i < len; i++) {
        if ((i - cnt) % 2 == 0 && i + 1 < len && nums[i] == nums[i + 1]) {
            cnt++;
        }
    }
    return (len - cnt) % 2 == 0 ? cnt : cnt + 1;
}
```

### go
```go
func minDeletion(nums []int) int {
    cnt := 0
    length := len(nums)
    for i := 0; i < length; i++ {
        if (i - cnt) % 2 == 0 && i + 1 < length && nums[i] == nums[i + 1] {
            cnt++
        }
    }
    if (length - cnt) % 2 == 0 {
        return cnt
    }
    return cnt + 1
}
```
