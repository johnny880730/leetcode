### 1488. 避免洪水泛滥

# 题目
你的国家有无数个湖泊，所有湖泊一开始都是空的。当第 n 个湖泊下雨前是空的，那么它就会装满水。如果第 n 个湖泊下雨前是 满的 ，这个湖泊会发生 洪水 。
你的目标是避免任意一个湖泊发生洪水。

给你一个整数数组 rains ，其中：
- rains[i] > 0 表示第 i 天时，第 rains[i] 个湖泊会下雨。
- rains[i] == 0 表示第 i 天没有湖泊会下雨，你可以选择 一个 湖泊并 抽干 这个湖泊的水。

请返回一个数组 ans ，满足：
- ans.length == rains.length
- 如果 rains[i] > 0 ，那么ans[i] == -1 。
- 如果 rains[i] == 0 ，ans[i] 是你第 i 天选择抽干的湖泊。

如果有多种可行解，请返回它们中的 任意一个 。如果没办法阻止洪水，请返回一个 空的数组 。

请注意，如果你选择抽干一个装满水的湖泊，它会变成一个空的湖泊。但如果你选择抽干一个空的湖泊，那么将无事发生。

https://leetcode.cn/problems/avoid-flood-in-the-city

提示：
- 1 <= rains.length <= 10<sup>5</sup>
- 0 <= rains[i] <= 10<sup>9</sup>

## 示例
```
输入：rains = [1,2,3,4]
输出：[-1,-1,-1,-1]
解释：第一天后，装满水的湖泊包括 [1]
第二天后，装满水的湖泊包括 [1,2]
第三天后，装满水的湖泊包括 [1,2,3]
第四天后，装满水的湖泊包括 [1,2,3,4]
没有哪一天你可以抽干任何湖泊的水，也没有湖泊会发生洪水。
```
```
输入：rains = [1,2,0,0,2,1]
输出：[-1,-1,2,1,-1,-1]
解释：第一天后，装满水的湖泊包括 [1]
第二天后，装满水的湖泊包括 [1,2]
第三天后，我们抽干湖泊 2 。所以剩下装满水的湖泊包括 [1]
第四天后，我们抽干湖泊 1 。所以暂时没有装满水的湖泊了。
第五天后，装满水的湖泊包括 [2]。
第六天后，装满水的湖泊包括 [1,2]。
可以看出，这个方案下不会有洪水发生。同时， [-1,-1,1,2,-1,-1] 也是另一个可行的没有洪水的方案。
```
```
输入：rains = [1,2,0,1,2]
输出：[]
解释：第二天后，装满水的湖泊包括 [1,2]。我们可以在第三天抽干一个湖泊的水。
但第三天后，湖泊 1 和 2 都会再次下雨，所以不管我们第三天抽干哪个湖泊的水，另一个湖泊都会发生洪水。
```

# 解析

&emsp;&emsp;第一次遍历数组 rains，准备一个哈希表 map，键为 rains[i] 也就是某个湖泊，值为一个链表，从前到后记录这个湖泊哪几天下雨

&emsp;&emsp;准备一个 set，初始化为空，存放已经有水的湖泊。第二次遍历数组 rains，遍历时索引为 i。当遍历到第 i 号湖泊时，
如果 set 中已有这个湖泊，说明会发生洪水泛滥，根据题意直接返回空数组即可。

&emsp;&emsp;如果 set 中没有这个湖泊的话，先将这个湖泊放入 set 中，表示此湖泊已经有水，返回结果 res[i] = -1。
然后从 map 中获取这个湖泊对应的表示下雨天数的链表 list。此时这个 list 的头结点必定是当前遍历的 i，将它拿掉，看这个湖泊是否还有其他天数会下雨。
- 如果没有，说明之后无论怎么遍历这个湖泊都不会泛滥，因此可以不用管他。
- 如果存在，拿出第一个放入一个小根堆 minHeap 中。这个小根堆的每个结点存放两个信息：湖泊号（lakeNum）和下个雨天（nextRain），
  按照 nextRain 字段来从小到大排序，那么最顶上的结点就是最接近 i 的下雨天。

&emsp;&emsp;当 rains[i] == 0 也就是抽水的时候，从小根堆顶拿出结点，将结点中包含的 lakeNum 从已经有水的湖泊 set 中去掉，也就是将它抽干了，
这样当这个湖泊下次下雨的时候就不会发生湖水泛滥的情况，结果数组 res[i] = lakeNum


# 代码
```php
class LeetCode1488
{
    public function main($rains)
    {
        print_r($this->avoidFlood($rains));
    }

    function avoidFlood($rains)
    {
        $res = array();
        $len = count($rains);
        // rains[i] = j 第i天轮到j号湖泊下雨
        // key: 某个湖.  val: 这个湖在哪些天降雨
        $map = [];
        for ($i = 0; $i < $len; $i++) {
            if ($rains[$i]) {
                if (!array_key_exists($rains[$i], $map)) {
                    $map[$rains[$i]] = new SplDoublyLinkedList();
                }
                $map[$rains[$i]]->push($i);
            }
        }

        // 某个湖如果有水了，就加入到set里，这里用数组(hash)来模拟set
        $set = array();
        // 小根堆的堆顶表示最先处理的湖是哪个
        $minHeap = new MyCompareHeap();

        for ($i = 0; $i < $len; $i++) {
            if ($rains[$i]) {
                // 没抽干的表中有这个湖泊，无法阻止洪水，返回空数组
                if (array_key_exists($rains[$i], $set)) {
                    return array();
                }
                // 放入到没抽干的湖泊表里
                $set[$rains[$i]] = true;
                // 弹出这个湖泊现在的日期
                $map[$rains[$i]]->shift();
                // 弹出之后如果这个湖还有会下雨的日子，加入到小根堆
                if (!$map[$rains[$i]]->isEmpty()) {
                    $myWork = new MyWork($rains[$i], $map[$rains[$i]]->top());
                    $minHeap->insert($myWork);
                }
                // 题目规定有雨就写 -1
                $res[$i] = -1;
            } else {
                // 这里要抽干某个湖泊的水
                if ($minHeap->isEmpty()) {
                    // 这里晴天如果没有湖泊抽的话貌似写死1
                    $res[$i] = 1;
                } else {
                    $first = $minHeap->extract();
                    unset($set[$first->lakeNum]);
                    $res[$i] = $first->lakeNum;
                }
            }
        }
        return $res;
    }
}

class MyWork
{
    public $lakeNum;
    public $nextRain;

    public function __construct($lakeNum, $nextRain)
    {
        $this->lakeNum = $lakeNum;
        $this->nextRain = $nextRain;
    }
}

class MyCompareHeap extends SplMinHeap
{
    public function compare($obj1, $obj2)
    {
        return $obj2->nextRain - $obj1->nextRain;
    }
}
```
