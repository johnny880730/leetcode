# 0048. 旋转图像

# 题目
给定一个 n × n 的二维矩阵 matrix 表示一个图像。请你将图像顺时针旋转 90 度。

你必须在 原地 旋转图像，这意味着你需要直接修改输入的二维矩阵。请不要 使用另一个矩阵来旋转图像。

https://leetcode.cn/problems/rotate-image/description/

提示：
- n == matrix.length == matrix[i].length
- 1 <= n <= 20
- -1000 <= matrix[i][j] <= 1000

# 示例
```
示例 1：

输入：matrix = [[1,2,3],[4,5,6],[7,8,9]]
输出：[[7,4,1],[8,5,2],[9,6,3]]
```

```
示例 2：

输入：matrix = [[5,1,9,11],[2,4,8,10],[13,3,6,7],[15,14,12,16]]
输出：[[15,13,2,5],[14,3,4,1],[12,6,8,9],[16,7,10,11]]
```

# 解析
每次都是按圈来转 90 度，外圈转不会影响到内圈的结果。所以按照圈旋转，转完往内深入一圈继续转。

这个矩阵肯定是个正方形，左上角的点为 (0, 0)，右下角的点为 (N - 1, N - 1)，每圈转完后，左上角点往右下方移动、右下角点往左上方移动。

转的逻辑：假设左上角的点为 T(pRow, pCol)，右下角的点为 D(qRow, qCol)，这两个点可以形成一个圈。假设这个圈是个 4 × 4 的圈，按照下图将圈上的点分成 3 组：  

![](./images/leetcode-0048-解析.png)

黑色的圈顺时针90度到黑色，红色的转到红色的，橙色的转到橙色的。如果是 4 × 4 的话一圈就这样转好了。推广到一般情况的话，就是 n × n 的圈需要分成 n - 1 组，
每组去转到对应的位置即可。
- 每组的上边的点：matrix[pRow][pCol + i]
- 每组的右边的点：matrix[pRow + i][qCol]
- 每组的下边的点：matrix[qRow][qCol - i]
- 每组的左边的点：matrix[qRow - i][pCol]

# 代码

### php
```php
class LeetCode0048 {
    
    function rotate(&$matrix) {
        $pRow = $pCol = 0;                      //左上角的点
        $qRow = $qCol = count($matrix) - 1;     //右下角的点
        while ($pRow < $qRow) {
            // 左上角点往右下方移动、右下角点往左上方移动
            $this->_rotateEdge($matrix, $pRow++, $pCol++, $qRow--, $qCol--);
        }
    }
    
    protected function _rotateEdge(&$matrix, $pRow, $pCol, $qRow, $qCol){
        $groups = $qCol - $pCol;                                        // 分的组数
        for ($i = 0; $i != $groups; $i++) {                             // i 就是组号
            $tmp = $matrix[$pRow][$pCol + $i];                          // 【上】边的数字暂存
            $matrix[$pRow][$pCol + $i] = $matrix[$qRow - $i][$pCol];    // 【左】边的数字放到【上】边
            $matrix[$qRow - $i][$pCol] = $matrix[$qRow][$qCol - $i];    // 【下】边的数字放到【左】边
            $matrix[$qRow][$qCol - $i] = $matrix[$pRow + $i][$qCol];    // 【右】边的数字放到【下】边
            $matrix[$pRow + $i][$qCol] = $tmp;                          //  暂存的【上】边的数字放到【右】边
        }
    }
}
```

### java
```java
class LeetCode0048 {

    public void rotate(int[][] matrix) {
        int pRow = 0, pCol = 0;
        int qRow = matrix.length - 1, qCol = matrix.length - 1;
        while (pRow < qRow) {
            _rotateEdge(matrix, pRow++, pCol++, qRow--, qCol--);
        }
    }

    protected void _rotateEdge(int[][] matrix, int pRow, int pCol, int qRow, int qCol) {
        int groups = qRow - pRow;
        for (int i = 0; i < groups; i++) {
            int tmp = matrix[pRow][pCol + i];
            matrix[pRow][pCol + i] = matrix[qRow - i][pCol];
            matrix[qRow - i][pCol] = matrix[qRow][qCol - i];
            matrix[qRow][qCol - i] = matrix[pRow + i][qCol];
            matrix[pRow + i][qCol] = tmp;
        }
    }
}
```

### go
```go
func rotate(matrix [][]int)  {
    length := len(matrix)
    pRow, pCol := 0, 0
    qRow, qCol := length - 1, length - 1
    for pRow < qRow {
        _rotateEdge(matrix, pRow, pCol, qRow, qCol)
        pRow++
        pCol++
        qRow--
        qCol--
    }
}

func _rotateEdge(matrix [][]int, pRow int, pCol int, qRow int, qCol int) {
    groups := qCol - pCol
    for i := 0; i < groups; i++ {
        tmp := matrix[pRow][pCol + i]
        matrix[pRow][pCol + i] = matrix[qRow - i][pCol]
        matrix[qRow - i][pCol] = matrix[qRow][qCol - i]
        matrix[qRow][qCol - i] = matrix[pRow + i][qCol]
        matrix[pRow + i][qCol] = tmp
    }
}
```
