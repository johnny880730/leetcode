# 977. 有序数组的平方

# 题目
给你一个按 非递减顺序 排序的整数数组 nums，返回 每个数字的平方 组成的新数组，要求也按 非递减顺序 排序。

提示：
- 1 <= nums.length <= 10^4
- -10^4 <= nums[i] <= 10^4
- nums 已按 非递减顺序 排序

# 实例
```
示例 1：
输入：nums = [-4,-1,0,3,10]
输出：[0,1,9,16,100]
解释：平方后，数组变为 [16,1,0,9,100]
排序后，数组变为 [0,1,9,16,100]
```
```
示例 2：
输入：nums = [-7,-3,2,3,11]
输出：[4,9,9,49,121]
```

# 解析

## 双指针法
数组其实是有序的，只不过负数平方之后可能成为最大数了。

那么数组平方的最大值就在数组的两端，不是最左边就是最右边，不可能是中间。

此时可以考虑双指针法了，i 指向起始位置，j 指向终止位置。

定义一个新数组 res，和原数组 A 一样的大小，定义一个变量 idx 指向 res 数组终止位置。
- 如果 A[i] * A[i] < A[j] * A[j] 那么 result[idx--] = A[j] * A[j]; 。
- 如果 A[i] * A[i] >= A[j] * A[j] 那么 result[idx--] = A[i] * A[i]; 。

![](./images/leetcode-0977.gif)

# 代码

### php
```php
class Leetcode0977 {

    // 双指针
    public function sortedSquares2($nums) {
        $len = count($nums);
        $res = array_fill(0, $len, 0);
        $i = 0;
        $j = $idx = $len - 1;
                
        // 这里循环条件如果写 i < j 的话，会漏掉 i == j 时候指向的那个元素
        while ($i <= $j) {
            if ($nums[$i] * $nums[$i] < $nums[$j] * $nums[$j]) {
                $res[$idx--] =  $nums[$j] * $nums[$j];
                $j--;
            } else {
                $res[$idx--] =  $nums[$i] * $nums[$i];
                $i++;
            }
        }
        return $res;
    }
}
```

### java
```java
class Solution {
    
    public int[] sortedSquares(int[] nums) {
        int i = 0, j = nums.length - 1, idx = nums.length - 1;
        int[] res = new int[nums.length];
        while (i <= j) {
            if (nums[i] * nums[i] < nums[j] * nums[j]) {
                res[idx--] = nums[j] * nums[j];
                j--;
            } else {
                res[idx--] = nums[i] * nums[i];
                i++;
            }
        }
        return res;
    }
}
```

### go
```go
func sortedSquares(nums []int) []int {
    length := len(nums)
    i, j, idx := 0, length - 1, length - 1
    res := make([]int, length)
    for i <= j {
        if nums[i] * nums[i] < nums[j] * nums[j] {
            res[idx] = nums[j] * nums[j]
            j--
        } else {
            res[idx] = nums[i] * nums[i]
            i++
        }
        idx--
    }
    return res
}
```
