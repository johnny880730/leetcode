# 0611. 有效三角形的个数

# 题目
给定一个包含非负整数的数组 nums ，返回其中可以组成三角形三条边的三元组个数。

提示：
- 1 <= nums.length <= 1000
- 0 <= nums[i] <= 1000

https://leetcode.cn/problems/valid-triangle-number/description/

# 示例
```
示例 1:

输入: nums = [2,2,3,4]
输出: 3
解释:有效的组合是: 
2,3,4 (使用第一个 2)
2,3,4 (使用第二个 2)
2,2,3
```
```
示例 2:

输入: nums = [4,2,3,4]
输出: 4
```

# 解析
判断三条边能组成三角形的条件为：
- 任意两边之和大于第三边，任意两边之差小于第三边。
- 三条边长从小到大为 a、b、c，当且仅当 a + b > c 这三条边能组成三角形。

## 双指针
首先对数组排序。

固定最长的一条边，运用双指针扫描
- 如果 nums[l] + nums[r] > nums[i]，同时说明 nums[l + 1] + nums[r] > nums[i], ..., nums[r - 1] + nums[r] > nums[i]，
满足的条件的有 r - l 种，r 左移进入下一轮。
- 如果 nums[l] + nums[r] <= nums[i]，l 右移进入下一轮。
- 枚举结束后，总和就是答案。
- 时间复杂度为 O(n2)O(n^2)O(n

# 代码

### php
```php
class LeetCode0611 {

    /**
     * @param Integer[] $nums
     * @return Integer
     */
    function triangleNumber($nums) {
        sort($nums);
        $len = count($nums);
        $res = 0;
        for ($i = $len - 1; $i >= 2; $i--) {
            $left = 0;
            $right = $i - 1;
            while ($left < $right) {
                if ($nums[$left] + $nums[$right] > $nums[$i]) {
                    $res += $right - $left;
                    $right--;
                } else {
                    $left++;
                }
            }
        }
        return $res;
    }
}
```

### java
```java
class LeetCode0611 {
    
    public int triangleNumber(int[] nums) {
        int res = 0;
        Arrays.sort(nums);
        for (int i = nums.length - 1; i >= 2; i--) {
            int left = 0, right = i - 1;
            while (left < right) {
                if (nums[left] + nums[right] > nums[i]) {
                    res += right - left;
                    right--;
                } else {
                    left++;
                }
            }
        }
        return res;
    }
}
```

### go
```go
func triangleNumber(nums []int) int {
    res := 0
    len := len(nums)
    sort.Ints(nums)
    for i := len - 1; i >= 2; i-- {
        left, right := 0, i - 1
        for left < right {
            if nums[left] + nums[right] > nums[i] {
                res += right - left
                right--
            } else {
                left++
            }
        }
    }
    return res
}
```
