### 914. 卡牌分组

# 题目
给定一副牌，每张牌上都写着一个整数。

此时，你需要选定一个数字 X，使我们可以将整副牌按下述规则分成 1 组或更多组：
- 每组都有 X 张牌。
- 组内所有的牌上都写着相同的整数。

仅当你可选的 X >= 2 时返回 true。

https://leetcode.cn/problems/x-of-a-kind-in-a-deck-of-cards

提示：
- 1 <= deck.length <= 10<sup>4</sup>
- 0 <= deck[i] < 10<sup>4</sup>

## 示例
```
输入：deck = [1,2,3,4,4,3,2,1]
输出：true
解释：可行的分组是 [1,1]，[2,2]，[3,3]，[4,4]
```
```
输入：deck = [1,1,1,2,2,2,3,3]
输出：false
解释：没有满足要求的分组。
```

# 解析

#### 最大公约数 

&emsp;&emsp;由于每一组都有 X 张牌，那么 X 必须是卡牌总数 N 的约数。

&emsp;&emsp;其次，对于写着数字 i 的牌，如果有 count_i 张，由于题目要求「组内所有的牌上都写着相同的整数」，那么 X 也必须是 count_i 的约数，
这个条件是对所有牌中存在的数字 i 成立的，所以我们可以推出，只有当 X 为所有 count_i 的约数，即所有 count_i 最大公约数的约数时，才存在可能的分组。
假设牌中存在的数字为 a、b、c、d、e，那么只有当 X为 gcd(count_a, count_b, count_c, count_d, count_e) 的约数时才满足要求。

&emsp;&emsp;因此我们只要求出所有 count_i 最大公约数 g，判断 g 是否大于等于 2 即可，如果大于等于 2，则满足条件，否则不满足。

# 代码
```php
class LeetCode0914 {

    function hasGroupsSizeX($deck) {
        $hash = array_count_values($deck);
        $g = 0;
        foreach ($hash as $v) {
            $g = $this->gcd($g, $v);
            if ($g == 1) {
                return false;
            }
        }
        return true;
    }

    function gcd($a, $b) {
        return $b > 0 ? $this->gcd($b, $a % $b) : $a;
    }
}
```