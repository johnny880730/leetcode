### 1979. 找出数组的最大公约数

# 题目
给你一个整数数组 nums ，返回数组中最大数和最小数的 最大公约数 。

两个数的 最大公约数 是能够被两个数整除的最大正整数。

https://leetcode.cn/problems/find-greatest-common-divisor-of-array/

提示：
- 2 <= nums.length <= 1000
- 1 <= nums[i] <= 1000


## 示例
```
输入：nums = [2,5,6,9,10]
输出：2
解释：
nums 中最小的数是 2
nums 中最大的数是 10
2 和 10 的最大公约数是 2
```
```
输入：nums = [7,5,6,8,3]
输出：1
解释：
nums 中最小的数是 3
nums 中最大的数是 8
3 和 8 的最大公约数是 1
```
```
输入：nums = [3,3]
输出：3
解释：
nums 中最小的数是 3
nums 中最大的数是 3
3 和 3 的最大公约数是 3
```

# 解析

&emsp;&emsp;首先遍历数组 nums 得到最大值与最小值后，再计算两者的最大公约数即可。

&emsp;&emsp;计算两个整数的最大公约数 gcd(a,b) 的一种常见方法是欧几里得算法，即辗转相除法。其核心部分为：
```
    gcd(a,b)=gcd(b,a mod b).
```

# 代码
```php
class LeetCode1979 {

    function findGCD($nums) {
        $min = PHP_INT_MAX;
        $max = PHP_INT_MIN;
        foreach ($nums as $num) {
            $num < $min && $min = $num;
            $num > $max && $max = $num;
        }
        return $this->getGcd($max, $min);
    }

    function getGcd($a, $b) {
        return $b == 0 ? $a : $this->getGcd($b, $a % $b);
    }
}
```