# 1002. 查找常用字符

# 题目
给定仅有小写字母组成的字符串数组 A，返回列表中的每个字符串中都显示的全部字符（包括重复字符）组成的列表。

例如，如果一个字符在每个字符串中出现 3 次，但不是 4 次，则需要在最终答案中包含该字符 3 次。

你可以按任意顺序返回答案。

https://leetcode.cn/problems/find-common-characters/

提示：
1 <= A.length <= 100
1 <= A[i].length <= 100
A[i][j] 是小写字母


# 示例 
```
输入：["bella","label","roller"]
输出：["e","l","l"]
```
```
输入：["cool","lock","cook"]
输出：["c","o"]
```

# 解析

题目一眼看上去，就是用哈希法，“小写字符”，“出现频率”， 这些关键字都是为哈希法量身定做的啊

首先可以想到的是暴力解法，一个字符串一个字符串去搜，时间复杂度是 O(n ^ m)，n 是字符串长度，m 是有几个字符串。

可以看出这是指数级别的时间复杂度，非常高，而且代码实现也不容易，因为要统计重复的字符，还要适当的替换或者去重。

## 哈希
理解了数组在哈希法中的应用之后，可以来看解题思路了。

整体思路就是统计出搜索字符串里 26 个字符的出现的频率，然后取每个字符频率最小值，最后转成输出格式就可以了。


# 代码

### php
```php
class Leetcode1002 {

    function commonChars($words) {
        $minFreq = array_fill(0, 26, 999);
        foreach ($words as $word) {
            $temp = array_fill(0, 26, 0);
            $len = strlen($word);
            for ($i = 0; $i < $len; $i++) {
                $temp[ord($word[$i]) - ord('a')]++;
            }
            for ($i = 0; $i < 26; $i++) {
                $minFreq[$i] = min($minFreq[$i], $temp[$i]);
            }
        }
        $res = [];
        for ($i = 0; $i < 26; $i++) {
            if ($minFreq[$i]) {
                for ($j = 0; $j < $minFreq[$i]; $j++) {
                    $res[] = chr($i + 97);
                }
            }
        }
        return $res;
    }
}
```

### go
```go
func commonChars(words []string) []string {
    minFreq := make([]int, 26)
	for k := range minFreq {
		minFreq[k] = 9999
	}
	for _, word := range words {
		tmp := make([]int, 26)
		strlen := len(word)
		for i := 0; i < strlen; i++ {
			tmp[int(word[i] - 'a')] ++
		}

		for i := 0; i < 26; i++ {
			minFreq[i] = min(minFreq[i], tmp[i])
		}
	}
	res := make([]string, 0)
	for i := 0; i < 26; i++ {
		if minFreq[i] > 0 {
			for minFreq[i] > 0 {
				res = append(res, string(byte(i) + 'a'))
				minFreq[i]--
			}
		}
	}
	return res
}


func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
```