# 0240. 搜索二维矩阵 II

# 题目
编写一个高效的算法来搜索 m x n 矩阵 matrix 中的一个目标值 target 。该矩阵具有以下特性：
- 每行的元素从左到右升序排列。
- 每列的元素从上到下升序排列。

https://leetcode.cn/problems/search-a-2d-matrix-ii/description/


提示：
- m == matrix.length
- n == matrix[i].length
-  1 <= n, m <= 300
- -10^9 <= matrix[i][j] <= 10^9
- 每行的所有元素从左到右升序排列
- 每列的所有元素从上到下升序排列
- -10^9 <= target <= 10^9


# 示例
```
示例 1：

输入：matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]], target = 5
输出：true
```
```
示例 2：

输入：matrix = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]], target = 20
输出：false
```


# 解析

## Z 字形查找

可以从矩阵 matrix 的右上角 (0, n - 1) 进行搜索。在每一步的搜索过程中，如果位于位置 (x, y)，
那么希望在以 matrix 的左下角为左下角、以 (x, y) 为右上角的矩阵中进行搜索，即行的范围为 [x, m − 1]，列的范围为 [0, y]：
- 如果 matrix[x, y] = target，说明搜索完成；
- 如果 matrix[x, y] > target，由于每一列的元素都是升序排列的，那么在当前的搜索矩阵中，所有位于第 y 列的元素都是严格大于 target 的，
  因此可以将它们全部忽略，即将 y 减少 1；
- 如果 matrix[x, y] < target，由于每一行的元素都是升序排列的，那么在当前的搜索矩阵中，所有位于第 x 行的元素都是严格小于 target 的，
  因此可以将它们全部忽略，即将 x 增加 1。

在搜索的过程中，如果超出了矩阵的边界，那么说明矩阵中不存在 target。

PS：无论是右上角开始还是由左下角开始都是可以的，无非查找方向有所不同罢了


# 代码

### php
```php

class LeetCode0240 {

    // 从右上角开始搜索
    function searchMatrix2($matrix, $target) {
        $i = 0;
        $j = count($matrix[0]) - 1;
        while ($i < count($matrix) && $j >= 0) {
            if ($matrix[$i][$j] < $target) {
                $i++;
            } else if ($matrix[$i][$j] > $target) {
                $j--;    
            } else {
                return true;
            }
        }

        return false;
    }
    
}
```

### go
```go
func searchMatrix(matrix [][]int, target int) bool {
    i, j := 0, len(matrix[0]) - 1
    for i < len(matrix) && j >= 0 {
        if matrix[i][j] < target {
            i++
        } else if matrix[i][j] > target {
            j--
        } else {
            return true
        }
    }
    return false
}
```

### java
```java
class LeetCode0240 
{

    public boolean searchMatrix(int[][] matrix, int target) {
        int i = 0, j = matrix[0].length - 1;
        while (i < matrix.length && j >= 0) {
            if (matrix[i][j] < target) {
                i++;
            } else if (matrix[i][j] > target) {
                j--;
            } else {
                return true;
            }
        }
        return false;
    }
    
}
```
