# 0074. 搜索二维矩阵

# 题目
编写一个高效的算法来判断 m x n 矩阵中，是否存在一个目标值。该矩阵具有如下特性：

每行中的整数从左到右按升序排列。
每行的第一个整数大于前一行的最后一个整数。

https://leetcode.cn/problems/search-a-2d-matrix/description/

提示：
- m == matrix.length
- n == matrix[i].length
- 1 <= m, n <= 100 
- -10<sup>4</sup> <= matrix[i][j], target <= 10<sup>4</sup>

# 示例
```
输入：matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 13
输出：false
```
```
输入：matrix = [[1,3,5,7],[10,11,16,20],[23,30,34,60]], target = 3
输出：true
```


# 解析

## 二分
若将矩阵每一行拼接在上一行的末尾，则会得到一个升序数组，我们可以在该数组上二分找到目标元素。

代码实现时，可以二分升序数组的下标，将其映射到原矩阵的行和列上：
- 比如实例 1 的矩阵，如果拉平成以为就是 0 ~ 11
- 当前 idx 为 9，实际是第三行第二列元素，对应的索引就是 matrix[ 9 // 4 ][ 9 % 4 ] = matrix[2][1]
 
也就是设 n = matrix[0].length，对应的索引就是 matrix[ idx // n][ idx % n]




# 代码
### php
```php
class LeetCode0074 {

    function searchMatrix($matrix, $target) {
        $m = count($matrix);
        $n = count($matrix[0]);
        $i = 0;
        $j = $m * $n - 1;
        while ($i <= $j) {
            $mid = $i + (($j - $i) >> 1);
            $x = $matrix[floor($mid / $n)][$mid % $n];
            if ($x > $target) {
                $j = $mid - 1;
            } else if ($x < $target) {
                $i = $mid + 1;
            } else {
                return true;
            }
        }
        return false;
    }
}
```

### go
```go
func searchMatrix(matrix [][]int, target int) bool {
    m, n := len(matrix), len(matrix[0])
	i, j := 0, m * n - 1
	for i <= j {
		mid := i + ((j - i) >> 1);
		x := matrix[mid / n][mid % n]
		if x > target {
		    j = mid - 1	
        } else if x < target {
		    i = mid + 1	
        } else {
		    return true	
        }
    }
	return false;
}
```

### java
```java
class LeetCode0074 {
    public boolean searchMatrix(int[][] matrix, int target) {
        int m = matrix.length, n = matrix[0].length;
        int i = 0, j = m * n - 1;
        while (i <= j) {
            int mid = i + ((j - i) >> 1);
            int x = matrix[mid / n][mid % n];
            if (x > target) {
                j = mid - 1;
            } else if (x < target) {
                i = mid + 1;
            } else {
                return true;
            }
        }
        return false;
    }
}
```