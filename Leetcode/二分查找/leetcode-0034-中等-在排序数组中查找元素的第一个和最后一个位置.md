# 0034. 在排序数组中查找元素的第一个和最后一个位置

# 题目
给定一个按照升序排列的整数数组 nums，和一个目标值 target。找出给定目标值在数组中的开始位置和结束位置。

如果数组中不存在目标值 target，返回[-1, -1]。

进阶：

你可以设计并实现时间复杂度为 O(logn) 的算法解决此问题吗？

https://leetcode.cn/problems/find-first-and-last-position-of-element-in-sorted-array/description/

提示：
- 0 <= nums.length <= 10000
- -10^9 <= nums[i] <= 10^9
- nums 是一个非递减数组
- -10^9 <= target <= 10^9


# 示例
```
输入：nums = [5,7,7,8,8,10], target = 8
输出：[3,4]
```
```
输入：nums = [5,7,7,8,8,10], target = 6
输出：[-1,-1]
```
```
输入：nums = [], target = 0
输出：[-1,-1]
```

# 解析

## 二分

寻找 target 在数组里的左右边界，有如下三种情况：
- target 在数组范围的右边或者左边，例如数组 {3, 4, 5}，target为 2；或者数组 {3, 4, 5}, target 为 6，此时应该返回 {-1, -1}
- target 在数组范围中，且数组中不存在 target，例如数组 {3,6,7}, target 为 5，此时应该返回 {-1, -1}
- target 在数组范围中，且数组中存在 target，例如数组 {3,6,7}, target 为 6，此时应该返回 {1, 1}

接下来，采用二分法来去寻找左右边界。

先来寻找右边界，二分查找中什么时候用 while (left <= right)，有什么时候用 while (left < right)，其实只要清楚循环不变量，很容易区分两种写法。

这里采用 while (left <= right)的写法，区间定义为 [left, right]，即左闭右闭的区间。



# 代码

### php
```php
class LeetCode0034 {

    function searchRange($nums, $target) {
        $left = 0;
        $right = count($nums) - 1;
        $first = $last = -1;
        
        // 找第一个等于 target 的位置
        while ($left <= $right) {
            $mid = $left + (($right - $left) >> 1);
            if ($nums[$mid] == $target) {
                $first = $mid;
                $right = $mid - 1;      // 重点
            } else if ($nums[$mid] > $target) {
                $right = $mid - 1;
            } else {
                $left = $mid + 1;
            }
        }
        
        // 找最后一个等于target 的位置
        $left = 0;
        $right = count($nums) - 1;
        while ($left <= $right) {
            $mid = $left + (($right - $left) >> 1);
            if ($nums[$mid] == $target) {
                $last = $mid;
                $left = $mid + 1;       // 重点
            } else if ($nums[$mid] > $target) {
                $right = $mid - 1;
            } else {
                $left = $mid + 1;
            }
        }
        
        return [$first, $last];
    }
}
```

### go
```go
func searchRange(nums []int, target int) []int {
    left, right := 0, len(nums) - 1
	first, last := -1, -1
	for left <= right {
	    mid := left + ((right -left) >> 1)
		if nums[mid] == target {
		    first = mid
			right = mid - 1
        } else if nums[mid] > target {
		    right = mid - 1	
        } else {
		    left = mid + 1	
        }
    }
	
	left, right = 0, len(nums) - 1
    for left <= right {
        mid := left + ((right -left) >> 1)
        if nums[mid] == target {
            last = mid
            left = mid + 1
        } else if nums[mid] > target {
            right = mid - 1
        } else {
            left = mid + 1
        }
    }
	
	return []int{first, last}
} 

```

### java
```java
class LeetCode0034 {

    public int[] searchRange(int[] nums, int target) {
        int left = 0, right = nums.length - 1;
        int first = -1, last = -1;
        while (left <= right) {
            int mid = left + ((right -left) >> 1);
            if (nums[mid] == target) {
                first = mid;
                right = mid - 1;
            } else if (nums[mid] > target) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }

        left = 0;
        right = nums.length - 1; 
        while (left <= right) {
            int mid = left + ((right -left) >> 1);
            if (nums[mid] == target) {
                last = mid;
                left = mid + 1;
            } else if (nums[mid] > target) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        
        return new int[]{first, last};
    }

}
```
