# 0033. 搜索旋转排序数组

# 题目
整数数组 nums 按升序排列，数组中的值 互不相同 。

在传递给函数之前，nums 在预先未知的某个下标 k（0 <= k < nums.length）上进行了 旋转，
使数组变为 [nums[k], nums[k + 1], ..., nums[n - 1], nums[0], nums[1], ..., nums[k - 1]]（下标 从 0 开始 计数）。
例如， [0, 1, 2, 4, 5, 6, 7] 在下标 3 处经旋转后可能变为 [4, 5, 6, 7, 0, 1, 2] 。

给你旋转后的数组 nums 和一个整数 target ，如果 nums 中存在这个目标值 target ，则返回它的下标，否则返回 -1 。

进阶：你可以设计一个时间复杂度为 O(logN) 的解决方案吗？

https://leetcode.cn/problems/search-in-rotated-sorted-array/description/

提示：
- 1 <= nums.length <= 5000
- -10000 <= nums[i] <= 10000
- nums 中的每个值都 **独一无二**
- 题目数据保证 nums 在预先未知的某个下标上进行了旋转
- -10000 <= target <= 10000

# 示例
```
输入：nums = [4,5,6,7,0,1,2], target = 0
输出：4
```
```
输入：nums = [4,5,6,7,0,1,2], target = 3
输出：-1
```
```
输入：nums = [1], target = 0
输出：-1
```

# 解析

## 二分查找
题目要求 O(logN) 的时间复杂度，基本可以断定本题是需要使用二分查找，怎么分是关键。

由于题目说数字了无重复，举个例子 nums = [1, 2, 3, 4, 5, 6, 7]，可以大致分为两类：
- 第一类 [2, 3, 4, 5, 6, 7, 1] 这种，也就是 nums[left] <= nums[mid]。此例子中就是 2 <= 5。
  - 这种情况下，前半部分有序。因此如果 nums[left] <= target < nums[mid]，则在前半部分找，否则去后半部分找。
- 第二类 [6, 7, 1, 2, 3, 4, 5] 这种，也就是 nums[left] > nums[mid]。此例子中就是 6 > 2。
  - 这种情况下，后半部分有序。因此如果 nums[mid] < target <= nums[right]，则在后半部分找，否则去前半部分找。

# 代码

### php
```php
class LeetCode0033 {

    function search($nums, $target) {
        $len = count($nums);
        $left = 0;
        $right = $len - 1;
        while ($left <= $right) {
            $mid = $left + (($right - $left) >> 1);
            if ($nums[$mid] == $target) {
                return $mid;
            }
            if ($nums[$left] <= $nums[$mid]) {
                // 前半部分有序，注意这里是小于等于
                if ($nums[$left] <= $target && $target < $nums[$mid]) {
                    // target 在左半部分
                    $right = $mid - 1;
                } else {
                    $left = $mid + 1;
                }
            } else {
                if ($nums[$mid] < $target && $target <= $nums[$right]) {
                    $left = $mid + 1;
                } else {
                    $right = $mid - 1;
                }
            }
        }

        return -1;
    }
}
```

### java
```java
class LeetCode0033 {

    public int search(int[] nums, int target) {
        int left = 0, right = nums.length - 1;
        while (left <= right) {
            int mid = left + ((right - left) >> 1);
            if (nums[mid] == target) {
                return mid;
            }

            if (nums[left] <= nums[mid]) {
                if (nums[left] <= target && target < nums[mid]) {
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            } else {
                if (nums[mid] < target && target <= nums[right]) {
                    left = mid + 1;
                } else {
                    right = mid - 1;
                }
            }
        }

        return -1;
    }
}
```

### go
```go
func search(nums []int, target int) int {
    left, right := 0, len(nums) - 1
    for left <= right {
        mid := left + ((right - left) >> 1)
        if nums[mid] == target {
            return mid
        }
    
        if (nums[left] <= nums[mid]) {
            if (nums[left] <= target && target < nums[mid]) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        } else {
            if (nums[mid] < target && target <= nums[right]) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
    }
    return -1
}
```
