# 0162. 寻找峰值

# 题目
峰值元素是指其值严格大于左右相邻值的元素。

给你一个整数数组 nums，找到峰值元素并返回其索引。数组可能包含多个峰值，在这种情况下，返回 任何一个峰值 所在位置即可。

你可以假设 nums[-1] = nums[n] = -∞ 。

你必须实现时间复杂度为 O(logn) 的算法来解决此问题。

https://leetcode.cn/problems/find-peak-element/description/

提示：
- 1 <= nums.length <= 1000
- -2^31 <= nums[i] <= 2^31 - 1
- 对于所有有效的 i 都有 nums[i] != nums[i + 1]


# 示例
```
输入：nums = [1,2,3,1]
输出：2
解释：3 是峰值元素，你的函数应该返回其索引 2。
```
```
输入：nums = [1,2,1,3,5,6,4]
输出：1 或 5 
解释：你的函数可以返回索引 1，其峰值元素为 2；或者返回索引 5， 其峰值元素为 6。
```


# 解析

## 二分查找
首先注意题目条件，在题目描述中出现了 nums[-1] = nums[n] = -∞，这就代表着 只要数组中存在一个元素比相邻元素大，那么沿着它一定可以找到一个峰值

根据上述结论，就可以使用二分查找找到峰值
- 查找时，左指针 l，右指针 r，以其保持左右顺序为循环条件
- 根据左右指针计算中间位置 m，并比较 m 与 m + 1 的值：
   - 如果 m 较大，则左侧存在峰值，r = m
   - 如果 m + 1 较大，则右侧存在峰值，l = m + 1

# 代码

### php
```php

class LeetCode0162 {

    function findPeakElement($nums) {
        $len = count($nums);
        if ($len < 2) {
            return 0;
        }
        if ($nums[0] > $nums[1]) {
            return 0;
        }
        if ($nums[$len - 1] > $nums[$len - 2]) {
            return $len - 1;
        }
        $left = 0;
        $right = count($nums) - 1;
        while ($left < $right) {
            $mid = $left + (($right - $left) >> 1);
            if ($nums[$mid - 1] < $nums[$mid] && $nums[$mid] > $nums[$mid + 1]) {
                return $mid;
            } else if ($nums[$mid] > $nums[$mid + 1]) {
                $right = $mid;
            } else {
                $left = $mid + 1;
            }
        }
        return $left;
    }
}
```

### go
```go
func findPeakElement(nums []int) int {
    length := len(nums)
    if length < 2 {
        return 0
    }
    if nums[0] > nums[1] {
        return 0
    }
    if nums[length - 1] > nums[length - 2] {
        return length - 1
    }
    left, right := 0, length - 1
    for left < right {
        mid := left + ((right - left) >> 1)
        if nums[mid - 1] < nums[mid] && nums[mid] > nums[mid + 1] {
            return mid
        } else if nums[mid] > nums[mid + 1] {
            right = mid
        } else {
            left = mid + 1
        }
    }
    return left
}
```

### java
```java
class LeetCode0162 {
    
    public int findPeakElement(int[] nums) {
        int len = nums.length;
        if (len < 2) {
            return 0;
        }
        if (nums[0] > nums[1]) {
            return 0;
        }
        if (nums[len - 1] > nums[len - 2]) {
            return len - 1;
        }
        int left = 0, right = len - 1;
        while (left < right) {
            int mid = left + ((right - left) >> 1);
            if (nums[mid - 1] < nums[mid] && nums[mid] > nums[mid + 1]) {
                return mid;
            } else if (nums[mid] > nums[mid + 1]) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }
}
```