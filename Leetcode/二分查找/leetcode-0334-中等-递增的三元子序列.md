# 334. 递增的三元子序列

# 题目
给你一个整数数组 nums ，判断这个数组中是否存在长度为 3 的递增子序列。

如果存在这样的三元组下标 (i, j, k) 且满足 i < j < k ，使得 nums[i] < nums[j] < nums[k] ，返回 true ；否则，返回 false 。

https://leetcode.cn/problems/increasing-triplet-subsequence

提示：
- 1 <= nums.length <= 5 * 10^5
  -2^31 <= nums[i] <= 2^31 - 1

进阶：你能实现时间复杂度为 O(n) ，空间复杂度为 O(1) 的解决方案吗？

# 示例
```
输入：nums = [1,2,3,4,5]
输出：true
解释：任何 i < j < k 的三元组都满足题意
```

```
输入：nums = [5,4,3,2,1]
输出：false
解释：不存在满足题意的三元组
```

```
输入：nums = [2,1,5,0,4,6]
输出：true
解释：三元组 (3, 4, 5) 满足题意，因为 nums[3] == 0 < nums[4] == 4 < nums[5] == 6
```

# 解析
## 最长递增子序列
最长递增子序列 长度大于 2 即可


# 代码
```php
class LeetCode0334 {

    function increasingTriplet($arr) {
        if (!$arr || count($arr) < 3) {
            return false;
        } 
        $ends = array_fill(0, 3, 0);
        $ends[0] = $arr[0];
        $right = 0;
        for ($i = 1, $len = count($arr); $i < $len ; $i++) {
            $l = 0;
            $r = $right;
            while ($l <= $r) {
                $m = $l + (($r - $l) >> 1);
                if ($arr[$i] > $ends[$m]) {
                    $l = $m + 1;
                } else {
                    $r = $m - 1;
                }
            }
            $right = max ($right, $l);
            if ($right > 1) {
                return true;
            }
            $ends[$l] = $arr[$i];
        }
        return false;
    }
}
```