# 0035. 搜索插入位置

# 题目
给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。

你可以假设数组中无重复元素。

https://leetcode.cn/problems/search-insert-position/description/

# 示例
```
输入: [1,3,5,6], 5
输出: 2
```
```
输入: [1,3,5,6], 2
输出: 1
```

# 解析

## 二分
这道题目的前提是数组是有序数组，这也是使用二分查找的基础条件。

以后只要看到面试题里给出的数组是有序数组，都可以想一想是否可以使用二分法。

同时题目还强调数组中无重复元素，因为一旦有重复元素，使用二分查找法返回的元素下标可能不是唯一的。

确定要查找的区间到底是左闭右开 [left, right)，还是左闭又闭 [left, right]，这就是不变量。

然后在二分查找的循环中，坚持循环不变量的原则，很多细节问题，自然会知道如何处理了。


# 代码

### php
```php
class LeetCode0035 {

    // 二分
    function searchInsert($nums, $target) {
        $len = count($nums);
        $l = 0;
        $r = $len - 1;          // 定义target在左闭右闭的区间里，[left, right]
        while ($l <= $r) {      // 当left==right，区间[left, right]依然有效
            $mid = $l + (($r - $l) >> 1);   // 防止溢出 等同于(left + right)/2
            if ($nums[$mid] == $target) {
                return $mid;
            } else if ($nums[$mid] > $target) {
                // target 在左区间，所以[left, middle - 1]
                $r = $mid - 1;
            } else if ($nums[$mid] < $target) {
                // target 在右区间，所以[middle + 1, right]
                $l = $mid + 1;
            }
        }
        return $l;
    }
}
```

### go
```go
func searchInsert(nums []int, target int) int {
    size := len(nums)
    l, r := 0, size - 1
    for l <= r {
        mid := l + ((r - l) >> 1)
        if nums[mid] == target {
            return mid
        } else if nums[mid] > target {
            r = mid - 1
        } else {
            l = mid + 1
        }
    }
    return l
}
```

### java
```java
class LeetCode0035 {
    
    public int searchInsert(int[] nums, int target) {
        int i = 0, j = nums.length - 1;
        while (i <= j) {
            int mid = i + ((j - i) >> 1);
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] > target) {
                j = mid - 1;
            } else {
                i = mid + 1;
            }
        }
        return i;
    }
}
```