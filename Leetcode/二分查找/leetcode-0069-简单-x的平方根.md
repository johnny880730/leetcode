# 0069. x 的平方根

# 题目
给你一个非负整数 x ，计算并返回 x 的 算术平方根 。由于返回类型是整数，结果只保留 整数部分 ，小数部分将被 舍去 。

注意：不允许使用任何内置指数函数和算符，例如 pow(x, 0.5) 或者 x ** 0.5 。

https://leetcode.cn/problems/sqrtx/description/

提示：
- 0 <= x <= 2^31 - 1

# 示例
```
输入：x = 4
输出：2
```

```
输入：x = 8
输出：2
解释：8 的算术平方根是 2.82842..., 由于返回类型是整数，小数部分将被舍去。
```

# 解析

## 二分
由于 x 平方根的整数部分是满足 k² <= x 的最大 k 值，因此可以对 k 进行二分查找，从而得到答案。

二分查找的下界为 0，上界可以粗略地设定为 x。在二分查找的每一步中，只需要比较中间元素 mid 的平方与 x 的大小关系，
并通过比较的结果调整上下界的范围。

由于所有的运算都是整数运算，不会存在误差，因此在得到最终的答案 ans 后，也就不需要再去尝试 ans + 1 了。


> Q：为什么二分查找的终止条件是 left <= right ?  
> A：如果不放这个条件，while 遍历就会遍历到之前已经遍历过的区域。为什么要加上 = 的判断条件，因为将左指针和右指针相同的时候，
> 就是判断指针本身和目标值是否相等。  
> 每当这里想不清楚的时候，就拿最简单的例子：1、2、3 来做推理。要想知道 3 在不在他们中间，先比较 3 和 2，然后 3 比 2 大，
> left = mid + 1 = 3，right = right = 3。这时候 left 和 right 相等，他们的中间也就是 3，于是 3 在他们中间，
> 所以如果 left < right 就返回，就会返回错误的答案。

# 代码

### php
```php
class LeetCode0069 {

    function sqrt($x) {
        if ($x <= 1) {
            return $x;
        } 
        $left = 1;
        $right = $x;
        $res = -1;
        while ($left <= $right) {
            $mid = $left + (($right - $left) >> 1);
            if ($mid * $mid <= $x) {
                $res = $mid;
                $left = $mid + 1;
            } else {
                $right = $mid - 1;
            }
        }
        return $res;
    }
}
```

### java
```java
class LeetCode0069 {

    public int mySqrt(int x) {
        if (x <= 1) {
            return x;
        }
        int left = 1, right = x;
        int res = -1;
        while (left <= right) {
            int mid = left + ((right - left) >> 1);
            if ((long)mid * mid <= x) {
                res = mid;
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return res;
    }
}
```

### go
```go
func mySqrt(x int) int {
    if x <= 1 {
        return x
    }
    left := 1
    right := x
    res := -1
    for left <= right {
        mid := left + ((right - left) >> 1)
        if mid * mid == x {
            return mid
        } else if mid * mid < x {
            res = mid
            left = mid + 1
        } else {
            right = mid - 1
        }
    }
    return res
}
```


