# 0461. 汉明距离

# 题目
两个整数之间的 汉明距离 指的是这两个数字对应二进制位不同的位置的数目。

给你两个整数 x 和 y，计算并返回它们之间的汉明距离。

提示：
- 0 <= x, y <= 2^31 - 1

https://leetcode.cn/problems/hamming-distance/description/

# 示例
```
示例 1：

输入：x = 1, y = 4
输出：2
解释：
1   (0 0 0 1)
4   (0 1 0 0)
       ↑   ↑
上面的箭头指出了对应二进制位不同的位置。
```
```
示例 2：

输入：x = 3, y = 1
输出：1
```

# 解析

## 位运算 - 异或
汉明距离广泛应用于多个领域。在编码理论中用于错误检测，在信息论中量化字符串之间的差异。

两个整数之间的汉明距离是对应位置上数字不同的位数。

根据以上定义，使用异或运算，当且仅当输入位不同时输出为 1。

计算 x 和 y 之间的汉明距离，可以先计算 x ^ y，然后统计结果中等于 1 的位数。

统计结果中等于 1 的位数，就是 [【leetcode-0191-简单-位1的个数】](./leetcode-0191-简单-位1的个数.md) 的内容了。简单来说就是 n & (n - 1) 
的结果来获取位 1 的数量。


# 代码

### php
```php
class LeetCodeXXXX {
    
    /**
     * @param Integer $x
     * @param Integer $y
     * @return Integer
     */
    function hammingDistance($x, $y) {
        $m = $x ^ $y;
        $res = 0;
        while ($m != 0) {
            $res++;
            $m = $m & ($m - 1);
        }
        return $res;
    }
}
```

### java
```java
class LeetCode0461 {
    
    public int hammingDistance(int x, int y) {
        int m = x ^ y;
        int res = 0;
        while (m != 0) {
            res++;
            m = m & (m - 1);
        }
        return res;

    }
}
```

### go
```go
func hammingDistance(x int, y int) int {
    m := x ^ y
    res := 0
    for m != 0 {
        res++
        m = m & (m - 1)
    }
    return res
}
```
