# 0338. 比特位计数

# 题目
给你一个整数 n ，对于 0 <= i <= n 中的每个 i ，计算其二进制表示中 1 的个数 ，返回一个长度为 n + 1 的数组 ans 作为答案。

提示：
- 0 <= n <= 100,000

# 示例
```
示例 1：

输入：n = 2
输出：[0,1,1]
解释：
0 --> 0
1 --> 1
2 --> 10
```
```
示例 2：

输入：n = 5
输出：[0,1,1,2,1,2]
解释：
0 --> 0
1 --> 1
2 --> 10
3 --> 11
4 --> 100
5 --> 101
```

# 解析

## 位运算
最直观的做法是对从 0 到 n 的每个整数直接计算「一比特数」。每个 int 型的数都可以用 32 位二进制数表示，只要遍历其二进制表示的每一位即可得到 1 的数目。

利用 Brian Kernighan 算法，可以在一定程度上进一步提升计算速度。算法的原理是：对于任意整数 x，令 x = x & (x - 1)，该运算将 x 的二进制表示的最后一个 1 变成 0。
因此，对 x 重复该操作，直到 x 变成 0，则操作次数即为 x 的「一比特数」。

对于给定的 n，计算从 0 到 n 的每个整数的「一比特数」的时间都不会超过 O(logn)，因此总时间复杂度为 O(nlogn)。


# 代码

### php
```php
class LeetCode0338 {

    function countBits($n) {
        $res = array_fill(0, $n + 1, 0);
        for ($i = 0; $i <= $n; $i++) {
            $ones = 0;
            $x = $i;
            while ($x > 0) {
                $ones++;
                $x = $x & ($x - 1);
            }
            $res[$i] = $ones;
        }
        return $res;
    }
    
}
```

### java
```java
class LeetCode0338 {
    
    public int[] countBits(int n) {
        int[] res = new int[n + 1];
        for (int i = 0; i <= n; i++) {
            int ones = 0, x = i;
            while (x > 0) {
                ones++;
                x = x & (x - 1);
            }
            res[i] = ones;
        }
        return res;
    }

}
```

### go
```go
func countBits(n int) []int {
    res := make([]int, n + 1)
    for i := 0; i <= n; i++ {
        ones, x := 0, i
        for x > 0 {
            ones++
            x = x & (x - 1)
        }
        res[i] = ones
    }
    return res
}
```
