# 0191. 位1的个数

# 题目
编写一个函数，获取一个正整数的二进制形式并返回其二进制表达式中 设置位 的个数（也被称为汉明重量）。

https://leetcode.cn/problems/number-of-1-bits/description/

提示：
- 

# 示例
```
示例 1：

输入：n = 11
输出：3
解释：输入的二进制串 1011 中，共有 3 个设置位。
```
```
示例 2：

输入：n = 128
输出：1
解释：输入的二进制串 10000000 中，共有 1 个设置位。
```
```
示例 3：

输入：n = 2147483645
输出：30
解释：输入的二进制串 11111111111111111111111111111101 中，共有 30 个设置位。
```

# 解析

## 位运算

### 方法一
一个数字 n 怎么提取它最右侧的 1 ？答案为 n & ((~n) + 1)

如 n = 01011000，~n = 10100111，~n + 1 = 10101000。n & ((~n) + 1) = 00001000，由此得到了最右侧的 1 . 此时将 n 与 这个数做异或运算，
就可以将 n 最右侧的 1 剔除出 n 。只要 n != 0，就可以一直计算，每次计算就去掉一个 1

### 方法二
n & (n - 1)

二进制数 n 变成 n - 1 后，如果最后一位是 0，将向前一位借 2，2 - 1=1。最后一位为 1。如果前一位为 0，将继续向前一位借 2，加上本身少掉的 1.
则变为 1。一直遇到 1。减为 0.

假设二进制 n = xxxx10000，n- 1 = xxxx01111，n & (n - 1) = xxxx0000，可以看到将原来的最右边的 1 变为 0 了。
重复这个操作，每一次 n 最右边的 1 少一个。从而统计 n 中的 1 的个数

# 代码

### php
```php

class LeetCode0191 {

    function hammingWeight($n) {
        $res = 0;
        while ($n != 0) {
            $res++;
            $rightOne = $n & ((~$n) + 1);
            $n ^= $rightOne;
        }
        return $res;
    }
    
    function hammingWeight2($n) {
        $res = 0;
        while ($n != 0) {
            $res++;
            $n = $n & ($n - 1);
        }
        return $res;
    }
}
```

### go
```go
func hammingWeight(n uint32) int {
    res := 0
    for n != 0 {
        n = n & (n - 1)
        res++
    }
    return res
}
```

### java
```java
class LeetCode0191 {

    public int hammingWeight(int n) {
        int res = 0;
        while (n != 0) {
            res++;
            n = n & (n - 1);
        }
        return res;
    }
    
}
```