# 289. 生命游戏

# 题目
根据 百度百科 ， 生命游戏 ，简称为 生命 ，是英国数学家约翰·何顿·康威在 1970 年发明的细胞自动机。

给定一个包含 m × n 个格子的面板，每一个格子都可以看成是一个细胞。每个细胞都具有一个初始状态： 1 即为 活细胞 （live），或 0 即为 死细胞 （dead）。
每个细胞与其八个相邻位置（水平，垂直，对角线）的细胞都遵循以下四条生存定律：
- 如果活细胞周围八个位置的活细胞数少于两个，则该位置活细胞死亡；
- 如果活细胞周围八个位置有两个或三个活细胞，则该位置活细胞仍然存活；
- 如果活细胞周围八个位置有超过三个活细胞，则该位置活细胞死亡；
- 如果死细胞周围正好有三个活细胞，则该位置死细胞复活；

下一个状态是通过将上述规则同时应用于当前状态下的每个细胞所形成的，其中细胞的出生和死亡是同时发生的。给你 m x n 网格面板 board 的当前状态，返回下一个状态。

https://leetcode.cn/problems/game-of-life

提示：
- m == board.length
- n == board[i].length
- 1 <= m, n <= 25
- board[i][j] 为 0 或 1

## 示例
```
输入：board = [[1,1],[1,0]]
输出：[[1,1],[1,1]]
```
```
输入：board = [[0,1,0],[0,0,1],[1,1,1],[0,0,0]]
输出：[[0,0,0],[1,0,1],[0,1,1],[0,1,0]]
```

# 解析

## 额外状态 + 位运算

题目中每个细胞只有两种状态 1 或 0，用二进制就是 0....001 和 0...000，区别只在最后一位。因此 把最右第二位作为更新之后的状态，
- 如果原来是 0...001 的细胞，它继续存活的话就是 0..011，不存活的话就是 0...001
- 如果原来是 0...000 的细胞，它继续存活的话就是 0..010，不存活的话就是 0...000
- 因此如果细胞存活的话，在代码实现上就用 与 2 做或运算就可得到
- 这样计算不会影响到原来的状态，又能获得进化后的状态
- 就算该格子已经变成了进化过后的状态（如 0...011），将其与 1 做 & 运算依然能够获得它原来的状态

除了下方列出的两种情况下细胞会存活之外，其他情况都是死细胞：
1. 周围八格正好有三个活细胞，那么自己无论是什么状态都会变成活的状态
2. 自己是活细胞的话，周围八格必须正好有两个活细胞

这样只在确定存活的细胞格子与 2 做 | 运算，其他都忽略了

每个细胞计算结束后，再重新遍历一次，往右移动一位即可获得最终的状态

# 代码

```php
class LeetCode0289 {

    function gameOfLife(&$board) {
        $rows = count($board);
        $cols = count($board[0]);
        // 遍历每个格子
        for ($i = 0; $i < $rows; $i++) {
            for ($j = 0; $j < $cols; $j++) {
                $neighbors = $this->_getNeighbors($board, $i, $j);
                if ($neighbors == 3 || ($board[$i][$j] == 1 && $neighbors == 2)) {
                    $board[$i][$j] |= 2;    //与 2 做或运算，将进化状态写到最右第二位上
                }
            }
        }
        // 重写每个格子
        for ($i = 0; $i < $rows; $i++) {
            for ($j = 0; $j < $cols; $j++) {
                $board[$i][$j] >>= 1;       //右移一位，将表示进化后的状态覆盖到现在的状态
            }
        }
    }

    // 获取board[i][j]位置的周围 有几个1
    function _getNeighbors($board, $i, $j) {
        return $this->_f($board, $i - 1, $j - 1)
            + $this->_f($board, $i - 1, $j)
            + $this->_f($board, $i - 1, $j + 1)
            + $this->_f($board, $i, $j - 1)
            + $this->_f($board, $i, $j + 1)
            + $this->_f($board, $i + 1, $j - 1)
            + $this->_f($board, $i + 1, $j)
            + $this->_f($board, $i + 1, $j + 1);
    }

    // b[i][j] 上面有1，就返回1；不是1，就返回 0
    function _f($b, $i, $j) {
        return ($i >= 0 && $i < count($b) && $j >= 0 && $j < count($b[0]) && ($b[$i][$j] & 1) == 1) ? 1 : 0;
    }
}
```

### go
```go
func gameOfLife(board [][]int)  {
    rows, cols := len(board), len(board[0])
    for i := 0; i < rows; i++ {
        for j := 0; j < cols; j++ {
            neighbors := _getNeighbors(board, i, j)
            if neighbors == 3 || (neighbors == 2 && board[i][j] == 1) {
                board[i][j] |= 2
            }
        }
    }
    for i := 0; i < rows; i++ {
        for j := 0; j < cols; j++ {
            board[i][j] >>= 1
        }
    }
}

func _getNeighbors(board [][]int, i, j int) int {
    res := _f(board, i - 1, j - 1)
    res += _f(board, i - 1, j)
    res += _f(board, i - 1, j + 1)
    res += _f(board, i, j - 1)
    res += _f(board, i, j + 1)
    res += _f(board, i + 1, j - 1)
    res += _f(board, i + 1, j)
    res += _f(board, i + 1, j + 1)
    return res
}

func _f(b [][]int, i, j int) int {
    if i < 0 || i >= len(b) || j < 0 || j < len(b[0]) {
        return 0
    }
    if (b[i][j] & 1) == 1 {
        return 1
    }
    return 0
}
```