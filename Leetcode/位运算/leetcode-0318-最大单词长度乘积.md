# 318. 最大单词长度乘积

# 题目
给你一个字符串数组 words ，找出并返回 length(words[i]) * length(words[j]) 的最大值，并且这两个单词不含有公共字母。

如果不存在这样的两个单词，返回 0 。

提示：
- 2 <= words.length <= 1000
- 1 <= words[i].length <= 1000
- words[i] 仅包含小写字母

# 示例
```
words = ["abcw","baz","foo","bar","xtfn","abcdef"]
输出：16 
解释：这两个单词为 "abcw", "xtfn"。
```
```
输入：words = ["a","ab","abc","d","cd","bcd","abcd"]
输出：4 
解释：这两个单词为 "ab", "cd"。
```
```
输入：words = ["a","aa","aaa","aaaa"]
输出：0 
解释：不存在这样的两个单词。
```

# 解析

## 位运算
为了得到最大单词长度乘积，朴素的做法是，遍历字符串数组 words 中的每一对单词，判断这一对单词是否有公共字母，如果没有公共字母，
则用这一对单词的长度乘积更新最大单词长度乘积。

用 n 表示数组 words 的长度，用 li 表示单词 words[i] 的长度，其中 0 <= i < n ，则上述做法需要遍历字符串数组 words 中的每一对单词，
对于下标为 i 和 j 的单词，其中 i < j，需要 O(li × lj) 的时间判断是否有公共字母和计算长度乘积。因此上述做法的时间复杂度高于 O(n²)

如果可以将判断两个单词是否有公共字母的时间复杂度降低到 O(1)，则可以将总时间复杂度降低到 O(n²)。可以使用位运算预处理每个单词，
通过位运算操作判断两个单词是否有公共字母。由于单词只包含小写字母，共有 26 个小写字母，因此可以使用位掩码的最低 26 位分别表示每个字母是否在这个单词中出现。
将 a 到 z 分别记为第 0 个字母到第 25 个字母，则位掩码的从低到高的第 i 位是 1 当且仅当第 i 个字母在这个单词中，其中 0 <= i <= 25。

如果用数组 masks 记录每个单词的位掩码表示。计算数组 masks 之后，判断第 i 个单词和第 j 个单词是否有公共字母可以通过判断 masks[i] & masks[j] 
是否等于 0 实现，当且仅当 masks[i] & masks[j] = 0 时第 i 个单词和第 j 个单词没有公共字母，此时使用这两个单词的长度乘积更新最大单词长度乘积。

优化：

需要对数组 words 中的每个单词计算位掩码，如果数组 words 中存在由相同的字母组成的不同单词，则会造成不必要的重复计算。例如单词 meet 
和 met 包含的字母相同，只是字母的出现次数和单词长度不同，因此这两个单词的位掩码表示也相同。

由于判断两个单词是否有公共字母是通过判断两个单词的位掩码的按位与运算实现，因此在位掩码相同的情况下，单词的长度不会影响是否有公共字母，
当两个位掩码的按位与运算等于 0 时，为了得到最大单词长度乘积，这两个位掩码对应的单词长度应该尽可能大。

根据上述分析可知，如果有多个单词的位掩码相同，则只需要记录该位掩码对应的最大单词长度即可。

可以使用哈希表记录每个位掩码对应的最大单词长度，然后遍历哈希表中的每一对位掩码，如果这一对位掩码的按位与运算等于 0，则用这一对位掩码对应
的长度乘积更新最大单词长度乘积。

由于每个单词的位掩码都不等于 0，任何一个不等于 0 的数和自身做按位与运算的结果一定不等于 0，因此当一对位掩码的按位与运算等于 000 时，
这两个位掩码一定是不同的，对应的单词也一定是不同的。


# 代码

### php
```php
class Leetcode0318 {

    function maxProduct($words) {
        $len = count($words);
        if ($len <= 1) {
            return 0;
        }
        // 逐个计算字符掩码，存入哈希数组中
        $hash = [];
        for ($i = 0; $i < $len; $i++) {
            // 计算掩码
            $mask = 0;
            $wordLen = strlen($words[$i]);
            for ($j = 0; $j < $wordLen; $j++) {
                $mask |= (1 << (ord($words[$i][$j]) - ord('a')));
            }
            // 相同掩码的单词只需保存长度更长的
            $hash[$mask] = max($wordLen, ($hash[$mask] ?? 0));
        }
        
        // 遍历哈希数组，通过两重循环进行比较
        $res = 0;
        foreach ($hash as $ym1 => $val1) {
            // 优化点：拿来遍历比较的单词从数组里剔除，后面无需再参与比较
            unset($hash[$ym1]);
            foreach ($hash as $ym2 => $val2) {
                if (($ym1 & $ym2) == 0) {
                    $res = max($val1 * $val2, $res);
                }
            }
        }

        return $res;
    }
}
```

### java
```java
public int maxProduct(String[] words) {
    if (words.length <= 1) {
        return 0;
    }
    Map<Integer, Integer> hash = new HashMap<>();
    for (int i = 0; i < words.length; i++) {
        int mask = 0;
        String word = words[i];
        int wordLen = words[i].length();
        for (int j = 0; j < wordLen; j++) {
            mask |= 1 << (word.charAt(j) - 'a');
        }
        if (wordLen > hash.getOrDefault(mask, 0)) {
            hash.put(mask, wordLen);
        }
    }
    int res = 0;
    for (int a : hash.keySet()) {
        for (int b : hash.keySet()) {
            if ((a & b) == 0) {
                res = Math.max(res, hash.get(a) * hash.get(b));
            }
        }
    }
    return res;
}
```
