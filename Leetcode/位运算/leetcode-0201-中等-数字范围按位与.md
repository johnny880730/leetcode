# 0201. 数字范围按位与

# 题目
给你两个整数 left 和 right ，表示区间 [left, right] ，返回此区间内所有数字 按位与 的结果（包含 left 、right 端点）。

https://leetcode.cn/problems/bitwise-and-of-numbers-range/description/

提示：
- 0 <= left <= right <= 2^31 - 1

# 示例
```
示例 1：

输入：left = 5, right = 7
输出：4
```
```
示例 2：

输入：left = 0, right = 0
输出：0
```
```
示例 3：

输入：left = 1, right = 2147483647
输出：0
```

# 解析

## 位运算
位与的特性，只要参与位与的元素有一个为 0，那么位与结果就为 0。换句话说，如果参与位与的元素都相同，位与结果就是这个相同元素。

![img1](./img/leetcode-0201-img1.png)

因此只要找到这些元素的公共前缀，位公共前缀部分保留，后面的位全为 0；

而要找范围 [left, right] 元素的位公共前缀，等价于找 left 和 right 的公共前缀；

![img2](./img/leetcode-0201-img2.png)

如何找这两个数的公共前缀呢？
- 可以先把将两个数不断右移，去掉低位不同的部分，直到两个数相同；
- 然后记录右移了多少次，再左移回去，就得到结果了；

![img3](./img/leetcode-0201-img3.png)


# 代码

### php
```php
class LeetCode0201 {

    function rangeBitwiseAnd($left, $right) {
        $cnt = 0;
        while ($left != $right) {
            $left >>= 1;
            $right >>= 1;
            $cnt++;
        }

        return ($left & $right) << $cnt;
    }
}
```

### java
```java
class LeetCode0201 {

    public int rangeBitwiseAnd(int left, int right) {
        int cnt = 0;
        while (left != right) {
            cnt++;
            left >>= 1;
            right >>= 1;
        }

        return (left & right) << cnt;
    }
}
```

### go
```go
func rangeBitwiseAnd(left int, right int) int {
    cnt := 0
    for left != right {
        cnt++
        left >>= 1
        right >>= 1
    }

    return (left & right) << cnt
}
```
