# 93. 复原 IP 地址

# 题目
给定一个只包含数字的字符串 s ，用以表示一个 IP 地址，返回所有可能从 s 获得的 有效 IP 地址 。你可以按 任何 顺序返回答案。

有效 IP 地址 正好由四个整数（每个整数位于 0 到 255 之间组成，且不能含有前导 0），整数之间用 '.' 分隔。

例如："0.1.2.201" 和 "192.168.1.1" 是 有效 IP 地址，但是 "0.011.255.245"、"192.168.1.312" 和 "192.168@1.1" 是 无效 IP 地址。

https://leetcode.cn/problems/restore-ip-addresses/

# 示例：
```
输入：s = "25525511135"
输出：["255.255.11.135","255.255.111.35"]
```

```
输入：s = "0000"
输出：["0.0.0.0"]
```

```
输入：s = "1111"
输出：["1.1.1.1"]
```

```
输入：s = "010010"
输出：["0.10.0.10","0.100.1.0"]
```

```
输入：s = "101023"
输出：["1.0.10.23","1.0.102.3","10.1.0.23","10.10.2.3","101.0.2.3"]
```

提示：
- 0 <= s.length <= 3000
- s 仅由数字组成

# 解析
只要意识到这是切割问题，就可以使用回溯搜索法，类似与《131. 分割回文串》

确定递归函数的参数：因为不能重复切割，一定要 startIndex，用于记录下一层递归分割的起始位置。还需要一个变量 pointNum，
用于记录添加 “.” 的数量

确定递归的终止条件：本题明确要求字符串只会分成四段，所以不能用切割线切割到最后作为终止条件，而是要将分割的段数作为终止条件。
根据 pointNum 的定义，当 pointNum==3 说明字符串分成了四段，然后验证第四段是否合法，如果合法就加入结果集。

确定单层搜索的逻辑：截取子字符串，判断是否合法。如果合法就在后面加上符号 “.” 表示已经分割。如果不合法就结束本层循环。

递归和回溯的过程：调用递归函数时，下一层递归的 startIndex 要从 i+2 开始（因为需要在字符串加入分隔符），同时记录分隔符的数量+1。
回溯的时候，删除刚刚加的分隔符，分隔符的数量也要-1。

最后就是判断段位是否为有效段位，主要考虑三点：
- 以 0 开头的数字不合法
- 有非正整数字符不合法
- 大于 255 不合法

# 代码

### php
```php
class LeetCode0093 {

    public $res = [];
    public $path = [];

    public function restoreIpAddresses($s) {
        if (strlen($s) > 12 || strlen($s) == 0) {
            return $this->res;
        }
        $this->_backTracking($s, 0, 0);
        return $this->res;
    }

    protected function _backTracking($s, $start, $pointNum) {
        if ($pointNum == 3) { // 逗点数量为3时，分隔结束
            // 判断第四段子字符串是否合法，如果合法就放进result中
            if ($this->_isValid($s, $start, strlen($s) - 1)) {
                $this->res[] = $s;
            }
            return;
        }

        for ($i = $start; $i < strlen($s); $i++) {
            if (!$this->_isValid($s, $start, $i)) {
                break;
            }
            //在str的后⾯插⼊⼀个逗点
            $s = substr($s, 0, $i+1) . '.' . substr($s, $i+1);
            $pointNum++;
            // 插⼊逗点之后下⼀个⼦串的起始位置为i+2
            $this->_backTracking($s, $i + 2, $pointNum);
            // 回溯
            $pointNum--;
            // 回溯删掉逗点
            $s = substr($s, 0, $i + 1) . substr($s, $i + 2);
        }
    }

    // 判断字符串s在左闭⼜闭区间[start, end]所组成的数字是否合法
    protected function _isValid($s, $start, $end)
    {
        if ($start > $end) {
            return false;
        }
        if ($s[$start] == '0' && $start != $end) { // 0开头的数字不合法
            return false;
        }
        $num = 0;
        for ($i = $start; $i <= $end; $i++) {
            if (ord($s[$i]) > ord('9') || ord($s[$i]) < ord('0')) { // 遇到⾮数字字符不合法
                return false;
            }
            $num = $num * 10 + (ord($s[$i]) - ord('0'));
            if ($num > 255) { // 如果⼤于255了不合法
                return false;
            }
        }
        return true;
    }
}
```