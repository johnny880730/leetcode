# 47. 全排列 II

# 题目
给定一个可包含重复数字的序列 nums ，按任意顺序 返回所有不重复的全排列。


提示：
- 1 <= nums.length <= 6
- -10 <= nums[i] <= 10

https://leetcode.cn/problems/permutations-ii/

# 示例：
```
输入：nums = [1,1,2]
输出：
[[1,1,2],
 [1,2,1],
 [2,1,1]]
```

```
输入：nums = [1,2,3]
输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
```



# 解析

## 回溯
请先做前置题目 [「46. 全排列」](./leetcode-0046-中等-全排列.md)。

本题和上一题的区别是数组中“存在重复元素”。当数组存在重复元素时，排列方案中也存在重复的排列方案。

为了排除这些重复方案，需在固定某位元素时，保证“每种元素只在此位固定一次”，即遇到重复元素时不交换，直接跳过，从而将生成重复排列的搜索分支进行“剪枝” 。

![](./images/leetcode-0047-解析.png)

递归解析：
- 终止条件： 当 idx == len(nums) - 1 时，代表所有位已固定（最后一位只有 1 种情况），则将当前组合 nums 转化为数组并加入 res ，并返回。
- 递推参数： 当前固定位 idx 。
- 递推工作：
    - 初始化一个 Set ，用于排除重复的元素；将第 idx 位元素与 i ∈ [idx, len(nums)] 元素分别交换，并进入下层递归。
    - 剪枝： 若 nums[i] 在 Set 中，代表其是重复元素，因此 “剪枝” 。
    - 将 nums[i] 加入 Set ，以便之后遇到重复元素时剪枝。
    - 固定元素： 将元素 nums[i] 和 nums[idx] 交换，即固定 nums[i] 为当前位元素。
    - 开启下层递归： 调用 backtrack(idx + 1) ，即开始固定第 idx + 1 个元素。
    - 还原交换： 将元素 nums[i] 和 nums[idx] 交换（还原之前的交换）。

# 代码

### php
```php
class LeetCode0047 {

    public $res = [];

    /**
     * @param Integer[] $nums
     * @return Integer[][]
     */
    function permuteUnique($nums) {
        $this->_backtrack($nums, 0);
        return $this->res;
    }

    private function _backtrack($nums, $idx) {
        if ($idx == count($nums) - 1) {
            $this->res[] = $nums;
            return;
        }
        $hash = array();
        for ($i = $idx; $i < count($nums); $i++) {
            if (array_key_exists($nums[$i], $hash)) {
                // 重复，因此剪枝
                continue;
            }
            $hash[$nums[$i]] = true;

            $this->_swap($nums, $i, $idx);          // 交换，将 nums[i] 固定在第 idx 位
            $this->_backtrack($nums, $idx + 1);     // 开启固定第 idx + 1 位元素
            $this->_swap($nums, $i, $idx);          // 恢复交换
        }
    }

    private function _swap(&$nums, $m, $n) {
        $tmp = $nums[$m];
        $nums[$m] = $nums[$n];
        $nums[$n] = $tmp;
    }
}
```

### java
```java
class LeetCode0047 {

    public List<List<Integer>> res;
    public List<Integer> path;

    public List<List<Integer>> permuteUnique(int[] nums) {
        this.res = new ArrayList<>();
        this.path = new ArrayList<>();
        for (int num : nums) {
            this.path.add(num);
        }
        _backtrack(0);
        return this.res;
    }

    private void _backtrack(int idx) {
        if (idx == this.path.size() - 1) {
            this.res.add(new ArrayList<>(this.path));
            return;
        }
        Map<Integer, Boolean> hash = new HashMap<>();
        for (int i = idx; i < this.path.size(); i++) {
            if (hash.containsKey(this.path.get(i))) {
                continue;
            }
            hash.put(this.path.get(i), true);

            _swap(i, idx);
            _backtrack(idx + 1);
            _swap(i, idx);
        }
    }

    private void _swap(int a, int b) {
        int tmp = this.path.get(a);
        this.path.set(a, this.path.get(b));
        this.path.set(b, tmp);
    }
}

```

### go
```go
func permuteUnique(nums []int) [][]int {
    res := [][]int{}
    _backtrack(nums, 0, &res)
    return res
}

func _backtrack(nums []int, idx int, res *[][]int) {
    if idx == len(nums) - 1 {
        tmp := make([]int, len(nums))
        copy(tmp, nums)
        *res = append(*res, tmp)
        return
    }
    hash := make(map[int]bool)
    for i := idx; i < len(nums); i++ {
        if _, ok := hash[nums[i]]; ok {
            continue
        }
        hash[nums[i]] = true
        
        nums[i], nums[idx] = nums[idx], nums[i]
        _backtrack(nums, idx + 1, res)
        nums[i], nums[idx] = nums[idx], nums[i]
    }
}
```