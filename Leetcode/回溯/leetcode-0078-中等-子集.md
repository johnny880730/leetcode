# 78. 子集

# 题目
给你一个整数数组 nums ，数组中的元素 互不相同 。返回该数组所有可能的子集（幂集）。

解集 不能 包含重复的子集。你可以按 任意顺序 返回解集。

https://leetcode.cn/problems/subsets/

提示：
- 1 <= nums.length <= 10
- -10 <= nums[i] <= 10
- nums 中的所有元素 互不相同

# 示例：
```
输入：nums = [1,2,3]
输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
```

```
输入：nums = [0]
输出：[[],[0]]
```



# 解析
如果把子集问题、组合问题、分割问题都想象为一棵树，那么组合问题和分割问题都是收集树的叶子节点，而子集问题是收集树的所有节点。

其实子集也是一种组合问题，因为它的集合是无序的，子集 [1, 2] 和子集 [2, 1] 是一样的。

既然集合是无序的，去过的元素不会重复去，那么写回溯的时候，for 循环就要从 startIndex 开始，而不是从零开始。

什么时候从 0 开始呢？求排列问题的时候，for 循环就要从 0 开始，因为排列问题中的集合是有序的。

确定递归函数的参数：path 为子集收集元素；result 存放子集组合；startIndex 循环开始位置。

确定递归的终止条件：当剩余集合为空的时候，就到达了叶子节点。那什么时候剩余结合为空呢？当 startIndex 大于数组的长度时，
遍历就终止了，因为没有元素可取了。

确定单层搜索逻辑：求子集问题不需要任何剪枝操作，因为求子集的过程就是要遍历整棵树。

# 代码

### php
```php

class LeetCode0078 {

    public $res = [];
    public $path = [];

    public function subsets($nums) {
        $this->_backTracking($nums, 0);
        return $this->res;
    }

    protected function _backTracking($nums, $index) {
        // 收集子集的问题，每一个树的节点都是结果的一部分，因此一上来就要把 path 加到 res 里
        $this->res[] = $this->path; 
        if ($index >= count($nums)) {
            return;   
        }
        for ($i = $index; $i < count($nums); $i++) {
            $this->path[] = $nums[$index];
            $this->_backTracking($nums, $i + 1);
            array_pop($this->path);
        }
    }
}
```

### go
```go
func subsets(nums []int) [][]int {
    res := [][]int{}

    var dfs func(idx int, path []int)
    dfs = func(idx int, path []int) {
        tmp := make([]int, len(path))
        copy(tmp, path)
        res = append(res, tmp)
		
        if idx >= len(nums) {
            return
        }
		for i, length := idx, len(nums); i < length; i++ {
            path = append(path, nums[i])
            dfs(i + 1, path)
            path = path[: len(path) - 1]
        }
        
    }
    dfs(0, []int{})

    return res
}
```

### java
```java
class LeetCode0078 {

    public List<List<Integer>> res = new ArrayList<>();

    public List<Integer> path = new ArrayList<>();

    public List<List<Integer>> subsets(int[] nums) {
        _backTracking(nums, 0);
        return res;
    }

    protected void _backTracking(int[] nums, int index) {
        res.add(new ArrayList<>(path));
        if (index >= nums.length) {
            return;
        }
        for (int i = index; i < nums.length; i++) {
            path.add(nums[i]);
            _backTracking(nums, i + 1);
            path.remove(path.size() - 1);
        }
    }
}
```
