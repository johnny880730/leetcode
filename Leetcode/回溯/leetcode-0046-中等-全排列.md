# 0046. 全排列

# 题目
给定一个不含重复数字的数组 nums ，返回其 所有可能的全排列 。你可以 按任意顺序 返回答案。

https://leetcode.cn/problems/permutations/description/

提示：
- 1 <= nums.length <= 6
- -10 <= nums[i] <= 10
- nums 中的所有整数 互不相同

# 示例：
```
输入：nums = [1,2,3]
输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
```

```
输入：nums = [0,1]
输出：[[0,1],[1,0]]
```

```
输入：nums = [1]
输出：[[1]]
```

# 解析

## 回溯
对于排列问题，如果想用 for 循环把结果搜索出来，那么很难写出相应的代码。为什么回溯法是暴力搜索，效率那么低还要用他呢？

因为一些问题能用暴力搜索解决就不错了

以 [1, 2, 3] 为例，抽象后的树形结构如下图所示：

![](./images/leetcode-0046-解析.png)

确定递归函数的参数：首先排列是有序的，这是与之前的子集和组合所不同的地方。处理排列问题不能用 startIndex 了。排列问题需要一个 used 数组，
用于标记已经选择的元素。

确定递归的终止条件：可以看出叶子节点就是收割结果的地方。当收集元素的数组 path 的长度和 nums 数组一样大的时候，就说明找到了一个全排列，
也就是到了叶子节点。

确定单层搜索的逻辑：处理排列问题时么此都要从头开始搜索，而 used 数组用于记录此时 path 中都有哪些元素被使用过了，一个排列中的元素只能使用一次。


这里最大的不同就是 for 循环里不用 startIndex 了。因为排列问题，每次都要从头开始搜索，例如元素1 在 [1,2] 中已经使用过了，
但是在 [2,1] 中还要再使用一次 1。而 used 数组，其实就是记录此时 path 里都有哪些元素使用了，一个排列里一个元素只能使用一次。

此时可以感受出排列问题的不同：
- 每层都是从 0 开始搜索而不是 startIndex
- 需要 used 数组记录 path 里都放了哪些元素了

# 代码

### php
```php
class LeetCode0046 {

    public $res = [];
    public $path = [];
    
    function permute($nums) {
        $used = array_fill(0, count($nums), false);
        $this->_backtrack($nums, $used);
        return $this->res;
    }

    protected function _backtrack($nums, $used)
    {
        if (count($this->path) == count($nums)) {
            $this->res[] = $this->path;
            return;
        }

        for ($i = 0; $i < count($nums); $i++) {
            if ($used[$i] == true) {
                // path 中已经收录的元素，跳过
                continue;
            }
            $used[$i] = true;
            $this->path[] = $nums[$i];
            $this->_backtrack($nums, $used);
            $used[$i] = false;
            array_pop($this->path);
        }
    }
}
```

### java
```java
class LeetCode0046 {

    private final List<List<Integer>> res = new ArrayList<>();

    private final List<Integer> path = new ArrayList<>();

    public List<List<Integer>> permute(int[] nums) {
        boolean[] used = new boolean[nums.length];
        _backtrack(nums, used);
        return this.res;
    }

    protected void _backtrack(int[] nums, boolean[] used) {
        if (path.size() == nums.length) {
            // 在 Java 中，参数传递是 值传递，对象类型变量在传参的过程中，复制的是变量的地址。
            // 这些地址被添加到 res 变量，但实际上指向的是同一块内存地址，
            // 如果直接 res.add(path)，会看到 6 个空的列表对象。
            // 解决的方法很简单，在 res.add(path); 这里做一次拷贝即可
            res.add(new ArrayList<>(path));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (used[i]) {
                continue;
            }
            used[i] = true;
            path.add(nums[i]);
            _backtrack(nums, used);
            used[i] = false;
            path.remove(path.size() - 1);
        }
    }
}
```

### go
```go
func permute(nums []int) [][]int {
    var res [][]int
    var path []int
    used := make([]bool, len(nums))
    var _backtrack func(nums []int)
    _backtrack = func(nums []int) {
        if len(path) == len(nums) {
            tmp := make([]int, len(path))
            copy(tmp, path)
            res = append(res, tmp)
            return
        }
        for i, length := 0, len(nums); i < length; i++ {
            if used[i] == true {
                continue
            }
            used[i] = true
            path = append(path, nums[i])
            _backtrack(nums)
            used[i] = false
            path = path[: len(path) - 1]
        }
    }
    
    _backtrack(nums)
    return res
}
```



