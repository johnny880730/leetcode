# 0077. 组合

# 问题
给定两个整数 n 和 k，返回范围 [1, n] 中所有可能的 k 个数的组合。

你可以按 任何顺序 返回答案。

https://leetcode.cn/problems/combinations/description/


提示：
- 1 <= n <= 20
- 1 <= k <= n

# 示例：
```
输入：n = 4, k = 2
输出：
[
  [2,4],
  [3,4],
  [2,3],
  [1,2],
  [1,3],
  [1,4],
]
```

```
输入：n = 1, k = 1
输出：[[1]]
```

# 解析

## 回溯
回溯法解决的问题都可以抽象为树形结构，用树形结构来理解回溯就容易很多。

确定递归函数的返回值和参数：这里定义两个全局变量，一个用来存放符合条件的单一结果，另一个存放符合条件的结果的集合。
还需要一个参数 startIndex，用来记录本层递归的过程中，集合从哪里开始遍历。每次从集合中选取元素，可选择的返回逐渐收缩，
而调整可选择的范围就需要 startIndex。

确定回溯函数的终止条件：什么时候到达所谓的叶子节点呢？如果 path 数组的大小达到 k，则说明我们找到了一个子集大小为 k 的组合了，
此时用 result 二维数组保存 path 数组。

确定单层搜索的逻辑：回溯法的搜索过程就是一个树形结构的遍历过程。for 循环每次从 startIndex 开始遍历，然后用 path 保存获取的节点 i。
递归函数通过不断调用自己往深处遍历，总会遇到叶子节点，遇到了就返回。递归函数后序的操作就是回溯，即撤销本次处理的结果。

# 代码

### php
```php
class LeetCode0077 {

    public $res = [];
    public $path = [];

    public function combine($n, $k) {
        $this->_backtrack($n, $k, 1);
        return $this->res;
    }

    protected function _backtrack($n, $k, $startIndex) {
        if (count($this->path) == $k) {
            $this->res[] = $this->path;
            return;
        }
        // 剪枝：剩下的数字不够的话就可以不用搜索了
        // count(path) 已经选取的元素的大小
        // k - count(path) 还剩多少个元素要选
        // n - (k - count(path)) + 1 至多从哪里开始选取
        for ($i = $startIndex; $i <= $n - ($k - count($this->path)) + 1; $i++) {
            $this->path[] = $i;
            $this->_backtrack($n, $k, $i + 1);
            array_pop($this->path);
        }
    }
}
```

### go
```go
func combine(n int, k int) [][]int {
	var path []int
	var res [][]int

	_backtrack(n, k, 1, path, &res)
	
	return res
}

func _backtrack(n, k, index int, path []int, res *[][]int) {
	if len(path) == k {
		tmp := make([]int, len(path))
		copy(tmp, path)
		*res = append(*res, tmp)
		return
	}
	for i := index; i <= n - (k - len(path)) + 1; i++ {
		path = append(path, i)
		_backtrack(n, k, i + 1, path, res)
		path = path[ : len(path) - 1]
	}
}
```


### java
```java
class LeetCode0077 {
    
    public List<List<Integer>> res = new ArrayList();
    public List<Integer> path = new ArrayList();

    public List<List<Integer>> combine(int n, int k) {
        _backtrack(n, k, 1);
        return res;
    }

    private void _backtrack(int n, int k, int index) {
        if (path.size() == k) {
            res.add(new ArrayList<>(path));
            return;
        }
        for (int i = index; i <= n - (k - path.size()) + 1; i++) {
            path.add(i);
            _backtrack(n, k, i + 1);
            path.remove(path.size() - 1);
        }
    }
}
```