# 2698. 求一个整数的惩罚数


# 题目
给你一个正整数 n ，请你返回 n 的 惩罚数 。

n 的 惩罚数 定义为所有满足以下条件 i 的数的平方和：
- 1 <= i <= n
- i * i 的十进制表示的字符串可以分割成若干连续子字符串，且这些子字符串对应的整数值之和等于 i 。

提示：
- 1 <= n <= 1000

https://leetcode.cn/problems/find-the-punishment-number-of-an-integer/

# 示例
```
输入：n = 10
输出：182
解释：总共有 3 个整数 i 满足要求：
- 1 ，因为 1 * 1 = 1
- 9 ，因为 9 * 9 = 81 ，且 81 可以分割成 8 + 1 。
- 10 ，因为 10 * 10 = 100 ，且 100 可以分割成 10 + 0 。
因此，10 的惩罚数为 1 + 81 + 100 = 182
```
```
输入：n = 37
输出：1478
解释：总共有 4 个整数 i 满足要求：
- 1 ，因为 1 * 1 = 1
- 9 ，因为 9 * 9 = 81 ，且 81 可以分割成 8 + 1 。
- 10 ，因为 10 * 10 = 100 ，且 100 可以分割成 10 + 0 。
- 36 ，因为 36 * 36 = 1296 ，且 1296 可以分割成 1 + 29 + 6 。
因此，37 的惩罚数为 1 + 81 + 100 + 1296 = 1478
```

# 解析

## 回溯
题目的关键在于如何找到所有满足要求的数字 i，在此直接利用回溯即可。

对于每个数字 i，首先需要将 i² 转换为字符串 s，然后依次将字符串 s 进行枚举分割子串，分别枚举第一个子串 s[0 ⋯ i]、第二个子串 s[i + 1 ⋯ j]，
依次枚举剩余的子串，同时累加这些子串对应的十进制整数值之和为 tot，如果存在分割方案使得数值之和 tot = i，则说明 i 符合要求，
如果当前的 tot > i 时则中止当前的搜索。

依次检测在区间 [1, n ] 中有多少满足要求的数字 i，并返回这些数字 i 的平方和。


# 代码

### php
```php
class Leetcode2698 {

    function punishmentNumber($n) {
        $res = 0;
        for ($i = 1; $i <= $n; $i++) {
            $s = strval($i * $i);
            if ($this->_dfs($s, 0, 0, $i)) {
                $res += $i * $i;
            }
        }
        return $res;
    }

    function _dfs($s, $pos, $tot, $target) {
        $len = strlen($s);
        if ($pos == $len) {
            return $tot == $target;
        }
        $sum = 0;
        for ($i = $pos; $i < $len; $i++) {
            $sum = $sum * 10 + intval($s[$i]);
            if ($sum + $tot > $target) {
                break;
            }
            if ($this->_dfs($s, $i + 1, $sum + $tot, $target)) {
                return true;
            }
        }
        return false;
    }
}
```

### java
```java
class LeetCode2698 {

    public int punishmentNumber(int n) {
        int res = 0;
        for (int i = 1; i <= n; i++) {
            String s = Integer.toString(i * i);
            if (_dfs(s, 0, 0, i)) {
                res += i * i;
            }
        }
        return res;
    }

    protected boolean _dfs(String s, int pos, int tot, int target) {
        if (pos == s.length()) {
            return tot == target;
        } 
        int sum = 0;
        for (int i = pos; i < s.length(); i++) {
            sum = sum * 10 + (s.charAt(i) - '0');
            if (sum + tot > target) {
                break;
            }
            if (_dfs(s, i + 1, sum + tot, target)) {
                return true;
            }
        }
        return false;
    }
    
}
```
