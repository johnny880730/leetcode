# 37. 解数独

# 题目
编写一个程序，通过填充空格来解决数独问题。

数独的解法需 遵循如下规则：
- 数字 1-9 在每一行只能出现一次。
- 数字 1-9 在每一列只能出现一次。
- 数字 1-9 在每一个以粗实线分隔的 3x3 宫内只能出现一次。（请参考示例图）
- 数独部分空格内已填入了数字，空白格用 '.' 表示。

提示：
- board.length == 9
- board[i].length == 9
- board[i][j] 是一位数字或者 '.'
- 题目数据 保证 输入数独仅有一个解

https://leetcode.cn/problems/sudoku-solver/

# 示例
```
输入：board = [
    ["5","3",".",".","7",".",".",".","."],
    ["6",".",".","1","9","5",".",".","."],
    [".","9","8",".",".",".",".","6","."],
    ["8",".",".",".","6",".",".",".","3"],
    ["4",".",".","8",".","3",".",".","1"],
    ["7",".",".",".","2",".",".",".","6"],
    [".","6",".",".",".",".","2","8","."],
    [".",".",".","4","1","9",".",".","5"],
    [".",".",".",".","8",".",".","7","9"]
]
输出：[
    ["5","3","4","6","7","8","9","1","2"],
    ["6","7","2","1","9","5","3","4","8"],
    ["1","9","8","3","4","2","5","6","7"],
    ["8","5","9","7","6","1","4","2","3"],
    ["4","2","6","8","5","3","7","9","1"],
    ["7","1","3","9","2","4","8","5","6"],
    ["9","6","1","5","3","7","2","8","4"],
    ["2","8","7","4","1","9","6","3","5"],
    ["3","4","5","2","8","6","1","7","9"]
]
```


# 解析

## 回溯
在 [有效的数独](./leetcode-0036-中等-有效的数独.md) 的基础上，在空格处一一填入数字，通过递归来判断是否符合要求，不符合要求则恢复原状。


# 代码

### php
```php
class LeetCode0037 {

    public function solveSudoku(&$board) {
        $rows  = array_fill(0, 9, array_fill(0, 10, false));
        $cols  = array_fill(0, 9, array_fill(0, 10, false));
        $boxes = array_fill(0, 9, array_fill(0, 10, false));
        for ($i = 0; $i < 9; $i++) {
            for ($j = 0; $j < 9; $j++) {
                if ($board[$i][$j] != '.') {
                    $box = 3 * intval($i / 3) + intval($j / 3);
                    $num = intval($board[$i][$j]);
                    $rows[$i][$num] = true;
                    $cols[$j][$num] = true;
                    $boxes[$box][$num] = true;
                }    
            }
        }
        $this->_backtracking($board, 0, 0, $rows, $cols, $boxes);
    }

    protected function _backtracking(&$board, $i, $j, $rows, $cols, $boxes) {
        if ($i == 9) {
            return true;
        }
        $nextI = $j != 8 ? $i : $i + 1;
        $nextJ = $j != 8 ? $j + 1 : 0;
        if ($board[$i][$j] != '.') {
            return $this->_backtracking($board, $nextI, $nextJ, $rows, $cols, $boxes);
        } else {
            $box = 3 * intval($i / 3) + intval($j / 3);
            for ($num = 1; $num <= 9; $num++) {
                // 剪枝
                if (!$rows[$i][$num] && !$cols[$j][$num] && !$boxes[$box][$num]) {
                    $rows[$i][$num] = true;
                    $cols[$j][$num] = true;
                    $boxes[$box][$num] = true;
                    $board[$i][$j] = strval($num);
                    if ($this->_backtracking($board, $nextI, $nextJ, $rows, $cols, $boxes)) {
                        return true;
                    }
                    // 还原
                    $rows[$i][$num] = false;
                    $cols[$j][$num] = false;
                    $boxes[$box][$num] = false;
                    $board[$i][$j] = '.';
                }
            }
            return false;
        }
    }

}
```

### go
```go
func solveSudoku(board [][]byte) {
    rows := [9][10]bool{}
    cols := [9][10]bool{}
    boxes := [9][10]bool{}
    for i := 0; i < 9; i++ {
        for j := 0; j < 9; j++ {
            if board[i][j] != '.' {
                box := 3 * (i / 3) + j / 3
                num, _ := strconv.Atoi(string(board[i][j]))
                rows[i][num] = true
                cols[j][num] = true
                boxes[box][num] = true
            }
        }
    }
    _backtracking(board, 0, 0, rows, cols, boxes)
}

func _backtracking(board [][]byte, i int, j int, rows [9][10]bool, cols [9][10]bool, boxes [9][10]bool) bool {
    if i == 9 {
        return true
    }
    nextI := i + 1
    nextJ := 0
    if j != 8 {
        nextI = i
        nextJ = j + 1
    }
    if board[i][j] != '.' {
        return _backtracking(board, nextI, nextJ, rows, cols, boxes)
    } else {
        box := 3 * (i / 3) + (j / 3)
        for num := 1; num <= 9; num++ {
            if !rows[i][num] && !cols[j][num] && !boxes[box][num] {
                rows[i][num] = true
                cols[j][num] = true
                boxes[box][num] = true
                board[i][j] = byte(num) + '0'
                if _backtracking(board, nextI, nextJ, rows, cols, boxes) {
                    return true
                }
                rows[i][num] = false
                cols[j][num] = false
                boxes[box][num] = false
                board[i][j] = byte('.')
            }
        }
        return false
    }
}
```