# 216. 组合总和 III

# 题目
找出所有相加之和为 n 的 k 个数的组合。组合中只允许含有 1 - 9 的正整数，并且每种组合中不存在重复的数字。

说明：
- 所有数字都是正整数。
- 解集不能包含重复的组合。

https://leetcode.cn/problems/combination-sum-iii/

# 示例:
```
输入: k = 3, n = 7
输出: [[1,2,4]]
```

# 示例
```
输入: k = 3, n = 9
输出: [[1,2,6], [1,3,5], [2,3,4]]
```


# 解析
本题中的 k 相当于树的身体，9（因为整个集合就九个数）就是树的宽度。

确定递归函数的返回值和参数：一维数组 path 存放符合条件的结果，二维数组 result 存放结果集。还需要如下参数：
- targetSum，目标和，也就是题目中的 n
- k，为题目中要求的 k 个数。
- sum，为已经收集的元素的综合，也就是 path 中元素的和
- startIndex，为下一层 for 循环搜索的起始位置

这个 sum 也可以省略，每次运算的时候 targetSum 减去元素的数值，判断是否为 0，是 0 表示收集到符合条件的结果了。

确定终止条件：k 其实已经限制了树形结构的深度，因为就 k 个元素，所以如果 path 里的数字数量与 k 相等，就终止遍历。
如果此时 path 中收集到的元素综合 sum 与 targetSum 相同，就用 result 收集当前的结果。

确定单层搜索的过程：本题由于题干限制，所以 for 循环的终止条件就是 i<=9，处理过程就是 path 收集每次选取的元素，
sum 用于统计 path 中元素的总和。别忘了处理过程和回溯过程是一一对应的，处理过程中有加法操作，回溯过程就要有对应的减法操作。

# 代码

### php
```php
class LeetCode0216 {

    public $res = [];
    public $path = [];

    public function combinationSum3($k, $n) {
        $this->_backTracking($n, $k, 0, 1);
        return $this->res;    
    }

    protected function _backTracking($target, $k, $curSum, $startIndex) {
        // 剪枝：已选元素总和如果已经大于target了，那么往后遍历就没有意义了，直接剪掉。
        if ($curSum > $target) {
            return;
        }
        if (count($this->path) == $k) {
            if ($curSum == $target) {
                $this->res[] = $this->path;
            }
            return;
        }

        for ($i = $startIndex; $i <= 9; $i++) {
            $curSum += $i;
            $this->path[] = $i;
            $this->_backTracking($target, $k, $curSum, $i + 1);
            // 回溯
            $curSum -= $i;
            array_pop($this->path);
        }
    }
}
```

### go
```go
func combinationSum3(k int, n int) [][]int {
    var path []int
    var res [][]int
    
    _backtracking(k, n, 0, 1, path, &res)
    return res
    }
    
func _backtracking(k int, n int, curSum int, idx int, path []int, res *[][]int) {
    if curSum > n {
        return
    }
    if curSum == n && k == len(path) {
        tmp := make([]int, len(path))
        copy(tmp, path)
        *res = append(*res, tmp)
        return
    }
    for i := idx; i <= 9; i++ {
        curSum += i
        path = append(path, i)
        _backtracking(k, n, curSum, i + 1, path, res)
        curSum -= i
        path = path[: len(path) - 1]
    }
}
```
