# 51. N 皇后

# 题目
n 皇后问题 研究的是如何将 n 个皇后放置在 n×n 的棋盘上，并且使皇后彼此之间不能相互攻击。

给你一个整数 n ，返回所有不同的 n 皇后问题 的解决方案。

每一种解法包含一个不同的 n 皇后问题 的棋子放置方案，该方案中 'Q' 和 '.' 分别代表了皇后和空位。

https://leetcode.cn/problems/n-queens

# 示例：
```
输入：n = 4
输出：[[".Q..","...Q","Q...","..Q."],["..Q.","Q...","...Q",".Q.."]]
解释：4 皇后问题存在两个不同的解法。
```

```
输入：n = 1
输出：[["Q"]]
```

提示：
- 1 <= n <= 9


# 解析
首先看下对皇后的约束条件：
- 不能同行
- 不能同列
- 不能同斜线

确定了约束条件条件，如何搜索皇后的位置。

二维矩阵中矩阵的高就是树形结构的高度，矩阵的宽就是树形结构的宽度。用对皇后的约束条件来回溯这棵树，只要搜索到了树的叶子节点，
就说明找到了皇后的合理位置了。

确定递归函数的参数：result 记录最终结果；n 为棋盘的大小；row 记录当前遍历到第几层。

确定递归终止条件：当递归到棋盘底层（也就是叶子节点）的时候，就可以收集结果并返回了。

确定单层搜索的逻辑：递归深度就是 row 控制棋盘的行数，每一层 for 循环的 col 用于控制棋盘的列，一行一列就确定了放皇后的位置。
每次都要从新的一行的起始位置开始搜索，所以都是从 0 开始搜索的。

验证棋盘是否合法：按照以下标准去重：
- 不能同行
- 不能同理
- 不能同那个斜线（45度和135度角）

# 代码

### php
```php
class LeetCode0051 {

    public $res = [];
    public $path = [];
    
    function solveNQueens($n) {
        $board = array_fill(0, $n, str_repeat('.', $n));
        $this->_backTracking($n, 0, $board);
        return $this->res;
    }

    protected function _backTracking($n, $row, &$board) {
        if ($row == $n) {
            $this->res[] = $board;
            return;
        }
        for ($col = 0; $col < $n; $col++) {
            if ($this->_isValid($row, $col, $board, $n)) {
                $board[$row][$col] = 'Q';
                $this->_backTracking($n, $row + 1, $board);
                $board[$row][$col] = '.';
            }
        }
    }
    
    protected function _isValid($row, $col, $board, $n)
    {
        // 检查列
        for ($i = 0; $i < $row; $i++) {
            if ($board[$i][$col] == 'Q') {
                return false;
            }
        }
        // 检查 45 度角
        for ($i = $row - 1, $j = $col - 1; $i >= 0 && $j >= 0; $i--, $j--) {
            if ($board[$i][$j] == 'Q') {
                return false;
            }
        }
        // 检查 135 度角
        for ($i = $row - 1, $j = $col + 1; $i >= 0 && $j < $n; $i--, $j++) {
            if ($board[$i][$j] == 'Q') {
                return false;
            }
        }
        return true;
    }
}
```

### go
```go
func solveNQueens(n int) [][]string {
	var res [][]string
	board := make([][]string, n)
	for i := 0; i < n; i++ {
		board[i] = make([]string, n)
	}
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			board[i][j] = "."
		}
	}

	var _backtracking func(int)
	_backtracking = func(row int) {
		if row == n {
			tmp := make([]string, n)
			for i, rowStr := range board {
				tmp[i] = strings.Join(rowStr, "")
			}
			res = append(res, tmp)
			return
		}
		for col := 0; col < n; col++ {
			if _isValid(n, row, col, board) {
				board[row][col] = "Q"
				_backtracking(row + 1)
				board[row][col] = "."
			}
		}
	}

	_backtracking(0)
	return res
}

func _isValid(n, row, col int, board [][]string) bool {
	for i := 0; i < row; i++ {
		if board[i][col] == "Q" {
			return false
		}
	}
	for i, j := row - 1, col - 1; i >= 0 && j >= 0; {
		if board[i][j] == "Q" {
			return false
		}
		i--
		j--
	}
	for i, j := row - 1, col + 1; i >= 0 && j < n;  {
		if board[i][j] == "Q" {
			return false
		}
		i--
		j++
	}

	return true
}
```