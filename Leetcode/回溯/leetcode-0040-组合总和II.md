# 40. 组合总和 II

# 题目
给定一个数组 candidates 和一个目标数 target ，找出 candidates 中所有可以使数字和为 target 的组合。

candidates 中的每个数字在每个组合中只能使用一次。

注意：解集不能包含重复的组合。

https://leetcode.cn/problems/combination-sum-ii/

# 示例:
```
输入: candidates = [10,1,2,7,6,1,5], target = 8,
输出:
[
[1,1,6],
[1,2,5],
[1,7],
[2,6]
]
```

# 示例
```
输入: candidates = [2,5,2,1,2], target = 5,
输出:
[
[1,2,2],
[5]
]
```

提示:
- 1 <= candidates.length <= 100
- 1 <= candidates[i] <= 50
- 1 <= target <= 30

# 解析

## 回溯
本题和《39. 组合总和》的题目有如下区别：
- 本题的样本中的数字在每个组合中只能使用一次
- 本题的样本中有重复的元素，而第39题是无重复元素的

相比于上题，本题的输入数组可能包含重复元素，这引入了新的问题。例如，给定数组 [4, 4, 5] 和目标元素 9 ，则现有代码的输出结果为 [4, 5]、[4, 5]，
出现了重复子集。

造成这种重复的原因是相等元素在某轮中被多次选择。如下图所示，第一轮共有三个选择，其中两个都为 4 ，会产生两个重复的搜索分支，从而输出重复子集；
同理，第二轮的两个 4 也会产生重复子集。

![img1](./images/leetcode-0040-解析.png)

### 相等元素剪枝
为解决此问题，我们需要限制相等元素在每一轮中只被选择一次。实现方式比较巧妙：由于数组是已排序的，因此相等元素都是相邻的。
这意味着在某轮选择中，若当前元素与其左边元素相等，则说明它已经被选择过，因此直接跳过当前元素。

与此同时，本题规定中的每个数组元素只能被选择一次。幸运的是，我们也可以利用变量 idx 来满足该约束：当做出选择 x 后，设定下一轮从索引 i + 1 
开始向后遍历。这样即能去除重复子集，也能避免重复选择元素。


# 代码

### php
```php
class LeetCode0040 {

    public $res = [];
    public $path = [];

    function combinationSum2($nums, $target) {
        sort($nums);
        $idx = 0;
        $this->_backtrack($nums, $target, $idx);
        return $this->res;
    }

    private function _backtrack($nums, $target, $idx) {
        // 子集和等于 target 时，记录解
        if ($target == 0) {
            $this->res[] = $this->path;
            return;
        }
        // 遍历所有选择
        // 遍历所有选择
        // 剪枝二：从 start 开始遍历，避免生成重复子集
        // 剪枝三：从 start 开始遍历，避免重复选择同一元素
        for ($i = $idx; $i < count($nums); $i++) {
            // 剪枝一：若子集和超过 target ，则直接结束循环
            // 这是因为数组已排序，后边元素更大，子集和一定超过 target
            if ($nums[$i] > $target) {
                break;
            }
            // 剪枝四：如果该元素与左边元素相等，说明该搜索分支重复，直接跳过
            if ($i > $idx && $nums[$i] == $nums[$i - 1]) {
                continue;
            }
            $this->path[] = $nums[$i];
            $this->_backtrack($nums, $target - $nums[$i], $i + 1);
            array_pop($this->path);
        }
    }
}
```

### java
```java
class LeetCode0040 {
    
    public List<List<Integer>> res = new ArrayList<>();
    public List<Integer> path = new ArrayList<>();

    public List<List<Integer>> combinationSum2(int[] nums, int target) {
        Arrays.sort(nums);
        _backtrack(nums, target, 0);
        return this.res;
    }

    private void _backtrack(int[] nums, int target, int idx) {
        if (target == 0) {
            this.res.add(new ArrayList<>(this.path));
            return;
        }
        for (int i = idx; i < nums.length;i ++) {
            if (nums[i] > target) {
                break;
            }
            if (i > idx && nums[i] == nums[i - 1]) {
                continue;
            }
            this.path.add(nums[i]);
            _backtrack(nums, target - nums[i], i + 1);
            this.path.removeLast();
        }
    }
}
```

### go
```go
func combinationSum2(nums []int, target int) [][]int {
    var path []int
    var res [][]int
    sort.Ints(nums)
    _backtrack(nums, target, 0, &path, &res)
    return res
}

func _backtrack(nums []int, target int, idx int, path *[]int, res *[][]int) {
    if target == 0 {
        tmp := make([]int, len(*path))
        copy(tmp, *path)
        *res = append(*res, tmp)
        return
    }
    for i := idx; i < len(nums); i++ {
        if nums[i] > target {
            break
        }
        if i > idx && nums[i] == nums[i - 1] {
            continue;
        }
        *path = append(*path, nums[i])
        _backtrack(nums, target - nums[i], i + 1, path, res)
        *path = (*path)[: len(*path) - 1]
    }
}
```
