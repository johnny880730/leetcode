# 994. 腐烂的橘子


# 题目
在给定的 m x n 网格 grid 中，每个单元格可以有以下三个值之一：
- 值 0 代表空单元格；
- 值 1 代表新鲜橘子；
- 值 2 代表腐烂的橘子。
每分钟，腐烂的橘子 周围 4 个方向上相邻 的新鲜橘子都会腐烂。

返回 直到单元格中没有新鲜橘子为止所必须经过的最小分钟数。如果不可能，返回 -1 。

https://leetcode.cn/problems/rotting-oranges/

提示：
- m == grid.length
- n == grid[i].length
- 1 <= m, n <= 10
- grid[i][j] 仅为 0、1 或 2

# 示例
```
输入：grid = [[2,1,1],[1,1,0],[0,1,1]]
输出：4
```
```
输入：grid = [[2,1,1],[0,1,1],[1,0,1]]
输出：-1
解释：左下角的橘子（第 2 行， 第 0 列）永远不会腐烂，因为腐烂只会发生在 4 个正向上。
```
```
输入：grid = [[0,2]]
输出：0
解释：因为 0 分钟时已经没有新鲜橘子了，所以答案就是 0 。
```

# 解析

## BFS
这道题的题目要求：返回直到单元格中没有新鲜橘子为止所必须经过的最小分钟数。翻译一下，实际上就是求腐烂橘子到所有新鲜橘子的最短路径

但是用 BFS 来求最短路径的话，这个队列中第 1 层和第 2 层的结点会紧挨在一起，无法区分。因此，需要稍微修改一下代码，在每一层遍历开始前，
记录队列中的结点数量 n ，然后一口气处理完这一层的 n 个结点。

有了计算最短路径的层序 BFS 代码框架，写这道题就很简单了。这道题的主要思路是：
- 一开始，找出所有腐烂的橘子，将它们放入队列，作为第 0 层的结点。
- 然后进行 BFS 遍历，每个结点的相邻结点可能是上、下、左、右四个方向的结点，注意判断结点位于网格边界的特殊情况。
- 由于可能存在无法被污染的橘子，需要记录新鲜橘子的数量。在 BFS 中，每遍历到一个橘子（污染了一个橘子），就将新鲜橘子的数量减一。
- 如果 BFS 结束后这个数量仍未减为零，说明存在无法被污染的橘子。

# 代码

### php
```php
class LeetcodeXXXX {

    function orangesRotting($grid) {
        $len1 = count($grid);
        $len2 = count($grid[0]);
        $queue = new SplQueue();

        // 表示新鲜橘子的数量
        $fresh = 0;
        for ($i = 0; $i < $len1; $i++) {
            for ($j = 0; $j < $len2; $j++) {
                if ($grid[$i][$j] == 1) {
                    $fresh++;
                } else if ($grid[$i][$j] == 2) {
                    $queue->enqueue([$i, $j]);
                }
            }
        }

        // 表示腐烂的轮数，或者说分钟数
        $res = 0;
        // 上下左右四个邻位
        $directions = [[-1, 0], [1, 0], [0, -1], [0, 1]];
        
        while ($fresh > 0 && $queue->isEmpty() == false) {
            $res++;
            $cnt = $queue->count();
            for ($i = 0; $i < $cnt; $i++) {
                list($r, $c) = $queue->dequeue();
                // 四个方向的检验，检验通过就污染橘子
                foreach ($directions as $d) {
                    $nr = $r + $d[0];
                    $nc = $c + $d[1];

                    if ($nr >= 0 && $nr < $len1 && $nc >= 0 && $nc < $len2 && $grid[$nr][$nc] == 1) {
                        $grid[$nr][$nc] = 2;
                        $fresh--;
                        $queue->enqueue([$nr, $nc]);
                    }
                }
            }
        }

        return $fresh == 0 ? $res : -1;
    }
}
```

### java
```java
public int orangesRotting(int[][] grid) {
    int len1 = grid.length, len2 = grid[0].length;
    Queue<int[]> queue = new LinkedList<>();

    int fresh = 0;
    for (int i = 0; i < len1; i++) {
        for (int j = 0; j < len2; j++) {
            if (grid[i][j] == 1) {
                fresh++;
            } else if (grid[i][j] == 2) {
                queue.offer(new int[]{i, j});
            }
        }
    }

    int[][] directions = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}}; // 上下左右四个方向
    int res = 0;
    while (fresh > 0 && queue.isEmpty() == false) {
        res++;

        int cnt = queue.size();
        for (int i = 0; i < cnt; i++) {
            int[] orange = queue.poll();
            int r = orange[0], c = orange[1];

            for (int[] d : directions) {
                int nr = r + d[0];
                int nc = c + d[1];

                if (nr >= 0 && nr < len1 && nc >= 0 && nc < len2 && grid[nr][nc] == 1) {
                    fresh--;
                    grid[nr][nc] = 2;
                    queue.offer(new int[]{nr, nc});
                }
            }
        }
    }

    return fresh == 0 ? res : -1;
}
```

### go
```go
func orangesRotting(grid [][]int) int {
    len1, len2 := len(grid), len(grid[0])
    queue := make([][]int, 0)

    fresh := 0
    for i := 0; i < len1; i++ {
        for j := 0; j < len2; j++ {
            if grid[i][j] == 1 {
                fresh++
            } else if grid[i][j] == 2 {
                queue = append(queue, []int{i, j})
            }
        }
    }

    res := 0
    directions := [][]int{{-1, 0}, {1, 0}, {0, -1}, {0, 1}}

    for fresh > 0 && len(queue) > 0 {
        res++
        
        cnt := len(queue)
        for i := 0; i < cnt; i++ {
            orange := queue[0]
            queue = queue[1:]
            r, c := orange[0], orange[1]

            for _, d := range directions {
                nr, nc := r + d[0], c + d[1]
                if nr >= 0 && nr < len1 && nc >= 0 && nc < len2 && grid[nr][nc] == 1 {
                    grid[nr][nc] = 2
                    fresh--
                    queue = append(queue, []int{nr, nc})
                }
            }
        }
    }

    if fresh > 0 {
        return -1
    }
    return res
}
```
