# 0039. 组合总和

# 题目
给定一个无重复元素的正整数数组 candidates 和一个正整数 target ，找出 candidates 中所有可以使数字和为目标数 target 的唯一组合。

candidates 中的数字可以无限制重复被选取。如果至少一个所选数字数量不同，则两种组合是唯一的。

对于给定的输入，保证和为 target 的唯一组合数少于 150 个。

https://leetcode.cn/problems/combination-sum/description/

# 示例：
```
输入: candidates = [2,3,6,7], target = 7
输出: [[7],[2,2,3]]
```

```
输入: candidates = [2,3,5], target = 8
输出: [[2,2,2,2],[2,3,3],[3,5]]
```

```
输入: candidates = [2], target = 1
输出: []
```

```
输入: candidates = [1], target = 1
输出: [[1]]
```

```
输入: candidates = [1], target = 2
输出: [[1,1]]
```

提示：
- 1 <= candidates.length <= 30
- 1 <= candidates[i] <= 200
- candidate 中的每个元素都是独一无二的。
- 1 <= target <= 500

# 解析

## 回溯
数字可以无限制的被重复选取，但有总和的限制，所以也是有个数限制的。因为本题没有组合数量的要求，仅仅是总和的限制，
所以递归没有层数的限制，只要选取的元素总和超过 target 就返回。

确定递归函数的参数：
- result 存放结果集
- path 存放符合条件的结果
- candidates 集合 
- target 目标值
- sum 统计 path 里的总和
- idx 控制 for 循环的起始位置

确定终止情况：sum 大于 target 和 sum 等于 target，当 sum 等于 target 时需要收集结果。

确定单层搜索的逻辑：单层循环依然从 idx 开始搜索 candidates 集合。

# 代码

### php
```php
class LeetCode0039 {

    public $res = [];
    public $path = [];
    
    public function combinationSum($candidates, $target) {
        $this->_backtrack($candidates, $target, 0, 0);
        return $this->res;
    }

    protected function _backtrack($candidates, $target, $curSum, $idx) {
        // 剪枝
        if ($curSum > $target) {
            return;
        }
        if ($target == $curSum) {
            $this->res[] = $this->path;
            return;
        }
        for ($i = $idx; $i < count($candidates); $i++) {
            $curSum += $candidates[$i];
            $this->path[] = $candidates[$i];
            // 注意这里不是 i+1，表示可以重复读取当前的数
            $this->_backtrack($candidates, $target, $curSum, $i);
            $curSum -= $candidates[$i];
            array_pop($this->path);
        }

    }
}
```

### java
```java
class LeetCode0039 {

    public ArrayList<List<Integer>> res = new ArrayList<>();

    public ArrayList<Integer> path = new ArrayList<>();

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        _backtrack(candidates, target, 0, 0);
        return this.res;
    }

    private void _backtrack(int[] candidates, int target, int curSum, int idx) {
        if (curSum > target) {
            return;
        }
        if (curSum == target) {
            this.res.add(new ArrayList<>(this.path));
            return;
        }
        for (int i = idx; i < candidates.length; i++) {
            curSum += candidates[i];
            this.path.add(candidates[i]);
            _backtrack(candidates, target, curSum, i);
            curSum -= candidates[i];
            this.path.remove(this.path.size() - 1);
        }
    }
}
```

### go
```go
func combinationSum(candidates []int, target int) [][]int {
	var path []int
	var res [][]int

	_backtrack(candidates, target, 0, 0, path, &res)
	return res
}

func _backtrack(candicates []int, target int, idx int, curSum int, path []int, res *[][]int) {
	if curSum > target {
		return
	}
	if curSum == target {
		tmp := make([]int, len(path))
		copy(tmp, path)
		*res = append(*res, tmp)
		return
	}
	for i, length := idx, len(candicates); i < length; i++ {
		curSum += candicates[i]
		path = append(path, candicates[i])
		_backtrack(candicates, target, i, curSum, path, res)
		path = path[: len(path) - 1]
		curSum -= candicates[i]
	}
}
```
