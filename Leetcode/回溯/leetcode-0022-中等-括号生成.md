# 0022. 括号生成
数字 n 代表生成括号的对数，请你设计一个函数，用于能够生成所有可能的并且 有效的 括号组合。

有效括号组合需满足：左括号必须以正确的顺序闭合。

https://leetcode.cn/problems/generate-parentheses/description/

提示：
- 1 <= n <= 8

# 示例
```
输入：n = 3
输出：["((()))","(()())","(())()","()(())","()()()"]
```
```
示例 2：
输入：n = 1
输出：["()"]
```

# 解析

## 回溯
可以只在序列仍然保持有效时才添加 "(" 或者 ")"，可以通过跟踪到目前为止放置的左括号和右括号的数目来做到这一点，

如果左括号数量不大于 n，可以放一个左括号。如果右括号数量小于左括号的数量，可以放一个右括号。


# 代码

### php
```php
class LeetCode0022 {

    public $res;
    public $path;

    function generateParenthesis($n) {
        $this->_backtrack($n, 0, 0);
        return $this->res;
    }

    protected function _backtrack($n, $left, $right) {
        if (strlen($this->path) == $n * 2 ) {
            $this->res[] = $this->path;
            return;
        }
        if ($this->_isValid($n, $left + 1, $right)) {
            $this->path .= '(';
            $this->_backtrack($n, $left + 1, $right);
            $this->path = substr($this->path, 0, -1);
        }
        if ($this->_isValid($n, $left, $right + 1)) {
            $this->path .= ')';
            $this->_backtrack($n, $left, $right + 1);
            $this->path = $tmp;
        }
    }

    protected function _isValid($n, $left, $right) {
        if ($left > $n || $right > $n || $right > $left) {
            return false;
        }
        return true;
    }

}
```

### java
```java
class LeetCode0022 {

    public List<String> res = new ArrayList<>();

    public String path = "";

    public List<String> generateParenthesis(int n) {
        _backtrack(n, 0, 0);
        return res;
    }

    private void _backtrack(int n, int left, int right) {
        if (left == n && right == n) {
            this.res.add(this.path);
            return;
        }

        if (_isValid(n, left + 1, right)) {
            this.path += "(";
            _backtrack(n, left + 1, right);
            this.path = this.path.substring(0, this.path.length() - 1);
        }
        if (_isValid(n, left, right + 1)) {
            this.path += ")";
            _backtrack(n, left, right + 1);
            this.path = this.path.substring(0, this.path.length() - 1);
        }
    }

    private boolean _isValid(int n, int left, int right) {
        if (left > n || right > n || left < right) {
            return false;
        }
        return true;
    }

}
```

### go
```go
var res []string
var path string

func generateParenthesis(n int) []string {
    res = []string{}
    _backtrack(n, 0, 0)
    return res
}

func _backtrack(n int, left int, right int) {
    if left == n && right == n {
        res = append(res, path)
        return
    }
    
    if _isValid(n, left + 1, right) {
        path += "("
        _backtrack(n, left + 1, right)
        path = path[:len(path) - 1]
    }
    if _isValid(n, left, right + 1) {
        path += ")"
        _backtrack(n, left, right + 1)
        path = path[:len(path) - 1]
    }
}

func _isValid(n int, left int, right int) bool {
    if left > n || right > n || left < right {
        return false
    }
    return true
}
```
