# 131. 分割回文串

# 题目
给你一个字符串 s，请你将 s 分割成一些子串，使每个子串都是 回文串 。返回 s 所有可能的分割方案。

回文串 是正着读和反着读都一样的字符串。

https://leetcode.cn/problems/palindrome-partitioning/

提示：
- 1 <= s.length <= 16
- s 仅由小写英文字母组成

# 示例：
```
输入：s = "aab"
输出：[["a","a","b"],["aa","b"]]
```

```
输入：s = "a"
输出：[["a"]]
```

# 解析

## 回溯
本题涉及两个关键问题：
- 切割问题，即 s 有不同的切割方式
- 判断回文

切割问题，其实类似于组合问题，也可以抽象为树形结构。

确定递归函数的参数：
- path 存放切割后回文的字符串
- result 结果集
- startIndex 循环的开始位置

确定递归终止条件：切割线到了字符串最后面，说明找到了一种切割方案，此时本层递归终止。startIndex 就是切割线。

确定单层搜索的逻辑：[startIndex, i] 就是要截取的子串，判断这个子串是不是回文，如果是回文加入到 path。
利用双指针法判断字符串是否回文。

# 代码

### php
```php

class LeetCode0131 {

    public $res = [];
    public $path = [];

    public function partition($s) {
        $this->_backTracking($s, 0);
        return $this->res;
    }

    protected function _backTracking($s, $start) {
        if ($start >= strlen($s)) {
            $this->res[] = $this->path;
            return;
        }

        for ($i = $start; $i < strlen($s); $i++) {
            if (!$this->_isPalidrome($s, $start, $i)) {
                continue;
            }
            $str = substr($s, $start, $i - $start + 1);
            $this->path[] = $str;
            $this->_backTracking($s, $i + 1);
            // 回溯
            array_pop($this->path);
        }
    }

    protected function _isPalidrome($s, $start, $end)
    {
        for ($i = $start, $j = $end; $i < $j; $i++, $j--) {
            if ($s[$i] != $s[$j]) {
                return false;
            }
        }
        return true;
    }
}
```

### go
```go
func partition(s string) [][]string {
    var res [][]string
    var path []string

    _backTracking(s, 0, path, &res)
    return res
}

func _backTracking (s string, start int, path []string, res *[][]string) {
    if start >= len(s) {
        t := make([]string, len(path))
        copy(t, path)
        *res = append(*res, t)
        return
    }
    for i := start; i < len(s); i++ {
        if !_isPalidrome(s, start, i) {
            continue
        }
        str := _substr(s, start, i - start + 1)
        path = append(path, str)
        _backTracking(s, i + 1, path, res)
        path = path[:len(path) - 1]
    }
}

func _substr(str string, start int, length int) string {
    rs := []rune(str)
    rl := len(rs)
    end := 0

    if start < 0 {
        start = rl - 1 + start
    }
    end = start + length

    if start > end {
        start, end = end, start
    }

    if start < 0 {
        start = 0
    }
    if start > rl {
        start = rl
    }
    if end < 0 {
        end = 0
    }
    if end > rl {
        end = rl
    }
    return string(rs[start:end])
}

func _isPalidrome (s string, start int, end int) bool {
    for i, j := start, end; i < j; {
        if s[i] != s[j] {
            return false
        }
        i++
        j--
    }
    return true
}
```


### java
```java
class LeetCode0131 {

    private final List<List<String>> res = new ArrayList<List<String>>();

    private final List<String> path = new ArrayList<String>();

    public List<List<String>> partition(String s) {
        _backtracking(s, 0);
        return res;
    }

    protected void _backtracking(String s, int start) {
        if (start >= s.length()) {
            res.add(new ArrayList<>(path));
            return;
        }
        for (int i = start; i < s.length(); i++) {
            if (!_check(s, start, i)) {
                continue;
            }
            String str = s.substring(start, i + 1);
            path.add(str);
            _backtracking(s, i + 1);
            path.remove(path.size() - 1);
        }
    }

    protected boolean _check(String s, int start, int end) {
        for (int i = start, j = end; i < j; i++, j--) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
        }
        return true;
    }
    
}
```
