# 491. 递增子序列

# 题目
给你一个整数数组 nums ，找出并返回所有该数组中不同的递增子序列，递增子序列中 至少有两个元素 。你可以按 任意顺序 返回答案。

数组中可能含有重复元素，如出现两个整数相等，也可以视作递增序列的一种特殊情况。

https://leetcode.cn/problems/non-decreasing-subsequences/

# 示例：
```
输入：nums = [4,6,7,7]
输出：[[4,6],[4,6,7],[4,6,7,7],[4,7],[4,7,7],[6,7],[6,7,7],[7,7]]
```

```
输入：nums = [4,4,3,2,1]
输出：[[4,4]]
```


提示：
- 1 <= nums.length <= 15
- -100 <= nums[i] <= 100


# 解析
递增子序列比较像有序的子集，而且本题也要求不能有相同的递增子序列。

本题是求自增子序列，是不能对原数组进行排序的，否则排序后的数组都是自增子序列了。所以不能用 used 数组的去重逻辑。

以 [4,7,6,7] 为例，抽象后的树形图如下图所示：

![](./images/leetcode-0491-解析.png)

确定递归函数的参数：本题是求子序列，很明显一个元素不能重复使用，所以需要 startIndex，调整下一层递归的起始位置。

确定终止条件：本题其实类似于求子集问题，也要遍历树形结构并查找每一个节点，可以不加终止条件，startIndex 每次都会加 1，
并且不会无限递归。但本题收集的结果有所不同，要求递增子序列的大小至少为 2。

确定单层搜索逻辑：从上图中可以看出，同一父节点下的同一层使用过的元素就不能再使用了。

# 代码

### php
```php
class LeetCode0491 {

    public $res = [];
    public $path = [];
    
    function findSubsequences($nums) {
        $this->_backTracking($nums, [], 0);
        return $this->res;
    }

    protected function _backTracking($nums, $selected, $start) {
        if (count($selected) >= 2) {
            $this->res[implode(',', $selected)] = $selected;
            // 注意这里不要加return，因为要取树上的所有节点
        }
        for ($i = $start; $i < count($nums); ++$i) {
            if (count($selected) === 0 || $nums[$i] >= end($selected)) {
                $selected[] = $nums[$i];
                $this->_backTracking($nums, $selected, $i + 1);
                array_pop($selected);
            }
        }
    }
}
```

### go
```go
func findSubsequences(nums []int) [][]int {
	var res [][]int
	var path []int
	hash := make(map[string][]int)

	var _backtracking func(nums []int, idx int)
	_backtracking = func(nums []int, idx int) {
		if len(path) >= 2  {
			tmp := make([]int, len(path))
			copy(tmp, path)

			numArr := make([]string, 0)
			for _, v := range tmp {
				numArr = append(numArr, strconv.Itoa(v))
			}
			hashKey := strings.Join(numArr, ",")
			if _, ok := hash[hashKey]; !ok {
				hash[hashKey] = tmp
				res = append(res, tmp)
			}
		}
		for i, length := idx, len(nums); i < length; i++ {
			if len(path) == 0 || nums[i] >= path[len(path) - 1] {
				path = append(path, nums[i])
				_backtracking(nums, i + 1)
				path = path[: len(path) - 1]
			}
		}
	}

	_backtracking(nums, 0)
	return res
}
```