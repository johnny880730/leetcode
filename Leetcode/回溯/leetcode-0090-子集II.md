# 90. 子集 II

# 题目
给你一个整数数组 nums ，其中可能包含重复元素，请你返回该数组所有可能的子集（幂集）。

解集 不能 包含重复的子集。返回的解集中，子集可以按 任意顺序 排列。

https://leetcode.cn/problems/subsets-ii/

# 示例：
```
输入：nums = [1,2,2]
输出：[[],[1],[1,2],[1,2,2],[2],[2,2]]
```

```
输入：nums = [0]
输出：[[],[0]]
```


提示：
- 1 <= nums.length <= 10
- -10 <= nums[i] <= 10

# 解析
本题给的集合中有重复元素了，而且求去的子集要去重

关于回溯中的去重问题，可参考《40. 组合总和II》，是一个套路。本题就是在《78. 子集》的基础上加了去重逻辑

以 [1,2,2] 为例，去重过程如下图所示（需要先排序）

![](./images/leetcode-0090-解析.png)

去重的两个关键逻辑如下：
- 同一树层上不可以重复选取 2：i 目前是 2，nums[i] 与 nums[i-1] 相同，而 used[i - 1] 为 0，说明同一树层上有两个重复元素，不可以选取
- 同一树枝上可以重复选取 2：i 目前是 2，nums[i] 与 nums[i-1] 相同，而 used[i - 1] 为 1，说明同一树枝上有两个重复元素，可以重复选取

如果同一树层上重复取 2 就要过滤掉，同一树枝上就可以重复取 2，一位内同一树枝上元素的集合才是唯一子集。

# 代码

### php
```php
class LeetCode0090 {

    public $res = [];
    public $path = [];
    
    function subsetsWithDup($nums) {
        $used = array_fill(0, count($nums), false);
        sort($nums);
        $this->_backTracking($nums, 0, $used);
        return $this->res;
    }

    protected function _backTracking($nums, $start, &$used) {
        // 子集的话，每个节点都是结果的一部分
        $this->res[] = $this->path;
        for ($i = $start; $i < count($nums); $i++) {
            // used[i - 1] == true，说明同一树支candidates[i - 1]使用过
            // used[i - 1] == false，说明同一树层candidates[i - 1]使用过
            // 而我们要对同一树层使用过的元素进行跳过
            if ($i > 0 && $nums[$i] == $nums[$i - 1] && $used[$i - 1] == false) {
                continue;
            }
            $this->path[] = $nums[$i];
            $used[$i] = true;
            $this->_backTracking($nums, $i + 1, $used);
            $used[$i] = false;
            array_pop($this->path);
        }
    }
}
```

### go
```go
var path []int
var res [][]int
var used []bool

func subsetsWithDup(nums []int) [][]int {
	res = make([][]int, 0)
	sort.Ints(nums)
	used = make([]bool, len(nums))
	_backtracking(nums, 0)
	return res
}


func _backtracking(nums []int, idx int) {
	tmp := make([]int, len(path))
	copy(tmp, path)
	res = append(res, tmp)

	for i, length := idx, len(nums); i < length; i++ {
		if i > 0 && nums[i] == nums[i - 1] && used[i - 1] == false {
			continue
		}
		path = append(path, nums[i])
		used[i] = true
		_backtracking(nums, i + 1)
		used[i] = false
		path = path[: len(path) - 1]
	}
}
```