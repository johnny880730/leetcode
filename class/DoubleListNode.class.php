<?php

/**
 * Definition for a singly-linked list.
 */
class DoubleListNode
{
    public $val = 0;
    public $left = null;
    public $right = null;

    public function __construct($data)
    {
        $this->val = $data;
    }

}

// 打印双向链表
function printDoubleLinkedList($head)
{
    echo "Double Linked List: ";
    $end = null;
    while ($head != null) {
        echo $head->value . ' ';
        $end  = $head;
        $head = $head->next;
    }
    echo "| ";
    while ($end != null) {
        echo $end->value . ' ';
        $end = $end->last;
    }
    echo PHP_EOL;
}

