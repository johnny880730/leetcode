<?php

$num = 9;
$leiNum = 10;

$map = initMap($num, $leiNum);

foreach ($map as $line) {
    echo join(', ', $line) . PHP_EOL;
}

function initMap($num, $leiNum) {
    $map = array_fill(0, $num, array_fill(0, $num, 0));
    $cache = [];
    for ($i = 0; $i < $leiNum; $i++) {
        while (true) {
            $x = random_int(0, $num - 1);
            $y = random_int(0, $num - 1);
            if (isset($cache[$x][$y])) {
                continue;
            }
            // 缓存
            $cache[$x][$y] = true;

            break;
        }
        $map[$x][$y] = -1;
        // 设置周围的数字加一
        _setNum($map, $x, $y, $num);
    }
    return $map;
}

function _setNum(&$map, $x, $y, $num) {
    $flag1 = $x - 1 >= 0;
    $flag2 = $y - 1 >= 0;
    $flag3 = $x + 1 < $num;
    $flag4 = $y + 1 < $num;

    // 上
    if ($flag1 && $map[$x - 1][$y] >= 0) {
        $map[$x - 1][$y]++;
    }
    // 左
    if ($flag2 && $map[$x][$y - 1] >= 0) {
        $map[$x][$y - 1]++;
    }
    // 下
    if ($flag3 && $map[$x + 1][$y] >= 0) {
        $map[$x + 1][$y]++;
    }
    // 右
    if ($flag4 && $map[$x][$y + 1] >= 0) {
        $map[$x][$y + 1]++;
    }
    // 左上
    if ($flag1 && $flag2 && $map[$x - 1][$y - 1] >= 0) {
        $map[$x - 1][$y - 1]++;
    }
    // 右上
    if ($flag1 && $flag4 && $map[$x - 1][$y + 1] >= 0) {
        $map[$x - 1][$y + 1]++;
    }
    // 左下
    if ($flag3 && $flag2 && $map[$x + 1][$y - 1] >= 0) {
        $map[$x + 1][$y - 1]++;
    }
    // 右下
    if ($flag3 && $flag4 && $map[$x + 1][$y + 1] >= 0) {
        $map[$x + 1][$y + 1]++;
    }
}