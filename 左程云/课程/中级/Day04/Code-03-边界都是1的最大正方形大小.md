# 边界都是1的最大正方形大小

# 题目
给定一个NN的矩阵matrix，在这个矩阵中，只有0和1两种值，返回边框全是1的最大正方形的边长长度。

# 示例
```
 0 1 1 1 1
 0 1 0 0 1
 0 1 0 0 1
 0 1 1 1 1
 0 1 0 1 1
 
其中，边框全是1的最大正方形的大小为4*4，所以返回4。
```

# 解析
一个 N × N 的矩阵：
- 包含的长方形数量的数量级为 O(N ^ 4)。左上角的点数量级为 O(N ^ 2)，右下角的点数量级为 O(N ^ 2)，长方形数量级就是 O(N ^ 4)。
- 包含的正方形数量的数量级为 O(N ^ 3)。左上角的点数量级为 O(N ^ 2)，边长数量级为 O(N)，所以正方形数量级为 O(N ^ 3)。

如何在 O(1) 的时间里验证一个已知起点(x, y) 和边长 n 的正方形是否都是 1 呢？
- 定义二维数组 r[i][j]，表示包含点(i, j) 右方出现连续的 1 的数量（如果 m[i][j] == 0 则直接填 0）
- 定义二维数组 d[i][j]，表示包含点(i, j) 下方出现连续的 1 的数量（如果 m[i][j] == 0 则直接填 0）
- 从起始点开始，用这两个预处理数组来验证边长是否足够 n 个 1，那么就可以在 O(1) 里确认是否存在符合要求的正方形



# 代码
```php
$matrix = [
    [0, 1, 1, 1, 1,],
    [0, 1, 0, 0, 1,],
    [0, 1, 0, 0, 1,],
    [0, 1, 1, 1, 1,],
    [0, 1, 0, 1, 1,],
];
$obj = new Code_03_MaxBorder();
echo $obj->main();

class Code_03_MaxBorder {

    public function main($matrix, $k) {
        $len = count($matrix);
        $right = array_fill(0, $len, array_fill(0, $len, 0));
        $down = array_fill(0, $len, array_fill(0, $len, 0));
        $this->_setBorderMap($matrix, $right, $down);
        
        for ($size = min(count($matrix), count($matrix[0])); $size != 0; $size--) {
            if ($this->_hasSizeOfBorder($size, $right, $down)) {
                return $size;
            }
        }
        return 0;
    }
    
    // 初始化 down  right 数组
    protected function _setBorderMap($m, &$right, &$down) {
        $r = count($m);
        $c = count($m[0]);
        
        if ($m[$r - 1][$c - 1] == 1) {
            $right[$r - 1][$c - 1] = 1;
            $down[$r - 1][$c - 1] = 1;
        }
        for ($i = $r - 2; $i != -1; $i--) {
            if ($m[$i][$c - 1] == 1) {
                $right[$i][$c - 1] = 1;
                $down[$i][$c - 1] = $down[$i + 1][$c - 1] + 1;
            }
        }
        for ($i = $c - 2; $i != -1; $i--) {
            if ($m[$r - 1][$i] == 1) {
                $right[$r - 1][$i] = $right[$r - 1][$i + 1] + 1;
                $down[$r - 1][$i] = 1;
            }
        }
        for ($i = $r - 2; $i != -1; $i--) {
            for ($j = $c - 2; $j != -1; $j--) {
                if ($m[$i][$j] == 1) {
                    $right[$i][$j] = $right[$i][$j + 1] + 1;
                    $down[$i][$j] = $down[$i + 1][$j] + 1;
                }
            }
        }
    }
    
    protected function _hasSizeOfBorder($size, $right, $down) {
        for ($i = 0; $i != count($right) - $size + 1; $i++) {
            for ($j = 0; $j != count($right[0]) - $size + 1; $j++) {
                if ($right[$i][$j] >= $size && $down[$i][$j] >= $size && 
                    $right[$i + $size - 1] >= $size && $down[$i][$j + $size - 1] >= $size) {
                    
                    return true;
                }
            }
        }
        return false;
    }
}
```