<?php
/*
 * 调整[0,x)区间上的数出现的概率
 * 【题目】
 * 假设函数Math.random()等概率随机返回一个在[0,1)范围上的数
 * 那么我们知道，在[0,x)区间上的数出现的概率为x（0<x≤1）。
 * 给定一个大于0的整数k，并且可以使用Math.random()函数，
 * 请实现一个函数依然返回在[0,1)范围上的数，但是在[0,x)区间上的数出现的概率为x的k次方(0<x≤1)
 */
$k = 2;
$obj = new Code_01_ChangeRandom();
$obj->main($k);

class Code_01_ChangeRandom
{
    public function main($k)
    {
        $range = 0.5;
        $count1 = $count2 = 0;
        $times = 1000000;
        for ($i = 0; $i < $times; $i++) {
            if (lcg_value() < $range) {
                $count1++;
            }
            if ($this->randXPower($k) < $range) {
                $count2++;
            }
        }
        echo "range = [0, $range), res1 = " . ($count1 / $times) . " res2 = " .($count2 / $times) . PHP_EOL;
    }

    protected function randXPower($k)
    {
        if ($k < 1) {
            return 0;
        }
        $res = -1;
        // 如果k=3，就是取3次随机数，返回最大的，3次都符合<range的才计入count，也就是range的k次方
        for ($i = 0; $i < $k; $i++) {
            $res = max($res, lcg_value());
        }
        return $res;
    }
}