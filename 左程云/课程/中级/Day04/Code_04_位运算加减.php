<?php
/*
 * 只用位运算不用算术运算实现整数的加减乘除运算
 * 【题目】
 * 给定两个32位整数a和b，可正、可负、可0。
 * 不能使用算术运算符，分别实现a和b的加减乘除运算。
 * 【要求】
 * 如果给定的a和b执行加减乘除的某些结果本来就会导致数据的溢出，那么你实现的函数不必对那些结果负责
 */

$a = -24; $b = 7;
$obj = new Code_04_BitMath();
$obj->main($a, $b);

class Code_04_BitMath
{
    public function main($a, $b)
    {
        $res1 = $this->add($a, $b);
        echo "$a + $b = $res1" . PHP_EOL;
        $res2 = $this->minus($a, $b);
        echo "$a - $b = $res2" . PHP_EOL;
    }

    public function add($a, $b)
    {
        $sum = $a;
        while ($b != 0) {
            $sum = $a ^ $b;
            $b = ($a & $b) << 1;
            $a = $sum;
        }
        return $sum;
    }

    // 相反数：取反+1
    public function negNum($n)
    {
        return $this->add(~$n, 1);
    }

    // 减法：x + y的相反数
    public function minus($x, $y)
    {
        return $this->add($x, $this->negNum($y));
    }


}