# 将数组切成四个等分部分

# 题目
给定一个正数数组 arr，返回该数组能不能分成 4 个部分，并且每个部分的累加和相等，切分位置的数不要。

# 示例
```
arr = [3, 2, 4, 1, 4, 9, 5, 10, 1, 2, 2]
返回true
三个切割点下标为2, 5, 7. 四个子数组为[3,2], [1,4], [5], [1,2,2]
```

# 解析
定义一个哈希表 map，key 是 arr[0..i] 的前缀和，val 是位置 i。

用三个切割点话分割成四个部分，所以数组长度起码为 7。

首先看第一刀的位置：第一刀不能从位置 0 开始，因为左边没数字了，因此从 1 开始。那么第一刀最远能到哪里呢？最远能到 N - 6。
第一刀是 N - 6 的话，第二刀是 N - 4，第三刀 N - 2，这样符合题目要求的四个部分的要求。因此第一刀的范围是 [1, N - 6]。

假设第一刀的位置是 i，用 leftSum 记录它左边的和，假设 leftSum = x 且 arr[i] = a，如何确定有第二刀呢？必须要有个部分的累加和也是 x，
形成 [x, a, x] 的布局，这样才能下第二刀。也就是必须有个累加和是 2x + a，如果 map 里没有 2x + a 的项，就做不了第二刀了。

假设能在map 里查到 2x + a 的位置，定义 map[2x + a] = j 也就是 j 位置。定义 arr[j] = b，那么同样看第三刀的情况就是 3x + a + b。
如果没有，就说明 i 位置是第一刀的情况下没有第三刀，要重新看第一刀了。

同样假设第三刀在 k 位置，看看最后剩下部分的累加和是不是 x，如果是 x 就说明 i 位置是第一刀的位置了。

![](./images/Day06-Code02-img1.png)

由于遍历的是 i 位置，所以整个流程的复杂度为 O(N)

# 代码

### php
```php
class Middle_Day06_Code_02 {

    public function main($arr) {
        // 分成4个部分，加上三个切割点，至少要7个数字
        $len = count($arr);
        if ($len < 7) {
            return false;
        }
        $hash = array();
        $sum = $arr[0];
        // hash的key是0~i的和，value是i
        for ($i = 1; $i < $len; $i++) {
            $hash[$sum] = $i;
            $sum += $arr[$i];
        }
        $leftSum = $arr[0];
        for ($s1 = 1; $s1 <= $len - 6; $s1++) {
            // 第一刀位置是s1,那么arr[s1]的左侧和右侧都必须是$leftSum，那么检查hash里是否有leftSum+arr[s1]+leftSum
            $checkSum = $leftSum * 2 + $arr[$s1];
            if (array_key_exists($checkSum, $hash)) {
                // 第二刀位置是s2,同理检测第三刀的s3
                $s2 = $hash[$checkSum];
                $checkSum += $leftSum + $arr[$s2];
                if (array_key_exists($checkSum, $hash)) {
                    $s3 = $hash[$checkSum];
                    // 第三刀的右侧就是数组最后部分
                    if ($checkSum + $arr[$s3] + $leftSum == $sum) {
                        return true;
                    }
                }
            }
            $leftSum += $arr[$s1];
        }
        return false;
    }
}
```