# range缺的数字

# 题目
给定一个有序的正数数组 arr 和一个正数 range。如果可以自由选择 arr 中的数字，想累加得到 1 ~ range 范围上所有的数，返回 arr 最少还缺几个数。

# 示例
```
arr = {1,2,3,7}，range = 15
想累加得到 1~15 范围上所有的数，arr 还缺 14 这个数，所以返回1
```

```
arr = {1,5,7}，range = 15
想累加得到 1~15 范围上所有的数，arr 还缺 2 和 4，所以返回2
```

# 解析
先考虑这个问题，如果 arr 是空的，range = 100，缺几个数？缺 [1, 2, 4, 8, 16, 32, 64] 这七个数。
- 只有 1，range 范围是 [1 ~ 1]
- 补上 2，range 范围是 [1 ~ 3]
- 补上 4，range 范围是 [1 ~ 7]
- 补上 8，range 范围是 [1 ~ 15]
- 补上 16，range 范围是 [1 ~ 31]
- 补上 32，range 范围是 [1 ~ 63]
- 补上 64，range 范围是 [1 ~ 127]
- 已经超过 100 了不用再补了

这样就得到一个结论：如果已经实现了 [1 ~ range] 所有数字的目标，但还没达到 [1 ~ aim] 的目标，那么缺的就是 range + 1 这个数，这个数是最经济的数。
补上了 range + 1，就能实现 [1 ~ 2 × range + 1] 的范围。

举例，arr = [7, 4, 13, 29]，range = 56。先给排个序得到 arr = [4, 7, 13, 29]。

为了最经济的用上 arr[0] = 4 这个数字，要先完成 [1 ~ 3] 这个小目标，显而易见没法完成。

当前的 range = 0，补数字 1 之后 range 更新为 [1 ~ 1]；补数字 2 之后 range 更新为 [1 ~ 3]。此时完成了 [1 ~ 3] 这个范围的小目标了，
可以最经济的使用这个 4 了，这时候 range 更新为 [1 ~ 7] 了。

接下来怎么最经济的使用 arr[1] = 7 呢，根据上文可知要事先完成 [1 ~ 6] 这个小目标。由于当前 range 已经是 [1 ~ 7] 了，所以这个小目标是可以满足的。
可以最经济的使用这个 7 了。这时候 range 更新为 [1 ~ 14] 了

接下来怎么最经济的使用 arr[2] = 13 呢，根据上文可知要事先完成 [1 ~ 12] 这个小目标。由于当前 range 已经是 [1 ~ 14] 了，所以这个小目标是可以满足的。
可以最经济的使用这个 13 了。这时候 range 更新为 [1 ~ 27] 了

接下来怎么最经济的使用 arr[2] = 29 呢，根据上文可知要事先完成 [1 ~ 28] 这个小目标。由于当前 range 是 [1 ~ 27]，这个小目标是不满足的，
所以要补数字 28。补上 28 后 range 更新为 [1 ~ 55]，这时候再用上 29 这个数字后 range 更新为 [1 ~ 84] 符合题目要求了。
因此最后要补三个数字：1、2、28。

总结：遍历时来到数字 a，要确保实现 [1 ~ a - 1] 能完成。如果当前 [1 ~ curRange] 无法达到这个目标就要补数字。


# 代码

```php
$arr = [1,2,3,7];
$n = 15;
$obj = new Middle_Day06_Code_05();
$res = $obj->minPatches($arr, $n);
var_dump($res);


class Middle_Day06_Code_05 {

    public function minPatches($arr, $aim) {
        sort($arr);
        $len = count($arr);
        $res = 0;           //缺多少个数字
		$curRange = 0;      //已经完成了 1 ~ curRange
		for ($i = 0; $i != $len; $i++) {
		    // 想要使用 arr[i]，就要先实现 curRange = [1 ~ arr[i]-1] 这个目标
            while ($arr[$i] - 1 > $curRange) {
                $curRange += $curRange + 1;   // curRange + 1 就是缺的数字
                $res++;
                if ($curRange >= $aim) {      // 只要 curRange 增加就要盯着是否达到了aim
                    return $res;
                }
            }
            // arr[i] 可以加到 curRange 里去扩充范围
            $curRange += $arr[$i];
            if ($curRange >= $aim) {
                return $res;
            }
        }
		// 遍历完数组还没到目标的情况
		while ($aim >= $curRange + 1) {
            $curRange += $curRange + 1;
            $res++;
        }
		return $res;
    }

}
```