# 局部排序让数组有序

# 题目
给定一个无序数组arr，如果只能对一个子数组进行排序，但是想让数组整体都有序，求需要排序的最短子数组长度。

# 示例
```
arr = [1,5,3,4,2,6,7]
返回 4，因为只有 [5,3,4,2] 需要排序
```

# 解析
第一次从左往右遍历，关注：
- 比较当前数 cur 和左侧划过数的最大数 maxLeft
- cur >= maxLeft 看作 true, 否则为false
- 记录结果 false 的最右侧的元素  

第二次从右到左遍历，关注：
- 比较当前数 cur 和右侧划过数的最小数 minRight
- cur <= minRight 看作 true，否则为 false
- 记录结果 false 的最左侧的元素

需要排序的就是 第二步的左边界 到 第一步的右边界的范围

# 代码
```php
$arr=[1,5,3,4,2,6,7];
$obj = new Middle_Day06_Code_03();
$res = $obj->main($arr);
var_dump($res);

class Middle_Day06_Code_03 {

    public function main($arr) {
        $len = count($arr);

        $max = $arr[0];
        $noMaxIndex = -1;
        for ($i = 1; $i != $len; $i++) {
            if ($arr[$i] < $max) {
                $noMaxIndex = $i;
            } else {
                $max = max($max, $arr[$i]);
            }
        }

        $min = $arr[$len - 1];
		$noMinIndex = -1;
		for ($i = $len - 2; $i != -1; $i--) {
            if ($arr[$i] > $min) {
                $noMinIndex = $i;
            } else {
                $min = min($min, $arr[$i]);
            }
        }
		if ($noMinIndex == -1) {
            return 0;
        }

        return $noMaxIndex - $noMinIndex + 1;
    }
}
```