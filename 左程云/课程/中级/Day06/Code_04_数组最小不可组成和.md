# 数组最小不可组成和 

# 题目 
给定一个正数数组 arr，其中所有的值都为整数，以下是最小不可组成和的概念:
- 把 arr 每个子集内的所有元素加起来会出现很多值，
- 其中最小的记为 min，最大的记为 max
- 在区间 [min, max]上，如果有数不可以被 arr 某一个子集相加得到，那么其中最小的那个数是 arr 的最小不可组成和
- 在区间 [min, max]上，如果所有的数都可以被 arr 的某一个子集相加得到，那么 max + 1 是 arr 的最小不可组成和

请写函数返回正数数组 arr 的最小不可组成和。

进阶：如果已知正数数组 arr 中肯定有 1 这个数，是否能更快地得到最小不可组成和?

# 示例
```
arr = [3, 2, 5]
子集 {2} 相加产生 2 为 min，子集 {3, 2, 5} 相加产生 10 为 max。
在区间 [2, 10] 上，4、6 和 9 不能被任何子集相加得到，其中 4 是 arr 的最小不可组成和。
```

```
arr = [1, 2, 4]
子集 {1} 相加产生 1 为 min，子集 {1, 2, 4} 相加产生 7 为 max。
在区间 [1, 7] 上，任何 数都可以被子集相加得到，所以 8 是 arr 的最小不可组成和。
```
  
# 解析
类似背包问题：构建一张 len * (sum + 1) 的 dp 表，值为布尔类型。

dp[i][j] 表示 arr[0...i] 里的数字自由组合，能否累加出 j。那么最后一行就代表数组的所有子集能否获得相应的和，
从 min 开始第一个为 false 的位置就是答案。

dp[i][j]中：
1. arr[i] == j，必为 true（表示只取自己作为子集）
2. arr[i] != j && 不用 arr[i], 看 dp[i - 1][j] 是不是 true
3. arr[i] != j && 采用 arr[i], 看 dp[i - 1][j - arr[i]] 是不是 true

三种情况有一种为 true 那么 dp[i][j] 就是 true。

---

进阶情况。

排序 O(NlogN) 的代价，数组第一个必定是 1。

一个变量 range，表示 1 ~ range 的范围的数都能累加获得。

当前数 cur，比较 cur > range + 1 是否成立：
- 没大于：当前数加到 range 上去，扩充了可获得的累加和
- 大于了：超过了累加和的界限，就是答案，直接返回 range + 1

如果没有一个大于 range + 1的情况，结果也就是 range + 1


# 代码

```php
$arr = [3, 2, 5];
$obj = new Middle_Day06_Code_04();
$res = $obj->unformedSum($arr);
var_dump($res);

// 进阶
$arr = [1,2,4];
$res = $obj->unformedSum2($arr);
var_dump($res);

class Middle_Day06_Code_04 {

    public function unformedSum($arr) {
        $len = count($arr);
        $sum = array_sum($arr);

        $dp = array_fill(0, $len, array_fill(0, $sum + 1, false));
        // 只取第一个数的情况，只有sum==$arr[0]情况下是true
        $dp[0][$arr[0]] = true;     //dp[0][3] = true
        for ($i = 1; $i < $len; $i++) {
            // 由于数组都是正数，第0列也就是sum为0不用考虑
            for ($j = 1; $j <= $sum; $j++) {
                if ($arr[$i] == $j) {
                    $dp[$i][$j] = true;
                } else if ($dp[$i - 1][$j] == true) {
                    $dp[$i][$j] = true;
                } else if ($j - $arr[$i] >= 0 && $dp[$i - 1][$j - $arr[$i]] == true) {
                    $dp[$i][$j] = true;
                }
            }
        }
        // 只使用最后一行的结果，从arr的最小值开始遍历
        $ans = min($arr);
        for (; $ans <= $sum; $ans++) {
            if (!$dp[$len - 1][$ans]) {
                return $ans;
            }
        }
        return $sum + 1;
    }

    // 进阶
    public function unformedSum2($arr) {
        sort($arr);
        $len = count($arr);
        // arr[0] 肯定是 1
        $range = 1;
		for ($i = 1; $i != $len; $i++) {
            if ($arr[$i] > $range + 1) {
                return $range + 1;
            } else {
                $range += $arr[$i];
            }
        }
		return $range + 1;
    }
}
```