# 可整合数组

# 题目
先给出可整合数组的定义：如果一个数组在排序之后，每相邻两个数差的绝对值都为 1，则该数组为可整合数组。

给定一个整型数组 arr，请返回最大可整合子数组的长度。

# 示例
```
arr = [5, 5, 3, 2, 6, 4, 3]
最大可整合子数组为 [5,3,2,6,4]，所以返回 5。
```

# 解析
可整合数组的特性：
1. 无重复
2. max - min = 个数 - 1

如 [0, 3, 2, 1, 4, 5]，max = 5，min = 0，5 - 0 = 6 - 1。

复杂度 O(n²)


# 代码
```php
$arr = [5,5,3,2,6,4,3];
$obj = new Middle_Day07_Code_01();
$res = $obj->main($arr);
var_dump($res);

class Middle_Day07_Code_01 {
    
    public function main($arr) {
        $res = 0;
        $len = count($arr);
        for ($L = 0; $L < $len; $L++) {     //目前以L开头的子数组
            $set = array();
            $max = PHP_INT_MIN;
            $min = PHP_INT_MAX;
            for ($R = $L; $R < $len; $R++) {    
                //arr[L..R] 验证这个子数组
                if (array_key_exists($arr[$R], $set)) {
                    // 出现重复值，肯定是不可整合的，不用再试了
                    break;
                }
                $set[$arr[$R]] = true;
                $max = max($max, $arr[$R]);
                $min = min($min, $arr[$R]);
                if ($max - $min == $R - $L ) {
                    $res = max($res, $R - $L + 1);
                }
            }
        }
        return $res;
    }
}
```