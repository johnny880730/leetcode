<?php
/*
 * 给定一个有序数组arr，和一个整数aim，请不重复打印arr中所有累加和为aim的二元组。
 * 给定一个有序数组arr，和一个整数aim，请不重复打印arr中所有累加和为aim的三元组。
 */

$arr = [-10,1,1,2,2,2,4,4,4,5,5,5,7,7,10,10,13];
$aim = 9;
$obj = new Middle_Day07_Code_03();
$obj->main1($arr, $aim);
$obj->main2($arr, $aim);


class Middle_Day07_Code_03
{
    // 二元的
    public function main1($arr, $aim)
    {
        $res = [];
        $len = count($arr);
        if ($arr == null || $len  < 2) {
            return;
        }
        $left = 0;
		$right = $len - 1;
		while ($left < $right) {
            if ($arr[$left] + $arr[$right] < $aim) {
                $left++;
            } else if ($arr[$left] + $arr[$right] > $aim) {
                $right--;
            } else {
                if ($left == 0 || $arr[$left - 1] != $arr[$left]) {
                    $res[] = [$arr[$left], $arr[$right]];
                }
                $left++;
                $right--;
            }
        }
        print_r($res);
    }

    // 三元的
    public function main2($arr, $aim)
    {
        $len = count($arr);
        if ($arr == null || $len < 3) {
            return;
        }
        // 以第i个为数为基准，获取剩下数组的二元组
        for ($i = 0; $i < $len - 2; $i++) {
            if ($i == 0 || $arr[$i] != $arr[$i - 1]) {
                $this->_printRest($arr, $i, $i + 1, $len-1, $aim - $arr[$i]);
            }
        }
    }

    protected function _printRest($arr, $f, $l, $r, $aim)
    {
        $res = [];
        while ($l < $r) {
            if ($arr[$l] + $arr[$r] < $aim) {
                $l++;
            } else if ($arr[$l] + $arr[$r] > $aim) {
                $r--;
            } else {
                if ($l == $f + 1 || $arr[$l - 1] != $arr[$l]) {
                    $res[] = [$arr[$f], $arr[$l], $arr[$r]];
                }
                $l++;
                $r--;
            }
        }
        if ($res) {
            print_r($res);
        }
    }

}