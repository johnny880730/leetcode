# 最长递增路径

# 题目
给定一个整型矩阵 nums，每个位置都可以走向左、右、上、下四个方向，找到其中最长的递增路径

题目同 leetcode 第 0329 题 [矩阵中的最长递增路径](/Leetcode/top-interview-questions/leetcode-0329-困难-矩阵中的最长递增路径.md)

# 示例
```
nums = [
  [9,9,4],
  [6,6,8],
  [2,1,1]
]
输出：4 最长的递增路径为：[1, 2, 6, 9]
```

```
nums = [
  [3,4,5],
  [3,2,6],
  [2,2,1]
]
输出：4 最长的递增路径为：[3, 4, 5, 6]
```

# 代码
```php
$arr = [
    [9,9,4],
    [6,6,8],
    [2,1,1],
];
$obj = new Middle_Day07_Code_04();
$obj->main1($arr);
$obj->main2($arr);

class Middle_Day07_Code_04 {

    // 递归
    public function main1($matrix) {
        $N = count($matrix);
        $M = count($matrix[0]);
        $res = PHP_INT_MIN;
        $t1 = microtime(true);
        for ($row = 0; $row < $N; $row++) {
            for ($col = 0; $col < $M; $col++) {
                // 任何一个row行col列的位置都出发一遍
                $res = max($res, $this->_getLongestIncreasing1($matrix, $row, $col));
            }
        }
        $t2 = microtime(true);
        echo $res . ' time = ' . ($t2-$t1) . PHP_EOL;
    }

    // 从matrix[row][col]触发，能够得到的最长递增链，返回长度
    protected function _getLongestIncreasing1($matrix, $row, $col) {
        // 尝试能不能走上
        $nextUp = 0;
        if ($row - 1 >= 0 && $matrix[$row - 1][$col] > $matrix[$row][$col]) {
            $nextUp = $this->_getLongestIncreasing1($matrix, $row - 1, $col);
        }
        // 尝试能不能走下
        $nextDown = 0;
        if ($row + 1 < count($matrix) && $matrix[$row + 1][$col] > $matrix[$row][$col]) {
            $nextDown = $this->_getLongestIncreasing1($matrix, $row + 1, $col);
        }
        // 尝试能不能走左
        $nextLeft = 0;
        if ($col - 1 >= 0 && $matrix[$row][$col - 1] > $matrix[$row][$col]) {
            $nextLeft = $this->_getLongestIncreasing1($matrix, $row, $col - 1);
        }
        // 尝试能不能走右
        $nextRight = 0;
        if ($col + 1 < count($matrix[0]) && $matrix[$row][$col + 1] > $matrix[$row][$col]) {
            $nextRight = $this->_getLongestIncreasing1($matrix, $row, $col + 1);
        }

        return max($nextUp, $nextDown, $nextLeft, $nextRight) + 1;
    }

    // dp缓存（记忆化搜索）
    public function main2($matrix) {
        $N = count($matrix);
        $M = count($matrix[0]);
        $res = PHP_INT_MIN;
        $dp = array_fill(0, $N, array_fill(0, $M, 0));
        $t1 = microtime(true);
        for ($row = 0; $row < $N; $row++) {
            for ($col = 0; $col < $M; $col++) {
                // 任何一个row行col列的位置都出发一遍
                $res = max($res, $this->_getLongestIncreasing2($matrix, $row, $col, $dp));
            }
        }
        $t2 = microtime(true);
        echo $res . ' time = ' . ($t2-$t1) . PHP_EOL;
    }

    // 从matrix[row][col]触发，能够得到的最长递增链，返回长度
    // dp[i][j] != 0 表示已经算过了
    protected function _getLongestIncreasing2($matrix, $row, $col, &$dp) {
        if ($dp[$row][$col] != 0) {
            return $dp[$row][$col];
        }

        // 尝试能不能走上
        $nextUp = 0;
        if ($row - 1 >= 0 && $matrix[$row - 1][$col] > $matrix[$row][$col]) {
            $nextUp = $this->_getLongestIncreasing2($matrix, $row - 1, $col, $dp);
        }
        // 尝试能不能走下
        $nextDown = 0;
        if ($row + 1 < count($matrix) && $matrix[$row + 1][$col] > $matrix[$row][$col]) {
            $nextDown = $this->_getLongestIncreasing2($matrix, $row + 1, $col, $dp);
        }
        // 尝试能不能走左
        $nextLeft = 0;
        if ($col - 1 >= 0 && $matrix[$row][$col - 1] > $matrix[$row][$col]) {
            $nextLeft = $this->_getLongestIncreasing2($matrix, $row, $col - 1, $dp);
        }
        // 尝试能不能走右
        $nextRight = 0;
        if ($col + 1 < count($matrix[0]) && $matrix[$row][$col + 1] > $matrix[$row][$col]) {
            $nextRight = $this->_getLongestIncreasing2($matrix, $row, $col + 1, $dp);
        }

        $res = max($nextUp, $nextDown, $nextLeft, $nextRight) + 1;
        $dp[$row][$col] = $res;
        return $res;
    }
}
```