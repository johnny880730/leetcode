<?php
/*
 * 一个人开始在(0,0)
 * 总是第一次走向上方，第二次走向左方，第三次走向下方，第四次走向右方，
 * 第五次走向上方，第六次走向左方，第七次走向下方，第八次走向右方，
 * 第九次走向上方...依次走下去。
 * 给定一个数组arr，arr[i]表示第i次走的步数。
 * 请返回，按照数组的步数走，会不会让走过的路径交叉在一起。
 */

$arr = [];
$obj      = new Code_04_SelfCrossing();
var_dump($obj->do($arr));

class Code_04_SelfCrossing
{
    public function do($arr)
    {
        $len = count($arr);
        // 只有三条边 肯定不交叉
        if ($arr == null || $len < 4) {
            return false;
        }
        if ( ($len > 3 && $arr[2] <= $arr[0] && $arr[3] >= $arr[1])     // 左边<=右边 且 下边>=上边，必定相交
             ||
             ($len > 4 &&
                 (
                    ($arr[3] <= $arr[1] && $arr[4] >= $arr[2])           // 下边<=上边 && 第二个右边比第一个左边长，就穿过了第一个上边
                    ||
                    ($arr[3] == $arr[1] && $arr[0] + $arr[4] >= $arr[2]) // 第一个上边下边相等，第二个右边+第一个右边>=第一个左边
                 )
             )
        ) {
            return true;
        }
        for ($i = 5; $i < $len; $i++) {
            if (
                $arr[$i - 1] <= $arr[$i - 3]
                && (
                        ($arr[$i] >= $arr[$i - 2])
                        ||
                        (
                            $arr[$i - 2] >= $arr[$i - 4]
                            &&
                            $arr[$i - 5] + $arr[$i - 1] >= $arr[$i - 3]
                            &&
                            $arr[$i - 4] + $arr[$i] >= $arr[$i - 2]
                        )
                )
            ) {
                return true;
            }
        }
		return false;
    }

}