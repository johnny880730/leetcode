<?php
/*
 * 如果str1和str2包含的字符种类一样，并且每种字符的个数也一样，那么str1和str2算作变形词。
 * 给定一个字符类型的数组，请把变形词分组。
 * 比如输入：["eat", "tea", "tan", "ate", "nat", "bat"]
 * 输出：
 * [
 *   ["ate", "eat","tea"],
 *   ["nat","tan"],
 *   ["bat"]
 * ]
 * 注意：所有的字符都是小写。
 */

$list = ["eat", "tea", "tan", "ate", "nat", "bat"];
$obj  = new Code_02_GroupAnagrams();
var_dump($obj->do($list));

class Code_02_GroupAnagrams
{
    // 哈希表
    public function do($list)
    {
        $hash = [];
        foreach ($list as $str) {
            $chs = str_split($str);
            sort($chs);
            $s = join('', $chs);
            $hash[$s][] = $str;
        }
        return array_values($hash);
    }


}