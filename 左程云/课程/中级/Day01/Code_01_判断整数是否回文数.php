<?php
/*
 * 给定一个整数，判断该数是否是回文数
 */
$n   = 1234321;
$obj = new Code_01_PalindromeNumber();
var_dump($obj->check($n));

class Code_01_PalindromeNumber
{
    public function check($n)
    {
        if ($n < 0) {
            return false;
        }
        // help用来确定最高位  【1234321 的 helper = 1000000】
        $help = 1;
		while (intval($n / $help) >= 10) {
            $help *= 10;
        }
		while ($n != 0) {
		    // n/help就是最高位；n%10就是最低位，判断最高位和最低为是否相等
            if (intval($n / $help) != $n % 10) {
                return false;
            }
            // 这一步可以去掉最高位和最低位 【比如：(1234321 % 1000000) / 10 =234321 / 10 = 23432】
            $n = intval(($n % $help) / 10);
            // 去掉了两位数字help也减两位
            $help /= 100;
        }
		return true;
    }
}