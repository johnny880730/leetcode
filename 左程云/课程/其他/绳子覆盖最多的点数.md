# 题目
给定一个有序数组 arr，代表落在 X 轴上的点。给定一个正整数 K，代表绳子的长度。
返回绳子最多压中几个点？即使绳子边缘盖住点也算盖住

## 示例
```
arr = [1,3,4,7,13,16,17]; K = 4
最多压住 3 个点
```

# 解析

## 方法一：贪心

以每个点作为绳子的末尾点，绳子往左推盖住几个点。因为绳子末尾处没必要放在一个不是点的位置。

假设当前一个点为 973，绳子长为 100，那么这个绳子的起始点就是 973 - 100 = 873，那么只要查出 973 左侧有多少点是大于等于 873 的，
结果再加上 1 就是答案（973自己）。而这个数组又是有序的，可以应用二分。每个点的二分为 O(logN)，一共 n 个点，所以总时间复杂度为 O(n × logn)

## 方法二：滑动窗口

定义左右两个指针 i 和 j，初始都指向数组头元素。i 不动，j 向右移动，得到此时 i 元素和 j 元素之间的距离 diff = arr[j] - arr[i]，
如果 diff <= L，j 指针向右移动一格，一旦 diff > L 时，获取两指针之间的差就是长度，更新到结果里，之后 i 指针向右移动一格。
整个过程 i 和 j 永远不回退，时间复杂度为 O(n)

# 代码

```php
$arr = [1,3,4,7,13,16,17]; $L = 4;
(new CordCoverMaxPoint())->main($arr, $L);

class CordCoverMaxPoint {

    public function main($arr, $L) {
        echo $this->maxPoint1($arr, $L) . PHP_EOL;
        echo $this->maxPoint2($arr, $L) . PHP_EOL;
    }

    public function maxPoint1($arr, $L) {
        $res = 1;
        $len = count($arr);
        for ($i = 0; $i < $len; $i++) {
            $nearest = $this->_nearestIndex($arr, $i, $arr[$i] - $L);
            $res = max($res, $i - $nearest + 1);
        }
        return $res;
    }

    protected function _nearestIndex($arr, $R, $value) {
        $L = 0;
        $index = $R;
        while ($L <= $R) {
            $mid = $L + (($R - $L) >> 1);
            if ($arr[$mid] >= $value) {
                $index = $mid;
                $R = $mid - 1;
            } else {
                $L = $mid + 1;
            }
        }
        return $index;
    }

    public function maxPoint2($arr, $L) {
        $left = $right = 0;
        $len = count($arr);
        $max = 0;
        while ($left < $len) {
            while ($right < $len && $arr[$right] - $arr[$left] <= $L) {
                $right++;
            }
            $max = max($max, $right - $left);
            $left++;
        }
        return $max;
    }
}
```