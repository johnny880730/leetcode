# 求累加和小于等于k但离k最近的子数组

# 题目
给定一个数组 arr，再给定一个值 k，返回累加和小于等于 k，但是离 k 最近的子数组累加和

# 解析
子数组以 i 位置结尾的情况下，小于等于 k 且离 k 最近的累加和是多少。

假设 arr[0..i] 的累加和是 100，k = 20，那么实际上也就是看 i 位置之前有没有累加和是大于等于 80 的子数组。如果有这么个位置 j，那么 arr[j + 1..i] 
就是答案。

# 代码

```java
class MaxSubArraySumLessOrEqualK {

    public static int getMaxLessOrEqualK(int[] arr, int k) {
        // 记录i之前的前缀和，按照有序表组织
        TreeSet<Integer> set = new TreeSet<Integer>();
        // 一个数也没有的时候，已经有一个前缀和是0
        set.add(0);
        
        int max = Integer.MIN_VALUE;
        int sum = 0;
        // 每一步的i，都求子数组必须以i结尾的情况下，求子数组的累加和，是<=k的，并且是最大的
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];      // sum ==> arr[0..i]的累加和
            if (set.ceiling(sum - k) != null) {
                max = Math.max(max, sum - set.ceiling(sum - k));
            }
            set.add(sum);       // 当前的前缀和加到set里
        }
        return max;
    }
}
```