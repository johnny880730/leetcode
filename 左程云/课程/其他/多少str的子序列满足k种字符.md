# 题目
来自百度

给定一个字符串 str，和一个正数 k，str 子序列的字符种数必须是 k 种，返回有多少子序列满足这个条件？

已知 str 中都是小写字母

# 解析


&emsp;&emsp;假设有 3 种字符，k=2，那么种类上就是 3 取 2，然后 2 种字符词频，求 2 的 n 次方相乘，最后累加。

&emsp;&emsp;比如 abbccc，词频：a=1, b=2, c=3。
- 选 a,b：1*(2^2-1)=3，
- 选 b,c：(2^2-1)*(2^3-1)=21，
- 选 a,c：1*(2^3-1)=7，  
3+21+7=31。

# 代码

```php

class SequenceKDifferentKinds
{
    function main($str, $k) {
        $arr = str_split($str);
        $counts = array_fill(0, 26, 0);
        foreach ($arr as $v) {
            $counts[ord($v) - 97]++;
        }
        echo $this->ways($counts, 0, $k);
    }

    // "aababddfeeef" a 3个; b 2个; c 0个; d 2个; e 3个; f 2个; g 0个; ... ; z 0个
    // int[] arr = { 3 2 0 2 3... }
    //             k=0 1 2 3 4...
    // 原始的字符串，变成了arr的形式来表达！
    // restKinds : 还剩下几种字符，需要去选！
    // return ways(arr, 3);
    // arr[i....]桶，自由选择，一定要选出restKinds种来
    function ways($arr, $i, $restKinds) {
		if ($i == count($arr)) { // 结束了
			return $restKinds == 0 ? 1 : 0;
		} else {
		    // 没结束，还有字符，可供选择

            // 就是不考虑i位置的字符
            $p1 = $this->ways($arr, $i + 1, $restKinds);
			// 一定要选择，i位置的字符
			$p2 = 0;
			if ($arr[$i] != 0) {
                //  p2 = 选i字符的方法  *  ways(arr, i+1,  restKinds-1);
                //	选i字符的方法 = C n 1 + C n 2 + C n 3 + ..... + C n n = 2的arr[i]次方  - 1
                //  p2 = (2的arr[i]次方  - 1)  *  ways(arr, i+1,  restKinds-1);
                $p2 = ((1 << $arr[$i]) - 1) * $this->ways($arr, $i + 1, $restKinds - 1);
            }
			return $p1 + $p2;
		}
    }
}
```