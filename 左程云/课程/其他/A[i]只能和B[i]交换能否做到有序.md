# 题目
来自华为

给定两个数组 A 和 B，长度都是 N。 A[i] 不可以和 A 中其他数交换，只可以选择和 B[i] 交换 (0 <= i < n)。
能否做到让 A 有序？

## 示例
```
A = [3, 2, 1, 5, 6]; B = [0, 6, 4, 3, 1]
A[0]和B[0]交换，A[2]和B[2]交换，最后 A = [0, 2, 4, 5, 6]，返回 true
```

# 解析

#### 贪心

&emsp;&emsp;

# 代码
```php
$A = [3, 2, 1, 5, 6]; $B = [0, 6, 4, 3, 1];
var_dump(new LetASorted)->canSorted($A, $B);

Class LetASorted {
    function canSorted($A, $B) {
        return $this->process($A, $B, 0, PHP_INT_MIN);
    }   
    
    // pre为前一次是否交换的结果
    function process($A, $B, $index, $pre) {
        if ($index == count($A)) {
            return true;
        }
        // index还没到终止位置
        // A[i]不交换
        $p1 = $pre > $A[$index] ? false : $this->process($A, $B, $index + 1, $A[$index]);
        // A[i]与B[i]交换
        $p2 = $pre > $B[$index] ? false : $this->process($A, $B, $index + 1, $B[$index]);
        
        return $p1 || $p2;
    }
}
```