# 单词表拼接成str的方式

# 题目 
假设所有字符都是小写字母。给定一个长字符串 str。再给一个 arr，是去重的单词表，每个单词都不是空字符串且可以使用任意次数。

使用 arr 中的单词有多少种拼接 str 的方式，返回方法数。

# 示例
```
str = "aaabc"
arr = ["a", "aa", "aaa", "ab", "b", "c"]
组合方式包括如下：
a + a + a + b + c
aa + a + b + c
aaa + b + c
aa + ab + c
a + aa + b + c
a + a + ab + c
```

# 代码
```php
class WordBreak {

    // 递归
    public function ways1($str, $arr) {
        $set = array();
        foreach ($arr as $item) {
            $set[$item] = true;
        }
        return $this->_process($str, 0, $set);
    }

    // 所以字典的词都已经放在了set中
    // str[i...] 能被字典的词分解的话，返回分解的方法数
    protected function _process($str, $i, $set) {
        $len = strlen($str);
        if ($i == $len) {
            // 什么字符都不用 也算1种
            return 1;
        }
        $ways = 0;
        // str[i...end] 是前缀串，枚举每一个前缀串
        for ($end = $i; $end < $len; $end++) {
            $pre = substr($str, $i, $end - $i + 1);
            if (array_key_exists($pre, $set)) {
                $ways += $this->_process($str, $end + 1, $set);
            }
        }
        
        return $ways;
    }
    
    // 将字典的单词生成前缀树来加速
    public function ways2($str, $arr) {
        $root = new NodeOfWordBreak();
        foreach ($arr as $s) {
            $node = $root;
            for ($i = 0, $len = strlen($s); $i < $len; $i++) {
                $index = ord($s[$i]) - ord('a');
                if (!$node->nexts[$index]) {
                    $node->nexts[$index] = new NodeOfWordBreak();
                }
                $node = $node->nexts[$index];
            }
            $node->end = true;
        }
        
        return $this->_g($str, $root, 0);
    }
    
    // str[i...]被分解的方法数
    protected function _g($str, $root, $i){
        $len = strlen($str);
        if ($i == $len) {
            return 1;
        }
        $ways = 0;
        $cur = $root;
        for ($end = $i; $i < $len; $end++) {
            $path = ord($str[$end]) - ord('a');
            if (!$cur->nexts[$path] ) {
                break;
            }
            $cur = $cur->nexts[$path];
            if ($cur->end) {    //str[i..end]是单词表里的一个
                $ways += $this->_g($str, $root, $end + 1);
            }
        }
        return $ways;
    }
}

class NodeOfWordBreak {
    public $end;
    public $nexts;
    
    function __construct() {
        $this->end = false;
        $this->nexts = array_fill(0, 26, 0);
    }
}
```