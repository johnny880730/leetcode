# 题目
来自腾讯

给定一个只由 0 和 1 组成的字符串 S，假设下标从 1 开始，规定i位置的字符价值 V[i] 计算方式如下 :
- i == 1 时，V[i] = 1
- i > 1 时，如果S[i] != S[i-1]，V[i] = 1
- i > 1 时，如果S[i] == S[i-1]，V[i] = V[i-1] + 1

你可以随意删除 S 中的字符，返回整个 S 的最大价值。字符串长度<=5000

# 解析

&emsp;&emsp;递归。从左往右的尝试模型。

&emsp;&emsp;当前 index 位置的字符保留；当前 index 位置的字符不保留。这两种情况取最大值。

# 代码

```php

class AddValue
{
    
    public function max2($string)
    {
        if (!$string) {
            return 0;
        }
        return $this->process($string, 0, 0, 0);
    }
    
    // 递归含义 :
	// 目前在arr[index...]上做选择, str[index...]的左边，最近的数字是lastNum
	// 并且lastNum所带的价值，已经拉高到baseValue
	// 返回在str[index...]上做选择，最终获得的最大价值
	// index -> 0 ~ 4999
	// lastNum -> 0 or 1
	// baseValue -> 1 ~ 5000
    public function process($str, $index, $lastNum, $baseValue){
        if ($index == count($str)) {
            return 0;
        }
        $curValue = $lastNum == $str[$index] ? ($baseValue + 1) : 1;
        // 当前index的位置保留
        $next1 = $this->process($str, $index + 1, $str[$index], $curValue);
        // 当前index的位置不保留
        $next2 = $this->process($str, $index + 1, $lastNum, $baseValue);
        
        return max($curValue + $next1, $next2);
    }
    
    
}
```