# 题目
给定一个正数数组 arr，正数 x，正数 y。

你的目标是让 arr 整体的累加和 <=0，你可以对数组中的数执行以下三种操作中的一种，且每个数最多能执行一次操作:
- 不变；
- 可以选择让 num 变成 0，承担 x 的代价
- 可以选择让 num 变成 -num，承担 y的代价

返回达到目标的最小代价。

来自微软面试。

数据规模: 面试时面试官没有说数据规模。

# 解析

#### 动态规划

&emsp;&emsp;算出当前数组的累加和，每个位置有三种操作，计算这三种操作的代价最小值，继续往下走即可。

#### 贪心

&emsp;&emsp;先将数组从大到小排序

&emsp;&emsp;若 y<=x，没有必要执行任何x操作，从头开始遍历，当累加和小于等于零就停止

&emsp;&emsp;若 y>x，将整个数组看成由三部分组成：Y 操作的部分（yLen)、X 操作的部分(xLen)、不操作的部分。要注意，yLen 和 xLen 是有可能为零的。
两次 for 循环就可以完全包括所有的情况，计算出 Y 操作的断点和 X 操作的断点。

&emsp;&emsp;对两次 for 循环的优化：假设执行 Y 操作的长度为 yLen，arr[0..yLen] 的累加和 sum1，此时从数组尾部往前的累加和 sum2<=sum1 的最左侧
位置，就是不执行任何操作的边界，剩下的部分就是执行 X 操作的部分。可以事先新开数组或者直接在原来的 arr 数组上搞定后缀和数组。如 arr=[9,6,3,2,2,2,1,1,1]，
后缀和数组为 [27,18,12,9,7,5,3,2,1]，如果 yLen=1，也就是对 arr[0] 执行 Y 操作，此时 sum1=arr[0]=9，那么将后缀和数组从后往前看，<=sum1 的最左侧
位置的索引就是 3，代表 arr[3..n-1] 部分都不用做任何操作，arr[0] 为 Y 操作，剩下的 arr[1..2] 就是 X 操作，此时的代价 cost=y+2x。
同样方式计算 yLen=2、3、4…… 时的代价是多少，取最小值即可。

&emsp;&emsp;取后缀和数组的那个最左边界，可以使用二分法

# 代码

```php

class SumNoPositiveMinCost
{
    // 动态规划
    public function min1($arr, $x, $y) {
        $sum = array_sum($arr);
        return $this->process($arr, $x, $y, 0, $sum);
    }
    
    // arr[i...]自由选择，每个位置的数可以执行三种操作中的一种！
	// 执行变0的操作，x操作，代价 -> x
	// 执行变相反数的操作，y操作，代价 -> y
	// 还剩下sum这么多累加和，需要去搞定！
	// 返回搞定了sum，最低代价是多少？
	public function process($arr, $x, $y, $index, $sum) {
	    if ($sum <= 0) {
	        return 0;
	    }
	    // sum > 0 没搞定
	    if ($index == count($arr)) {
	        return PHP_INT_MAX;
	    }
	    // 选择1：什么都不做
	    $p1 = $this->process($arr, $x, $y, $index + 1, $sum);
	    // 选择2：执行x的操作，变成0，代价为 x + 后续
	    $p2 = PHP_INT_MAX;
	    $next2 = $this->process($arr, $x, $y, $index + 1, $sum - $arr[$index]);
	    if ($next2 != PHP_INT_MAX) {
	        $p2 = $x + $next2;
	    }
	    // 选择3：执行y的操作，变相反数，代价 y + 后续
	    $p3 = PHP_INT_MAX;
	    $next3 = $this->process($arr, $x, $y, $index + 1, $sum - ($arr[$index] << 1));
	    if ($next3 != PHP_INT_MAX) {
	        return $y + $next3;
	    }
	    // 三种选择选最小的
	    return min($p1, $p2, $p3);
	}
	
	// 贪心
	public function min2($arr, $x, $y) {
	    $len = count($arr);
	    sort($arr); // arr从小到大
	    $arr = array_reverse($arr); // arr 从大到小
	    if ($y >= $x) {
	        // 没有必要做任何操作
	        $sum = array_sum($arr);
	        $cost = 0;
	        for ($i = 0; $i < $len && $sum > 0; $i++) {
	            $sum -= $arr[$i] << 1;
	            $cost += $y;
	        }
	        return $cost;
	    } else {
	        // 在arr上直接拿到后缀和数组
	        for ($i = $len - 2; $i >= 0; $i++) {
	            $arr[$i] += $arr[$i + 1];
	        }
	        $benefit = 0;
	        // 执行Y操作的数有0个时候，代价就是只有X操作的了
	        $left = $this->mostLeft($arr, 0, $benefit);
	        $cost = $left * $x;
	        for ($i = 0; $i < $len - 1; $i++) {
	            $benefit += $arr[$i] - $arr[$i + 1];    //此时arr是后缀和数组，当前项-后一项=arr原来的数字
	            $left = $this->mostLeft($arr, $i + 1, $benefit);
	            $cost = min($cost, ($i + 1) * $y + ($left - 1 - $i) * $x);
	        }
	        return $cost;
	    }
	}
	
	// arr是后缀和数组， arr[left...]中找到值<=val的最左位置
	protected function mostLeft($arr, $left, $val) {
	    $right = count($arr) - 1;
	    $res = count($arr);
	    while ($left <= $right) {
	        $m = $left + (($right - $left) >> 1);
	        if ($arr[$m] <= $val) {
	            $res = $m;
	            $right = $m - 1;
	        } else {
	            $left = $m + 1;
	        }
	    }
	    return $res;
	}

    
    
}
```