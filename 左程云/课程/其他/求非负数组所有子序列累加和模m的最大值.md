# 题目
给定一个非负数组 arr，和一个正数 m。 返回 arr 的所有子序列中累加和模 m 之后的最大值。

# 解析

&emsp;&emsp;暴力解：每个位置做出选择和不选择的决策，得到每一个 sum 值，去模 m 获得结果，复杂度 O(2^n)

### 动态规划

&emsp;&emsp;获取整个数组的和 sum，定义 n 行、sum+1 列的 dp 数组 dp[i][j]，含义：arr[0..i] 里的数字任意选择能否获得累加和 j 的结果，默认 false。
以 arr=[5, 1, 2] 为例，整个数组累加和为 8，生成的三行九列的 dp[] 数组为

DP    | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8
---   |---|---|---|---|---|---|---|---|---
0(5)  |   |   |   |   |   |   |   |   |
1(1)  |   |   |   |   |   |   |   |   |
2(2)  |   |   |   |   |   |   |   |   |

来看第一列 dp[i][0]，表示 arr[0..i] 里的数字任意取舍是否能获得累加和为 0 的结果。显然都不取就是 0，所以第一列的结果都是 true。

DP    | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8
---   |---|---|---|---|---|---|---|---|---
0(5)  | T |   |   |   |   |   |   |   |
1(1)  | T |   |   |   |   |   |   |   |
2(2)  | T |   |   |   |   |   |   |   |

来看第一行 dp[0][j]，候选数字只有 arr[0]=5，如果不用累加和就是 0，用了就是 5，因此只有 arr[0][0] 和 arr[0][5] = true，
其他情况都是 false

DP    | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8
---   |---|---|---|---|---|---|---|---|---
0(5)  | T | F | F | F | F | T | F | F | F
1(1)  | T |   |   |   |   |   |   |   |
2(2)  | T |   |   |   |   |   |   |   |

剩下的格子，选择从左往右、从上往下填。可见，当填 dp[i][j]的格子时，可以使用它左方、上方、左上的格子。
由于 dp[i][j] 的定义，有两种决策：
1. 不使用 arr[i] 的数字，那么就剩下 arr[0..i-1] 的数字来供决策，也就是 dp[i-1][j]
2. 使用 arr[i] 的数字，相当于用 arr[0..i-1] 的数字来决策 j-arr[i]，也就是 dp[i-1][j-arr[i]]  

这两种决策有一种能得到 j，那么 dp[i][j]=true

整个 dp[i][j] 填充完毕后，来看看最后一行。根据定义，dp[n-1][j] 这一行表示任意选取 arr[0..n-1] 的数字能否获得累加和为 j。
如果为 true，去模 m 获得结果。这一行所有 true 去模 m 的最大值就是答案。

为何是最后一行？因为根据定义就是所有 arr 里的数字去自由选择用还是不用。

复杂度为 O(n × sum)

**但是这个方法适用于 arr 中数不太大的时候**

#### 优化
&emsp;&emsp;如果 arr 里的数字较大，但 m 较小，dp[i][j] 含义变更为：任意使用 arr[0..i] 的数字，**累加和模 m 后能否得到 j**。
假设 arr=[23, 17, 19]，m=5，dp 表如下：

 DP   | 0 | 1 | 2 | 3 | 4 
---   |---|---|---|---|---
0(23) |   |   |   |   |   
1(17) |   |   |   |   |   
2(19) |   |   |   |   |   

先看第一列，dp[i][0] 表示任意选取 arr[0..i] 的数字，累加和模 m 后能否得到 0，同样都不使用的累加和就是 0，模 m 也是 0，所以第一列都为 true。

DP    | 0 | 1 | 2 | 3 | 4
---   |---|---|---|---|---
0(23) | T |   |   |   |
1(17) | T |   |   |   |
2(19) | T |   |   |   |

再看第一行，dp[0][j]，只有第一个数字 arr[0] = 23 可以选择使用或不适用，使用的话累加和就是 23，模 5 得 3，也就是 dp[0][3]=true，其他为 false。

DP    | 0 | 1 | 2 | 3 | 4
---   |---|---|---|---|---
0(23) | T | F | F | T | F
1(17) | T |   |   |   |
2(19) | T |   |   |   |

剩下的格子，依然选择从左往右、从上往下填。由于 dp[i][j] 的定义，有两种决策：
1. 不使用 arr[i]，那么就剩下 arr[0..i-1] 的数字来供决策，也就是 dp[i-1][j]
2. 使用 arr[i]，假设 arr[i] % m = x，那么如果要凑出模之后的余数为 j，要在 0-i-1 范围上所有累加和模 m 能得到 j-x，加上arr[i] 自己模 m 得到 x，
就能拼出 j（如 求 dp[5][7]，arr[5]=12，m=9。由于 arr[5] % 9 = 3，要凑出 j=7 的情况，需要看 0-4 范围上的所有累加和模 9 能否得到 7-3=4，
有的话就是 true）  
   
使用 arr[i] 的话还有一种情况：如果 arr[i] 的值大于 j，假设 arr[i] % m = x，那么如果要凑出模之后的余数为 j，要在 0-i-1 范围上所有累加和模 m 能得到 j+m-arr[i]，
加上arr[i] 自己模 m 得到 x（如 求 dp[5][7]，arr[5]=8，m=9。由于 arr[5] % 9 = 8，要凑出 j=7 的情况，需要看 0~4 范围上的累加和模 9 能否得到 j+m-arr[i]=7+9-8=8，
因为(8+8)%9=7）

整个 dp[i][j] 填充完毕后，来看看最后一行。根据定义，dp[n-1][j] 这一行表示任意选取 arr[0..n-1] 的数字累加模 m 后能否得到 j。
当 j 从小到大遍历，dp[n-1][j] 为 true 的时候最大的 j 就是结果

# 代码

```php
class SubsquenceMaxModM {

    // 暴力解
    function max1 ($arr, $m) {
    
        $set = [];
        $this->process($arr, 0, 0, $set);
        $max = 0;
        foreach ($set as $sum => $val) {
            $max = max($max, $sum % $m);
        }
        return $max;
    }
    
    // arr[index..] 能形成多少个不同的累加和，全部存到set里
    // arr，原数组
    // index，索引，要还是不要
    // sum，arr[0..index-1]所做的决定已经形成的累加和是多少
    protected function process($arr, $index, $sum, &$set){
        if ($index == count($arr)) {
            $set[$sum] = true;
        } else {
            $this->process($arr, $index + 1, $sum, $set);
            $this->process($arr, $index + 1, $sum + $arr[$index], $set);
        }
    }
    
    // 动态规划
    function max2 ($arr, $m) {
        $sum = array_sum($arr);
        $len = count($arr);
        $dp = array_fill(0, $len, array_fill(0, $sum + 1, false));
        // 第一列都是true
        for ($i = 0; $i < $len; $i++) {
            $dp[$i][0] = true;
        }
        // 第一行只有 arr[0] 是true
        $dp[0][$arr[0]] = true;
        // 其他情况
        for ($i = 1; $i < $len; $i++) {
            for ($j = 1; $j <= $sum; $j++) {
                // 不用 arr[i] 的数字
                $dp[$i][$j] = $dp[$i - 1][$j];
                // 使用 arr[i] 的数字，还要考虑越界
                if ($j - $arr[$i] >= 0) {
                    $dp[$i][$j] = $dp[$i][$j] | $dp[$i - 1][$j - $arr[$i]];
                }
            }
        }
        
        $res = 0;
        for ($j = 0; $j <= $sum; $j++) {
            if ($dp[$len - 1][$j]) {
                $res = max($res, $j % $m);
            }
        }
        return $res;
    }
    
    // DP优化
    function max3 ($arr, $m) {
        $len = count($arr);
        $dp = array_fill(0, $len, array_fill(0, $m, false));
        // 第一列为 true
        for ($i = 0; $i < $len; $i++) {
            $dp[$i][0] = true;
        }
        // 第一行
        $dp[0][$arr[0] % $m] = true;
        // 一般情况
        for ($i = 1; $i < $len; $i++) {
            for ($j = 1; $j < $m; $j++) {
                $dp[$i][$j] = $dp[$i - 1][$j];
                $cur = $arr[$i] % $m;
                // 下面两个 if 只会中一个
                if ($j - $cur >= 0) {
                    $dp[$i][$j] = $dp[$i][$j] | $dp[$i - 1][$j - $cur];
                }
                //  8    3     9    0..i-1  m+j-cur
                if ($j + $m - $cur < $m) {
                    $dp[$i][$j] = $dp[$i][$j] | $dp[$i - 1][$j + $m - $cur];
                }
            }
        }
        
        $res = 0;
        for ($j = 0; $j < $m; $j++) {
            if ($dp[$len - 1][$j]) {
                $res = $j;
            }
        }
        return $res;
    }
    
}

```