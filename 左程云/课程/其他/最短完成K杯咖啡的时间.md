# 最短完成 K 杯咖啡的时间

# 题目
有一个数组 nums，nums[i] 表示第 i 台咖啡机完成一杯咖啡需要的时间。

现在同时来了 K 杯咖啡的订单，请问当订单完成的时候，每个咖啡机分别做了多少杯咖啡？

# 解析

## 小根堆
小根堆的元素是节点，节点包含两个信息：开始制作的时间点、做一杯咖啡需要多久。小根堆根据这两个信息的和去排序。

假设 nums = [3, 1, 7], k = 100。

一开始的时间点都是 0，将 [0, 3]、[0, 1]、[0, 7] 都生成节点丢入小根堆中。

弹出第一个节点，此节点的开始时间是 0，完成一杯咖啡需要的时间是 1，那么下一杯的开始时间就是 0 + 1 = 1，生成新的节点 [1, 1] 重新丢入小根堆，
同时记录下是哪个咖啡机制作的这杯咖啡。可发现这时候这个新节点还是在堆顶，继续弹出，根据前面的逻辑去生成新节点重新放入堆中。这样弹出 100 
个节点后就能得到每个咖啡机做了多少个了

# 代码
```php
$nums = [3, 1, 7]; $k = 100;
var_dump((new makeCoffee)->getCups($nums, $k));

class makeCoffee {

    function getCups($nums, $key) {
        $res = array_fill(0, count($nums), 0);
        $heap = new makeCoffeeMinHeap();
        foreach ($nums as $i => $d) {
            $heap->insert(new NodeOfMakeCoffee($i, 0, $d));
        }
        for ($i = 0; $i < $key; $i++) {
            $node = $heap->extract();
            $res[$node->i]++;
            $newNode = new NodeOfMakeCoffee($node->i,$node->t + $node->d, $node->d);
            $heap->insert($newNode);
        }
        return $res;
    }
}

class NodeOfMakeCoffee {
    public $i;      //咖啡机所在的索引
    public $t;      //当前这杯咖啡开始制作的时间
    public $d;      //咖啡机制作一杯咖啡需要的时间


    function __construct($i, $t, $d) {
        $this->i = $i;
        $this->t = $t;
        $this->d = $d;
    }

}

class makeCoffeeMinHeap extends SplMinHeap {

    function compare($o1, $o2) {
        $n = ($o1->t + $o1->d) - ($o2->t + $o2->d);
        return $n == 0 ? 0 : ($n > 0 ? -1 : 1);
    }
}

```

