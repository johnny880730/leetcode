# 题目
只由小写字母（a~z）组成的一批字符串，都放在字符类型的数组 arr 中。

如果其中某两个字符串，所含有的字符种类完全一样，就将两个字符串算作一类 比如：baacba 和 bac 就算作一类。

虽然长度不一样，但是所含字符的种类完全一样（a、b、c） 返回 arr 中有多少类？

# 解析

&emsp;&emsp;由于只有小写字母，按字符顺序统计每个 str 生成摘要，就可以得出多少类。例如 baacba，出现了 a b c ，摘要就是 abc。
将摘要放到一个 hashset 里就能统计多少个了

&emsp;&emsp;优化：由于定死了只有26个字母，利用位运算来进行优化。利用一个 int 型数字，最右第一位表示 a 是否出现，最有第二位表示 b 是否出现……
这样在常数项可以进行优化

# 代码

```php
class HowManyTypes {

    // 用字符串生成摘要
    function types1 ($arr) {
        $hash = [];
        foreach ($arr as $str) {
            $map = array_fill(0, 26, false);
            for($len = strlen($str), $i = 0; $i < $len; $i++) {
                $k = ord($str[$i]) - ord('a');
                $map[$k] = true;
            }
            $hashKey = '';
            for ($i = 0; $i < 26; $i++) {
                if ($map[$i]) {
                    $hashKey .= chr(ord('a') + $i);
                }
            }
            !array_key_exists($hashKey, $hash) && $hash[$hashKey] = true;
        }
        return count($hash);
    }
    
    // 利用位运算生成摘要
    function types2 ($arr) {
        $hash = array();
        foreach ($arr as $str) {
            $hashKey = 0;
            for ($len = strlen($str), $i = 0; $i < $len; $i++) {
                // 1左移多少位，然后“或”到整体的状态里去，相当于给这个字符对应的位置标记1
                $hashKey |= (1 << (ord($str[$i]) - ord('a')));
            }
            !array_key_exists($hashKey, $hash) && $hash[$hashKey] = true;
        }
        return count($hash);
    } 
    
}

```