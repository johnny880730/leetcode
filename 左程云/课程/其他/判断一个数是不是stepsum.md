# 题目
定义何为step sum？比如 680，680+68+6=754，680 的 step sum 叫754。

给定一个整数 num，判断它是不是某个数的 step sum？

# 解析

#### 二分法

&emsp;&emsp;在 0 到 num 之间找中点，然后求中点的 step sum。如果 step sum 太大，取左边；如果 step sum 太小，取右边。
时间复杂度是(log2N)*(log10N)

# 代码

```php

class IsStepSum
{
    
    public function checkIsStepSum($num)
    {
        $l = 0;
        $r = $num;
        while ($l <= $r) {
            $mid = $l + (($r - $l) >> 1);
            $cur = $this->getStepSum($mid);
            if ($cur == $num) {
                return true;
            } else if ($cur < $num) {
                $l = $mid + 1;
            } else {
                $r = $mid - 1;
            }
        }
        return false;
    }
    
    protected function getStepSum($n)
    {
        $sum = 0;
        while ($n != 0) {
            $sum += $n;
            $n = intval($n / 10);
        }
        return $sum;
    }
    
}
```