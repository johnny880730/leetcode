# 题目
给定一个只由小写字母组成的字符串 str，返回其中最长无重复字符的字串长度

# 解析

#### 动态规划

&emsp;&emsp;思路：求出以位置 i 结尾时，最长无重复字符的长度。其中最大的长度就是题目的答案。

&emsp;&emsp;假设 arr[i]=m，推算以当前字符 arr[i] 结尾的最长无重复字符字串的长度时有两个瓶颈：
1. m 字符上次出现的位置：因为该位置和 i 位置的字符重复了，所以往前推肯定到这里就停了
2. 前一个位置 i-1 往左推的长度：在推算当前位置 i 时，必定已经推算过了前一个位置 i-1 了。如果推算前一个位置的瓶颈点为 x，
   那么推算当前位置时最多到 x 点就停了（因为再往前就有重复字符了）。

&emsp;&emsp;以上两个瓶颈点，哪个位置离当前位置 i 更近就决定了当前 i 能往前推多远，也就是当前位置 i 的瓶颈。

# 代码

```php
class LongestNoRepeatSubstring {
    
    // 双指针 O(n²)，双指针遍历 + hash或者set来确定是否重复
    function lnrs1($str) {
        if (!$str) {
            return 0;
        }
        $len = strlen($str);
        $max = 0;
        for ($i = 0; $i < $len; $i++) {
            $map = array_fill(0, 26, false);
            for ($j = $i; $j < $len; $j++) {
                $k = ord($str[$j]) - ord('a');
                if (array_key_exists($k, $map) && $map[$k] == true) {
                    break;
                }
                $map[$k] = true;
                $max = max($max, $j - $i + 1);
            }
        }
        return $max;
    }
    
    // 动态规划
    function lnrs2($str) {
        if (!$str) {
            return 0;
        }
        $len = strlen($str);
        //last[0]表示a上次出现的位置，last[1]表示b上次出现的位置…… 默认值-1表示未出现
        $last = array_fill(0 ,26, -1);
        // str第一个字符 出现在 0 位置
        $last[ord($str[0]) - ord('a')] = 0;
        $max = 1;
        $preMaxLen = 1;
        for ($i = 1; $i < $len; $i++) {
            // 瓶颈1：$last[ord($str[$i]) - ord('a')] => i位置的字符上次出现的位置，当前位置减去上次出现的位置获得长度
            // 瓶颈2：$preMaxLen + 1 => 上个位置的字符+自己的长度
            // 2个瓶颈以小的为准（离当前i更近）
            $preMaxLen = min($i - $last[ord($str[$i]) - ord('a')], $preMaxLen + 1);
            $max = max($max, $preMaxLen);
            $last[ord($str[$i]) - ord('a')] = $i;
        }
        return $max;
    }
}

```