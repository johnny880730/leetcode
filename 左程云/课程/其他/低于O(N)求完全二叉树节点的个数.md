# 低于 O(N) 求完全二叉树节点的个数

# 题目
如题

# 解析
第一步：统计完全二叉树的总深度（一直向左向左捅到最深）。假设深度为 4（根节点的深度为 1）

第二步：从头结点出发，找他右树上的最左节点，看看这个节点是否到了总深度。

如果达到了深度 4，说明根节点的左子树是满的，而且高度为 3。可以直接用公式 2 ^ 3 - 1 来获得左树的节点数量，
加上头结点自己就是 2 ^ 3 - 1 + 1 = 2 ^ 3，这个数字就是左树 + 根节点的数量。现在不知道的就是右树节点的数量。
将右树的节点作为新的根节点去递归计算右树有多少 m，这个 m 加上前面的 2 ^ 3 就是整棵树的节点数量。参见下图：

![](./images/img01.png)

如果没有达到深度 4，说明根节点的右子树是满的，只不过高度是 2（深度为 3）。可以直接用公式 2 ^ 2 - 1 来获得右树的节点数量，
加上头结点自己就是 2 ^ 2 - 1 + 1 = 2 ^ 2，这个数字就是右树 + 根节点的数量。同样去递归左树获得数量 m，
这个 m 加上前面的 2 ^ 2 就是整棵树的节点数量。参见下图：

![](./images/img02.png)

分析复杂度：  
树的总深度为 h，要么从根节点的左子树去递归，要么从右子树去递归，每次下探都会少一层深度，也就是个等差数列，所以总的下探层数就是深度 h 的平方级别，
也就是 O(h²)。而完全二叉树的高度 h 和节点数 N 的关系是 h = log(N)，所以最终复杂度为 O( (logN)² )
  
# 代码

### php
```php
class CompleteTreeNodeNumber {
    
    // 保证 head 为头的树 是完全二叉树
    public function nodeNum($head){
        if ($head == null) {
            return 0;
        }
        return $this->_bs($head, 1, $this->_mostLeftLevel($head, 1));
    }
    
    // 如果 node 在第 level 层，求以 node 为头的子树最大深度是多少
    // 以 node 为头的子树，一定是完全二叉树
    protected function _mostLeftLevel($node, $level){
        while ($node != null) {
            $level++;
            $node = $node->left;
        }
        return $level - 1;
    }
    
    // node 在第 level 层， h 是总深度（永远不变）
    // 以 node 为头的完全二叉树的节点个数是多少
    protected function _bs($node, $level, $h){
        if ($level == $h) {
            // node在最深层，就只有它自己一个节点
            return 1;
        }
        // 右树在 level+1 层，一直往左捅，看看最深度是否达到总深度 h
        if ($this->_mostLeftLevel($node->right, $level + 1) == $h) {
            // 右树能到最深层 => 左树是满的，递归右树
            return (1 << ($h - $level)) + $this->_bs($node->right, $level + 1, $h);
        } else {
            // 右树补能到最深层 => 右树是满的，递归左树
            return (1 << ($h - $level - 1)) + $this->_bs($node->left, $level + 1, $h);
        }
    }
    
        
}
```  
