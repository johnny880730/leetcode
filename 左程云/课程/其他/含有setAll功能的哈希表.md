# 题目
哈希表常见的三个操作是 put、get、containsKey，而且这三个操作的时间复杂度为 O(1)。

现在想加一个 setAll 功能，就是把所有记录 value 都设成统一的值。请设计并实现这种有setAll功能的哈希表，
并且 put、get、containsKey、setAll 四个操作的时间复杂度都为O(1)。

# 解析

&emsp;&emsp;设置一个 setAllValue 和 时间戳 setAllTime，如果查出来的 value 的时间戳比 setAllTime 的时间戳早，则以 setAllValue 为准，否则以 value 为准。

&emsp;&emsp;因为时间复杂度是 O(1)，所以肯定不能每次都去更新，那就利用时间戳，如果某条记录的时间早于 setAllTime 记录的时间，
说明 setAllValue 是最新数据，返回 setAllValue 。如果某条记录的时间晚于 setAllTime 记录的时间，说明记录的值是最新数据，返回该条记录的值

# 代码

```php

class MySetAllHashMap
{
    protected $hashMap = [];
    protected $setAllValue = PHP_INT_MAX;
    protected $setAllTime = 0;
    protected $time = 0;
    
    public function put($key, $val)
    {
        $this->hashMap[$key] = ['val' => $val, 'time' => $this->time++];
    }
    
    public function containsKey($key)
    {
        return array_key_exists($key, $this->hashMap);    
    }
    
    public function get($key)
    {
        if (!$this->containsKey($key)) {
            return -1;
        }
        $data = $this->hashMap[$key];
        return $data['time'] < $this->setAllTime ? $this->setAllValue : $data['val'];
    }
    
    public function setAll($val)
    {
        $this->setAllValue = $val;
        $this->setAllTime = $this->time++;    
    }
}
```