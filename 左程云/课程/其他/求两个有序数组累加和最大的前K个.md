# 求两个有序数组累加和最大的前K个

# 题目
给定两个有序数组 arr1 和 arr2，再给定一个正数 K，求两个数累加和最大的前 K 个，两个数必须分别来自 arr1 和 arr2。

# 解析
假设 arr1 = [1, 5, 9, 12]，arr2 = [3, 6, 7, 14]，K = 3。
top1 就是 12 + 14；top2 就是 14 + 9。数字可以重复用，但组合不能重复。

以一个数组作为行、另一个数组作为列生成一张二维表的话表示元素之和的话，最大的肯定是二维表最右下角。
准备一个大根堆，大根堆的每个节点含有的信息是：来自 arr1 的数字的索引 i、来自 arr2 的数字的索引 j、两个数字的和。
大根堆根据数字的和来组织。

然后每次从大根堆里弹出一个节点，这个节点就是 top1，它收集到了来自两个数组的索引 i 和 j。将 i - 1 和 j - 1 之后分别得到两个组合 
[arr[i - 1], arr[j]] 以及 [arr[i], arr[j - 1]]，也就是二维表右下角格子的左方和上方，分别将这两个组合生成新节点放入大根堆去组织。
之后就是大根堆再弹出一个节点，这个节点就是 top2，继续将其包含的 i 和 j 减一获得新的组合生成节点放入大根堆。
注意不要重复放入同一个组合的节点进大根堆即可。这样直到拿出 K 个结束。



# 代码
```php
$arr1 = [1, 5, 9, 12]; $arr2 = [3, 6, 7, 14]; $k = 3;
var_dump((new TopKSumCrossTwoArrays)->topKSum($arr1, $arr2, $k));

class TopKSumCrossTwoArrays {

    public function topKSum($arr1, $arr2, $k){
        if (!$arr1 || !$arr2 || $k < 1) {
            return null;
        }
        $len1 = count($arr1);
        $len2 = count($arr2);
        $k = min($k, $len1 * $len2);    // 防止给的 k 太过大

        $res = array_fill(0, $k, 0);
        $resIndex = 0;
        
        $myHeap = new MyMaxHeap();
        // 表示 arr1[i], arr2[j] 有没有进过大根堆
        $setMap = array_fill(0, $len1, array_fill(0, $len2, false));
        $i1 = $len1 - 1;
        $i2 = $len2 - 1;
        $myHeap->insert(new NodeOfTopKSumCrossTwoArrays($i1, $i2, $arr1[$i1] + $arr2[$i2]));
        $setMap[$i1][$i2] = true;
        while ($resIndex != $k) {
            $curNode = $myHeap->extract();
            $res[$resIndex++] = $curNode->sum;
            $i1 = $curNode->index1;
            $i2 = $curNode->index2;
            echo "i1 = $i1, i2 = $i2, sum = $curNode->sum" . PHP_EOL;
            if ($i1 - 1 >= 0 && !$setMap[$i1 - 1][$i2]) {
                $setMap[$i1 - 1][$i2] = true;
                $myHeap->insert(new NodeOfTopKSumCrossTwoArrays($i1 - 1, $i2, $arr1[$i1 - 1] + $arr2[$i2]));
            }
            if ($i2 - 1 >= 0 && !$setMap[$i1][$i2 - 1]) {
                $setMap[$i1][$i2 - 1] = true;
                $myHeap->insert(new NodeOfTopKSumCrossTwoArrays($i1, $i2 - 1, $arr1[$i1] + $arr2[$i2 - 1]));
            }
        }
        return $res;
    }

}

// 节点类型
class NodeOfTopKSumCrossTwoArrays {
    public $index1;     //arr1 的位置
    public $index2;     //arr2 的位置
    public $sum;        //arr1[index1] + arr2[index2]

    function __construct($i1, $i2, $s) {
        $this->index1 = $i1;
        $this->index2 = $i2;
        $this->sum = $s;
    }
}

// 大根堆类型
class MyMaxHeap extends SplMaxHeap {

    function compare ($o1, $o2) {
        return $o1->sum - $o2->sum;
    }
}
```