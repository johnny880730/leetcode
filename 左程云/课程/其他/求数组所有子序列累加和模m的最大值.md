# 求数组所有子序列累加和模m的最大值

# 题目
给定一个非负数组 nums，和一个正数 m。返回 nums 所有子序列中累加和模 m 之后的最大值。

# 示例
```
nums = [3, 2, 1, 7] m = 5
子序列 [2, 7] 模 5 得到 4，已经是最大值了
```

# 解析

方法一、类似背包问题  

构建一张表，行是 i ，列 sum。dp[i][sum] 代表 nums[0 ~ i] 自由选择能否得到 sum 这个累加和，值为布尔类型。
能搞定的累加和去模 m ，最后一定能得到一个答案。

复杂度 O(N × SUM)

---

方法二

构建一张表，行是 i，列是 j，范围是 0 ~ m - 1。dp[i][j] 表示 nums[0 ~ i] 的数字自由组合，得到每一个累加和模 m 之后看能否得到 j，
只要有一个能得到就是 true。

比如 nums = [2, 4, 3]，m = 7。

第一列，也就是 dp[i][0]，表示随便用 nums[0 ~ i] 得到累加和模 m 后能否得到 0。显然一个数都不取就可以，所以 dp[i][0] 都是 true。

第一行，也就是 dp[0][j]，表示只有第一个数 2 作为备选的情况下模 m 后能否得到 j，所以只有 j == 2 的格子是 true（dp[0][0] 已经是 true）了 ，
其他都是 false。

普遍位置 dp[i][j] 怎么填？
- 如果 dp[i - 1][j] == true 的话，表示 nums[0 ~ i - 1] 任意选择就可以得到模 m 得 j，那么这里不选择 nums[i] 这个数字也肯定可以得到 j。
  因此得到这种情况下 dp[i][j] = true。 
- 如果一定要使用 nums[i] 这个数字，假设 nums[i] % m = a，又有 dp[i][j] 含义是模 m 之后要得到 j，所以就看 dp[i - 1][j - a] 
  是否为 true，如果是则 dp[i][j] 为 true
- 还有种情况，nums[i] % m = a，可以加上之前的商大于 m 的情况下得到 j，相当于绕了一圈回来。比如 nums[5] = 3，m = 10，要求 dp[5][2]。
  现在 nums[5] 已经是 3，比 j = 2 要大，所以之前的数字要凑出 9，(9 + 3) % 10 == 2，因此如果 dp[5 - 1][2 + 10 - 3] 是 true 的话，
  dp[i][j] 也为 true

复杂度 O(N × M)

# 代码

```php
class SubsequenceMaxModM {

    // 背包问题
    public function max1($nums, $m){
        $sum = array_sum($nums);
        $len = count($nums);
        $dp = array_fill(0, $len, array_fill(0, $sum + 1, false));
        for ($i = 0; $i < $len; $i++) {
            $dp[$i][0] = true;
        }
        $dp[0][$nums[0]] = true;
        
        for ($i = 1; $i < $len; $i++) {
            for ($j = 1; $j <= $sum; $j++) {
                $dp[$i][$j] = $dp[$i - 1][$j];
                if ($j - $nums[$j] >= 0) {
                    $dp[$i][$j] = $dp[$i][$j] | $dp[$i - 1][$j - $nums[$j]];
                }
            }
        }
        $res = 0;
        for ($j = 0; $j <= $sum; $j++) {
            if ($dp[$len - 1][$j]) {
                $res = max($res, $j % $m);
            }
        }
        return $res;
    }
    
    // 方法二
    public function max2($nums, $m){
        $len = count($nums);
        $dp = array_fill(0, $len, array_fill(0, $m, false));
        for ($i = 0; $i < $len; $i++) {
            $dp[$i][0] = true;
        }
        $dp[0][$nums[0] % $m] = true;
        
        for ($i = 1; $i < $len; $i++) {
            for ($j = 1; $j < $m; $j++) {
                $dp[$i][$j] = $dp[$i - 1][$j];
                $cur = $nums[$i] % $m;
                if ($j - $cur >= 0) {
                    $dp[$i][$j] = $dp[$i][$j] | $dp[$i - 1][$j - $cur];
                }
                if ($m + $j - $cur < $m) {
                    $dp[$i][$j] = $dp[$i][$j] | $dp[$i - 1][$m + $j - $cur];
                }
            }
        }
        $res = 0;
        for ($j = $m - 1; $j >= 0; $j--) {
            if ($dp[$len - 1][$j]) {
                return $j;
            }
        }
        return $res;
    }
}

```