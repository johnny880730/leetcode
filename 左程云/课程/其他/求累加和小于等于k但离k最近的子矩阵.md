# 求累加和小于等于k但离k最近的子矩阵

# 题目
给定一个矩阵 matrix，再给定一个值 k，返回累加和小于等于 k，但是离 k 最近的子矩阵累加和

# 解析
看此题之前先看《求累加和小于等于k但离k最近的子数组》。将解决子数组的问题的函数称作 f() 函数。

先求包含第 0 行且只包含第 0 行数据的情况下，答案是多少。那么将第 0 行数据作为数组传给 f() 函数即可。

再求包含第 0、1 行且只包含第 0、1 行数据的情况下，答案是多少。将第 0 行和第 1 行的数字，相同索引上的数字相加，得到一个新的一维数组，
将这个新数组作为参数传入 f() 函数。

然后再求只包含0、1、2 行数据的答案、只包含0、1、2、……、N 行数据的答案。

以 0 开头的求完之后，开始求以 1 开头的，也就是先求第 1 行的答案、然后是第 1、2 行的答案、第1、2、3 行的答案……
相当于在行数上玩了 N² 的枚举。如果列数为 M ，f() 函数的复杂度就是 O(M * logM)，所以总体复杂度是 O(N² * M * logM) 

# 代码

```java
class MaxSubMatrixSumLessOrEqualK {

    public static int getMaxLessOrEqualK(int[][] matrix, int k) {
        
    }
}
```