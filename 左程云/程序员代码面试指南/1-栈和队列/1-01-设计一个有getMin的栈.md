## 设计一个有 getMin 功能的栈

# 题目
实现一个特殊的栈，在实现栈的基本功能的基础上，再实现返回栈中最小元素的操作

【要求】
1. pop、push、getMin 操作的时间复杂度都为O(1)
2. 设计的栈类型可以使用现成的栈结构

# 解析
&emsp;&emsp;设计时，我们使用两个栈，一个用来保存当前栈中的元素，其功能和一个正常的栈没区别，这个栈记为 stackData。
另一个栈用于保存每一步的最小值，这个栈记为 stackMin。具体的实现方式如下：

1. 压入数据规则：  
   假设当前数据为 newNum，先将其压入 stackData。然后判断 stackMin 是否为空。
      1. 如果为空，则 newNum 也压入 stackMin；
      2. 如果不为空，则比较 newNum 和 stackMin 的栈顶元素哪一个更小。
         - 如果 newNum 更小或者相等，则 newNum 也压入 stackMin；
         - 如果 stackMin 的栈顶元素更小，则把这个元素再压一份到 stackMin，也就是在栈顶元素上再压一个栈顶元素
2. 弹出数据规则：  
   在 stackData 中弹出数据记为 value，弹出 stackMin 的栈顶，返回 value。很明显可以看出，压入与弹出规则时对应的。
3. 查询当前栈中的最小值操作：  
   由压入数据和弹出数据的规则可知，stackMin 的栈顶元素始终是当前 stackData 中的最小值

&emsp;&emsp;时间复杂度 O(1)，空间复杂度为 O(n)，代码参考 GetMinStack 类   
    
# 代码
```php
$arr = [3,4,5,1,2,1];
$o = new Zuochengyun1_01();
$o->main($arr);

class Zuochengyun1_01
{
    public function main($arr)
    {
        $stack = new GetMinStack();
        foreach ($arr as $item) {
            $stack->push($item);
        }
        echo $stack->getMin() . PHP_EOL;    //输出：1
        echo $stack->pop() . PHP_EOL;       //输出：1
        echo $stack->getMin() . PHP_EOL;    //输出：1
        echo $stack->pop() . PHP_EOL;       //输出：2
        echo $stack->getMin() . PHP_EOL;    //输出：1
        echo $stack->pop() . PHP_EOL;       //输出：1
        echo $stack->getMin() . PHP_EOL;    //输出：3
        echo $stack->pop() . PHP_EOL;       //输出：5
        echo $stack->getMin() . PHP_EOL;    //输出：3
        echo $stack->pop() . PHP_EOL;       //输出：4
    }
}


class GetMinStack
{
    private $stackData;
    private $stackMin;

    public function __construct()
    {
        $this->stackData = new SplStack();
        $this->stackMin  = new SplStack();
    }

    public function push($newNum)
    {
        if ($this->stackMin->isEmpty()) {
            $this->stackMin->push($newNum);
        } else if ($newNum < $this->getMin()) {
            $this->stackMin->push($newNum);
        } else {
            $newMin = $this->stackMin->top();
            $this->stackMin->push($newMin);
        }
        $this->stackData->push($newNum);
    }

    public function pop()
    {
        if ($this->stackData->isEmpty()) {
            throw new Exception('stackData is Empty');
        }
        $this->stackMin->pop();
        return $this->stackData->pop();
    }

    public function getMin()
    {
        if ($this->stackMin->isEmpty()) {
            throw new Exception('stackMin is Empty');
        }
        return $this->stackMin->top();
    }
}
```