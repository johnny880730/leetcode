## 单调栈结构

# 题目
给定一个没有重复值的数组 arr，找到每一个 i 位置左边和右边离 i 最近且值比 arr[i] 更小的位置。返回所有位置相应的信息（-1表示不存在）  
例如：arr = [3,4,1,5,6,2,7]，返回  
[  
&emsp;[-1, 2],  
&emsp;[0, 2],  
&emsp;[-1, -1],  
&emsp;[2, 5],  
&emsp;[3, 5],  
&emsp;[2, -1],  
&emsp;[5, -1],  
]  

要求：时间复杂度为 O(n)


# 解析
&emsp;&emsp;准备一个栈 stack，栈中放的元素是数组的位置，开始时候 stack 为空。

&emsp;&emsp;如果找到每一个 i 位置左边和右边离 i 位置最近且值比 arr[i] 小的位置，那么需要让栈顶到栈底的位置所代表的值是严格递减的。

&emsp;&emsp;举例说明：
- 初始时，arr=[3,4,1,5,6,2,7], stack = {}
- 遍历到 arr[0]==3，发现 stack 为空，直接放入 0 位置。stack 从顶到底为：{0位置(3)}
- 遍历到 arr[1]==4，发现直接放入 1 位置，不会破坏 stack 从顶到底所代表的值是严格递减的，那么直接放入。  
   此时栈为：{1位置(4)、0位置(3)}
- 遍历到 arr[2]==1，如果放入 2 位置(值是1)会破坏从顶到底的严格递减的要求，所以开始从 stack 弹出位置。  
   **如果 X 位置被弹出，在栈中位于 X 位置下面的位置，就是 X 位置左边离 X 位置最近且比 arr[X] 小的位置；当前遍历到的位置就是 X 位置右边离 X 位置最近且比 arr[X] 小的位置。**    
   从 stack 弹出位置 1，在栈中位于1位置下面的是位置0，而当前遍历到的位置是2，所以 res[1]={0,2}。  
   弹出 1 位置之后，发现放入 2 位置（值是1）还是会破坏从顶到底的严格递减的要求，所以继续弹出。  
   从 stack 弹出位置 0，栈中位置 0 下面已经没有东西了，说明在位置 0 左边没有比 arr[0] 更小的值，当前遍历到的是位置 2，所以 res[0]={-1,2}。  
   stack 已经为空，所以放入 2 位置，此时栈为：{2位置(1)}
- 遍历到 arr[3]==5，发现放入 3 位置不会破坏要求，直接放入栈，此时栈为：{3位置(5)、2位置(1)}
- 遍历到 arr[4]==6，发现放入 4 位置不会破坏要求，直接放入栈，此时栈为：{4位置(6)、3位置(5)、2位置(1)}
- 遍历到 arr[5]==2，发现放入 5 位置会破坏要求，开始弹出。  
   弹出位置 4，位置 4 下面的是位置 3，当前遍历到的是位置 5，所以 res[4] = {3,5}。  
   弹出位置 3，位置 3 下面的是位置 2，当前遍历到的是位置 5，所以 res[3] = {2,5}。  
   这时放入位置 5 不会破坏单调递减的要求了。此时栈为：{5位置(2)、2位置(1)}
- 遍历到 arr[6]==7，发现放入 6 位置不会破坏要求，直接放入栈，此时栈为：{6位置(7)、5位置(2)、2位置(1)}
- 遍历结束，清算栈中剩下的内容
- 弹出 6 位置，它下面的是位置 5，由于是清算阶段弹出的，所以 res[6]={5,-1}
- 弹出 5 位置，它下面的是位置 2，由于是清算阶段弹出的，所以 res[5]={2,-1}
- 弹出 2 位置，它下面没有东西了，由于是清算阶段弹出的，所以 res[2]={-1,-1}

&emsp;&emsp;整个流程中，每个位置进栈一次，出栈一次，所以整个流程的复杂度为 O(n)

# 代码
```php
$arr = [3, 4, 1, 5, 6, 2, 7];
$o   = new Zuochengyun1_6();
$o->main($arr);

class Zuochengyun1_6
{
    public function main($arr)
    {
        print_r($this->getNearLessNoRepeat($arr));
    }

    public function getNearLessNoRepeat($arr)
    {
        $res   = array_fill(0, count($arr), array_fill(0, 2, null));
        $stack = new SplStack();
        for ($i = 0; $i < count($arr); $i++) {
            while (!$stack->isEmpty() && $arr[$stack->top()] > $arr[$i]) {
                $popIndex          = $stack->pop();
                $leftLessIndex     = $stack->isEmpty() ? -1 : $stack->top();
                $res[$popIndex][0] = $leftLessIndex;
                $res[$popIndex][1] = $i;
            }
            $stack->push($i);
        }
        while (!$stack->isEmpty()) {
            $popIndex          = $stack->pop();
            $leftLessIndex     = $stack->isEmpty() ? -1 : $stack->top();
            $res[$popIndex][0] = $leftLessIndex;
            $res[$popIndex][1] = -1;
        }
        return $res;
    }
}
```
