## 单调栈结构【进阶版】

# 题目
给定一个可能有重复值的数组 arr，找到每一个 i 位置左边和右边离 i 最近且值比 arr[i]更小的位置。返回所有位置相应的信息

要求：时间复杂度为 O(n)

# 解析
&emsp;&emsp;准备一个栈 stack，栈中放的元素是数组的位置，开始时候 stack 为空。

&emsp;&emsp;如果找到每一个 i 位置左边和右边离 i 位置最近且值比 arr[i] 小的位置，那么需要让栈顶到栈底的位置所代表的值是严格递减的。

&emsp;&emsp;举例说明：
- 初始时，arr=[3,1,3,4,3,5,3,2,2], stack = {}
- 遍历到 arr[0]==3，发现 stack 为空，直接放入 0 位置。stack 从顶到底为：{0位置(3)}
- 遍历到 arr[1]==1，从栈中弹出位置 0，得到 res[0]={-1,1}。位置 1 进栈，此时 stack 为 {1位置(1)}
- 遍历到 arr[2]==3，可以直接放入，stack 为 {2位置(3)、1位置(1)}
- 遍历到 arr[3]==4，可以直接放入，stack 为 {3位置(4)、2位置(3)、1位置(1)}
- 遍历到 arr[4]==3，从栈中弹出位置 3，可得到 res[3]={2,4}。此时栈顶为 2，值为 3，当前的位置是 4，值也为 3，所以两个位置压在一起。  
   此时 stack 为 {2位置4位置(3)、1位置(1)}
- 遍历到 arr[5]==5，可以直接放入，此时 stack 为 {5位置(5)、2位置4位置(3)、1位置(1)}
- 遍历到 arr[6]==3，从栈中弹出位置 5，这个位置 5 下面的是 2位置4位置，选最晚加入的 4 位置，当前遍历到位置 6，所以得到 res[5]={4,6}。  
   位置 6 进栈，发现又是和栈顶位置代表的值相等的情况，所以继续压在一起。  
   此时 stack 为 {2位置4位置6位置(3)、1位置(1)}
- 遍历到 arr[7]==2，从栈中弹出 2位置4位置6位置，栈中这些位置下面的是 1 位置，当前是 7 位置，所以得到  
   res[2]={1,7], res[4]={1,7}, res[6]={1,7}。  
   位置 7 进栈，此时 stack 为 {7位置(2)、1位置(1)}
- 遍历到 arr[8]==2，可以进栈，且又是和栈顶对应元素相等的情况，此时 stack 为 {7位置8位置(2)、1位置(1)}
- 遍历结束，开始清算
- 弹出 7位置8位置，下面的是位置 1，得到 res[7]={1, -1}, res[8]={1,-1}
- 弹出 1 位置，得到 res[1]={-1,-1}

# 代码
```php
$arr = [3,1,3,4,3,5,3,2,2];
$o   = new Zuochengyun1_6_2();
$o->main($arr);

class Zuochengyun1_6_2
{
    public function main($arr)
    {
        print_r($this->getNearLess($arr));
    }

    public function getNearLess($arr)
    {
        $res   = array_fill(0, count($arr), array_fill(0, 2, null));
        $stack = new SplStack();
        for ($i = 0; $i < count($arr); $i++) {
            while (!$stack->isEmpty() && $arr[$stack->top()[0]] > $arr[$i]) {
                $popIs = $stack->pop();
                // 取位于下面位置的列表中，最晚加入的那个
                $leftLessIndex = $stack->isEmpty() ? -1 : $stack->top()[count($stack->top()) - 1];
                foreach ($popIs as $popi) {
                    $res[$popi][0] = $leftLessIndex;
                    $res[$popi][1] = $i;
                }
            }
            if (!$stack->isEmpty() && $arr[$stack->top()[0]] == $arr[$i]) {
                $temp = $stack->pop();
                $temp[] = $i;
                $stack->push($temp);
            } else {
                $list = array($i);
                $stack->push($list);
            }
        }
        while (!$stack->isEmpty()) {
            $popIs = $stack->pop();
            // 取位于下面位置的列表中，最晚加入的那个
            $leftLessIndex = $stack->isEmpty() ? -1 : $stack->top()[count($stack->top()) - 1];
            foreach ($popIs as $popi) {
                $res[$popi][0] = $leftLessIndex;
                $res[$popi][1] = -1;
            }
        }
        return $res;
    }
}
```