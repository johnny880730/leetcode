## 合并两个有序的单链表

# 题目
给定两个有序单链表的头节点 head1 和 head2，请合并两个单链表，合并后的链表依然有序，返回合并后的链表头节点。

例如：  
0-2-3-7-null  
1-3-5-7-9-null  
结果为：0-1-2-3-3-5-7-7-9-null

# 解析
&emsp;&emsp;两个链表长度为 M 和 N，时间复杂度O(M+N)，空间复杂度O(1)
- 如果两个链表有一个为空，无须合并，返回另一个链表的头节点即可
- 比较 head1 和 head2 的值，小的节点也是合并后的头节点，记为 head；
- 假设 head 所在的链表为链表1，另一个为链表2，两个链表都从头部开始一起遍历，比较两个节点的值，记为 cur1 和 cur2。  
  根据大小关系做出不同的调整，同时用一个变量 pre 表示上次比较时较小的节点。
- 如果链表1先走完，此时 cur1=null，pre 为链表1的最后一个节点，那么就把 pre 的 next 指向链表2的当前节点 cur2，  
  表示把链表2没遍历到的部分直接拼接在最后，结束。如果链表2先走完，说明链表2的所有节点已经插入到链表1中，结束。
- 代码参考下面的 merge 方法

# 代码
```php
require_once './class/ListNode.class.php';
$arr1 = [0,2,3,7]; $arr2= [1,3,5,7,9];
$head1 = array2LinkList($arr1);
$head2 = array2LinkList($arr2);
$o = new Zuochengyun2_14();
$res = $o->main($head1, $head2);
fetchNode($res);

class Zuochengyun2_14
{
    public function main($head1, $head2)
    {
        return $this->merge($head1, $head2);
    }

    public function merge($head1, $head2)
    {
        if ($head1 == null || $head2 == null) {
            return $head1 == null ? $head2 : $head1;
        }
        $head = $head1->val < $head2->val ? $head1 : $head2;
        $cur1 = $head == $head1 ? $head1 : $head2;
        $cur2 = $head == $head1 ? $head2 : $head1;
        $pre = null;
        while ($cur1 != null && $cur2 != null) {
            if ($cur1->val <= $cur2->val) {
                $pre = $cur1;
                $cur1 = $cur1->next;
            } else {
                $next = $cur2->next;
                $pre->next = $cur2;
                $cur2->next = $cur1;
                $pre = $cur2;
                $cur2 = $next;
            }
        }
        $pre->next = $cur1 == null ? $cur2 : $cur1;

        return $head;
    }
}
```