## 二叉树的按层打印和ZigZag打印

# 题目
给定一个二叉树的头节点 root，分别实现按层打印和 ZigZag 打印

例如，二叉树如下：
```
                1
            /       \
           2         3
         /          /  \
        4          5    6
                  / \
                 7   8
```

按层打印时，输出格式必须如下：  
Level 1: 1  
Level 2: 2 3  
Level 3: 4 5 6  
Level 4: 7 8

ZigZag 打印时，输出格式必须如下：  
Level 1 from left to right: 1  
Level 2 from right to left: 3 2  
Level 3 from left to right: 4 5 6  
Level 4 from right to left: 8 7

# 解析
&emsp;&emsp;按层打印的实现：
- 除了对二叉树做简单的宽度优先遍历外，本体还有额外的要求，就是同一层的节点必须打印在一行上，并且要求输出行号。
- 只需要用两个 node 类型的变量 last 和 nLast 就可以解决这个问题，  
  last 表示正在打印当前行的最右节点，nLast 表示下一行的最右节点。
- 假设我们每一层都做从左到右的遍历，如果发现遍历到的节点等于 last，则说明该换行。  
  换行之后只要令 last=nLast，就可以下一行的打印过程，直到结束
- 如何更新 nLast? 只需要让 nLast 一直跟踪记录宽度优先队列中的最新加入的节点即可。  
  因为最新加入队列的节点一定是目前已经发现的下一行的最右节点，  
  所以当前行打印完时，nLast 一定是下一行所有节点中的最右节点。
- 参考下面的 printByLevel 方法

&emsp;&emsp;结合题目的例子来说明：
- 开始时，last=节点1，nLast=null，把节点1放入队列 queue，遍历开始，queue={1}
- 从 queue 中弹出节点1并打印，然后把节点1的孩子节点依次放入 queue，放入节点2时，nLast=节点2；  
  放入节点3时，nLast=节点3，此时发现弹出的节点 1==last，所以换行，并令 last=nLast=节点3，queue={2, 3}
- 从 queue 中弹出节点2并打印，然后把节点2的孩子节点放入 queue，放入节点4时，nLast=节点4，queue={3, 4}
- 从 queue 中弹出节点3并打印，然后把节点3的孩子节点依次放入 queue，放入节点5时，nLast=节点5；  
  放入节点6时，nLast=节点6。此时发现弹出的节点 3==last，所以换行，并令 last=nLast=节点6，queue={4, 5, 6}
- 从 queue 中弹出节点4并打印，节点4没有子节点，所以 nLast 也不更新。queue={5, 6}
- 从 queue 中弹出节点5并打印，然后把节点5的子节点依次放入 queue 中，放入节点7时，nLast=节点7；
  放入节点8时，nLast=节点8。此时 queue={6, 7, 8}
- 从 queue 中弹出节点6并打印，节点6没有子节点，nLast不更新。此时发现节点 6==last，所以换行，并令 last=nLast=节点8，queue={7, 8}
- 用同样的判断过程打印节点7和节点8，整个过程结束。

---

&emsp;&emsp;ZigZag打印的实现：
- 使用一个双端队列 dq，将节点1从 dq 的头部放入dq。然后按照以下两条原则：
- 原则1：如果是从左到右的过程，那么一律从 dq 的头部弹出节点。如果弹出的节点没有孩子就忽略，有孩子的话，先让左子节点从尾部进入 dq，再让右子节点从尾部进入 dq;
- 原则2：如果是从右到左的过程，那么一律从 dq 的尾部弹出节点。如果弹出的节点没有孩子就忽略，有孩子的话，先让右子节点从头部进入 dq，再让左子节点从头部进入 dq;
- 用原则1和原则2的过程切换可以完成这个打印过程，现在问题就剩下，如何确定切换原则1和原则2的时机？
- 在打印的过程中，下一层最后打印的节点是当前层有孩子节点的节点中最先进入 dq 的节点。

&emsp;&emsp;结合题目的例子来说明：
- 将节点1从 dq 的头部放入dq，从头到尾的 dq={1}
- 根据原则1，从 dq 头部弹出节点1并打印，然后先让节点2从 dq 尾部进入，再让节点3从 dq 尾部进入。从头到尾的 dq={2, 3}
- 根据原则2，从 dq 尾部弹出节点3并打印，然后先让节点6从 dq 头部进入，再让节点5从 dq 头部进入。从头到尾的 dq={5, 6, 2}
- 根据原则2，从 dq 尾部弹出节点2并打印，然后先让节点4从 dq 头部进入。从头到尾的 dq={4, 5, 6}
- 根据原则1，依次从 dq 头部弹出节点4、5、6并打印，这期间让节点7从 dq 尾部进入，再让节点8从 dq 尾部进入。从头到尾的 dq={7, 8}
- 根据原则2，依次从 dq 尾部弹出节点8和7并打印即可。

# 代码
```php
require_once '../../class/TreeNode.class.php';
$arr = [1,2,3,4,null,5,6,null,null,7,8];
$root = generateTreeByArray($arr);
$o = new Zuochengyun3_6();
$o->main($root);

class Zuochengyun3_6
{
    public function main($root)
    {
        echo 'print by level:'.PHP_EOL;
        $this->printByLevel($root);
        echo PHP_EOL;
        echo PHP_EOL;
        echo 'print by zigzag:' . PHP_EOL;
        $this->printByZigZag($root);

    }

    public function printByLevel($root)
    {
        $queue = new SplQueue();
        $level = 1;
        $last = $root;
        $nLast = null;
        $queue->enqueue($root);
        echo "Level " . $level++ . " : ";
        while ($queue->isEmpty() == false) {
            $cur = $queue->dequeue();
            echo $cur->val . ' ';
            if ($cur->left) {
                $queue->enqueue($cur->left);
                $nLast = $cur->left;
            }
            if ($cur->right) {
                $queue->enqueue($cur->right);
                $nLast = $cur->right;
            }
            if ($cur == $last && $queue->isEmpty() == false) {
                echo "\nLevel " . $level++ . " : ";
                $last = $nLast;
            }
        }

    }

    public function printByZigZag($root)
    {
        $dq = new SplDoublyLinkedList();
        $level = 1;
        $lr = true;
        $last = $root;
        $nLast = null;
        $dq->unshift($root);
        $this->printLevelAndOrientation($level++, $lr);
        while ($dq->isEmpty() == false) {
            if ($lr) {
                $cur = $dq->shift();
                if ($cur->left) {
                    $nLast = $nLast == null ? $cur->left : $nLast;
                    $dq->push($cur->left);
                }
                if ($cur->right) {
                    $nLast = $nLast == null ? $cur->right : $nLast;
                    $dq->push($cur->right);
                }
            } else {
                $cur = $dq->pop();
                if ($cur->right) {
                    $nLast = $nLast == null ? $cur->right : $nLast;
                    $dq->unshift($cur->right);
                }
                if ($cur->left) {
                    $nLast = $nLast == null ? $cur->left : $nLast;
                    $dq->unshift($cur->left);
                }
            }
            echo $cur->val . ' ';
            if ($cur == $last && $dq->isEmpty() == false) {
                $lr = !$lr;
                $last = $nLast;
                $nLast = null;
                echo PHP_EOL;
                $this->printLevelAndOrientation($level++, $lr);
            }
        }
        echo PHP_EOL;
    }

    protected function printLevelAndOrientation($level, $lr)
    {
        echo "Level " . $level . ' from ';
        echo $lr ? 'left to right: ' : 'right to left: ';
    }
}
```