## 遍历二叉树的神级方法：Morris遍历

# 题目
给定一个二叉树的头节点 root，完成前序、中序、后序遍历。要是时间复杂度 O(N)，空间复杂度O(1)

# 解析
&emsp;&emsp;之前的所有方法都无法做到空间复杂度为 O(1)，因为递归方法用了函数栈，非递归方法申请了栈。两者的额外空间都与树的高度相关 O(h)。
利用二叉树节点中大量指向 null 的指针，实现空间复杂度 O(1)的遍历，就是 Morris 遍历。

&emsp;&emsp;Morris 遍历的实质就是避免用栈结构，而是让下层到上层有指针，具体是通过让底层节点指向 null 的空闲指针指回上层的某个节点，从而完成下层到上层的移动。

&emsp;&emsp;先不管前序、中序、后序的概念，看看 Morris 遍历的过程。

&emsp;&emsp;假设当前节点为 cur，初始 cur 就是根节点，根据以下标准让 cur 移动：
1. 如果 cur 为 null，则过程停止，否则继续下面的过程
2. 如果 cur 没有左子树，则让 cur 向右移动，即 cur=cur.right
3. 如果 cur 有左子树，则找到 cur 左子树上最右的节点，记为 mostRight
4. 如果 mostRight 的右指针指向空，令 mostRight.right=cur，也就是让 mostRight 的右指针指向当前节点，
   然后让 cur 向左移动，即 cur=cur.left
5. 如果 mostRight 的右指针指向 cur，令 mostRight.right=null，也就是让 mostRight 的右指针指向空，
   然后让 cur 向右移动，即 cur=cur.right

```
                  4
              /      \
             2        6
           /   \     /  \
          1     3   5    7
 ```
这么一棵树的遍历结果为：4 2 1 2 3 4 6 5 6 7，这个序列叫做 Morris 序

&emsp;&emsp;可以看出，在一个二叉树中，有左子树的节点都可以到达两次（如4、2、6)，没有左子树的节点都只到达一次。
对于任何一个只到达一次的节点 X，接下来 cur 要么跑到 X 的右子树上，要么就返回上级。
对于任何一个能到达两次的节点 Y，在第一次到达 Y 后 cur 都会去Y的左子树转一圈，然后第二次来到 Y。
接下来 cur 要么去 Y 的右子树上，要么返回上级。  

&emsp;&emsp;同时，对于任何一个能够到达两次的节点 Y，如何知道 cur 是第一次还是第二次来呢？
如果 Y 的左子树的最右节点的右指针 (mostRight.right) 是指向 null 的，说明 cur 是第一次来到 Y；
如果 mostRight.right 是指向 Y 的，就是第二次到达 Y。

&emsp;&emsp;以上就是 Morris 遍历。代码参考下面的 morris 方法。

--- 

&emsp;&emsp;根据 Morris 遍历，加工出前序遍历，参考下面的 morrisPre 方法，是对 morris 方法的简单改写：
1. 对于 cur 只能到达一次的节点（无左子树的节点），cur 到达时直接打印
2. 对于 cur 可以到达两次的节点（有左子树的节点），cur 第一次到达时【打印】，第二次到达时【不打印】

&emsp;&emsp;根据 Morris遍历，加工出中序遍历，参考下面的 morrisIn 方法，是对 morris 方法的简单改写：
1. 对于 cur 只能到达一次的节点（无左子树的节点），cur 到达时直接打印
2. 对于 cur 可以到达两次的节点（有左子树的节点），cur 第一次到达时【不打印】，第二次到达时【打印】

&emsp;&emsp;Morris 的后序遍历，也是 morris 方法的改写，但稍微复杂点：
1. 对于 cur 只能到达一次的节点（无左子树的节点），直接跳过，不打印
2. 对于 cur 可以到达两次的节点（有左子树的节点）X，cur 第一次到达 X 时【不打印】；第二次到达 X 时，打印 X 左子树的右边界
3. cur 遍历完成后，逆序打印整棵树的右边界

# 代码
```php
require_once './class/TreeNode.class.php';
$arr = [4,2,6,1,3,5,7];
$root = generateTreeByArray($arr);
$o = new Zuochengyun3_4();
$o->main($root);

class Zuochengyun3_4
{
    public function main($root)
    {
        // morris 遍历
        $this->morris($root);
        // morris 前序遍历
        $this->morrisPre($root);
        // morris 中序遍历
        $this->morrisIn($root);
        // morris 后序遍历
        $this->morrisPost($root);


    }

    public function morris($root)
    {
        $cur = $root;
        while ($cur != null) {
            $mostRight = $cur->left;
            // 如果当前cur有左子树
            if ($mostRight != null) {
                // 找到cur的左子树最右的节点
                while ($mostRight->right != null && $mostRight->right != $cur) {
                    $mostRight = $mostRight->right;
                }
                // 从上面的while出来后，mostRight就是cur左子树最右的节点了
                if ($mostRight->right == null) {
                    // 如果mostRight->right指向空的情况
                    $mostRight->right = $cur;   // 让其指向cur
                    $cur = $cur->left;          // cur向左移动
                    continue;                   // 回到最外层的while，继续判断cur的情况
                } else {
                    // mostRight->right指向cur的情况
                    $mostRight->right = null;       // 让其指向空
                }
            }
            // cur没有左子树，向右移动
            // 或者cur左子树上最右节点的右指针是指向cur的，cur向右移动
            $cur = $cur->right;
        }
    }

    public function morrisPre($root)
    {
        $cur = $root;
        while ($cur != null) {
            $mostRight = $cur->left;
            if ($mostRight != null) {
                while ($mostRight->right != null && $mostRight->right != $cur) {
                    $mostRight = $mostRight->right;
                }
                if ($mostRight->right == null) {
                    $mostRight->right = $cur;
                    echo $cur->val . ' ';       //打印行为
                    $cur = $cur->left;
                    continue;
                } else {
                    $mostRight->right = null;
                }
            } else {
                echo $cur->val . ' ';       //打印行为
            }
            $cur = $cur->right;
        }
        echo PHP_EOL;
    }

    public function morrisIn($root)
    {
        $cur = $root;
        while ($cur != null) {
            $mostRight = $cur->left;
            if ($mostRight != null) {
                while ($mostRight->right != null && $mostRight->right != $cur) {
                    $mostRight = $mostRight->right;
                }
                if ($mostRight->right == null) {
                    $mostRight->right = $cur;
                    $cur = $cur->left;
                    continue;
                } else {
                    $mostRight->right = null;
                }
            }
            echo $cur->val . ' ';       //打印行为
            $cur = $cur->right;
        }
        echo PHP_EOL;
    }

    public function morrisPost($root)
    {
        $cur = $root;
        while ($cur != null) {
            $mostRight = $cur->left;
            if ($mostRight != null) {
                while ($mostRight->right != null && $mostRight->right != $cur) {
                    $mostRight = $mostRight->right;
                }
                if ($mostRight->right == null) {
                    $mostRight->right = $cur;
                    $cur = $cur->left;
                    continue;
                } else {
                    $mostRight->right = null;
                    $this->printEdge($cur->left);
                }
            }
            $cur = $cur->right;
        }
        $this->printEdge($root);
        echo PHP_EOL;
    }

    protected function printEdge($node)
    {
        $tail = $this->reverseEdge($node);
        $cur = $tail;
        while ($cur != null) {
            echo $cur->val . ' ';
            $cur = $cur->right;
        }
        $this->reverseEdge($tail);
    }

    protected function reverseEdge($from)
    {
        $pre = null;
        while ($from != null) {
            $next = $from->right;
            $from->right = $pre;
            $pre = $from;
            $from = $next;
        }
        return $pre;
    }
}
```