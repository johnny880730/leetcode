## 在有序但含有空的数组中查找字符串

# 题目
给定一个字符串数组 strs[]，其中有些位置为 null，但在不为 null 的位置上，其字符串是按字典顺序由小到大出现的。
再给定一个字符串 str，请返回 str 在 strs 中出现的最左的位置。

【举例】  
strs=[null, 'a', null, 'a', null, 'b', null, 'c'], str='a'，返回1  
strs=[null, 'a', null, 'a', null, 'b', null, 'c'], str=null，只要 str 为 null，就返回-1  
strs=[null, 'a', null, 'a', null, 'b', null, 'c'], str='d'，返回-1

# 解析
&emsp;&emsp;尽可能多的使用二分查找，过程如下：
1. 假设在 strs[left..right] 上进行查找的过程，全部变量 res 表示字符串 str 在 strs 中最左的位置。  
   初始时，left=0，right=strs.length-1, res=-1。
2. 令 mid=(left+right)/2，则 strs[mid] 表示 strs[left..right] 中间的字符串。
3. 如果 strs[mid] 与 str 一样，说明找到了 str，令 res=mid。但要找的是最左的位置，还要在左半区查找，所以令 right=mid-1，重复步骤。
4. 如果 strs[mid] 与 str 不同，并且 strs[mid]!=null，此时可以比较 strs[mid] 和 str，  
   如果 strs[mid] 比 str 小，说明整个左半区不会出现 str，需要在右半区找，令 left=mid+1，重复步骤2。
5. 如果 strs[mid] 与 str 不同，且 strs[mid]==null，此时从 mid开始，从右到左的遍历左半区 (即strs[left..mid]）。  
   如果整个左半区都为 null，那么继续用二分的方式在右半区查找 (即left=mid+1)，重复步骤2；  
   如果整个左半区不都为 null，假设发现第一个不为 null 的位置为 i，将 str 和 strs[i] 进行比较，
     - 如果 strs[i] 小于 str，说明整个左半区没有 str，令 left=mid+1，重复步骤2；
     - 如果 strs[i] 大于 str，说明 strs[i..right] 没有 str，需要在 strs[left..i-1] 查找，令 right=i-1，重复步骤2；
     - 如果 strs[i] 等于 str，说明找到 str，令 res=mid，继续往左找（strs[left..i-1]）看有没有更左的，令 right=i-1，重复步骤2。
6. 代码参考 getIndex 方法

# 代码
```php
$strs = [null, 'a', null, 'a', null, 'b', null, 'c'];$str = 'a';
$o   = new Zuochengyun5_6();
$o->main($strs, $str);

class Zuochengyun5_6
{
    public function main($strs, $str)
    {
        echo $this->getIndex($strs, $str);
    }

    public function getIndex($strs, $str)
    {
        if (!$strs || !$str) {
            return -1;
        }
        $res = -1;
        $left = 0;
        $right = count($strs) - 1;
        while ($left <= $right) {
            $mid = ($left + $right) >> 1;
            if ($strs[$mid] != null && $strs[$mid] == $str) {
                $res = $mid;
                $right = $mid - 1;
            } else if ($strs[$mid] != null) {
                if (ord($strs[$mid]) < ord($str)) {
                    $left = $mid + 1;
                } else {
                    $right = $mid - 1;
                }
            } else {
                $i = $mid;
                while ($strs[$i] == null && --$i >= $left) {
                    ;
                }
                if ($i < $left || ord($strs[$mid]) < ord($str)) {
                    $left = $mid + 1;
                } else {
                    $res = $strs[$i] == $str ? $i : $res;
                    $right = $mid - 1;
                }
            }
        }
        return $res;
    }

}
```