## 回文最少分割数

# 题目
给定一个字符串 str，返回把 str 全部切成回文子串的最小分割数。

【举例】  
str="ABA"，str本身就是回文串，返回0；  
str="ACDCDCDAD"，最少需要切2次分成3个回文子串："A"、"CDCDC"和"DAD"，返回 2。

#### 解答
&emsp;&emsp;用动态规划来解。

&emsp;&emsp;定义动态规划数组 dp，dp[i] 含义是子串 str[i..len-1] 至少需要切割几次，才能把 str[i..len-1] 全部切成回文子串。那么 dp[0] 就是最后的结果。

&emsp;&emsp;从右往左依次计算 dp[i] 的值，i 初始为 len-1，具体计算过程如下：
1. 假设 j 位置处在 i 与 len-1 之间 (i≤j<len)，如果 str[i..j] 是回文串，那么 dp[i] 的值可能是dp[j+1]+1，  
   其含义是在 str[i..len-1] 上，既然 str[i..j] 是一个回文串，那么它可以自己作为一个分割的部分，  
   剩下的部分（str[j+1..len-1]）继续做最经济的切割，而 dp[j+1] 值的含义正好是 str[j+1..len-1] 的最少回文分割数。
2. 让 j 在 i 到 len-1 位置上枚举，那么所有可能情况中的最小值就是 dp[i] 的值，即 dp[i]=min(dp[j+1]+1 （i≤j<len，且 str[i..j] 必须是回文串） )
3. 如何快速方便的判断 str[i..j] 是否回文串呢？具体过程如下
   - 定一个二维数组 boolean[][]p，如果 p[i][j] 的值为true，说明 str[i..j] 是回文串，否则不是
   - 如果 p[i][j] 为true，那么一定是以下三种情况
      1) str[i..j] 由1个字符组成
      2) str[i..j] 由2个字符组成且2个字符相等
      3) str[i+1..j-1] 是回文串，即 str[i+1][j-1] 为true，且 str[i]==str[j]，即 str[i..j] 首尾字符相等
   - 计算 dp 数组的过程中，位置 i 是从右向左依次计算的。对每一个 i 来说，又依次从 i 位置向右枚举所有的位置 j（i≤j<len），以此来决策出 dp[i] 的值。  
     所以对 p[i][j] 来说，p[i+1][j-1] 的值一定已经计算过，这就是判断一个子串是否为回文串变得方便。
4. 最终返回dp[0]的值，代码参考 minCut 方法


# 代码
```php
$str = "ACDCDCDAD";
$o   = new Zuochengyun5_14();
$o->main($str);

class Zuochengyun5_14
{
    public function main($str)
    {
        echo $this->minCut($str);
    }

    public function minCut($str)
    {
        $len = strlen($str);
        $dp = array_fill(0, $len+1, 0);
        $dp[$len] = -1;
        $p = array_fill(0, $len, array_fill(0, $len, false));
        for ($i = $len - 1; $i >= 0; $i--) {
            $dp[$i] = PHP_INT_MAX;
            for ($j = $i ; $j < $len; $j++) {
                if ($str[$i] == $str[$j] && ($j - $i < 2 || $p[$i + 1][$j - 1])) {
                    $p[$i][$j] = true;
                    $dp[$i] = min($dp[$i], $dp[$j + 1] + 1);
                }
            }
        }
        return $dp[0];
    }

}
```