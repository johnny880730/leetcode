## 判断两个字符串是否互为变形词

# 题目
给定两个字符串 s1 和 s2，如果 s1 和 s2 中出现的字符种类一样且每种字符出现的次数也一样，那么它们互为变形词。
请实现函数判断两个字符串是否互为变形词。

【举例】  
s1="123", s2="231", 返回true  
s1="123", s2="2331", 返回false

# 解析
&emsp;&emsp;如果 s1 和 s2 长度不同，直接返回 false。如果长度相同，假设字符的编码值在 0~255 之间，那么申请一个长度为 256 的数组 map，
map[a]=b 代表字符编码为 a 的字符出现了 b 次，初始时候令 map[0..255]=0。

&emsp;&emsp;遍历字符串 s1，统计每种字符出现的字数。这样 map 就成了 s1 每个字符的词频统计表。
然后遍历 s2，每遍历一个字符，都在 map 中把词频减下来。如果减少之后的值小于0，直接返回 false。

&emsp;&emsp;遍历完 s2，map 中的值也没有出现负值，返回true。

&emsp;&emsp;参考下面的 isDeformation 方法

# 代码
```php
$s1 = "123"; $s2 = "241";   //false
$s1 = "123"; $s2 = "231";   //true
$o = new Zuochengyun5_1();
$o->main($s1, $s2);

class Zuochengyun5_1
{
    public function main($s1, $s2)
    {
        var_dump($this->isDeformation($s1, $s2));
    }

    public function isDeformation($s1, $s2)
    {
        if ($s1 == null || $s2 == null || strlen($s1) != strlen($s2)) {
            return false;
        }
        $map = array_fill(0, 256, 0);
        for ($i = 0; $i < strlen($s1); $i++) {
            $map[ord($s1[$i])]++;
        }
        for ($i = 0; $i < strlen($s2); $i++) {
            if (--$map[ord($s2[$i])] < 0) {
                return false;
            }
        }
        return true;
    }
}
```