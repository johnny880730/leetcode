## 字典树（前缀树）的实现

# 题目
字典树又称为前缀树或者 Trie 树，是处理字符串常见的数据结构。假设组成所有单词的字母仅是 a 到 z，请实现字典树结构，并且包含以下四个主要功能：
- void insert(String word): 添加 word，可重复添加
- void delete(String word): 删除 word，如果 word 添加过多次，仅删除一次
- boolean search(String word): 查询 word 是否在字典树中
- int prefixNumber(String pre): 返回以字符串 pre为前缀的单词数量

# 解析
&emsp;&emsp;字典树是一种树形结构，优点是利用字符串的公共前缀来节约存储空间。字典树的基本性质如下：
- 根节点没有字符路径。除了根节点外，每一个节点都被一个字符路径找到
- 从根节点出发到任何一个节点，如果将沿途经过的字符连接起来，一定为某个加入过的字符串的前缀
- 每个节点向下所有的字符路径上的字符都不同

&emsp;&emsp;在字典树上搜索添加过的单词的步骤如下：
1. 从根节点开始搜索
2. 取得要查找单词的第一个字母，并根据该字母选择对应的字符路径向下继续搜索
3. 字符路径指向的第二层节点上，根据第二个字母选择对应的字符路径向下继续搜索
4. 一直向下搜索，如果单词搜索完后，找到的最后一个节点是一个终止节点，说明字典树中含有这个单词，如果不是，说明不是添加过的单词。
   如果单词没搜索完，但已经没有后续的节点，也说明单词不是字典树中添加过的单词。

&emsp;&emsp;有关字典树节点的类型，参考下面的 TrieNode 类。
其中，path 表示有多少个单词共用这个节点，end 表示有多少个单词以这个节点结尾，
map 是一个哈希表结构，key 代表该节点的一条字符路径，value 为字符路径指向的节点。
根据题目的说明 map 是一个长度为 26 的数组，字符种类较多的情况下可以选择真实的哈希表结构实现 map。

---

&emsp;&emsp;下面介绍 Trie 树类如何实现。

void insert(String word)：  
&emsp;&emsp;假设单词 word 的长度为 N，从左到右遍历每个字符，并依次从头节点开始根据每一个 word[i]，找到下一个节点。
如果找的过程中节点不存在，就建立新节点，记为 a，并令 a.path=1。如果节点存在，记为 b，令 b.path++。
通过最后一个字符找到最后一个节点，记为 e，令 e.path++、e.end++。

boolean search(String word)：  
&emsp;&emsp;从左到右遍历每个字符，并依次从头节点开始根据每一个 word[i] 找到下一个节点。如果找的过程中节点不存在，直接返回false。
如果能通过 word[N-1] 找到最后一个节点，记为 e，若有e.end!=0，说明有单词通过 word[N-1] 的字符路径，并以 e 节点结尾，返回true。
如果 e.end==0，返回 false。

void delete(String word)：   
&emsp;&emsp;先调用 search(word)，看 word 是否在 Trie 树中，若在则继续执行下面的过程，若不在则直接返回。
从左到右遍历每个字符，并依次从头节点开始根据 word[i] 找到下一个节点。
找的过程中把每个节点的 path 值减1。如果发现下一个节点的 path 减完后已经为0，直接从当前节点的 map 中删除后续的所有路径，并返回解。
如果遍历到最后一个节点 e，令 e.path--、e.end--。

int prefixNumber(String pre)：  
&emsp;&emsp;和查找操作同理，根据 pre 不断找到节点，假设最后的节点为 e，返回 e.path 即可。

&emsp;&emsp;代码参考下面的 Trie 类。


# 代码
```php
class TrieNode
{
    public $path;
    public $end;
    public $map;

    public function __construct()
    {
        $this->path = 0;
        $this->end = 0;
        $this->map = array_fill(0, 26, null);
    }
}

class Trie
{
    private $root;

    public function __construct()
    {
        $this->root = new TrieNode();
    }

    public function insert($word)
    {
        if (!$word) {
            return;
        }
        $node = $this->root;
        $node->path++;
        for ($i = 0; $i < strlen($word); $i++) {
            $index = ord($word[$i]) - ord('a');
            if ($node->map[$index] == null) {
                $node->map[$index] = new TrieNode();
            }
            $node = $node->map[$index];
            $node->path++;
        }
        $node->end++;
    }

    public function delete($word)
    {
        if ($this->search($word)) {
            $node = $this->root;
            $node->path++;
            for ($i = 0; $i < strlen($word); $i++) {
                $index = ord($word[$i]) - ord('a');
                if ($node->map[$index]->path-- == 1) {
                    $node->map[$index] = null;
                    return;
                }
                $node = $node->map[$index];
            }
            $node->end--;
        }
    }

    public function search($word)
    {
        if (!$word) {
            return false;
        }
        $node = $this->root;
        for ($i = 0; $i < strlen($word); $i++) {
            $index = ord($word[$i]) - ord('a');
            if ($node->map[$index] == null) {
                return false;
            }
            $node = $node->map[$index];
        }
        return $node->end != 0;
    }

    public function prefixNumber($pre)
    {
        if (!$pre) {
            return 0;
        }
        $node = $this->root;
        for ($i = 0; $i < strlen($pre); $i++) {
            $index = ord($pre[$i]) - ord('a');
            if ($node->map[$index] == null) {
                return 0;
            }
            $node = $node->map[$index];
        }
        return $node->path;
    }
}

$obj = new Trie();
$obj->insert('abc');
$obj->insert('abd');
$obj->insert('abe');
var_dump($obj->search('abc'));      //true
var_dump($obj->search('aba'));      //false
var_dump($obj->prefixNumber('ab'));  //3
$obj->delete('abaaaaa');
var_dump($obj->prefixNumber('ab'));  //3
$obj->delete('abc');
var_dump($obj->prefixNumber('ab'));  //2
```