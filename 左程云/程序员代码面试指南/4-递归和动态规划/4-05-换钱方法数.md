## 换钱方法数

# 题目
给定数组 arr，所有值为正数且不重复。每个值代表一种货币，每种货币可以使用任意张。再给一个整数 aim，代表要找的钱数，求换钱有多少种方法。

【举例】  
arr=[5,10,25,1]，  
aim=0时，所有的货币都不用，也算一种，返回1.  
aim=15时，有6种。分别为3×5，1×10+1×5，1×10+5×1，10×1+1×5，2×5+5×1，15×1，返回6.

无法组成就返回0。

#### 解答
&emsp;&emsp;这道题的经典之处在于可以体现暴力递归、记忆搜索和动态规划之间的关系，并可以在动规基础上再次优化。

&emsp;&emsp;首先是暴力递归的方法。

&emsp;&emsp;如果 arr=[5,10,25,1], aim=1000，分析过程如下：  
1、用0张5元的，让 [10,25,1] 组成剩下的1000，最终方法数记为 res1  
2、用1张5元的，让 [10,25,1] 组成剩下的995，最终方法数记为 res2  
3、用2张5元的，让 [10,25,1] 组成剩下的990，最终方法数记为 res3  
……  
201、用200张5元的，让[10,25,1]组成剩下的0，最终方法数记为 res201

&emsp;&emsp;那么 res1+res2+...+res201 就是总的方法数。定义递归函数 process1(arr, index, aim)，
定义是如果用 arr[index...N-1] 这些面值的钱组成aim，返回总的方法数。

&emsp;&emsp;参考下面的 coins1 方法。最差情况下为O(aim^N)。

--- 
&emsp;&emsp;基于暴力递归初步优化的方法，也就是记忆搜索。

&emsp;&emsp;process1(arr, index, aim) 中 arr 是始终不变的，变化的只有 index 和 aim，所以可以用 p(index, aim) 表示一个递归过程。
暴力递归会存在大量的重复计算，用一个 map 记录每一个递归过程的结果。下次进行同样的递归过程之前，先去 map 查询。
因为本题有两个变量，所以 map 是一个二维表。map(i,j)表示 p(i,j) 的返回值。
还有些特殊值：map[i][j]==0 表示还没计算过，为 -1 表示计算过，但返回值是0.

&emsp;&emsp;参考下面的 coin2 方法。

---
&emsp;&emsp;动态规划方法：

&emsp;&emsp;生成行数为 N，列数为 aim+1 的矩阵 dp，dp[i][j] 的含义是在使用 arr[0..i] 货币的情况下，组成钱数 j 有多少方法。dp[i][j] 的值求法如下：
1. 对于第一列 dp[..][0]，表示组成钱数为0的方法数，很明显是1种，所以 dp 第一列都是1
2. 对于第一行 dp[0][..]，表示只使用 arr[0] 货币的情况下，组成钱的方法数。比如 arr[0]=5 的话，能组成的就是0,5,10...。  
   所以令 dp[0][k×arr[0]]=1，其中 0≤k×arr[0]≤aim，k 为非负整数。
3. 其他位置，记为位置 (i,j)，dp[i][j] 的值为以下几个值的累加。
   - 完全不用 arr[i] 货币，只使用 arr[0..i-1] 货币，方法数为 arr[i-1][j]
   - 用 1 张 arr[i] 货币，剩下的钱用 arr[0..i-1] 组成，方法数为 arr[i-1][j-1*arr[i]]
   - 用 2 张 arr[i] 货币，剩下的钱用 arr[0..i-1] 组成，方法数为 arr[i-1][j-2*arr[i]]
   - 用 k 张 arr[i] 货币，剩下的钱用 arr[0..i-1] 组成，方法数为 arr[i-1][j-k*arr[i]]
4. 最终 arr[N-1][aim] 的值就是最终结果
5. 代码参考 coins3 方法

# 代码
```php
$arr = [5, 10, 25, 1];
$aim = 1000;
$o   = new Zuochengyun4_5();
$res = $o->main($arr, $aim);
echo $res;

class Zuochengyun4_5
{
    public function main($arr, $aim)
    {
        $t1 = microtime(true);
        echo $this->coins1($arr, $aim) . PHP_EOL;
        $t2 = microtime(true);
        echo "coins1 = " . ($t2-$t1) . PHP_EOL;


        echo $this->coins2($arr, $aim) . PHP_EOL;
        $t3 = microtime(true);
        echo "coins2 = " . ($t3-$t2) . PHP_EOL;

        echo $this->coins3($arr, $aim) . PHP_EOL;
        $t4 = microtime(true);
        echo "coins3 = " . ($t4-$t3) . PHP_EOL;
    }

    public function coins1($arr, $aim)
    {
        return $this->process1($arr, 0, $aim);
    }

    protected function process1($arr, $index, $aim)
    {
        $res = 0;
        if ($index == count($arr)) {
            $res = $aim == 0 ? 1 : 0;
        } else {
            for ($i = 0; $arr[$index] * $i <= $aim; $i++) {
                $res += $this->process1($arr, $index + 1, $aim - $arr[$index] * $i);
            }
        }
        return $res;
    }

    public function coins2($arr, $aim)
    {
        $map = array_fill(0, count($arr) + 1, array_fill(0, $aim + 1, 0));
        return $this->process2($arr, 0, $aim, $map);
    }

    protected function process2($arr, $index, $aim, &$map)
    {
        $res = 0;
        if ($index == count($arr)) {
            $res = $aim == 0 ? 1 : 0;
        } else {
            for ($i = 0; $arr[$index] * $i <= $aim; $i++) {
                $mapValue = $map[$index + 1][$aim - $arr[$index] * $i];
                if ($mapValue != 0) {
                    $res += $mapValue == -1 ? 0 : $mapValue;
                } else {
                    $res += $this->process2($arr, $index + 1, $aim - $arr[$index] * $i, $map);
                }
            }
        }
        $map[$index][$aim] = $res == 0 ? -1 : $res;
        return $res;
    }

    public function coins3($arr, $aim)
    {
        $dp = array_fill(0, count($arr), array_fill(0, $aim+1, 0));
        for ($i = 0; $i < count($arr); $i++) {
            $dp[$i][0] = 1;
        }
        for ($j = 1; $arr[0] * $j <= $aim; $j++) {
            $dp[0][$arr[0] * $j] = 1;
        }
        for ($i = 1; $i < count($arr); $i++) {
            for ($j = 1; $j <= $aim; $j++) {
                $num = 0;
                for ($k = 0; $j - $arr[$i] * $k >= 0; $k++) {
                    $num += $dp[$i-1][$j - $arr[$i] * $k];
                }
                $dp[$i][$j] = $num;
            }
        }
        return $dp[count($arr) - 1][$aim];
    }
}
```