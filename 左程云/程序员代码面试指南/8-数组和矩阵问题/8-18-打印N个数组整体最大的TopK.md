## 打印N个数组整体最大的TopK

# 题目
有N个长度不一的数组，所有的数组都是有序的，请从大到小打印这N个数组整体最大的前N个数。

例如，输入含有N行元素的二维数组可以代表N个一维数组。  
&emsp;219, 405, 538, 845, 971  
&emsp;148, 558  
&emsp;52, 99, 348, 691

再输入整数 k=5，则打印：  
Top 5: 971, 845, 691, 558, 538

【要求】  
1. 如果所有数组的元素个数小于 K，则从大到小打印所有的数
2. 时间复杂度为 O(KlogN)

# 解析
&emsp;&emsp;利用对结构和堆排序的过程完成。具体步骤如下：

1. 构建一个大小为 N 的大根堆 heap，建堆的过程就是把每个数组的最后一个值（也就是该数组的最大值）依次加入堆里，这个过程是建堆时的调整过程（heapInsert）
2. 建好堆之后，此时 heap 堆顶的元素时所有数组的最大值中最大的那个，打印堆顶元素。
3. 假设堆顶元素来自 a 数组的 i 位置。那么接下来把堆顶的前一个数（即 a[i-1]）放在 heap 的头部，也就是用 a[i-1] 替换原本的堆顶，
   然后从堆的头部开始调整堆，使其重新标为大根堆（heapify）
4. 这样每次都可以得到一个堆顶元素 max，在打印完成都经历步骤3的调整过程，整体打印 K 次，就是从大到小全部的 Top K。
5. 在重复步骤3的过程中，如果 max 来自的那个数组（假设是数组 a）已经没有元素，也就是说 max 已经是 a[0] 了，那么就把 heap 中的最后一个元素
   放在 heap 的头部，然后把 heap 的大小减一（heapSize-1），最后依然从堆的头部开始调整堆，使其重新变成大根堆。
6. 直到打印了 K 个数，过程结束
7. 为了知道每一次的 max 来自哪个数组的哪个位置，放在堆里的元素是如下的 HeapNode 类。
8. 代码参考 printTopK 方法。

# 代码
```php
$matrix = [
    [219, 405, 538, 845, 971],
    [148, 558],
    [52, 99, 348, 691],
];
$K = 5;
$o = new Zuochengyun8_18();
$o->main($matrix, $K);

class HeapNode
{
    public $value;      //值
    public $arrNum;     //来自哪个数组
    public $index;      //数组的哪个位置

    public function __construct($value, $arrNum, $index)
    {
        $this->value = $value;
        $this->arrNum = $arrNum;
        $this->index = $index;
    }
}


class Zuochengyun8_18
{
    public function main($matrix, $K)
    {
        $this->printTopK($matrix,  $K);
    }

    public function printTopK($matrix, $K)
    {
        $heapSize = count($matrix);
        $heap = array();
        for ($i = 0; $i < $heapSize; $i++) {
            $index = count($matrix[$i]) - 1;
            $heap[$i] = new  HeapNode($matrix[$i][$index], $i, $index);
            $this->heapInsert($heap, $i);
        }
        echo "Top {$K}: ";
        for ($i = 0 ; $i != $K; $i++) {
            if ($heapSize == 0) {
                break;
            }
            echo $heap[0]->value . ' ';
            if ($heap[0]->index != 0) {
                $heap[0]->value = $matrix[$heap[0]->arrNum][--$heap[0]->index];
            } else {
                $this->swap($heap, 0, $heapSize);
            }
            $this->heapify($heap, 0, $heapSize);
        }
    }

    protected function heapInsert(&$heap, $index)
    {
        while ($index != 0) {
            $parent = ($index - 1) >> 1;
            if ($heap[$parent]->value < $heap[$index]->value) {
                $this->swap($heap, $parent, $index);
                $index = $parent;
            } else {
                break;
            }
        }
    }

    protected function heapify(&$heap, $index, $heapSize)
    {
        $left = $index * 2 + 1;
        $right = $index * 2 + 2;
        $largest = $index;
        while ($left < $heapSize) {
            if ($heap[$left]->value > $heap[$index]->value) {
                $largest = $left;
            }
            if ($right < $heapSize && $heap[$right]->value > $heap[$largest]->value) {
                $largest = $right;
            }
            if ($largest != $index) {
                $this->swap($heap, $largest, $index);
            } else {
                break;
            }
            $index = $largest;
            $left = $index * 2 + 1;
            $right = $index * 2 + 2;
        }
    }

    protected function swap(&$heap, $index1, $index2)
    {
        $tmp = $heap[$index1];
        $heap[$index1] = $heap[$index2];
        $heap[$index2] = $tmp;
    }

}
```
