## 自然数数组的排序

# 题目
给定一个长度为N的整型数组arr，其中有N个互不相等的自然数1~N。

请实现arr的排序，但是不要把下标0\~N-1位置上的数通过直接赋值的方式替换成1\~N。

【要求】
时间复杂度为O(N)，空间复杂度为O(1)


# 解析
&emsp;&emsp;arr在调整之后应该是下标从0到N-1的位置上依次放着1~N，也就是 arr[index] = index + 1

&emsp;&emsp;方法一：
1. 从左到右遍历 arr，当前位置假设为 i
2. 如果 arr[i]==i+1，说明当前位置不用调整，继续遍历下个位置
3. 如果 arr[i]!=i+1，说明此时 i 位置的数 arr[i] 不应该放在 i 位置上，接下来进行跳的过程。

&emsp;&emsp;举例说明，比如 [1,2,5,3,4]：
- 假设遍历到位置2，也就是5这个数。5应该在位置4，所以把5放进去，数组变成[1,2,5,3,5]
- 同时，4这个数是被5踢下来的数，应该在位置3，所以把4放进去，数组变成[1,2,5,4,5]
- 同时，3这个数是被4踢下来的数，应该在位置2，所以把3放进去，数组变成[1,2,3,4,5]。
- 当跳了一圈回到原位置后，会发现此时 arr[i]==i+1，继续遍历下个位置
- 代码参考 sort1 方法

---

&emsp;&emsp;方法二：
1. 从左到右遍历 arr，当前位置假设为 i
2. 如果 arr[i]==i+1，说明当前位置不用调整，继续遍历下个位置
3. 如果 arr[i]!=i+1，说明此时 i 位置的数 arr[i] 不应该放在 i 位置上，接下来进行交换的过程。

&emsp;&emsp;举例说明：比如[1,2,5,3,4]:
- 假设遍历到位置2，也就是5这个数。5应该在位置4，所以位置4上的数4和5交换，数组变成[1,2,4,3,5]
- 此时 arr[2]!=3，4这个数应该在位置3上，所以3和4交换，数组变成[1,2,3,4,5]
- 此时 arr[2]==3，遍历下个位置
- 代码参考 sort2 方法

# 代码
```php
$arr=[1,2,5,3,4];
$o = new Zuochengyun8_12();
$o->main($arr);

class Zuochengyun8_12
{
    public function main($arr)
    {
        print_r($this->sort1($arr));
        print_r($this->sort2($arr));
    }

    public function sort1($arr)
    {
        for ($i = 0; $i < count($arr); $i++) {
            $tmp = $arr[$i];
            while ($arr[$i] != $i + 1) {
                $next = $arr[$tmp - 1];
                $arr[$tmp - 1] = $tmp;
                $tmp = $next;
            }
        }
        return $arr;
    }

    public function sort2($arr)
    {
        for ($i = 0; $i < count($arr); $i++) {
            while ($arr[$i] != $i + 1) {
                $tmp = $arr[$arr[$i] - 1];
                $arr[$arr[$i] - 1] = $arr[$i];
                $arr[$i] = $tmp;
            }
        }
        return $arr;
    }
}
```
