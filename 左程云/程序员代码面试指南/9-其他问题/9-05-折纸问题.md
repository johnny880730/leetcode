## 折纸问题

# 题目
把一张纸条竖着放在桌子上，然后把纸条的下边向上对折一次，压出折痕后展开。此时折痕是凹下去的，即折痕突起的方向指向纸条的背面。
如果从纸条的下边向上连续对折两次，压出折痕后展开，此时有三条折痕，从上到下依次是下折痕、下折痕、上折痕。给定一个输入参数 N，
代表纸条从下边向上连续对折 N 次，请从上到下打印所有折痕的方向。

【例如】  
N=1 时，打印  
down  
N=2 时，打印：
down  
down  
up  


# 解析
&emsp;&emsp;对折第一次产生的折痕：&emsp;&emsp;&emsp;&emsp;下  
&emsp;&emsp;对折第二次产生的折痕：&emsp;&emsp;下&emsp;&emsp;&emsp;上  
&emsp;&emsp;对折第三次产生的折痕：&emsp;下&emsp;上&emsp;下&emsp;上  
&emsp;&emsp;对折第四次产生的折痕：下 上 下 上 下 上 下 上

&emsp;&emsp;根据上述关系可以总结出：
- 产生第 i+1 次 折痕的过程，就是在对折 i 次 产生的每一条折痕的左右两侧，依次插入下折痕和上折痕的过程
- 所有折痕的结构时一棵满二叉树，在这棵满二叉树中，头节点为下折痕，每一颗左子树的头节点为下折痕，每一个右节点的头节点为上折痕。
- 从上到下打印所有折痕方向的过程，就是二叉树的中序遍历

&emsp;&emsp;代码参考 printAllFolds 方法。

&emsp;&emsp;连续对阵 N 次后一定会产生 2^(N-1) 条折痕，所以要打印所有的节点，不管用什么方法，时间复杂度肯定都是 O(2^N)，
因为解的空间本身就这么大，但是提供的方法的额外空间复杂度为 O(N)，也就是这棵满二叉树的高度，额外空间主要用来维持递归函数的运行，
也就是函数栈的大小。


# 代码
```php
$n = 4;
$o  = new Zuochengyun9_05();
$o->main($n);

class Zuochengyun9_05
{
    public function main($n)
    {
        $this->printAllFolds($n);
    }

    public function printAllFolds($n)
    {
        $this->printProcess(1, $n ,true);
    }

    public function printProcess($i, $n, $down)
    {
        if ($i > $n) {
            return;
        }
        $this->printProcess($i + 1, $n, true);
        echo ($down ? 'down ' : 'up ');
        $this->printProcess($i + 1, $n, false);
    }
}
```