## 在有序数组中找到最小值

# 题目
有序数组 arr 可能经过一次旋转处理，也可能没有，且 arr 可能存在重复的数。例如：有序数组 [1,2,3,4,5,6,7]，可以旋转成 
[4,5,6,7,1,2,3]等。给定一个可能旋转过的有序数组 arr，返回 arr 中的最小值。

# 解析
&emsp;&emsp;为了方便描述，我们把没经过旋转前有序数组 arr 最左边的数在经过旋转之后所处的位置叫做“断点”。例如题目例子的数组，
旋转后断点在 1 所处的位置，也就是位置 4。如果没有经过旋转处理，断点在位置 0。只要找到断点，就找到了最小值。

&emsp;&emsp;这里提供的方式做到了尽可能利用二分查找，但是最差情况下无法避免 O(N) 的时间复杂度。假设目前想在 arr[low..high] 范围上
找到这个范围的最小值（那么初始时 low=0，high=arr.length-1），以下是具体过程：
1. 如果 arr[low]<arr[high]，说明 arr[low..high] 上没有旋转，断点就是 arr[low]，返回 arr[low] 即可。
2. 令 mid=(low+high)/2，mid 即 中间的位置
   1. 如果 arr[low]>arr[mid]，说明断点一定在 arr[low..mid] 上，则令 high=mid，回到步骤1。
   2. 如果 arr[mid]>arr[high]，说明断点一定在 arr[mid..high]上，则令 low=mid，回到步骤1。
3. 如果步骤1和2的逻辑都没有命中，说明什么呢？步骤1没有命中说明 arr[low]≥arr[high]，步骤2的第1小步没有命中说明 arr[low]≤arr[mid]，
步骤2的第2小步没有命中说明 arr[mid]≤arr[high]。此时只有一种情况，就是 arr[low]==arr[mid]==arr[high]。面对这种情况根本无法判断
断点在哪里，很多书籍在面对这种情况是都选择直接遍历 arr[low..high] 的方法找出断点。但其实还是可以继续为二分创造条件，生成变量 i，初始时令
i=low，开始向右遍历arr(i++)，那么会有三种情况：
   - 情况1：遍历到某个位置时发现 arr[low]>arr[i]，那么 arr[i] 就是断点的值，因为在数组中发现的降序必然是断点，直接返回 arr[i]。
   - 情况2：遍历到某个位置时发现 arr[low]<arr[i]，说明 arr[i]>arr[mid]，那么说明断点在 arr[i..mid] 上。  
     此时又可以开始二分，令 high=mid，重新回到步骤1
   - 情况3：如果 i==mid 都没有出现情况1和2，说明从 arr 的 low 位置到 mid 位置，值全部都一样。那么断点只可能在 arr[mid..high] 上，
     所以令 low=mid，进行后续的二分过程，重新回到步骤1.

&emsp;&emsp;代码参考 getMin 方法

# 代码
```php
$arr = [4,5,6,7,1,2,3];
$o = new Zuochengyun9_13();
$o->main($arr);

class Zuochengyun9_13
{
    public function main($arr)
    {
        echo $this->getMin($arr) . PHP_EOL;
    }

    public function getMin($arr)
    {
        $low = 0;
        $high = count($arr) - 1;
        while ($low < $high) {
            if ($low == $high - 1) {
                break;
            }
            if($arr[$low] < $arr[$high]) {
                return $arr[$low];
            }
            $mid = ($low + $high) >> 1;
            if ($arr[$low] > $arr[$mid]) {
                $high = $mid;
                continue;
            }
            if ($arr[$mid] > $arr[$high]) {
                $low = $mid;
                continue;
            }
            while ($low < $mid) {
                if ($arr[$low] == $arr[$mid]) {
                    $low++;
                } else if ($arr[$low] < $arr[$mid]) {
                    return $arr[$low];
                } else {
                    $high = $mid;
                    break;
                }
            }
        }
        return min($arr[$low], $arr[$high]);
    }
}
```