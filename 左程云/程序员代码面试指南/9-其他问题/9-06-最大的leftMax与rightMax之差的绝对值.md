# 最大的 leftMax 与 rightMax 之差的绝对值

# 题目
给定一个长度为 N(N>1) 的整型数组 arr，可以划分成左右两个部分，左部分为 arr[0..K]，右部分为 arr[K+1..N-1]，K 可以取值的范围为 [0..N-2]。
求这么多划分方案中，左部分中的最大值减去右部分最大值的绝对值中，最大是多少？  

# 示例
```
arr = [2,7,3,1,1]
当左部分为 [2,7]，右部分为 [3,1,1] 时，左部分最大值减去右部分最大值的绝对值为4。
当左部分为 [2,7,3]，右部分为 [1,1] 时，左部分最大值减去右部分最大值的绝对值为6。
还有很多划分方案，但最终返回6。
```


# 解析
方法一：时间复杂度为 O(N²)，额外空间复杂度为 O(1)。

这是最笨的方法，在数组的每个位置 i 都做一次划分，找到 arr[0..i] 的最大值 leftMax，找到 arr[i+1..N-1] 的最大值 rightMax，
然后计算两个值相减的绝对值。每次划分都计算一次，自然可以得到最终结果。

代码参考 maxABS1 方法。

---

方法二：时间复杂度为 O(N)，额外空间复杂度为 O(N)。

使用预处理数组的方法，先从左到右遍历一次生成 leftArr。leftArr[i] 表示 arr[0..i] 中的最大值。
再从右到左遍历一次生成 rightArr。rightArr[i] 表示 arr[i..N-1] 中的最大值。
最后一次遍历看哪种划分情况下可以得到两部分最大的相减的绝对值。
因为预处理数组已经保存了左右划分的 max 值，所以过程得到了加速。

代码参考 maxABS2 方法。

---

方法三：时间复杂度为 O(N)，额外空间复杂度为 O(1)。

先求整个 arr 的最大值 max，因为 max 是全局最大值，所以不管怎么划分，max 要么成为左部分的最大值，要么成为右部分的最大值。
如果 max 作为左部分的最大值，只要让右部分的最大值尽量小就可以。怎么让右部分的最大值尽量小呢？

右部分只含有 arr[N-1] 的时候就是尽量小的时候。同理，如果 max 是右部分的最大值，只要让左部分的最大值尽量小就可以，左部分只含有 arr[0] 的时候
就是尽量小的时候。所以整个过程会变得很简单。

因为当范围增大时，范围内的最大值只会增大不会减小，所以保证范围内只有一个数即可。

代码参考 maxABS3 方法。

# 代码
```php
$arr = [2,7,3,1,1];
$o  = new Zuochengyun9_06();
$o->main($arr);

class Zuochengyun9_06 {

    public function main($arr) {
        echo $this->maxABS1($arr) . PHP_EOL;
        echo $this->maxABS2($arr) . PHP_EOL;
        echo $this->maxABS3($arr) . PHP_EOL;
    }

    public function maxABS1($arr) {
        $res = PHP_INT_MIN;
        $len = count($arr);
        for ($i = 0; $i < $len - 1; $i++) {
            $maxLeft = PHP_INT_MIN;
            for ($j = 0; $j != $i + 1; $j++) {
                $maxLeft = max($arr[$j], $maxLeft);
            }
            $maxRight = PHP_INT_MIN;
            for ($j = $i + 1; $j != $len; $j++) {
                $maxRight = max($arr[$j], $maxRight);
            }
            $res = max($res, abs($maxRight - $maxLeft));
        }
        return $res;
    }

    public function maxABS2($arr) {
        $len = count($arr);
        $leftArr = array_fill(0, $len, 0);
        $rightArr = array_fill(0, $len, 0);
        $leftArr[0] = $arr[0];
        $rightArr[$len - 1] = $arr[$len - 1];
        for ($i = 1; $i < $len; $i++) {
            $leftArr[$i] = max($leftArr[$i - 1], $arr[$i]);
        }
        for ($i = $len - 2; $i > -1; $i--) {
            $rightArr[$i] = max($rightArr[$i + 1], $arr[$i]);
        }
        $res = 0;
        for ($i = 0; $i < $len - 1; $i++) {
            $res = max($res, abs($leftArr[$i] - $rightArr[$i + 1]));
        }
        return $res;
    }

    public function maxABS3($arr) {
        $len = count($arr);
        $max = max($arr);
        return $max - min($arr[0], $arr[$len - 1]);
    }

}
```